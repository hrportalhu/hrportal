<?
include("install/modulinfo.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="cache-control" content="no-cache" />
  <meta name="robots" content="index,follow" />
  <meta name="publisher" content="Planet Star Kft" />
  <meta name="copyright" content="" />
  <meta name="author" content="Planet Star Kft" />
  <meta name="distribution" content="global" />
  <meta name="description" content="Webalkalmazás" />
  <meta name="keywords" content="webalkalmazás," />
  <link rel="stylesheet" type="text/css" media="screen,projection,print" href="./css/setup.css" />
  <link rel="stylesheet" type="text/css" media="screen,projection,print" href="./css/text.css" />
  <link rel="icon" type="image/x-icon" href="./img/favicon.ico" />
  <title>PlanetSystems</title>
  	<script type="text/javascript" src="js/ajax.js">;</script>
  	<script type="text/javascript" src="js/plajax.js">;</script>
  	<script type="text/javascript" src="js/prototype.js"></script>
	<script type="text/javascript" src="js/fastinit.js"></script>
	<script type="text/javascript" src="js/tablekit.js"></script>
	<script type="text/javascript" src="js/fabtabulous.js"></script>
    <script type="text/javascript" src="js/dropdown.js"></script>
</head>
<body>

  <div class="page-container">
    <div class="header">
      <div class="header-top">
        <a class="sitelogo" href="#" title="Go to Start page"></a>
        <div class="sitename">
          <h1><a href="index.html" title="Go to Start page">Planet System v2.01</a></h1>
          <h2>Template rendszer</h2>
        </div>
      </div>
        <div class="header-bottom">
         <div class="nav2">
          <ul>
            <li><a href="index.php">Instal</a></li>
          </ul>
        </div>
      <div class="header-breadcrumbs">
        <ul>
          <li><a href="index.php">Alapinformációk gyűjtée</a></li>
		 </ul>
       </div>
    </div>
    <div class="main">
      <div class="main-navigation">
        <div class="round-border-topright"></div>
		<h1 class="first">Információk</h1>	
		  <?
			for($i=0;$i<count($modulinfo);$i++){
				echo"<h1>".$modulinfo[$i]['name']."</h1>";
				echo"<p>".$modulinfo[$i]['stext']."<p>";	
			}
		?>	
      </div>
      <div class="main-content">
	  
	  <h1>Telepítési folyamat</h1>
	  <?
			include("install/".$modulinfo[0]['formname']);
				
			
		?>
        <hr class="clear-contentunit" />          
      </div>
    </div>
    <div class="footer">
      <p>Copyright &copy; 2009 Planet Star Kft. | All Rights Reserved</p>
      <p class="credits">Design by <a href="http://www.planetstar.hu" title="Designer Homepage">Planet Star Kft</a> | Powered by <a href="#" title="">PlanetSys</a> | <a href="http://validator.w3.org/check?uri=referer" title="Validate XHTML code">XHTML 1.0</a> | <a href="http://jigsaw.w3.org/css-validator/" title="Validate CSS code">CSS 2.0</a></p>
    </div>      
  </div> 
</div>  
</body>
<?
//mysql_close($connid);
?>
</html>
