<?

/*Planet Star ügyviteli rendszerek alapmoduljának adatbázisa
 *Ver:1.001; 
 *Created By: szucsjoee@gmail.com
 *Published by Planet Star Kft.  
*/
db_execute("CREATE TABLE ".$tableprefix."almeu (
  id int(11) NOT NULL AUTO_INCREMENT,
  m_id int(11) NOT NULL,
  jcs_id int(11) NOT NULL,
  name varchar(30) COLLATE utf8_hungarian_ci NOT NULL,
  status char(1) COLLATE utf8_hungarian_ci DEFAULT '1',
  PRIMARY KEY (id),
  KEY m_id (m_id),
  KEY jcs_id (jcs_id),
  KEY status (status)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci");

db_execute("INSERT INTO ".$tableprefix."almeu (id, m_id, jcs_id, name, status) VALUES  (1,1,1,'Alapadminisztració','1')");

db_execute("CREATE TABLE ".$tableprefix."user (
  id int(11) NOT NULL AUTO_INCREMENT,
  u_login varchar(30) COLLATE utf8_hungarian_ci NOT NULL,
  jcs_id int(11) NOT NULL,
  u_realname varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  u_shortname varchar(50) COLLATE utf8_hungarian_ci NOT NULL,
  u_pass varchar(250) COLLATE utf8_hungarian_ci NOT NULL,
  u_mail varchar(250) COLLATE utf8_hungarian_ci NOT NULL,
  u_telno varchar(7) COLLATE utf8_hungarian_ci NOT NULL,
  u_mobil varchar(20) COLLATE utf8_hungarian_ci DEFAULT NULL,
  u_photo varchar(250) COLLATE utf8_hungarian_ci NOT NULL,
  status char(1) COLLATE utf8_hungarian_ci DEFAULT '1',
  u_ugr varchar(250) COLLATE utf8_hungarian_ci NOT NULL,
  u_fgr varchar(200) COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci");

db_execute("INSERT INTO ".$tableprefix."user (id, u_login, jcs_id, u_realname, u_shortname, u_pass, u_mail, u_telno, u_mobil, u_photo, status, u_ugr, u_fgr) VALUES 
  (1,'".$login."',1,'".$arealname."','".$login."','".$cryptedpass."','".$aemail."','".$atelno."','".$amobil."','','1','1,2,3,4,6,7','1,2,3')");

db_execute("CREATE TABLE ".$tableprefix."func (
  id int(11) NOT NULL AUTO_INCREMENT,
  al_id int(11) NOT NULL,
  name varchar(50) COLLATE utf8_hungarian_ci NOT NULL,
  path varchar(250) COLLATE utf8_hungarian_ci NOT NULL,
  m_id int(11) NOT NULL,
  pic varchar(50) COLLATE utf8_hungarian_ci DEFAULT NULL,
  status char(1) COLLATE utf8_hungarian_ci DEFAULT '1',
  PRIMARY KEY (id),
  KEY al_id (al_id),
  KEY path (path),
  KEY m_id (m_id),
  KEY status (status)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci");

db_execute("INSERT INTO ".$tableprefix."func (id, al_id, name, path, m_id, pic, status) VALUES 
  (1,1,'Menükezelés','admmenu','1','admmenu.png','1'),
  (2,1,'Jogcsoportkezelés','admjogcsop',1,'admjogcsop.png','1'),
  (3,1,'Almenükezelés','admalmeu','1','admalmeu.png','1'),
  (4,1,'Funkciókezelés','admfunc','1','admfunc.png','1'),
  (5,1,'Felhasználók','admuser','1','admuser.png','1'),
  (6,1,'Kilépes','logout=yes','1','logout.png','0'),
  (7,1,'Modulkezelő','admmoduls','1','admmoduls.png','1'),
  (8,1,'Segítségnyújtó','admhelp','1','admhelp.png','1'),
  (9,1,'Naplózó','admlogs','1','admlogs.png','1')");

db_execute("CREATE TABLE ".$tableprefix."fu_us (
  fx_id int(11) NOT NULL AUTO_INCREMENT,
  f_id int(11) NOT NULL DEFAULT '0',
  u_id int(11) NOT NULL DEFAULT '0',
  fx_valid char(1) COLLATE utf8_hungarian_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (fx_id),
  KEY f_id (f_id),
  KEY u_id (u_id),
  KEY fx_valid (fx_valid)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci");

db_execute("INSERT INTO ".$tableprefix."fu_us (fx_id, f_id, u_id, fx_valid) VALUES 
  (1,1,1,'Y'),
  (2,2,1,'Y'),
  (3,3,1,'Y'),
  (4,4,1,'Y'),
  (5,5,1,'Y'),
  (6,6,1,'N'),
  (7,7,1,'Y'),
  (8,8,1,'Y'),
  (9,9,1,'Y')");

db_execute("CREATE TABLE ".$tableprefix."jogcsop (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(20) COLLATE utf8_hungarian_ci NOT NULL,
  status char(1) COLLATE utf8_hungarian_ci DEFAULT '1',
  PRIMARY KEY (id),
    KEY status (status)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci");

db_execute("INSERT INTO ".$tableprefix."jogcsop (id, name, status) VALUES 
  (1,'Admin','1'),
  (2,'Operator','1'),
  (3,'User','1')");
    
db_execute("CREATE TABLE ".$tableprefix."menu (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(200) COLLATE utf8_hungarian_ci NOT NULL,
  status char(1) COLLATE utf8_hungarian_ci DEFAULT '1',
  PRIMARY KEY (id),
  KEY status (status)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci");
db_execute("INSERT INTO ".$tableprefix."menu (id, name, status) VALUES  (1,'Adminisztrációk','1')");

db_execute("CREATE TABLE ".$tableprefix."moduls (
  id int(11) NOT NULL AUTO_INCREMENT,
  modul varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  modul_dir varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  version float(9,3) NOT NULL,
  pubdate datetime NOT NULL,
  status char(1) COLLATE utf8_hungarian_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (id),
  KEY modul (modul),
  KEY modul_dir (modul_dir),
  KEY status (status)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci");

db_execute("INSERT INTO ".$tableprefix."moduls (id, modul, modul_dir, version, pubdate, status) VALUES 
  (1,'main','main_1001',1.001,'2009-02-20 00:00:00','1')");

db_execute("CREATE TABLE  ".$tableprefix."help (
  id int(11) NOT NULL AUTO_INCREMENT,
  cs_id int(11) NOT NULL,
  acs_id int(11) NOT NULL,
  func_id int(11) NOT NULL,
  short_text varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  helptext text COLLATE utf8_hungarian_ci NOT NULL,
  status char(1) COLLATE utf8_hungarian_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (id),
  KEY cs_id (cs_id),
  KEY acs_id (acs_id),
  KEY func_id (func_id),
  KEY status (status)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci"); 

db_execute("CREATE TABLE ".$tableprefix."logs (
  id int(11) NOT NULL AUTO_INCREMENT,
  paranoid char(1) COLLATE utf8_hungarian_ci NOT NULL DEFAULT '0',
  uid int(11) NOT NULL,
  func varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  events varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  text varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  ldate datetime NOT NULL,
  status char(1) COLLATE utf8_hungarian_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (id),
  KEY paranoid (paranoid),
  KEY func (func),
  KEY ldate (ldate),
  KEY uid (uid)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci");

?>
