(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 250,
	height: 250,
	fps: 30,
	color: "#FFFFFF",
	manifest: [
		{src:"images/bgr.jpg", id:"bgr"},
		{src:"images/gradiant_top.png", id:"gradiant_top"},
		{src:"images/logo.png", id:"logo"},
		{src:"images/text1.png", id:"text1"},
		{src:"images/text2.png", id:"text2"},
		{src:"images/text3.png", id:"text3"}
	]
};



// symbols:



(lib.bgr = function() {
	this.initialize(img.bgr);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,250,250);


(lib.gradiant_top = function() {
	this.initialize(img.gradiant_top);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,250,70);


(lib.logo = function() {
	this.initialize(img.logo);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,117,46);


(lib.text1 = function() {
	this.initialize(img.text1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,121,17);


(lib.text2 = function() {
	this.initialize(img.text2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,206,63);


(lib.text3 = function() {
	this.initialize(img.text3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,124,94);


(lib.text3_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.text3();
	this.instance.setTransform(-65.5,-46.5);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-65.5,-46.5,124,94);


(lib.text2_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.text2();
	this.instance.setTransform(-101,-31.5);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-101,-31.5,206,63);


(lib.text1_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.text1();
	this.instance.setTransform(-72.5,-9.5);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-72.5,-9.5,121,17);


(lib.logo_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.logo();
	this.instance.setTransform(-62.5,-24.5);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-62.5,-24.5,117,46);


(lib.gradiant_top_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.gradiant_top();
	this.instance.setTransform(-150,0);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-150,0,250,70);


(lib.bgr_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.bgr();
	this.instance.setTransform(-125,-125);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-125,-125,250,250);


// stage content:
(lib.expat_banner_250x250 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// text-3
	this.instance = new lib.text3_1();
	this.instance.setTransform(326.4,112);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(64).to({_off:false},0).to({x:80.4},15,cjs.Ease.get(1)).wait(71));

	// text-2
	this.instance_1 = new lib.text2_1();
	this.instance_1.setTransform(-108.6,209);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(64).to({_off:false},0).to({x:131.4},15,cjs.Ease.get(1)).wait(71));

	// text-1
	this.instance_2 = new lib.text1_1();
	this.instance_2.setTransform(325.9,26.4);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(44).to({_off:false},0).to({x:195.9},10,cjs.Ease.get(1)).wait(96));

	// logo
	this.instance_3 = new lib.logo_1();
	this.instance_3.setTransform(-64.6,30.4);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(39).to({_off:false},0).to({x:65.4},10,cjs.Ease.get(1)).wait(101));

	// gradiant_top
	this.instance_4 = new lib.gradiant_top_1();
	this.instance_4.setTransform(150,-71);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(19).to({_off:false},0).to({y:0},10).wait(121));

	// bgr
	this.instance_5 = new lib.bgr_1();
	this.instance_5.setTransform(125,125);
	this.instance_5.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).to({alpha:1},9).wait(141));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(125,125,250,250);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;
