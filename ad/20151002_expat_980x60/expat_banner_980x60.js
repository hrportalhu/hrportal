(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 980,
	height: 60,
	fps: 24,
	color: "#FFFFFF",
	manifest: [
		{src:"images/bgr.jpg", id:"bgr"},
		{src:"images/gradiant_top.png", id:"gradiant_top"},
		{src:"images/logo.png", id:"logo"},
		{src:"images/text1.png", id:"text1"},
		{src:"images/text2.png", id:"text2"},
		{src:"images/text3.png", id:"text3"}
	]
};



// symbols:



(lib.bgr = function() {
	this.initialize(img.bgr);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,980,60);


(lib.gradiant_top = function() {
	this.initialize(img.gradiant_top);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,251,60);


(lib.logo = function() {
	this.initialize(img.logo);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,135,53);


(lib.text1 = function() {
	this.initialize(img.text1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,161,20);


(lib.text2 = function() {
	this.initialize(img.text2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,307,25);


(lib.text3 = function() {
	this.initialize(img.text3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,303,25);


(lib.text3_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.text3();
	this.instance.setTransform(-65.5,-46.5);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-65.5,-46.5,303,25);


(lib.text2_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.text2();
	this.instance.setTransform(-101,-31.5);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-101,-31.5,307,25);


(lib.text1_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.text1();
	this.instance.setTransform(-72.5,-9.5);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-72.5,-9.5,161,20);


(lib.logo_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.logo();
	this.instance.setTransform(-62.5,-24.5);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-62.5,-24.5,135,53);


(lib.gradiant_top_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.gradiant_top();
	this.instance.setTransform(-150,0);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-150,0,251,60);


(lib.bgr_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.bgr();
	this.instance.setTransform(-490,-30);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-490,-30,980,60);


// stage content:
(lib.expat_banner_980x60 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// text-3
	this.instance = new lib.text3_1();
	this.instance.setTransform(1052.4,51);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(59).to({_off:false},0).to({x:504.4},15,cjs.Ease.get(1)).wait(76));

	// text-2
	this.instance_1 = new lib.text2_1();
	this.instance_1.setTransform(-209.2,64);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(59).to({_off:false},0).to({x:708.8},15,cjs.Ease.get(1)).wait(76));

	// text-1
	this.instance_2 = new lib.text1_1();
	this.instance_2.setTransform(216.4,-17);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(39).to({_off:false},0).to({y:46},10,cjs.Ease.get(1)).wait(101));

	// logo
	this.instance_3 = new lib.logo_1();
	this.instance_3.setTransform(71.4,88);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(39).to({_off:false},0).to({y:28},10,cjs.Ease.get(1)).wait(101));

	// gradiant_top
	this.instance_4 = new lib.gradiant_top_1();
	this.instance_4.setTransform(-100,0);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(19).to({_off:false},0).to({x:150},10).wait(121));

	// bgr
	this.instance_5 = new lib.bgr_1();
	this.instance_5.setTransform(490,30);
	this.instance_5.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).to({alpha:1},9).wait(141));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(490,30,980,60);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;