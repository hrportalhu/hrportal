function Ximage (context, src, x, y, xalpha) {
	this.src = src;
	if (x === undefined) { x = 0; }
	if (y === undefined) { y = 0; }
	if (xalpha === undefined) { xalpha = 1; }

	var startX = this.x = x;
	var startY = this.y = y;
	var startAlpha = this.xalpha = xalpha;
	var startContext = this.context = context;
	this.vx = 0;
	this.vy = 0;
	this.rotation = 0;
	this.scaleX = 1;
	this.scaleY = 1;

	this.isTweening = true;
	this.time = 0;

	var image = new Image();
	image.src = src;
	image.onload = function () {
		startContext.globalAlpha = startAlpha;
		startContext.drawImage(image, startX, startY);
	};
	this.image = image;
}

Ximage.prototype.draw = function () {
	this.context.save();
	this.context.globalAlpha = this.xalpha;
	this.context.drawImage(this.image, this.x, this.y);
	this.context.restore();
};

Ximage.prototype.move = function(startX, startY, targetX, targetY, duration) {
	if (duration === undefined) { duration = 50; }

	if (this.isTweening) {
		change_x = targetX - startX;
		change_y = targetY - startY;
		this.x = easing_function(this.time, startX, change_x, duration);
		this.y = easing_function(this.time, startY, change_y, duration);
		this.time += 1;
		if (this.time > duration) {
			this.isTweening = false;
		}
	}
	this.draw();
};

Ximage.prototype.chgalpha = function(startAlpha, targetAlpha, duration) {
	if (duration === undefined) { duration = 50; }

	if (this.isTweening) {
		change_alpha = targetAlpha - startAlpha;
		if (change_alpha == 0) change_alpha = -1;
		this.xalpha = easing_function(this.time, startAlpha, change_alpha, duration);
		this.time += 1;
		if (this.time > duration || this.xalpha == targetAlpha) {
			this.isTweening = false;
		}
	}
	this.draw();
};

Ximage.prototype.reset = function() {
	this.isTweening = true;
	this.time = 0;
};