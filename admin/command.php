<!DOCTYPE html>
<html lang="hu">

<head>
	
    <meta charset="utf-8">
	<title>HRPortal.hu parancsoldal</title>    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/planetstar.css" type="text/css" rel="stylesheet" />


</head>
<body style="background-color:#BFBFBF;">
<div style="width:960px;margin:0 auto;background-color:#fff;border:1px solid #4D4D4D;padding:10px;" >
	<h1>HRPortal.hu parancsfájlok</h1>
	<a href="http://www.hrportal.hu/do_index.php" target="_blank">HRPortal.hu index oldal újragenerálása</a><br />
	<a href="http://www.hrportal.hu/cron/generate_hrblogbox.php" target="_blank">Blog BOX újragenerálása</a><br />
	<a href="http://www.hrportal.hu/cron/generate_hetiajanlo.php" target="_blank">Heti Ajánló BOX újragenerálása</a><br />
	<a href="http://www.hrportal.hu/cron/generate_szakertobox.php" target="_blank">Szekértő BOX újragenerálása</a><br />
	<a href="http://www.hrportal.hu/cron/generate_hrclubbox.php" target="_blank">HRClub BOX újragenerálása</a><br />
	<a href="http://www.hrportal.hu/refreshcommon.phtml" target="_blank">5 perces generálás - hrportal.hu statikus index</a><br />
	<a href="http://www.hrportal.hu/refreshcommon_rss.phtml" target="_blank">RSS FEED-ek generálása</a><br />
	<a href="http://www.legjob.hu/rss/rss_create.phtml" target="_blank">LEGJOB RSS FEED-ek generálása</a><br />
	<a href="http://www.hrportal.hu/cron/generate_hetiajanlo_hrclub.php" target="_blank">HRClub hetiajanló box generálása</a><br />
	<a href="http://www.hrportal.hu/cron/generate_hrallasok.php" target="_blank">HRÁllások doboz generálása</a><br />
</div>
</body>
</html>
