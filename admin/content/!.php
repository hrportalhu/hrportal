<?php
session_start();
if (!session_is_registered('count')) {
    session_register('count');
    $count = 1;
}
else {
    $count++;
}
?>

Hello visitor, you have seen this page <?php echo $count; ?> times.<p>

To continue, <A HREF="!.php?<?php echo strip_tags (SID)?>">click here</A>
