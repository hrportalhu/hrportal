<?php
 function make_menuitem($text, $url, $new=false, $kereslet=false)
  {
   global $page;
   
   //if ($page==$url) $active=true; else $active=false;
   if (strpos($_SERVER['QUERY_STRING'],$url)===false) $active=false; else $active=true;
   if ($page=="fooldal" and $page==$url) $active=true;
   
   if (substr($url,-1)=="!")
    $url=substr($url,0,-1)."?".SID;
   else
    {
     if ($kereslet==false){
	  if (!ereg("http://",$url) AND ($url!="/linktar.phtml")) 	 
      //if (($url!="/linktar.phtml") AND ($url!="/") AND ($url!="http://hrclub.hu")
	  $url="index.phtml?page=".$url."&".SID;
	 }
     else{
      $url="kereslet_data.phtml?page=".$url."&id=".$kereslet."&".SID;
	 }
    }
   
   switch ($active)
    {
     case false:
               switch ($new)
                {
                 case "feature":
                  $bgcolor="#5E6C89";
                  $bgcolor_lighter="#9198AE";
                  $highlight="Highlight_feature";                   
                 break;
                  
                 case "new":
                  $bgcolor="#EB8E16";
                  $bgcolor_lighter="#F1B25F";
                  $highlight="Highlight_new";                   
                 break;
                 
                 case "szak":
                  $bgcolor="#DC3A54";
                  $bgcolor_lighter="#DC8694";
                  $highlight="Highlight_szak";                   
                 break;                 
                 
				 case "club":
                  $bgcolor="#959583";
                  $bgcolor_lighter="#B0B79C";
                  $highlight="Highlight_club";                 
                  break;                 
				 
                 default:
                  $bgcolor="#7886A4";
                  $bgcolor_lighter="#A6ADC4";
                  $highlight="Highlight";                 
                  break;                 
                }

               $ret="
                 <tr>\n
                  <td colspan='2' height='1' background='/images/pixel.gif' bgcolor='$bgcolor_lighter'><img src='/images/pixel.gif'></td>\n
                 </tr>\n
                 <tr>\n
                  <td width='14' height='14' background='/images/pixel.gif' bgcolor='$bgcolor_lighter'><img src='/images/pixel.gif'></td>\n
                  <td align='left' height='14' bgcolor='$bgcolor' class='menuitem' onClick=\"window.location.href='$url';\" onMouseOver=\"$highlight(this,1)\" onMouseOut=\"$highlight(this,0)\">&nbsp;&nbsp;&nbsp;<a href='$url' class='menuitem'>$text</a></td>\n
                 </tr>\n
                 <tr>\n
                  <td colspan='2' height='1' background='/images/pixel.gif' bgcolor='#000000'><img src='/images/pixel.gif'></td>\n
                 </tr>\n"; 
               break;
     default  :
               $ret="
                 <tr>\n
                  <td colspan='2' height='1' background='/images/pixel.gif' bgcolor='#5A7B9D'><img src='/images/pixel.gif'></td>\n
                 </tr>\n
                 <tr>\n
                  <td colspan='2' align='left' height='14' bgcolor='#003366' class='menuitem' onClick=\"window.location.href='$url';\" onMouseOver=\"Highlight_active(this,1)\" onMouseOut=\"Highlight_active(this,0)\">&nbsp;&nbsp;&nbsp;<a href='$url' class='menuitem'>$text</a></td>\n
                 </tr>\n                 
                 <tr>\n
                  <td colspan='2' height='1' background='/images/pixel.gif' bgcolor='#000000'><img src='/images/pixel.gif'></td>\n
                 </tr>   ";              
               break;
    }
     
    return $ret;
  }

 # This function returns a HTTP POST, GET variable
 function http_gpvar($var)
  {
   $return=false;
   @$return=urldecode($HTTP_GET_VARS["$var"]);
   if (!$return)
    @$return=urldecode($HTTP_POST_VARS["$var"]);
   return $return;
  }   
     
 function http_gpcvar($var_name)
  {
   return @$_REQUEST[$var_name];
  }   
       
  function http_get_var($var_name)
  {
   global $_GET;
   return $_GET[$var_name];
  }

 function http_post_var($var_name)
  {
   global $_POST;  
   return $_POST[$var_name];
  }
  
 function http_file_var($var_name)
  {
   return @$_FILES[$var_name];
  }  
  
function fine_date($date,$onlydate=false)
 {
  if ($date=="0000-00-00 00:00:00")
   return "- nincs adat -";
   
  $myrow=getdate(strtotime($date));
  $dat=$myrow['year'].".";
  switch($myrow['mon'])
   {
    case 1: $dat.=' janu�r '; break;
    case 2: $dat.=' febru�r '; break;
    case 3: $dat.=' m�rcius '; break;
    case 4: $dat.=' �prilis '; break;
    case 5: $dat.=' m�jus '; break;
    case 6: $dat.=' j�nius '; break;
    case 7: $dat.=' j�lius '; break;
    case 8: $dat.=' augusztus '; break;
    case 9: $dat.=' szeptember '; break;
    case 10: $dat.=' okt�ber '; break;
    case 11: $dat.=' november '; break;
    case 12: $dat.=' december '; break;
   }     
  $dat.=$myrow['mday'].".";
  if ($onlydate==false)
   {
    if ($myrow['hours']!=0 || $myrow['minutes']!=0 || $myrow['seconds']!=0)
     $dat.=" ".(strlen($myrow['hours'])==1?"0".$myrow['hours']:$myrow['hours']).":".(strlen($myrow['minutes'])==1?"0".$myrow['minutes']:$myrow['minutes']).":".(strlen($myrow['seconds'])==1?"0".$myrow['seconds']:$myrow['seconds']);
   }
  return($dat);
 }  
 
function fine_date_digit($date,$onlydate=false)
 {
  if ($date=="0000-00-00 00:00:00")
   return "- nincs adat -";
   
  $myrow=getdate(strtotime($date));
  
  if ($myrow['mon']<10) $myrow['mon']="0".$myrow['mon'];
  if ($myrow['mday']<10) $myrow['mday']="0".$myrow['mday'];
  
  $dat=$myrow['year'].". ".$myrow['mon'].". ".$myrow['mday'].".";
  if ($onlydate==false)
   {
    if ($myrow['hours']!=0 || $myrow['minutes']!=0 || $myrow['seconds']!=0)
     $dat.=" ".(strlen($myrow['hours'])==1?"0".$myrow['hours']:$myrow['hours']).":".(strlen($myrow['minutes'])==1?"0".$myrow['minutes']:$myrow['minutes']).":".(strlen($myrow['seconds'])==1?"0".$myrow['seconds']:$myrow['seconds']);
   }
  return($dat);
 }   
 

// this function loads/saves data from/into a cookie.
function manage_cookie($cookiename)
 {
  if (http_gpvar($cookiename))
   {
    if (isset($_COOKIE[$cookiename]))
    { 
     if (http_gpvar($cookiename)!=$_COOKIE[$cookiename])
      {
       cookieHeader($cookiename,http_gpvar($cookiename),90);
       ${$cookiename}=http_gpvar($cookiename);
      } else
         ${$cookiename}=$_COOKIE[$cookiename];
    }
    else
     {
      ${$cookiename}=http_gpvar($cookiename);
      cookieHeader($cookiename,http_gpvar($cookiename),90); 
     }
    return ${$cookiename};
   } else
   {
    if (isset($_COOKIE[$cookiename]))
     return $_COOKIE[$cookiename];
    else
     return false;
   }
 }

function cookieHeader($name, $value, $expDays=0, $expHours=0, $expMins=0, $expSecs=0, $manualExp=0)
 {
 $exp = ($expDays * 86400) + ($expHours * 3600) + ($expMins * 60) + $expSecs;

 if (ereg("MSIE", getenv("HTTP_USER_AGENT"))) {
    $time = mktime()+$exp;
     $date = gmdate("D, d-M-Y H:i:s \G\M\T", ($time));
     header("Set-Cookie: $name=$value; expires=$date; path=/;");
 } else {
    setcookie($name,$value,time()+$exp,"/");
 }
 
 return true;
}

  # E-mail checker script
  function check_email($mail)
   {
    if(!ereg("^([0-9,a-z,A-Z]+)([.,_,-]([0-9,a-z,A-Z]+))*[@]([0-9,a-z,A-Z]+)([.,_,-]([0-9,a-z,A-Z]+))*[.]([0-9,a-z,A-Z]){2}([0-9,a-z,A-Z])?$",$mail))
     return false; // not valid mail addr
    else
     return true;  // valid addr
   }

?>
