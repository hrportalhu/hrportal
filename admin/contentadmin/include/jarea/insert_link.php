<?php
session_start();

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Insert Link</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	 <link rel="STYLESHEET" type="text/css" href="jarea.css" />
<script language="JavaScript" type="text/javascript">
<!--
var ua = navigator.userAgent.toLowerCase();
var isIE = ((ua.indexOf("msie") != -1) && (ua.indexOf("opera") == -1) && (ua.indexOf("webtv") == -1));
var ieVersion = parseFloat( ua.substring( ua.indexOf('msie ') + 5 ) );
var isGecko = (ua.indexOf("gecko") != -1);
var isSafari = (ua.indexOf("safari") != -1);
var safariVersion = parseFloat(ua.substring(ua.lastIndexOf("safari/") + 7));
var isKonqueror = (ua.indexOf("konqueror") != -1);
var isOpera = (ua.indexOf("opera") != -1);
var isNetscape = (ua.indexOf("netscape") != -1);

function getQueryVariable(variable) {
	try {
		var query = window.location.search.substring(1);
		var vars = query.split("&");
		for (var i=0; i < vars.length; i++) {
			var pair = vars[i].split("=");
			if (pair[0] == variable) {
				return unescape(pair[1]);
			}
		}
		alert('Query Variable ' + variable + ' not found');
	} catch (e) {
		alert(e);
	}
}


var formname='<?=$_GET['formname'];?>';
var control='<?=$_GET['control'];?>';



function AddLink() {
	try {
		var oForm = document.linkForm;
		
		//validate form
		if (oForm.url.value == '') {
			alert('Please enter a url.');
			return false;
		}
		if (oForm.linkText.value == '') {
			alert('Please enter link text.');
			return false;
		}
	
		var html = '<a style="font-weight:bold;" href="' + document.linkForm.url.value + '" target="' + document.linkForm.linkTarget.options[document.linkForm.linkTarget.selectedIndex].value + '">' + document.linkForm.linkText.value + '</a>';
		
		var newhtml =  html ;
			//alert("aaaa"+newhtml);
		window.opener.InsertElements(newhtml,'8',formname,control);

		window.close();
		return true;
	} catch (e) {
		alert(e);
	}
}

function setLinkText(linkText) {
	//set link text value in insert link dialog
	try {
		document.linkForm.linkText.value = linkText;
	} catch (e) {
		//may take some time to create dialog window.
		//Keep looping until able to set.
		//alert(e);
		setInterval("setLinkText('" + linkText + "')", 100);
	}
}
//-->
</script>
</head>

<body style="">

<form name="linkForm" onSubmit="return AddLink();">
<table cellpadding="4" cellspacing="0" border="0">
	<tr><td colspan="2"><span style="font-style: italic;  font-size: x-small;"><b>&ouml;tlet:</b> Ha e-mail linket szeretn&eacute;l besz&uacute;rni,akkor kezd az URL-edet "mailto:"-val</span></td></tr>
	<tr>
		<td align="right">URL:</td>
		<td><input name="url" type="text" id="url" size="30" value='http://'></td>
	</tr>
	<tr>
		<td align="right">Ez legyen a kattintható szöveg:</td>
		<td><input name="linkText" type="text" id="linkText" size="30" value="<?=$_REQUEST['seltext'];?>"></td>
	</tr>
	<tr>
		<td align="right">C&eacute;l:</td>
		<td align="left">
			<select name="linkTarget" id="linkTarget">
				<option value="_blank" selected>_blank</option>
				<option value="_parent">_parent</option>
				<option value="_self" >_self</option>
				<option value="_top">_top</option>
			</select>
		</td>
	</tr>
	<tr>
		<td colspan="3" align="center">
			<input type="submit" value="Link besz&uacute;r&aacute;sa" />
			<input type="button" value="M&eacute;gse" onClick="window.close();" />
		</td>
	</tr>
</table>

</form>

</body>
</html>
