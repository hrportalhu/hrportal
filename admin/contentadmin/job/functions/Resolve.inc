<?php

// definitions used for the different index parts 
// jobs, resume, content
define("VERITY_INDEX_JOBS",1);       
define("VERITY_INDEX_RESUME",2); 
define("VERITY_INDEX_CONTENT",3);


// ID-Types resolved via the Resolve funtion 
define("PROFESSION",             "1");
define("REGION",                 "2");
define("FIELDS",                 "3");
define("CITY",                   "4");
define("PROFESSION_DESCRIPTION", "5");

// Languages and column names
define("CHINESE",     "1"); // no own language column
$column[PROFESSION][CHINESE]                   ="berufe_neu.bez_eng";
$column[REGION][CHINESE]                       ="region.region_eng";
$column[FIELDS][CHINESE]                       ="felder.wert_eng";
$column[PROFESSION_DESCRIPTION][CHINESE]       ="berufe_neu.desc_eng";

define("GERMAN",      "2");
$column[PROFESSION][GERMAN]                    ="berufe_neu.bez_ger";
$column[REGION][GERMAN]                        ="region.region";
$column[FIELDS][GERMAN]                        ="felder.wert";
$column[PROFESSION_DESCRIPTION][GERMAN]        ="berufe_neu.desc_ger";      

define("DANISH",      "3"); // no own language column
$column[PROFESSION][DANISH]                    ="berufe_neu.bez_dk";
$column[REGION][DANISH]                        ="region.region_dk";
$column[FIELDS][DANISH]                        ="felder.wert_dk";
$column[PROFESSION_DESCRIPTION][DANISH]        ="berufe_neu.desc_dk";

define("ENGLISH",     "4");
$column[PROFESSION][ENGLISH]                   ="berufe_neu.bez_eng";
$column[REGION][ENGLISH]                       ="region.region_eng";
$column[FIELDS][ENGLISH]                       ="felder.wert_eng";
$column[PROFESSION_DESCRIPTION][ENGLISH]       ="berufe_neu.desc_eng";			      

define("FINNISH",     "5"); // no own language column
$column[PROFESSION][FINNISH]                   ="berufe_neu.bez_eng";
$column[REGION][FINNISH]                       ="region.region_eng";
$column[FIELDS][FINNISH]                       ="felder.wert_eng";
$column[PROFESSION_DESCRIPTION][FINNISH]       ="berufe_neu.desc_eng";

define("FRENCH",      "6");
$column[PROFESSION][FRENCH]                    ="berufe_neu.bez_fra";
$column[REGION][FRENCH]	                       ="region.region_fra";
$column[FIELDS][FRENCH]	                       ="felder.wert_fra";
$column[PROFESSION_DESCRIPTION][FRENCH]        ="berufe_neu.desc_fra";			      

define("GREEK",       "7"); // no own language column
$column[PROFESSION][GREEK]                     ="berufe_neu.bez_eng";
$column[REGION][GREEK]                         ="region.region_eng";
$column[FIELDS][GREEK]                         ="felder.wert_eng";
$column[PROFESSION_DESCRIPTION][GREEK]         ="berufe_neu.desc_eng";

define("DUTCH",       "8");
$column[PROFESSION][DUTCH]                     ="berufe_neu.bez_nl";
$column[REGION][DUTCH]                         ="region.region_nl";
$column[FIELDS][DUTCH]                         ="felder.wert_nl";
$column[PROFESSION_DESCRIPTION][DUTCH]         ="berufe_neu.desc_nl";			      

define("ITALIAN",     "9");
$column[PROFESSION][ITALIAN]                   ="berufe_neu.bez_ita";
$column[REGION][ITALIAN]                       ="region.region_ita";
$column[FIELDS][ITALIAN]                       ="felder.wert_ita";
$column[PROFESSION_DESCRIPTION][ITALIAN]       ="berufe_neu.desc_ita";			      

define("JAPANESE",   "10"); // no own language column
$column[PROFESSION][JAPANESE]                  ="berufe_neu.bez_eng";
$column[REGION][JAPANESE]                      ="region.region_eng";
$column[FIELDS][JAPANESE]                      ="felder.wert_eng";
$column[PROFESSION_DESCRIPTION][JAPANESE]      ="berufe_neu.desc_eng";

define("POLISH",     "11");
$column[PROFESSION][POLISH]                    ="berufe_neu.bez_pol";
$column[REGION][POLISH]	                       ="region.region_pol";
$column[FIELDS][POLISH]	                       ="felder.wert_pol";
$column[PROFESSION_DESCRIPTION][POLISH]        ="berufe_neu.desc_pol";			      

define("PORTUGUESE", "12"); // no own language column
$column[PROFESSION][PORTUGUESE]                ="berufe_neu.bez_eng";
$column[REGION][PORTUGUESE]                    ="region.region_eng";
$column[FIELDS][PORTUGUESE]                    ="felder.wert_eng";
$column[PROFESSION_DESCRIPTION][PORTUGUESE]    ="berufe_neu.desc_eng";

define("RUSSIAN",    "13"); // no own language column
$column[PROFESSION][RUSSIAN]                   ="berufe_neu.bez_eng";
$column[REGION][RUSSIAN]                       ="region.region_eng";
$column[FIELDS][RUSSIAN]                       ="felder.wert_eng";
$column[PROFESSION_DESCRIPTION][RUSSIAN]       ="berufe_neu.desc_eng";

define("SWEDISH",    "14");
$column[PROFESSION][SWEDISH]                   ="berufe_neu.bez_se";
$column[REGION][SWEDISH]                       ="region.region_se";
$column[FIELDS][SWEDISH]                       ="felder.wert_se";
$column[PROFESSION_DESCRIPTION][SWEDISH]       ="berufe_neu.desc_se";			      

define("OTHER",      "15"); // no own language column
$column[PROFESSION][OTHER]                     ="berufe_neu.bez_eng";
$column[REGION][OTHER]                         ="region.region_eng";
$column[FIELDS][OTHER]                         ="felder.wert_eng";
$column[PROFESSION_DESCRIPTION][OTHER]         ="berufe_neu.desc_eng";

define("SPANISH",    "16");
$column[PROFESSION][SPANISH]                   ="berufe_neu.bez_esp";
$column[REGION][SPANISH]                       ="region.region_esp";
$column[FIELDS][SPANISH]                       ="felder.wert_esp";
$column[PROFESSION_DESCRIPTION][SPANISH]       ="berufe_neu.desc_esp";			      

define("TURKISH",    "17"); // no own language column
$column[PROFESSION][TURKISH]                   ="berufe_neu.bez_eng";
$column[REGION][TURKISH]                       ="region.region_eng";
$column[FIELDS][TURKISH]                       ="felder.wert_eng";
$column[PROFESSION_DESCRIPTION][TURKISH]       ="berufe_neu.desc_eng";

define("NORWEGIAN",  "18");
$column[PROFESSION][NORWEGIAN]                 ="berufe_neu.bez_no";
$column[REGION][NORWEGIAN]                     ="region.region_no";
$column[FIELDS][NORWEGIAN]                     ="felder.wert_no";
$column[PROFESSION_DESCRIPTION][CHINESE]       ="berufe_neu.desc_eng";			      

define("HUNGARIAN",  "19");
$column[PROFESSION][HUNGARIAN]                 ="berufe_neu.bez_hu";
$column[REGION][HUNGARIAN]                     ="region.region_hu"; 
$column[FIELDS][HUNGARIAN]                     ="felder.wert_hu";   
$column[PROFESSION_DESCRIPTION][HUNGARIAN]     ="berufe_neu.desc_hu";

define("CROATIAN",   "20"); // no own language column
$column[PROFESSION][CROATIAN]                  ="berufe_neu.bez_eng";
$column[REGION][CROATIAN]                      ="region.region_eng";
$column[FIELDS][CROATIAN]                      ="felder.wert_eng";
$column[PROFESSION_DESCRIPTION][CROATIAN]      ="berufe_neu.desc_eng";

define("SERBIAN",    "21"); // no own language column
$column[PROFESSION][SERBIAN]                   ="berufe_neu.bez_eng";
$column[REGION][SERBIAN]                       ="region.region_eng";
$column[FIELDS][SERBIAN]                       ="felder.wert_eng";
$column[PROFESSION_DESCRIPTION][SERBIAN]       ="berufe_neu.desc_eng";

define("CZECH",      "22");
$column[PROFESSION][CZECH]                     ="berufe_neu.bez_cze";
$column[REGION][CZECH]                         ="region.region_cze";
$column[FIELDS][CZECH]                         ="felder.wert_cze";
$column[PROFESSION_DESCRIPTION][CZECH]         ="berufe_neu.desc_cze";			      

define("BULGARIAN",  "23"); // no own language column
$column[PROFESSION][BULGARIAN]                 ="berufe_neu.bez_eng";
$column[REGION][BULGARIAN]                     ="region.region_eng";
$column[FIELDS][BULGARIAN]                     ="felder.wert_eng";
$column[PROFESSION_DESCRIPTION][BULGARIAN]     ="berufe_neu.desc_eng";

define("INDIAN",     "24"); // no own language column
$column[PROFESSION][INDIAN]                    ="berufe_neu.bez_eng";
$column[REGION][INDIAN]                        ="region.region_eng";
$column[FIELDS][INDIAN]                        ="felder.wert_eng";
$column[PROFESSION_DESCRIPTION][INDIAN]        ="berufe_neu.desc_eng";

define("THAI",       "25"); // no own language column
$column[PROFESSION][THAI]                      ="berufe_neu.bez_th";
$column[REGION][THAI]                          ="region.region_th";
$column[FIELDS][THAI]                          ="felder.wert_th";
$column[PROFESSION_DESCRIPTION][THAI]          ="berufe_neu.desc_th";

/** 
# function Resolve(..)
# Resolves different types of IDs
# Default language is german
**/
function Resolve($IDType, $ID, $Language=GERMAN) {
  global $DEBUG;
  // example Resolve(REGION,220,GERMAN) => Deutschland
  //         Resolve(REGION,220,2)      => Deutschland
  //         Resolve("anrede",2,2)      => Frau

  global $column;
  static $resolve_db;

  if (!is_object($resolve_db)) {     // Create a JOBS_DB object 
    $resolve_db = new JOBS_DB_RO;      // if it hasn't been done yet
  } 

  if ($DEBUG>2) $resolve_db->Debug=1;
    
  switch ($IDType) { 
  case PROFESSION: case PROFESSION_DESCRIPTION:
    $resolve_db->query("SELECT ".$column[$IDType][$Language]." as value 
			 FROM berufe_neu 
			 WHERE beruf_id='$ID'");
    if ($resolve_db->num_rows() > 0) {
      $resolve_db->next_record();
      $Result = $resolve_db->f("value");
    } else {
      $Result = $ID;
    }
    break;
  case REGION:  
    $resolve_db->query("SELECT ".$column[REGION][$Language]." as value
			 FROM region 
			 WHERE region_id='$ID'");
    if ($resolve_db->num_rows() > 0) {
      $resolve_db->next_record();
      $Result = $resolve_db->f("value");
    } else {
      $Result = $ID;
    }
    break;
  case CITY:
    $resolve_db->query("SELECT distinct city_name AS value
		          FROM map_city 
			 WHERE city_id='$ID'");
    if ($resolve_db->num_rows() > 0) {
      $resolve_db->next_record();
      $Result = $resolve_db->f("value");
    } else {
      $Result = $ID;
    }
    break;
  default:
    $resolve_db->query("SELECT ".$column[FIELDS][$Language]." as value
			 FROM felder 
			 WHERE feld_id='$ID' and name='$IDType'");

    if ($resolve_db->num_rows() > 0) {
      $resolve_db->next_record();
      $Result = $resolve_db->f("value");

    } else {
      $Result = $ID;

      if ($IDType=='ke_Jahre' && $ID==1) { // old value for nothing, now it MUST be zero,
	$Result = 0;                       // else '1' is shown instead of '--------' !!
      }
    }
    break;
  }
#print "<BR><FONT color=\"ff0000\">Used Link-ID: $resolve_db->Link_ID</FONT><BR>";
  return $Result;
}
# end function Resolve (..)

/**
* @param $name name in jobs.felder
* @param $Language
* @param $main_id (optional)   get subregions or subprofessions
* @param $description (option) include description of profession
* @param $type (optional)   "RESUME", "AD", "SKILLS", "SKILLS_REQ"         
* @return array ($key=>$value)
* @example getResolvedList(REGION,GERMAN,220)   => german regions
*          getResolvedList(PROFESSION,ENGLISH)  => main professions
*          getResolvedList("appkind",FRENCH)    => type of work
**/
function getResolvedList($name,$Language=0,$main_id=0,$description=false, $type="")
{
    global  $column;
    static  $resolve_db; 
    if (!is_object($resolve_db)) 
    {
        $resolve_db = new JOBS_DB_RO;
    }

    switch($name)
    {
        case PROFESSION:
            if ($main_id>0)
            {
                $resolve_db->query("SELECT ".$column[PROFESSION][$Language]." as value,
                                           berufe_baum.beruf_id AS id
                                      FROM berufe_baum left join berufe_neu USING (beruf_id)
                                     WHERE berufe_baum.main_id='$main_id'");

                while ($resolve_db->next_record())
                {
                    $array[]=array("value"       => $resolve_db->f("id"),
                                   "text"        => $resolve_db->f("value"));
                }
            }
            else
            {
                $resolve_db->query("SELECT DISTINCT berufe_baum.main_id AS id 
                                      FROM berufe_baum,berufe_neu 
                                     WHERE berufe_neu.beruf_id=berufe_baum.beruf_id  
                                       AND main_id != 51");          
        
                while ($resolve_db->next_record())
                {
                    $array[]=array("value"       => $resolve_db->f("id"),
                                   "text"        => Resolve(PROFESSION,$resolve_db->f("id"),$Language),
                                   "description" => Resolve(PROFESSION_DESCRIPTION,$resolve_db->f("id"),$Language));
                }
            }
            break;
        case REGION:
            if ($main_id>0)
            { 
                $resolve_db->query("SELECT ".$column[REGION][$Language]." as value,
                                           region_baum.region_id as id
                                      FROM region_baum, region 
                                     WHERE region_baum.region_id=region.region_id
                                       AND region_baum.main_id='$main_id'
                                       AND region_baum.active!='0'");
            }
            else
            {
                $resolve_db->query("SELECT distinct ".$column[REGION][$Language]." as value,
                                           region_baum.region_id as id
                                      FROM region_baum LEFT JOIN region USING (region_id)
                                     WHERE main_id=102");
            }
            while ($resolve_db->next_record())
            {
                $array[]=array("value"       => $resolve_db->f("id"),
                               "text"        => $resolve_db->f("value"));
            } 
            break;
        default:

            $query="SELECT distinct ".$column[FIELDS][$Language]." as value,
                                       feld_id as id
                                  FROM felder
                                 WHERE name='$name' ";
            switch ($type)
            {
                case "RESUME":
                $include = "2,3,4,6,8,9,11,14,16,18,19,22";
                $query.=" AND feld_id IN ($include) ";
                break;
            case "AD":
                $include = "2,3,4,6,8,9,11,12,14,16,18,19,22,25,31";
                $query.=" AND feld_id IN ($include) ";
                break;
            case "SKILLS_REQ":
                $exclude = "15";  # exclude "Others" in "required skills"
                $query.=" AND feld_id NOT IN ($exclude) ";
                break;
            }   

            $query.=" ORDER BY sorting,".$column[FIELDS][$Language];

            $resolve_db->query("$query");
            while ($resolve_db->next_record())
            {
                $array[]=array("value"       => $resolve_db->f("id"),
                               "text"        => $resolve_db->f("value"));

            } 
            break;
    }
    return($array);
}

?>
