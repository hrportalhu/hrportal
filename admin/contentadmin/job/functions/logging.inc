<?php
if (!defined ("LOGGING_INC")) {
define ("LOGGING_INC", "1");


if (!isset($JOBSADVERTS)) {
    include_once "GlobalVars.inc";
}

$LOG_ERROR=1;

function fill_with_space ($text, $len) {
    if (strlen($text)>$len) {
	$text=substr($text,0,$len);
	return $text;
    } else
    {
      $len=$len-strlen($text);
      for($i=1; $i<=$len;$i++){
	$text.=" ";
      }
      return $text;
    }
}
	

function db_error($msg,$script,$errtext,$errno) {
    global $DEBUG, $JOBSADVERTS, $PHP_SELF;

    if (strlen($msg)>255) $msg=substr($msg,0,255);

    $msg_complete=$errno." '".$script."' ".$errtext." ".$msg;
    $msg_complete=str_replace("\n"," ",$msg_complete);
    $s=sprintf("%s [dberror] %s\n", date("[D M d H:i:s Y]"), $msg_complete);
    error_log($s ,3, "$JOBSADVERTS/var/log/php-errors.log");
    if ($DEBUG) {
        print nl2br($s);
    }
}

function php_error($msg) {
    global $DEBUG, $JOBSADVERTS, $PHP_SELF;
    $script=$PHP_SELF;

    $msg_complete=$errno." '".$script."' ".$msg;
    $s=sprintf("%s [error] %s\n", date("[D M d H:i:s Y]"), $msg_complete);
    error_log($s ,3, "$JOBSADVERTS/var/log/php-errors.log");
    if ($DEBUG) {
        print nl2br($s);
    }
}


function php_warning($msg) {
    global $DEBUG, $JOBSADVERTS, $PHP_SELF;
    $script=$PHP_SELF;

    $msg_complete=$errno." '".$script."' ".$msg;
    $s=sprintf("%s [warning] %s\n", date("[D M d H:i:s Y]"), $msg_complete);
    error_log($s ,3, "$JOBSADVERTS/var/log/php-errors.log");
    if ($DEBUG) {
        print nl2br($s);
    }
}


function php_info($msg) {
    global $JOBSADVERTS, $PHP_SELF;
    $script=$PHP_SELF;

    $msg_complete=$errno." '".$script."' ".$msg;
    $s=sprintf("%s [info] %s\n", date("[D M d H:i:s Y]"), $msg_complete);
    error_log($s ,3, "$JOBSADVERTS/var/log/php-errors.log");
}

} # LOGGING_INC

?>
