<?php

if (!defined ("MCONNECT_FUNCTION")) {
define ("MCONNECT_FUNCTION", "1");

global $LOG_ERROR;

if (isset($GLOBALS["JOBSDB_LOG"]) && $GLOBALS["JOBSDB_LOG"]==1) {
	require("functions/logging.inc");
}

function dbo_error_handler ($errno, $errstr, $errfile, $errline) {
    if ($errno & (E_WARNING | E_ERROR))
    print ("\n<!-- DB ERROR: $errstr in $errfile on line $errline -->\n");
}

function my_mysql_connect ($Host, $User, $Passwd, $DB, $Port="", $IgnoreError=0) {
	global $PARTNER;
	global $LANGUAGE_DEFAULT;
	global $LANG_PAGE_ID;
	global $PHP_SELF;

###	include "shared/style.inc";

	set_error_handler ("dbo_error_handler");

	if ($Port != "")
		$CONNECTION=mysql_connect("$Host:$Port", $User, $Passwd);
	else
		$CONNECTION=mysql_connect($Host, $User, $Passwd);

        restore_error_handler ();

	if ($CONNECTION) {
		if (! mysql_select_db ($DB, $CONNECTION)) {
			$CONNECTION="";
			$error = mysql_error ($CONNECTION);
			$errno = 1046;
			$msg = "Database error $DB";
		}
	} else {
		$error = "Database error: connect($Host, $User, \$Password) failed";
		$errno = 1044;
		$msg = "Connection error $DB";
	}

	if(!$CONNECTION) {
		####
		if ($GLOBALS["JOBSDB_LOG"]==1) {
			db_error($msg,$PHP_SELF,$error,$errno);
		}
		####
		if ($IgnoreError) {
			print "<!--  Error: $error -->";
			return "";
		}

		if($PARTNER) {
			$Situation="No Database Connection: $error";
			include "cooperation/partner/errormessage.inc";
		} else {
		  ###include "class/language.class";  // correct position ?
			### $LANG_PAGE_ID = 1022;
			/*** $lang = new Language($LANG_PAGE_ID, $LANGUAGE_DEFAULT);
			$text = $lang->get_text ("not_available");

			if (!$text) {
			  $Hinweis="\n<ul><br><br><font".$FONT["face_a"].$FONT["size_m"].">
			  <b>Our database is currently not available, please try again <br>
			  in a few moments.<br>
			  <br> Please accept our apologies and thank you for your
			  patience.<br><br>HR Portal Team</b>
			  <br>(<a href=mailto:info@hrportal.hu>info@hrportal.hu</a>)\n
			  <!--  Error: $error -->\n</font></ul>\n";

			} else {
			  $Hinweis="\n<ul><br><br><font".$FONT["face_a"].$FONT["size_m"].">".$text.
			  "\n<!--  Error: $error -->".
			  "\n</font></ul>\n";
			}***/

			include "index.inc";
		}

		exit;
	}

	return $CONNECTION;
}
}

?>
