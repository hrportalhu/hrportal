<?php
 # This function returns a HTTP POST, GET variable
 function http_gpvar($var)
  {
   $return=false;
   @$return=urldecode($HTTP_GET_VARS["$var"]);
   if (!$return)
    @$return=urldecode($_POST["$var"]);
   return $return;
  }   
     
 function http_get_var($var_name)
  {
   global $HTTP_GET_VARS;
   return @$HTTP_GET_VARS[$var_name];
  }

 function http_post_var($var_name)
  {
   global $_POST;  
   return @$_POST[$var_name];
  }
  
 function http_file_var($var_name)
  {
   return @$_FILES[$var_name];
  }  
  
function fine_date($date,$onlydate=false)
 {
  if ($date=="0000-00-00 00:00:00")
   return "- nincs adat -";
   
  $myrow=getdate(strtotime($date));
  $dat=$myrow['year'].".";
  switch($myrow['mon'])
   {
    case 1: $dat.=' janu�r '; break;
    case 2: $dat.=' febru�r '; break;
    case 3: $dat.=' m�rcius '; break;
    case 4: $dat.=' �prilis '; break;
    case 5: $dat.=' m�jus '; break;
    case 6: $dat.=' j�nius '; break;
    case 7: $dat.=' j�lius '; break;
    case 8: $dat.=' augusztus '; break;
    case 9: $dat.=' szeptember '; break;
    case 10: $dat.=' okt�ber '; break;
    case 11: $dat.=' november '; break;
    case 12: $dat.=' december '; break;
   }     
  $dat.=$myrow['mday'].".";
  if ($onlydate==false)
   {
    if ($myrow['hours']!=0 || $myrow['minutes']!=0 || $myrow['seconds']!=0)
     $dat.=" ".(strlen($myrow['hours'])==1?"0".$myrow['hours']:$myrow['hours']).":".(strlen($myrow['minutes'])==1?"0".$myrow['minutes']:$myrow['minutes']).":".(strlen($myrow['seconds'])==1?"0".$myrow['seconds']:$myrow['seconds']);
   }
  return($dat);
 }  
 
function fine_date_digit($date,$onlydate=false)
 {
  if ($date=="0000-00-00 00:00:00")
   return "- nincs adat -";
   
  $myrow=getdate(strtotime($date));
  
  if ($myrow['mon']<10) $myrow['mon']="0".$myrow['mon'];
  if ($myrow['mday']<10) $myrow['mday']="0".$myrow['mday'];
  
  $dat=$myrow['year'].". ".$myrow['mon'].". ".$myrow['mday'].".";
  if ($onlydate==false)
   {
    if ($myrow['hours']!=0 || $myrow['minutes']!=0 || $myrow['seconds']!=0)
     $dat.=" ".(strlen($myrow['hours'])==1?"0".$myrow['hours']:$myrow['hours']).":".(strlen($myrow['minutes'])==1?"0".$myrow['minutes']:$myrow['minutes']).":".(strlen($myrow['seconds'])==1?"0".$myrow['seconds']:$myrow['seconds']);
   }
  return($dat);
 }   
 

// this function loads/saves data from/into a cookie.
function manage_cookie($cookiename)
 {
  if (http_gpvar($cookiename))
   {
    if (isset($_COOKIE[$cookiename]))
    { 
     if (http_gpvar($cookiename)!=$_COOKIE[$cookiename])
      {
       cookieHeader($cookiename,http_gpvar($cookiename),90);
       ${$cookiename}=http_gpvar($cookiename);
      } else
         ${$cookiename}=$_COOKIE[$cookiename];
    }
    else
     {
      ${$cookiename}=http_gpvar($cookiename);
      cookieHeader($cookiename,http_gpvar($cookiename),90); 
     }
    return ${$cookiename};
   } else
   {
    if (isset($_COOKIE[$cookiename]))
     return $_COOKIE[$cookiename];
    else
     return false;
   }
 }

function cookieHeader($name, $value, $expDays=0, $expHours=0, $expMins=0, $expSecs=0, $manualExp=0)
 {
 $exp = ($expDays * 86400) + ($expHours * 3600) + ($expMins * 60) + $expSecs;

 if (ereg("MSIE", getenv("HTTP_USER_AGENT"))) {
    $time = mktime()+$exp;
     $date = gmdate("D, d-M-Y H:i:s \G\M\T", ($time));
     header("Set-Cookie: $name=$value; expires=$date; path=/;");
 } else {
    setcookie($name,$value,time()+$exp,"/");
 }
 
 return true;
}

# $host includes host and path and filename
   # ex: "myserver.com/this/is/path/to/file.php"
# $query is the POST query data
   # ex: "a=thisstring&number=46&string=thatstring
# $others is any extra headers you want to send
   # ex: "Accept-Encoding: compress, gzip\r\n"
function post($host,$query,$others='',$port=80){
   $path=explode('/',$host);
   $host=$path[0];
   unset($path[0]);
   $path='/'.(implode('/',$path));
   $post="POST $path HTTP/1.1\r\nHost: $host\r\nContent-type: application/x-www-form-urlencoded\r\n${others}User-Agent: Mozilla 4.0\r\nContent-length: ".strlen($query)."\r\nConnection: close\r\n\r\n$query";
  
   $h=fsockopen($host,$port);
   fwrite($h,$post);
   for($a=0,$r='';!$a;){
       $b=fread($h,8192);
       $r.=$b;
       $a=(($b=='')?1:0);
   }
   fclose($h);
   return $r;
}

?>