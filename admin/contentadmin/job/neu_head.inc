<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
	<title>Templates</title>
    <style type="text/css">
     td, .td {
      font-family: Arial, Verdana;
      font-size: 11pt;
      color: black;
      background-color: #DDDDDD;
     }

     a {
      color: #990000;
      text-decoration: none;
     }
     
     a:hover {
      text-decoration: underline;
     }
     
     .head {
      background-color: #424F78;
     }
    </style>
</head>

<body>