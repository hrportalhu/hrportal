<?php

$JOBSHost="localhost";
$JOBSUser="hrportal";
$JOBSPasswd="qawUcAd8";
$JOBSDB="hrportal_new";

$VISHost="localhost";
$VISUser="hrportal";
$VISPasswd="qawUcAd8";
$VISDB="hrportal_new";

$HTMLHost="localhost";
$HTMLUser="hrportal";
$HTMLPasswd="qawUcAd8";
$HTMLDB="hrportal_new";

require_once ("class/db.class");



# overwrite Vars from conf.HOSTNAME as long as we get
# errors with connection Links
# if ($HOSTNAME=="hades") $VISUser="VISUser";

if (!isset($RESUMEDBHost)){
   if (isset($DEBUG) && $DEBUG>0) print "\n<br><b>Warning (phplib/local.inc)</b>: \$RESUMEDBHost is undefined. Set \$RESUMEDBHost=\$JOBSHost";
   $RESUMEDBHost=$JOBSHost;
}
if (!isset($RESUMEDB)){
   if (isset($DEBUG) && $DEBUG>0) print "\n<br><b>Warning (phplib/local.inc)</b>: \$RESUMEDB is undefined. Set \$RESUMEDB=\$JOBSDB";
   $RESUMEDB=$JOBSDB;
}
if (!isset($RESUMEDBUser)){
   if (isset($DEBUG) && $DEBUG>0) print "\n<br><b>Warning (phplib/local.inc)</b>: \$RESUMEDBUser is undefined. Set \$RESUMEDBUser=\$JOBSUser";
   $RESUMEDBUser=$JOBSUser;
}
if (!isset($RESUMEDBPasswd)){
   if (isset($DEBUG) && $DEBUG>0) print "\n<br><b>Warning (phplib/local.inc)</b>: \$RESUMEDBPasswd is undefined. Set \$RESUMEDBPasswd=\$JOBSPasswd";
   $RESUMEDBPasswd=$JOBSPasswd;
}


if (isset ($ALIGNMENTHost)) {
class ALIGNMENT_DB extends phplibDB {
  function ALIGNMENT_DB() {
    global $ALIGNMENTHost, $ALIGNMENTDB, $ALIGNMENTUser, $ALIGNMENTPasswd;

    $this->Host      = $ALIGNMENTHost;
    $this->Database  = $ALIGNMENTDB;
    $this->User      = $ALIGNMENTUser;
    $this->Password  = $ALIGNMENTPasswd;
  }
}
}

if (isset ($JOBSHost)) {
class JOBS_DB extends phplibDB {
  var $IsJobsDB;
  
  function JOBS_DB() {
    global $JOBSHost, $JOBSDB,$JOBSUser, $JOBSPasswd; 

    $this->Host      = $JOBSHost;
    $this->Database  = $JOBSDB;
    $this->User      = $JOBSUser;
    $this->Password  = $JOBSPasswd;
    $this->IsJobsDB  = 1;
  }
}
}

if (isset ($JOBSROHost)) {
class JOBS_DB_RO extends phplibDB {
  function JOBS_DB_RO() {
    global $JOBSROHost, $JOBSRODB,$JOBSROUser, $JOBSROPasswd;

    $this->Host      = $JOBSROHost;
    $this->Database  = $JOBSRODB;
    $this->User      = $JOBSROUser;
    $this->Password  = $JOBSROPasswd;
  }
}
}

if (isset ($LANGUAGEDBHost)) {
class LANG_DB extends phplibDB {
  var $classname;

  function LANG_DB() {
    global $LANGUAGEDBHost, $LANGUAGEDB, $LANGUAGEDBUser, $LANGUAGEDBPasswd;

    $this->Host      = $LANGUAGEDBHost;
    $this->Database  = $LANGUAGEDB;
    $this->User      = $LANGUAGEDBUser;
    $this->Password  = $LANGUAGEDBPasswd;
    $this->classname = "LANG_DB";
  }
}
}


if (isset ($VISHost)) {
class VIS_DB extends phplibDB {
  function VIS_DB() {
    global $VISHost, $VISDB, $VISUser, $VISPasswd;

    $this->Host      = $VISHost;
    $this->Database  = $VISDB;
    $this->User      = $VISUser;
    $this->Password  = $VISPasswd;
  }
}
}

if (isset ($HTMLHost)) {
class HTML_DB extends phplibDB {
    function HTML_DB() {
      global $HTMLHost, $HTMLDB, $HTMLUser, $HTMLPasswd;

      $this->Host      = $HTMLHost;
      $this->Database  = $HTMLDB;
      $this->User      = $HTMLUser;
      $this->Password  = $HTMLPasswd;
    }
}
}

if (isset ($RESUMEDBHost)) {
class RESUME_DB extends phplibDB {
  function RESUME_DB() {
    global $RESUMEDBHost, $RESUMEDB, $RESUMEDBUser, $RESUMEDBPasswd;

    $this->Host      = $RESUMEDBHost;
    $this->Database  = $RESUMEDB;
    $this->User      = $RESUMEDBUser;
    $this->Password  = $RESUMEDBPasswd;
  }
}
}

if (isset ($RESUMERODBHost)) {
class RESUME_DB_RO extends phplibDB {
  function RESUME_DB_RO() {
    global $RESUMERODBHost, $RESUMERODB, $RESUMERODBUser, $RESUMERODBPasswd;

    $this->Host      = $RESUMERODBHost;
    $this->Database  = $RESUMERODB;
    $this->User      = $RESUMERODBUser;
    $this->Password  = $RESUMERODBPasswd;
  }
}
}


if (isset ($MAILHost)) {
class MAIL_DB extends phplibDB {
  function MAIL_DB() {
    global $MAILHost, $MAILDB, $MAILUser, $MAILPasswd;

    $this->Host      = $MAILHost;
    $this->Database  = $MAILDB;
    $this->User      = $MAILUser;
    $this->Password  = $MAILPasswd;
  }     
}
}


if (isset ($BANNERDBHost)) {
class BANNER_DB extends phplibDB {
  var $classname;

  function BANNER_DB() {
    global $BANNERDBHost, $BANNERDB, $BANNERDBUser, $BANNERDBPasswd; 

    $this->Host      = $BANNERDBHost;
    $this->Database  = $BANNERDB;
    $this->User      = $BANNERDBUser;
    $this->Password  = $BANNERDBPasswd;
    $this->classname = "BANNER_DB";
  }   
}
}


if (isset ($BANNERADMINDBHost)) {
class BANNERADMIN_DB extends phplibDB {
  var $classname;

  function BANNERADMIN_DB() {
    global $BANNERADMINDBHost, $BANNERADMINDB, $BANNERADMINDBUser, $BANNERADMINDBPasswd; 

    $this->Host      = $BANNERADMINDBHost;
    $this->Database  = $BANNERADMINDB;
    $this->User      = $BANNERADMINDBUser;
    $this->Password  = $BANNERADMINDBPasswd;
    $this->classname = "BANNERADMIN_DB";
  }   
}
}


if (isset ($INCREMENT_Host)) {
class INCREMENT_DB extends phplibDB {
  function INCREMENT_DB() {
    global $INCREMENT_Host, $INCREMENT_DB, $INCREMENT_User, $INCREMENT_Passwd; 

    $this->Host      = $INCREMENT_Host;
    $this->Database  = $INCREMENT_DB;
    $this->User      = $INCREMENT_User;
    $this->Password  = $INCREMENT_Passwd;
  }   

  function getNextID($tablename) {
     return $this->nextid ($tablename);
  }
}
}

if (isset ($SESSIONHost)) {
class SESSION_DB extends phplibDB {
  function SESSION_DB() {
    global $SESSIONHost, $SESSIONDB, $SESSIONUser, $SESSIONPasswd;

    $this->Host      = $SESSIONHost;
    $this->Database  = $SESSIONDB;
    $this->User      = $SESSIONUser;
    $this->Password  = $SESSIONPasswd;
  }
}
}

if (isset ($SPOOLHost)) {
class SPOOL_DB extends phplibDB {
  function SPOOL_DB() {
    global $SPOOLHost, $SPOOLDB, $SPOOLUser, $SPOOLPasswd;

    $this->Host      = $SPOOLHost;
    $this->Database  = $SPOOLDB;
    $this->User      = $SPOOLUser;
    $this->Password  = $SPOOLPasswd;
  }
}
}

if (isset ($STATSHost)) {
class STATS_DB extends phplibDB {
  function STATS_DB() {
     global $STATSHost, $STATSDB, $STATSUser, $STATSPasswd;
 
     $this->Host      = $STATSHost;
     $this->Database  = $STATSDB;
     $this->User      = $STATSUser;
     $this->Password  = $STATSPasswd;
  }
}
}

class Jobs_CT_Sql extends CT_Sql {
  var $database_class;
  var $database_table;
  var $encoding_mode;

  function Jobs_CT_Sql() {
    $this->database_class = "SESSION_DB";          ## Which database to connect...
    $this->database_table = "active_sessions";  ## and find our session data in this table.
    $this->encoding_mode = "base64";            ## encodes special characters correctly
#    $this->encoding_mode = "slashes";          ## lets see whats in the session
  }
}


class Jobs_Session extends Session {
  var $classname;
  var $cookiename;
  var $magic;
  var $mode;
  var $fallback_mode;
  var $lifetime;
  var $gc_time;
  var $that_class;
  var $gc_probability;
  var $allowcache;

  function Jobs_session() {
global $SESSION_COOKIES;
    $this->classname      = "Jobs_Session";
    $this->cookiename     = "";              // defaults to classname
    $this->magic          = "BIG secret!";   // ID seed

if (isset ($SESSION_COOKIES))
    $this->mode           = "cookie";       // We propagate session IDs with cookies
else
    $this->mode           = "get";           // We propagate session IDs with GET

    $this->fallback_mode  = "get";
    $this->lifetime       = 30;              // 0 = do session cookies, else minutes
    $this->gc_time        = 30;              // Purge all session data older than 1440 minutes.
    $this->that_class     = "Jobs_CT_Sql";   // name of data storage container class
    $this->gc_probability = 0;               // turn off gc

    $this->allowcache     = "unlimited";     // "public", "private", or "no"
  }
}

// needs the definition of JOBS_DB
### include_once "functions/Resolve.inc";

?>
