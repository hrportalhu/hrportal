// PLANET STAR KFT, 2007.

var requestVan = 0;
var pla_ajaxObjects = new Array();
// Ez azert tomb, mert ugye elkepzelheto, hogy nem egy AJAX-os mezo lesz
// a kepernyon, es a tomb egy eleme fog azonositani egy AJAX-os kapcsolatot

function trimString(sInString) {
// Ez a fuggveny csak legyomlalja egy stringrol a felesleget
  sInString = sInString.replace( /^\s+/g, "" );
  return sInString.replace( /\s+$/g, "" );
}

function parseEmails(ajaxIndex){
    // Ez a fuggveny hivodik meg, ha visszajon az AJAX keres. Ugye a lenyeg eppen
    // az, hogy nem varunk az adatok visszateresere, hanem majd azok egyszercsak
    // megjonnek. Abban a pillanatban ez a fuggveny meg fog hivodni.
    var itemsToBeCreated = new Array();
    // Ebben a tombben lesz az XML tag hierarchia
    var myDiv = document.getElementById('filmList');
    myDiv.innerHTML = '';
    // Megkerestuk a DIV-et, ahova kiirjuk az adatokat, es kinullazzuk
    var myText = '';
    // Ebbe irjuk majd a vegeredmenyt, amit a myDiv-re irunk ki

    var items = pla_ajaxObjects[ajaxIndex].response.split(/<email>/g);
    // Itt most felteteleztuk, hogy XML-t kapunk vissza. Eldaraboljuk az <email>
    // tag menten
    pla_ajaxObjects[ajaxIndex] = false;
    // Ertelemszeruen kinullazzuk ezt a kapcsolatot, nincs mar ra szukseg
    // Egy-egy <email> tobb tagot is tartalmaz, csak azert mert ez igy a
    // legbonyolultabb
    // A kovetkezo ciklusokban lenyegeben manualisan kibontjuk az XML tag-eket
    // es bepakoljuk a tombbe
    for(var no=1;no<items.length;no++){
	var lines = items[no].split(/\n/g);
	itemsToBeCreated[no] = new Array();
	for(var no2=0;no2<lines.length;no2++){
	    var key = lines[no2].replace(/<([^>]+)>.*/g,'$1');
	    if(key)key = trimString(key);
	    var pattern = new RegExp("<\/?" + key + ">","g");
	    var value = lines[no2].replace(pattern,'');
	    value = trimString(value);
	    itemsToBeCreated[no][key] = value;
	}
	if(itemsToBeCreated[no]['id']){
	    myText = myText + "<a href=" + itemsToBeCreated[no]['id'] + ">" + itemsToBeCreated[no]['id']  + "</a> | "  + itemsToBeCreated[no]['mailAddress'] + " | " + itemsToBeCreated[no]['realname'] + "<BR />";
	}
    }
    myDiv.innerHTML = myText;
}

function getItems(){
    var ajaxIndex = pla_ajaxObjects.length;
    var mname = document.getElementById('mname').value;
    // azaz vesszuk a tombben levo elemek + 1-et, mivel JS 0-tol indexel
    pla_ajaxObjects[ajaxIndex] = new sack();
    // letrehoztuk az uj AJAX kapcsolat azonositot
    pla_ajaxObjects[ajaxIndex].requestFile = 'getemails.php?mname=' + mname;
    // A domain nevu valtozot ugye 3 sorral feljebb kerestuk ki
    pla_ajaxObjects[ajaxIndex].onCompletion = function(){ parseEmails(ajaxIndex); };
    // Megadjuk azt a fuggvenyt, ami a visszatero adatokat feldolgozza. Az ajaxIndex-
    // et at kell adni neki, hogy tudja, melyik AJAX kapcsolat szalrol van szo
    pla_ajaxObjects[ajaxIndex].runAJAX();
}

function getPartner(){
    if(requestVan == 0)
    {
    var ajaxIndex = pla_ajaxObjects.length;
    var pname = document.getElementById('pname').value;
    pla_ajaxObjects[ajaxIndex] = new sack();
    pla_ajaxObjects[ajaxIndex].requestFile = 'adm/getpartners.php?pname=' + pname;
    pla_ajaxObjects[ajaxIndex].onCompletion = function(){ parsePartners(ajaxIndex); };
    requestVan=1;
    pla_ajaxObjects[ajaxIndex].runAJAX();
    }else
    {
	//alert("Request van!");
    }
}

function gettipus(){
    if(requestVan == 0)
    {
    var ajaxIndex = pla_ajaxObjects.length;
    var tname = document.getElementById('tname').value;
    
    pla_ajaxObjects[ajaxIndex] = new sack();
    pla_ajaxObjects[ajaxIndex].requestFile = 'func/gettip.php?tname=' + tname;
    pla_ajaxObjects[ajaxIndex].onCompletion = function(){ parsetip(ajaxIndex); };
    requestVan=1;
    pla_ajaxObjects[ajaxIndex].runAJAX();
    }else
    {
	//alert("Request van!");
    }
}

function parsetip(ajaxIndex){
	
    var itemsToBeCreated = new Array();
    var myDiv = document.getElementById('altipus');
    myDiv.innerHTML = '';
    var myText = '<label for=JC2>Alt�pus</label><select name=JC2 width=100px>';
   	var items = pla_ajaxObjects[ajaxIndex].response.split(/<at>/g);
   	
    pla_ajaxObjects[ajaxIndex] = false;
    for(var no=1;no<items.length;no++)
    {
		var lines = items[no].split(/\n/g);
		itemsToBeCreated[no] = new Array();
		for(var no2=0;no2<lines.length;no2++)
		{
			var key = lines[no2].replace(/<([^>]+)>.*/g,'$1');
			if(key)key = trimString(key);
			var pattern = new RegExp("<\/?" + key + ">","g");
			var value = lines[no2].replace(pattern,'');
			value = trimString(value);
			itemsToBeCreated[no][key] = value;
			
		}
		myText = myText + "<option value=" + itemsToBeCreated[no]['id'] + ">"  + itemsToBeCreated[no]['neve']  + "</option>"   ;
		
		
	}
    myText=myText + "</select>";
    myDiv.innerHTML = myText;
    requestVan=0;
}

function getaddr(){
    if(requestVan == 0)
    {
    var ajaxIndex = pla_ajaxObjects.length;
    var tname = document.getElementById('irnum').value;
    
    pla_ajaxObjects[ajaxIndex] = new sack();
    pla_ajaxObjects[ajaxIndex].requestFile = 'func/getadress.php?tname=' + tname;
    pla_ajaxObjects[ajaxIndex].onCompletion = function(){ parseutc(ajaxIndex); };
    requestVan=1;
    pla_ajaxObjects[ajaxIndex].runAJAX();
    }else
    {
	//alert("Request van!");
    }
}

function parseutc(ajaxIndex){
	
    var itemsToBeCreated = new Array();
    var myDiv = document.getElementById('ucs');
    var myC = document.getElementById('telepname');
    myC.innerHTML = '';
    myDiv.innerHTML = '';
    var myCtext ='';
    var myText = '<label for=uts>Utc&aacute;k</label><select name=uts width=100px>';
   	var items = pla_ajaxObjects[ajaxIndex].response.split(/<at>/g);
   	
    pla_ajaxObjects[ajaxIndex] = false;
    for(var no=1;no<items.length;no++)
    {
		var lines = items[no].split(/\n/g);
		itemsToBeCreated[no] = new Array();
		for(var no2=0;no2<lines.length;no2++)
		{
			var key = lines[no2].replace(/<([^>]+)>.*/g,'$1');
			if(key)key = trimString(key);
			var pattern = new RegExp("<\/?" + key + ">","g");
			var value = lines[no2].replace(pattern,'');
			value = trimString(value);
			itemsToBeCreated[no][key] = value;
			
		}
		myText = myText + "<option value=" + itemsToBeCreated[no]['id'] + ">"  + itemsToBeCreated[no]['neve']  + "</option>"   ;
		myCtext="<label for='telepname'>Telep&uuml;l&eacute;s neve:</label><input class='textfield' type='text' name='telepname' value=" +itemsToBeCreated[no]['varos'] + "><br>";
		
	}
    myText=myText + "</select>";
    myDiv.innerHTML = myText;
    myC.innerHTML = myCtext;
    requestVan=0;
}



function parsePartners(ajaxIndex){
	
    var itemsToBeCreated = new Array();
    var myDiv = document.getElementById('tartalom');
    myDiv.innerHTML = '';
    var myText = '<TABLE class=itemList><thead><tr><th>R�vid neve</th><th>Teljes neve</th><th>Ad�sz�m</th><th>behegyzes</th><th>Telefon</th><th>M&#x0171;veletek</th></tr></thead>';
   	var items = pla_ajaxObjects[ajaxIndex].response.split(/<atad>/g);
    pla_ajaxObjects[ajaxIndex] = false;
    for(var no=1;no<items.length;no++)
    {
		var lines = items[no].split(/\n/g);
		itemsToBeCreated[no] = new Array();
		for(var no2=0;no2<lines.length;no2++)
		{
			var key = lines[no2].replace(/<([^>]+)>.*/g,'$1');
			if(key)key = trimString(key);
			var pattern = new RegExp("<\/?" + key + ">","g");
			var value = lines[no2].replace(pattern,'');
			value = trimString(value);
			itemsToBeCreated[no][key] = value;
		}
		if(itemsToBeCreated[no]['id'])
		{
	    	myText = myText + "<tr><td>" + itemsToBeCreated[no]['shortname'] + "</td><td>"  + itemsToBeCreated[no]['name']  + "</td><td>" + itemsToBeCreated[no]['adoszam']  + "</td><td>" + itemsToBeCreated[no]['bejegyzes']  + "</td><td>" + itemsToBeCreated[no]['telno']  + "</td>"  ;
		}
		myText=myText + "<td><input type='button' class='itemButton' name='edit' value='Szerkeszt' onClick='javascript:document.location=\"admin.php?page=datas&action=onepartner&id=" + itemsToBeCreated[no]['id'] +"\"'></td></tr>";
	}
    myText=myText + "</table>";
    myDiv.innerHTML = myText;
    requestVan=0;
}

var mname;
var user;
function chval(fname,user)
{
	if(requestVan == 0)
    {
    var ajaxIndex = pla_ajaxObjects.length;
     pla_ajaxObjects[ajaxIndex] = new sack();
    pla_ajaxObjects[ajaxIndex].requestFile = 'func/getchbox.php?fname=' + fname +'&usar=' + user;
    pla_ajaxObjects[ajaxIndex].onCompletion = function(){ parsechbox(ajaxIndex,fname); };
    requestVan=1;
    pla_ajaxObjects[ajaxIndex].runAJAX();
    }else
    {
	//alert("Request van!");
    }
}

function parsechbox(ajaxIndex,fname)
{
	var myDiv = document.getElementById(fname);
    myDiv.innerHTML = '';
    var myText = '';
   	var items = pla_ajaxObjects[ajaxIndex].response;
    pla_ajaxObjects[ajaxIndex] = false;
    myText=items;
    myDiv.innerHTML = myText;
    requestVan=0;
}
