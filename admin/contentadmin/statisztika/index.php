<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="content-type" content="text/html;charset=iso-8859-2" />
<meta name="author" content="szucsjoee@gmail.com"/>
<meta name="description" content="HRPortal statisztika" />
<meta name="keywords" content="pdfs" />	
<script type="text/javascript"  src="js/ajax.js">;</script>
<script type="text/javascript"  src="js/plajax.js">;</script>
<script type="text/javascript" src="js/prototype.js"></script>
<script type="text/javascript" src="js/fastinit.js"></script>
<script type="text/javascript" src="js/tablekit.js"></script>
<script type="text/javascript" src="js/fabtabulous.js"></script>
<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
<?

if (isset($_GET['limit']) && $_GET['limit']>0){
	$limit=$_GET['limit'];
}
else{
	$limit=50;
}

if (isset($_GET['stli']) && $_GET['stli']>0){
	$stli=$_GET['stli'];
}
else{
	$stli=1;
}
include_once dirname(__FILE__) . "/include/functions.php";
if(isset($_POST['startdate'])){
	$start=$_POST['startdate'];	
}
else{
	$start=date("Y-m",time())."-01";
}
if(isset($_POST['enddate'])){
	$end=$_POST['enddate']	;
}
else{
	$end=date("Y-m-d",time());
}
?>
<title>HRPort�l statisztikai rendszer</title>
</head>
<body>
<div class="content">
			<div class="header">

			<div class="logo">
				<h1><a href="#" title="">HR<span class="orange">Port�l</span></a><sup>cikkstatisztika</sup></h1>
			</div>
		</div>
		
	<div class="bar" style="padding:5px;">
		<div class="searchform" style="width:600px;float:left;">
			<form name="lekerd" action="index.php" method="post" enctype="multipart/form-data">
			<?
			mkdate("Kezdeti id�","startdate",$start,"","","lekerd");
			mkdate("Befejez�si id�","enddate",$end,"","","lekerd");
			?>
			<input type="submit" name="lekup" value="Keres�s">
			</form>
		</div>	
		<div class="spacer" style="width:100px;float:left;"></div>
		<div class="dosearch" style="width:200px;float:left;"></div>
	</div>
	<div class="search_field" style="height:20px;">
			<div class="search_form">
			<p>&nbsp;</p>
			</div>
	</div>
	
	<div class="elvalasz" style="clear:both;"></div>
	<div class="tartalom" id="tartalom" style="padding:10px;">
	
	<div class="left">
			<h2>Tal&aacute;lati <span class="orange"> Lista</span></h2>

			<div class="left_box" style='padding:10px;'>
				<p>
					<?
					$files=db_all("select 	a.id AS id,
											a.teaser AS teaser,
											a.contentwriter_login AS contentwriter_login,
											a.click_counter AS click_counter,
											a.date_last_modified  AS date_last_modified,
											l.link AS link,
											cw.shown_name AS cw_shown_name
										from 
											articles a
										LEFT JOIN
                              				contentwriters cw ON a.contentwriter_login=cw.login
			 							LEFT JOIN
											articles_link l ON (a.id=l.art_id) 				
										where 
											date_last_modified between '$start' and '$end' 
										and 
											is_approved='yes' 
										and 
											contentwriter_login!='fn' 
										and 
											contentwriter_login!='mfor' 
										and 
											contentwriter_login!='Prb' 
										and 
											contentwriter_login!='medi' 	
										order by 
											date_last_modified DESC");
					
					echo"<TABLE class='itemList sortable resizable'><thead><tr>";
					echo"<th>Cikkszerz&#x0151;</th><th>Cikkc&iacute;m</th><th>Kattint�si sz�m</th><th>Utols� m�dos�t�s</th><th>M&#x0171;velet</th></tr></thead>";						
					for($i=0;$i<count($files);$i++){
							echo"<tr>
								<td>
									".$files[$i]['cw_shown_name']."
								</td>
								<td>
									".substr($files[$i]['teaser'],0,30)."...
								</td>
								<td>";
								printf("%5s",   $files[$i]['click_counter']);
									//".$files[$i]['click_counter']."
								echo"</td>
								<td>
									".$files[$i]['date_last_modified']."
								</td>
								<td><a href='".$files[$i]['link']."'>Megtekint</a></td>	
							</tr>";
					}
					echo"</table>";
/*
					for($i=0;$i<count($files);$i++){
						if($i/2==floor($i/2)){
							$c="#fff";
						}
						else{
							$c="#ddd";	
						}
						if(substr($files[$i]['teaser'],0,30)!=""){
						echo"<div class='onerow' style='background:$c;display:block;height:25px;'>
								<div class='filen' style='float:left;width:250px;'>
									".$files[$i]['cw_shown_name']."
								</div>
								<div class='filen' style='float:left;width:240px;'>
									".substr($files[$i]['teaser'],0,30)."...
								</div>
								<div class='filen' style='float:left;width:45px;'>
									".$files[$i]['click_counter']."
								</div>
								<div class='filen' style='float:left;width:140px;'>
									".$files[$i]['date_last_modified']."
								</div>
									<div class='dels' style='float:left;width:60px;'><a href='".$files[$i]['link']."'>Megtekint</a></div>	
							</div>";
						}		
					}
*/
					?>
				
				</p>
			</div>
	</div>
	</div>
</div>
</body>
</html>
