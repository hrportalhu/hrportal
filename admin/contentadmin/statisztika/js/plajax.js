// PLANET STAR KFT, 2007.

var requestVan = 0;
var pla_ajaxObjects = new Array();
// Ez azert tomb, mert ugye elkepzelheto, hogy nem egy AJAX-os mezo lesz
// a kepernyon, es a tomb egy eleme fog azonositani egy AJAX-os kapcsolatot

function trimString(sInString) {
// Ez a fuggveny csak legyomlalja egy stringrol a felesleget
  sInString = sInString.replace( /^\s+/g, "" );
  return sInString.replace( /\s+$/g, "" );
}

function getdoc(){
	var ajaxIndex = pla_ajaxObjects.length;
	pla_ajaxObjects[ajaxIndex] = new sack();
	var myDiv = document.getElementById('tartalom');
	var fid =encodeURIComponent(document.getElementById("search").value);
	myDiv.innerHTML = "<img src=\"images/loading.gif\" alt=\"töltögetek\" />";
	pla_ajaxObjects[ajaxIndex].requestFile = 'ajax_getdoc.php?fid=' +  fid ;
	pla_ajaxObjects[ajaxIndex].onCompletion = function(){ displaydoc(ajaxIndex); };
	pla_ajaxObjects[ajaxIndex].runAJAX();
}

function displaydoc(ajaxIndex){
    var itemsToBeCreated = new Array();
    var myDiv = document.getElementById('tartalom');
    myDiv.innerHTML = pla_ajaxObjects[ajaxIndex].response;
    requestVan=0;
}

function del(article_id)
  {
   if (confirm("Biztos, hogy törli ?"))
    {
     location.replace("index.php?del="+article_id);
    }
  }
