<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>HRP Start</title>
    <style type="text/css">
     body, td, .td {
      font-family: Arial Narrow, Arial, Verdana;
      font-size: 16px;
      color: #8D9AB2; 
      font-weight: bold;
     }
     
	 .small {
	  font-family: Arial Narrow, Arial, Verdana;
      font-size: 12px;
      color: #8D9AB2; 
      font-weight: normal;
	 }
     
	 a {
      text-decoration: none;
      color: #82B1D3;
     }
     
     a:hover {
      text-decoration: none;
      color: #003076;
     }

    </style>
</head>

<body bgcolor="#FFFFFF" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<table width='100%' height='100%' cellpadding='0' cellspacing='0' border='0' align='center' valign='middle'>
  <tr>
   <td width='100%' height='100%' align='center' valign='middle'>

<table cellpadding='3' cellspacing='0' border='0' width="600">
 <tr>
  <td bgcolor='#FFFFFF' align='left' valign="bottom"><img src='hrstart.gif' border='0'></td><td bgcolor='#FFFFFF' align='right' valign="bottom"> Szeml�zett oldalak<br><img src="http://hrportal.hu/images/pixel.gif" width="60" height="4" border="0"></td>
 </tr>
 <tr>
  <td bgcolor='#EBEEF3' colspan="2">&nbsp;</td>
 </tr>  
  <tr>
  <td bgcolor='#EBEEF3' colspan="2"><a href="http://www.jpt.hu/magazin" target="_blank"><font color='red'>jobpilot.hu P�lyacs�cs Magazin</font></a><span class="small"> - �tvehetj�k forr�smegjel�l�ssel sz� szerint</span></td>
 </tr>
 <tr>
 <tr>
  <td bgcolor='#EBEEF3' colspan="2"><a href="http://www.magyarorszag.hu/" target="_blank">Magyarorsz�g.hu</a><span class="small"> - �tvehet� a title �s a lead, viszont a teljes cikk, csak �tkattint�ssal jelenhet meg. XML dump nincs.</span></td>
 </tr>
 <tr>
  <td bgcolor='#EBEEF3' colspan="2"><a href="http://www.jogiforum.hu/" target="_blank">Jogi F�rum</a><span class="small"> - Csak a h�reket szabad �tvenni. Azokat a cikkeket, ahol fel van t�ntetve a szerz�, nem lehet csak az � el�zetes enged�ly�kkel �tvenni szerz�i jogok miatt. �gyhogy csak a h�reket vegy�k �t forr�smegjel�l�ssel. Illetve mi is �tk�ldhet�nk <strong>saj�t</strong> tartalmat</span></td>
 </tr>
 <tr>
  <td bgcolor='#EBEEF3' colspan="2"><a href="http://www.fmm.gov.hu/" target="_blank">Foglalkoztat�spolitikai �s Munka�gyi Miniszt�rium</a><span class="small"> - �tvehetj�k forr�smegjel�l�ssel sz� szerint</span></td>
 </tr>   
 <tr>
  <td bgcolor='#EBEEF3' colspan="2"><a href="http://www.kum.hu/" target="_blank">K�l�gyminiszt�rium</a><span class="small"> - �tvehetj�k forr�smegjel�l�ssel sz� szerint</span></td>
 </tr>    
 <tr>
 <tr>
  <td bgcolor='#EBEEF3' colspan="2"><a href="http://www.piac-profit.hu/" target="_blank">Piac & Profit</a><span class="small"> - �tvehetj�k forr�smegjel�l�ssel �tm�solva sz� szerint, illetve �k is �tvenn�nek <strong>saj�t</strong> tartalmunkat + B�rkalkul�tort. A contetbeviteli oldalunkon van egy "Cikk �tk�ld�se tartalompartner sz�m�ra" funkci�, itt k�ldhet� el SAJ�T!!! cikk�nk nekik �tv�telre.</span></td>
 </tr>   
 <tr>
  <td bgcolor='#EBEEF3' colspan="2"><a href="http://www.dashofer.hu/dumpmaker_hrpt.php " target="_blank"><font color='red'>Verlag Dash�fer</font></a><span class="small"> - �tvehetj�k, de csak a <strong>SAJ�T</strong> cikkeiket. A forr�sokn�l a saj�t cikkek azok, amelyek vagy szem�lyn�vvel, vagy a
Verlag Dash�fer felirattal ell�tottak, vagy �resek.</span></td>
 </tr>   
 <tr>
  <td bgcolor='#EBEEF3' colspan="2"><a href="http://www.dashofer.hu/dumpmaker_hrpt_szem.php" target="_blank"><font color='red'>Verlag Dash�fer K�pz�sek, szemin�riumok a HR Clubra</font></a><span class="small"> - Vegy�k �t a HR-hez valamennyire kapcsol�d�kat! Bevitele: <a href="http://hrportal.hu/features" target="_blank">Features-bevitel oldal</a> Dash�fer Szemin�riumok</span></td>
 </tr>   
  <tr>
  <td bgcolor='#EBEEF3' colspan="2"><a href="http://www.dashofer.hu/dumpmaker_hrpt_konyv.php" target="_blank"><font color='red'>Verlag Dash�fer K�nyvaj�nl�hoz a HR Portalra</font></a><span class="small"> - A HR-hez kapcsol�d� szakk�nyveket vegy�k �t, ami m�g nincs fent! Bevitele: <a href="http://hrportal.hu/features" target="_blank">Features-bevitel oldal</a> Dash�fer K�nyvaj�nl�</span></td>
 </tr>   
  <tr>
  <td bgcolor='#EBEEF3' colspan="2"><a href="http://cegos.hu/index.php?c=trnaptar&v_t=25&m_m=0&m_s=2&m_sel=2&v_s=0" target="_blank"><font color='red'>Cegos Ny�lt tr�ningek a HR Clubra</font></a><span class="small"> - �t kell venn�nk �ket. Vegy�k �t ebb�l a n�gy kateg�ri�b�l (<u>Vezet�i k�szs�gfejleszt�s, Kommunik�ci�, Hum�ner�forr�s-gazd�lkod�s, P�nz�gyek, gazd�lkod�s</u>) mindig a legk�zelebbi esem�nyt (mert minden tr�ningj�k van vagy 3x �vente �s az sok lenne a HR Clubra) A forr�s kattanjon k�zvetlen�l az adott tr�ningre.</span></td>
 </tr>  
 <tr>
  <td bgcolor='#EBEEF3' colspan="2"><a href="http://www.ashcroft.hu/" target="_blank"><font color='green'>Ashcroft</font></a><span class="small"> - Mag�t�l �r be a /content oldalon �t. Tess�k figyleni az info@hrportal.hu �s info@hrclub.hu e-maileket �s a contentadmint!!</span></td>
 </tr>     
 <tr>
  <td bgcolor='#EBEEF3' colspan="2"><a href="http://www.munkaugyiszemle.hu/" target="_blank"><font color='green'>Munka�gyi szemle</font></a><span class="small"> - Mag�t�l k�ld �t havonta 2-3 szakcikket �v�nak mailben. Ha nem �r, h�vogatni kell!</span></td>
 </tr>      
 <tr>
  <td bgcolor='#EBEEF3' colspan="2"><a href="http://www.wolfoutdoor.hu/" target="_blank"><font color='green'>Wolf Outdoor</font></a><span class="small"> - Mag�t�l k�ne havi 1 outdoor tr�ninggel kapcsolatos cikket k�ldenie. Ha nem k�ld, a Bacsa B�l�t kell h�vni. 06.30.525.7447</span></td>
 </tr>  
 <tr>
  <td bgcolor='#EBEEF3' colspan="2"><a href="http://www.telkes.hu/" target="_blank"><font color='green'>Telkes Tan�csad� Rt.</font></a><span class="small"> - Mag�t�l k�ne cikkeket k�ldenie els�sorban munkajogi t�m�ban. <a href="mailto:fehere@telkes.hu">Feh�r Erika</a> h�vand�, ha nem k�ld.</span></td>
 </tr>   
 <tr>
  <td bgcolor='#EBEEF3' colspan="2"><a href="http://www.cvonline.hu/" target="_blank"><font color='green'>CV Online</font></a><span class="small"> - Mag�t�l k�ne elvileg cikkeket k�ldenie HR t�m�ban a HR Port�lra az �v�nak.</span></td>
 </tr>    
 <tr>
  <td bgcolor='#EBEEF3' colspan="2"><a href="http://www.cvonline.hu/" target="_blank"><font color='green'>CV Online</font></a><span class="small"> - Mag�t�l k�ne �ltal�nos munka�gyi cikkeket betennie a <a href="http://legjob.hu/addcontent">legjob.hu/addcontent</a> oldalon kereszt�l, ami automatikusan felker�l a legjobra. A jobpilot �s a 4Smarts is ezen �t viszi be a tartalmi linkeket.</span></td>
 </tr>   
 <tr>
  <td bgcolor='#EBEEF3' colspan="2">&nbsp;</td>
 </tr>   
</table>

  </td>
 </tr>
</table>
</body>
</html>
