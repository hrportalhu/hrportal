<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
<title>Szem�lyzet-fejleszt�si referens</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-2">
<link rel="STYLESHEET" type="text/css" href="http://www.hrportal.hu/style2.css">
   <style type="text/css">
	A:LINK {
		COLOR: #424D7B; TEXT-DECORATION: none; 
		font-weight:bold;		
	}
	A:VISITED {
		COLOR: #424D7B; TEXT-DECORATION: none; 
		font-weight:bold;				
	}
	A:HOVER {
		color : #AAAAAA; TEXT-DECORATION: underline; text-underline: single;
		font-weight:bold;				
	}    	
	TD {
  		color: #000000;
		font-size:10pt;
		font-family:"Arial, Helvetica, sans-serif";
		font-weight:normal;
		text-decoration: none;		
	}
        DIV { 
               font-family: Arial, Helvetica, sans-serif; 
               font-size: 10pt; 
	       font-style: none; 
	       font-weight: normal; 
	       color: #000000; 
	       line-height: 18px; 
	}
	.blue {
  		color: #424D7B;
		font-size:10pt;
		font-family:"Arial, Helvetica, sans-serif";
		font-weight:normal;
		text-decoration: none;	
        }
	.ti {
  		color: #424D7B;
		font-size:14pt;
		font-family:"Arial, Helvetica, sans-serif";
		font-weight:normal;
		text-decoration: none;	
	    line-height: 24px; 				
	}	
	B {
	       color: #424D7B;
	       font-size:12pt;
	       font-family:"Arial";		
	}
	
	
    </style>	
</head>

<body bgcolor="#FFFFFF" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" text="#000000" link="#000000" vlink="#000000" alink="#000000">

<table width="480" cellpadding="0" cellspacing="0" border="0" align="center">

 <tr>
  <td colspan="2" bgcolor="#FfFfFf" height="1" valign="top"><img src="http://www.hrportal.hu/images/pixel.gif" border="0" width="480" height="1"></td>  
 </tr>  
 <tr>
  <td bgcolor="#ffffff" width="277" valign="bottom"><a title="HR / Hum�n Ero"forr�s Port�l - minden a munka vil�g�b�l" href="http://www.hrportal.hu/" target="_blank"><img src="http://www.hrportal.hu/images/HR_Portal_06.gif" border="0"></a></td>
  <td width="303" align="right" class="horizontalmenu" valign="bottom">
   <table width="303" cellpadding="0" cellspacing="0" border="0">
    <tr>
     <td valign="bottom" align="right" class="horizontalmenu" style="padding-top: 2px; padding-bottom: 6px; padding-left: 2px; padding-right: 0px;">
	   
     </td>
    </tr>
   </table>
  </td>
 </tr>
 <tr>
  <td colspan="2" bgcolor="#999999" height="1" valign="top"><img src="http://www.hrportal.hu/images/pixel.gif" border="0" width="480" height="1"></td>  
 </tr>  
 <tr>
  <td colspan="2" height="5"><img src="http://www.hrportal.hu/images/pixel.gif" border="0" height="5" width="1"></td>
 </tr>  
 <tr>
  <td colspan="2" height="12" bgcolor="#C6D3EF" background="http://www.hrportal.hu/images/kereso_bg.gif"><img src="http://www.hrportal.hu/images/pixel.gif" border="0" height="12" width="1"></td>
 </tr> 
 <tr>
  <td colspan="2" bgcolor="#FFFFFF">
   <table width="100%" cellpadding="7" cellspacing="0" border="0">
    <tr>
     <td bgcolor="#FFFFFF">
   
	  <br>
     <div align="justify">

<div align="center"><img src="/allas/images/audi_hungaria.gif">
                  	  </div>
				  <br>


	  </div><BR>	
      <div align="left" class="ti">Szem�lyzet-fejleszt�si referens
</div> 
<div align="justify" class="blue"><BR>
Szem�lyzet- �s szervezetfejleszt�s, AUDI HUNGARIA MOTOR Kft.
</div>	  <br><br>
                  <div align="justify">

<p><strong>Elv�r�sok:</strong>

<p><ul>
<li>Fo"iskolai /egyetemi v�gzetts�g (HR vagy gazdas�gi szakon)
<li>7-8 �ves munkatapasztalat, min. 4 �v azonos munkak�rben
<li>Egy�b tan�csad�i k�pz�s elo"nyt jelent
<li>T�rgyal�k�pes n�met nyelvtud�s
<li> Terhelheto", �n�ll� szem�lyis�g, aki sz�m�ra fontos a teljes�tm�nyorient�lts�g
<li>Kiv�l� kommunik�ci�s k�pess�g
<li>Csoportmunk�ra val� k�szs�g
<li>Komplex gondolkod�s
<li>Kreativit�s
<li>Folyamatorient�lts�g
<li>Felelo"ss�gtudat
</ul>	

<p><strong>Feladatok:</strong>

<p><ul>
<li>Szem�lyzetfejleszt�si koncepci�k, folyamatok kidolgoz�sa �s mu"k�dtet�se
<li>A vezeto"k kiv�laszt�si-, �s fejleszt�si folyamat�nak kidolgoz�sa �s mu"k�dtet�se
<li>Munkat�rsak fejleszt�si folyamat�nak kidolgoz�sa �s mu"k�dtet�se
<li>Tan�csad�i feladatok (coaching) szem�lyzetfejleszt�si t�m�kban
<li>Akt�v k�zremu"k�d�s a v�llalati �s szem�ly�gyi projektekben, k�l�n�s tekintettel a szem�lyzeti fejleszt�sre
</ul>

<p><strong>Hivatkoz�si sz�m:</strong> 12567 

<p><strong>Kapcsolat:</strong> Dreska Marianna, szem�ly�gyi referens
<strong>Tel.:</strong> 96/66 8305
<strong>Fax:</strong> 96/66 1261
<p><strong>E-mail:</strong> <a href='mailto:karrier@audi.hu '>karrier@audi.hu </a>

<p><strong>AUDI HUNGARIA MOTOR Kft</strong>.
H-9027 Gyo"r, Kard�n u. 1.



<br><br>




                   </div>
<br>
</td>
    </tr>
   </table>
  </td>
 </tr>

 <tr><td colspan="2" bgcolor="#FFFFFF"><img src="http://www.hrportal.hu/images/pixel.gif" border="0" width="1" height="14"></td></tr>    
 


 <tr>
  <td colspan="2" bgcolor="#FFFFFF">
   <table width="100%" cellpadding="7" cellspacing="0" border="0">
    <tr>
     <td bgcolor="#FFFFFF">

     </td>
    </tr>
   </table>
  </td>
 </tr>  

 <tr>
  <td colspan="2" height="5"><img src="http://www.hrportal.hu/images/pixel.gif" border="0" height="5" width="1"></td>
 </tr>
 <tr>
  <td colspan="2" height="12" bgcolor="#A5B5D5" background="http://www.hrportal.hu/images/kereso_bg.gif"><img src="http://www.hrportal.hu/images/pixel.gif" border="0" height="12" width="1"></td>
 </tr>
 <tr>
  <td colspan="2" height="5"><img src="http://www.hrportal.hu/images/pixel.gif" border="0" height="5" width="1"></td>
 </tr>  
 <tr>
  <td colspan="2" height="1" bgcolor="#999999"><img src="http://www.hrportal.hu/images/pixel.gif" border="0" height="1"></td>
 </tr> 
 <tr>
  <td colspan="2" height="5"><img src="http://www.hrportal.hu/images/pixel.gif" border="0" height="5" width="1"></td>
 </tr>   
 <tr>
  <td colspan="2" height="10"><img src="http://www.hrportal.hu/images/pixel.gif" border="0" height="10" width="1"></td>
 </tr>   
 

</table>

</body>
</html>