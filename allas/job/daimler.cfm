
<html xmlns="http://www.w3.org/1999/xhtml"><head>


	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-2">

	<title>Stellenangebot</title>

	
	<link href="http://career.daimler.com/dhr/favicon.ico" rel="SHORTCUT ICON">
	<link href="http://career.daimler.com/dhr/wms/css/daimler_styles.css" rel="stylesheet" type="text/css" media="screen">
	<link href="http://career.daimler.com/dhr/wms/css/daimler_styles_std.css" title="daimler_styles_std" rel="stylesheet" type="text/css" media="screen">
	<link href="http://career.daimler.com/dhr/wms/css/daimler_styles_big.css" title="daimler_styles_big" rel="alternate stylesheet" type="text/css" media="screen">

	<link href="http://career.daimler.com/dhr/wms/css/daimler_styles_verybig.css" title="daimler_styles_verybig" rel="alternate stylesheet" type="text/css" media="screen">
	<link href="http://career.daimler.com/dhr/wms/js/jquery.js" type="text/javascript"></script>
	<link href="http://career.daimler.com/dhr/wms/js/function.js.php?l=1" type="text/javascript"></script>
	<link href="http://career.daimler.com/dhr/wms/js/ui.base.js" type="text/javascript"></script>
	<link href="http://career.daimler.com/dhr/wms/js/ui.slider.js" type="text/javascript"></script>
	<link href="http://career.daimler.com/dhr/wms/js/stylesheetToggle.js" type="text/javascript"></script>


	<!--[if gte IE 6]>
	<style>
	* {
		overflow-x: hidden !important;
	}
	</style>
	<![endif]-->
</head>
<body class="layerframe">
	<div id="header">
				<div id="tophead" title="Daimler">
			<div id="toph_logo"><img src="http://career.daimler.com/dhr/wms/images/header/logo_daimler.gif" class="print_ignore" border="0"></div>
			<div id="chapter"><img src="http://career.daimler.com/dhr/wms/images/header/hd_stelle_de.gif" class="print_ignore"></div>
			<div id="close_layer"><a href="javascript:closeLayerDisplay('1')"><img src="http://career.daimler.com/dhr/wms/images/icons/close_layer.gif" title="Schlie�en" class="print_ignore"></a></div>		</div>

	</div>



	
		<div class="clearer"></div>
	
	<div id="stelle_merken">
		<div id="stelle_merken_top">&nbsp;</div>
		<div id="stelle_merken_bottom">
			<div id="stelle_merken_inner">

				<form action="#" method="post"><input name="DAIMLERHR" value="11a0e052b306cb0e0bab75c5a6d233f7" type="hidden">
					<input id="stelle_email" name="stelle_email" value="Ihre eMail Adresse" type="text">
					<input id="stelle_send" value="Senden" type="button">
					<input id="stelle_cancel" value="" type="button">
					<div class="clearer">&nbsp;</div>
				</form>
			</div>
		</div>
	</div>

	<div id="content">
			<h1>Mercedes-Benz:  Toborz�s-kiv�laszt�si munkat�rs (77404) - Personal Recruiter, Werk Kecskem�t, Ungarn (77404)</h1>

		<div id="b_inhalt_links">
			<p>
				</p><h2>Aufgaben</h2>
				<ul>
					<li>Feladatok:</li><li>a szem�lyzeti ig�nyekhez kapcsol�d� toborz�si m�dszerek meghat�roz�sa,  kiv�laszt�si folyamat (kiv�laszt�i nap, interj�) defini�l�sa �s kialak�t�sa a szakter�lettel k�z�sen, </li><li>be�rkezett p�ly�zatok �ttekint�se, el�sz�r�se,</li><li>v�gs� kiv�laszt�s illetve kiv�laszt�i napok lebonyol�t�sa a szakter�lettel,</li><li>b�rt�rgyal�sok teljes k�r� levezet�se,</li><li>�j toborz�si koncepci�k �s folyamatok megval�s�t�sa a sz�ks�ges m�dos�t�sokkal,</li><li>a r�sztvev� vezet�k �s munkat�rsak inform�l�sa �s tan�csad�s r�sz�kre,</li><li>az operat�v megval�s�t�s biztos�t�sa, nyomon k�vet�se valamint �tfog� koordin�l�sa �s ir�ny�t�sa,</li><li>elt�r�sek felismer�se, bemutat�sa, esetleg megold�si javaslatok kidolgoz�sa,</li><li>a teljes rendszer m�k�d�k�pess�g�nek biztos�t�sa,</li><li>standardok defini�l�sa, riportok, kimutat�sok elk�sz�t�se, </li><li>gyakornokok �s a k�lcs�nz�tt munkaer�k munk�j�nak, feladatainak koordin�l�sa �s ellen�rz�se.</li><li>Aufgaben:</li><li>Abstimmung mit dem Fachbereich den Beschaffungsweg und den Auswahlprozess festlegen und gestalten (AC-Design, Interview, etc.),</li><li>Bewerberunterlagen pr�fen, Vorauswahl treffen, </li><li>Endauswahl , Auswahltag gemeinsam mit Fachbereich durchf�hren,</li><li>Vertragsbedingungen verhandeln und vereinbaren,</li><li>Neue Beschaffungs-Konzepte und Prozesse mit notwendigen Anpassungen umsetzen,</li><li>Beteiligte (z. B. F�hrungskr�fte, Mitarbeiter) informieren und beraten,</li><li>Operative Umsetzung verfolgen und sicherstellen, �bergreifend koordinieren und vorantreiben,</li><li>Abweichungen erkennen, darstellen und ggf. L�sungsvorschl�ge erarbeiten,</li><li>Funktionsf�higkeit des Gesamtsystems sicherstellen,</li><li>Definition von Standards festlegen und Entwicklung von Auswertungen und Reports,</li><li>F�hrung und Kontrolle der Praktikanten und AN�-s</li>				</ul>

			

			<p>
				</p><h2>Qualifikationen</h2>
				<ul>
					<li>Elv�r�sok:</li><li>lez�rt fels�fok� gazdas�gi v�gzetts�g (hum�n er�forr�s szakir�ny),</li><li>t�bb �ves, lehet�leg multinacion�lis �s/vagy termel�si k�rnyezetben szerzett  tapasztalat toborz�s-kiv�laszt�s �s szem�ly�gy ter�let�n, </li><li>struktur�lt gondolkod�sm�d,</li><li>csapatszellem,</li><li>magasfok� terhelhet�s�g, rugalmass�g,</li><li>j� n�metnyelv-tud�s.</li><li>Anforderungen:</li><li>Abgeschlossenes kaufm�nnisches Studium (HR)</li><li>Mehrj�hrige Kenntnisse im Bereich HR-Betreuung und Recruitment w�nschenswert im multinationalen Umfeld und/oder im Produktionsbereich</li><li>Strukturiertes Denken </li><li>Teamplayer</li><li>Hohe Belastbarkeit und Flexibilit�t</li><li>Sehr gute Deutsch- und Ungarischkenntnisse</li>				</ul>

			

			<p>
							</p><h2>zus�tzliche Informationen</h2>
				<ul style="padding-bottom: 15px;">
					<li>Die T�tigkeit ist in Vollzeit<br>Jelentkez�skor k�rj�k vegy�k figyelembe, hogy az �ll�saj�nlatok a Mercedes-Benz Manufacturing Hungary Kft. poz�ci�i., �s nem a Daimler AG �ltal meghirdetett Expat poz�ci�k. <br><p></p></li><li>P�ly�zat�nak elk�ld�s�re k�t lehet�s�g �ll rendelkez�sre. K�rj�k, hogy v�lassza ki az egyik lehet�s�get, de nem sz�ks�ges p�ly�zat�t mindk�t m�don beny�jtani. <br><p> </p></li><li>1. Lehet�s�g: az "Onlinebewerbung" gombon kereszt�l. <br> Ezt a gombot megtal�lja ennek a p�ly�zatnak az alj�n. Itt lehet�s�ge ny�lik arra, hogy szem�lyes adatait �s dokumentumait (motiv�ci�s lev�l, �n�letrajz �s bizony�tv�nyok) felt�ltse sz�munkra. <br><p></p></li><li>2. Lehet�s�g: a k�vetkez� email c�men kereszt�l: <a href="mailto:77404@daimler.com"><u><b>77404@daimler.com</b></u></a> <br> K�rj�k �nt, hogy ebben az esetben is k�ldje el r�sz�nkre a fent felsorolt dokumentumokat. K�rj�k vegye figyelembe, hogy erre az email c�mre elk�ld�tt p�ly�zata csak erre a konkr�t �ll�sra �rv�nyes: "Toborz�s-kiv�laszt�si munkat�rs (77404)". Ha m�s �ll�sra is szeretne jelentkezni, akkor az ott megadott konkr�t email c�mre kell p�ly�zat�t elk�ldenie. <br></li><li>Tov�bbi k�rd�sek eset�n az al�bbi ingyenesen h�vhat� telefonsz�mon v�rjuk h�v�s�t 8:00 �s 17:00 �ra k�z�tt: 0036/80/205 087.</li><li>Bitte beachten Sie, dass die Stellenausschreibung f�r die Mercedes-Benz Manufacturing Hungary, Ungarn, und nicht f�r die Daimler AG (Expatriates), gilt. <br><p></p></li><li>Sie haben zwei alternative M�glichkeiten, sich zu bewerben. Bitte entscheiden Sie sich f�r einen der beiden Wege. Es ist nicht notwendig, die Bewerbung �ber beide Wege einzureichen. <br><p> </p></li><li>1. M�glichkeit: �ber den Button "Onlinebewerbung". <br> Diesen finden Sie unterhalb dieser Ausschreibung. Dort haben Sie die M�glichkeit Angaben zu Ihrer Person zu machen sowie Dokumente (Anschreiben, Lebenslauf und Zeugnisse) anzuh�ngen. <br><p></p></li><li>2. M�glichkeit: �ber die folgende email-Adresse: <a href="mailto:77404@daimler.com"><u><b>77404@daimler.com</b></u></a> <br> Bitte f�gen Sie bei Bewerbung �ber diese email-Adresse ebenfalls oben genannte Anh�nge bei. Bitte beachten Sie, dass diese email-Adresse ausschlie�lich f�r Bewerbungen auf die Stelle " Personal Recruiter, Werk Kecskem�t, Ungarn (77404)" gilt. Bewerbungen f�r andere Stellen m�ssen �ber die jeweils dort angegebene email-Adresse eingereicht werden. <br></li><li>Bei weiteren Fragen rufen Sie uns zwischen 8:00 und 17:00 Uhr unter folgender kostenloser Telefonnummer an: 0036/80/205 087.<br></li>				</ul>



			
		</div>


		<div id="b_inhalt_rechts">
			

	<div id="tools">

		<h3>TOOLS</h3>
		<ul>
		<li><a href="javascript:window.parent.printPage('','/dhr/wms/extern/jobdetail.inc.php?vacancyId=077404&amp;print=1')" class="print_site">Seite drucken</a></li><li><a href="javascript:window.parent.forwardPage('077404','2')" class="forward_site">Seite weiterleiten</a></li>	</ul>

	</div>
	<div class="clearer">&nbsp;</div>
			<h3>Kurzinfos</h3>

			<h4>Ver�ffentlicht am:</h4>
			<p>08.11.2010</p>

			<h4>Ausschreibungsnummer:</h4>

			<p>077404</p>

						<h4>Standortinformation:</h4>
			<ul>
				<li><a href="javascript:window.parent.displayLayer('index.php?locId=4117')">Kecskem�t</a></li>			</ul>
			<h4>Abteilung:</h4><p>Personal Compact Cars &amp; Personal Werk Kecskem�t</p>

			<h4>T�tigkeitsbereich:</h4>
			<p>Personal</p>

<br><br><br><br><br><br><br>
		</div>

		<div class="clearer"></div>

			</div>
	
	<div id="error_layer" style="display: none;">
		<div id="close_alertlayer">

			<a href="#"><img src="http://career.daimler.com/dhr/wms/images/icons/close_layer.gif" class="print_ignore"></a>
		</div>
		<h1></h1>
		<p id="msgcontent"></p>
		<p id="buttons"></p>
	</div>



</body></html>
