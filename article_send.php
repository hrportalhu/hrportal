<?php
die();
error_reporting(E_ALL);
ini_set('display_errors','On');
include_once ("inc/sys.conf");
include_once ("inc/_functions.php");

global $db;
header('Content-Type: text/html;charset=utf-8');

$id=$_GET['id'];
if($id==""){
	$id=$_POST['id'];
}

$res_article=mysql_query("SELECT
                              a.id AS a_id,
                              a.teaser AS a_teaser,
                              a.lead AS a_lead,
                              a.body AS a_body,
                              a.small_pic AS a_small_pic,
                              a.small_pic_align AS a_small_pic_align,
                              a.large_pic AS a_large_pic,
                              a.large_pic_align AS a_large_pic_align,
                              a.date_last_modified AS a_date_last_modified,
                              a.nl2br AS a_nl2br,
                              a.source AS a_source,
                              c.name AS c_name,
                              cw.shown_name AS cw_shown_name,
                              l.link as link
                             FROM
                              (articles a LEFT JOIN categories c ON a.category_id=c.id)
                             LEFT JOIN
                              contentwriters cw ON a.contentwriter_login=cw.login
                               LEFT JOIN
                              links l ON a.id=l.art_id
                             WHERE
                              a.is_approved='yes'
                              and l.def=1
                             AND
                              a.id='$id'") or die(mysql_error());
                              
 if (mysql_num_rows($res_article)==0)
  die("Hiba: a cikk nem található");
 else
  $article=mysql_fetch_array($res_article);
                  
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
    <title>HR Portal - <?=($article['a_teaser'])?></title>
    <link rel="STYLESHEET" type="text/css" href="style22.css">
</head>
<body bgcolor='#ffffff' topmargin='0' leftmargin='0' marginheight='0' marginwidth='0' text='#000000' link='#000000' vlink='#000000' alink='#000000'>
<table width='400' cellpadding='0' cellspacing='0' border='0'>
 <tr>
  <td colspan='2' bgcolor='#999999' height='1' valign='top'><img src='images/pixel.gif' border='0' width='400' height='1'></td>  
 </tr>  
 <tr>
  <td bgcolor='#ffffff' width='277' valign='bottom'><a href='http://www.hrportal.hu/' target='_blank'><img src='images/HR_Portal_06.gif' border='0'></a></td>
  <td width='123' align='right' class='horizontalmenu' valign='bottom'>
   &nbsp;
  </td>
 </tr>
 <tr>
  <td colspan='2' bgcolor='#999999' height='1' valign='top'><img src='images/pixel.gif' border='0' width='400' height='1'></td>  
 </tr>  
 <tr>
  <td colspan='2' height='5'><img src='images/pixel.gif' border='0' height='5'></td>
 </tr>  
 <tr>
  <td colspan='2' height='8' bgcolor='#424F78'><img src='images/pixel.gif' border='0' height='8'></td>
 </tr> 
</table> 
<script language="Javascript">
 function mehet()
  {
   var f=document.forms.frm_send;
   if (f.felado.value=='' || f.cimzett.value=='')
    {
     alert("Kérjük, töltse ki a Feladó és a Címzett e-mail címét!");
    }
     else
    {
     f.submit();
    }
  }
</script>
<?php
 $cimzett=dangChars($_POST['cimzett']);
 $felado=dangChars($_POST['felado']);
 $megj=$_POST['megj'];
?> 

<?php
 if ($felado=="")
  {
?>
<table width='100%' cellpadding='0' cellspacing='5' border='0'>
 <form action='/article_send.php' method='post' name='frm_send'>
 <input class='horizontalinputp' type='hidden' name='id' value='<?=$id?>'>
 <tr>
  <td valign='top' colspan='2'>
  <br>
  <b>Cikk küldése:</b><br>
  <?=nl2br($article['a_teaser'])?><br>
  <br>
  <br>
  </td>
 </tr>
 <tr>
  <td align='right'>Feladó e-mail címe:</td>
  <td align='left'><input class='horizontalinputp' type='text' name='felado' size='20'></td>
 </tr>
 <tr>
  <td align='right'>Címzett e-mail címe:</td>
  <td align='left'><input class='horizontalinputp' type='text' name='cimzett' size='20'></td>
 </tr>
 <tr>
  <td align='right' valign='top'>Üzenet:</td>
  <td align='left'><textarea class='horizontalinputp' name='megj' cols='35' rows='4' wrap="hard"></textarea>
 </tr> 
 <tr>
  <td align='center' colspan='2'><input class='horizontalinputp' type="button" value="Mehet" onClick="mehet()"></td>
 </tr>
 </form>
</table>
<br>
<table width='400' cellpadding='0' cellspacing='0' border='0'>
 <tr>
  <td bgcolor='#DDDDDD' height='1' valign='top'><img src='images/pixel.gif' border='0' width='400' height='1'></td>  
 </tr>
</table>
<table width='400' cellpadding='10' cellspacing='0' border='0'>
 <tr>
  <td class='small'>
   Ez a kis program azonnal elküldi e-mailben a cikk internet-címét a Címzettnek, Önnek csak a Címzett e-mailjét, illetve saját mailcímét kell megadnia. Pár soros megjegyzést is mellékelhet az "üzenet" rovatban. 
  </td>
 </tr>
</table>
<?php
  } 
   else // if ($felado=="")
  {  
   //$article['a_teaser'] = ereg_replace("<([^>]+)>", "", $article['a_teaser']);
   //$article['a_lead'] = ereg_replace("<([^>]+)>", "", $article['a_lead']);
   
   $subject="HR Portal - Cikkajánló: ".$article['a_teaser'];
   $message="Tisztelt Címzett!\n\nA(z) $felado e-mail cím tulajdonosa úgy gondolta, Önt esetleg érdekelheti a HRPortal alábbi cikke:\n\n".$article['a_date_last_modified']." - ".$article['a_teaser']."\n\n".$article['a_lead']."\n\n";
   
   if ($megj!="")
    {
     $megj=str_replace("\r","",$megj);
     $message.="A Feladó megjegyzést is írt az ajánlathoz:\n$megj\n\n";
    }
    
   $message.="A teljes cikket az alábbi címen olvashatja el: http://new.hrportal.hu/".$article['link']."\n\nAmennyiben szeretne regisztrálni a HR Club / HR Portal HíRlevelére ezen a címen megteheti: http://www.hrportal.hu/do_hirlevel.phtml?edt_email=$cimzett\n\nÜdvözlettel:\n\tHR Portal - minden a munka világából <info@hrportal.hu>\n\n";
   $message.="-------------------------------------------------------------------\nEzt a levelet a HRPortal.hu-n található cikkajánló funkció segítségével küldték Önnek. A rendszer nem ellenőrzi a Feladó e-mail címének valódiságát. A HRPortal nem vállalja a felelősséget a Feladó véleményéért.";
//	gomail($cimzett,$message,$subject);
	 //mail($cimzett, $subject, $message, "From: ".$felado);
	 gomail($cimzett,$message,$subject);
 //  mail($cimzett, $subject, $message,  "From: HR Portal - minden a munka világából <info@hrportal.hu>");

?>
<table width='400' cellpadding='10' cellspacing='0' border='0'>
 <tr>
  <td class='small' align='center' valign='middle'>
   <br>
   <br>
   <b>Köszönjük, a cikkajánló útban van a címzett felé.</b><br>
   <br>
   <form action="http://www.hrportal.hu/hirlevreg.html" method="post" target="_blank">
    Szeretne feliratkozni HíRlevelünkre?<br>
    <br>
    <input class='horizontalinputp' type='button' value=' Igen ' onClick="window.opener.location.replace('http://www.hrportal.hu/hirlevreg.html'); self.close();"> <input type='button' value='Nem' onClick='self.close();'><br>
    <br>
   </form><br>
   <a href='javascript:self.close();' class='small'>ablak bezárása</a><br>
   <br>
  </td>
 </tr>
</table>
<?php
  } // end of else if ($felado=="")
?>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-696329-1";
urchinTracker();
</script>
</body>
</html>

<?
 mysql_close($connid); 
?>
