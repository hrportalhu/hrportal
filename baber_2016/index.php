﻿<!DOCTYPE html>
<html lang="en">
<head>
    
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-2">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>HR PORTÁL – BABÉR PANORÁMA - 2016</title>

    <!-- ## Bootstrap ## -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="assets/css/supersized.css" rel="stylesheet" />
    <link id="skin-styler" href="assets/css/skin1.css" rel="stylesheet" />
    <link href="assets/css/styleswitcher.css" rel="stylesheet" />
    <link href="assets/ytplayer/css/YTPlayer.css" rel="stylesheet" />
    <link type="text/css" href='http://fonts.googleapis.com/css?family=Aldrich' rel="stylesheet" />
    <!-- bxSlider CSS file -->
<link href="js/jquery.bxslider.css" rel="stylesheet" />
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js" type="text/javascript"></script>
      <script src="assets/js/respond.min.js" type="text/javascript"></script>
    <![endif]-->

    <link rel="apple-touch-fa-precomposed" sizes="144x144" href="assets/ico/apple-touch-fa-144-precomposed.png" />
    <link rel="shortcut icon" href="assets/ico/favicon.ico" />
    
    <style>
      #map-canvas {
        width: 500px;
        height: 400px;
      }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script>
      function initialize() {
        var mapCanvas = document.getElementById('map-canvas');
        var mapOptions = {
          center: new google.maps.LatLng(47.4984232,19.0705289),
          zoom: 14,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(mapCanvas, mapOptions)
		  marker = new google.maps.Marker({
			position: new google.maps.LatLng(47.4984232, 19.0705289),
			map: map
		  });
      }
      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
<script type="text/javascript">



function checkEmail(emailstr){
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailstr)){
		return (true)
	}
	return (false)
 } 


</script>
  
</head>
<body id="top" data-spy="scroll" data-target=".navbar" data-offset="200">
    <div class="bg-video"></div>
    <!-- ## page wrapper start ## -->
    <div id="page-wrapper">
        <!-- ## parallax section 1 start ## -->
        <section id="parallax-section-1">
            <!-- ## parallax background 1 ## -->
            <div class="bg1"></div>
            <!-- ## parallax background pattern 1 start ## -->
            <div >



                <!-- ## header start ## -->
                <header id="header">
                    <div class="container">
                        <div class="row"></div>
                    </div>
                    <!-- ## nav start ## -->
                <nav id="nav">
                    <div class="navbar navbar-inverse navbar-fixed-bottom" role="navigation">
                        <div class="container">

                            <div class="navbar-collapse collapse navbar-right">
                                <ul class="nav navbar-nav">
                                    <li class="active"><a href="#top">Nyitó</a></li>
                                    <li><a href="#about">Esemény bemutatása</a></li>
                                    <li><a href="#content-boxes">Program</a></li>
                                    <li><a href="#team">Előadók</a></li>
                                    <li><a href="#contact">Jelentkezés</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </nav>
                <!-- ## nav end ## -->
                </header>
                <!-- ## header end ## -->
                    <!-- ## about section start ## -->
                <section id="about" class="scrollblock">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 text-center about-box">
                                <h2>Program
                            <span class="line"><span class="dot"></span></span>
                                </h2>
                                <p>
                                  <?
                                  $text='

				<span style="color:#d5ecf3;font-weight:bold;">HR PORTÁL – BABÉR PANORÁMA - 2016</span> <br /><br />
				A mai gyorsan változó világban, egy cég életében talán az egyik legnagyobb kihívással teli feladat a saját dolgozók megtartása, illetve motiválása. Amikor a cégek belső struktúrájában változás történik, munkakörök szűnnek meg, vagy éppen új feladatok jönnek létre, változik a cégstruktúra, szabályzatok, szervezeti felépítések, vállalati rendszerek,  juttatások, ezektől az átalakulásoktól egy munkavállaló alapvetően fél, nem érzi komfortosan magát a saját munkahelyén, ami nyilván kihat mind a munkájára, mind pedig a céghez való elköteleződésére. Ráadásul a versenyszférában harcolnak a vállalatok a szakemberekért, egyre nagyobb a munkaerőhiány.<br /><br />
A válság már régen megváltoztatta az üzleti életet, maguk az emberek is változnak, akiket motiválni, ösztönözni kell. A motiváció mára sokkal mélyebben gyökeredző, komplexebb jelenség lett, melyet nem csupán anyagi javak hajtanak, ráadásul egyénenként nagymértékben változnak. De mik is ezek a hajtóerők? Hogyan lehet a mai világban megtartani és ösztönözni a dolgozót? <br /><br />
Olyan neves előadók segítségével járjuk körbe a témát, akik mentálhigiénés, klinikai pszichológiai vagy éppen cafeteria szakértői szemmel, fejvadász vagy coaching tapasztalataikból merítve világítanak rá a felmerülhető nehézségekre és ezek megoldásaira. <br /><br />
Egy szervezeti átalakulás folyamán nélkülözhetetlen képesség az előrelátás, gyors helyzetfelismerés és a megfelelő reakció. Az ilyen komplex helyzetekhez nyújt gyakorlati segítséget előadásunk Galambos Ágnessel (Krauthammer).<br /><br />
Az összeolvadás „kemény” oldala. Molnár Mónika (B3 TAKARÉK Szövetkezet) HR igazgatójától példát hallunk arról, hogy hogyan lehet kreatívan menedzselni egy céges szintű változást, amikor már a klasszikus lépéseket nem lehet követni.<br /> <br />
Szó lesz a 2017-től érvénybe lépő cafeteria változásokról. Fata László (Cafeteria Trend) a téma neves szakértője, tapasztalataiból merítve mutat be néhány példát a legújabb béren kívüli juttatásokból.<br /><br />
Hári Péter (CX-Ray) a hálózatelemzés fontosságára hívja fel a figyelmet, mely által könnyebben kiszűrhetőek a kevésbé motivált munkatársak, illetve tippeket ad azzal kapcsolatban is, hogy hogyan lehet csökkenteni a fluktuációt.<br /><br />
Ízelítőt adunk Dr. Szondy Máté és Bíró Gabriella (Rávezető projekt) felmérései alapján olyan eszközökből, amelyek béremelés nélkül is a munkatársi elköteleződést növelik. <br /><br />
Milyen mértékben hatnak a motivációt befolyásoló tényezők az egyénre, mi késztet egy munkavállalót arra, hogy elcsábuljon? Ezeket a kérdéseket boncolgatja Egyed Ildikó (Aurum Oktatási Központ-LeanCenter) fejvadász tapasztalatai alapján.<br /><br />
És végül, de nem utolsó sorban sztár előadónk Dr Belső Nóra (pszichiáter szakorvos) tart előadást arról, hogy mi kell ahhoz, hogy lelki és mentális értelemben is készen álljunk feladataink elvégzésére.<br /><br />
Délután 3 workshop keretén belül a résztvevők kicserélhetik tapasztalataikat a számukra jól bevált béren kívüli juttatásokról, gyakorlati tippeket, trükköket szerezhetnek arról, hogy egy csapatot hogyan lehet pozitívan befolyásolni. Motiváció vagy manipuláció. 3. témánk pedig a lemorzsolódás rejtett okait veszi számba, ami az exit interjúból nem derül ki.<br /><br />
				';
					
                                  //echo iconv("utf-8","iso-8859-2",$text);
                                  echo $text;
                                  
                                  ?>
                                </p>
                            </div>
                        </div>

                     
                    </div>
                </section>
        <!-- ## content boxes start ## -->
        <section id="content-boxes" class="scrollblock">
            <div class="container">
                <div class="row">

                   <ul class="bxslider">
					  <li><img src="Slideok-01.jpg" /></li>
					  <li><img src="Slideok-02.jpg" /></li>
					  <li><img src="Slideok-03.jpg" /></li>
					  <li><img src="Slideok-04.jpg" /></li>
					  <li><img src="Slideok-05.jpg" /></li>
					  <li><img src="Slideok-06.jpg" /></li>
					  <li><img src="Slideok-07.jpg" /></li>
					  <li><img src="Slideok-08.jpg" /></li>
					  <li><img src="Slideok-09.jpg" /></li>
					  <li><img src="Slideok-10.jpg" /></li>
					  <li><img src="Slideok-11.jpg" /></li>
					  <li><img src="Slideok-12.jpg" /></li>
					 
					</ul>
                </div>
            </div>
        </section>
        <!-- ## content boxes end ## -->
        
         <!-- ## parallax section 2 start ## -->
        <section id="parallax-section-2">
            <!-- ## parallax background 2 ## -->
            <div class="bg2"></div>
            <!-- ## parallax background pattern 2 start ## -->
            <div class="bg-pattern2" style="background: none;">
                <!-- ## team section start ## -->
                <section id="team" class="scrollblock_team">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <h2>Előadók
                             <span class="line"><span class="dot"></span></span>
                                </h2>
                            </div>
                        </div>
                        <div class="row0">
                            <div class="col-md-3 col-sm-6">
                                <div class="col-lg-12 member-box">
                                    <div class="member-img">
                                        <a href="#"><img class="img-responsive" src="assets/images/team/dr-belso-nora.jpg" alt=".img-responsive" /></a>
                                    </div>
                                    <div class="member-data">
                                        <div class="member-info">
                                            <div class="member-name"><a href="#">Dr Belső Nóra</a></div>
                                            <div class="member-title">Pszichiáter szakorvos, egészségügyi menedzser specialista</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="member-content">Több mint húsz éve dolgozik praktizáló pszichiáterként. Pályáját az azóta már bezárt OPNI-ban kezdte, ahol a gyógyító munka mellett több hazai és nemzetközi kutatásban is részt vett. Számos publikációja mellett tankönyv fejezeteket írt, segédszerkesztőként működött közre. Szakmai, társszakmai és más konferenciák állandó felkért előadója. Vallja, hogy az egészséges társadalom alapja az egészséges lelkű ember és család, ezért a pszichiátriai betegségek gyógyítása mellett párkapcsolati problémákkal, családi konfliktusrendezéssel és életmód tanácsadással is foglalkozik. Öt sikeres könyv szerzője.</div>
                                    </div>
                                    <div class="clearfix"></div>
                               </div>
                            </div>
                                
                                <!-- ## member box end ## -->
                            <div class="col-md-3 col-sm-6">
                                <div class="col-lg-12 member-box">
                                    <div class="member-img">
                                        <a href="#"><img class="img-responsive" src="assets/images/team/fata-laszlo.jpg" alt=".img-responsive" /></a>
                                    </div>
                                    <div class="member-data">
                                        <div class="member-info">
                                            <div class="member-name"><a href="#">Fata László</a></div>
                                            <div class="member-title">Cafeteria szakértő</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="member-content">A Cafeteria TREND magazin alapítója, szerkesztője az Országos Humánmenedzsment Egyesület elnökségi tagja. 2005 óta foglalkozik béren kívüli juttatásokkal. Tanácsadói tevékenysége mellett rendszeres kutatásaival évek óta vizsgálja ezt a területet. Tudását a magazin mellett több száz fős konferenciákon, egyetemeken és eddig megjelent két könyvében osztja meg az érdeklődőkkel. Nevével többek közt az MR1-Kossuth Rádióban, Gazdasági Rádióban, RTL Klubon, M1-en, a Haszon magazin, HR Portál, HVG, Napi gazdaság hasábjain is találkozhatunk.</div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                                <!-- ## member box end ## -->
                            <div class="col-md-3 col-sm-6">
                                <div class="col-lg-12 member-box">
                                    <div class="member-img">
                                        <a href="#"><img class="img-responsive" src="assets/images/team/molnar-monika.jpg" alt=".img-responsive" /></a>
                                    </div>
                                    <div class="member-data">
                                        <div class="member-info">
                                            <div class="member-name"><a href="#">Molnár Mónika</a></div>
                                            <div class="member-title">B3 TAKARÉK Szövetkezet HR Igazgatója</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="member-content">Innovatív, lendületes HR vezető, aki 10 év alatt a HR különböző területén szerzett szakmai tapasztalatot. Trenkwalder cégcsoport HR vezetőjeként 8000 fő számfejtéséért felelt. Részt vett HR stratégia kidolgozásában, szervezeti- változásmenedzsmentben, szervezeti kultúraváltásban, új bónuszrendszer kidolgozásában. Számos nagyobb projektet vezetett nem csak hazai, de nemzetközi környezetben is. 6 ország új HR szoftver eredményes bevezetéséért 2015-ben Implementation Award díjjal jutalmazták. 2014-ben elnyerte az Év vezetője díjat is. Jelenleg a B3 TAKARÉK Szövetkezet HR Igazgatója.</div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                                <!-- ## member box end ## -->
                       
                                
                            <div class="col-md-3 col-sm-6">
                                <div class="col-lg-12 member-box">
                                    <div class="member-img">
                                        <a href="#"><img class="img-responsive" src="assets/images/team/galambos-agnes.jpg" 	 /></a>
                                    </div>
                                    <div class="member-data">
                                        <div class="member-info">
                                            <div class="member-name"><a href="#">Galambos Ágnes</a></div>
                                            <div class="member-title">Ügyvezető és senior tanácsadó</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="member-content">Banki szakemberből lett HR vezető, később tanácsadó, tréner-coach. Itthoni és amerikai végzettséggel egyaránt rendelkezik, akkreditált DISC tanácsadó, valamint a Krauthammer belső egyetem oszlopos tagja. Jelenleg a Krauthammer magyarországi ügyvezetőjeként és senior tanácsadójaként segít embereknek, szervezeteknek céljaik elérésében. Legfőbb területei: felsővezetői csapatok fejlesztése, sales management, leadership tréning és coaching, változási folyamatok megtervezése és támogatása, valamint a folyamatos újítás az „egyszerűsítés” módszerének alkalmazásával.</div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                                <!-- ## member box end ## -->
                         </div>
                     </div>
                     
                     
                       <div class="container">
						 <div class="row0">
							<div class="col-md-3 col-sm-6">
								<div class="col-lg-12 member-box2">
									<div class="member-img">
										<a href="#"><img class="img-responsive" src="assets/images/team/biro-gabriella.jpg" alt=".img-responsive"  /></a>
									</div>
									<div class="member-data">
										<div class="member-info">
											<div class="member-name"><a href="#"><font style="font-family:Aldrich; color:#000000; text-size:18px;"><strong>Bíró Gabriella</strong></font></a></div>
											<div class="member-title">OD tanácsadó</div>
										</div>
										<div class="clearfix"></div>
										<div class="member-content">Tanár, mentálhigiénés segítő szakember, kulturális antropológus, OD tanácsadó – tanulmányai mind emberekhez, emberi csoportokhoz, közösségekhez köthetők. Leginkább az érdekeli, milyen lelki és környezeti tényezők alakítják, formálják viselkedésünket, milyen mintázatok és törvényszerűségek befolyásolják döntéseinket, cselekedeteinket.</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="col-lg-12 member-box2">
									<div class="member-img">
										<a href="#"><img class="img-responsive" src="assets/images/team/dr-szondy-mate.jpg" alt=".img-responsive" /></a>
									</div>
									<div class="member-data">
										<div class="member-info">
											<div class="member-name"><a href="#"><font style="font-family:Aldrich; color:#000000; text-size:18px;"><strong>Dr. Szondy Máté</strong></font></a></div>
											<div class="member-title">Klinikai szakpszichológus és tréner</div>
										</div>
										<div class="clearfix"></div>
										<div class="member-content">Klinika szakpszichológus, egyetemi adjunktus és kutató. Terápiás munkájában és a céges tréningjeiben elsősorban mindfulness (tudatos jelenlét) alapú terápiás módszereket alkalmazza. Elkötelezett híve a bizonyítottan hatékony módszerek alkalmazásának és a pszichológiai ismeretek minél szélesebb körben való népszerűsítésének.</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="col-lg-12 member-box2" >
									<div class="member-img">
										<a href="#"><img class="img-responsive" src="assets/images/team/egyed-ildiko.jpg" alt=".img-responsive" /></a>
									</div>
									<div class="member-data">
										<div class="member-info">
											<div class="member-name"><a href="#"><font style="font-family:Aldrich; color:#000000; text-size:18px;"><strong>Egyed Ildikó</strong></font></a></div>
											<div class="member-title">Ügyvezető Partner, vezető tanácsadó</div>
										</div>
										<div class="clearfix"></div>
										<div class="member-content">Több mint 14 éve dolgozik HR területen, kezdetben a versenyszférában, majd 2007 óta a saját vállalkozásában. Jelenleg az Aurum Oktatási Központ - LeanCenter Üzletágának vezetőjeként, a cég ügyvezető partnereként széleskörű feladatokat lát el. Fő területe az executive fejvadászat és hatékony szervezet-építés. Több ezer interjúval a háta mögött, jól ismeri a munkavállalók és munkaadók igényeit, motivációit. Egyetemi előadásaival segíti a pályakezdők és álláskeresők elhelyezkedését. A munkaerő-piaci változások folyamatos nyomon követése érdekében rendszeresen végez felméréseket, kutatásokat.</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="col-lg-12 member-box2" >
									<div class="member-img">
										<a href="#"><img class="img-responsive" src="assets/images/team/hari-peter.jpg" alt=".img-responsive" /></a>
									</div>
									<div class="member-data">
										<div class="member-info">
											<div class="member-name"><a href="#"><font style="font-family:Aldrich; color:#000000; text-size:18px;"><strong>Hári Péter</strong></font></a></div>
											<div class="member-title">CX-Ray ügyvezető igazgató, alapító</div>
										</div>
										<div class="clearfix"></div>
										<div class="member-content">Több éves tapasztalattal rendelkezik szervezetfejlesztés terén. PhD kutatásának témája a szociális háló hatása a szervezeti hatékonyságra. 2014-ben társalapítóként vett részt a szervezeti hálózatelemzésen alapuló startup, a CX-Ray életében. 2015-től már ügyvezető igazgatóként tevékenykedik. </div>												
									<div class="clearfix"></div>
								</div>
							</div> 
						</div>
					</div>
     
                </section>
                <!-- ## team section end ## -->
            </div>
            <!-- ## parallax background pattern 2 end ## -->
        </section>
        <!-- ## parallax section 2 end ## -->




       
        
        <!-- ## contact section start ## -->
        <section id="contact" class="scrollblock">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h2>Jelentkezés
                        <span class="line"><span class="dot"></span></span>
                        </h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 contact-form contact-box">
                        <h4>Jelentkezzen a BABÉR PANORÁMA konferenciánkra!</h4>
                        <p>
						HR PORTÁL – BABÉR PANORÁMA - 2016<br>
						
						</p>
						<p>
							<b>A konferencia díja: Earlybird szeptember 16-ig 23 900 Ft + áfa/ fő.</b><br />
							<b>Szeptember 10 után 29 500 Ft + áfa/fő</b><br />
							
							
A díj tartalmazza az ebédet és a napközbeni finomságokat, frissítőket.
						</p>
						
                      <form name="form-contact-us" id="form-contact-us">
                       
                            <div class="form-group">
								
                            Online jelentkezéséről e-mailben visszaigazolást küldünk. Amennyiben a visszaigazoló levélben rákattint az aktiváló linkre, azzal elfogadja az adatok helyességét. Ezt követően a jóváhagyott adatok alapján átutalásos, ELEKTRONIKUS számlát küldünk a részvételi díjról, melyet kérünk a regisztráció visszaigazolásától számított 8 munkanapon belül - de legkésőbb a rendezvény napjáig - kiegyenlíteni. Az online regisztráció megrendelésnek minősül. Amennyiben a számlát postai úton kérik, az üzenet részben kérjük ezt  feltüntetni.
							<br><br>A rendezvényen a részvétel feltétele a részvételi díj előzetes kiegyenlítése. Lemondást a részvételi díj teljes visszafizetése mellett a rendezvény napja előtti 5. munkanapon 12 óráig - KIZÁRÓLAG ÍRÁSBAN a info@hrportal.hu címre - fogadunk el. Az ezutáni lemondások esetében a részvétel átruházható, de a teljes díjat ki kell fizetni. Telefonos vagy egyéb szóbeli lemondásokat nem áll módunkban elfogadni.
							<br><br>A lemondási határidő utáni regisztráció esetén lemondásra nincs lehetőség.
                            </div>
							<div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="txtName">Név: </label>
                                        <input type="text" placeholder="Név"
                                            class="form-control required" id="txtName" name="txtName" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="txtEmail">E-mail:</label>
                                        <input type="text" placeholder="E-mail ID"
                                            class="form-control required email" id="txtEmail" name="txtEmail" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="txtphone">Telefonszám:</label>
                                        <input type="text" placeholder="Telefonszám"
                                            class="form-control required phone" id="txtphone" name="txtphone" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="txtszlaname">Számlázási név:</label>
                                        <input type="text" placeholder="Számlázási név"
                                            class="form-control " id="txtszlaname" name="txtszlaname" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="txtszlaaddr">Számlázási cím:</label>
                                        <input type="text" placeholder="Számlázási cím"
                                            class="form-control " id="txtszlaaddr" name="txtszlaaddr" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="txtMessage">Üzenet:</label>
                                <textarea placeholder="Message" class="form-control required"
                                    id="txtMessage" name="txtMessage" rows="10"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="txtCaptcha"></label>
                                <input
                                    type="text" class="form-control required captcha"
                                    placeholder="Egy keresztkérdés?" id="txtCaptcha" name="txtCaptcha" />
                            </div>
                            <div class="form-group">
                                <input type="button" id="btn-Send" value="Küld" class="btn btn-theme-inverse" />
                            </div>
                            <div class="col-lg-12" id="contact-form-message">
                        </form>
                        <!-- ## contact form end ## -->
                    </div>
                    </div>

                    <div class="col-sm-6 contact-info contact-box">
                        <h4>Elérhetőségek</h4>
                        <!-- ## contact info start ## -->
                        <ul class="fa-ul">
                            <li><i class="fa-li fa fa-clock-o text-theme-inverse"></i><span>Időpont: 2016. október 24. 9:00</span></li>
                            <li><i class="fa-li fa fa-home text-theme-inverse"></i><span>1073 Budapest, Erzsébet krt. 9</span></li>
                            <li><i class="fa-li fa fa-envelope text-theme-inverse"></i><span>info@hrportal.hu</span></li>
                            <li><i class="fa-li fa fa-globe text-theme-inverse"></i><span>www.hrportal.hu/baber_2016/</span></li>
                        </ul>
                        <!-- ## contact info endstart ## -->
                        <!-- ## map start ## -->
<div id="map-canvas"></div>
                        <!-- ## map end ## -->
                    </div>
                </div>
            </div>
        </section>
        <!-- ## contact section end ## -->

        <!-- ## blockquote section start ## -->
        <section id="bquote" class="scrollblock">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-lg-offset-1 quote-box">
                        <blockquote>
                            <p>Jelentkezzen programunkra!</p>
                        </blockquote>
                    </div>
                </div>
            </div>
        </section>
        <!-- ## blockquote section end ## -->


        <!-- ## twitter section end ## -->

        <!-- ## footer section start ## -->
        <section id="footer">
            <!-- ## footer background pattern start ## -->
            <div class="bg-pattern3">

                <!-- ## footer copyright start ## -->
                <div class="footer-copyright text-center">
                    HRPortal.hu Copyright &copy; 2016, All rights reserved.
                </div>
                <!-- ## footer copyright end ## -->
            </div>
            <!-- ## footer background pattern end ## -->
        </section>
        <!-- ## footer section end ## -->

        <!-- ## preloader start ## -->
        <div id="preloader">
            <div id="status"><span class="fa fa-spin fa-spinner fa-5x"></span></div>
        </div>
        <!-- ## preloader end ## -->

        <!-- ## Time Bar begins here ## -->
        <!-- ## do not change it. ## -->
        <div id="progress-back" class="load-item">
            <div id="progress-bar"></div>
        </div>
        <!-- ## Time Bar ends here ## -->
    </div>
    <!-- ## page wrapper end ## -->

    <script src="assets/js/jquery-2.0.0.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/twitter/jquery.tweet.min.js" type="text/javascript"></script>
    <script src="assets/js/jquery.parallax-1.1.3.js" type="text/javascript"></script>
    <script src="assets/js/jquery.nicescroll.min.js" type="text/javascript"></script>
    <script src="assets/js/detectmobilebrowser.js" type="text/javascript"></script>
    <script src="assets/js/jquery.smooth-scroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.scrollorama.js" type="text/javascript"></script>
    <script src="assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="assets/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="assets/js/jquery.cookie.js" type="text/javascript"></script>
    <script src="assets/ytplayer/js/jquery.mb.YTPlayer.js" type="text/javascript"></script>
    <script src="assets/js/app.variables.js" type="text/javascript"></script>
    <!--<script src="assets/js/jquery.countdown.js" type="text/javascript"></script>
    <script src="assets/js/app.subscribe.js" type="text/javascript"></script>
    <script src="assets/js/app.contact.js" type="text/javascript"></script>
    <script src="assets/js/app.ui.js" type="text/javascript"></script>
    <script src="assets/js/app.styleswitcher.js" type="text/javascript"></script>-->
    <script src="/js/bxslider/jquery.bxslider.min.js"></script>

    <script src="assets/js/app.landlite.js" type="text/javascript"></script>
    <script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-696329-1', 'auto');
	  ga('send', 'pageview');

	</script> 
	<script type="text/javascript">
		<!--//--><![CDATA[//><!--
		var pp_gemius_identifier = 'bDhFBhiFYYCqPthkhx47lXZJHSGNJKL9TT54fXM4747.D7';
		// lines below shouldn't be edited
		function gemius_pending(i) { window[i] = window[i] || function() {var x = window[i+'_pdata'] = window[i+'_pdata'] || []; x[x.length]=arguments;};};
		gemius_pending('gemius_hit'); gemius_pending('gemius_event'); gemius_pending('pp_gemius_hit'); gemius_pending('pp_gemius_event');
		(function(d,t) {try {var gt=d.createElement(t),s=d.getElementsByTagName(t)[0],l='http'+((location.protocol=='https:')?'s':''); gt.setAttribute('async','async');
		gt.setAttribute('defer','defer'); gt.src=l+'://huadn.hit.gemius.pl/xgemius.js'; s.parentNode.insertBefore(gt,s);} catch (e) {}})(document,'script');
		//--><!]]>
		</script>
</body>
</html>
