<?
//Developed by: Moe Sweet <moesweet@myanmar.com.mm>
//From: Myanmar (Burma)
//Date: 28-June-07

class ColorElement{

	var $colorName;
	var $colorCode; 
	var $width;
	var $height;
	var $path;//path to "drawColorElement.php"
	
	function ColorElement($colorName,$colorCode,$width,$height){
		$this->colorCode = strtolower(substr($colorCode,1,6));
		$this->width = $width;
		$this->height = $height;
		$this->colorName= $colorName;
	}
	function setPath($path){
		$this->path=$path;
	}
	function draw(){
	?><img src="<?=$this->path?>drawColorElement.php?colorCode=<?=$this->colorCode?>&width=<?=$this->width?>&height=<?=$this->height?>"><?
	}
}
?>