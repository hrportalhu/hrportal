<?
//Developed by: Moe Sweet <moesweet@myanmar.com.mm>
//From: Myanmar (Burma)
//Date: 28-June-07

class TrueColorPicker{

	//Determine how many nubmer of steps/blocks between each corner of the picker
	//least detail: 16, fast
	//true color, Photoshop-standard: 256, slow
	//optimal: 64	
	var $detailFactor;//

	//a color code to be appeared in "Current" pane
	var $preloadColor;
	
	var $retForm;
	
	
	//Constructor
	function TrueColorPicker(){
		$this->detailFactor=64;//default detail factor
	}	
	//set default factor
	function setDf($df){
		if($df>=16 && $df<=256){
			$this->detailFactor = $df;
		}
	}
	function setMform($fname){
		$this->retForm = $fname;
		
	}
	function setPreload($preload){
		$this->preloadColor = substr($preload,1,6);
	}
	
	function build(){
		$borderColor = "#666666";
	?>
	<table cellpadding="0" cellspacing="0" style="border:2px <?=$borderColor?> solid;background:#dddddd;font-family:Verdana, Arial, Helvetica, sans-serif;font-size:10px">
		<tr>
			<td align="center" rowspan="3" id="brightnessBoxContainer" width="256" height="256" style="padding:3px 3px 3px 2px;border-right:1px <?=$borderColor?> solid">Kattintson a jobboldali vonalra
			
			</td>
			<td rowspan="3" style="padding:2px 3px 2px 3px;"><?=$this->buildHueBox()?></td>
			<td align="center" valign="top" width="100" height="100" style="padding:3px 2px 0 3px">
				
				<img style="border:1px <?=$borderColor?> solid" id="selectedColor" src="drawColorElement.php?colorCode=ffffff&width=100&height=50">
				<?
				if(!empty($this->preloadColor)){ ?>
					&Uacute;j<br /><br />
					Aktu&aacute;lis
					<img style="border:1px <?=$borderColor?> solid" id="selectedColor" src="drawColorElement.php?colorCode=<?=$this->preloadColor?>&width=100&height=50" onclick="brightnessBoxClick('<?=$this->preloadColor?>')">
				<? }
				?>
			</td>
		</tr>
		<tr>
			<td width="100" height="60" valign="bottom" align="right" style="font-family:'Courier New', Courier, monospace;font-size:14px;padding-right:2px">
					 # <input id="txtSelectedColorCode" style="font-family:'Courier New', Courier, monospace" type="text" size="6" value="ffffff"/>
			</td>
		</tr>
		<tr>
			<td align="right" valign="bottom" style="padding:0 2px 2px 0">
				<button type="button" onclick="okClick('<?=$this->retForm;?>')">Rendben</button><button type="button" onclick="cancelClick()">M&eacute;gsem</button>
			</td>
		</tr>
	</table>
	<?
	}
	//Private
	function buildHueBox(){
		$hueFile = "hueColors.txt";
		eval(file_get_contents($hueFile));		
		?><table cellpadding="0" cellspacing="0" style="background:url('hueBox.png')" width="20" height="258"><?
			foreach($hueArray as $hueColorCode){
				?><tr><td onclick="hueColorClick('<?=substr($hueColorCode,1,6)?>',<?=$this->detailFactor?>)" height="1">
				  </td></tr><?	
			}
		?></table><?
	}

}
?>
