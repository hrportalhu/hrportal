// JavaScript Document

//****************************
// Customize these 2 functions
//****************************
var oha;
function okClick(oha){
	clr = (document.getElementById("txtSelectedColorCode").value);
	window.opener.passColorCode(clr,oha);
	window.opener.Listazz();
	self.close();
}

function cancelClick(){
	window.close();
}

//******************************
// Leave the following unchanged
//******************************

function hueColorClick(baseColor,detailFactor){
	requestPage('buildBrightnessTable.php?df=' + detailFactor + '&bc=' + baseColor,'brightnessBoxContainer','Loading...');
	brightnessBoxClick(baseColor);
}

function brightnessBoxClick(clr){
	document.getElementById("selectedColor").src = 'drawColorElement.php?colorCode=' + clr + '&width=100&height=50';
	document.getElementById("txtSelectedColorCode").value = clr;
}

var xmlHttpRequestObjGroup 	= new Array();	// this is to store Http Request Objects 
var arrayTracker			= new Array();	// this is to store "OK" or "Not OK" to overwrite HttpRequestObject
var objIndex				= null;			// index to the xmlHttpRequestObjGroup and arrayTracker
/**
This function creates http request object depending on the browser with which user view.
**/
function createObject()
{
	var xmlHttpRequestObject = false;

	if (window.XMLHttpRequest){ // if Mozilla, Safari etc
		if(checkArrayTrack()){
			xmlHttpRequestObjGroup[objIndex] = new XMLHttpRequest();
			arrayTracker[objIndex] = "Not OK to overwrite";
			xmlHttpRequestObject = xmlHttpRequestObjGroup[objIndex];
		}else{
			xmlHttpRequestObjGroup.push(new XMLHttpRequest());
			arrayTracker.push("Not OK to overwrite");
			objIndex = xmlHttpRequestObjGroup.length-1;
			xmlHttpRequestObject = xmlHttpRequestObjGroup[objIndex];
		}
	}
	else if (window.ActiveXObject){ // if IE
		try {
			if(checkArrayTrack()){
				xmlHttpRequestObjGroup[objIndex] = new ActiveXObject("Msxml2.XMLHTTP");
				arrayTracker[objIndex] = "Not OK to overwrite";
				xmlHttpRequestObject = xmlHttpRequestObjGroup[objIndex];
			}else{			
				xmlHttpRequestObjGroup.push(new ActiveXObject("Msxml2.XMLHTTP"));
				arrayTracker.push("Not OK to overwrite");
				objIndex = xmlHttpRequestObjGroup.length-1;
				xmlHttpRequestObject = xmlHttpRequestObjGroup[objIndex];			
			}
		} 
		catch (e){
			try{
				if(checkArrayTrack()){
					xmlHttpRequestObjGroup[objIndex] = new ActiveXObject("Microsoft.XMLHTTP");
					arrayTracker[objIndex] = "Not OK to overwrite";
					xmlHttpRequestObject = xmlHttpRequestObjGroup[objIndex];
				}else{						
					xmlHttpRequestObjGroup.push(new ActiveXObject("Microsoft.XMLHTTP"));
					arrayTracker.push("Not OK to overwrite");
					objIndex = xmlHttpRequestObjGroup.length-1;
					xmlHttpRequestObject = xmlHttpRequestObjGroup[objIndex];				
				}
			}
			catch (e){}
		}
	}
	else{
		alert('This browswer cannot support HttpRequest Object!');
		return false;
	}
	
	return xmlHttpRequestObject;
}
/**
 *This function destroys http request object
*/
function destroyObject(object){
	delete object;
	object = null;
	arrayTracker[objIndex] = 'OK to overwrite';
}
/**
 * This function checks one of the arrayTracker is available ('ok to overwrite')
**/
function checkArrayTrack(){
	for(i=0; i<arrayTracker.length; i++){
		if(arrayTracker[i] == 'OK to overwrite'){
			objIndex = i;
			return true;
		}
	}
	return false;
}
/*
 * This function is directly set the data downloaded from server into the innerHTML property of the container. 
 * And preload information is supported.
*/
function requestPage(url, containerid, preload)
{
	if(preload != undefined){
		document.getElementById(containerid).innerHTML = preload;
	}
	
	var xmlHttpRequestObject = createObject();
	if(xmlHttpRequestObject)
	{	
		xmlHttpRequestObject.open('GET', url, true);
		xmlHttpRequestObject.onreadystatechange=function()
		{
			if (xmlHttpRequestObject.readyState == 4 && xmlHttpRequestObject.status==200)
			{
				//alert(xmlHttpRequestObject.responseText);
				document.getElementById(containerid).innerHTML = xmlHttpRequestObject.responseText;
				destroyObject(xmlHttpRequestObject);
			}
		}
		
	}	
	xmlHttpRequestObject.send(null)	
}
