<?
	function generateBrightnessArrays($df,$baseColorCode){
	//this function reveices a base color code and returns a 2 dimentional array holding color codes of 65563 colors
	//base color code is to be placed at the upper right corner of the brightness box
	$folder = "ColorArrays/128/";
		extract(hexToRgb($baseColorCode));
			$baseR = $r;
			$baseG = $g;
			$baseB = $b;
		
			$roundColumnR = $baseR;$roundColumnG = $baseG;$roundColumnB = $baseB;
			
		$columnRinterval = getColorInterval($baseR,0,$df);
		$columnGinterval = getColorInterval($baseG,0,$df);
		$columnBinterval = getColorInterval($baseB,0,$df);

		$fileContent = '$returnArr=Array(';


		$leftmostGrayInterval = getColorInterval(255,0,$df);
		$leftmostGray = 255;
		//two loops are going left to right, top to bottom
		//outer loop, column loop
		for($outerLoop=0;$outerLoop<($df);$outerLoop++){
			//the left most side color, grayscale
			unset($row);
			$fileContent.="Array(";			

			$leftmost=rgbToColorCode($leftmostGray,$leftmostGray,$leftmostGray);
			$fileContent.="\"$leftmost\"";	
			$leftmostGray += $leftmostGrayInterval;

			$row[] = $leftmost;
			

			//innerloop, rowloop
			
			$rowR = $leftmostGray;
			$rowG = $leftmostGray;
			$rowB = $leftmostGray;
		
			$rowRInterval = getColorInterval($leftmostGray,$roundColumnR,$df);
			$rowGInterval = getColorInterval($leftmostGray,$roundColumnG,$df);
			$rowBInterval = getColorInterval($leftmostGray,$roundColumnB,$df);

			for($innerLoop=1;$innerLoop<=($df-2);$innerLoop++){
				$rowR += $rowRInterval;
					$roundRowR = round($rowR);
				$rowG += $rowGInterval;
					$roundRowG = round($rowG);
				$rowB += $rowBInterval;
					$roundRowB = round($rowB);

				$rowColorCode = rgbToColorCode($roundRowR,$roundRowG,$roundRowB);

				$fileContent.=",\"$rowColorCode\"";

			}
			$columnLastColorCode = rgbToColorCode($roundColumnR,$roundColumnG,$roundColumnB);

			$fileContent.=",\"$columnLastColorCode\"";

			//approaching down
			$baseR += $columnRinterval;
				$roundColumnR = round($baseR);
			$baseG += $columnGinterval;
				$roundColumnG = round($baseG);
			$baseB += $columnBinterval;
				$roundColumnB = round($baseB);
			$fileContent.=")";
			if($outerLoop<$df-1)$fileContent.=", ";

		}//inner loop
		$fileContent .= ");";
		//$file = fopen($folder.$colorName.'.txt','w');
		//fwrite($file,$fileContent);
		eval($fileContent);
		if($returnArr)return $returnArr;
		else return false;

	}
	function hexToRgb($colorCode){
		$r = hexdec(substr($colorCode,0,2)); 
		$g= hexdec(substr($colorCode,2,2));
		$b = hexdec(substr($colorCode,4,2));
		return array('r'=>$r,'g'=>$g,'b'=>$b);
	}
	
	function rgbToColorCode($r,$g,$b){
		if($r<0)$r=0;
		if($g<0)$g=0;
		if($b<0)$b=0;
		$rHex = dechex($r);
			if(strlen($rHex)==1)$rHex='0'.$rHex;
			
		$gHex = dechex($g);
			if(strlen($gHex)==1)$gHex='0'.$gHex;		
			
		$bHex = dechex($b);
			if(strlen($bHex)==1)$bHex='0'.$bHex;		
			
		return '#'.$rHex.$gHex.$bHex;
	}
	
	function getColorInterval($fromDecimalColor,$toDecimalColor,$df){
	//returns the VALUE TO BE "SUBTRACTED" OR TO BE "ADDED" in interval when
	//we want to GRADIENTLY change from $fromDecimalColor to $toDecimalColor in 256 steps
	
		if($fromDecimalColor==$toDecimalColor){
			return 0;
		}else{
			if($fromDecimalColor>$toDecimalColor){
				$greaterDecimalColor = 'fromDecimalColor';
				$smallerDecimalColor = 'toDecimalColor';
				$isIntervalNegative = true;
			}else{
				$greaterDecimalColor = 'toDecimalColor';
				$smallerDecimalColor = 'fromDecimalColor';
			}
			$interval = (${$greaterDecimalColor}-${$smallerDecimalColor})/$df;
			if($isIntervalNegative){
				$interval = '-'.$interval;
			}
			return $interval;
		}
	}
?>