
<title>True Color Picker: Example</title>
	<script language="javascript" type="text/javascript">
		function popColorPicker(){
			//preload should contain '#' hash, followed by 6 digit hex color code
			preload = '#' + document.getElementById('txtColorCode').value;
			
			//detail factor, see class.TrueColorPicker.php
			df = 64;
			//Modify these
				//Where to pop the color picker window
				_top = 230;
				_left = 220;
				//Where the index.php exists
				path = 'TrueColorPicker/';
			if(preload != ''){
				preload = encodeURIComponent(preload);
			}
			window.open(path + 'index.php?df='+df+'&preload=' + preload, null,'width=420, height=290, top='+_top+', left='+_left+', help=no, status=no, scrollbars=no, resizable=no, dependent=yes,status=no', true);
		}
		function passColorCode(clr){
			document.getElementById('txtColorCode').value = clr;
		}
	</script>
</head>
<body>

<h1>Using Color Picker</h1>
	Your Color Code: #
	<input type="text" id="txtColorCode" size="6" >
	<button type="button" onClick="popColorPicker()">Pop Color Picker</button>
	
<h1>Drawing Color Elements</h1>
	<?
	$colorsArr = array();
	$colorsArr[] = array("Navy","#27187b");
	$colorsArr[] = array("Lime","#b0f64c");
	$colorsArr[] = array("Passion Pink","#f22573");
	$colorsArr[] = array("Aqua","#72fdfa");
	$colorsArr[] = array("Brown","#9f7113");
	include("TrueColorPicker/class.ColorElement.php");
	foreach($colorsArr as $color){
		$cName = $color[0];
		$cCode = $color[1];
		
		$cElem = new ColorElement($cName,$cCode,20,20);//20,20 is width and height
		$cElem->setPath('TrueColorPicker/');
		
		$cElem->draw();
		echo $cElem->colorName;
		echo '<br>';
	}	?>
</body>
</html>
