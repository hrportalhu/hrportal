<?
$pagearray=array();
$pagearray[]=array(	'name' => "mind",
					'text' => "HR Portal - Minden h�r�nk �s cikk�nk",
					'url'  => "http://hrportal.hu/rss/mind.xml",
					'num'  => "1");

$pagearray[]=array(	'name' => "hr-szakmai",
					'text' => "HR Portal - Minden HR szakmai cikk�nk",
					'url'  => "http://hrportal.hu/rss/hr-szakmai.xml",
					'num'  => "2");

$pagearray[]=array(	'name' => "hirek",
					'text' => "Minden h�r�nk (H�rek, C�gh�rek, Magazin)",
					'url'  => "http://hrportal.hu/rss/hirek.xml",
					'num'  => "3");

$pagearray[]=array(	'name' => "hr-informatika",
					'text' => "HR Informatika",
					'url'  => "http://hrportal.hu/rss/hr-informatika.xml",
					'num'  => "4");
					
$pagearray[]=array(	'name' => "munkajog",
					'text' => "Munkajog",
					'url'  => "http://hrportal.hu/rss/munkajog.xml",
					'num'  => "5");

$pagearray[]=array(	'name' => "kafetera",
					'text' => "B�ren kv�li juttat�sok, kafet�ria",
					'url'  => "http://hrportal.hu/rss/berenkivuli-kafeteria.xml",
					'num'  => "6");
					
$pagearray[]=array(	'name' => "trening",
					'text' => "Tr�ning, k�pz�s",
					'url'  => "http://hrportal.hu/rss/trening-kepzes.xml",
					'num'  => "7");
						
$pagearray[]=array(	'name' => "toborzas",
					'text' => "Toborz�s",
					'url'  => "http://hrportal.hu/rss/toborzas.xml",
					'num'  => "8");		
					
$pagearray[]=array(	'name' => "szerv-fejl",
					'text' => "Szervezet-fejleszt�s",
					'url'  => "http://hrportal.hu/rss/szervezet-fejlesztes.xml",
					'num'  => "9");		
					
$pagearray[]=array(	'name' => "kompenzacio",
					'text' => "Kompenz�ci�",
					'url'  => "http://hrportal.hu/rss/kompenzacio.xml",
					'num'  => "10");
					
$pagearray[]=array(	'name' => "munkakor",
					'text' => "Munkak�r",
					'url'  => "http://hrportal.hu/rss/munkakor.xml",
					'num'  => "11");
					
$pagearray[]=array(	'name' => "kommunikacio",
					'text' => "Kommunik�ci�",
					'url'  => "http://hrportal.hu/rss/kommunikacio.xml",
					'num'  => "12");	
					
$pagearray[]=array(	'name' => "teljert",
					'text' => "Teljes�tm�ny-�rt�kel�s",
					'url'  => "http://hrportal.hu/rss/teljesitmeny-ertekeles.xml",
					'num'  => "13");																																		

$pagearray[]=array(	'name' => "elbocsat",
					'text' => "Elbocs�t�s",
					'url'  => "http://hrportal.hu/rss/elbocsatas.xml",
					'num'  => "14");
					
$pagearray[]=array(	'name' => "munkpiac",
					'text' => "Munkaer�piac",
					'url'  => "http://hrportal.hu/rss/munkaeropiac.xml",
					'num'  => "15");
					
$pagearray[]=array(	'name' => "munkavedelem",
					'text' => "Munkav�delem",
					'url'  => "http://hrportal.hu/rss/munkavedelem.xml",
					'num'  => "16");
					
$pagearray[]=array(	'name' => "interim",
					'text' => "Interim-menedzsment",
					'url'  => "http://hrportal.hu/rss/interim-menedzsment.xml",
					'num'  => "17");
					
$pagearray[]=array(	'name' => "hr-controlling",
					'text' => "HR kontrolling",
					'url'  => "http://hrportal.hu/rss/hr-controlling.xml",
					'num'  => "18");	
					
$pagearray[]=array(	'name' => "atipikus",
					'text' => "Atipikus foglalkoztat�s",
					'url'  => "http://hrportal.hu/rss/atipikus_foglalkoztatas.xml",
					'num'  => "19");	
					
$pagearray[]=array(	'name' => "osztonzes-man",
					'text' => "�szt�nz�s-menedzsment",
					'url'  => "http://hrportal.hu/rss/osztonzes_menedzsment.xml",
					'num'  => "20");	

$pagearray[]=array(	'name' => "komp-ert",
					'text' => "Kompetencia-�rt�kel�s",
					'url'  => "http://hrportal.hu/rss/kompetencia_ertekeles.xml",
					'num'  => "21");	

$pagearray[]=array(	'name' => "outsourcing",
					'text' => "Outsourcing",
					'url'  => "http://hrportal.hu/rss/outsourcing.xml",
					'num'  => "22");	
					
$pagearray[]=array(	'name' => "hr-startegia",
					'text' => "HR strat�gia",
					'url'  => "http://hrportal.hu/rss/hr_strategia.xml",
					'num'  => "23");
					
$logoarray=array();
$logoarray[]=array(	'name' => "sotet",
					'txt'  => "S�t�t",
					'path' => "http://www.hrportal.hu/dinamicbox2/logos/hrportal_d.png",
					'width' => "85px",
					'height' => "19px"
					);

$logoarray[]=array(	'name' => "feher",
					'txt'  => "feh�r",
					'path' => "http://www.hrportal.hu/dinamicbox2/logos/hrportal_l.png",
					'width' => "85px",
					'height' => "16px"
					);


$fontarray=array();
$fontarray[]=array(	'name' => "tahoma",
					'txt'  => "Verdana,Tahoma, Arial,sans-serif"
				);
$fontarray[]=array(	'name' => "arial",
					'txt'  => "Arial, Helvetica, sans-serif"
				);	
$fontarray[]=array(	'name' => "platino",
					'txt'  => "Verdana, Palatino, serif"
				);	
$fontarray[]=array(	'name' => "times",
					'txt'  => "georgia, times, serif"
				);

$fontarray[]=array(	'name' => "castellar",
					'txt'  => "castellar"
				);
$fontarray[]=array(	'name' => "courier",
					'txt'  => "Courier"
				);
$fontarray[]=array(	'name' => "ms",
					'txt'  => "arial,ms"
				);				
				
?>
