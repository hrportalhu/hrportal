<?
function readDatabase_newsbox($filename){
    $data = implode("", file($filename));
    $parser = xml_parser_create();
    xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
    xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
    xml_parse_into_struct($parser, $data, $values, $tags);
    xml_parser_free($parser);

    foreach ($tags as $key=>$val){
        if ($key == "item") {
            $elemek = $val;
            for ($i=0; $i < count($elemek); $i+=2) {
                $offset = $elemek[$i] + 1;
                $len = $elemek[$i + 1] - $offset;
                $mvalues=array_slice($values, $offset, $len);
                $elem="";
                for ($h=0; $h < count($mvalues); $h++) {
                	if($mvalues[$h]["tag"]=="pubDate"){
                		$elem['unixtime'] = strtotime(substr($mvalues[$h]["value"],5,-6));
                	}
        			$elem[$mvalues[$h]["tag"]] = $mvalues[$h]["value"];
   				}
               	$tdb[] =$elem; 
            }
        } else {
            continue;
        }
    }
    return $tdb;
}

function named_records_sort($named_recs, $order_by){
	$named_hash = array();
	$checkarray=array();
	foreach($named_recs as $key=>$fields){
		$named_hash["$key"] = $fields[$order_by];
	}
	arsort($named_hash,$flags=0) ;
	$sorted_records = array();
	$checkarry=array();	
	foreach($named_hash as $key=>$val){
		if(!in_array($named_recs[$key]['link'],$checkarry)){
			$sorted_records["$key"]= $named_recs[$key];
			$checkarry[]=$named_recs[$key]['link'];
		}
	}
return $sorted_records;
}

?>
