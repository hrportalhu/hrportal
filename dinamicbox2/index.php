<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="hu">
<head>
   <title>Humánerőforrás / HR Portál | HR Portal - minden a munka világából</title>
    <meta name="description" content="Emberi erőforrás, HR Portál | HR Portal - minden a munka világából. Munkaügyi, vállalati hírek, állások HR-területen, tanulmányok, munkajog-tár, bérkalkulátor, szabadság kalkulátor és sok minden más" />
    <meta name="keywords" content="HR, humánerőforrás, humán erőforrás, emberi erőforrás, human resource, munka, munkaügy, karrier" />
    <meta name="Author" content="" />
    <meta http-equiv="Content-Language" content="hu" />
    <meta name="robots" content="all" />
    <meta name="distribution" content="Global" />

    <meta name="revisit-after" content="2 days" />
    <meta name="rating" content="General" />
    <meta name="doc-type" content="Web Page" /><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

  <link rel="stylesheet" type="text/css" href="format/style.css" />
  <link rel="stylesheet" type="text/css" href="format/style3.css" />
  <title>HR Portal</title>
<script language="JavaScript">
var requestVan = 0;
var pla_ajaxObjects = new Array();

function trimString(sInString) {
// Ez a fuggveny csak legyomlalja egy stringrol a felesleget
  sInString = sInString.replace( /^\s+/g, "" );
  return sInString.replace( /\s+$/g, "" );
}

function Listazz(){
	var xmls=2;
	if(xmls=="1"){
	var pagelista=new Array('mind','hr-szakmai','hirek','hr-informatika');
		pagelista.push('munkajog','kafetera','trening','toborzas');
		pagelista.push('szerv-fejl','kompenzacio','munkakor','kommunikacio');
		pagelista.push('teljert','elbocsat','munkpiac','munkavedelem');
		pagelista.push('interim','hr-controlling','atipikus','osztonzes-man');
		pagelista.push('komp-ert','outsourcing','hr-startegia');
	
	}
	else{
		var pagelista=new Array;
		for(var i=1;i<78;i++){
			pagelista.push('a'+i);
		}		
	}
	
	
	var url="tertlist2.php?go=yes";
	var logos=new Array('logo0','logo1','logo2');
	
	for(var i=0;i<pagelista.length;i++){
		if(document.getElementById(pagelista[i])){
		var mind =document.getElementById(pagelista[i]);
		if(mind.checked==false){}
		else{
			url=url+"&" + pagelista[i] +"=yes";
		}
		}
	}
	
	for(var i=0;i<logos.length;i++){
		var logo =document.getElementById(logos[i]);
		if(logo.checked==false){}
		else{
			var logochoose="&logo="+document.getElementById(logos[i]).value;
		}
	}
	var magas=document.getElementById("magas").value;
	var szeles =document.getElementById("szeles").value;
	var cikknum =document.getElementById("cikknum").value;
	var szerkezet =document.getElementById("szerkezet").value;
	var fejlmagas =document.getElementById("fejlmagas").value;
	var margin =document.getElementById("margin").value;
	var fontf =document.getElementById("fontf").value;
	var fonts =document.getElementById("fonts").value;
	var fontc =document.getElementById("fontc").value;
	var bgc =document.getElementById("bgc").value;
	var keret=document.getElementById("keret").value;
	var borderc=document.getElementById("borderc").value;
	var borderw=document.getElementById("borderw").value;
	var sormag=document.getElementById("sormag").value;
	var cstav=document.getElementById("cstav").value;
	var hfonts=document.getElementById("hfonts").value;
	var hfontc=document.getElementById("hfontc").value;
	var hfontcu=document.getElementById("hfontcu").value;
	var hfontcb=document.getElementById("hfontcb").value;
	var hfontdec=document.getElementById("hfontdec").value;
	//var inwidth=szeles-(borderw*2);
	alert(szeles);
		
	url = url + "&cikknum=" + cikknum + "&szeles=" + inwidth + "&magas=" + magas + "&szerkezet=" + szerkezet+ logochoose + "&fejlmagas=" + fejlmagas + "&margin=" + margin + "&fontf=" +fontf + "&fonts=" +fonts+ "&fontc=" +fontc + "&bgc=" +bgc + "&keret=" +keret + "&borderc=" +borderc + "&borderw=" +borderw;
	url = url + "&sormag=" + sormag + "&cstav=" + cstav + "&hfonts=" + hfonts +"&hfontc=" + hfontc + "&hfontcu=" + hfontcu + "&hfontcb=" + hfontcb + "&hfontdec=" + hfontdec;
 	var cucumucu='<iframe name="childframe" width="' + szeles + '" height="'+ magas +'" style="width:' +
	szeles +'px;height:'+ magas +'px;" src="'+url+'" ';
	var myDiv = document.getElementById('getlista');
	cucumucu = cucumucu + ' scrolling="no" frameborder="0" allowtransparency="1"> </iframe>';
	myDiv.value=cucumucu;

var ajaxIndex = pla_ajaxObjects.length;
	pla_ajaxObjects[ajaxIndex] = new sack();
	var myMegjelenit = document.getElementById('megjelenit');
   	myMegjelenit.innerHTML = "<img src=\"format/loading.gif\" alt=\"töltögetek\" />";
	pla_ajaxObjects[ajaxIndex].requestFile = url;
	pla_ajaxObjects[ajaxIndex].onCompletion = function(){ displayPre(ajaxIndex); };
	pla_ajaxObjects[ajaxIndex].runAJAX();
}

	

function displayPre(ajaxIndex){
    var itemsToBeCreated = new Array();
    var myDiv = document.getElementById('megjelenit');
    myDiv.innerHTML = pla_ajaxObjects[ajaxIndex].response;
    requestVan=0;
}


function sack(file) {
    this.xmlhttp = null;

    this.resetData = function() {
	this.method = "GET";
	this.queryStringSeparator = "?";
	this.argumentSeparator = "&";
	this.URLString = "";
	this.encodeURIString = true;
	this.execute = false;
	this.element = null;
	this.elementObj = null;
	this.requestFile = file;
	this.vars = new Object();
	this.responseStatus = new Array(2);
    };

    this.resetFunctions = function() {
	this.onLoading = function() { };
	this.onLoaded = function() { };
	this.onInteractive = function() { };
	this.onCompletion = function() { };
	this.onError = function() { };
	this.onFail = function() { };
    };

    this.reset = function() {
	this.resetFunctions();
	this.resetData();
    };

    this.createAJAX = function() {
	try {
	    this.xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	} catch (e1) {
	    try {
		this.xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	    } catch (e2) {
		this.xmlhttp = null;
	    }
	}

	if (! this.xmlhttp) {
	    if (typeof XMLHttpRequest != "undefined") {
		this.xmlhttp = new XMLHttpRequest();
	    } else {
		this.failed = true;
	    }
	}
    };

    this.setVar = function(name, value){
	this.vars[name] = Array(value, false);
    };

    this.encVar = function(name, value, returnvars) {
	if (true == returnvars) {
	    return Array(encodeURIComponent(name), encodeURIComponent(value));
	} else {
	    this.vars[encodeURIComponent(name)] = Array(encodeURIComponent(value), true);
	}
    }

    this.processURLString = function(string, encode) {
	encoded = encodeURIComponent(this.argumentSeparator);
	regexp = new RegExp(this.argumentSeparator + "|" + encoded);
	varArray = string.split(regexp);
	for (i = 0; i < varArray.length; i++){
	    urlVars = varArray[i].split("=");
	    if (true == encode){
		this.encVar(urlVars[0], urlVars[1]);
	    } else {
		this.setVar(urlVars[0], urlVars[1]);
	    }
	}
    }

    this.createURLString = function(urlstring) {
	if (this.encodeURIString && this.URLString.length) {
	    this.processURLString(this.URLString, true);
	}

	if (urlstring) {
	    if (this.URLString.length) {
		this.URLString += this.argumentSeparator + urlstring;
	    } else {
		this.URLString = urlstring;
	    }
	}

	// prevents caching of URLString
	this.setVar("rndval", new Date().getTime());

	urlstringtemp = new Array();
	for (key in this.vars) {
	    if (false == this.vars[key][1] && true == this.encodeURIString) {
		encoded = this.encVar(key, this.vars[key][0], true);
		delete this.vars[key];
		this.vars[encoded[0]] = Array(encoded[1], true);
		key = encoded[0];
	    }

	    urlstringtemp[urlstringtemp.length] = key + "=" + this.vars[key][0];
	}
	if (urlstring){
	    this.URLString += this.argumentSeparator + urlstringtemp.join(this.argumentSeparator);
	} else {
	    this.URLString += urlstringtemp.join(this.argumentSeparator);
	}
    }

    this.runResponse = function() {
	eval(this.response);
    }

    this.runAJAX = function(urlstring) {
	if (this.failed) {
	    this.onFail();
	} else {
	    this.createURLString(urlstring);
	    if (this.element) {
		this.elementObj = document.getElementById(this.element);
	    }
	    if (this.xmlhttp) {
		var self = this;
		if (this.method == "GET") {
//		    totalurlstring = this.requestFile + this.queryStringSeparator + this.URLString;
		    totalurlstring = this.requestFile + this.argumentSeparator + this.URLString;
		    this.xmlhttp.open(this.method, totalurlstring, true);
		} else {
		    this.xmlhttp.open(this.method, this.requestFile, true);
		    try {
			this.xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded")
		    } catch (e) { }
		}

		this.xmlhttp.onreadystatechange = function() {
		    switch (self.xmlhttp.readyState) {
			case 1:
			    self.onLoading();
			    break;
			case 2:
			    self.onLoaded();
			    break;
			case 3:
			    self.onInteractive();
			    break;
			case 4:
			    self.response = self.xmlhttp.responseText;
			    self.responseXML = self.xmlhttp.responseXML;
			    self.responseStatus[0] = self.xmlhttp.status;
			    self.responseStatus[1] = self.xmlhttp.statusText;

			    if (self.execute) {
				self.runResponse();
			    }

			    if (self.elementObj) {
				elemNodeName = self.elementObj.nodeName;
				elemNodeName.toLowerCase();
				if (elemNodeName == "input"
				|| elemNodeName == "select"
				|| elemNodeName == "option"
				|| elemNodeName == "textarea") {
				    self.elementObj.value = self.response;
				} else {
				    self.elementObj.innerHTML = self.response;
				}
			    }
			    if (self.responseStatus[0] == "200") {
				self.onCompletion();
			    } else {
				self.onError();
			    }

			    self.URLString = "";
			    break;
		    }
		};

		this.xmlhttp.send(this.URLString);
	    }
	}
    };

    this.reset();
    this.createAJAX();
}
function chalmenu1(){
	document.getElementById("cikk").style.display="block";
	document.getElementById("csatorna").style.display="none";
	document.getElementById("logo").style.display="none";
	document.getElementById("border").style.display="none";
	document.getElementById("sourcea").style.display="none";
	document.getElementById("cikk-tab").style.background="#066";
	document.getElementById("cikk-tab").style.color="#FFF";
	document.getElementById("csatorna-tab").style.background="#fff";
	document.getElementById("csatorna-tab").style.color="#000";
	document.getElementById("border-tab").style.background="#fff";
	document.getElementById("border-tab").style.color="#000";
	document.getElementById("source-tab").style.background="#fff";
	document.getElementById("source-tab").style.color="#000";
	document.getElementById("logo-tab").style.background="#fff";
	document.getElementById("logo-tab").style.color="#000";
	
}

function chalmenu2(){
	document.getElementById("cikk").style.display="none";
	document.getElementById("csatorna").style.display="block";
	document.getElementById("border").style.display="none";
	document.getElementById("logo").style.display="none";
	document.getElementById("sourcea").style.display="none";
	document.getElementById("cikk-tab").style.background="#fff";
	document.getElementById("cikk-tab").style.color="#000";
	document.getElementById("csatorna-tab").style.background="#066";
	document.getElementById("csatorna-tab").style.color="#fff";
	document.getElementById("border-tab").style.background="#fff";
	document.getElementById("border-tab").style.color="#000";
	document.getElementById("source-tab").style.background="#fff";
	document.getElementById("source-tab").style.color="#000";
	document.getElementById("logo-tab").style.background="#fff";
	document.getElementById("logo-tab").style.color="#000";
}

function chalmenu3(){
	document.getElementById("cikk").style.display="none";
	document.getElementById("csatorna").style.display="none";
	document.getElementById("border").style.display="block";
	document.getElementById("sourcea").style.display="none";
	document.getElementById("logo").style.display="none";
	document.getElementById("cikk-tab").style.background="#fff";
	document.getElementById("cikk-tab").style.color="#000";
	document.getElementById("csatorna-tab").style.background="#fff";
	document.getElementById("csatorna-tab").style.color="#000";
	document.getElementById("border-tab").style.background="#066";
	document.getElementById("border-tab").style.color="#fff";
	document.getElementById("source-tab").style.background="#fff";
	document.getElementById("source-tab").style.color="#000";
	document.getElementById("logo-tab").style.background="#fff";
	document.getElementById("logo-tab").style.color="#000";
}

function chalmenu4(){
	document.getElementById("cikk").style.display="none";
	document.getElementById("csatorna").style.display="none";
	document.getElementById("border").style.display="none";
	document.getElementById("sourcea").style.display="none";
	document.getElementById("logo").style.display="block";
	document.getElementById("cikk-tab").style.background="#fff";
	document.getElementById("cikk-tab").style.color="#000";
	document.getElementById("csatorna-tab").style.background="#fff";
	document.getElementById("csatorna-tab").style.color="#000";
	document.getElementById("border-tab").style.background="#fff";
	document.getElementById("border-tab").style.color="#000";
	document.getElementById("source-tab").style.background="#fff";
	document.getElementById("source-tab").style.color="#000";
	document.getElementById("logo-tab").style.background="#066";
	document.getElementById("logo-tab").style.color="#fff";
}
function chalmenu5(){
	document.getElementById("cikk").style.display="none";
	document.getElementById("csatorna").style.display="none";
	document.getElementById("border").style.display="none";
	document.getElementById("logo").style.display="none";
	document.getElementById("sourcea").style.display="block";
	document.getElementById("cikk-tab").style.background="#fff";
	document.getElementById("cikk-tab").style.color="#000";
	document.getElementById("csatorna-tab").style.background="#fff";
	document.getElementById("csatorna-tab").style.color="#000";
	document.getElementById("border-tab").style.background="#fff";
	document.getElementById("border-tab").style.color="#000";
	document.getElementById("source-tab").style.background="#066";
	document.getElementById("source-tab").style.color="#fff";
	document.getElementById("logo-tab").style.background="#fff";
	document.getElementById("logo-tab").style.color="#000";
}
var elem;
		function popColorPicker(elem){
			//preload should contain '#' hash, followed by 6 digit hex color code
			preload = '#' + document.getElementById(elem).value;
			
			//detail factor, see class.TrueColorPicker.php
			df = 64;
			//Modify these
				//Where to pop the color picker window
				_top = 230;
				_left = 220;
				//Where the index.php exists
				path = 'TrueColorPicker/';
			if(preload != ''){
				preload = encodeURIComponent(preload);
			}
			window.open(path + 'index.php?df='+df+'&preload=' + preload+'&elem=' +elem, null,'width=450, height=320, top='+_top+', left='+_left+', help=no, status=no, scrollbars=no, resizable=no, dependent=yes,status=no', true);
		}
		function passColorCode(clr,elem){
			document.getElementById(elem).value = clr;
		}
</script>
<?
include("include/functions.php");
include("include/codedir.php");
echo"</head>";
echo"<body onload=\"Listazz();\">";
echo"<form>";
echo"<div id='mainmenu'>";
echo"<ul id='tabs'>";
echo"<li class='empty'>&nbsp;</li>";
echo"<li><a href='#' id='cikk-tab'  onclick='chalmenu1()' class='active-tab'>Cikk be&aacute;ll&iacute;t&aacute;sok</a></li>";
echo"<li class='empty'>&nbsp;</li>";
echo"<li><a href='#' id='csatorna-tab' onclick='chalmenu2()'>Csatorn&aacute;k</a></li>";
echo"<li class='empty'>&nbsp;</li>";
echo"<li><a href='#' id='border-tab' onclick='chalmenu3()'>Keretek</a></li>";
echo"<li class='empty'>&nbsp;</li>";
echo"<li><a href='#' id='logo-tab' onclick='chalmenu4()'>Logók</a></li>";
echo"<li class='empty'>&nbsp;</li>";
echo"<li><a href='#' id='source-tab' onclick='chalmenu5()'>Beillesztend&#x0151; forr&aacute;sk&oacute;d</a></li>";
echo"</ul>";
echo"</div>";

//line-height: 19px; font-size: 15px; font-weight: bold; font-family: Arial, Helvetica, sans-serif; color: #141414; background: #ffffff ; 
echo"<div class='panel' id='cikk' style='display:block'>";
echo"<label for='cikknum'>Cikkek száma</label><input type='text' name='cikknum' id='cikknum' value='6' onKeyup='javascript:Listazz();'><br>";
echo"<label for='margin'>Margó</label><input type='text' name='margin' id='margin' value='3' onKeyup='javascript:Listazz();'><br>";
echo"<label for='fontf'>Betütípus</label><select name='fontf' id='fontf' onChange='javascript:Listazz();'>";
for($i=0;$i<count($fontarray);$i++){
	$dd="";
	if($i==0){
		$dd="selected";
	}
	echo"<option value='".$fontarray[$i]['name']."' $dd style='font-family:".$fontarray[$i]['txt'].";'>".$fontarray[$i]['txt']."</option>";
}
echo"</select><br>";
echo"<label for='fonts'>Betüméret</label><input type='text' name='fonts' id='fonts' value='12' onKeyup='javascript:Listazz();'><br>";
echo"<label for='fontc'>Betüszin /#/</label><input type='text' name='fontc' id='fontc' value='000000' onKeyup='javascript:Listazz();'><button type=\"button\" onClick=\"popColorPicker('fontc')\">Válasszon szint</button><br>";
echo"<label for='sormag'>Sormagasság</label><input type='text' name='sormag' id='sormag' value='12' onKeyup='javascript:Listazz();'><br>";
echo"<label for='cstav'>Cimsor tartalom távolság</label><input type='text' name='cstav' id='cstav' value='12' onKeyup='javascript:Listazz();'><br>";
echo"<label for='hfonts'>Címsor betüméret</label><input type='text' name='hfonts' id='hfonts' value='12' onKeyup='javascript:Listazz();'><br>";
echo"<label for='hfontc'>Címsor betüszin /#/</label><input type='text' name='hfontc' id='hfontc' value='000000' onKeyup='javascript:Listazz();'><button type=\"button\" onClick=\"popColorPicker('hfontc')\">Válasszon szint</button><br>";
echo"<label for='hfontcu'>Címsor betüszin felette/#/</label><input type='text' name='hfontcu' id='hfontcu' value='000000' onKeyup='javascript:Listazz();'><button type=\"button\" onClick=\"popColorPicker('hfontcu')\">Válasszon szint</button><br>";
echo"<label for='hfontcb'>Címsor betüforma</label>
<select name='hfontcb' id='hfontcb' onChange='javascript:Listazz();'>
<option value='normal' selected>Normál</option>
<option value='bold'>Félkövér</option>
</select>
<br>";
echo"<label for='hfontdec'>Címsor dekoráció</label>
<select name='hfontdec' id='hfontdec' onChange='javascript:Listazz();'>
<option value='none' selected>Nincs dekoráció</option>
<option value='underline'>Aláhuzott</option>
<option value='overline'>Felülvonal</option>
</select>
<br>";

echo"<label for='szerkezet'>Szerkezet</label>
<select name='szerkezet' id='szerkezet' onChange='javascript:Listazz();'>
<option value='withbody'>Címsor és tartalom</option>
<option value='withoutbody'>Csak Címsor</option>
</select>
<br>";
echo"</div>";
$xmls=2;
if($xmls=="1"){
	echo"<div class='panel' id='csatorna' style='display:none'>";
	for($i=0;$i<count($pagearray);$i++){
		if($i==0){
			echo"<label for='".$pagearray[$i]['name']."' >".$pagearray[$i]['text']."</label><input type='checkbox' name='".$pagearray[$i]['name']."' id='".$pagearray[$i]['name']."' onClick='javascript:Listazz();' checked><br/>";
		}
		else{
			echo"<label for='".$pagearray[$i]['name']."' >".$pagearray[$i]['text']."</label><input type='checkbox' name='".$pagearray[$i]['name']."' id='".$pagearray[$i]['name']."' onClick='javascript:Listazz();'><br/>";
			
		}
	}
	echo"</div>";
}
else{
	echo"<div class='panel' id='csatorna' style='display:none'>";
	$kategs=db_all("select id, name from categories order by name");
	for($i=0;$i<count($kategs);$i++){
		if($i==0){
			echo"<label for='a".$kategs[$i]['id']."' >".$kategs[$i]['name']."</label><input type='checkbox' name='a".$kategs[$i]['name']."' id='a".$kategs[$i]['id']."' onClick='javascript:Listazz();' checked><br/>";
		}
		else{
			echo"<label for=a'".$kategs[$i]['id']."' >".$kategs[$i]['name']."</label><input type='checkbox' name='a".$kategs[$i]['name']."' id='a".$kategs[$i]['id']."' onClick='javascript:Listazz();'><br/>";
			
		}
	}
	echo"</div>";
}

echo"<div class='panel' id='border' style='display:none'>";
echo"<label for='szeles'>Szélesség</label><input type='text' name='szeles' id='szeles' value='300' onKeyup='javascript:Listazz();'><br>";
echo"<label for='magas'>Magasság</label><input type='text' name='magas' id='magas' value='250' onKeyup='javascript:Listazz();'><br>";
echo"<label for='bgc'>Háttérszin /#/</label><input type='text' name='bgc' id='bgc' value='ffffff' onKeyup='javascript:Listazz();'><button type=\"button\" onClick=\"popColorPicker('bgc')\">Válasszon szint</button><br>";
echo"<label for='keret' >Keret típusa</label><select name='keret' id='keret' onChange='javascript:Listazz();'>";
echo"<option value='noborder' selected>Nem kérek keretet</option>";
echo"<option value='cornerborder'>Szögletes keret</option>";
echo"<option value='smrborder'>Kis sugarú keret</option>";
echo"<option value='bigborder'>Nagy sugarú keret</option>";
echo"</select><br />";
echo"<label for='borderc'>Keretszin /#/</label><input type='text' name='borderc' id='borderc' value='ffffff' onKeyup='javascript:Listazz();'><button type=\"button\" onClick=\"popColorPicker('borderc')\">Válasszon szint</button><br>";
echo"<label for='borderw'>Keret vastagság</label><input type='text' name='borderw' id='borderw' value='1' onKeyup='javascript:Listazz();'><br>";

echo"<br /></div>";

echo"<div class='panel' id='logo' style='display:none'>";
for($i=0;$i<count($logoarray);$i++){
	if($logoarray[$i]['name']=="feher"){
		echo"<div style='background-color:#333;min-height:36px;width:350px;'><label for='".$logoarray[$i]['name']."' class='magas'>".$logoarray[$i]['txt']."</label><input type='radio' name='logo' id='logo".$i."' value='".$logoarray[$i]['name']."' onClick='javascript:Listazz();' value=".$logoarray[$i]['name']."><img src='".$logoarray[$i]['path']."' width='".$logoarray[$i]['width']."' height='".$logoarray[$i]['height']."'></div>";
	}
	else{
		echo"<div style='min-height:36px;width:350px;'><label for='".$logoarray[$i]['name']."' class='magas'>".$logoarray[$i]['txt']."</label><input type='radio' name='logo' id='logo".$i."' value='".$logoarray[$i]['name']."' onClick='javascript:Listazz();' value=".$logoarray[$i]['name']."><img src='".$logoarray[$i]['path']."' width='".$logoarray[$i]['width']."' height='".$logoarray[$i]['height']."'></div>";
	}
}
echo"<label for='sima' class='magas'>Nem kérek logót</label><input type='radio' checked name='logo' id='logo2' onClick='javascript:Listazz();' value='simaszoveg'><br>";
echo"<label for='fejlmagas'>Fejléc magasság</label><input type='text' name='fejlmagas' id='fejlmagas' value='30' onKeyup='javascript:Listazz();'><br>";
echo"</div>";


echo"<div class='panel' id='sourcea' style='display:none'>";
echo"<label for='getlista'>Eredmény</label><br>";
echo"<textarea name='getlista' id='getlista' style='width:400px;height:150px;'></textarea>";
echo"</div>";
echo"</form>";

echo"<h2>El&#x0151;n&eacute;zet</h2>";
echo"<div id='megjelenit'>";
echo"</div>";
