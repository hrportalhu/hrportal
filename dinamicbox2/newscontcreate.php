<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="hu">
<head>
   <title>Hum�ner�forr�s / HR Port�l | HR Portal - minden a munka vil�g�b�l</title>
    <meta name="description" content="Emberi er�forr�s, HR Port�l | HR Portal - minden a munka vil�g�b�l. Munka�gyi, v�llalati h�rek, �ll�sok HR-ter�leten, tanulm�nyok, munkajog-t�r, b�rkalkul�tor, szabads�g kalkul�tor �s sok minden m�s" />
    <meta name="keywords" content="HR, hum�ner�forr�s, hum�n er�forr�s, emberi er�forr�s, human resource, munka, munka�gy, karrier" />
    <meta name="Author" content="" />
    <meta http-equiv="Content-Language" content="hu" />
    <meta name="robots" content="all" />
    <meta name="distribution" content="Global" />

    <meta name="revisit-after" content="2 days" />
    <meta name="rating" content="General" />
    <meta name="doc-type" content="Web Page" /><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-2">

  <link rel="stylesheet" type="text/css" href="format/style.css" />
  <link rel="stylesheet" type="text/css" href="format/style2.css" />
  <title>HR Portal</title>
<script language="JavaScript">
var requestVan = 0;
var pla_ajaxObjects = new Array();
// Ez azert tomb, mert ugye elkepzelheto, hogy nem egy AJAX-os mezo lesz
// a kepernyon, es a tomb egy eleme fog azonositani egy AJAX-os kapcsolatot

function trimString(sInString) {
// Ez a fuggveny csak legyomlalja egy stringrol a felesleget
  sInString = sInString.replace( /^\s+/g, "" );
  return sInString.replace( /\s+$/g, "" );
}

function Listazz(){
	var pagelista=new Array('mind','hr-szakmai','hirek','hr-informatika');
		pagelista.push('munkajog','kafetera','trening','toborzas');
		pagelista.push('szerv-fejl','kompenzacio','munkakor','kommunikacio');
		pagelista.push('teljert','elbocsat','munkpiac','munkavedelem');
		pagelista.push('interim','hr-controlling','atipikus','osztonzes-man');
		pagelista.push('komp-ert','outsourcing','hr-startegia');

	var url="tertlist2.php?go=yes";
	var logos=new Array('logo0','logo1','logo2','logo3');
	
	for(var i=0;i<pagelista.length;i++){
		var mind =document.getElementById(pagelista[i]);
		if(mind.checked==false){}
		else{
			url=url+"&" + pagelista[i] +"=yes";
		}
	}
	
	for(var i=0;i<logos.length;i++){
		var logo =document.getElementById(logos[i]);
		if(logo.checked==false){}
		else{
			var logochoose="&logo="+document.getElementById(logos[i]).value;
		}
	}
	var magas=document.getElementById("magas").value;
	var szeles =document.getElementById("szeles").value;
	var cikknum =document.getElementById("cikknum").value;
	var szerkezet =document.getElementById("szerkezet").value;
		var fejlmagas =document.getElementById("fejlmagas").value;
	
	url = url + "&cikknum=" + cikknum + "&szeles=" + szeles + "&magas=" + magas + "&szerkezet=" + szerkezet+ logochoose + "&fejlmagas=" + fejlmagas;
	var cucumucu='<iframe name="childframe" width="' + szeles + '" height="'+ magas +'" style="width:' +
	szeles +'px;height:'+ magas +'px;" src="'+url+'" ';
	var myDiv = document.getElementById('getlista');
	cucumucu = cucumucu + ' scrolling="no" frameborder="0" allowtransparency="1"> </iframe>';
	myDiv.value=cucumucu;

var ajaxIndex = pla_ajaxObjects.length;
	pla_ajaxObjects[ajaxIndex] = new sack();
	var myMegjelenit = document.getElementById('megjelenit');
   	myMegjelenit.innerHTML = "<img src=\"format/loading.gif\" alt=\"t�lt�getek\" />";
	pla_ajaxObjects[ajaxIndex].requestFile = url;
	pla_ajaxObjects[ajaxIndex].onCompletion = function(){ displayPre(ajaxIndex); };
	pla_ajaxObjects[ajaxIndex].runAJAX();
}

	

function displayPre(ajaxIndex){
    var itemsToBeCreated = new Array();
    var myDiv = document.getElementById('megjelenit');
    myDiv.innerHTML = pla_ajaxObjects[ajaxIndex].response;
    requestVan=0;
}


function sack(file) {
    this.xmlhttp = null;

    this.resetData = function() {
	this.method = "GET";
	this.queryStringSeparator = "?";
	this.argumentSeparator = "&";
	this.URLString = "";
	this.encodeURIString = true;
	this.execute = false;
	this.element = null;
	this.elementObj = null;
	this.requestFile = file;
	this.vars = new Object();
	this.responseStatus = new Array(2);
    };

    this.resetFunctions = function() {
	this.onLoading = function() { };
	this.onLoaded = function() { };
	this.onInteractive = function() { };
	this.onCompletion = function() { };
	this.onError = function() { };
	this.onFail = function() { };
    };

    this.reset = function() {
	this.resetFunctions();
	this.resetData();
    };

    this.createAJAX = function() {
	try {
	    this.xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	} catch (e1) {
	    try {
		this.xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	    } catch (e2) {
		this.xmlhttp = null;
	    }
	}

	if (! this.xmlhttp) {
	    if (typeof XMLHttpRequest != "undefined") {
		this.xmlhttp = new XMLHttpRequest();
	    } else {
		this.failed = true;
	    }
	}
    };

    this.setVar = function(name, value){
	this.vars[name] = Array(value, false);
    };

    this.encVar = function(name, value, returnvars) {
	if (true == returnvars) {
	    return Array(encodeURIComponent(name), encodeURIComponent(value));
	} else {
	    this.vars[encodeURIComponent(name)] = Array(encodeURIComponent(value), true);
	}
    }

    this.processURLString = function(string, encode) {
	encoded = encodeURIComponent(this.argumentSeparator);
	regexp = new RegExp(this.argumentSeparator + "|" + encoded);
	varArray = string.split(regexp);
	for (i = 0; i < varArray.length; i++){
	    urlVars = varArray[i].split("=");
	    if (true == encode){
		this.encVar(urlVars[0], urlVars[1]);
	    } else {
		this.setVar(urlVars[0], urlVars[1]);
	    }
	}
    }

    this.createURLString = function(urlstring) {
	if (this.encodeURIString && this.URLString.length) {
	    this.processURLString(this.URLString, true);
	}

	if (urlstring) {
	    if (this.URLString.length) {
		this.URLString += this.argumentSeparator + urlstring;
	    } else {
		this.URLString = urlstring;
	    }
	}

	// prevents caching of URLString
	this.setVar("rndval", new Date().getTime());

	urlstringtemp = new Array();
	for (key in this.vars) {
	    if (false == this.vars[key][1] && true == this.encodeURIString) {
		encoded = this.encVar(key, this.vars[key][0], true);
		delete this.vars[key];
		this.vars[encoded[0]] = Array(encoded[1], true);
		key = encoded[0];
	    }

	    urlstringtemp[urlstringtemp.length] = key + "=" + this.vars[key][0];
	}
	if (urlstring){
	    this.URLString += this.argumentSeparator + urlstringtemp.join(this.argumentSeparator);
	} else {
	    this.URLString += urlstringtemp.join(this.argumentSeparator);
	}
    }

    this.runResponse = function() {
	eval(this.response);
    }

    this.runAJAX = function(urlstring) {
	if (this.failed) {
	    this.onFail();
	} else {
	    this.createURLString(urlstring);
	    if (this.element) {
		this.elementObj = document.getElementById(this.element);
	    }
	    if (this.xmlhttp) {
		var self = this;
		if (this.method == "GET") {
//		    totalurlstring = this.requestFile + this.queryStringSeparator + this.URLString;
		    totalurlstring = this.requestFile + this.argumentSeparator + this.URLString;
		    this.xmlhttp.open(this.method, totalurlstring, true);
		} else {
		    this.xmlhttp.open(this.method, this.requestFile, true);
		    try {
			this.xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded")
		    } catch (e) { }
		}

		this.xmlhttp.onreadystatechange = function() {
		    switch (self.xmlhttp.readyState) {
			case 1:
			    self.onLoading();
			    break;
			case 2:
			    self.onLoaded();
			    break;
			case 3:
			    self.onInteractive();
			    break;
			case 4:
			    self.response = self.xmlhttp.responseText;
			    self.responseXML = self.xmlhttp.responseXML;
			    self.responseStatus[0] = self.xmlhttp.status;
			    self.responseStatus[1] = self.xmlhttp.statusText;

			    if (self.execute) {
				self.runResponse();
			    }

			    if (self.elementObj) {
				elemNodeName = self.elementObj.nodeName;
				elemNodeName.toLowerCase();
				if (elemNodeName == "input"
				|| elemNodeName == "select"
				|| elemNodeName == "option"
				|| elemNodeName == "textarea") {
				    self.elementObj.value = self.response;
				} else {
				    self.elementObj.innerHTML = self.response;
				}
			    }
			    if (self.responseStatus[0] == "200") {
				self.onCompletion();
			    } else {
				self.onError();
			    }

			    self.URLString = "";
			    break;
		    }
		};

		this.xmlhttp.send(this.URLString);
	    }
	}
    };

    this.reset();
    this.createAJAX();
}


</script>
<?
include("include/functions.php");
include("include/codedir.php");


echo"<h2>Csatorn�k</h2>";
echo"<form>";
for($i=0;$i<count($pagearray);$i++){
	echo"<label for='".$pagearray[$i]['name']."' >".$pagearray[$i]['text']."</label><input type='checkbox' name='".$pagearray[$i]['name']."' id='".$pagearray[$i]['name']."' onClick='javascript:Listazz();'><br/>";
}
echo"<h2>Megjelen�s</h2>";
for($i=0;$i<count($logoarray);$i++){
	echo"<label for='".$logoarray[$i]['name']."' class='magas'>".$logoarray[$i]['txt']."</label><input type='radio' name='logo' id='logo".$i."' value='".$logoarray[$i]['name']."' onClick='javascript:Listazz();' value=".$logoarray[$i]['name']."><img src='".$logoarray[$i]['path']."'><br>";
}
echo"<label for='sima' class='magas'>Nem k�rek log�t</label><input type='radio' checked name='logo' id='logo3' onClick='javascript:Listazz();' value='simaszoveg'><br>";
echo"<label for='fejlmagas'>Fejl�c magass�g</label><input type='text' name='fejlmagas' id='fejlmagas' value='30' onKeyup='javascript:Listazz();'><br>";

echo"<label for='szeles'>Sz�less�g</label><input type='text' name='szeles' id='szeles' value='300' onKeyup='javascript:Listazz();'><br>";
echo"<label for='magas'>Magass�g</label><input type='text' name='magas' id='magas' value='250' onKeyup='javascript:Listazz();'><br>";
echo"<h2>Egyebek</h2>";
echo"<label for='cikknum'>Cikkek sz�ma</label><input type='text' name='cikknum' id='cikknum' value='6' onKeyup='javascript:Listazz();'><br>";
echo"<label for='szerkezet'>Szerkezet</label>
<select name='szerkezet' id='szerkezet' onChange='javascript:Listazz();'>
<option value='withbody'>C�msor �s tartalom</option>
<option value='withoutbody'>Csak C�msor</option>
</select>
<br>";
echo"</form>";
echo"</div>";

echo"<div id='tartcontent'>";
echo"<label for='getlista'>Eredm�ny</label><br>";
echo"<textarea name='getlista' id='getlista' style='width:400px;height:150px;'></textarea>";
echo"</div>";

echo"<h2>El&#x0151;n&eacute;zet</h2>";
echo"<div id='megjelenit'>";
echo"</div>";

