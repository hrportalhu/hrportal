//----------------------------------------------------------
//                       _        _                               _
//  _ _ ___ _  _ _ _  __| |___ __| |__ ___ _ _ _ _  ___ _ _ ___  (_)___
// | '_/ _ \ || | ' \/ _` / -_) _` / _/ _ \ '_| ' \/ -_) '_(_-<_ | (_-<
// |_| \___/\_,_|_||_\__,_\___\__,_\__\___/_| |_||_\___|_| /__(_)/ /__/
//                                                             |__/
//
//----------------------------------------------------------
// File      : roundedcorners.js
// Author    : Richard Lewis
// Project   : /mnt/web/localhost/htdocs/js/rounded_corners/
// Syntax    : javascript
// Date      : Fri 08 Feb 2008
// Copyright : Richard Lewis 2008
//----------------------------------------------------------
// roundedcorners.js - roundedcorners
//----------------------------------------------------------

function goRoundedCorners() {
    // onload

    roundCorners();
}
//----------------------------------------------------------

function roundCorners() {
    // round corners

    var eles = [
        'jsrounded0',
        'jsrounded1']

    for (var c = 0; c < eles.length; ++c) {

        var ele = getEle(eles[c]);

        var width;
        var bgcolor;

        // get element width
        if (window.getComputedStyle)
            width =
                window.getComputedStyle(
                    ele, null).width;

        else if (ele.currentStyle) 
            width =
                ele.currentStyle.width;
        else
            continue

        // get element background colour
        if (window.getComputedStyle)
            bgcolor =
                window.getComputedStyle(
                    ele, null).backgroundColor;

        else if (ele.currentStyle) 
            bgcolor =
                ele.currentStyle.backgroundColor;

        else
            continue

        width  = parseInt(width) + 16;
        width += "px";

        ele.style.margin  = "0px auto";

        topRow(ele, bgcolor, width);

        bottomRow(ele, bgcolor, width);
    }
}
//----------------------------------------------------------

function topRow(ele, bgcolor, width) {
    // top row

    var div1 = crtEle('div');
    var div2 = crtEle('div');
    var div3 = crtEle('div');

    div1.className = "top";
    div2.className = "topleft";
    div3.className = "topright";

    div1.style.width = width;

    div1.style.backgroundColor = bgcolor;

    div1.appendChild(div2);
    div1.appendChild(div3);

    ele.parentNode.insertBefore(div1, ele);
}
//----------------------------------------------------------

function bottomRow(ele, bgcolor, width) {
    // bottom row

    var div1 = crtEle('div');
    var div2 = crtEle('div');
    var div3 = crtEle('div');

    div1.className = "bottom";
    div2.className = "bottomleft";
    div3.className = "bottomright";

    div1.style.width = width;

    div1.style.backgroundColor = bgcolor;

    div1.appendChild(div2);
    div1.appendChild(div3);

    if (ele.nextSibling)
        ele.parentNode.insertBefore(div1, ele.nextSibling);
    else
        ele.parentNode.appendChild(div1);
}
//----------------------------------------------------------

function getEle(id) {
    // get document element by id

    if (document.getElementById(id))
        return document.getElementById(id);
    return false;
}
//----------------------------------------------------------

function crtEle(tag) {
    // create a document element from a tag

    if (document.createElement(tag))
        return document.createElement(tag);
    return false;
}
//----------------------------------------------------------

function crtTxt(txt) {
    // create textnode

    if (document.createTextNode)
        return document.createTextNode(txt);
    return false;
}
//----------------------------------------------------------

function nbsp(count) {
    // non breaking space

    var spc = "\u00a0";
    var spaces = new Array();

    for (var k = 0; k < count; ++k)
        spaces.push(spc);

    return crtTxt(spaces.join(""));
}
//----------------------------------------------------------

function br() {
    // return a line break

    var lb = crtEle('br');

    return lb;
}
//----------------------------------------------------------

function hr() {
    // return a hr

    var div = crtEle('div');

    div.className = "hr";

    return div
}
//----------------------------------------------------------

function clrEle(ele) {
    // clear element

    while(ele.firstChild)
        ele.removeChild(ele.firstChild);
}
//----------------------------------------------------------

if (window.addEventListener)
    window.addEventListener("load", goRoundedCorners, false);

else if (window.attachEvent)
    window.attachEvent("onload", goRoundedCorners);
//----------------------------------------------------------
