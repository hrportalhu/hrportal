//----------------------------------------------------------
//  _         _                 _ _  __ _        _    _
// | |__ _ __| |_ _ __  ___  __| (_)/ _(_)___ __| |  (_)___
// | / _` (_-<  _| '  \/ _ \/ _` | |  _| / -_) _` |_ | (_-<
// |_\__,_/__/\__|_|_|_\___/\__,_|_|_| |_\___\__,_(_)/ /__/
//                                                 |__/
//
//----------------------------------------------------------
// File      : lastmodified.js
// Author    : Richard Lewis
// Project   : /mnt/web/localhost/htdocs/js/rounded_corners/
// Syntax    : javascript
// Date      : Fri 08 Feb 2008
// Copyright : Richard Lewis 2008
//----------------------------------------------------------
// lastmodified.js - rounded corners
//----------------------------------------------------------

function goLastModified() {
    // onload

    lastModified();
}
//----------------------------------------------------------

function lastModified() {
    // add last modified date to page

    var modified = getEle('lastmodified');

    var now = new Date();
    var mod = new Date(document.lastModified);
    var dif = Math.floor(
        (now.getTime()  -
         mod.getTime()) /
        (1000 * 60 * 60 * 24));

    if (dif < 0) dif = 0;

    var day   = getDay(mod.getDay());
    var mday  = mod.getDate();
    var mord  = ord(mday);
    var month = getMonth(mod.getMonth());
    var year  = getYear(mod.getYear());

    var lastMod1 =
        "Last Modified : " +
        day  + ", "        +
        mday;
    modified.appendChild(crtTxt(lastMod1));

    var sup = crtEle('sup');
    sup.appendChild(crtTxt(mord));
    modified.appendChild(sup);

    var lastMod2 = " "  + month + " " + year;
    modified.appendChild(crtTxt(lastMod2));

    var lastMod3 = " (" + dif + " days ago).";
    modified.appendChild(crtTxt(lastMod3));
}
//----------------------------------------------------------

function ord(num) {
    // return ordinal of number

    var ordA = new Array('th', 'st', 'nd', 'rd');

    if (num >= 10 && num <= 20) i = 0;
    else i = num % 10;

    if (i > 3) i = 0;

    return ordA[i];
}
//----------------------------------------------------------

function getDay(day) {
    // get day str

    var dayA = new Array(
        'Sun', 'Mon', 'Tue',
        'Wed', 'Thu', 'Fri', 'Sat');

    return dayA[day];
}
//----------------------------------------------------------

function getMonth(month) {
    // get month str

    var monthA = new Array(
        'Jan', 'Feb', 'Mar',
        'Apr', 'May', 'Jun',
        'Jul', 'Aug', 'Sep',
        'Oct', 'Nov', 'Dec');

    return monthA[month];
}
//----------------------------------------------------------

function getYear(year) {
    // return full year

    if (year < 2000)
        return year + 1900;

    return year;
}
//----------------------------------------------------------

if (window.addEventListener)
    window.addEventListener("load", goLastModified, false);

else if (window.attachEvent)
    window.attachEvent("onload", goLastModified);
//----------------------------------------------------------
