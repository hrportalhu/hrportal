﻿<?php
include("../inc/functions.php");	
    $formType = $_POST["formType"];
    $MailTo = 'jozsef.szucs@planetstar.hu';

    $txtName = $_POST["txtName"];
    $txtEmail = $_POST["txtEmail"];
    $txtRating = "";
    $txtWebsite = "";
    $txtMessage = $_POST["txtMessage"];
    
    if ($formType == "comments") {
        $txtWebsite = $_POST["txtWebsite"];
    }
    
    if ($formType == "reviews") {
        $txtRating = $_POST["txtRating"];
    }
    
    $success = TRUE;    
    
    try{
        if(trim($txtEmail)==NULL){
            throw new Exception("Please enter your e-mail id.");
        }
        else
        {
            if (!preg_match("/\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/", trim($txtEmail)))
            {
                throw new Exception("Please enter the valid e-mail.");
            }
        }

        if(trim($txtName)==NULL){
            throw new Exception("Please enter your name.");
        }
    
        if(trim($txtMessage)==NULL){
            throw new Exception("Please enter your message.");
        }
    
        if($formType == "contact") {   
            $MailSubject = "Új jelentkezés a HRPiknikre";
            $MailBody = "<strong>Email: </strong> ".$txtEmail."<br />";
            $MailBody .= "<strong>Név: </strong> ".$txtName."<br />";
            $MailBody .= "<strong>Telefonszám: </strong> ".$_POST['txtphone']."<br />";
            $MailBody .= "<strong>Számlázási név: </strong> ".$_POST['txtszlaname']."<br />";
            $MailBody .= "<strong>Számlázási cím: </strong> ".$_POST['txtszlaaddr']."<br />";
            $MailBody .= "<strong>Üzenet: </strong> ".$txtMessage."<br />";
            
            
            $MailSubject2= "Köszönjük jelentkezését a HRPIKNIK-re";
            $MailBody2 = "Ezekkel az adatokkal jelentkezett:<br /><strong>Email: </strong> ".$txtEmail."<br />";
            $MailBody2 .= "<strong>Név: </strong> ".$txtName."<br />";
            $MailBody2 .= "<strong>Telefonszám: </strong> ".$_POST['txtphone']."<br />";
            $MailBody2 .= "<strong>Számlázási név: </strong> ".$_POST['txtszlaname']."<br />";
            $MailBody2 .= "<strong>Számlázási cím: </strong> ".$_POST['txtszlaaddr']."<br />";
            $MailBody2 .= "<strong>Üzenet: </strong> ".$txtMessage."<br />";
            $MailBody2 .= "<strong>Fizetendő összeg: </strong> 19 500 Ft +ÁFA/fő<br />";
            $MailBody2 .= "";
            
            
            
            
        }

     
        $MailFrom = $txtEmail;
        $MailHeaders = 'MIME-Version: 1.0' . "\r\n";
        $MailHeaders .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $MailHeaders .= "From: <info@hrportal.hu>"."\r\n";
        $result =  mail($MailTo, $MailSubject, $MailBody, $MailHeaders);
    
        if(!$result){
            throw new Exception("Mail was not sent :(");
        }
    
        $success = TRUE;
    }
    catch (Exception $e){
        header("Status: 400 Bad Request", TRUE, 400);
        echo($e->getMessage());
        $success = FALSE;
    }
    
    if ($success)
    {
        header("Status: 200 OK", TRUE, 200);    
        // add your custom success message here
        echo("Mail sent successfully :)");
        db_execute("insert into workshop_landing_2015 (nev1,cegnev,szlacim,postcim,email,telefon,megjegyzes,sessid,regdate) 
		values('".txtName."','".$_POST['cegnev']."','".$_POST['szlacim']."','".$_POST['postcim']."','".$_POST['email']."','".$_POST['telefon']."','".$_POST['megjegyzes']."','".session_id()."',NOW())");
	
    }
    else
    {
        header("Status: 400 Bad Request", TRUE, 400);
    }
?>
