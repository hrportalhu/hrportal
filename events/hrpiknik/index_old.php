﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="iso-8859-2">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>HRPIKNIK</title>

    <!-- ## Bootstrap ## -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="assets/css/supersized.css" rel="stylesheet" />
    <link id="skin-styler" href="assets/css/skin1.min.css" rel="stylesheet" />
    <link href="assets/css/styleswitcher.css" rel="stylesheet" />
    <link href="assets/ytplayer/css/YTPlayer.css" rel="stylesheet" />
    <link type="text/css" href='http://fonts.googleapis.com/css?family=Aldrich' rel="stylesheet" />
    <!-- bxSlider CSS file -->
<link href="js/jquery.bxslider.css" rel="stylesheet" />
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js" type="text/javascript"></script>
      <script src="assets/js/respond.min.js" type="text/javascript"></script>
    <![endif]-->

    <link rel="apple-touch-fa-precomposed" sizes="144x144" href="assets/ico/apple-touch-fa-144-precomposed.png" />
    <link rel="shortcut icon" href="assets/ico/favicon.ico" />
    
    <style>
      #map-canvas {
        width: 500px;
        height: 400px;
      }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script>
      function initialize() {
        var mapCanvas = document.getElementById('map-canvas');
        var mapOptions = {
          center: new google.maps.LatLng(47.4695946,19.071223),
          zoom: 14,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(mapCanvas, mapOptions)
		  marker = new google.maps.Marker({
			position: new google.maps.LatLng(47.4695946, 19.071223),
			map: map
		  });
      }
      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
<script type="text/javascript">



function checkEmail(emailstr){
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailstr)){
		return (true)
	}
	return (false)
 } 

function do_submitlanding(){
	var f=document.forms.frm_landing;
	if(f.allok.checked==1){
		if(f.email.value!="" && f.nev1.value!="" && f.cegnev.value!="" && f.szlacim.value!="" && f.postcim.value!="" && f.telefon.value!=""){
			if (checkEmail(f.email.value)){
				f.submit();
			}
			else{
				alert("Nem megfelelő emailcím formátum!");
			}
		}
		else{
				alert("A kötelező mezőket ki kell tölteni!!");
		}
	}
	else{
		alert("A jelentkezéshez tudomásul kell venni a feltételeket!");
	}
}
</script>
<?
$sending="0";
if(isset($_POST['landingsend'])){
	$check=db_one("select id from workshop_landing_2015 where nev1='".$_POST['nev1']."' and email='".$_POST['email']."' and sessid='".session_id()."'");
	if($check==""){
	$bele="";
	$osszes=0;
		if(strlen($_POST['nev1'])>1){
			if(isset($_POST['tcheck1'])){
				$osszes+=19500;
				$bele+=$_POST['nev1'].": 19 500 Ft\n";
	
			}
			else{
				if(strlen($_POST['nev2'])>1 && strlen($_POST['nev2'])>1){
					$osszes+=19500;
					$bele+=$_POST['nev1'].": 19 500 Ft\n";
			
				}
				else{
					$osszes+=19500;
					$bele+=$_POST['nev1'].": 19 500 Ft\n";
			
				}
			}
	
			if(strlen($_POST['nev2'])>1 && strlen($_POST['nev1'])>1){
			if(isset($_POST['tcheck2'])){
				$osszes+=19500;
				$bele+=$_POST['nev2'].": 19 500 Ft\n";
	
			}
			else{
				$osszes+=19500;
				$bele+=$_POST['nev2'].": 19 500 Ft\n";
				}
			
			}
			if(strlen($_POST['nev3'])>1 && strlen($_POST['nev1'])>1){
			if(isset($_POST['tcheck3'])){
				$osszes+=19500;
				$bele+=$_POST['nev3'].": 19 500 Ft\n";
	
			}
			else{
				$osszes+=19500;
				$bele+=$_POST['nev3'].": 19 500 Ft\n";
				}
			
			}
	

		}
		
	//	db_execute("insert into workshop_landing_2015 (nev1,nev2,nev3,cegnev,szlacim,postcim,email,telefon,megjegyzes,sessid,regdate) 
	//	values('".$_POST['nev1']."','".$_POST['nev2']."','".$_POST['nev3']."','".$_POST['cegnev']."','".$_POST['szlacim']."','".$_POST['postcim']."','".$_POST['email']."','".$_POST['telefon']."','".$_POST['megjegyzes']."','".session_id()."',NOW())");
		$sysmessage="Kedves ".$_POST['nev1']."!\n\nKöszönjük jelentkezését a 2015. május 27-én 09:00-kor a MÜPA-ban megrendezendő HR Piknikünkre.\n\nA jelentkezés jóváhagyásához kérjük kattintson erre a linkre http://www.hrportal.hu/index.phtml?page=landing_2015_hrpiknik&p=jelentkez&lsid=".session_id()."\n\nJelentkezési és számlázási adatai a következők:\n\n1. személy neve: ".$_POST['nev1']."\n";if($_POST['nev2']!=""){$sysmessage.="2. személy neve: ".$_POST['nev2']."\n";}if($_POST['nev3']!=""){$sysmessage.="3. személy neve: ".$_POST['nev3']."\n";}$sysmessage.="E-mail: ".$_POST['email']."\nTelefonszám: ".$_POST['telefon']."\nCégnév: ".$_POST['cegnev']."\nSzámlázási cím: ".$_POST['szlacim']."\nPostázási cím: ".$_POST['postcim']."\nMegjegyzes:\n\n".$_POST['megjegyzes']."\n\nÖsszesen fizetendő:".$osszes."\n\nJelentkezési díjáról  hamarosan egy számlát állít ki és küld el Önnek a HRPortal.hu, melyet kérünk a regisztráció visszaigazolásától számított 8 munkanapon belül - de legkésőbb a rendezvény napjáig - kiegyenlíteni. Az online regisztráció megrendelésnek minősül.\n\nA rendezvényen a részvétel feltétele a részvételi díj előzetes kiegyenlítése. Lemondást a részvételi díj teljes visszafizetése mellett a rendezvény napja előtti 5. munkanapon 12 óráig - KIZÁRÓLAG ÍRÁSBAN az info@hrportal.hu címre - fogadunk el. Az ezutáni lemondások esetében a részvétel átruházható, de a teljes díjat ki kell fizetni. Telefonos vagy egyéb szóbeli lemondásokat nem áll módunkban elfogadni.\nA lemondási határidő utáni regisztráció esetén lemondásra nincs lehetőség.\n\nA programról és a helyszínről az alábbi linken tájékozódhat: http://www.hrportal.hu/index.phtml?page=landing_2015_hrpiknik\n\nÜdvözlettel,\n\nHRportal.hu";
		$cmmessage="Landing regisztráció történt!\n A regisztrációban ez a levél került az ügyfélnek kiküldésre\n\nKedves ".$_POST['nev1']."!\n\nKöszönjük jelentkezését a 2015. május 27-én 09:00-kor a MÜPA-ban megrendezendő HR Piknikünkre.\n\nA jelentkezés jóváhagyásához kérjük kattintson erre a linkre http://www.hrportal.hu/index.phtml?page=landing_2015_hrpiknik&p=jelentkez&lsid=".session_id()."\n\nJelentkezési és számlázási adatai a következők:\n\n1. személy neve: ".$_POST['nev1']."\n";if($_POST['nev2']!=""){$cmmessage.="2. személy neve: ".$_POST['nev2']."\n";}if($_POST['nev3']!=""){$cmmessage.="3. személy neve: ".$_POST['nev3']."\n";}$cmmessage.="E-mail: ".$_POST['email']."\nTelefonszám: ".$_POST['telefon']."\nCégnév: ".$_POST['cegnev']."\nSzámlázási cím: ".$_POST['szlacim']."\nPostázási cím: ".$_POST['postcim']."\nMegjegyzes:\n\n".$_POST['megjegyzes']."\n\nÖsszesen fizetendő:".$osszes."\n\nJelentkezési díjáról  hamarosan egy számlát állít ki és küld el Önnek a HRPortal.hu, melyet kérünk a regisztráció visszaigazolásától számított 8 munkanapon belül - de legkésőbb a rendezvény napjáig - kiegyenlíteni. Az online regisztráció megrendelésnek minősül.\n\nA rendezvényen a részvétel feltétele a részvételi díj előzetes kiegyenlítése. Lemondást a részvételi díj teljes visszafizetése mellett a rendezvény napja előtti 5. munkanapon 12 óráig - KIZÁRÓLAG ÍRÁSBAN az info@hrportal.hu címre - fogadunk el. Az ezutáni lemondások esetében a részvétel átruházható, de a teljes díjat ki kell fizetni. Telefonos vagy egyéb szóbeli lemondásokat nem áll módunkban elfogadni.\nA lemondási határidő utáni regisztráció esetén lemondásra nincs lehetőség.\n\nA programról és a helyszínről az alábbi linken tájékozódhat: http://www.hrportal.hu/index.phtml?page=landing_2015_hrpiknik\n\nÜdvözlettel,\n\nHRportal.hu";
		@mail("jozsef.szucs@planetstar.hu", "HR Portal - HRPiknik regisztráció történt!!", $cmmessage, "From: info@hrportal.hu");
		@mail("andras.markovics@gmail.com", "HR Portal - HRPiknik regisztráció történt!!", $cmmessage, "From: info@hrportal.hu");
		@mail("info@hrportal.hu", "HR Portal - HRPiknik regisztráció történt!!", $cmmessage, "From: info@hrportal.hu");
		@mail("edit.halmi@hrportal.hu", "HR Portal - HRPiknik regisztráció történt!!", $cmmessage, "From: info@hrportal.hu");
		
		@mail($_POST['email'], "HR Portal - HRPiknik regisztráció", $sysmessage, "From: info@hrportal.hu");
		$sending="1";
	}
}
if(isset($_GET['lsid'])){
	
	db_execute("update workshop_landing_2015 set confirm='1', confirm_date=NOW() where sessid='".$_GET['lsid']."' ");	
	$sending="2";
}

?>   
</head>
<body id="top" data-spy="scroll" data-target=".navbar" data-offset="200">
    <div class="bg-video"></div>
    <!-- ## page wrapper start ## -->
    <div id="page-wrapper">
        <!-- ## parallax section 1 start ## -->
        <section id="parallax-section-1">
            <!-- ## parallax background 1 ## -->
            <div class="bg1"></div>
            <!-- ## parallax background pattern 1 start ## -->
            <div >



                <!-- ## header start ## -->
                <header id="header">
                    <div class="container">
                        <div class="row"></div>
                    </div>
                    <!-- ## nav start ## -->
                <nav id="nav">
                    <div class="navbar navbar-inverse navbar-fixed-bottom" role="navigation">
                        <div class="container">

                            <div class="navbar-collapse collapse navbar-right">
                                <ul class="nav navbar-nav">
                                    <li class="active"><a href="#top">Nyitó</a></li>
                                    <li><a href="#about">Esemény bemutatása</a></li>
                                    <li><a href="#content-boxes">Program</a></li>
                                    <li><a href="#team">Előadók</a></li>
                                    <li><a href="#contact">Jelentkezés</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </nav>
                <!-- ## nav end ## -->
                </header>
                <!-- ## header end ## -->

                <!-- ## about section start ## -->
                <section id="about" class="scrollblock">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 text-center about-box">
                                <h2>Program
                            <span class="line"><span class="dot"></span></span>
                                </h2>
                                <p>
                                  <span style="color:#f7941d;font-weight:bold;">NŐI AGY / FÉRFI AGY<br />
                                  HOGY JOBBAN ÉRTSÜK EGYMÁST A MUNKAHELYEN!</span><br /><br />
                                  Miben is működünk másképp? Észlelés, gondolkodás, kockázatkezelés, felelősségvállalás, döntéshozatal, érzelmek kezelése, autoritás.  A férfi-női különbségekből fakadó konfliktusok. Eltérő kommunikáció. Férfi erősségek – női erősségek.Előadás- Kerekasztal- Workshop
                                  <br /><br /> <span style="color:#f7941d;font-weight:bold;">ÖTLETGYÁR <br />
                                  HOGYAN TEREMTS KREATÍV MUNKAHELYET?</span><br /><br />
                                  Hogy lehet kreatív céges környezetet teremteni?
                                  Hogy lehet egy jó ötletet megvalósítani?
                                  Hogy tudjuk átállítani magunkat, hogy a napi rohanás helyett figyelemmel és kitartással forduljunk az újító ötletek felé?  
                                  A kivételesen sikeres cégek, mint az Apple, Prezi, Southwest, USTREAM, Patagonia, Toyota mögött makacsul kreatív emberek vannak, ettől vonzóak a termékeik, ettől profitábilisabbak, mint versenytársaik! HOGYAN LEHET A CÉGEMBŐL IGAZI ÖTLETGYÁR? Előadás-Kerekasztal-Workshop
                                  <br /><br /> <span style="color:#f7941d;font-weight:bold;">JÓ CSAPATOT! <br />
                                  DE HOGYAN?</span><br /><br />
                                  Milyen belső, és külső kötülmények befolyásolják egy csapat működését? 
                                  Mi is a valódi szembenézés? Hogyan hátráltatják a kimondatlan feszültségek az eredményességet? A FÉRFI VIZILABDA válogatott sportpszichológusától, Imre Tóvári Zsuzsától kapunk válaszokat, aki ma már coachként céges csapatok működését is segíti.
                                </p>
                            </div>
                        </div>

                     
                    </div>
                </section>
                <!-- ## about section end ## -->
            </div>
            <!-- ## parallax bg pattern 1 end ## -->
        </section>
        <!-- ## parallax section 1 end ## -->

        <!-- ## content boxes start ## -->
        <section id="content-boxes" class="scrollblock">
            <div class="container">
                <div class="row">

                   <ul class="bxslider">
					  <li><img src="prog1.jpg" /></li>
					  <li><img src="prog2.jpg" /></li>
					  <li><img src="prog3.jpg" /></li>
					  <li><img src="prog4.jpg" /></li>
					  <li><img src="prog5.jpg" /></li>
					  <li><img src="prog6.jpg" /></li>
					</ul>
                </div>
            </div>
        </section>
        <!-- ## content boxes end ## -->

        <!-- ## parallax section 2 start ## -->
        <section id="parallax-section-2">
            <!-- ## parallax background 2 ## -->
            <div class="bg2"></div>
            <!-- ## parallax background pattern 2 start ## -->
            <div class="bg-pattern2" style="background: none;">
                <!-- ## team section start ## -->
                <section id="team" class="scrollblock_team">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <h2>Előadók
                             <span class="line"><span class="dot"></span></span>
                                </h2>
                            </div>
                        </div>
                        <div class="row0">
                            <div class="col-md-3 col-sm-6">
                                <div class="col-lg-12 member-box">
                                    <div class="member-img">
                                        <a href="#"><img class="img-responsive" src="assets/images/team/pic_1.jpg" alt=".img-responsive" /></a>
                                    </div>
                                    <div class="member-data">
                                        <div class="member-info">
                                            <div class="member-name"><a href="#">Molnár Éva</a></div>
                                            <div class="member-title">Női vezetőképzés, képzésvezető</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="member-content">Molnár Éva 15 éve foglalkozik coachinggal, szervezetfejlesztéssel. Sok női vezetővel volt alkalma együttdolgozni, az ő kérdéseik adták az ötletet, hogy igény van női vezetők képzésére, ezért is indította el a RESTART-tal közösen az első erős önismereti elemeken alapuló programot. Ennek a programnak része a a NŐI AGY / FÉRFI AGY működése, hogy két nem jobban értse egymást.</div>
                                    </div>
                                    <div class="clearfix"></div>
                               </div>
                            </div>
                                
                                <!-- ## member box end ## -->
                            <div class="col-md-3 col-sm-6">
                                <div class="col-lg-12 member-box">
                                    <div class="member-img">
                                        <a href="#"><img class="img-responsive" src="assets/images/team/pic_2.jpg" alt=".img-responsive" /></a>
                                    </div>
                                    <div class="member-data">
                                        <div class="member-info">
                                            <div class="member-name"><a href="#">Kreiter Éva </a></div>
                                            <div class="member-title">DREHER SÖRGYÁRAK, HR Igazgató</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="member-content">Kreiter Éva a Ford Motor Company és a Schneider Electric  után érkezett a Dreher Sörgyárakhoz. Igazi férfias iparágban menedzseli, és menedzselte a HR csapatokat eddigi karrierje során. Szívügye a NŐI VEZETŐ téma, több fórumon beszélt, írt már erről.</div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                                <!-- ## member box end ## -->
                            <div class="col-md-3 col-sm-6">
                                <div class="col-lg-12 member-box">
                                    <div class="member-img">
                                        <a href="#"><img class="img-responsive" src="assets/images/team/pic_4.jpg" alt=".img-responsive" /></a>
                                    </div>
                                    <div class="member-data">
                                        <div class="member-info">
                                            <div class="member-name"><a href="#">Csányi Sándor</a></div>
                                            <div class="member-title">Színművész</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="member-content">Csányi Sándor színművész, a Thália Színház művészeti vezetője. A FÉRFIAGY című előadása nagy színházi siker, de ezen túl, beszélni fog arról is, hogy milyen a nőkkel, operatív vezetőként együttdolgoznia.</div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                                <!-- ## member box end ## -->
                                
                            <div class="col-md-3 col-sm-6">
                                <div class="col-lg-12 member-box">
                                    <div class="member-img">
                                        <a href="#"><img class="img-responsive" src="assets/images/team/pic_6.jpg" alt=".img-responsive" /></a>
                                    </div>
                                    <div class="member-data">
                                        <div class="member-info">
                                            <div class="member-name"><a href="#">Kiss Péter</a></div>
                                            <div class="member-title">RESTART alapító, volt médiaszakember (Tv2 csoport programigazgató)</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="member-content">Kiss Péter összesen 10 évig volt felsővezető a médiában, mielőtt megalapította a RESTARTOT! Szinte végig kreatív területen, kreatív emberek vezetésével foglalkozott, akkor és most is azt tartotta legfontosabb feladatának, hogy bevonva az embereket megmutassa, lehet kreatívan, alkotóan élni, és ez a jó érzésen túl anyagilag is nagyon profitábilis egyéni és cég szinten is.</div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                                <!-- ## member box end ## -->
                         </div>
                     </div>
                     <div class="container">
                         <div class="row0">
                            <div class="col-md-3 col-sm-6">
                                <div class="col-lg-12" style="
                                  -webkit-background-clip: border-box;
								  -webkit-background-origin: padding-box;
								  -webkit-background-size: auto;
								  -webkit-transition-delay: 0s;
								  -webkit-transition-duration: 0.2s;
								  -webkit-transition-property: all;
								  -webkit-transition-timing-function: ease-in-out;
								  background-attachment: scroll;
								  background-clip: border-box;
								  background-color: rgba(255, 228, 0, 0.8);
								  background-image: none;
								  background-origin: padding-box;
								  background-size: auto;
								  box-sizing: border-box;
								  color: rgb(51, 51, 51);
								  cursor: pointer;
								  display: block;
								  float: left;
								  font-family: Aldrich;
								  font-size: 14px;
								  height: 100%;
								  line-height: 20px;
								  margin-bottom: 10px;
								  margin-left: 0px;
								  margin-right: 0px;
								  margin-top: 10px;
								  min-height: 1px;
								  opacity: 1;
								  padding-bottom: 10px;
								  padding-left: 15px;
								  padding-right: 15px;
								  padding-top: 6px;
								  position: relative;
								  transition-delay: 0s;
								  transition-duration: 0.2s;
								  transition-property: all;
								  transition-timing-function: ease-in-out;
								  width: 255px;
                                ">
                                    <div class="member-img">
                                        <a href="#"><img class="img-responsive" src="assets/images/team/pic_7.jpg" alt=".img-responsive" /></a>
                                    </div>
                                    <div class="member-data">
                                        <div class="member-info">
                                            <div class="member-name"><a href="#"><font style="font-family:Aldrich; color:#000000; text-size:18px;"><strong>Till Attila</strong></font></a></div>
                                            <div class="member-title">Műsorvezető, filmrendező</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="member-content">Till Attila elsőként mint a „megállíthatatlan” műsorvezető ugrik be mindenkinek, pedig filmrendezőként, forgatókönyv íróként is sikeres. Egy film összetett alkotófolyamat, amiben sok emberrel kell együttdolgozni, vezetni, erről beszélünk vele.</div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="col-lg-12" style="
                                  -webkit-background-clip: border-box;
								  -webkit-background-origin: padding-box;
								  -webkit-background-size: auto;
								  -webkit-transition-delay: 0s;
								  -webkit-transition-duration: 0.2s;
								  -webkit-transition-property: all;
								  -webkit-transition-timing-function: ease-in-out;
								  background-attachment: scroll;
								  background-clip: border-box;
								  background-color: rgba(255, 228, 0, 0.8);
								  background-image: none;
								  background-origin: padding-box;
								  background-size: auto;
								  box-sizing: border-box;
								  color: rgb(51, 51, 51);
								  cursor: pointer;
								  display: block;
								  float: left;
								  font-family: Aldrich;
								  font-size: 14px;
								  height: 100%;
								  line-height: 20px;
								  margin-bottom: 10px;
								  margin-left: 0px;
								  margin-right: 0px;
								  margin-top: 10px;
								  min-height: 1px;
								  opacity: 1;
								  padding-bottom: 10px;
								  padding-left: 15px;
								  padding-right: 15px;
								  padding-top: 6px;
								  position: relative;
								  transition-delay: 0s;
								  transition-duration: 0.2s;
								  transition-property: all;
								  transition-timing-function: ease-in-out;
								  width: 255px;
                                ">
                                    <div class="member-img">
                                        <a href="#"><img class="img-responsive" src="assets/images/team/pic_8.jpg" alt=".img-responsive" /></a>
                                    </div>
                                    <div class="member-data">
                                        <div class="member-info">
                                            <div class="member-name"><a href="#"><font style="font-family:Aldrich; color:#000000; text-size:18px;"><strong>Benkő Katalin</strong></font></a></div>
                                            <div class="member-title">Prezi</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="member-content">Benkő Katalin a legmenőbb startupnál látja nap, mint nap, milyen is a teremtő, innovatív légkör. Milyen munkatársak kellenek ehhez? Milyen is az a szervezet belülről, ami kevesebb mint 10 éves, mégis világhírű?</div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="col-lg-12" style="
                                  -webkit-background-clip: border-box;
								  -webkit-background-origin: padding-box;
								  -webkit-background-size: auto;
								  -webkit-transition-delay: 0s;
								  -webkit-transition-duration: 0.2s;
								  -webkit-transition-property: all;
								  -webkit-transition-timing-function: ease-in-out;
								  background-attachment: scroll;
								  background-clip: border-box;
								  background-color: rgba(255, 228, 0, 0.8);
								  background-image: none;
								  background-origin: padding-box;
								  background-size: auto;
								  box-sizing: border-box;
								  color: rgb(51, 51, 51);
								  cursor: pointer;
								  display: block;
								  float: left;
								  font-family: Aldrich;
								  font-size: 14px;
								  height: 100%;
								  line-height: 20px;
								  margin-bottom: 10px;
								  margin-left: 0px;
								  margin-right: 0px;
								  margin-top: 10px;
								  min-height: 1px;
								  opacity: 1;
								  padding-bottom: 10px;
								  padding-left: 15px;
								  padding-right: 15px;
								  padding-top: 6px;
								  position: relative;
								  transition-delay: 0s;
								  transition-duration: 0.2s;
								  transition-property: all;
								  transition-timing-function: ease-in-out;
								  width: 255px;
                                ">
                                    <div class="member-img">
                                        <a href="#"><img class="img-responsive" src="assets/images/team/Friedl Zsuzsa_ff.jpg" alt=".img-responsive" /></a>
                                    </div>
                                    <div class="member-data">
                                        <div class="member-info">
                                            <div class="member-name"><a href="#"><font style="font-family:Aldrich; color:#000000; text-size:18px;"><strong>Friedl Zsuzsanna</strong></font></a></div>
                                            <div class="member-title">Microsoft HR vezető</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="member-content">A MICROSOFT újra zászlajára tűzte a kreativitást, a megújulást. Zsuzsával arról fogunk beszélgetni, hogyan lesz egy globális cél: azaz „MERJ ÚJÍTANI!” helyi szinten megvalósítva.</div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="col-lg-12" style="
                                  -webkit-background-clip: border-box;
								  -webkit-background-origin: padding-box;
								  -webkit-background-size: auto;
								  -webkit-transition-delay: 0s;
								  -webkit-transition-duration: 0.2s;
								  -webkit-transition-property: all;
								  -webkit-transition-timing-function: ease-in-out;
								  background-attachment: scroll;
								  background-clip: border-box;
								  background-color: rgba(255, 228, 0, 0.8);
								  background-image: none;
								  background-origin: padding-box;
								  background-size: auto;
								  box-sizing: border-box;
								  color: rgb(51, 51, 51);
								  cursor: pointer;
								  display: block;
								  float: left;
								  font-family: Aldrich;
								  font-size: 14px;
								  height: 100%;
								  line-height: 20px;
								  margin-bottom: 10px;
								  margin-left: 0px;
								  margin-right: 0px;
								  margin-top: 10px;
								  min-height: 1px;
								  opacity: 1;
								  padding-bottom: 10px;
								  padding-left: 15px;
								  padding-right: 15px;
								  padding-top: 6px;
								  position: relative;
								  transition-delay: 0s;
								  transition-duration: 0.2s;
								  transition-property: all;
								  transition-timing-function: ease-in-out;
								  width: 255px;
                                ">
                                    <div class="member-img">
                                        <a href="#"><img class="img-responsive" src="assets/images/team/pic_10.jpg" alt=".img-responsive" /></a>
                                    </div>
                                    <div class="member-data">
                                        <div class="member-info">
                                            <div class="member-name"><a href="#"><font style="font-family:Aldrich; color:#000000; text-size:18px;"><strong>Imre Tóvári Zsuzsa</strong></font></a></div>
                                            <div class="member-title">Viziladbda válogatott sportpszichológusa, coach</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="member-content">Tóvári Zsuzsa 7 éve dolgozik együtt a férfi vizilabdaválogatottal. Nem a medence partján, hanem a háttérben alakítja gondolkodásmódjukat, lelküket. Sikereiket mindenki ismeri, de mégis  mit is csinál egy sportpszichológus? Hogy fordítja ezt le az üzleti élet nyelvére, mint coach?</div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                     </div>
                     <div class="container">
                         <div class="row0">
                            <div class="col-md-3 col-sm-6">
                                <div class="col-lg-12" style="
                                  -webkit-background-clip: border-box;
								  -webkit-background-origin: padding-box;
								  -webkit-background-size: auto;
								  -webkit-transition-delay: 0s;
								  -webkit-transition-duration: 0.2s;
								  -webkit-transition-property: all;
								  -webkit-transition-timing-function: ease-in-out;
								  background-attachment: scroll;
								  background-clip: border-box;
								  background-color: rgba(255, 228, 0, 0.8);
								  background-image: none;
								  background-origin: padding-box;
								  background-size: auto;
								  box-sizing: border-box;
								  color: rgb(51, 51, 51);
								  cursor: pointer;
								  display: block;
								  float: left;
								  font-family: Aldrich;
								  font-size: 14px;
								  height: 100%;
								  line-height: 20px;
								  margin-bottom: 10px;
								  margin-left: 0px;
								  margin-right: 0px;
								  margin-top: 10px;
								  min-height: 1px;
								  opacity: 1;
								  padding-bottom: 10px;
								  padding-left: 15px;
								  padding-right: 15px;
								  padding-top: 6px;
								  position: relative;
								  transition-delay: 0s;
								  transition-duration: 0.2s;
								  transition-property: all;
								  transition-timing-function: ease-in-out;
								  width: 255px;
                                ">
                                    <div class="member-img">
                                        <a href="#"><img class="img-responsive" src="assets/images/team/pic_3.jpg" alt=".img-responsive" /></a>
                                    </div>
                                    <div class="member-data">
                                        <div class="member-info">
                                            <div class="member-name"><a href="#"><font style="font-family:Aldrich; color:#000000; text-size:18px;"><strong>Adriány Kincső</strong></font></a></div>
                                            <div class="member-title">HBLF</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="member-content">Adriány Kincső a Hungarian Bussiness Leaders Forum női tagozatával sok helyen járt már a világban és az itthoni viszonyokat is jól ismeri. Az üzleti világból, a Világbank és CEU-val a háta mögött érkezett a HBLF-hez.</div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
						</div>
                     </div>
                </section>
                <!-- ## team section end ## -->
            </div>
            <!-- ## parallax background pattern 2 end ## -->
        </section>
        <!-- ## parallax section 2 end ## -->
        
        <!-- ## contact section start ## -->
        <section id="contact" class="scrollblock">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h2>Jelentkezés
                        <span class="line"><span class="dot"></span></span>
                        </h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 contact-form contact-box">
                        <h4>Jelentkezzen a HR Piknikre!</h4>
                        <p>
						A RESTART ÉS A HRPORTAL BEMUTATJA: HRPIKNIK<br>
						(3 TÉMA 1 NAPON, Előadás, kerekasztal, workshop)
						</p>
						<p>
							<b>A workshop díja 19 500 Ft +ÁFA/fő.</b><br />
A díj tartalmazza az ebédet és a napközbeni finomságokat, frissítőket.
						</p>
						
                      <form name="form-contact-us" id="form-contact-us">
                       
                            <div class="form-group">
								
                            Online jelentkezéséről e-mailben visszaigazolást küldünk. Amennyiben a visszaigazoló levélben rákattint az aktiváló linkre, azzal elfogadja az adatok helyességét. Ezt követően a jóváhagyott adatok alapján átutalásos számlát küldünk a részvételi díjról, melyet kérünk a regisztráció visszaigazolásától számított 8 munkanapon belül - de legkésőbb a rendezvény napjáig - kiegyenlíteni. Az online regisztráció megrendelésnek minősül.
							<br><br>A rendezvényen a részvétel feltétele a részvételi díj előzetes kiegyenlítése. Lemondást a részvételi díj teljes visszafizetése mellett a rendezvény napja előtti 5. munkanapon 12 óráig - KIZÁRÓLAG ÍRÁSBAN a info@hrportal.hu címre - fogadunk el. Az ezutáni lemondások esetében a részvétel átruházható, de a teljes díjat ki kell fizetni. Telefonos vagy egyéb szóbeli lemondásokat nem áll módunkban elfogadni.
							<br><br>A lemondási határidő utáni regisztráció esetén lemondásra nincs lehetőség.
                            </div>
							<div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="txtName">Név: </label>
                                        <input type="text" placeholder="Név"
                                            class="form-control required" id="txtName" name="txtName" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="txtEmail">E-mail:</label>
                                        <input type="text" placeholder="E-mail ID"
                                            class="form-control required email" id="txtEmail" name="txtEmail" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="txtphone">Telefonszám:</label>
                                        <input type="text" placeholder="Telefonszám"
                                            class="form-control required phone" id="txtphone" name="txtphone" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="txtszlaname">Számlázási név:</label>
                                        <input type="text" placeholder="Számlázási név"
                                            class="form-control " id="txtszlaname" name="txtszlaname" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="txtszlaaddr">Számlázási cím:</label>
                                        <input type="text" placeholder="Számlázási cím"
                                            class="form-control " id="txtszlaaddr" name="txtszlaaddr" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="txtMessage">Üzenet:</label>
                                <textarea placeholder="Message" class="form-control required"
                                    id="txtMessage" name="txtMessage" rows="10"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="txtCaptcha"></label>
                                <input
                                    type="text" class="form-control required captcha"
                                    placeholder="Egy keresztkérdés?" id="txtCaptcha" name="txtCaptcha" />
                            </div>
                            <div class="form-group">
                                <input type="button" id="btn-Send" value="Küld" class="btn btn-theme-inverse" />
                            </div>
                            <div class="col-lg-12" id="contact-form-message">
                        </form>
                        <!-- ## contact form end ## -->
                    </div>
                    </div>

                    <div class="col-sm-6 contact-info contact-box">
                        <h4>Elérhetőségek</h4>
                        <!-- ## contact info start ## -->
                        <ul class="fa-ul">
                            <li><i class="fa-li fa fa-clock-o text-theme-inverse"></i><span>Időpont: 2015. május 27. 9:00</span></li>
                            <li><i class="fa-li fa fa-home text-theme-inverse"></i><span>1095 Budapest Komor Marcell u. 1.</span></li>
                            <li><i class="fa-li fa fa-envelope text-theme-inverse"></i><span>info@hrportal.hu</span></li>
                            <li><i class="fa-li fa fa-globe text-theme-inverse"></i><span>www.hrportal.hu/hrpiknik/</span></li>
                        </ul>
                        <!-- ## contact info endstart ## -->
                        <!-- ## map start ## -->
<div id="map-canvas"></div>
                        <!-- ## map end ## -->
                    </div>
                </div>
            </div>
        </section>
        <!-- ## contact section end ## -->

        <!-- ## blockquote section start ## -->
        <section id="bquote" class="scrollblock">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-lg-offset-1 quote-box">
                        <blockquote>
                            <p>Jelentkezzen programunkra!</p>
                        </blockquote>
                    </div>
                </div>
            </div>
        </section>
        <!-- ## blockquote section end ## -->


        <!-- ## twitter section end ## -->

        <!-- ## footer section start ## -->
        <section id="footer">
            <!-- ## footer background pattern start ## -->
            <div class="bg-pattern3">

                <!-- ## footer copyright start ## -->
                <div class="footer-copyright text-center">
                    HRPortal.hu Copyright &copy; 2015, All rights reserved.
                </div>
                <!-- ## footer copyright end ## -->
            </div>
            <!-- ## footer background pattern end ## -->
        </section>
        <!-- ## footer section end ## -->

        <!-- ## preloader start ## -->
        <div id="preloader">
            <div id="status"><span class="fa fa-spin fa-spinner fa-5x"></span></div>
        </div>
        <!-- ## preloader end ## -->

        <!-- ## Time Bar begins here ## -->
        <!-- ## do not change it. ## -->
        <div id="progress-back" class="load-item">
            <div id="progress-bar"></div>
        </div>
        <!-- ## Time Bar ends here ## -->
    </div>
    <!-- ## page wrapper end ## -->

    <script src="assets/js/jquery-2.0.0.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/twitter/jquery.tweet.min.js" type="text/javascript"></script>
    <script src="assets/js/jquery.parallax-1.1.3.js" type="text/javascript"></script>
    <script src="assets/js/jquery.nicescroll.min.js" type="text/javascript"></script>
    <script src="assets/js/detectmobilebrowser.js" type="text/javascript"></script>
    <script src="assets/js/jquery.smooth-scroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.scrollorama.js" type="text/javascript"></script>
    <script src="assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="assets/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="assets/js/jquery.cookie.js" type="text/javascript"></script>
    <script src="assets/ytplayer/js/jquery.mb.YTPlayer.js" type="text/javascript"></script>
    <script src="assets/js/app.variables.js" type="text/javascript"></script>
    <!--<script src="assets/js/jquery.countdown.js" type="text/javascript"></script>
    <script src="assets/js/app.subscribe.js" type="text/javascript"></script>
    <script src="assets/js/app.contact.js" type="text/javascript"></script>
    <script src="assets/js/app.ui.js" type="text/javascript"></script>
    <script src="assets/js/app.styleswitcher.js" type="text/javascript"></script>-->
    <script src="/js/jquery.bxslider.min.js"></script>

    <script src="assets/js/app.landlite.js" type="text/javascript"></script>
</body>
</html>
