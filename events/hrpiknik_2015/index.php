﻿<!DOCTYPE html>
<html lang="en">
<head>
    
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-2">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>HRPIKNIK - 2015</title>

    <!-- ## Bootstrap ## -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="assets/css/supersized.css" rel="stylesheet" />
    <link id="skin-styler" href="assets/css/skin1.min.css" rel="stylesheet" />
    <link href="assets/css/styleswitcher.css" rel="stylesheet" />
    <link href="assets/ytplayer/css/YTPlayer.css" rel="stylesheet" />
    <link type="text/css" href='http://fonts.googleapis.com/css?family=Aldrich' rel="stylesheet" />
    <!-- bxSlider CSS file -->
<link href="js/jquery.bxslider.css" rel="stylesheet" />
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js" type="text/javascript"></script>
      <script src="assets/js/respond.min.js" type="text/javascript"></script>
    <![endif]-->

    <link rel="apple-touch-fa-precomposed" sizes="144x144" href="assets/ico/apple-touch-fa-144-precomposed.png" />
    <link rel="shortcut icon" href="assets/ico/favicon.ico" />
    
    <style>
      #map-canvas {
        width: 500px;
        height: 400px;
      }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script>
      function initialize() {
        var mapCanvas = document.getElementById('map-canvas');
        var mapOptions = {
          center: new google.maps.LatLng(47.4695946,19.071223),
          zoom: 14,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(mapCanvas, mapOptions)
		  marker = new google.maps.Marker({
			position: new google.maps.LatLng(47.4695946, 19.071223),
			map: map
		  });
      }
      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
<script type="text/javascript">



function checkEmail(emailstr){
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailstr)){
		return (true)
	}
	return (false)
 } 

function do_submitlanding(){
	var f=document.forms.frm_landing;
	if(f.allok.checked==1){
		if(f.email.value!="" && f.nev1.value!="" && f.cegnev.value!="" && f.szlacim.value!="" && f.postcim.value!="" && f.telefon.value!=""){
			if (checkEmail(f.email.value)){
				f.submit();
			}
			else{
				alert("Nem megfelelő emailcím formátum!");
			}
		}
		else{
				alert("A kötelező mezőket ki kell tölteni!!");
		}
	}
	else{
		alert("A jelentkezéshez tudomásul kell venni a feltételeket!");
	}
}
</script>
<?
$sending="0";
if(isset($_POST['landingsend'])){
	$check=db_one("select id from workshop_landing_2015 where nev1='".$_POST['nev1']."' and email='".$_POST['email']."' and sessid='".session_id()."'");
	if($check==""){
	$bele="";
	$osszes=0;
		if(strlen($_POST['nev1'])>1){
			if(isset($_POST['tcheck1'])){
				$osszes+=19500;
				$bele+=$_POST['nev1'].": 19 500 Ft\n";
	
			}
			else{
				if(strlen($_POST['nev2'])>1 && strlen($_POST['nev2'])>1){
					$osszes+=19500;
					$bele+=$_POST['nev1'].": 19 500 Ft\n";
			
				}
				else{
					$osszes+=19500;
					$bele+=$_POST['nev1'].": 19 500 Ft\n";
			
				}
			}
	
			if(strlen($_POST['nev2'])>1 && strlen($_POST['nev1'])>1){
			if(isset($_POST['tcheck2'])){
				$osszes+=19500;
				$bele+=$_POST['nev2'].": 19 500 Ft\n";
	
			}
			else{
				$osszes+=19500;
				$bele+=$_POST['nev2'].": 19 500 Ft\n";
				}
			
			}
			if(strlen($_POST['nev3'])>1 && strlen($_POST['nev1'])>1){
			if(isset($_POST['tcheck3'])){
				$osszes+=19500;
				$bele+=$_POST['nev3'].": 19 500 Ft\n";
	
			}
			else{
				$osszes+=19500;
				$bele+=$_POST['nev3'].": 19 500 Ft\n";
				}
			
			}
	

		}
		
	//	db_execute("insert into workshop_landing_2015 (nev1,nev2,nev3,cegnev,szlacim,postcim,email,telefon,megjegyzes,sessid,regdate) 
	//	values('".$_POST['nev1']."','".$_POST['nev2']."','".$_POST['nev3']."','".$_POST['cegnev']."','".$_POST['szlacim']."','".$_POST['postcim']."','".$_POST['email']."','".$_POST['telefon']."','".$_POST['megjegyzes']."','".session_id()."',NOW())");
		$sysmessage="Kedves ".$_POST['nev1']."!\n\nKöszönjük jelentkezését a 2015. november 03-án 09:00-kor a MÜPA-ban megrendezendő HR Piknikünkre.\n\nA jelentkezés jóváhagyásához kérjük kattintson erre a linkre http://www.hrportal.hu/index.phtml?page=landing_2015_hrpiknik&p=jelentkez&lsid=".session_id()."\n\nJelentkezési és számlázási adatai a következők:\n\n1. személy neve: ".$_POST['nev1']."\n";if($_POST['nev2']!=""){$sysmessage.="2. személy neve: ".$_POST['nev2']."\n";}if($_POST['nev3']!=""){$sysmessage.="3. személy neve: ".$_POST['nev3']."\n";}$sysmessage.="E-mail: ".$_POST['email']."\nTelefonszám: ".$_POST['telefon']."\nCégnév: ".$_POST['cegnev']."\nSzámlázási cím: ".$_POST['szlacim']."\nPostázási cím: ".$_POST['postcim']."\nMegjegyzes:\n\n".$_POST['megjegyzes']."\n\nÖsszesen fizetendő:".$osszes."\n\nJelentkezési díjáról  hamarosan egy számlát állít ki és küld el Önnek a HRPortal.hu, melyet kérünk a regisztráció visszaigazolásától számított 8 munkanapon belül - de legkésőbb a rendezvény napjáig - kiegyenlíteni. Az online regisztráció megrendelésnek minősül.\n\nA rendezvényen a részvétel feltétele a részvételi díj előzetes kiegyenlítése. Lemondást a részvételi díj teljes visszafizetése mellett a rendezvény napja előtti 5. munkanapon 12 óráig - KIZÁRÓLAG ÍRÁSBAN az info@hrportal.hu címre - fogadunk el. Az ezutáni lemondások esetében a részvétel átruházható, de a teljes díjat ki kell fizetni. Telefonos vagy egyéb szóbeli lemondásokat nem áll módunkban elfogadni.\nA lemondási határidő utáni regisztráció esetén lemondásra nincs lehetőség.\n\nA programról és a helyszínről az alábbi linken tájékozódhat: http://www.hrportal.hu/index.phtml?page=landing_2015_hrpiknik\n\nÜdvözlettel,\n\nHRportal.hu";
		$cmmessage="Landing regisztráció történt!\n A regisztrációban ez a levél került az ügyfélnek kiküldésre\n\nKedves ".$_POST['nev1']."!\n\nKöszönjük jelentkezését a 2015. november 03-án 09:00-kor a MÜPA-ban megrendezendő HR Piknikünkre.\n\nA jelentkezés jóváhagyásához kérjük kattintson erre a linkre http://www.hrportal.hu/index.phtml?page=landing_2015_hrpiknik&p=jelentkez&lsid=".session_id()."\n\nJelentkezési és számlázási adatai a következők:\n\n1. személy neve: ".$_POST['nev1']."\n";if($_POST['nev2']!=""){$cmmessage.="2. személy neve: ".$_POST['nev2']."\n";}if($_POST['nev3']!=""){$cmmessage.="3. személy neve: ".$_POST['nev3']."\n";}$cmmessage.="E-mail: ".$_POST['email']."\nTelefonszám: ".$_POST['telefon']."\nCégnév: ".$_POST['cegnev']."\nSzámlázási cím: ".$_POST['szlacim']."\nPostázási cím: ".$_POST['postcim']."\nMegjegyzes:\n\n".$_POST['megjegyzes']."\n\nÖsszesen fizetendő:".$osszes."\n\nJelentkezési díjáról  hamarosan egy számlát állít ki és küld el Önnek a HRPortal.hu, melyet kérünk a regisztráció visszaigazolásától számított 8 munkanapon belül - de legkésőbb a rendezvény napjáig - kiegyenlíteni. Az online regisztráció megrendelésnek minősül.\n\nA rendezvényen a részvétel feltétele a részvételi díj előzetes kiegyenlítése. Lemondást a részvételi díj teljes visszafizetése mellett a rendezvény napja előtti 5. munkanapon 12 óráig - KIZÁRÓLAG ÍRÁSBAN az info@hrportal.hu címre - fogadunk el. Az ezutáni lemondások esetében a részvétel átruházható, de a teljes díjat ki kell fizetni. Telefonos vagy egyéb szóbeli lemondásokat nem áll módunkban elfogadni.\nA lemondási határidő utáni regisztráció esetén lemondásra nincs lehetőség.\n\nA programról és a helyszínről az alábbi linken tájékozódhat: http://www.hrportal.hu/index.phtml?page=landing_2015_hrpiknik\n\nÜdvözlettel,\n\nHRportal.hu";
		@mail("jozsef.szucs@planetstar.hu", "HR Portal - HRPiknik regisztráció történt!!", $cmmessage, "From: info@hrportal.hu");
		@mail("andras.markovics@gmail.com", "HR Portal - HRPiknik regisztráció történt!!", $cmmessage, "From: info@hrportal.hu");
		@mail("info@hrportal.hu", "HR Portal - HRPiknik regisztráció történt!!", $cmmessage, "From: info@hrportal.hu");
		@mail("edit.halmi@hrportal.hu", "HR Portal - HRPiknik regisztráció történt!!", $cmmessage, "From: info@hrportal.hu");
		
		@mail($_POST['email'], "HR Portal - HRPiknik regisztráció", $sysmessage, "From: info@hrportal.hu");
		$sending="1";
	}
}
if(isset($_GET['lsid'])){
	
	db_execute("update workshop_landing_2015 set confirm='1', confirm_date=NOW() where sessid='".$_GET['lsid']."' ");	
	$sending="2";
}

?>   
</head>
<body id="top" data-spy="scroll" data-target=".navbar" data-offset="200">
    <div class="bg-video"></div>
    <!-- ## page wrapper start ## -->
    <div id="page-wrapper">
        <!-- ## parallax section 1 start ## -->
        <section id="parallax-section-1">
            <!-- ## parallax background 1 ## -->
            <div class="bg1"></div>
            <!-- ## parallax background pattern 1 start ## -->
            <div >



                <!-- ## header start ## -->
                <header id="header">
                    <div class="container">
                        <div class="row"></div>
                    </div>
                    <!-- ## nav start ## -->
                <nav id="nav">
                    <div class="navbar navbar-inverse navbar-fixed-bottom" role="navigation">
                        <div class="container">

                            <div class="navbar-collapse collapse navbar-right">
                                <ul class="nav navbar-nav">
                                    <li class="active"><a href="#top">Nyitó</a></li>
                                    <li><a href="#about">Esemény bemutatása</a></li>
                                    <li><a href="#content-boxes">Program</a></li>
                                    <li><a href="#team">Előadók</a></li>
                                    <li><a href="#contact">Jelentkezés</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </nav>
                <!-- ## nav end ## -->
                </header>
                <!-- ## header end ## -->
        <!-- ## content boxes start ## -->
        <section id="content-boxes" class="scrollblock">
            <div class="container">
                <div class="row">

                   <ul class="bxslider">
					  <li><img src="prog1.jpg" /></li>
					  <li><img src="prog2.jpg" /></li>
					  <li><img src="prog3.jpg" /></li>
					  <li><img src="prog4.jpg" /></li>
					  <li><img src="prog5.jpg" /></li>
					  <li><img src="prog6.jpg" /></li>
					  <li><img src="prog7.jpg" /></li>
					</ul>
                </div>
            </div>
        </section>
        <!-- ## content boxes end ## -->
        
         <!-- ## parallax section 2 start ## -->
        <section id="parallax-section-2">
            <!-- ## parallax background 2 ## -->
            <div class="bg2"></div>
            <!-- ## parallax background pattern 2 start ## -->
            <div class="bg-pattern2" style="background: none;">
                <!-- ## team section start ## -->
                <section id="team" class="scrollblock_team">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <h2>Előadók
                             <span class="line"><span class="dot"></span></span>
                                </h2>
                            </div>
                        </div>
                        <div class="row0">
                            <div class="col-md-3 col-sm-6">
                                <div class="col-lg-12 member-box">
                                    <div class="member-img">
                                        <a href="#"><img class="img-responsive" src="assets/images/team/pic_1.jpg" alt=".img-responsive" /></a>
                                    </div>
                                    <div class="member-data">
                                        <div class="member-info">
                                            <div class="member-name"><a href="#">Dr. Zacher Gábor</a></div>
                                            <div class="member-title">addiktológus a homokba dugott fejekről és lelkekről.</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="member-content">Zacher Gábor az országaddiktológusa, szókimondásáért szeretjük.  Arról fog beszélni a HR PIKNIK-en, hogy a vezetéssel járófeszültség enyhítésére először az alkohol a válasz, de a vezetők körében a rendszeres drogfogyasztás sem ritka.</div>
                                    </div>
                                    <div class="clearfix"></div>
                               </div>
                            </div>
                                
                                <!-- ## member box end ## -->
                            <div class="col-md-3 col-sm-6">
                                <div class="col-lg-12 member-box">
                                    <div class="member-img">
                                        <a href="#"><img class="img-responsive" src="assets/images/team/pic_2.jpg" alt=".img-responsive" /></a>
                                    </div>
                                    <div class="member-data">
                                        <div class="member-info">
                                            <div class="member-name"><a href="#">Dr. Várkonyi Andrea </a></div>
                                            <div class="member-title"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="member-content">A Tények műsorvezetője két éve kezdte meg pszichológiai tanulmányait, mert előbb-utóbb újra emberekkel szeretne foglalkozni, konkrtétan megromlott párkapcsolatok rendbetételével. Neki is kedvenctémája a JÁTSZMÁZÁS, erről fog beszélni  a HR PIKNIKEN.</div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                                <!-- ## member box end ## -->
                            <div class="col-md-3 col-sm-6">
                                <div class="col-lg-12 member-box">
                                    <div class="member-img">
                                        <a href="#"><img class="img-responsive" src="assets/images/team/pic_3.jpg" alt=".img-responsive" /></a>
                                    </div>
                                    <div class="member-data">
                                        <div class="member-info">
                                            <div class="member-name"><a href="#">Szabó György </a></div>
                                            <div class="member-title">a Sanoma Lapkiadó volt vezérigazgatója</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="member-content">STORY, NŐK LAPJA, BEST, STARTLAP, PROFESSION.HU. Szabó György 22 évig volt a Sanoma, a hazai lapkiadó piac legnagyobb kiadójának vezérigazgatója, de dolgozott régiós vezetőként is a holland, majd később finn multinál. Beosztottjai karizmatikus vezetőként  tartják számon a mai napig, elfogadottsága, támogatottsága mindig magas volt cégenbelül. Hogyan tudta megőrizni hatalmát? Mi az, amit jól csinált tart és mi az, amit nem csinálna úgy, ahogy akkor tette? Előfordult-e, hogy félelemből kontrollált? Ezekről beszél nekünk.	</div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                                <!-- ## member box end ## -->
                                
                            <div class="col-md-3 col-sm-6">
                                <div class="col-lg-12 member-box">
                                    <div class="member-img">
                                        <a href="#"><img class="img-responsive" src="assets/images/team/pic_6.jpg" alt=".img-responsive" /></a>
                                    </div>
                                    <div class="member-data">
                                        <div class="member-info">
                                            <div class="member-name"><a href="#">Kiss Péter</a></div>
                                            <div class="member-title">RESTART alapító, volt médiaszakember (Tv2 csoport programigazgató)</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="member-content">Kiss Péter összesen 10 évig volt felsővezető a médiában, mielőtt megalapította a RESTARTOT! Szinte végig kreatív területen, kreatív emberek vezetésével foglalkozott, akkor és most is azt tartotta legfontosabb feladatának, hogy bevonva az embereket megmutassa, lehet kreatívan, alkotóan élni, és ez a jó érzésen túl anyagilag is nagyon profitábilis egyéni és cég szinten is.</div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                                <!-- ## member box end ## -->
                         </div>
                     </div>
                      <div class="container">
                         <div class="row0">
                            <div class="col-md-3 col-sm-6">
                                <div class="col-lg-12" style="
                                  -webkit-background-clip: border-box;
								  -webkit-background-origin: padding-box;
								  -webkit-background-size: auto;
								  -webkit-transition-delay: 0s;
								  -webkit-transition-duration: 0.2s;
								  -webkit-transition-property: all;
								  -webkit-transition-timing-function: ease-in-out;
								  background-attachment: scroll;
								  background-clip: border-box;
								  background-color: rgba(255, 228, 0, 0.8);
								  background-image: none;
								  background-origin: padding-box;
								  background-size: auto;
								  box-sizing: border-box;
								  color: rgb(51, 51, 51);
								  cursor: pointer;
								  display: block;
								  float: left;
								  font-family: Aldrich;
								  font-size: 14px;
								  height: 100%;
								  line-height: 20px;
								  margin-bottom: 10px;
								  margin-left: 0px;
								  margin-right: 0px;
								  margin-top: 10px;
								  min-height: 1px;
								  opacity: 1;
								  padding-bottom: 10px;
								  padding-left: 15px;
								  padding-right: 15px;
								  padding-top: 6px;
								  position: relative;
								  transition-delay: 0s;
								  transition-duration: 0.2s;
								  transition-property: all;
								  transition-timing-function: ease-in-out;
								  width: 255px;
                                ">
                                    <div class="member-img">
                                        <a href="#"><img class="img-responsive" src="assets/images/team/pic_7.jpg" alt=".img-responsive" /></a>
                                    </div>
                                    <div class="member-data">
                                        <div class="member-info">
                                            <div class="member-name"><a href="#"><font style="font-family:Aldrich; color:#000000; text-size:18px;"><strong>Rakonczay Viktória</strong></font></a></div>
                                            <div class="member-title">Igazi álmodozó</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="member-content">aki 22 évesen keresztül evezte férjével az Atlanti Óceánt. Egy kenuval, amit saját kezükkel építettek. Olyan négy napos viharban volt részük, hogy a mentő hajó nem vállalta a kimentésüket. Milyenaz, amikor ennyire közel vagy ahhoz, hogy meg halhatsz? Hogy tudsz szembenézni legnagyobb félelmeiddel? Mit tanulsz ebből? Mit tudsz felhasználni a civil életedben?</div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="col-lg-12" style="
                                  -webkit-background-clip: border-box;
								  -webkit-background-origin: padding-box;
								  -webkit-background-size: auto;
								  -webkit-transition-delay: 0s;
								  -webkit-transition-duration: 0.2s;
								  -webkit-transition-property: all;
								  -webkit-transition-timing-function: ease-in-out;
								  background-attachment: scroll;
								  background-clip: border-box;
								  background-color: rgba(255, 228, 0, 0.8);
								  background-image: none;
								  background-origin: padding-box;
								  background-size: auto;
								  box-sizing: border-box;
								  color: rgb(51, 51, 51);
								  cursor: pointer;
								  display: block;
								  float: left;
								  font-family: Aldrich;
								  font-size: 14px;
								  height: 100%;
								  line-height: 20px;
								  margin-bottom: 10px;
								  margin-left: 0px;
								  margin-right: 0px;
								  margin-top: 10px;
								  min-height: 1px;
								  opacity: 1;
								  padding-bottom: 10px;
								  padding-left: 15px;
								  padding-right: 15px;
								  padding-top: 6px;
								  position: relative;
								  transition-delay: 0s;
								  transition-duration: 0.2s;
								  transition-property: all;
								  transition-timing-function: ease-in-out;
								  width: 255px;
                                ">
                                    <div class="member-img">
                                        <a href="#"><img class="img-responsive" src="assets/images/team/pic_8.jpg" alt=".img-responsive" /></a>
                                    </div>
                                    <div class="member-data">
                                        <div class="member-info">
                                            <div class="member-name"><a href="#"><font style="font-family:Aldrich; color:#000000; text-size:18px;"><strong>Magyar Miklós</strong></font></a></div>
                                            <div class="member-title">HR-igazgató, Xerox Közép-Kelet Európa, Törökország és Izrael régió</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="member-content">Magyar Miklós a BAT és a General Electric utánérkezett a Xerox-hoz. Nemzetközi és multikulturális környezetben dolgozik együtt HR-es munkatársaival és a régió üzleti vezetőivel. Szeret konstruktívan vitatkozni, új megoldásokat keresni és építkezni.</div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
            
                            <div class="col-md-3 col-sm-6">
                                <div class="col-lg-12" style="
                                  -webkit-background-clip: border-box;
								  -webkit-background-origin: padding-box;
								  -webkit-background-size: auto;
								  -webkit-transition-delay: 0s;
								  -webkit-transition-duration: 0.2s;
								  -webkit-transition-property: all;
								  -webkit-transition-timing-function: ease-in-out;
								  background-attachment: scroll;
								  background-clip: border-box;
								  background-color: rgba(255, 228, 0, 0.8);
								  background-image: none;
								  background-origin: padding-box;
								  background-size: auto;
								  box-sizing: border-box;
								  color: rgb(51, 51, 51);
								  cursor: pointer;
								  display: block;
								  float: left;
								  font-family: Aldrich;
								  font-size: 14px;
								  height: 100%;
								  line-height: 20px;
								  margin-bottom: 10px;
								  margin-left: 0px;
								  margin-right: 0px;
								  margin-top: 10px;
								  min-height: 1px;
								  opacity: 1;
								  padding-bottom: 10px;
								  padding-left: 15px;
								  padding-right: 15px;
								  padding-top: 6px;
								  position: relative;
								  transition-delay: 0s;
								  transition-duration: 0.2s;
								  transition-property: all;
								  transition-timing-function: ease-in-out;
								  width: 255px;
                                ">
                                    <div class="member-img">
                                        <a href="#"><img class="img-responsive" src="assets/images/team/pachgabriella.jpg" alt=".img-responsive" /></a>
                                    </div>
                                    <div class="member-data">
                                        <div class="member-info">
                                            <div class="member-name"><a href="#"><font style="font-family:Aldrich; color:#000000; text-size:18px;"><strong>Pach Gabriella</strong></font></a></div>
                                            <div class="member-title">HR szakember, coach, mentor </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="member-content">Magát "iparággyűjőnek" mondja, hiszen közel két évtizedes szakmai múltja során hat iparágban dolgozott HR-esként. Utóbbi hét munkakörében HR közép- és felsővezetői pozíciókat töltött be. Számára a munka egyben szakma és hobbi is. Célja, hogy csapatával és az üzleti vezetőkkel szorosan együttműködve, az üzlet számára értékes, maradandó és fenntartható HR megoldásokat alakítson ki. </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
            
                        </div>
                     </div>
                    
                     
                </section>
                <!-- ## team section end ## -->
            </div>
            <!-- ## parallax background pattern 2 end ## -->
        </section>
        <!-- ## parallax section 2 end ## -->
      
        
                <!-- ## about section start ## -->
                <section id="about" class="scrollblock">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 text-center about-box">
                                <h2>Program
                            <span class="line"><span class="dot"></span></span>
                                </h2>
                                <p>
                                  <?
                                  $text='<span style="color:#f7941d;font-weight:bold;">Hatalommánia! <br />
                                  függőség?</span><br /><br />
                                  A hatalom gyakorlása szerves része a vezetésnek. Azt jelenti, fel vagyok hatalmazva arra, hogy döntéseket hozzak, irányítsak, befolyásoljak. Pozitív, teremtő, de negatív lehúzó energia is lehet. Az első számú vezető viselkedése, hatalomhoz való hozzáállása egy egész szervezet, cég életét befolyásolja. Ha egy vezető zárkózott, fél, agresszív, túlzott kontrollra törekszik az nemcsak magát, de a környezetét is mérgezi, bénítja. Ha nyitott, pozitív, és bátor, akkor felemeli munkatársait is.<br /><br />
                                  <strong>Három előadónk:</strong><br />1. Egy őszinte vezető - Szabó György, a Sanoma Lapkiadó volt vezérigazgatója 22 évnyi vezetői tapasztalatát, érzéseit osztja meg velünk.<br />2. Borba, porba fojtott félelmek - Dr. Zacher Gábor addiktológus a homokba dugott fejekről és lelkekről.<br />3. Integritással legyőzhető félelmek - Rakonczay Viktória kenus, aki szembenézett a halállal miközben parányi kenuján átevezte az Atlanti Óceánt. 
                                  <br /><br /> <span style="color:#f7941d;font-weight:bold;">A HR 50 árnyalata.  <br />
                                  HOGY egyszerre kezdeményező és támogató is legyen a hr</span><br /><br />
                                  Hol a határ a vezetői és a HR feladatok között például az emberek vezetése, fejlesztése és motiválása kapcsán? Adminisztrátor, belső tanácsadó, szürke eminenciás, üzletfejlesztő és hangulatfelelős – melyik szerepben vagyunk? Végrehajtunk, befolyásoljuk a történéseket és konfrontálódunk is az üzleti vezetéssel ha kell? Bár a végletek léteznek, a válaszok sohasem fekete-fehérek.<br /> HOGYAN LEHET MEGTALÁLNI A HELYES UTAT ÉS ELKERÜLNI A CSAPDÁKAT?<br /><br /> Előadás - Eszmecsere - Workshop <br />Pach Gabriella  HR szakember, coach, mentor, és Magyar Miklós a XEROX régiós HR igazgatója vezetésével.<br /><br />Hogyan mondjon nemet a HR? Délutáni workshopunk alatt, interaktív és játékos módon keresünk új és régi megoldásokat, eszközöket és szűrjük le a tanulságokat olyan valódi helyzetek kapcsán, ahol a HR nem egészen ért egyet az üzleti vezetéssel.
                                  <br /><br /> <span style="color:#f7941d;font-weight:bold;">ELVESZETT JELENTÉSEK,<br />
                                  AZAZ MIÉRT ÉRTJÜK FÉLRE EGYMÁST FOLYAMATOSAN?!</span><br /><br />
                                  Főnök a beosztottnak a megbeszélés végén:<br />F. Akkor minden világos, és kész leszel az anyaggal holnapig?<br />B: Már miért ne lennék?<br />F: …Rendben, várom reggel 9-re legyen az asztalomon.<br />B: Már mondtad. (durrogva el)<br /><br />Rengeteg ilyen párbeszéd zajlik a munkahelyeken. A szavak szintjén egy ártatlan beszélgetésnek tűnik, azonban mindkét fél számára kellemetlen a helyzet. A kimondatlan (rejtett) üzenetek vezetnek a játszmákhoz. Hogy vegyük észre, ha belesodródtunk a játszmába? És hogy kavarodjunk ki belőle?<br /><br />Kiss Péter RESTART alapító/coach előadása. Vendég a pszichológia felé forduló Dr. Várkonyi Andrea.Interaktív előadás és workshop.<br /><br />HOGY TÖRJÜK MEG A JÁTSZMÁINKAT?<br /><br />A délutáni workshopon minden résztvevő behoz egy számára kényelmetlen beszélgetést, aminek közösen fogjuk megfejteni a JÁTSZMAKÉPLETÉT, és adunk megoldási javaslatokat.<br />';
                                  
                                  //echo iconv("utf-8","iso-8859-2",$text);
                                  echo $text;
                                  
                                  ?>
                                </p>
                            </div>
                        </div>

                     
                    </div>
                </section>
                <!-- ## about section end ## -->
            </div>
            <!-- ## parallax bg pattern 1 end ## -->
        </section>
        <!-- ## parallax section 1 end ## -->



       
        
        <!-- ## contact section start ## -->
        <section id="contact" class="scrollblock">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h2>Jelentkezés
                        <span class="line"><span class="dot"></span></span>
                        </h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 contact-form contact-box">
                        <h4>Jelentkezzen a HR Piknikre!</h4>
                        <p>
						A RESTART ÉS A HRPORTAL BEMUTATJA: HRPIKNIK<br>
						(3 TÉMA 1 NAPON, Előadás, kerekasztal, workshop)
						</p>
						<p>
							<b>A workshop díja 19 500 Ft +ÁFA/fő.</b><br />
A díj tartalmazza az ebédet és a napközbeni finomságokat, frissítőket.
						</p>
						
                      <form name="form-contact-us" id="form-contact-us">
                       
                            <div class="form-group">
								
                            Online jelentkezéséről e-mailben visszaigazolást küldünk. Amennyiben a visszaigazoló levélben rákattint az aktiváló linkre, azzal elfogadja az adatok helyességét. Ezt követően a jóváhagyott adatok alapján átutalásos számlát küldünk a részvételi díjról, melyet kérünk a regisztráció visszaigazolásától számított 8 munkanapon belül - de legkésőbb a rendezvény napjáig - kiegyenlíteni. Az online regisztráció megrendelésnek minősül.
							<br><br>A rendezvényen a részvétel feltétele a részvételi díj előzetes kiegyenlítése. Lemondást a részvételi díj teljes visszafizetése mellett a rendezvény napja előtti 5. munkanapon 12 óráig - KIZÁRÓLAG ÍRÁSBAN a info@hrportal.hu címre - fogadunk el. Az ezutáni lemondások esetében a részvétel átruházható, de a teljes díjat ki kell fizetni. Telefonos vagy egyéb szóbeli lemondásokat nem áll módunkban elfogadni.
							<br><br>A lemondási határidő utáni regisztráció esetén lemondásra nincs lehetőség.
                            </div>
							<div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="txtName">Név: </label>
                                        <input type="text" placeholder="Név"
                                            class="form-control required" id="txtName" name="txtName" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="txtEmail">E-mail:</label>
                                        <input type="text" placeholder="E-mail ID"
                                            class="form-control required email" id="txtEmail" name="txtEmail" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="txtphone">Telefonszám:</label>
                                        <input type="text" placeholder="Telefonszám"
                                            class="form-control required phone" id="txtphone" name="txtphone" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="txtszlaname">Számlázási név:</label>
                                        <input type="text" placeholder="Számlázási név"
                                            class="form-control " id="txtszlaname" name="txtszlaname" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="txtszlaaddr">Számlázási cím:</label>
                                        <input type="text" placeholder="Számlázási cím"
                                            class="form-control " id="txtszlaaddr" name="txtszlaaddr" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="txtMessage">Üzenet:</label>
                                <textarea placeholder="Message" class="form-control required"
                                    id="txtMessage" name="txtMessage" rows="10"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="txtCaptcha"></label>
                                <input
                                    type="text" class="form-control required captcha"
                                    placeholder="Egy keresztkérdés?" id="txtCaptcha" name="txtCaptcha" />
                            </div>
                            <div class="form-group">
                                <input type="button" id="btn-Send" value="Küld" class="btn btn-theme-inverse" />
                            </div>
                            <div class="col-lg-12" id="contact-form-message">
                        </form>
                        <!-- ## contact form end ## -->
                    </div>
                    </div>

                    <div class="col-sm-6 contact-info contact-box">
                        <h4>Elérhetőségek</h4>
                        <!-- ## contact info start ## -->
                        <ul class="fa-ul">
                            <li><i class="fa-li fa fa-clock-o text-theme-inverse"></i><span>Időpont: 2015. november 03. 9:00</span></li>
                            <li><i class="fa-li fa fa-home text-theme-inverse"></i><span>1095 Budapest Komor Marcell u. 1.</span></li>
                            <li><i class="fa-li fa fa-envelope text-theme-inverse"></i><span>info@hrportal.hu</span></li>
                            <li><i class="fa-li fa fa-globe text-theme-inverse"></i><span>www.hrportal.hu/hrpiknik_2015/</span></li>
                        </ul>
                        <!-- ## contact info endstart ## -->
                        <!-- ## map start ## -->
<div id="map-canvas"></div>
                        <!-- ## map end ## -->
                    </div>
                </div>
            </div>
        </section>
        <!-- ## contact section end ## -->

        <!-- ## blockquote section start ## -->
        <section id="bquote" class="scrollblock">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-lg-offset-1 quote-box">
                        <blockquote>
                            <p>Jelentkezzen programunkra!</p>
                        </blockquote>
                    </div>
                </div>
            </div>
        </section>
        <!-- ## blockquote section end ## -->


        <!-- ## twitter section end ## -->

        <!-- ## footer section start ## -->
        <section id="footer">
            <!-- ## footer background pattern start ## -->
            <div class="bg-pattern3">

                <!-- ## footer copyright start ## -->
                <div class="footer-copyright text-center">
                    HRPortal.hu Copyright &copy; 2015, All rights reserved.
                </div>
                <!-- ## footer copyright end ## -->
            </div>
            <!-- ## footer background pattern end ## -->
        </section>
        <!-- ## footer section end ## -->

        <!-- ## preloader start ## -->
        <div id="preloader">
            <div id="status"><span class="fa fa-spin fa-spinner fa-5x"></span></div>
        </div>
        <!-- ## preloader end ## -->

        <!-- ## Time Bar begins here ## -->
        <!-- ## do not change it. ## -->
        <div id="progress-back" class="load-item">
            <div id="progress-bar"></div>
        </div>
        <!-- ## Time Bar ends here ## -->
    </div>
    <!-- ## page wrapper end ## -->

    <script src="assets/js/jquery-2.0.0.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/twitter/jquery.tweet.min.js" type="text/javascript"></script>
    <script src="assets/js/jquery.parallax-1.1.3.js" type="text/javascript"></script>
    <script src="assets/js/jquery.nicescroll.min.js" type="text/javascript"></script>
    <script src="assets/js/detectmobilebrowser.js" type="text/javascript"></script>
    <script src="assets/js/jquery.smooth-scroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.scrollorama.js" type="text/javascript"></script>
    <script src="assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="assets/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="assets/js/jquery.cookie.js" type="text/javascript"></script>
    <script src="assets/ytplayer/js/jquery.mb.YTPlayer.js" type="text/javascript"></script>
    <script src="assets/js/app.variables.js" type="text/javascript"></script>
    <!--<script src="assets/js/jquery.countdown.js" type="text/javascript"></script>
    <script src="assets/js/app.subscribe.js" type="text/javascript"></script>
    <script src="assets/js/app.contact.js" type="text/javascript"></script>
    <script src="assets/js/app.ui.js" type="text/javascript"></script>
    <script src="assets/js/app.styleswitcher.js" type="text/javascript"></script>-->
    <script src="/js/jquery.bxslider.min.js"></script>

    <script src="assets/js/app.landlite.js" type="text/javascript"></script>
</body>
</html>
