﻿// set twitter username
var twitter_username = 'envato';

// countdown date time format
// '30 Nov 2014, 11:00:00'
var countdown_timer = '30 Nov 2014, 11:00:00';

// set true/false to show/hide the countdown timer
var showTimer = false;

// set one of the following parameter to true for enabling that feature
// set rest of the parameters to false
var isSliderEnabled = true;
var isVideoBgEnabled = false;
var isParallaxEnabled = true;
var isSolidColorEnabled = true;

// set hex color code for fixed solid color background
var solidColor = '#cc0000';

// set the youtube video URL for video background
var youtube_video = 'http://www.youtube.com/watch?v=UWb5Qc-fBvk';

// show/hide style switcher
// note: if style switcher is true then it will not 
// allow the fixed background effect. Set the above parameters (except timer) and 
// switcher to false to enable fixed bg effect.
var showHideStyleSwitcher = false;
