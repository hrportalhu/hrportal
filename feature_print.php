<?php
include_once ("inc/sys.conf");
include_once ("inc/_functions.php");

global $db;
header('Content-Type: text/html;charset=utf-8');

$id=$_GET['id'];
 
 $res_article=mysql_query("SELECT
                              a.id AS a_id,
                              a.teaser AS a_teaser,
                              a.lead AS a_lead,
                              a.body AS a_body,
                              a.small_pic AS a_small_pic,
                              a.small_pic_align AS a_small_pic_align,
                              a.large_pic AS a_large_pic,
                              a.large_pic_align AS a_large_pic_align,
                              a.date_last_modified AS a_date_last_modified,
                              a.nl2br AS a_nl2br,
                              a.source AS a_source,
                              c.name AS c_name,
                              cw.shown_name AS cw_shown_name
                             FROM
                              (features a LEFT JOIN f_categories c ON a.category_id=c.id)
                             LEFT JOIN
                              contentwriters cw ON a.contentwriter_login=cw.login
                             WHERE
                              a.is_approved='yes'
                             AND
                              a.id='$id'") or die(mysql_error());
                              
 if (mysql_num_rows($res_article)==1)
  {
   $article=mysql_fetch_array($res_article);
   mysql_query("UPDATE features SET click_counter=click_counter+1 WHERE id='$id'");
  }
 else
  $article=false;       
                  
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
    <title>HR Portal - <?=nl2br($article['a_teaser'])?></title>
    <link rel="STYLESHEET" type="text/css" href="style.css">
</head>
<body bgcolor='#ffffff' topmargin='0' leftmargin='0' marginheight='0' marginwidth='0' text='#000000' link='#000000' vlink='#000000' alink='#000000'>
<table width='100%' cellpadding='0' cellspacing='11' border='0'>
 <tr>
  <td><img src="images/hrportal_logo_100x50.gif" alt="" border="0"></td>
 </tr> 
 <tr>
  <td valign='top'>
  <span class='teaser'><?=nl2br($article['a_teaser'])?></span><br>
  <p align='justify' class='lead'>
   <?php
    if ($article['a_nl2br']=='yes')
     echo nl2br($article['a_lead']);
    else
     echo $article['a_lead'];
   ?>
  </p>
  <p align='justify' class='body'>
   <?php
    if ($article['a_nl2br']=='yes')
     echo nl2br($article['a_body']);
    else
     echo $article['a_body'];
   ?>
  </p>
  <br>
  <br>
  <table cellspacing='0' cellpadding='0' border='0' width='100%'>
   <tr>
	<td align='left'>  
     <?php
       if ($article['a_source']!='')
       echo "<div align='left' class='body'><i>Forrás: ".$article['a_source']."</i></div>";
     ?>
	</td>
   </tr>
  </table>
 </td>
</table>
<script type="text/javascript" language="javascript1.2">
if (typeof(window.print) != 'undefined') {
    window.print();
}
</script>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-696329-1";
urchinTracker();
</script>
</body>
</html>
<?php mysql_close($db); ?>
