﻿<!DOCTYPE html>
<html lang="en">
<head>
    
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-2">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>HR PORTÁL - KRS MUNKAJOGI KONFERENCIA - 2016</title>

    <!-- ## Bootstrap ## -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="assets/css/supersized.css" rel="stylesheet" />
    <link id="skin-styler" href="assets/css/skin1.css" rel="stylesheet" />
    <link href="assets/css/styleswitcher.css" rel="stylesheet" />
    <link href="assets/ytplayer/css/YTPlayer.css" rel="stylesheet" />
    <link type="text/css" href='http://fonts.googleapis.com/css?family=Aldrich' rel="stylesheet" />
    <!-- bxSlider CSS file -->
<link href="js/jquery.bxslider.css" rel="stylesheet" />
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js" type="text/javascript"></script>
      <script src="assets/js/respond.min.js" type="text/javascript"></script>
    <![endif]-->

    <link rel="apple-touch-fa-precomposed" sizes="144x144" href="assets/ico/apple-touch-fa-144-precomposed.png" />
    <link rel="shortcut icon" href="assets/ico/favicon.ico" />
    
    <style>
      #map-canvas {
        width: 500px;
        height: 400px;
      }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script>
      function initialize() {
        var mapCanvas = document.getElementById('map-canvas');
        var mapOptions = {
          center: new google.maps.LatLng(47.4695946,19.071223),
          zoom: 14,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(mapCanvas, mapOptions)
		  marker = new google.maps.Marker({
			position: new google.maps.LatLng(47.4695946, 19.071223),
			map: map
		  });
      }
      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
<script type="text/javascript">



function checkEmail(emailstr){
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailstr)){
		return (true)
	}
	return (false)
 } 

function do_submitlanding(){
	var f=document.forms.frm_landing;
	if(f.allok.checked==1){
		if(f.email.value!="" && f.nev1.value!="" && f.cegnev.value!="" && f.szlacim.value!="" && f.postcim.value!="" && f.telefon.value!=""){
			if (checkEmail(f.email.value)){
				f.submit();
			}
			else{
				alert("Nem megfelelő emailcím formátum!");
			}
		}
		else{
				alert("A kötelező mezőket ki kell tölteni!!");
		}
	}
	else{
		alert("A jelentkezéshez tudomásul kell venni a feltételeket!");
	}
}
</script>
<?
$sending="0";
if(isset($_POST['landingsend'])){
	$check=db_one("select id from wworkshop_krs_2016 where nev1='".$_POST['nev1']."' and email='".$_POST['email']."' and sessid='".session_id()."'");
	if($check==""){
	$bele="";
	$osszes=0;
		if(strlen($_POST['nev1'])>1){
			if(isset($_POST['tcheck1'])){
				$osszes+=19500;
				$bele+=$_POST['nev1'].": 19 500 Ft\n";
	
			}
			else{
				if(strlen($_POST['nev2'])>1 && strlen($_POST['nev2'])>1){
					$osszes+=19500;
					$bele+=$_POST['nev1'].": 19 500 Ft\n";
			
				}
				else{
					$osszes+=19500;
					$bele+=$_POST['nev1'].": 19 500 Ft\n";
			
				}
			}
	
			if(strlen($_POST['nev2'])>1 && strlen($_POST['nev1'])>1){
			if(isset($_POST['tcheck2'])){
				$osszes+=19500;
				$bele+=$_POST['nev2'].": 19 500 Ft\n";
	
			}
			else{
				$osszes+=19500;
				$bele+=$_POST['nev2'].": 19 500 Ft\n";
				}
			
			}
			if(strlen($_POST['nev3'])>1 && strlen($_POST['nev1'])>1){
			if(isset($_POST['tcheck3'])){
				$osszes+=19500;
				$bele+=$_POST['nev3'].": 19 500 Ft\n";
	
			}
			else{
				$osszes+=19500;
				$bele+=$_POST['nev3'].": 19 500 Ft\n";
				}
			
			}
	

		}
		
	//	db_execute("insert into workshop_landing_2015 (nev1,nev2,nev3,cegnev,szlacim,postcim,email,telefon,megjegyzes,sessid,regdate) 
	//	values('".$_POST['nev1']."','".$_POST['nev2']."','".$_POST['nev3']."','".$_POST['cegnev']."','".$_POST['szlacim']."','".$_POST['postcim']."','".$_POST['email']."','".$_POST['telefon']."','".$_POST['megjegyzes']."','".session_id()."',NOW())");
		$sysmessage="Kedves ".$_POST['nev1']."!\n\nKöszönjük jelentkezését a 2015. november 03-án 09:00-kor a MÜPA-ban megrendezendő HR Piknikünkre.\n\nA jelentkezés jóváhagyásához kérjük kattintson erre a linkre http://www.hrportal.hu/index.phtml?page=landing_2015_hrpiknik&p=jelentkez&lsid=".session_id()."\n\nJelentkezési és számlázási adatai a következők:\n\n1. személy neve: ".$_POST['nev1']."\n";if($_POST['nev2']!=""){$sysmessage.="2. személy neve: ".$_POST['nev2']."\n";}if($_POST['nev3']!=""){$sysmessage.="3. személy neve: ".$_POST['nev3']."\n";}$sysmessage.="E-mail: ".$_POST['email']."\nTelefonszám: ".$_POST['telefon']."\nCégnév: ".$_POST['cegnev']."\nSzámlázási cím: ".$_POST['szlacim']."\nPostázási cím: ".$_POST['postcim']."\nMegjegyzes:\n\n".$_POST['megjegyzes']."\n\nÖsszesen fizetendő:".$osszes."\n\nJelentkezési díjáról  hamarosan egy számlát állít ki és küld el Önnek a HRPortal.hu, melyet kérünk a regisztráció visszaigazolásától számított 8 munkanapon belül - de legkésőbb a rendezvény napjáig - kiegyenlíteni. Az online regisztráció megrendelésnek minősül.\n\nA rendezvényen a részvétel feltétele a részvételi díj előzetes kiegyenlítése. Lemondást a részvételi díj teljes visszafizetése mellett a rendezvény napja előtti 5. munkanapon 12 óráig - KIZÁRÓLAG ÍRÁSBAN az info@hrportal.hu címre - fogadunk el. Az ezutáni lemondások esetében a részvétel átruházható, de a teljes díjat ki kell fizetni. Telefonos vagy egyéb szóbeli lemondásokat nem áll módunkban elfogadni.\nA lemondási határidő utáni regisztráció esetén lemondásra nincs lehetőség.\n\nA programról és a helyszínről az alábbi linken tájékozódhat: http://www.hrportal.hu/index.phtml?page=landing_2015_hrpiknik\n\nÜdvözlettel,\n\nHRportal.hu";
		$cmmessage="Landing regisztráció történt!\n A regisztrációban ez a levél került az ügyfélnek kiküldésre\n\nKedves ".$_POST['nev1']."!\n\nKöszönjük jelentkezését a 2015. november 03-án 09:00-kor a MÜPA-ban megrendezendő HR Piknikünkre.\n\nA jelentkezés jóváhagyásához kérjük kattintson erre a linkre http://www.hrportal.hu/index.phtml?page=landing_2015_hrpiknik&p=jelentkez&lsid=".session_id()."\n\nJelentkezési és számlázási adatai a következők:\n\n1. személy neve: ".$_POST['nev1']."\n";if($_POST['nev2']!=""){$cmmessage.="2. személy neve: ".$_POST['nev2']."\n";}if($_POST['nev3']!=""){$cmmessage.="3. személy neve: ".$_POST['nev3']."\n";}$cmmessage.="E-mail: ".$_POST['email']."\nTelefonszám: ".$_POST['telefon']."\nCégnév: ".$_POST['cegnev']."\nSzámlázási cím: ".$_POST['szlacim']."\nPostázási cím: ".$_POST['postcim']."\nMegjegyzes:\n\n".$_POST['megjegyzes']."\n\nÖsszesen fizetendő:".$osszes."\n\nJelentkezési díjáról  hamarosan egy számlát állít ki és küld el Önnek a HRPortal.hu, melyet kérünk a regisztráció visszaigazolásától számított 8 munkanapon belül - de legkésőbb a rendezvény napjáig - kiegyenlíteni. Az online regisztráció megrendelésnek minősül.\n\nA rendezvényen a részvétel feltétele a részvételi díj előzetes kiegyenlítése. Lemondást a részvételi díj teljes visszafizetése mellett a rendezvény napja előtti 5. munkanapon 12 óráig - KIZÁRÓLAG ÍRÁSBAN az info@hrportal.hu címre - fogadunk el. Az ezutáni lemondások esetében a részvétel átruházható, de a teljes díjat ki kell fizetni. Telefonos vagy egyéb szóbeli lemondásokat nem áll módunkban elfogadni.\nA lemondási határidő utáni regisztráció esetén lemondásra nincs lehetőség.\n\nA programról és a helyszínről az alábbi linken tájékozódhat: http://www.hrportal.hu/index.phtml?page=landing_2015_hrpiknik\n\nÜdvözlettel,\n\nHRportal.hu";
		@mail("jozsef.szucs@planetstar.hu", "HR Portal - HRPiknik regisztráció történt!!", $cmmessage, "From: info@hrportal.hu");
		@mail("andras.markovics@gmail.com", "HR Portal - HRPiknik regisztráció történt!!", $cmmessage, "From: info@hrportal.hu");
		@mail("info@hrportal.hu", "HR Portal - HRPiknik regisztráció történt!!", $cmmessage, "From: info@hrportal.hu");
		@mail("edit.halmi@hrportal.hu", "HR Portal - HRPiknik regisztráció történt!!", $cmmessage, "From: info@hrportal.hu");
		
		@mail($_POST['email'], "HR Portal - HRPiknik regisztráció", $sysmessage, "From: info@hrportal.hu");
		$sending="1";
	}
}
if(isset($_GET['lsid'])){
	
	db_execute("update workshop_krs_2016 set confirm='1', confirm_date=NOW() where sessid='".$_GET['lsid']."' ");	
	$sending="2";
}

?>   
</head>
<body id="top" data-spy="scroll" data-target=".navbar" data-offset="200">
    <div class="bg-video"></div>
    <!-- ## page wrapper start ## -->
    <div id="page-wrapper">
        <!-- ## parallax section 1 start ## -->
        <section id="parallax-section-1">
            <!-- ## parallax background 1 ## -->
            <div class="bg1"></div>
            <!-- ## parallax background pattern 1 start ## -->
            <div >



                <!-- ## header start ## -->
                <header id="header">
                    <div class="container">
                        <div class="row"></div>
                    </div>
                    <!-- ## nav start ## -->
                <nav id="nav">
                    <div class="navbar navbar-inverse navbar-fixed-bottom" role="navigation">
                        <div class="container">

                            <div class="navbar-collapse collapse navbar-right">
                                <ul class="nav navbar-nav">
                                    <li class="active"><a href="#top">Nyitó</a></li>
                                    <li><a href="#about">Esemény bemutatása</a></li>
                                    <li><a href="#content-boxes">Program</a></li>
                                    <li><a href="#team">Előadók</a></li>
                                    <li><a href="#contact">Jelentkezés</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </nav>
                <!-- ## nav end ## -->
                </header>
                <!-- ## header end ## -->
                    <!-- ## about section start ## -->
                <section id="about" class="scrollblock">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 text-center about-box">
                                <h2>Program
                            <span class="line"><span class="dot"></span></span>
                                </h2>
                                <p>
                                  <?
                                  $text='

				<span style="color:#d5ecf3;font-weight:bold;">HR PORTÁL - KRS MUNKAJOGI KONFERENCIA</span> <br /><br />
				A hatályos magyar jogrendszer a folyamatos változások korát éli.  Ezen változások gyakran koncepcionális újításokat is jelentenek, melyekre példák az új Polgári törvénykönyv, illetve az új Munka törvénykönyve is. A változások velejárója a dolog természeténél fogva minden esetben az elbizonytalanodó jogalkalmazás, hiszen a jogi normák egyedi esetre való alkalmazásakor hosszú ideig nem áll rendelkezésre iránymutató, egységes bírói gyakorlat. A jogszabályok változását rendszerint újabb és újabb módosítások követik, melyek az időközben felvetődött, de el nem varrt kérdésekre igyekeznek megfelelő válaszokat adni. A munkajog területén a 2012. évi I.tv. hatályba lépésével következett be egy paradigmaváltás, mely a munkajog polgári joghoz való közelítésével fenekestől fordította fel a szabályozott területet. Ezzel egyidejűleg megjelent a szabályozás megváltoztatására, továbbfejlesztésére irányuló igény is. A gyakorlat szempontjából legfontosabb változásokat, illetve fókuszpontokat kívánja ez a rendezvény egyedülálló komplexitással bemutatni. A munkajogi helyzetjelentésen belül egyes területeket külön is kiemeltünk: ilyen a Kúria munkajogi gyakorlata, a kiküldött munkavállalókra vonatkozó megváltozó szabályozás, az adatvédelem, valamint a kollektív jogok gyakorlati alkalmazása. A munkajogi szabályozással összefüggésben hangsúlyosan meg kívánjuk jeleníteni a munkaügyi ellenőrzés területét is, hiszen szükséges látni, hogy melyek is azok az előírások, melyeknek a követését az állam különösen megköveteli.<br /><br />
A rendezvény a munkaügyi ellenőrzések 2015. évi tapasztalatinak bemutatásával indul. Dr. Bakos József főosztályvezető személyében olyan előadó mutatja be a területet, aki az adott területen hosszú évek vezetői tapasztalatával rendelkezik, s akinek ez a terület nem mellékesen ténylegesen a szívügye.<br /><br />
Ezt követően dr. Kéri Ádám ügyvéd, a VKF szakértői bizottságának résztvevője,a Munka törvénykönyve (küszöbön álló) módosításával összefüggésben összegzi a törvénnyel kapcsolatosan felmerült legfontosabb munkáltatói, munkavállalói, illetve jogalkalmazói problémákat. A konkrét változási irányokat dr. Berke Gyula tanszékvezető egyetemi docens, rektor helyettesmagyarázza el a hallgatóság részére, akinek előadását a jogszabály előkészítésével kapcsolatos személyes érintettségén, több évtizedestapasztalatain túlmenően élvezetes előadói stílusa is feledhetetlenné teszi.<br /><br />
A személyiségi jogok védelme a munkajog területén belül is egyre növekvő jelentőségre tesz szert. Ennek egyik területe a munkavállalók munkahelyi ellenőrzése, mely a munkáltató egyik munkaviszonyhoz tapadó jogosítványa. Az azonban nem minden esetben világos, hogy ezen jogosítványnak mik is a pontos keretei. Erre világít rá dr. Tamás Zita a szakember szemüvegén keresztül.<br /><br />
Míg az egyes munkavállalókat megillető un. egyéni jogokkal, illetve azok jelentőségével valamennyi szakember tisztában van, ez nem mondható el az un. kollektív jogokról. Nem véletlen ugyanakkor az, hogy ezen jogosultságokat mind az Alaptörvény, mind pedig a nemzetközi jog védelmébe veszi. A LIGA konföderáció főtitkár jogtanácsosa interpretálásával ezen terület gyakorlati működésébe pillanthatunk bele.<br /><br />
Az Európai Unió alapjait többek között a személyek és a szolgáltatások szabad mozgása jelenti. Amint arra a 96/71/EK irányelv is rámutat, a szolgáltatás nyújtásának szabadságából következik a szolgáltatás nyújtójának az a joga is, hogy saját országának a munkavállalóit munkavégzés céljából egy másik államba küldje. Ez a lehetőség azonban számos konfliktus forrása egyben, hiszen a fogadó országnak mások a szociális, illetve munkajogi alapjai is. Ezt a mechanizmust mutatja be egy olyan szakember, aki évek során keresztül vett részt a vonatkozó irányelvek felülvizsgálatában. A téma aktualitását a végrehajtási szabályok változása is adja.<br /><br />
A női foglalkoztatás alakulása specialitásokkal rendelkezik. A markáns munkajogi védelem okán e munkavállalói réteg valamennyi munkaerőpiacon kifejezett hátrányokkal küzd mind a munkaviszony létesítése, megszüntetése, mind pedig annak fennállása során. Ezzel együtt a legutóbbi gazdasági válság kivételes módon nem őket, hanem a férfiak által dominált szektorokat sújtotta. Ezt a folyamatot, illetve a jelenlegi hazai helyzetet mutatja be Szalai Piroska a szakember szemszögéből.<br /><br />
A rendezvény utolsó, rutinos előadója a Kúria munkajogi gyakorlatát, ezen belül a jogellenes munkaviszony megszüntetésre vonatkozó gyakorlatot mutatja be. A szabályok ezen a területen – elsősorban a munkavállalók hátrányára – a korábbiakhoz képest jelentősen megváltoztak. Nem véletlen ezért, hogy a szakszervezeteken kívül a terület más szereplői is a szabályozás szigorítása mellett kardoskodnak. Úgy tűnik, hogy a kormányzat is enyhíteni kíván a szabályozási szigoron.<br /><br />

				';
					
                                  //echo iconv("utf-8","iso-8859-2",$text);
                                  echo $text;
                                  
                                  ?>
                                </p>
                            </div>
                        </div>

                     
                    </div>
                </section>
        <!-- ## content boxes start ## -->
        <section id="content-boxes" class="scrollblock">
            <div class="container">
                <div class="row">

                   <ul class="bxslider">
					  <li><img src="prog1.jpg" /></li>
					  <li><img src="prog2.jpg" /></li>
					  <li><img src="prog3.jpg" /></li>
					  <li><img src="prog4.jpg" /></li>
					  <li><img src="prog5.jpg" /></li>
					  <li><img src="prog6.jpg" /></li>
					  <li><img src="prog7.jpg" /></li>
					  <li><img src="prog8.jpg" /></li>
					  <li><img src="prog9.jpg" /></li>
					  <li><img src="prog10.jpg" /></li>
					  <li><img src="prog11.jpg" /></li>
					  <li><img src="prog12.jpg" /></li>
					</ul>
                </div>
            </div>
        </section>
        <!-- ## content boxes end ## -->
        
         <!-- ## parallax section 2 start ## -->
        <section id="parallax-section-2">
            <!-- ## parallax background 2 ## -->
            <div class="bg2"></div>
            <!-- ## parallax background pattern 2 start ## -->
            <div class="bg-pattern2" style="background: none;">
                <!-- ## team section start ## -->
                <section id="team" class="scrollblock_team">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <h2>Előadók
                             <span class="line"><span class="dot"></span></span>
                                </h2>
                            </div>
                        </div>
                        <div class="row0">
                            <div class="col-md-3 col-sm-6">
                                <div class="col-lg-12 member-box">
                                    <div class="member-img">
                                        <a href="#"><img class="img-responsive" src="assets/images/team/dr_simon_attila_istvan.jpg" alt=".img-responsive" /></a>
                                    </div>
                                    <div class="member-data">
                                        <div class="member-info">
                                            <div class="member-name"><a href="#">DR. SIMON ATTILA ISTVÁN</a></div>
                                            <div class="member-title">MUNKAERŐPIACÉRT FELELŐS HELYETTES ÁLLAMTITKÁR - NEMZETGAZDASÁGI MINISZTÉRIUM</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="member-content">A helyettes államtitkár 2010-ben a Vidékfejlesztési Minisztériumban kezdte a közigazgatási pályafutását jogi és igazgatási helyettes államtitkárként. Jelenleg a Nemzetgazdasági Minisztérium munkaerő-piacért felelős helyettes államtitkára, aki ebben a minőségében a Nemzeti ILO Tanács aktív kormányzati képviselője is. Pályafutásába, valamint terveibe betekintést enged a HR Portálnak adott interjúja, melyet több sajtóorgánum is hivatkozási alapnak tekint.</div>
                                    </div>
                                    <div class="clearfix"></div>
                               </div>
                            </div>
                                
                                <!-- ## member box end ## -->
                            <div class="col-md-3 col-sm-6">
                                <div class="col-lg-12 member-box">
                                    <div class="member-img">
                                        <a href="#"><img class="img-responsive" src="assets/images/team/dr_bakos_jozsef.jpg" alt=".img-responsive" /></a>
                                    </div>
                                    <div class="member-data">
                                        <div class="member-info">
                                            <div class="member-name"><a href="#">DR. BAKOS JÓZSEF</a></div>
                                            <div class="member-title">FOGLALKOZTATÁSFELÜGYELETI FŐOSZTÁLY, FŐOSZTÁLYVEZETŐ - NEMZETGAZDASÁGI MINISZTÉRIUM</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="member-content">Dr. Bakos József 2010 októberétől az OMMF elnöke, 2012. január 1. napjától pedig a Nemzeti Munkaügyi Hivatal igazgatóhelyettese. Ezen túlmenően a Nemzeti ILO Tanács kormányzati oldalának szakmai képviselője. Az adott területtel fennálló több, mint két évtizedes, jórészt irányítói tapasztalata a munkaügyi ellenőrzési gyakorlat bemutatására egyedülálló módon alkalmassá teszi.</div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                                <!-- ## member box end ## -->
                            <div class="col-md-3 col-sm-6">
                                <div class="col-lg-12 member-box">
                                    <div class="member-img">
                                        <a href="#"><img class="img-responsive" src="assets/images/team/dr_berke_gyula.jpg" alt=".img-responsive" /></a>
                                    </div>
                                    <div class="member-data">
                                        <div class="member-info">
                                            <div class="member-name"><a href="#">DR. BERKE GYULA</a></div>
                                            <div class="member-title">REKTORHELYETTES, TANSZÉKVEZETŐ - PÉCSI TUDOMÁNYEGYETEM ÁLLAM- ÉS JOGTUDOMÁNYI KAR - MUNKAJOGIÉS TÁRSADALOMBIZTOSÍTÁSI JOGI TANSZÉK </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="member-content">Dr. Berke Gyula a Pécsi Tudományegyetem rektor helyettese, az Állam- és Jogtudományi Kar munkajogi és társadalombiztosítási tanszékének tanszékvezetője. Munkajogi, társadalombiztosítási, valamint európai munkajogi tárgyú elemzései a 90-es évek első felétől jelennek meg. Az azóta eltelt időszakban változatlan lelkesedéssel és intenzitással publikál, társszerzője több munkajogi kommentárnak, illetve az érintett területeken valamennyi szinten oktat. Elismert szakemberként az új Munkatörvénykönyve (2012. évi I.tv.) megalkotásában, illetve módosításában is részt vett. Végezetül rendszeres meghívottja munkajogi tárgyú rendezvényeknek, ahol élvezetes előadói stílusával magyarázza a laikus, illetve szakmai hallgatóság számára a folyamatos változásban lévő munkajogi szabályozás egyes elemeinek racionalitását.</div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                                <!-- ## member box end ## -->
                       
                                
                            <div class="col-md-3 col-sm-6">
                                <div class="col-lg-12 member-box">
                                    <div class="member-img">
                                        <a href="#"><img class="img-responsive" src="assets/images/team/dr_tanczos_rita.jpg" 	 /></a>
                                    </div>
                                    <div class="member-data">
                                        <div class="member-info">
                                            <div class="member-name"><a href="#">DR. TÁNCZOS RITA</a></div>
                                            <div class="member-title">BÍRÓ - KÚRIA</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="member-content">Dr. Tánczos Rita 1993-tól a Fővárosi Munkaügyi Bíróság, majd a Fővárosi Törvényszék, jelenleg pedig a Kúria közigazgatási és munkaügyi szakágának bírája. Nem csupán kiváló szakember, szakmai publikációk szerzője, hanem tapasztalt előadó is. Nevével szinte minden színvonalasabb munkajogi tárgyú képzésen találkozni lehet. Az utóbbi években előadások tucatján magyarázta az új Munka törvénykönyve rendelkezéseit, valamint az általa felvetett jogalkalmazói problémákat.</div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                                <!-- ## member box end ## -->
                         </div>
                     </div>
                     
                     
                       <div class="container">
						 <div class="row0">
							<div class="col-md-3 col-sm-6">
								<div class="col-lg-12 member-box2">
									<div class="member-img">
										<a href="#"><img class="img-responsive" src="assets/images/team/szalai_piroska.jpg" alt=".img-responsive"  /></a>
									</div>
									<div class="member-data">
										<div class="member-info">
											<div class="member-name"><a href="#"><font style="font-family:Aldrich; color:#000000; text-size:18px;"><strong>SZALAI PIROSKA</strong></font></a></div>
											<div class="member-title">MINISZTERI TANÁCSADÓ - NEMZETGAZDASÁGI MINISZTÉRIUM - BUDAPESTI VÁLLALKOZÁSFEJLESZTÉSI KÖZALAPÍTVÁNY - KURATÓRIUMI ELNÖK</div>
										</div>
										<div class="clearfix"></div>
										<div class="member-content">Szalai Piroska mérnök, korábban a nők munkaerő-piaci helyzetének javításáért felelős miniszteri biztos, jelenleg a Budapesti Vállalkozásfejlesztési Közalapítvány (BVK) kuratóriumának elnöke, illetve miniszteri tanácsadó. A BVK keretében többek között az innovatív, nagy növekedési potenciállal rendelkező, globális piacokat megcélzó startup mikro vállalkozások támogatását koordinálja.Szakterületén számos publikációja jelent meg, melyekre elemzések is előszeretettel hivatkoznak. Saját weboldala pedig színes és közérthető formában mutatja be a közvélemény számára egyébként nehezen átlátható és megérthető, foglalkoztatást érintő folyamatokat, trendeket.</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="col-lg-12 member-box2">
									<div class="member-img">
										<a href="#"><img class="img-responsive" src="assets/images/team/dr_keri_adam.jpg" alt=".img-responsive" /></a>
									</div>
									<div class="member-data">
										<div class="member-info">
											<div class="member-name"><a href="#"><font style="font-family:Aldrich; color:#000000; text-size:18px;"><strong>DR. KÉRI ÁDÁM</strong></font></a></div>
											<div class="member-title">MUNKAJOGI ÜZLETÁGVEZETŐ ÜGYVÉD - KRS ÜGYVÉDI IRODA</div>
										</div>
										<div class="clearfix"></div>
										<div class="member-content">A rendezvény fő moderátora, illetve előadója a KRS Ügyvédi Iroda munkajogi üzletágvezető ügyvédje. Az egyéni, valamint kollektív munkajogon túlmenően adatvédelemmel foglalkozik, valamint a Nemzetközi Munkaügyi Szervezet (ILO) vonatkozásában a LIGA szakszervezeti konföderáción keresztül munkavállalói delegált. Munkájával összefüggésben részt vett a Versenyszféra és a Kormány Állandó Konzultációs Fóruma (VKF) keretében a Munka törvénykönyve módosításának előkészítésében is. A fentieken túlmenően a HR Portál jogi szakértője, valamint az Adózóna munkajogi szakértője. Elemzései számos más napi, illetve hetilapban rendszeresen megjelennek.</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="col-lg-12 member-box2" >
									<div class="member-img">
										<a href="#"><img class="img-responsive" src="assets/images/team/dr_tamas_zita.jpg" alt=".img-responsive" /></a>
									</div>
									<div class="member-data">
										<div class="member-info">
											<div class="member-name"><a href="#"><font style="font-family:Aldrich; color:#000000; text-size:18px;"><strong>DR. TAMÁS ZITA</strong></font></a></div>
											<div class="member-title">ÜGYVÉD - KRS Ügyvédi Iroda </div>
										</div>
										<div class="clearfix"></div>
										<div class="member-content">Dr. Tamás Zita a kereskedelmi jog mellett a szellemi alkotások jogára, valamint az adatvédelemre szakosodott ügyvéd a KRS Ügyvédi Iroda csapatában. Ezen témakörökben üzleti reggelik aktív előadója, illetve kiváló szakmai elemzések tucatjainak a szerzője az Origo internetes portálon.</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="col-lg-12 member-box2" >
									<div class="member-img">
										<a href="#"><img class="img-responsive" src="assets/images/team/dr_bodgar_ildiko.jpg" alt=".img-responsive" /></a>
									</div>
									<div class="member-data">
										<div class="member-info">
											<div class="member-name"><a href="#"><font style="font-family:Aldrich; color:#000000; text-size:18px;"><strong>DR. BODGÁL ILDIKÓ</strong></font></a></div>
											<div class="member-title">JOGÁSZ - NEMZETGAZDASÁGI MINISZTÉRIUM - MUNKAERŐPIACI SZABÁLYOZÁSI FŐOSZTÁLY</div>
										</div>
										<div class="clearfix"></div>
										<div class="member-content">Bodgál Ildikó jogi diplomáját a Corvinus Egyetem Nemzetközi és Diplomáciai Tanulmányok Intézetén szerzett posztgraduális végzettséggel egészítette ki. Jogi pályafutását – melyet a Komáromi és Erős / Squire, Sanders & Dempsey Ügyvédi Irodában kezdett – az államigazgatásban folytatta, jelenleg az NGM nemzetközi jogi kiemelt szakreferense. Munkajogi szakértőként a magyar munkaerőpiaci szabályozás EU-s jogharmonizációjához, valamint nemzetközi egyezmények magyarországi végrehajtásához kapcsolódó feladatokat lát el. Nemzeti delegáltként számos európai szakértői testület munkájában vesz részt, többek között a kiküldött munkavállalók szabályozását alakító tanácsi és bizottsági testületekben is. Az uniós jogalkotás mellett Magyarország képviseletében az ENSZ munkaügyi szervezetének, az ILO-nak a jogalkotási munkájában is hosszú évek óta szerepet vállal.</div>												
									<div class="clearfix"></div>
								</div>
							</div> 
						</div>
					</div>
                       <div class="container">
						 <div class="row0">
							<div class="col-md-3 col-sm-6">
								<div class="col-lg-12 member-box3">
									<div class="member-img">
										<a href="#"><img class="img-responsive" src="assets/images/team/doszpolyne.jpg" alt=".img-responsive"  /></a>
									</div>
									<div class="member-data">
										<div class="member-info">
											<div class="member-name"><a href="#"><font style="font-family:Aldrich; color:#000000; text-size:18px;"><strong>DOSZPOLYNÉ DR. MÉSZÁROS MELINDA</strong></font></a></div>
											<div class="member-title">VEZETŐ JOGTANÁCSOS, FŐTITKÁR - LIGA SZAKSZERVEZETEK
</div>
										</div>
										<div class="clearfix"></div>
										<div class="member-content">A LIGA Szakszervezetek főtitkára és vezető jogtanácsosa. A szakszervezeti konföderáció több mint 25 éve önálló jogi személyiségű tagszervezeteinek hazai (NGTT, VKF, OKÉT) és nemzetközi makroszintű érdekképviseletét biztosítja elsődlegesen, az ágazatokon keresztül a társadalom széles körének bevonásával. Munkája során számos esetben találkozik a szakszervezeteket megillető kollektív jogok területén elméleti és gyakorlati problémákkal, melyekre vonatkozó megoldási lehetőségek kidolgozása a szervezet feladatrendszerébe illeszkedik. </div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="col-lg-12 member-box3">
									<div class="member-img">
										<a href="#"><img class="img-responsive" src="assets/images/team/szabados_timea.jpg" alt=".img-responsive" /></a>
									</div>
									<div class="member-data">
										<div class="member-info">
											<div class="member-name"><a href="#"><font style="font-family:Aldrich; color:#000000; text-size:18px;"><strong>SZABADOS TÍMEA</strong></font></a></div>
											<div class="member-title">PROJEKTVEZETŐ SZOCIOLÓGUS </div>
										</div>
										<div class="clearfix"></div>
										<div class="member-content">Az ELTE Szociológiai Intézet és Továbbképző Központ szociológia szak kutatószociológus szakirányán diplomázott. 1997-2000 között az ELTE Szociológiai Intézet PhD képzési programjában vett részt. 2000-2006 között a MTA Szociológiai Kutatóintézetében tudományos kutatások területén dolgozott, és ezzel párhuzamosan felsőoktatási intézményekben külső óraadóként oktatott. Érdeklődési köre középpontjában a magyar társadalom értékrendszere, előítéletek, sztereotípiák, bizalmi viszonyai, igazságosság kérdése, az életmód, kommunikáció, a munka és szervezet szociológia áll. 2009-2014 között az Egyenlő Bánásmód Hatóság, TÁMOP-5.5.5. A diszkriminációs elleni küzdelem a társadalmi szemléletformálás és a hatósági munka erősítése c. projekt szakmai vezetője volt. 2014-2016-ban a LIGA Szakszervezetek „A Munkáért” projekt szakképzési pillérének vezetője volt.</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>

							
						</div>
					</div>
                </section>
                <!-- ## team section end ## -->
            </div>
            <!-- ## parallax background pattern 2 end ## -->
        </section>
        <!-- ## parallax section 2 end ## -->




       
        
        <!-- ## contact section start ## -->
        <section id="contact" class="scrollblock">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h2>Jelentkezés
                        <span class="line"><span class="dot"></span></span>
                        </h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 contact-form contact-box">
                        <h4>Jelentkezzen a MUNKAJOGI KONFERENCIA!</h4>
                        <p>
						HR PORTÁL - KRS MUNKAJOGI KONFERENCIA - 2016<br>
						
						</p>
						<p>
							<b>A konferencia díja: Earlybird aprilis 10-ig 23 900 Ft + áfa/ fő.</b><br />
							<b>Április 10 után 28 500 Ft + áfa/fő</b><br />
							
							
A díj tartalmazza az ebédet és a napközbeni finomságokat, frissítőket.
						</p>
						
                      <form name="form-contact-us" id="form-contact-us">
                       
                            <div class="form-group">
								
                            Online jelentkezéséről e-mailben visszaigazolást küldünk. Amennyiben a visszaigazoló levélben rákattint az aktiváló linkre, azzal elfogadja az adatok helyességét. Ezt követően a jóváhagyott adatok alapján átutalásos számlát küldünk a részvételi díjról, melyet kérünk a regisztráció visszaigazolásától számított 8 munkanapon belül - de legkésőbb a rendezvény napjáig - kiegyenlíteni. Az online regisztráció megrendelésnek minősül.
							<br><br>A rendezvényen a részvétel feltétele a részvételi díj előzetes kiegyenlítése. Lemondást a részvételi díj teljes visszafizetése mellett a rendezvény napja előtti 5. munkanapon 12 óráig - KIZÁRÓLAG ÍRÁSBAN a info@hrportal.hu címre - fogadunk el. Az ezutáni lemondások esetében a részvétel átruházható, de a teljes díjat ki kell fizetni. Telefonos vagy egyéb szóbeli lemondásokat nem áll módunkban elfogadni.
							<br><br>A lemondási határidő utáni regisztráció esetén lemondásra nincs lehetőség.
                            </div>
							<div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="txtName">Név: </label>
                                        <input type="text" placeholder="Név"
                                            class="form-control required" id="txtName" name="txtName" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="txtEmail">E-mail:</label>
                                        <input type="text" placeholder="E-mail ID"
                                            class="form-control required email" id="txtEmail" name="txtEmail" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="txtphone">Telefonszám:</label>
                                        <input type="text" placeholder="Telefonszám"
                                            class="form-control required phone" id="txtphone" name="txtphone" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="txtszlaname">Számlázási név:</label>
                                        <input type="text" placeholder="Számlázási név"
                                            class="form-control " id="txtszlaname" name="txtszlaname" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="txtszlaaddr">Számlázási cím:</label>
                                        <input type="text" placeholder="Számlázási cím"
                                            class="form-control " id="txtszlaaddr" name="txtszlaaddr" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="txtMessage">Üzenet:</label>
                                <textarea placeholder="Message" class="form-control required"
                                    id="txtMessage" name="txtMessage" rows="10"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="txtCaptcha"></label>
                                <input
                                    type="text" class="form-control required captcha"
                                    placeholder="Egy keresztkérdés?" id="txtCaptcha" name="txtCaptcha" />
                            </div>
                            <div class="form-group">
                                <input type="button" id="btn-Send" value="Küld" class="btn btn-theme-inverse" />
                            </div>
                            <div class="col-lg-12" id="contact-form-message">
                        </form>
                        <!-- ## contact form end ## -->
                    </div>
                    </div>

                    <div class="col-sm-6 contact-info contact-box">
                        <h4>Elérhetőségek</h4>
                        <!-- ## contact info start ## -->
                        <ul class="fa-ul">
                            <li><i class="fa-li fa fa-clock-o text-theme-inverse"></i><span>Időpont: 2016. május 24. 9:00</span></li>
                            <li><i class="fa-li fa fa-home text-theme-inverse"></i><span>1095 Budapest Komor Marcell u. 1.</span></li>
                            <li><i class="fa-li fa fa-envelope text-theme-inverse"></i><span>info@hrportal.hu</span></li>
                            <li><i class="fa-li fa fa-globe text-theme-inverse"></i><span>www.hrportal.hu/hrp-krs-munkajogikonferencia-2016/</span></li>
                        </ul>
                        <!-- ## contact info endstart ## -->
                        <!-- ## map start ## -->
<div id="map-canvas"></div>
                        <!-- ## map end ## -->
                    </div>
                </div>
            </div>
        </section>
        <!-- ## contact section end ## -->

        <!-- ## blockquote section start ## -->
        <section id="bquote" class="scrollblock">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-lg-offset-1 quote-box">
                        <blockquote>
                            <p>Jelentkezzen programunkra!</p>
                        </blockquote>
                    </div>
                </div>
            </div>
        </section>
        <!-- ## blockquote section end ## -->


        <!-- ## twitter section end ## -->

        <!-- ## footer section start ## -->
        <section id="footer">
            <!-- ## footer background pattern start ## -->
            <div class="bg-pattern3">

                <!-- ## footer copyright start ## -->
                <div class="footer-copyright text-center">
                    HRPortal.hu Copyright &copy; 2016, All rights reserved.
                </div>
                <!-- ## footer copyright end ## -->
            </div>
            <!-- ## footer background pattern end ## -->
        </section>
        <!-- ## footer section end ## -->

        <!-- ## preloader start ## -->
        <div id="preloader">
            <div id="status"><span class="fa fa-spin fa-spinner fa-5x"></span></div>
        </div>
        <!-- ## preloader end ## -->

        <!-- ## Time Bar begins here ## -->
        <!-- ## do not change it. ## -->
        <div id="progress-back" class="load-item">
            <div id="progress-bar"></div>
        </div>
        <!-- ## Time Bar ends here ## -->
    </div>
    <!-- ## page wrapper end ## -->

    <script src="assets/js/jquery-2.0.0.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/twitter/jquery.tweet.min.js" type="text/javascript"></script>
    <script src="assets/js/jquery.parallax-1.1.3.js" type="text/javascript"></script>
    <script src="assets/js/jquery.nicescroll.min.js" type="text/javascript"></script>
    <script src="assets/js/detectmobilebrowser.js" type="text/javascript"></script>
    <script src="assets/js/jquery.smooth-scroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.scrollorama.js" type="text/javascript"></script>
    <script src="assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="assets/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="assets/js/jquery.cookie.js" type="text/javascript"></script>
    <script src="assets/ytplayer/js/jquery.mb.YTPlayer.js" type="text/javascript"></script>
    <script src="assets/js/app.variables.js" type="text/javascript"></script>
    <!--<script src="assets/js/jquery.countdown.js" type="text/javascript"></script>
    <script src="assets/js/app.subscribe.js" type="text/javascript"></script>
    <script src="assets/js/app.contact.js" type="text/javascript"></script>
    <script src="assets/js/app.ui.js" type="text/javascript"></script>
    <script src="assets/js/app.styleswitcher.js" type="text/javascript"></script>-->
    <script src="/js/bxslider/jquery.bxslider.min.js"></script>

    <script src="assets/js/app.landlite.js" type="text/javascript"></script>
    <script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-696329-1', 'auto');
	  ga('send', 'pageview');

	</script> 
	<script type="text/javascript">
		<!--//--><![CDATA[//><!--
		var pp_gemius_identifier = 'bDhFBhiFYYCqPthkhx47lXZJHSGNJKL9TT54fXM4747.D7';
		// lines below shouldn't be edited
		function gemius_pending(i) { window[i] = window[i] || function() {var x = window[i+'_pdata'] = window[i+'_pdata'] || []; x[x.length]=arguments;};};
		gemius_pending('gemius_hit'); gemius_pending('gemius_event'); gemius_pending('pp_gemius_hit'); gemius_pending('pp_gemius_event');
		(function(d,t) {try {var gt=d.createElement(t),s=d.getElementsByTagName(t)[0],l='http'+((location.protocol=='https:')?'s':''); gt.setAttribute('async','async');
		gt.setAttribute('defer','defer'); gt.src=l+'://huadn.hit.gemius.pl/xgemius.js'; s.parentNode.insertBefore(gt,s);} catch (e) {}})(document,'script');
		//--><!]]>
		</script>
</body>
</html>
