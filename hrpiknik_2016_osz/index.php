﻿<!DOCTYPE html>
<html lang="en">
<head>
    
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-2">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>HRPIKNIK - 2016 - ősz</title>

    <!-- ## Bootstrap ## -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="assets/css/supersized.css" rel="stylesheet" />
    <link id="skin-styler" href="assets/css/skin1.css" rel="stylesheet" />
    <link href="assets/css/styleswitcher.css" rel="stylesheet" />
    <link href="assets/ytplayer/css/YTPlayer.css" rel="stylesheet" />
    <link type="text/css" href='http://fonts.googleapis.com/css?family=Aldrich' rel="stylesheet" />
    <!-- bxSlider CSS file -->
<link href="js/jquery.bxslider.css" rel="stylesheet" />
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js" type="text/javascript"></script>
      <script src="assets/js/respond.min.js" type="text/javascript"></script>
    <![endif]-->

    <link rel="apple-touch-fa-precomposed" sizes="144x144" href="assets/ico/apple-touch-fa-144-precomposed.png" />
    <link rel="shortcut icon" href="assets/ico/favicon.ico" />
    
    <style>
      #map-canvas {
        width: 500px;
        height: 400px;
      }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js"></script>
     <script>
      function initialize() {
        var mapCanvas = document.getElementById('map-canvas');
        var mapOptions = {
          center: new google.maps.LatLng(47.4695946,19.071223),
          zoom: 14,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(mapCanvas, mapOptions)
		  marker = new google.maps.Marker({
			position: new google.maps.LatLng(47.4695946, 19.071223),
			map: map
		  });
      }
      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
<script type="text/javascript">



function checkEmail(emailstr){
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailstr)){
		return (true)
	}
	return (false)
 } 


</script>
  
</head>
<body id="top" data-spy="scroll" data-target=".navbar" data-offset="200">
    <div class="bg-video"></div>
    <!-- ## page wrapper start ## -->
    <div id="page-wrapper">
        <!-- ## parallax section 1 start ## -->
        <section id="parallax-section-1">
            <!-- ## parallax background 1 ## -->
            <div class="bg1"></div>
            <!-- ## parallax background pattern 1 start ## -->
            <div >



                <!-- ## header start ## -->
                <header id="header">
                    <div class="container">
                        <div class="row"></div>
                    </div>
                    <!-- ## nav start ## -->
                <nav id="nav">
                    <div class="navbar navbar-inverse navbar-fixed-bottom" role="navigation">
                        <div class="container">

                            <div class="navbar-collapse collapse navbar-right">
                                <ul class="nav navbar-nav">
                                    <li class="active"><a href="#top">Nyitó</a></li>
                                    <li><a href="#about">Esemény bemutatása</a></li>
                                     <li><a href="#content-boxes">Program</a></li>
                                      <li><a href="#team">Előadók</a></li>
                                    <li><a href="#contact">Jelentkezés</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </nav>
                <!-- ## nav end ## -->
                </header>
                <!-- ## header end ## -->
                    <!-- ## about section start ## -->
                <section id="about" class="scrollblock">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 text-center about-box">
                                <h2>Program
                            <span class="line"><span class="dot"></span></span>
                                </h2>
                                <p>
                                  <?
                                  $text='

				<span style="color:#f7941d;font-weight:bold;">TUDATOS KREATIVITÁS – AZ INNOVATÍV MUNKAHELY</span> <br /><br />
				A cégek értékét ma már nem (csak) a cash flow, vagyon, vagy részvények ára határozza meg, hanem a bennük rejlő KREATÍV, INNOVATÍV potenciál. Soha ennyi tőke nem legyeskedett még az induló vállalatok körül, és soha ennyi pénzt nem fektettek még a nagyvállalatok fejlesztésbe, vagy startupok felvásárlásába, mint ma. <br /><br />
				A kreativitás és az innováció tehát szexi szavak. Gyakran hangzanak el megbeszéléseken, vannak kiragasztva a liftben, képezik egy vállalati stratégia részét, vagy akár egy cég alapértékei is lehetnek. A valóság mégis az, hogy nagy ötletek helyett általában középszerűek születnek, és az innovációs szakaszban a megvalósítás is gyakran elbukik. De vajon miért?<br /><br />
				Talán azért mert a nagy ötlet megszületéséhez – vagyis  a KREATIVITÁShoz - nem tudjuk eléggé elengedni fegyelmezettségünket, a nagy ötlet megvalósításához – vagyis  az INNOVÁCIÓhoz - viszont nem tudunk elég fegyelmezettek lenni. Jó lenne tudnunk a választ az olyan kérdésekre, mint \'Hogyan tudunk kreatív környezetet kialakítani? Hogyan tudunk kreatív állapotba kerülni? Hogyan tudunk hatékonyan innoválni? Hogyan tudunk olyan csapatot építeni, melynek tagjai nemcsak képesek, hanem igénylik is a kreatív és innovatív szemléletet?\'<br /><br />';
					
                                  //echo iconv("utf-8","iso-8859-2",$text);
                                  echo $text;
                                  
                                  ?>
                                </p>
                            </div>
                        </div>

                     
                    </div>
                </section>
       
        <!-- ## content boxes end ## -->
        
               <!-- ## content boxes start ## -->
        <section id="content-boxes" class="scrollblock">
            <div class="container">
                <div class="row">

                   <ul class="bxslider">
					  <li><img src="prog_01.jpg" /></li>
					  <li><img src="prog_02.jpg" /></li>
					  <li><img src="prog_03.jpg" /></li>
					  <li><img src="prog_04.jpg" /></li>
					  <li><img src="prog_05.jpg" /></li>
					  <li><img src="prog_06.jpg" /></li>
					  <li><img src="prog_07.jpg" /></li>
					  <li><img src="prog_08.jpg" /></li>
					  <li><img src="prog_09.jpg" /></li>
					  <li><img src="prog_10.jpg" /></li>
					  <li><img src="prog_11.jpg" /></li>
					</ul>
                </div>
            </div>
        </section>
        <!-- ## content boxes end ## -->

 <!-- ## parallax section 2 start ## -->
        <section id="parallax-section-2">
            <!-- ## parallax background 2 ## -->
            <div class="bg2"></div>
            <!-- ## parallax background pattern 2 start ## -->
            <div class="bg-pattern2" style="background: none;">
                <!-- ## team section start ## -->
                <section id="team" class="scrollblock_team">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <h2>Előadók
                             <span class="line"><span class="dot"></span></span>
                                </h2>
                            </div>
                        </div>
                        <div class="row0">
                            <div class="col-md-3 col-sm-6">
                                <div class="col-lg-12 member-box">
                                    <div class="member-img">
                                        <a href="#"><img class="img-responsive" src="assets/images/team/vekerdy_tamas.jpg" alt=".img-responsive" /></a>
                                    </div>
                                    <div class="member-data">
                                        <div class="member-info">
                                            <div class="member-name"><a href="#">Vekerdy Tamás</a></div>
                                            <div class="member-title">Pszichológus, író</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="member-content">Ha gyerek, akkor Vekerdy. Volt házitanító, nevelési tanácsadóban pszichológus, de a Nemzeti Színház statisztája is. Az itthoni Waldorf iskolák megszervezője, számtalan cikk és könyv szerzője. Előadásai tisztánlátása, szókimondása, bátorsága miatt katartikus élmények.</div>
                                    </div>
                                    <div class="clearfix"></div>
                               </div>
                            </div>
                                
                                <!-- ## member box end ## -->
                            <div class="col-md-3 col-sm-6">
                                <div class="col-lg-12 member-box">
                                    <div class="member-img">
                                        <a href="#"><img class="img-responsive" src="assets/images/team/caramel.jpg" alt=".img-responsive" /></a>
                                    </div>
                                    <div class="member-data">
                                        <div class="member-info">
                                            <div class="member-name"><a href="#">Molnár Ferenc Caramel</a></div>
                                            <div class="member-title">Zeneszerző, énekes</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="member-content">Több kiadó viszautasította az alkalmi munkákból élő Molnár Ferencet. Ágyban feküdt serve miatt, amikor látta az első Megasztárt. A többit már tudjuk:  a  hazai zenei élet meghatározó alakja lett. Szállok a Dallal, Lélekdonor, Vízió, Jelenés, Végtelen. 12 éve megállíthatatlanul jönnek az örökérvényű dalok.</div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                                <!-- ## member box end ## -->
                            <div class="col-md-3 col-sm-6">
                                <div class="col-lg-12 member-box">
                                    <div class="member-img">
                                        <a href="#"><img class="img-responsive" src="assets/images/team/simo.jpg" alt=".img-responsive" /></a>
                                    </div>
                                    <div class="member-data">
                                        <div class="member-info">
                                            <div class="member-name"><a href="#">Simó György</a></div>
                                            <div class="member-title">Gondolkodó, üzletember, befektető, startupper, egykori Matáv vezérigazgatóhelyettes</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="member-content">Szociológus, aki a médiában kezdett, Magyar Narancs szerzőként, Tilos Rádió alapítóként. A Matáv tartalomszolgáltatási vezetőjeként sokat tett a magyar internet beindításáért. Ott volt az Origo indulásánál, később az Axelerot vezette.Ma STARTUP befektető, és bizonyos startupokban operatív résztvevő, vezető is. Gondolkodó vezető. Missziója a 3D nyomtatás elterjesztése.</div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                                <!-- ## member box end ## -->
                       
                                
                            <div class="col-md-3 col-sm-6">
                                <div class="col-lg-12 member-box">
                                    <div class="member-img">
                                        <a href="#"><img class="img-responsive" src="assets/images/team/varnagy_priscilla.jpg" 	 /></a>
                                    </div>
                                    <div class="member-data">
                                        <div class="member-info">
                                            <div class="member-name"><a href="#">Várnagy Priszcilla</a></div>
                                            <div class="member-title">CEO Be-novative, pszichológus, startupper. </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="member-content">2011-ben globális közösségi ötletelő-platformot indított a társával, akit a Google-keresőn keresztül talált meg. Versenyen nyerte az induláshoz szükséges pénzt, fél millió eurót. Megnyerte a Singularity University versenyét is. A Szilícium-völgyben is talált üzleti angyalt, persze ahogy az lenni szokott, véletlenül egy buszon beszélgetve. Ma 6 kontinensen van jelen cége, olyan nagyválalati ügyfelekkel, mint Microsoft, vagy a Cisco.</div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                                <!-- ## member box end ## -->
                         </div>
                     </div>
                     
                     
                       <div class="container">
						 <div class="row0">
							<div class="col-md-3 col-sm-6">
								<div class="col-lg-12 member-box2">
									<div class="member-img">
										<a href="#"><img class="img-responsive" src="assets/images/team/kiss_peter.jpg" alt=".img-responsive"  /></a>
									</div>
									<div class="member-data">
										<div class="member-info">
											<div class="member-name"><a href="#"><font style="font-family:Aldrich; color:#000000; text-size:18px;"><strong>Kiss Péter</strong></font></a></div>
											<div class="member-title">Restart Alapító</div>
										</div>
										<div class="clearfix"></div>
										<div class="member-content">Beindítóként definiálja magát. Saját tanácsadó- és coaching cége a RESTART, ami abban segít, hogy az emberek egyénileg, és csapatban is nyitottak legyenek az újdonságokra, bátran nézzenek szembe, sőt keressék a kihívásokat, akarjanak kreatívak lenni, és valósítsák is meg ötleteiket! Péter korábban kalandos utat járt be a televíziózás világában itthon és külföldön és 15 éves médiakarriarjének második felében felsővezető volt a TV2 csoportnál tartalmi területen. Olyan műsorok kitalálója - alkotója volt, mint a Jóban Rosszban, vagy a Nagy Duett.</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="col-lg-12 member-box2">
									<div class="member-img">
										<a href="#"><img class="img-responsive" src="assets/images/team/pach_gabriella_f.jpg" alt=".img-responsive" /></a>
									</div>
									<div class="member-data">
										<div class="member-info">
											<div class="member-name"><a href="#"><font style="font-family:Aldrich; color:#000000; text-size:18px;"><strong>Pach Gabi</strong></font></a></div>
											<div class="member-title">Talent developer</div>
										</div>
										<div class="clearfix"></div>
										<div class="member-content">Végzettsége szerint jogi szakokleveles közgazdász és coach. HR és vezetői tapasztalatot olyan multinacionális vállalatoknál szerzett, mint a Tesco, British American Tobacco, Xerox, Syngenta és GE Lighting. Közel 20 éve dolgozik emberekkel HR szakemberként és vezetőként, közben folyamatosan fejlesztve saját magát és azokat akikkel együtt dolgozik. A tehetség fejlesztés nem csak hivatás számára, hanem szenvedély is egyben. </div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="col-lg-12 member-box2" >
									<div class="member-img">
										<a href="#"><img class="img-responsive" src="assets/images/team/barathi_tamas.jpg" alt=".img-responsive" /></a>
									</div>
									<div class="member-data">
										<div class="member-info">
											<div class="member-name"><a href="#"><font style="font-family:Aldrich; color:#000000; text-size:18px;"><strong>Barathi Tamás</strong></font></a></div>
											<div class="member-title">Colibri HR Solution alapító, ügyvezető</div>
										</div>
										<div class="clearfix"></div>
										<div class="member-content">Egyetemi hálózat építő, employer branding kommunikációs szakértő, HR témájú videós tartalom gyártó, régiós recruitment támogató kreatív kommunikációs projektek kitalálója, nem mellesleg 2015. év HR Innovátora. 2003-tól foglalkozik marketing-kommunikációs projektekkel. Fiatal csapatával a Y és Z generációs kihívásokra koncentrálnak, részt vesznek több gyakornoki program fejlesztésében. </div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="col-lg-12 member-box2" >
									<div class="member-img">
										<a href="#"><img class="img-responsive" src="assets/images/team/szalay_nadja.jpg" alt=".img-responsive" /></a>
									</div>
									<div class="member-data">
										<div class="member-info">
											<div class="member-name"><a href="#"><font style="font-family:Aldrich; color:#000000; text-size:18px;"><strong>Szalay Nadja </strong></font></a></div>
											<div class="member-title">Aon Hewitt, Senior tanácsadó</div>
										</div>
										<div class="clearfix"></div>
										<div class="member-content">12 éve dolgozik az Aon Hewitnál. a csapathoz. Legfontosabb feladatának azt tartja, hogy ügyfelei számára olyan megoldásokat kínáljon, ami leginkább hozzásegíti a szervezeteket, hogy jobb munkahellyé váljanak: egy belül motiváló (elkötelezettség kultúrájának fejlesztése, elkötelező vezetők formálása), kifelé pedig hiteles és vonzó munkáltatói márka építésével.</div>												
									<div class="clearfix"></div>
								</div>
							</div> 
						</div>
					</div>
     
                </section>
                <!-- ## team section end ## -->
            </div>
            <!-- ## parallax background pattern 2 end ## -->
        </section>
        <!-- ## parallax section 2 end ## -->
        
               <!-- ## contact section start ## -->
        <section id="contact" class="scrollblock">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h2>Jelentkezés
                        <span class="line"><span class="dot"></span></span>
                        </h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 contact-form contact-box">
                        <h4>Jelentkezzen a HR Piknikre!</h4>
                        <p>
						A RESTART ÉS A HRPORTAL BEMUTATJA: HRPIKNIK 2016<br>
						
						</p>
						<p>
							<b>A workshop díja: Earlybird szeptember 16-ig 23 900 Ft + áfa/ fő.</b><br />
							<b>Szeptember 16 után 29 500 Ft + áfa/fő</b><br />
							
							
A díj tartalmazza az ebédet és a napközbeni finomságokat, frissítőket.
						</p>
						
                      <form name="form-contact-us" id="form-contact-us">
                       
                            <div class="form-group">
								
                            Online jelentkezéséről e-mailben visszaigazolást küldünk. Amennyiben a visszaigazoló levélben rákattint az aktiváló linkre, azzal elfogadja az adatok helyességét. Ezt követően a jóváhagyott adatok alapján átutalásos számlát küldünk a részvételi díjról, melyet kérünk a regisztráció visszaigazolásától számított 8 munkanapon belül - de legkésőbb a rendezvény napjáig - kiegyenlíteni. Az online regisztráció megrendelésnek minősül.
							<br><br>A rendezvényen a részvétel feltétele a részvételi díj előzetes kiegyenlítése. Lemondást a részvételi díj teljes visszafizetése mellett a rendezvény napja előtti 5. munkanapon 12 óráig - KIZÁRÓLAG ÍRÁSBAN a info@hrportal.hu címre - fogadunk el. Az ezutáni lemondások esetében a részvétel átruházható, de a teljes díjat ki kell fizetni. Telefonos vagy egyéb szóbeli lemondásokat nem áll módunkban elfogadni.
							<br><br>A lemondási határidő utáni regisztráció esetén lemondásra nincs lehetőség.
                            </div>
							<div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="txtName">Név: </label>
                                        <input type="text" placeholder="Név"
                                            class="form-control required" id="txtName" name="txtName" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="txtEmail">E-mail:</label>
                                        <input type="text" placeholder="E-mail ID"
                                            class="form-control required email" id="txtEmail" name="txtEmail" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="txtphone">Telefonszám:</label>
                                        <input type="text" placeholder="Telefonszám"
                                            class="form-control required phone" id="txtphone" name="txtphone" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="txtszlaname">Számlázási név:</label>
                                        <input type="text" placeholder="Számlázási név"
                                            class="form-control " id="txtszlaname" name="txtszlaname" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="txtszlaaddr">Számlázási cím:</label>
                                        <input type="text" placeholder="Számlázási cím"
                                            class="form-control " id="txtszlaaddr" name="txtszlaaddr" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="txtMessage">Üzenet:</label>
                                <textarea placeholder="Message" class="form-control required"
                                    id="txtMessage" name="txtMessage" rows="10"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="txtCaptcha"></label>
                                <input
                                    type="text" class="form-control required captcha"
                                    placeholder="Egy keresztkérdés?" id="txtCaptcha" name="txtCaptcha" />
                            </div>
                            <div class="form-group">
                                <input type="button" id="btn-Send" value="Küld" class="btn btn-theme-inverse" />
                            </div>
                            <div class="col-lg-12" id="contact-form-message">
                        </form>
                        <!-- ## contact form end ## -->
                    </div>
                    </div>

                    <div class="col-sm-6 contact-info contact-box">
                        <h4>Elérhetőségek</h4>
                        <!-- ## contact info start ## -->
                        <ul class="fa-ul">
                            <li><i class="fa-li fa fa-clock-o text-theme-inverse"></i><span>Időpont: 2016. november 10. 9:00</span></li>
                            <li><i class="fa-li fa fa-home text-theme-inverse"></i><span>MÜPA 1095 Budapest Komor Marcell u. 1.</span></li>
                            <li><i class="fa-li fa fa-envelope text-theme-inverse"></i><span>info@hrportal.hu</span></li>
                            <li><i class="fa-li fa fa-globe text-theme-inverse"></i><span>www.hrportal.hu/hrpiknik_2016_osz/</span></li>
                        </ul>
                        <!-- ## contact info endstart ## -->
                        <!-- ## map start ## -->
<div id="map-canvas"></div>
                        <!-- ## map end ## -->
                    </div>
                </div>
            </div>
        </section>
        <!-- ## contact section end ## -->

        <!-- ## blockquote section start ## -->
        <section id="bquote" class="scrollblock">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-lg-offset-1 quote-box">
                        <blockquote>
                            <p>Jelentkezzen programunkra!</p>
                        </blockquote>
                    </div>
                </div>
            </div>
        </section>
        <!-- ## blockquote section end ## -->



        <!-- ## twitter section end ## -->

        <!-- ## footer section start ## -->
        <section id="footer">
            <!-- ## footer background pattern start ## -->
            <div class="bg-pattern3">

                <!-- ## footer copyright start ## -->
                <div class="footer-copyright text-center">
                    HRPortal.hu Copyright &copy; 2016, All rights reserved.
                </div>
                <!-- ## footer copyright end ## -->
            </div>
            <!-- ## footer background pattern end ## -->
        </section>
        <!-- ## footer section end ## -->

        <!-- ## preloader start ## -->
        <div id="preloader">
            <div id="status"><span class="fa fa-spin fa-spinner fa-5x"></span></div>
        </div>
        <!-- ## preloader end ## -->

        <!-- ## Time Bar begins here ## -->
        <!-- ## do not change it. ## -->
        <div id="progress-back" class="load-item">
            <div id="progress-bar"></div>
        </div>
        <!-- ## Time Bar ends here ## -->
    </div>
    <!-- ## page wrapper end ## -->

    <script src="assets/js/jquery-2.0.0.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/twitter/jquery.tweet.min.js" type="text/javascript"></script>
    <script src="assets/js/jquery.parallax-1.1.3.js" type="text/javascript"></script>
    <script src="assets/js/jquery.nicescroll.min.js" type="text/javascript"></script>
    <script src="assets/js/detectmobilebrowser.js" type="text/javascript"></script>
    <script src="assets/js/jquery.smooth-scroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.scrollorama.js" type="text/javascript"></script>
    <script src="assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="assets/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="assets/js/jquery.cookie.js" type="text/javascript"></script>
    <script src="assets/ytplayer/js/jquery.mb.YTPlayer.js" type="text/javascript"></script>
    <script src="assets/js/app.variables.js" type="text/javascript"></script>
    <!--<script src="assets/js/jquery.countdown.js" type="text/javascript"></script>
    <script src="assets/js/app.subscribe.js" type="text/javascript"></script>
    <script src="assets/js/app.contact.js" type="text/javascript"></script>
    <script src="assets/js/app.ui.js" type="text/javascript"></script>
    <script src="assets/js/app.styleswitcher.js" type="text/javascript"></script>-->
    <script src="/js/bxslider/jquery.bxslider.min.js"></script>

    <script src="assets/js/app.landlite.js" type="text/javascript"></script>
    <script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-696329-1', 'auto');
	  ga('send', 'pageview');

	</script> 
	<script type="text/javascript">
		<!--//--><![CDATA[//><!--
		var pp_gemius_identifier = 'bDhFBhiFYYCqPthkhx47lXZJHSGNJKL9TT54fXM4747.D7';
		// lines below shouldn't be edited
		function gemius_pending(i) { window[i] = window[i] || function() {var x = window[i+'_pdata'] = window[i+'_pdata'] || []; x[x.length]=arguments;};};
		gemius_pending('gemius_hit'); gemius_pending('gemius_event'); gemius_pending('pp_gemius_hit'); gemius_pending('pp_gemius_event');
		(function(d,t) {try {var gt=d.createElement(t),s=d.getElementsByTagName(t)[0],l='http'+((location.protocol=='https:')?'s':''); gt.setAttribute('async','async');
		gt.setAttribute('defer','defer'); gt.src=l+'://huadn.hit.gemius.pl/xgemius.js'; s.parentNode.insertBefore(gt,s);} catch (e) {}})(document,'script');
		//--><!]]>
		</script>
		<!-- Google remarketingcímke-kód -->
<!--------------------------------------------------
A remarketingcímkék nem társíthatók személyazonosításra alkalmas adatokkal, és nem helyezhetők el érzékeny kategóriához kapcsolódó oldalakon. A címke beállításával kapcsolatban további információt és útmutatást a következő címen olvashat: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 983428301;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/983428301/?value=1&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</body>
</html>
