﻿<!DOCTYPE html>
<html lang="en">
<head>
    
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-2">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>HRPIKNIK - 2016 - tavasz</title>

    <!-- ## Bootstrap ## -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="assets/css/supersized.css" rel="stylesheet" />
    <link id="skin-styler" href="assets/css/skin1.min.css" rel="stylesheet" />
    <link href="assets/css/styleswitcher.css" rel="stylesheet" />
    <link href="assets/ytplayer/css/YTPlayer.css" rel="stylesheet" />
    <link type="text/css" href='http://fonts.googleapis.com/css?family=Aldrich' rel="stylesheet" />
    <!-- bxSlider CSS file -->
<link href="js/jquery.bxslider.css" rel="stylesheet" />
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js" type="text/javascript"></script>
      <script src="assets/js/respond.min.js" type="text/javascript"></script>
    <![endif]-->

    <link rel="apple-touch-fa-precomposed" sizes="144x144" href="assets/ico/apple-touch-fa-144-precomposed.png" />
    <link rel="shortcut icon" href="assets/ico/favicon.ico" />
    
    <style>
      #map-canvas {
        width: 500px;
        height: 400px;
      }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script>
      function initialize() {
        var mapCanvas = document.getElementById('map-canvas');
        var mapOptions = {
          center: new google.maps.LatLng(47.4695946,19.071223),
          zoom: 14,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(mapCanvas, mapOptions)
		  marker = new google.maps.Marker({
			position: new google.maps.LatLng(47.4695946, 19.071223),
			map: map
		  });
      }
      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
<script type="text/javascript">



function checkEmail(emailstr){
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(emailstr)){
		return (true)
	}
	return (false)
 } 

function do_submitlanding(){
	var f=document.forms.frm_landing;
	if(f.allok.checked==1){
		if(f.email.value!="" && f.nev1.value!="" && f.cegnev.value!="" && f.szlacim.value!="" && f.postcim.value!="" && f.telefon.value!=""){
			if (checkEmail(f.email.value)){
				f.submit();
			}
			else{
				alert("Nem megfelelő emailcím formátum!");
			}
		}
		else{
				alert("A kötelező mezőket ki kell tölteni!!");
		}
	}
	else{
		alert("A jelentkezéshez tudomásul kell venni a feltételeket!");
	}
}
</script>
<?
$sending="0";
if(isset($_POST['landingsend'])){
	$check=db_one("select id from workshop_landing_2015 where nev1='".$_POST['nev1']."' and email='".$_POST['email']."' and sessid='".session_id()."'");
	if($check==""){
	$bele="";
	$osszes=0;
		if(strlen($_POST['nev1'])>1){
			if(isset($_POST['tcheck1'])){
				$osszes+=19500;
				$bele+=$_POST['nev1'].": 19 500 Ft\n";
	
			}
			else{
				if(strlen($_POST['nev2'])>1 && strlen($_POST['nev2'])>1){
					$osszes+=19500;
					$bele+=$_POST['nev1'].": 19 500 Ft\n";
			
				}
				else{
					$osszes+=19500;
					$bele+=$_POST['nev1'].": 19 500 Ft\n";
			
				}
			}
	
			if(strlen($_POST['nev2'])>1 && strlen($_POST['nev1'])>1){
			if(isset($_POST['tcheck2'])){
				$osszes+=19500;
				$bele+=$_POST['nev2'].": 19 500 Ft\n";
	
			}
			else{
				$osszes+=19500;
				$bele+=$_POST['nev2'].": 19 500 Ft\n";
				}
			
			}
			if(strlen($_POST['nev3'])>1 && strlen($_POST['nev1'])>1){
			if(isset($_POST['tcheck3'])){
				$osszes+=19500;
				$bele+=$_POST['nev3'].": 19 500 Ft\n";
	
			}
			else{
				$osszes+=19500;
				$bele+=$_POST['nev3'].": 19 500 Ft\n";
				}
			
			}
	

		}
		
	//	db_execute("insert into workshop_landing_2015 (nev1,nev2,nev3,cegnev,szlacim,postcim,email,telefon,megjegyzes,sessid,regdate) 
	//	values('".$_POST['nev1']."','".$_POST['nev2']."','".$_POST['nev3']."','".$_POST['cegnev']."','".$_POST['szlacim']."','".$_POST['postcim']."','".$_POST['email']."','".$_POST['telefon']."','".$_POST['megjegyzes']."','".session_id()."',NOW())");
		$sysmessage="Kedves ".$_POST['nev1']."!\n\nKöszönjük jelentkezését a 2015. november 03-án 09:00-kor a MÜPA-ban megrendezendő HR Piknikünkre.\n\nA jelentkezés jóváhagyásához kérjük kattintson erre a linkre http://www.hrportal.hu/index.phtml?page=landing_2015_hrpiknik&p=jelentkez&lsid=".session_id()."\n\nJelentkezési és számlázási adatai a következők:\n\n1. személy neve: ".$_POST['nev1']."\n";if($_POST['nev2']!=""){$sysmessage.="2. személy neve: ".$_POST['nev2']."\n";}if($_POST['nev3']!=""){$sysmessage.="3. személy neve: ".$_POST['nev3']."\n";}$sysmessage.="E-mail: ".$_POST['email']."\nTelefonszám: ".$_POST['telefon']."\nCégnév: ".$_POST['cegnev']."\nSzámlázási cím: ".$_POST['szlacim']."\nPostázási cím: ".$_POST['postcim']."\nMegjegyzes:\n\n".$_POST['megjegyzes']."\n\nÖsszesen fizetendő:".$osszes."\n\nJelentkezési díjáról  hamarosan egy számlát állít ki és küld el Önnek a HRPortal.hu, melyet kérünk a regisztráció visszaigazolásától számított 8 munkanapon belül - de legkésőbb a rendezvény napjáig - kiegyenlíteni. Az online regisztráció megrendelésnek minősül.\n\nA rendezvényen a részvétel feltétele a részvételi díj előzetes kiegyenlítése. Lemondást a részvételi díj teljes visszafizetése mellett a rendezvény napja előtti 5. munkanapon 12 óráig - KIZÁRÓLAG ÍRÁSBAN az info@hrportal.hu címre - fogadunk el. Az ezutáni lemondások esetében a részvétel átruházható, de a teljes díjat ki kell fizetni. Telefonos vagy egyéb szóbeli lemondásokat nem áll módunkban elfogadni.\nA lemondási határidő utáni regisztráció esetén lemondásra nincs lehetőség.\n\nA programról és a helyszínről az alábbi linken tájékozódhat: http://www.hrportal.hu/index.phtml?page=landing_2015_hrpiknik\n\nÜdvözlettel,\n\nHRportal.hu";
		$cmmessage="Landing regisztráció történt!\n A regisztrációban ez a levél került az ügyfélnek kiküldésre\n\nKedves ".$_POST['nev1']."!\n\nKöszönjük jelentkezését a 2015. november 03-án 09:00-kor a MÜPA-ban megrendezendő HR Piknikünkre.\n\nA jelentkezés jóváhagyásához kérjük kattintson erre a linkre http://www.hrportal.hu/index.phtml?page=landing_2015_hrpiknik&p=jelentkez&lsid=".session_id()."\n\nJelentkezési és számlázási adatai a következők:\n\n1. személy neve: ".$_POST['nev1']."\n";if($_POST['nev2']!=""){$cmmessage.="2. személy neve: ".$_POST['nev2']."\n";}if($_POST['nev3']!=""){$cmmessage.="3. személy neve: ".$_POST['nev3']."\n";}$cmmessage.="E-mail: ".$_POST['email']."\nTelefonszám: ".$_POST['telefon']."\nCégnév: ".$_POST['cegnev']."\nSzámlázási cím: ".$_POST['szlacim']."\nPostázási cím: ".$_POST['postcim']."\nMegjegyzes:\n\n".$_POST['megjegyzes']."\n\nÖsszesen fizetendő:".$osszes."\n\nJelentkezési díjáról  hamarosan egy számlát állít ki és küld el Önnek a HRPortal.hu, melyet kérünk a regisztráció visszaigazolásától számított 8 munkanapon belül - de legkésőbb a rendezvény napjáig - kiegyenlíteni. Az online regisztráció megrendelésnek minősül.\n\nA rendezvényen a részvétel feltétele a részvételi díj előzetes kiegyenlítése. Lemondást a részvételi díj teljes visszafizetése mellett a rendezvény napja előtti 5. munkanapon 12 óráig - KIZÁRÓLAG ÍRÁSBAN az info@hrportal.hu címre - fogadunk el. Az ezutáni lemondások esetében a részvétel átruházható, de a teljes díjat ki kell fizetni. Telefonos vagy egyéb szóbeli lemondásokat nem áll módunkban elfogadni.\nA lemondási határidő utáni regisztráció esetén lemondásra nincs lehetőség.\n\nA programról és a helyszínről az alábbi linken tájékozódhat: http://www.hrportal.hu/index.phtml?page=landing_2015_hrpiknik\n\nÜdvözlettel,\n\nHRportal.hu";
		@mail("jozsef.szucs@planetstar.hu", "HR Portal - HRPiknik regisztráció történt!!", $cmmessage, "From: info@hrportal.hu");
		@mail("andras.markovics@gmail.com", "HR Portal - HRPiknik regisztráció történt!!", $cmmessage, "From: info@hrportal.hu");
		@mail("info@hrportal.hu", "HR Portal - HRPiknik regisztráció történt!!", $cmmessage, "From: info@hrportal.hu");
		@mail("edit.halmi@hrportal.hu", "HR Portal - HRPiknik regisztráció történt!!", $cmmessage, "From: info@hrportal.hu");
		
		@mail($_POST['email'], "HR Portal - HRPiknik regisztráció", $sysmessage, "From: info@hrportal.hu");
		$sending="1";
	}
}
if(isset($_GET['lsid'])){
	
	db_execute("update workshop_landing_2015 set confirm='1', confirm_date=NOW() where sessid='".$_GET['lsid']."' ");	
	$sending="2";
}

?>   
</head>
<body id="top" data-spy="scroll" data-target=".navbar" data-offset="200">
    <div class="bg-video"></div>
    <!-- ## page wrapper start ## -->
    <div id="page-wrapper">
        <!-- ## parallax section 1 start ## -->
        <section id="parallax-section-1">
            <!-- ## parallax background 1 ## -->
            <div class="bg1"></div>
            <!-- ## parallax background pattern 1 start ## -->
            <div >



                <!-- ## header start ## -->
                <header id="header">
                    <div class="container">
                        <div class="row"></div>
                    </div>
                    <!-- ## nav start ## -->
                <nav id="nav">
                    <div class="navbar navbar-inverse navbar-fixed-bottom" role="navigation">
                        <div class="container">

                            <div class="navbar-collapse collapse navbar-right">
                                <ul class="nav navbar-nav">
                                    <li class="active"><a href="#top">Nyitó</a></li>
                                    <li><a href="#about">Esemény bemutatása</a></li>
                                    <li><a href="#content-boxes">Program</a></li>
                                    <li><a href="#team">Előadók</a></li>
                                    <li><a href="#contact">Jelentkezés</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </nav>
                <!-- ## nav end ## -->
                </header>
                <!-- ## header end ## -->
                    <!-- ## about section start ## -->
                <section id="about" class="scrollblock">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 text-center about-box">
                                <h2>Program
                            <span class="line"><span class="dot"></span></span>
                                </h2>
                                <p>
                                  <?
                                  $text='

				<span style="color:#f7941d;font-weight:bold;">A SZEMLÉLETVÁLTÁS elkerülhetetlen.</span> <br /><br />
				Robbanásszerű fejlődésen megyünk keresztül. Naponta kapkodjuk a fejünket az újításokon, próbáljuk feldolgozni az évente duplázódó információ mennyiséget, kezelni a változó gazdasági tendenciákat, helytállni a lassan öt generációt foglalkoztató munka világában. Ez valóban egy VUCA világ – illékony (volatile), bizonytalan (uncertain), összetett (complex) és többértelmű (ambiguous). Ma már a múlt tudása és tapasztalata nem visz közelebb a jövőhöz. Személetváltásra van szükségünk, mely által megtanulhatunk jól reagálni a váratlan kihívásokra.<br /><br />
				<span style="color:#f7941d;font-weight:bold;">A harmadik HR PIKNIK-en ezért "több sebességben" is SZEMLÉLET VÁLTUNK.</span>
				<br />	<br />
					Szó lesz arról, hogy mi mondatja azt velünk: “ez nekem nem fog menni” és hogyan juthatunk el oda, hogy azt mondjuk: “ez még nem megy, de egyszer majd fog”. Azaz a RÖGZÜLT, illetve NÖVEKVŐ SZEMLÉLETRŐL
					Kiss Péter (Restart), Pach Gabriella (Talent-developer)
<br /><br />
					Szó lesz a szükségszerű SZEMLÉLETVÁLTÁSRÓL a SZERVEZETFEJLESZTÉSBEN. Egy kerekasztal keretében megkérdezzük a vállalati oldalt hogy mire van szükségük és milyen tendenciákat látnak, valamint a tanácsadói oldalt, hogy hogyan tudnak egyedi módszerekkel ebben partnerek lenni.
					Friedl Zsuzsanna (Microsoft, HR Igazgató), Karl Anikó (Mol- Training and Competency Development Manager), Sárvári György (Bardo Consulting), Molnár Erika (Szereprulett)
		<br /><br />
				Szó lesz arról, hogy kommunikáljunk érthetően a felgyorsult világban, ahol a rengeteg csatorna, és a generációs különbségek sem könnyítik helyzetünket.
				Prof. Kepes András (TRIPLAcoaching és Budapesti Metropolitan Egyetem)
				<br /><br />
				Szó lesz arról, hogy milyen szemlélettel forduljanak egymáshoz a különböző Generációk. 
				Tari Annamária, pszichoanalitikus, több sikerkönyv szerzője az Y, Z generációkról, a generációs együttműködésről.
				<br /><br />
				Szó lesz arról, hogyan érhetünk el fenntartható sikert és jókedvet a munkahelyen. Mindezt MÁS SZEMLÉLETTEL, a dobozon kívülről, megismerve például azt, hogy miért fontos cégépítő alapelv a "két pizza" stratégia, és milyen titkot rejt a fehérre festett padló.
				Szalay Ádám (Sikerkód)
				<br /><br />
				Valamint lesz két workshop is délután: egy a NÖVEKVŐ SZEMLÉLETRŐL, egy a SIKERKÓDRÓL,</div>';
					
                                  //echo iconv("utf-8","iso-8859-2",$text);
                                  echo $text;
                                  
                                  ?>
                                </p>
                            </div>
                        </div>

                     
                    </div>
                </section>
        <!-- ## content boxes start ## -->
        <section id="content-boxes" class="scrollblock">
            <div class="container">
                <div class="row">

                   <ul class="bxslider">
					  <li><img src="prog1.jpg" /></li>
					  <li><img src="prog2.jpg" /></li>
					  <li><img src="prog3.jpg" /></li>
					  <li><img src="prog4.jpg" /></li>
					  <li><img src="prog5.jpg" /></li>
					  <li><img src="prog6.jpg" /></li>
					  <li><img src="prog7.jpg" /></li>
					  <li><img src="prog8.jpg" /></li>
					  <li><img src="prog9.jpg" /></li>
					  <li><img src="prog10.jpg" /></li>
					  <li><img src="prog11.jpg" /></li>
					</ul>
                </div>
            </div>
        </section>
        <!-- ## content boxes end ## -->
        
         <!-- ## parallax section 2 start ## -->
        <section id="parallax-section-2">
            <!-- ## parallax background 2 ## -->
            <div class="bg2"></div>
            <!-- ## parallax background pattern 2 start ## -->
            <div class="bg-pattern2" style="background: none;">
                <!-- ## team section start ## -->
                <section id="team" class="scrollblock_team">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <h2>Előadók
                             <span class="line"><span class="dot"></span></span>
                                </h2>
                            </div>
                        </div>
                        <div class="row0">
                            <div class="col-md-3 col-sm-6">
                                <div class="col-lg-12 member-box">
                                    <div class="member-img">
                                        <a href="#"><img class="img-responsive" src="assets/images/team/_Kepes_Andras_ff.jpg" alt=".img-responsive" /></a>
                                    </div>
                                    <div class="member-data">
                                        <div class="member-info">
                                            <div class="member-name"><a href="#">Prof. Kepes András</a></div>
                                            <div class="member-title">TRIPLAcoaching és Budapesti Metropolitan Egyetem.</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="member-content">Sokan sikeres tévéműsorai és könyvei alapján emlékeznek rá, alapító dékánja a Budapesti Metropolitan Egyetem Kommunikációs és Művészeti Karának, kommunikációs tréningeket tart, és Dettai Máriával és Imre Tóvári Zsuzsával alapítója a T R I P L A coaching-nak.</div>
                                    </div>
                                    <div class="clearfix"></div>
                               </div>
                            </div>
                                
                                <!-- ## member box end ## -->
                            <div class="col-md-3 col-sm-6">
                                <div class="col-lg-12 member-box">
                                    <div class="member-img">
                                        <a href="#"><img class="img-responsive" src="assets/images/team/_Szalay_Adam_ff.jpg" alt=".img-responsive" /></a>
                                    </div>
                                    <div class="member-data">
                                        <div class="member-info">
                                            <div class="member-name"><a href="#">Szalay Ádám </a></div>
                                            <div class="member-title">Sikerkód</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="member-content">Motivációs tréner, coach és kommunikációs szakember.  A TV2 egykori Naplójának főszerkesztő-riportereként húsz évnyi, frontvonalbeli médiatapasztalattal érkezett a személyiségfejlesztésbe. Ma már számos, értékesítéssel foglalkozó nagyvállalat állandó trénere; programjával, a Sikerkóddal az a legfontosabb célja, hogy örömöt vigyen a munkába.</div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                                <!-- ## member box end ## -->
                            <div class="col-md-3 col-sm-6">
                                <div class="col-lg-12 member-box">
                                    <div class="member-img">
                                        <a href="#"><img class="img-responsive" src="assets/images/team/_Molnar_Erika_ff.jpg" alt=".img-responsive" /></a>
                                    </div>
                                    <div class="member-data">
                                        <div class="member-info">
                                            <div class="member-name"><a href="#">Molnár Erika</a></div>
                                            <div class="member-title">Szereprulett</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="member-content">2004-ben alapította a Szereprulettet, amelynek keretein belül gender-érzékeny csoportterápiás módszertan kidolgozására és alkalmazására fókuszál. Egyik lábával az üzleti-tanácsadó szektorban, a másikkal pedig az önismereti szakmában áll, ezért alapos rálátással és megértéssel rendelkezik a vállalati szereplőket érintő egyéni és csoportos kihívásokkal kapcsolatban.</div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                                <!-- ## member box end ## -->
                       
                                
                            <div class="col-md-3 col-sm-6">
                                <div class="col-lg-12 member-box">
                                    <div class="member-img">
                                        <a href="#"><img class="img-responsive" src="assets/images/team/_Kiss_Peti_ff.jpg" 	 /></a>
                                    </div>
                                    <div class="member-data">
                                        <div class="member-info">
                                            <div class="member-name"><a href="#">Kiss Péter</a></div>
                                            <div class="member-title">RESTART alapító, volt médiaszakember (Tv2 csoport programigazgató)</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="member-content">Kiss Péter összesen 10 évig volt felsővezető a médiában, mielőtt megalapította a RESTARTOT! Szinte végig kreatív területen, kreatív emberek vezetésével foglalkozott, akkor és most is azt tartotta legfontosabb feladatának, hogy bevonva az embereket megmutassa, lehet kreatívan, alkotóan élni, és ez a jó érzésen túl anyagilag is nagyon profitábilis egyéni és cég szinten is.</div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                                <!-- ## member box end ## -->
                         </div>
                     </div>
                     
                     
                       <div class="container">
						 <div class="row0">
							<div class="col-md-3 col-sm-6">
								<div class="col-lg-12 member-box2">
									<div class="member-img">
										<a href="#"><img class="img-responsive" src="assets/images/team/_Pach_Gabi_ff.jpg" alt=".img-responsive"  /></a>
									</div>
									<div class="member-data">
										<div class="member-info">
											<div class="member-name"><a href="#"><font style="font-family:Aldrich; color:#000000; text-size:18px;"><strong>Pach Gabriella</strong></font></a></div>
											<div class="member-title">Talent-developer</div>
										</div>
										<div class="clearfix"></div>
										<div class="member-content">Végzettsége szerint jogi szakokleveles közgazdász és coach. HR és vezetői tapasztalatot olyan multinacionális vállalatoknál szerzett, mint a Tesco, British American Tobacco, Xerox, Syngenta és GE Lighting. Közel 20 éve dolgozik emberekkel HR szakemberként és vezetőként, közben folyamatosan fejlesztve saját magát és azokat akikkel együtt dolgozik. A tehetség fejlesztés nem csak hivatás számára, hanem szenvedély is egyben.</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="col-lg-12 member-box2">
									<div class="member-img">
										<a href="#"><img class="img-responsive" src="assets/images/team/_Friedl_Zsuzsa_ff.jpg" alt=".img-responsive" /></a>
									</div>
									<div class="member-data">
										<div class="member-info">
											<div class="member-name"><a href="#"><font style="font-family:Aldrich; color:#000000; text-size:18px;"><strong>Friedl Zsuzsanna </strong></font></a></div>
											<div class="member-title">Microsoft</div>
										</div>
										<div class="clearfix"></div>
										<div class="member-content">A Microsoft HR igazgatója. A cég jelenleg globális SZEMLÉLETVÁLTÁSban van, Zsuzsa ezt Magyarországon támogatja egy önálló, belső programmal.</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="col-lg-12 member-box2" >
									<div class="member-img">
										<a href="#"><img class="img-responsive" src="assets/images/team/_Karl_Aniko_ff.jpg" alt=".img-responsive" /></a>
									</div>
									<div class="member-data">
										<div class="member-info">
											<div class="member-name"><a href="#"><font style="font-family:Aldrich; color:#000000; text-size:18px;"><strong>Karl Anikó</strong></font></a></div>
											<div class="member-title">Mol-Training and Competency Development Manager</div>
										</div>
										<div class="clearfix"></div>
										<div class="member-content">Magyarország legnagyobb cégének képzési-, és kompetencia fejlesztő vezetőjeként folyamatosan új programokat keresnek, támogatják az innovatív szervezetfejlesztési megoldásokat.</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="col-lg-12 member-box2" >
									<div class="member-img">
										<a href="#"><img class="img-responsive" src="assets/images/team/_Havas_Gabor_ff.jpg" alt=".img-responsive" /></a>
									</div>
									<div class="member-data">
										<div class="member-info">
											<div class="member-name"><a href="#"><font style="font-family:Aldrich; color:#000000; text-size:18px;"><strong>Havas Gábor</strong></font></a></div>
											<div class="member-title"></div>
										</div>
										<div class="clearfix"></div>
										<div class="member-content">Szakmai pályafutását az ING Biztosítónál kezdte, majd a SIGNAL Biztosítónál termékmenedzseri munkakörben tevékenykedett. 2007-ben váltott az Aegon Közép-kelet európai szervezetébe, ahol akvizíciós és zöldmezős projektekért volt felelős, elsősorban Romániában és Törökországban; továbbá összefogta a nemzetközi értékesítési kapcsolatokat, és felügyelte a régiós stratégiai értékesítési kezdeményezéseket. 2011-től az Aegon Magyarország új értékesítési és ügyfél-kiszolgálási csatornájának, a Prémium Üzletágnak a vezetője.</div>												
									<div class="clearfix"></div>
								</div>
							</div> 
						</div>
					</div>
                       <div class="container">
						 <div class="row0">
							<div class="col-md-3 col-sm-6">
								<div class="col-lg-12 member-box3">
									<div class="member-img">
										<a href="#"><img class="img-responsive" src="assets/images/team/_Sarvari_Gyorgy_ff.jpg" alt=".img-responsive"  /></a>
									</div>
									<div class="member-data">
										<div class="member-info">
											<div class="member-name"><a href="#"><font style="font-family:Aldrich; color:#000000; text-size:18px;"><strong>Sárvári György</strong></font></a></div>
											<div class="member-title">Bardo Consulting</div>
										</div>
										<div class="clearfix"></div>
										<div class="member-content">Hazánk egyik meghatározó coach tanácsadója. Itthon és külföldön dolgozik és tanít a coaching, team-coaching területén. Négy könyve jelent meg, melyek közül a "Belső harcos útja, Coaching kézikönyv" sokáig piacvezető volt az üzleti könyvek között.</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="col-lg-12 member-box3">
									<div class="member-img">
										<a href="#"><img class="img-responsive" src="assets/images/team/_Komocsin_Laura_ff.jpg" alt=".img-responsive" /></a>
									</div>
									<div class="member-data">
										<div class="member-info">
											<div class="member-name"><a href="#"><font style="font-family:Aldrich; color:#000000; text-size:18px;"><strong>Komócsin Laura</strong></font></a></div>
											<div class="member-title">PPC</div>
										</div>
										<div class="clearfix"></div>
										<div class="member-content">A SPARKLE SELF COACHING GLOBAL CSR PROGRAM ötletgazdája és nagykövete. Az International Coach Federation magyar tagozatának alapító-elnöke. Kollegájával, Benedek Nikolettával közösen alkották meg az első magyar coaching modellt, a DIADAL-t.</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="col-lg-12 member-box3">
									<div class="member-img">
										<a href="#"><img class="img-responsive" src="assets/images/team/tari_annamaria.jpg" alt=".img-responsive" /></a>
									</div>
									<div class="member-data">
										<div class="member-info">
											<div class="member-name"><a href="#"><font style="font-family:Aldrich; color:#000000; text-size:18px;"><strong>Tari Annamária</strong></font></a></div>
											<div class="member-title">klinikai szakpszichológus, pszichoterapeuta, pszichoanalitikus.</div>
										</div>
										<div class="clearfix"></div>
										<div class="member-content">Több éve rendszeresen foglalkozik a fogyasztói társadalom hatásaival, a média és az emberi tényezők összefüggéseivel, a társadalmi változások egyénekre ható vonásaival. Több könyvet írt az X, Y, Z generációkról, legfrissebb kötete a 2015. decemberében megjelent Generációk online, az online tér és az emberi kapcsolatok változásait vizsgálja. Y generációs rovata rendszeresen hallható az MR2 Kultúrfitnesz műsorában.</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>

							
						</div>
					</div>
                </section>
                <!-- ## team section end ## -->
            </div>
            <!-- ## parallax background pattern 2 end ## -->
        </section>
        <!-- ## parallax section 2 end ## -->




       
        
        <!-- ## contact section start ## -->
        <section id="contact" class="scrollblock">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h2>Jelentkezés
                        <span class="line"><span class="dot"></span></span>
                        </h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 contact-form contact-box">
                        <h4>Jelentkezzen a HR Piknikre!</h4>
                        <p>
						A RESTART ÉS A HRPORTAL BEMUTATJA: HRPIKNIK 2016<br>
						
						</p>
						<p>
							<b>A workshop díja: Earlybird március 15-ig 17 900 Ft + áfa/ fő.</b><br />
							<b>Március 15 után 22 900 Ft + áfa/fő</b><br />
							
							
A díj tartalmazza az ebédet és a napközbeni finomságokat, frissítőket.
						</p>
						
                      <form name="form-contact-us" id="form-contact-us">
                       
                            <div class="form-group">
								
                            Online jelentkezéséről e-mailben visszaigazolást küldünk. Amennyiben a visszaigazoló levélben rákattint az aktiváló linkre, azzal elfogadja az adatok helyességét. Ezt követően a jóváhagyott adatok alapján átutalásos számlát küldünk a részvételi díjról, melyet kérünk a regisztráció visszaigazolásától számított 8 munkanapon belül - de legkésőbb a rendezvény napjáig - kiegyenlíteni. Az online regisztráció megrendelésnek minősül.
							<br><br>A rendezvényen a részvétel feltétele a részvételi díj előzetes kiegyenlítése. Lemondást a részvételi díj teljes visszafizetése mellett a rendezvény napja előtti 5. munkanapon 12 óráig - KIZÁRÓLAG ÍRÁSBAN a info@hrportal.hu címre - fogadunk el. Az ezutáni lemondások esetében a részvétel átruházható, de a teljes díjat ki kell fizetni. Telefonos vagy egyéb szóbeli lemondásokat nem áll módunkban elfogadni.
							<br><br>A lemondási határidő utáni regisztráció esetén lemondásra nincs lehetőség.
                            </div>
							<div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="txtName">Név: </label>
                                        <input type="text" placeholder="Név"
                                            class="form-control required" id="txtName" name="txtName" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="txtEmail">E-mail:</label>
                                        <input type="text" placeholder="E-mail ID"
                                            class="form-control required email" id="txtEmail" name="txtEmail" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="txtphone">Telefonszám:</label>
                                        <input type="text" placeholder="Telefonszám"
                                            class="form-control required phone" id="txtphone" name="txtphone" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="txtszlaname">Számlázási név:</label>
                                        <input type="text" placeholder="Számlázási név"
                                            class="form-control " id="txtszlaname" name="txtszlaname" />
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="txtszlaaddr">Számlázási cím:</label>
                                        <input type="text" placeholder="Számlázási cím"
                                            class="form-control " id="txtszlaaddr" name="txtszlaaddr" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="txtMessage">Üzenet:</label>
                                <textarea placeholder="Message" class="form-control required"
                                    id="txtMessage" name="txtMessage" rows="10"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="txtCaptcha"></label>
                                <input
                                    type="text" class="form-control required captcha"
                                    placeholder="Egy keresztkérdés?" id="txtCaptcha" name="txtCaptcha" />
                            </div>
                            <div class="form-group">
                                <input type="button" id="btn-Send" value="Küld" class="btn btn-theme-inverse" />
                            </div>
                            <div class="col-lg-12" id="contact-form-message">
                        </form>
                        <!-- ## contact form end ## -->
                    </div>
                    </div>

                    <div class="col-sm-6 contact-info contact-box">
                        <h4>Elérhetőségek</h4>
                        <!-- ## contact info start ## -->
                        <ul class="fa-ul">
                            <li><i class="fa-li fa fa-clock-o text-theme-inverse"></i><span>Időpont: 2016. május 10. 9:00</span></li>
                            <li><i class="fa-li fa fa-home text-theme-inverse"></i><span>1095 Budapest Komor Marcell u. 1.</span></li>
                            <li><i class="fa-li fa fa-envelope text-theme-inverse"></i><span>info@hrportal.hu</span></li>
                            <li><i class="fa-li fa fa-globe text-theme-inverse"></i><span>www.hrportal.hu/hrpiknik_2016_tavasz/</span></li>
                        </ul>
                        <!-- ## contact info endstart ## -->
                        <!-- ## map start ## -->
<div id="map-canvas"></div>
                        <!-- ## map end ## -->
                    </div>
                </div>
            </div>
        </section>
        <!-- ## contact section end ## -->

        <!-- ## blockquote section start ## -->
        <section id="bquote" class="scrollblock">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-lg-offset-1 quote-box">
                        <blockquote>
                            <p>Jelentkezzen programunkra!</p>
                        </blockquote>
                    </div>
                </div>
            </div>
        </section>
        <!-- ## blockquote section end ## -->


        <!-- ## twitter section end ## -->

        <!-- ## footer section start ## -->
        <section id="footer">
            <!-- ## footer background pattern start ## -->
            <div class="bg-pattern3">

                <!-- ## footer copyright start ## -->
                <div class="footer-copyright text-center">
                    HRPortal.hu Copyright &copy; 2016, All rights reserved.
                </div>
                <!-- ## footer copyright end ## -->
            </div>
            <!-- ## footer background pattern end ## -->
        </section>
        <!-- ## footer section end ## -->

        <!-- ## preloader start ## -->
        <div id="preloader">
            <div id="status"><span class="fa fa-spin fa-spinner fa-5x"></span></div>
        </div>
        <!-- ## preloader end ## -->

        <!-- ## Time Bar begins here ## -->
        <!-- ## do not change it. ## -->
        <div id="progress-back" class="load-item">
            <div id="progress-bar"></div>
        </div>
        <!-- ## Time Bar ends here ## -->
    </div>
    <!-- ## page wrapper end ## -->

    <script src="assets/js/jquery-2.0.0.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/twitter/jquery.tweet.min.js" type="text/javascript"></script>
    <script src="assets/js/jquery.parallax-1.1.3.js" type="text/javascript"></script>
    <script src="assets/js/jquery.nicescroll.min.js" type="text/javascript"></script>
    <script src="assets/js/detectmobilebrowser.js" type="text/javascript"></script>
    <script src="assets/js/jquery.smooth-scroll.js" type="text/javascript"></script>
    <script src="assets/js/jquery.scrollorama.js" type="text/javascript"></script>
    <script src="assets/js/jquery.easing.min.js" type="text/javascript"></script>
    <script src="assets/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="assets/js/jquery.cookie.js" type="text/javascript"></script>
    <script src="assets/ytplayer/js/jquery.mb.YTPlayer.js" type="text/javascript"></script>
    <script src="assets/js/app.variables.js" type="text/javascript"></script>
    <!--<script src="assets/js/jquery.countdown.js" type="text/javascript"></script>
    <script src="assets/js/app.subscribe.js" type="text/javascript"></script>
    <script src="assets/js/app.contact.js" type="text/javascript"></script>
    <script src="assets/js/app.ui.js" type="text/javascript"></script>
    <script src="assets/js/app.styleswitcher.js" type="text/javascript"></script>-->
    <script src="/js/jquery.bxslider.min.js"></script>

    <script src="assets/js/app.landlite.js" type="text/javascript"></script>
    <script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-696329-1', 'auto');
	  ga('send', 'pageview');

	</script> 
	<script type="text/javascript">
		<!--//--><![CDATA[//><!--
		var pp_gemius_identifier = 'bDhFBhiFYYCqPthkhx47lXZJHSGNJKL9TT54fXM4747.D7';
		// lines below shouldn't be edited
		function gemius_pending(i) { window[i] = window[i] || function() {var x = window[i+'_pdata'] = window[i+'_pdata'] || []; x[x.length]=arguments;};};
		gemius_pending('gemius_hit'); gemius_pending('gemius_event'); gemius_pending('pp_gemius_hit'); gemius_pending('pp_gemius_event');
		(function(d,t) {try {var gt=d.createElement(t),s=d.getElementsByTagName(t)[0],l='http'+((location.protocol=='https:')?'s':''); gt.setAttribute('async','async');
		gt.setAttribute('defer','defer'); gt.src=l+'://huadn.hit.gemius.pl/xgemius.js'; s.parentNode.insertBefore(gt,s);} catch (e) {}})(document,'script');
		//--><!]]>
		</script>
</body>
</html>
