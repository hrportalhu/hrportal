#!/bin/bash
MY_NAME="Altalanos mentoscript"
MY_PERIOD="weekly"
MY_BASEDIR="/var/backups/"
MY_ARCHDIR1="/mnt/river/var/samba/Planetstar/Save/Interfood/"
MY_ARCHIVER="/bin/bzip2"
MY_MYSQLPW=6rometheus
MY_MYSQL_DBS="if_plsys_hu"
MY_MYSQLDUMP="/usr/bin/mysqldump"
	
if [ -d $MY_BASEDIR ]; then
   cd $MY_BASEDIR
else
   echo "FIGYELEM! Nem letezik a megadott mentes konyvtar! Az aktualis konyvtarat fogom hasznalni."  
fi
if [ -x $MY_ARCHIVER ]; then
    if [ $(echo $MY_ARCHIVER | grep 'bzip2') != '' ]; then
        MY_SAVEFNAME=`date +%d`_daily.tar.bz2
	MY_ARCHOPTS=' -cjf'
    elif [echo $MY_ARCHIVER | grep 'gzip' != '']; then
        MY_SAVEFNAME=`date +%d`_daily.tar.gz
        MY ARCHOPTS=' -czf'
    else
        echo "Nem ismerem ezt az archivalo programot: $MY_ARCHIVER"
	exit
    fi
else
    echo "Nincs megfelelo archivalo program megadva: $MY_ARCHIVER"
    exit
fi

  
   for i in $MY_MYSQL_DBS; do
     $MY_MYSQLDUMP -ujoee -p$MY_MYSQLPW $i > $i.db
     tar $MY_ARCHOPTS dbsave-$i-$MY_SAVEFNAME $i.db > /dev/null
     rm $i.db
   done

cp  dbsave-$i-$MY_SAVEFNAME $MY_ARCHDIR1.
