<?PHP
//      Funkcio:        Planet Star alap funkciok
//
//                      Verzió  Dátum        	Módosította     Módosítások
//      Verziokovetes:  1.002	2010.08.14      six             Másolás a lib alól
//						1.100	2011.06.29		six				Kód tisztítás keret2-hoz

if(basename($_SERVER['PHP_SELF']) == '_functions.php'){print "Nem megengedett hozzaferesi mod!";exit;}



// safe_chars csere
$wrong=array('[','\\',"'",'"','$','%',']','@','/');
$right=array('&#91;','&#92;','&apos;','&quot;','&#36;','&#37;','&#93;','&#64;','&#47;');

foreach($_GET as $k=>$v){if(get_magic_quotes_gpc()){$_GET[$k]=stripslashes($v);}$_GET[$k]=str_replace($wrong,$right,$v);}

$arrfrom=array("<",">",'"',"%","‘","’","‚","“","”","„","'","!","|",'/',"[","]","(",")","{","}","*","?","@",":","$");
$arrto=array("&#60;","&#62;","&quot;","&#37;","&#8216;","&#8217;","&#8218;","&#8220;","&#8221;","&#8222;","&#39;","&#33;","&#124;","&#47;","&#91;","&#93;","&#40;","&#41;","&#123;","&#125;","&#42;","&#63;","&#64;","&#58;","&#36;");
foreach($_POST as $k=>$v){if(get_magic_quotes_gpc()){$_POST[$k]=stripslashes($v);}
    $v=preg_replace('/&([a-z]{2,4});/i','[[$1]]',$v); // A &quot; típusúakat cseréli
    $v=preg_replace('/&#([0-9]{2,4});/i','[[$1]]',$v); // A &#92; típusúakat cseréli
    $v=str_replace("&","&amp;",$v); // Elméletben ezután már csak szövegbeli & lehet
    $_POST[$k]=str_replace($arrfrom,$arrto,$v); // Ezek után cserélhetjük a veszélyes karaktereket
}
// '/safe_chars csere

global $connid;

/* MYSQL specific db functions */
if(!$connid = @mysql_pconnect(DB_SERVER, DB_USER, DB_PASSWORD)){
	die ("<p class='error'>Cannot connect to mysql server. Is the server running?</p>");
}

if (!mysql_select_db(DB_NAME, $connid)){
	mysql_query("CREATE DATABASE ".DB_NAME." DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci", $connid);
	@mysql_select_db(DB_NAME,$connid) or die ("<p class='error'>Cannot find database. Please initialize database (".DB_NAME.")!</p>");
}
mysql_query("SET NAMES utf8;");
$c=db_one("SELECT id FROM users LIMIT 1");
if($c==''){
    echo "<p class='error'>".eee("Úgy tűnik, ez a rendszer első indulása. A továbbiakban létrehozom az alap rendszert. Ez eltarthat 30-60 másodpercig.</p>");
    include_once dirname(__FILE__).("/_dbinstall.php");
}

function db_execute($query,$debug=0){
	if($debug==1){
		echo $query;
	}
	extract($GLOBALS);$qid=mysql_query($query,$connid);
	return $qid;
}

function db_rownumber($qid){
	extract($GLOBALS);$rows=@mysql_num_rows($qid);if($rows=='') $rows=0;
	return $rows;
}

function db_one($query, $i=0){
	extract($GLOBALS);$result=mysql_query($query,$connid);
 	$output=mysql_fetch_array($result);
	return $output[0];
}

function db_row($query, $i = 0){
	extract($GLOBALS);$result=mysql_query($query,$connid);
 	$output=mysql_fetch_array($result);
	return $output;
}

function db_onerow($r,$i=MYSQL_ASSOC){
	extract($GLOBALS);return mysql_fetch_assoc($r);
}
function db_multirow($result, $i = 0){
	extract($GLOBALS);
	$myar = array();
	for($myi=0;$myi<db_rownumber($result);$myi++){
        $output = mysql_fetch_array($result,MYSQL_ASSOC);
        foreach($output as $key => $value){
            if($key != '0'){$myar[$myi][$key] = $value;}
        }
    }
	return $myar;
}

function db_all($query,$debug=0){
	extract($GLOBALS);
	if($debug==1){
		echo $query;	
	}
	$result=mysql_query($query,$connid);
	$myar = array();
	$rows=@mysql_num_rows($result);
	for($myi=0;$myi<$rows;$myi++){
		$output=mysql_fetch_assoc($result);
		foreach($output as $key => $value){
			if($key != '0'){
				$myar[$myi][$key] = $value;
			}
		}
	}
	return $myar;
}
/* /MYSQL specific db functions */

/*	Dangerous characters - a biztonságos betuket visszacseréli az eredetire (pl. jelszó ellenőrzés,
 *   email-cím ellenőrzés igényli)
 * */
function dangChars($s){

	$arrfrom=array("&#60;","&#62;","&quot;","&#37;","&#8216;","&#8217;","&#8218;","&#8220;","&#8221;","&#8222;","&#39;","&#33;","&#124;",
			"&#47;","&#91;","&#93;","&#40;","&#41;","&#123;","&#125;","&#42;","&#63;","&#64;","&#58;","&#36;");
	$arrto=array("<",">",'"',"%","‘","’","‚","“","”","„","'","!","|",'/',"[","]","(",")","{","}","*","?","@",":","$");
// Forditott sorrendben jarunk el, mint a safe_chars esetében
	$s=str_replace($arrfrom,$arrto,$s);
	$s=str_replace("&amp;","&",$s);
	$s=preg_replace('/\[\[([0-9]{2,4})\]\]/i','&#$1;',$s); // A &#92; típusúakat cseréli (vissza)
	return preg_replace('/\[\[([a-z]{2,4})\]\]/i','&$1;',$s); // A &quot; típusúakat cseréli (vissza)
}

/* Jogosult-e a felhasznalo hasznalni egy funkciot
 * Szintaxis:
 *  jogosult('user','partners') vagy jogosult('user',5) *
 *  jogosult('group','partners',<group_id>) vagy jogosult('group',5,<group_id>)
 *
 * */

function jogosult($who,$whatfunc,$id=0){

	if(!is_numeric($whatfunc)){
		$whatfunc = db_one("SELECT id FROM functions WHERE funcname = '$whatfunc'");
	}

	if($who=='user'){
		if($id===0 && isset($_SESSION['planetsys']['user_id'])){
			$gid = db_one("SELECT group_id FROM users WHERE id = '".$_SESSION['planetsys']['user_id']."'");
		}	
		else{
			$gid = db_one("SELECT group_id FROM users WHERE id = '$id'");
		}
		if($gid == ''){return false;}
		$jog=db_execute("SELECT id FROM func_x_gr WHERE function_id = '$whatfunc' AND group_id = '$gid'");
	}elseif($who=='group'){
		if($id===0) return false;
		$jog=db_execute("SELECT id FROM func_x_gr WHERE function_id = '$whatfunc' AND group_id = '$id'");
	}
	else{
		return false;
	}
	return (db_rownumber($jog)>0);
}
function priv($funcname,$subfunc=""){
	if($subfunc==""){
		$funcid=db_one("select id from functions where funcname='".$funcname."'");
	}
	else{
		$funcid=db_one("select id from functions where funcname='".$funcname."' and subfunction_code='".$subfunc."'");
	}
	
	$priv=db_one("select status from fu_x_us where user_id='".$_SESSION['planetsys']['user_id']."' and func_id='".$funcid."'");
	//echo"select status from fu_x_us where user_id='".$_SESSION['planetsys']['user_id']."' and func_id='".$funcid."'";
	
	
	if($priv==""){
		$priv=0;	
	}
	//$priv=1;
	return $priv;
}

function mkheader($mylabel,$myimagename){
	if($myimagename!=""){
		//echo $myimagename;
		echo"<h1 class=\"pagetitle\"><img src=\"".$myimagename."\" alt=\"fopic\" style=\"border:0;\"/> ".$mylabel."</h1>";
	}
	else{
			echo"<h1 class=\"pagetitle\">".$mylabel."</h1>";
	}
}

function parseget($newstr){
$getparameters = '?';foreach ($_GET as $getkey => $getval){
if(strpos($newstr, $getkey.'=') !== false){$stpos = strpos($newstr, $getkey.'=');$cutstr = substr($newstr, $stpos, (strpos($newstr, '&', $stpos)));$getparameters .= substr($newstr, $stpos, (strpos($newstr, '&', $stpos))).'&';$newpl=strlen(substr($newstr, $stpos, (strpos($newstr, '&', $stpos))));
$newstr = str_replace($cutstr,"",$newstr);if($newstr == '&'){$newstr = '';}}else{$getparameters .= $getkey .'='. preg_replace('/[\'";$%]/','',$getval) .'&';}}$getparameters.=$newstr;if(strpos($getparameters,'adm=') === true)
{$getparameters.='adm=help';}else{$getparameters = substr($getparameters,0,-1);}$getparameters = str_replace('&&','&',$getparameters);
return $getparameters;
}


function antikiddie($szoveg) {
    $szoveg=preg_replace("`<([\/b]?[abipr])>`im","`!--\\1--`",$szoveg);
//$szoveg=preg_replace("`<([\/?[abipr])>`im","`!--\\1--`",$szoveg);
    $szoveg=preg_replace("`<(\/?[oule][mli])>`im","`!--\\1--`",$szoveg);
    $szoveg=preg_replace("`<(\/?h[12345])>`im","`!--\\1--`",$szoveg);
    $szoveg=preg_replace("`<(\/?strong)>`im","`!--\\1--`",$szoveg);
    $szoveg=preg_replace("`<(\/?t[dr])>`im","`!--\\1--`",$szoveg);
    $szoveg=preg_replace("`<(\/?table)>`im","`!--\\1--`",$szoveg);
    $szoveg=preg_replace("`<(\/?t[dr]\s+[\w\/\"\'\=\+\\\?\!\.\|\:\;\,\{\}\[\]\-\@\#\$\%\^\&\*\(\)\~\s]+)>`im","`!--\\1--`",$szoveg);
    $szoveg=preg_replace("`<(\/?table\s+[\w\/\"\'\=\+\\\?\!\.\|\:\;\,\{\}\[\]\-\@\#\$\%\^\&\*\(\)\~\s]+)>`im","`!--\\1--`",$szoveg);
    $szoveg=preg_replace("`<(img\s+[\w\/\"\'\=\+\\\?\!\.\|\:\;\,\{\}\[\]\-\@\#\$\%\^\&\*\(\)\~\s]+)>`im","`!--\\1--`",$szoveg);
    $szoveg=preg_replace("`<([ap]\s+[\w\/\"\'\=\+\\\?\!\.\|\:\;\,\{\}\[\]\-\@\#\$\%\^\&\*\(\)\~\s]+)>`im","`!--\\1--`",$szoveg);
    $szoveg=preg_replace("`\<[\w\/\"\'\=\+\\\?\!\.\|\:\;\,\{\}\[\]\-\@\#\$\%\^\&\*\(\)\~\s]*\>`im","",$szoveg);
    $szoveg=preg_replace("`\`!--([\w\/\"\'\=\+\\\?\!\.\|\:\;\,\{\}\[\]\-\@\#\$\%\^\&\*\(\)\~\s]+)--\``im","<\\1>",$szoveg);
    $szoveg=preg_replace("'<(|/)(span|o:p).*?>'si", "", $szoveg);
    $szoveg=preg_replace  ("' style.*?>'si", ">", $szoveg);
    $szoveg=str_replace("#8211;","-",$szoveg);
    $szoveg=str_replace(chr(132),"\"",$szoveg);
    $szoveg=str_replace(chr(148),"\"",$szoveg);
    $szoveg=str_replace(chr(146),"'",$szoveg);
  # $szoveg=str_replace(chr(150),"-",$szoveg);
return $szoveg;
}

function sqlize($s){
	return(str_replace("\'","'",str_replace("'","''",str_replace('"',"&quot;",str_replace("\n","<br />",antikiddie($s))))));
}
function resqlize($s){
	return(str_replace("'","\'",str_replace("''","'",str_replace("&quot;",'"',str_replace("<br />","\n",antikiddie($s))))));
}


function szamlista($db,$act,$url,$name){
	$pre="";
	$egysor=5;
	$szelet=ceil($act/$egysor);
	//echo"<a href=\"".$url.$name."=1><< Első</a>&nbsp;&nbsp;";
	echo"<a href=\"".$url.$name."=1\"><< Első&nbsp;</a>";
	if ($act>1){
		echo "<a href=\"".$url.$name."=".($act-1)."\">";
	}
	echo "&lt;&nbsp;El&#x0151;z&#x0151;&nbsp;";
	if ($act>1){
		echo "</a> ";
	}
	if ($szelet>1){
		echo "<a href=\"".$url.$name."=".(($szelet-2)*$egysor+1)."\">";
		echo "...";
		echo "</a> ";
	}
	echo "&nbsp;";
	for ($i=($szelet-1)*$egysor+1;($i<=$db)and($i<$szelet*$egysor+1);$i++){
		if ($i!=$act){
			echo " <a href=\"".$url.$name."=".$i."\">";
		}else{
			echo " <b>";
		}
		echo $i;
		if ($i!=$act){
			echo "</a> ";
		}else{
			echo "</b> ";
		}
	}
	echo "&nbsp;";
	if ($szelet<ceil($db/$egysor)){
		echo "<a href=\"".$url.$name."=".($szelet*$egysor+1)."\">";
		echo "...";
		echo "</a> ";
	}
	if ($act<$db){
		echo " <a href=\"".$url.$name."=".($act+1)."\">";
	}
	echo " K&ouml;vetkez&#x0151;&nbsp;&gt;";
	if ($act<$db){
		echo "</a>";
	}
	echo"&nbsp;&nbsp;<a href=\"".$url.$name."=".($db)."\">Utolsó >></a>";
}
function naplo($level,$type,$message,$modul='',$func='',$valamiid='',$sqlins=''){
/*Naplózási szintek: info,warn,critical,error
 * Naplózási típusok: system,user,admin
 * 
 */ 	
	
	$uid=$_SESSION['planetsys']['user_id'];
	$cid=$_SESSION['planetsys']['customer']['id'];
	
	if($uid==''){
		$uid=0;
		$username="System";
		$cid=0;
		$cname="System";
	}
	else{
		$username=$_SESSION['planetsys']['realname'];
		$cname=$_SESSION['planetsys']['customer']['name'];
	}
	/*
	if($type!='admin'&&$type!='system'&&$type!='user'){$type='system';$level='error';}
	if($level!='info'&&$level!='warn'&&$level!='error'&&$level!='crit'){$level='warn';}
	*/
	if((SYS_LOGLEVEL==0&&$level=='crit') ||
	   (SYS_LOGLEVEL==1&&($level=='crit'||$level=='error')) ||
	   (SYS_LOGLEVEL==2&&($level=='crit'||$level=='error'||$level=='warn')) ||
	   (SYS_LOGLEVEL>2)
	  ){
		$insertlog=db_execute("INSERT INTO ".DB_TABLEPREFIX."logs (rdate,user_id,user_name,customer_id,customer_name,loglevel,logtype,message,ip,url,modul,func,function_dataid,sqlins) 
		VALUES(NOW(),'".$uid."','$username','".$cid."','".$cname."','$level','$type','$message','".$_SERVER['REMOTE_ADDR']."','".$_SERVER['REQUEST_URI']."','".$modul."','".$func."','".$valamiid."','".$sqlins."')");
		if($insertlog!=1){
			if (!$fh = @fopen("planetsys_paniclog.txt", "a")){
				echo "Critical error, system logging is not possible!";
				die;
			}
			if (fwrite($fh, date("Y.m.d H:i:s", strtotime("now")).
				"$uid $username $level $type $message\n") === FALSE){
					echo "Critical error, system logging is not possible!";
					die;
			}
			@fclose($fh);
		}
	   }
}


function limit_text($myvalue,$length,$separator=" "){
    $myvalue=dangChars($myvalue); // vissza kell alakitanunk, mivel a kulonleges karakterek
                                  // sok helyet foglalnak es nem lesz jo a limitalas. Pl. a
                                  // felkialtojelbol &#33; lesz ami 5x annyi helyet foglal
    if(strlen(utf8_decode($myvalue))<=$length)return $myvalue;
    $avalue="";
    $av=split(" ",$myvalue);
    foreach($av as $ava){
        if(strlen($avalue." ".$ava)<=$length){
            $avalue.=$ava." ";
        }else{break;}
    }
    $avalue=substr($avalue,0,-1);
    if($avalue==""){$avalue=substr($av[0],0,$length-3)."...";}
    if(strlen($myvalue)>strlen($avalue)){$avalue.="...";}
    return $avalue;
}

function safeNames($check,$ponte=1){

    if($ponte==1){ // Lehet szam, kotojel, pont, alahuzas es betu
    $newchars=array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u",
                    "v","w","x","y","z","_",".","-","1","2","3","4","5","6","7","8","9","0");
    }elseif($ponte==2){ // A leheto legszigorubb, csak alfanumerikus jatszik
    $newchars=array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u",
                    "v","w","x","y","z","1","2","3","4","5","6","7","8","9","0");
    $check=str_replace(' ','',strtolower($check));
        }
    else{ // Lehet betu, szam, alahuzasjel es kotojel, de pont NEM
    $newchars=array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u",
                    "v","w","x","y","z","_","-","1","2","3","4","5","6","7","8","9","0");
    }
    $safename = "";
    for($j=0;$j<strlen($check);$j++){
        if(in_array($check[$j],$newchars)){ $safename .= $check[$j];}
        else{ $safename .= "_";}
    }
    return $safename;
}

function safe_filename($text){
	$oldchars=array("á","é","í","ó","ö","ő","ú","ü","ű","Á","É","Í","Ó","Ö","Ő","Ú","Ű"," ",",");
    $newchars=array("a","e","i","o","o","o","u","u","u","A","E","I","O","O","O","U","U","_","_");
    $filename=strtolower(str_replace($oldchars,$newchars,$text));
    if (preg_match ("/^.*\\\\(.*)$/", $filename, $regs)){$filename = $regs[1];}
	return safeNames($filename);
}

/* Eksemel: Az ajax* fájlok szabványos XML válaszát állítja össze a hívó oldal számára.
   A visszaadott XML így fog kinézni:
   <?xml version='1.0' encoding='utf-8'?>
      <screen>
         <status>0</status>
         <error>Első hiba</error>
         <error>Második hiba</error>
		 <warn>Ez csak figyelmeztetés</warn>
         <msg>Ez pedig csak egy felhasználót tájékoztató üzenet (pl. adatbázis update sikeres)</msg>
         <fields>
          <![CDATA[
            (Ide jön tetszőleges html, ami eddig is)
          ]]>
         </fields>
         <actions>
          <![CDATA[
            (Ide jön tetszőleges Javascript, ami le fog futni)
          ]]>
         </actions>
      </screen>

	Az üzeneteket tömbként kell átadni, így: array(1 => array('type' => 'error', 'msg' => 'Baj van!'), 2 ....)
	Ezt megteszi az eeea függvény!
*/
function eksemel($statusCode,$messageArray="",$htmlCode="",$jsCode=""){

	$ret="<?xml version='1.0' encoding='utf-8'?>
      <screen>
         <status>$statusCode</status>\n";
	if($messageArray!=""){
		foreach($messageArray as $m){
			if($m['type']!='') $ret.="         <".$m['type'].">".$m['msg']."</".$m['type'].">\n";
		}
	}
	if($htmlCode!=""){
		$ret.="         <fields>
          <![CDATA[
          $htmlCode
          ]]>
         </fields>\n";
	}
	if($jsCode!=""){
		$ret.="         <actions>
          <![CDATA[
          $jsCode
          ]]>
         </actions>\n";
	}
	return $ret."      </screen>\n";
}


// Ezek esetleg kellhetnek:
// resizeImage
// sec2time
// formatBytes

/* User and GUI-related functions */
function crypt_password($clear, $salt=''){
// Ha kódolva küldjük a klienstől a szerverig, akkor challenge-et kell használnunk
// viszont akkor nem sózhatunk, hiszen az eredeti jelszót nem tudjuk, csak a challenge-et
//       md5(clearpassw) = db_passw
//       md5(salt1 + md5(clearpassw)) = md5(salt1 + db_passw)
//    Mivel az md5 mindig ugyanazt adja, ezert a salt (mas neven challenge) az, ami a
//    halozaton utazva "feltorhetetlenne" teszi a jelszot. Sajnos az adatbazisban viszont
//    csak az md5 hash-t tudjuk tarolni, sot nem, mert:
//       md5(salt1 + clearpassw) = db_passw
//       md5(salt2 + md5(clearpassw) = md5(salt2 + md5(salt1 + clearpassw))
//    A probléma, hogy a salt1-et nem tudjuk, mert azt akkor hasznajuk, ha a user megadja a jelszavat
//    Eltarolhatnank persze az adatbazisban azt is, de ez semmivel sem biztonsagosabb, minthogy ugyanaz
//    a jelszo ugyanarra a hash-re kodolodik le.
//
//    Magyarul nem használhatjuk azt a szabályt, hogy
//       crypt(clearpassw,db_passw) = db_passw
//    azaz
//		 crypt2(clearpassw,crypt1(clearpassw)) = crypt1(clearpassw);
//
//    if ($salt!==null){
//        $salt=substr($salt,0,12);
//    }
//    else{
//        $salt=substr(md5(uniqid(rand(),true)),0,12);
//    }
//    return crypt($clear,$salt);

	  return md5($clear);

}

function eee($htext){
	if(isset($_SESSION['lang'])){$mylang=$_SESSION['lang'];}else{$mylang='hu';}
	if($mylang=='hu')return $htext;

    if(isset($_SESSION['langarray'][$mylang][$htext])){
        return $_SESSION['langarray'][$mylang][$htext];
    }
    else{
		$getT=db_execute("SELECT translated FROM language WHERE original = '$htext' AND langcode = '$mylang'");
		$T=db_onerow($getT);
		$sztring=$T['translated'];
		if($sztring!=""){
			if(count($_SESSION['langarray'][$mylang])>100)$tmp=array_shift($_SESSION['langarray'][$mylang]);
			if($htext!=$sztring)$_SESSION['langarray'][$mylang][$htext]=$sztring;
			return $sztring;
		}else{
			db_execute("INSERT INTO language(original,translated,langcode)VALUES('$htext','$htext','$mylang')");
			return $htext;
		}
    }
}

/* eeea = Visszatér az üzenettel adott nyelven, tömbként
 * 		  Erre a plaform-os megoldásnak elengedhetetlenül szüksége van. A strukturált xml átadáshoz előbb egy
 * 		  tömb alakban állítjuk elő a hiba, figyelmeztető és tájékoztató üzeneteket. Eddig az $errmsg string
 *        változóban volt egyszerűen benne, de most a $errmsg[] tömbnek kell értéket adni
 *        $errmsg = eeea(<tipus>,<üzenet>); ahol tipus = error, warn, msg lehet.
 * */
function eeea($tip,$htext){
	return array("type" => $tip, "msg" => eee($htext));
}

function validate_password($p,$p2){
	// FIXME ide kell egy unsafe_chars függvényt hívni, mert a megadott jelszóban lévő ékezetes stb. karaktereket
	// lecserélte a _functions elején lévő safe_chars cserélő

	if(!preg_match("/^[-_\.0-9a-zA-Z]*$/", $p))return eee("Nem megengedett karakterek a jelszóban!");
	if($p!=$p2)return eee("A két jelszó nem egyezik!");
	if(PASS_MINLENGTH > 0 && strlen($p)<PASS_MINLENGTH) return eee("A jelszó hossza minimum ".PASS_MINLENGTH." karakter!");
	if(PASS_MAXLENGTH > 0 && strlen($p)>PASS_MAXLENGTH) return eee("A jelszó hossza maximum ".PASS_MAXLENGTH." karakter!");
	if(PASS_NUMBER > 0 && !preg_match("/^[0-9]*$/", $p)) return eee("A jelszónak számokat is kell tartalmaznia!");
	if(PASS_UDCASE > 0 && !(preg_match("/^[A-Z]*$/", $p)&&preg_match("/^[a-z]*$/", $p))) return eee("A jelszónak kis- és nagybetűket is tartalmaznia kell!");
}

// checkEmail majd kell

function szamlista_browser($elemszam,$limit,$act,$workfunc){
	$db=ceil($elemszam/$limit);
	$max=$act*$limit;
	$min=$max-$limit;
	//$bele="<div>";
	
	
	$bele.="<div style=\"clear:both;height:1px;\"></div>";
	$bele.="<div class=\"fg-toolbar ui-toolbar ui-widget-header  ui-helper-clearfix\" >";
	
	//$bele.="<div id=\"example_info\" class=\"dataTables_info\">".($min+1)." -től ".$max." -ig a ".$elemszam." -ból</div>";
	$bele.="<div id=\"example_paginate\" class=\"dataTables_paginate fg-buttonset ui-buttonset fg-buttonset-multi ui-buttonset-multi paging_full_numbers\" style=\"float:left;text-align:left;\" >";

	$pre="";
	$egysor=4;
	$szelet=ceil($act/$egysor);
	
	$dis="";if($act==0 || $act=="1"){$dis=" ui-state-disabled ";}
	$bele.="<span id=\"example_first\" class=\"first ui-corner-tl ui-corner-bl fg-button ui-button ui-state-default ".$dis." \" onclick=\"javascript:".$workfunc."(1);\"> << </span>";
	$bele.="<span id=\"example_previous\" class=\"previous fg-button ui-button ui-state-default ".$dis."\" onclick=\"javascript:".$workfunc."(".($act-1).");\"> < </span>";
		
	
	
	if ($szelet>1){
		$bele.="<span class=\"fg-button ui-button ui-state-default ".$dis."\" onclick=\"javascript:".$workfunc."(".(($szelet-2)*$egysor+1).");\">...</span>";
	}
	$bele.= "&nbsp;";
	$bele.="<span>";
	for ($i=($szelet-1)*$egysor+1;($i<=$db)and($i<$szelet*$egysor+1);$i++){
		$dis="";if($act==$i ){$dis=" ui-state-disabled ";}
		$bele.="<span class=\"fg-button ui-button ui-state-default ".$dis."\" onclick=\"javascript:".$workfunc."(".$i.");\">".$i."</span>";
		
	}
	$bele.="</span>";
	$bele.= "&nbsp;";
	if ($szelet<ceil($db/$egysor)){

		$bele.="<span class=\"fg-button ui-button ui-state-default ".$dis."\" onclick=\"javascript:".$workfunc."(".($szelet*$egysor+1).");\">...</span>";
	}
//$bele.=$act."==".$db;
	$dis="";if($act==$db ){$dis=" ui-state-disabled ";}
	$bele.="<span id=\"example_next\" class=\"next fg-button ui-button ui-state-default ".$dis."\" onclick=\"javascript:".$workfunc."(".($act+1).");\"> > </span>";
	$bele.="<span id=\"example_last\" class=\"last ui-corner-tr ui-corner-br fg-button ui-button ui-state-default ".$dis."\" onclick=\"javascript:".$workfunc."(".$db.");\"> >> </span>";

	$bele.="</div>";
	//$bele.="</div>";
	
	return $bele;
}

function uploadplace($modul,$tipus,$user,$valamiid,$boxfelirat){
	
	$bele="";
	$bele.="<div>";
	$bele.="<h3>Csatolt állományok</h3>";
	$bele.="<div id=\"filecommander\"></div>";
	$bele.="<div id=\"filelists\"></div>";
		
		$bele.="<button class=\"btn btn-sm btn-primary\" onclick=\"javascript:getupload('".$modul."','".$tipus."','".$user."','".$valamiid."','".$boxfelirat."');\"> Feltöltés</button>";
	$bele.="</div>";
	return $bele;
}

function uploadplace_new($modul,$tipus,$user,$valamiid,$boxfelirat){
	$_SESSION['planetsys']['modulname']=$modul;
	$_SESSION['planetsys']['tipus']=$tipus;
	$_SESSION['planetsys']['modul']=$modul;
	$_SESSION['planetsys']['valamiid']=$valamiid;
	echo"
		<div class=\"col-lg-6\">
			<div class=\"block\">
				<div class=\"block-title\">
					<h2><i class=\"fa fa-picture-o\"></i> <strong>Fájl </strong> feltöltés</h2>
				</div>
				<div class=\"block-section\" >
					<form action=\"/u.php\" class=\"dropzone\" id=\"myDropzone\"></form>
				</div>
			</div>
		</div>
		
		";
}

function commentplace($modul,$tipus,$user,$valamiid,$boxfelirat){
	$bele="";
	$bele.="<div>";
	$bele.="<h3>Hozzászólások</h3>";
		$bele.="<div id=\"commentcommander\"></div>";
		$bele.="<div id=\"commentlists\"></div>";
		$bele.="<button class=\"btn btn-sm btn-primary\" onclick=\"javascript:getcomment('".$modul."','".$tipus."','".$user."','".$valamiid."','".$boxfelirat."');\"> Hozzászólás felvétéle</button>";
	$bele.="</div>";
	return $bele;
}
/* /User and GUI-related functions */


function resizeImage($filename, $max_width, $max_height,$size,$crop="",$csize=""){
	if($filename==""){return;}
	if(is_file('thumb/thumbnail.inc.php')){include_once('thumb/thumbnail.inc.php');}
	else{include_once('thumbnail.inc.php');}
	$gettype=substr($filename,-3);
	if($gettype=="jpg" || $gettype=="JPG"){$fileveg=".jpg";}
	if($gettype=="gif" || $gettype=="GIF"){$fileveg=".gif";}
	if($gettype=="png" || $gettype=="PNG"){$fileveg=".png";}
		if(file_exists($filename)){
			$filename2=substr($filename,0,-4)."_thumb_".$size.$fileveg;
			if (!file_exists($filename2)){
				$thumb = new Thumbnail($filename);
				$thumb->resize($max_width,$max_height);
				if($crop=="1"){$thumb->cropFromCenter($csize);}
				$thumb->save($filename2,100);
			}
		}
		else{
			$twofile="../".$filename;
			if(file_exists($twofile)){
				$filename2=substr($twofile,0,-4)."_thumb_".$size.$fileveg;
				if (!file_exists($filename2)){
					$thumb = new Thumbnail($twofile);
					$thumb->resize($max_width,$max_height);
					if($crop=="1"){	$thumb->cropFromCenter($csize);}
					$thumb->save($filename2,100);
				}
				$filename2=substr($filename2,3);
			}
			else{$filename2="format/intra.jpg";}
		}
    return($filename2);
}

function format_bytes($a_bytes)
{
    if ($a_bytes < 1024) {
        return $a_bytes .' B';
    } elseif ($a_bytes < 1048576) {
        return round($a_bytes / 1024, 2) .' KiB';
    } elseif ($a_bytes < 1073741824) {
        return round($a_bytes / 1048576, 2) . ' MiB';
    } elseif ($a_bytes < 1099511627776) {
        return round($a_bytes / 1073741824, 2) . ' GiB';
    } elseif ($a_bytes < 1125899906842624) {
        return round($a_bytes / 1099511627776, 2) .' TiB';
    } elseif ($a_bytes < 1152921504606846976) {
        return round($a_bytes / 1125899906842624, 2) .' PiB';
    } elseif ($a_bytes < 1180591620717411303424) {
        return round($a_bytes / 1152921504606846976, 2) .' EiB';
    } elseif ($a_bytes < 1208925819614629174706176) {
        return round($a_bytes / 1180591620717411303424, 2) .' ZiB';
    } else {
        return round($a_bytes / 1208925819614629174706176, 2) .' YiB';
    }
}
function cutgraphname($filename,$size){
	if($size!="orig"){
		$gettype=substr($filename,-3);
		if($gettype=="jpg" || $gettype=="JPG"){
			$fileveg=".jpg";
		}
		if($gettype=="gif" || $gettype=="GIF"){
			$fileveg=".gif";
		}
		if($gettype=="png" || $gettype=="PNG"){
			$fileveg=".png";
		}
		$filename2=substr($filename,0,-4)."_thumb_".$size.$fileveg;
	}
	else{
		$filename2=$filename;
	}
	return $filename2;
}

function toolbar_button($name,$img,$action,$module=""){
  global $sys_skin;
  $gy="<div class='toolbar_button' onClick=\"".$action."\" ><img src='";
  if($module!=""){ $gy.="/modules/".$module; }
  $gy.="/skin/".$sys_skin."/".$img."' border='0' width='16' height='16'><br />".eee($name)."</div>";
  return $gy;
}

function getlistoptions($screen){
	
	$screenlist="";
	$screendb="";
	$screencmd="";
	
	foreach($screen['elements'] as $record){
		if($record['deflist']==1){
			$screenlist.=$record['name'].",";
		}	
		if($record['scrdb']==1){
			$screendb.=$record['name'].",";
		}	
		if($record['scrcmd']==1){
			$screencmd.=$record['name'].",";
		}

	}

	$listoptions=db_row("select * from listoptions where user_id='".$_SESSION['planetsys']['user_id']."' and modulname='".$screen['modulname']."' and funcname='".$screen['funcname']."'");
	if($listoptions['id']==""){
		db_execute("insert into listoptions (user_id,modulname,funcname,screenlist,width,pagelimit,owner,status) values 
		('".$_SESSION['planetsys']['user_id']."','".$screen['modulname']."','".$screen['funcname']."','id,".$screenlist."','".$screen['mywidth']."','".$screen['mylimit']."','".$_SESSION['planetsys']['user_id']."','1')");
		
		$listoptions=db_row("select * from listoptions where user_id='".$_SESSION['planetsys']['user_id']."' and modulname='".$screen['modulname']."' and funcname='".$screen['funcname']."'");
	
	}
	$_SESSION['planetsys']['listoption']=$listoptions;
	$_SESSION['planetsys']['screendb']=$screendb;
	$_SESSION['planetsys']['screencmd']=$screencmd;
	$_SESSION['planetsys']['screen']=$screen;

}

function getscreenelems($modulname,$funcname){
    $screen=array();
	
		$screenelem=db_all("SELECT elem,param,value FROM ".$tableprefix."screenelem WHERE funcname = '".$funcname."' AND modulname = '".$modulname."' ORDER BY id");
		foreach($screenelem as $s){
		if($s['elem']!=''){
			if($s['param']=='collapsed'){
				foreach(explode("|",$s['value']) as $x){
				list($q,$w)=explode(":",$x);
				$screen['elements'][$s['elem']][$q]=$w;
				}
			}else{
				$screen['elements'][$s['elem']][$s['param']]=$s['value'];
			}
		}else{
			if($s['param']=='scrtable'){$s['value']=$tableprefix.$s['value'];}
			$screen[$s['param']]=$s['value'];
		}
		}
	 
    return $screen;
    //print_r($screen);
    
    
}

function widgetmenu($felirat,$kep,$url,$bgcolor="F8E096"){
	return "<div class='a-widget'>
		<div class='widget-header'>$felirat</div>
		<div class='widget-content' style='background-color:#".$bgcolor."'><a href='$url'><img src='$kep'><br />$felirat</a></div>
	</div>";
}



function showleftmenu($funcs){
	$subfunc=db_all("SELECT * FROM ".$tableprefix."functions WHERE parent!='0' and status != '0'  AND parent = (SELECT id FROM ".$tableprefix."functions WHERE funcname = '".$funcs."') ORDER BY menuorder,name");
	$myfunc=db_row("select * from functions where funcname='".$funcs."'");
	$myparent=db_row("select * from functions where id='".$myfunc['parent']."'");
	$mybigparent=db_row("select * from functions where id='".$myparent['parent']."'");
	//echo"<div><a href=\"/".$myparent['menuname']."/".$myfunc['menuname']."\">".$myfunc['name']."</a></div>";
	//echo"<div class=\"firstleftopt\"><a href=\"/".$mybigparent['menuname']."/".$myparent['menuname']."\">".$myfunc['name']."</a></div>";
	if($mybigparent['menuname']!=""){
		//echo"<div style=\"text-align:center;margin-top:6px;\"><a href=\"/".$mybigparent['menuname']."/".$myparent['menuname']."\">".$myparent['name']."</a></div>";
		echo"<div class=\"ui-widget-header my-widget-header\" style=\"padding:5px;\"><a href=\"/".$mybigparent['menuname']."/".$myparent['menuname']."\">".$myparent['name']."</a></div>";
	}
	else{
		
	}
	
	
//	echo"<div class=\"firstleftopt\" style=\"color:fff;background-color:#fff;line-height:25px;border:1px solid #ccc;width:198px;\"><a href=\"/".$myparent['menuname']."/".$myfunc['menuname']."\">".$myfunc['name']."</a></div>";
echo"<div class=\"ui-widget-header my-widget-header\" style=\"padding:5px;\">Gyorsfunkciók - <a href=\"/".$myparent['menuname']."/".$myfunc['menuname']."\">".$myfunc['name']."</a></div>";
echo "<select >";
		
		
		for($i=0;$i<count($subfunc);$i++){
			$pname=db_one("select funcname from functions where id='".$subfunc[$i]['parent']."'");
			if($subfunc[$i]['path']=='ajax'){
				echo"<li style=\"margin-left:20px;\"><a href=\"javascript:/**/\" onClick=\"".$subfunc[$i]['menuname']."\">".$subfunc[$i]['name']."</a>";
			}
			else{
				//echo"<li style=\"margin-left:20px;\"><a href=\"/moduls/".$pname."/".$subfunc[$i]['funcname']."\">".$subfunc[$i]['name']."</a>";
				echo"<option onclick=\"document.location='/moduls/".$pname."/".$subfunc[$i]['funcname']."'\">".$subfunc[$i]['name']."</option>";
			}
			//getsubmenu($subfunc[$i]['id']);
			echo"</li>";
		}
	echo"</select>";
	echo"<div style=\"clear:both;height:10px;\"></div>";
          //  if(db_rownumber($subfunctions)==0){
		//		$myparent=db_one("SELECT parent FROM ".$tableprefix."functions WHERE funcname = '".$funcs."' AND parent != 0");
		//		$subfunctions=db_execute("SELECT * FROM ".$tableprefix."functions WHERE parent!='0' and status != '0' AND parent = (SELECT id FROM ".$tableprefix."functions WHERE id = '".$myparent."') ORDER BY menuorder,name");
		//	}
}

function getsubmenu($subid){
	$subfunc=db_all("SELECT * FROM ".$tableprefix."functions WHERE parent!='0' and status != '0'  AND parent = '".$subid."' ORDER BY menuorder,name");
			echo "<ul >";
		for($i=0;$i<count($subfunc);$i++){
				$pname=db_one("select funcname from functions where id='".$subfunc[$i]['parent']."'");
			if($subfunc[$i]['path']=='ajax'){
				echo"<li><a href=\"javascript:/**/\" onClick=\"".$subfunc[$i]['menuname']."\">".$subfunc[$i]['name']."</a>";
			}
			else{
				echo"<li><a href=\"/".$pname."/".$subfunc[$i]['menuname']."\">".$subfunc[$i]['name']."</a>";
			}
			getsubmenu($subfunc[$i]['id']);
			}
		echo"</ul>";
}

function parsemodules($tdir='modules'){
	$dirs = scandir($tdir);
	foreach($dirs as $file){
		if (($file == '.')||($file == '..')){}
		elseif (is_dir($tdir.'/'.$file)){
		   parsemodules($tdir.'/'.$file);
		}
		else{
			if($file=="modul.inc.php"){
				include($tdir.'/'.$file);
				if(in_array($modul['code'],$_SESSION['planetsys']['modullist'])){}
				else{
					$_SESSION['planetsys']['modullist'][]=$modul['codeversion'];
					insertmodul($modul);
				}
			}
		}
	}
}

function insertmodul($modul){
	$checkmodul=db_row("select * from modules where modulname='".$modul['code']."' and status='1'");
	if($checkmodul['version']!=$modul['version']){
		db_execute("update modules set status='0' where modulename='".$modul['code']."'");
	}	
}
function idchange($tablename,$mezo,$defval){
	$Fields=db_row("SELECT $mezo FROM $tablename WHERE id = '$defval'");
	$rex=$Fields[$mezo];
	return $rex;
}
function checkFormJ($fieldarray){
	
}
function genFormJ($formname,$fieldarray,$refvar,$cmd){
	 $ro=0;
    $vb=0;
    $fs=0;
    $nofc=0;
    $nosize=0;
    $rules=array();

    $gy .= "<div><form id='".$formname."' name='".$formname."' action='' method='POST' enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">"; // automatikus mkhbox

    $nextField = strtok($cmd, ",");
    while ($nextField !== false) {
	 
		$F = $nextField;
		$nextField = strtok(",");

		if($F=='SB'){
			$gy.="<input type='submit' name='Rendben' value='Rendben'";
			if($ro)$gy.=" disabled='disabled'";
			$gy.=" >";
		}

		elseif($F=='-'){$gy.="<br />\n";}
		elseif($F=='RO'){$ro=1;} 		// From this point readonly
		elseif($F=='RW'){$ro=0;} 		// Till this point readonly
		elseif($F=='NOFC'){$nofc = 1;} 		// No close form!
		elseif($F=='NOSIZE'){$nosize = 1;} 	// No size parameter for input fields
		elseif(substr($F,0,3)=='FSS'){ 		// Fieldset start
			if($fs==1)$gy.="</fieldset>";$fs=1;
			$gy.="<fieldset><legend>".substr($F,4)."</legend>";
		}
		elseif($F=='FSE'){ // Fieldset end
			if($fs==1)$gy.="</fieldset>";$fs=0;
		}
		elseif(isset($fieldarray['elements'][$F])&&$fieldarray['elements'][$F]!='' && $fieldarray['elements'][$F]['scrdb']!=0){

			$E=$fieldarray['elements'][$F];

			$mytype  = $E['type'];
# Validation rules
			$rules[$F]['type']=$mytype;
			if($E['required']==1){$kotelezo=1;$rules[$F]['req_text']=eee('A kitöltés kötelező!');}else{$kotelezo=0;}
			if($E['help']!=''){$rules[$F]['help']=$E['help'];}
			if($E['promptvalid']!=''){$rules[$F]['promptvalid']=$E['promptvalid'];} // A mezobol kilepeskor azonnal ellenorzunk
			if($E['custom_format']!=''){$rules[$F]['format']=$E['custom_format'];$rules[$F]['format_text']='Nem megfelelő formátum: '.$E['custom_format']."!";}

			if($E['min']!=''){
				if($mytype == 'date' || $mytype == 'datetime'){
					$rules[$F]['min']=date("Y-m-d",strtotime($E['min']));
					$rules[$F]['min_text']=$E['textname']." ".eee("nem lehet régebbi, mint")." ".$rules[$F]['min']."!";
				}else{
					$rules[$F]['min']=$E['min'];
				}
				if($E['min_text']!=''){
					$rules[$F]['min_text']=$E['min_text'];
				}else{
					if(isset($fieldarray[$E['min']])){
						$rules[$F]['min_text']=$E['textname']." ".($E['type']=='text'?eee("hossza "):"").eee("nem lehet kisebb, mint")." ".$fieldarray[$E['min']]['textname']."!";
					}else{
						$rules[$F]['min_text']=$E['textname']." ".($E['type']=='text'?eee("hossza "):"").eee("nem lehet kisebb, mint")." ".$E['min']."!";
					}
				}
			}

			if($E['max']!=''){
				if($mytype == 'date' || $mytype == 'datetime'){
					$rules[$F]['max']=date("Y-m-d",strtotime($E['max']));
				}else{
					$rules[$F]['max']=$E['max'];
				}
				if($E['max_text']!=''){
					$rules[$F]['max_text']=$E['max_text'];
				}else{
					if(isset($fieldarray[$E['max']])){
						$rules[$F]['max_text']=$E['textname']." ".($E['type']=='text'?eee("hossza "):"").eee("nem lehet nagyobb, mint")." ".$fieldarray[$E['max']]['textname']."!";
					}else{
						$rules[$F]['max_text']=$E['textname']." ".($E['type']=='text'?eee("hossza "):"").eee("nem lehet nagyobb, mint")." ".$E['max']."!";
					}
				}
			}

			if($E['equal']!=''){
				$rules[$F]['equal']=$E['equal'];
				if($E['equal_text']!=''){
					$rules[$F]['equal_text']=$E['equal_text'];
				}else{
					if(isset($fieldarray[$E['equal']])){
						$rules[$F]['equal_text']=$E['textname']." ".($E['type']=='text'?eee("hosszának "):"").eee("meg kell egyezzen ")." ".$fieldarray[$E['equal']]['textname'].eee(" értékével")."!";
					}else{
						$rules[$F]['equal_text']=$E['textname']." ".($E['type']=='text'?eee("hosszának "):"").eee("meg kell egyeznie ezzel: ")." ".$E['equal']."!";
					}
				}
			}

#			if($E['with']!=''){$rules[$F]['with']=$E['with'];}



	//		$gy.="<div class='ff";if($nextField=='|'){$gy.=" vbox";}else{$gy.=" hbox";}$gy.="'>";
# Label
			//if($E['textname']!=""){$gy.="<label for='$F'".($kotelezo ? " class='field-req col-md-3 control-label'" : " 'col-md-3 control-label' ").">".$E['textname']."</label>";}
			//$gy.="<div class=\"col-md-9\">";

# Type		
		

			if(preg_match("/^text[0-9]*$/i",$mytype)){ // textXX (e.g. text20) is depreciated, use "max"
				
				$gy.="<div class=\"form-group\">
                        <label class=\"col-md-3 control-label\" for=\"".$E['name']."\">".$E['textname']."</label>
                        <div class=\"col-md-9\">
                            <input type=\"text\" id=\"".$E['name']."\" name=\"".$E['name']."\" class=\"form-control\" placeholder=\"Text\" value=\"".$refvar[$F]."\" />
                            <span class=\"help-block\">".$E['helptext']."</span>
                        </div>
                    </div>";


			}
			elseif($mytype=="date"){ 
				
				$gy.="<div class=\"form-group\">
                        <label class=\"col-md-3 control-label\" for=\"".$E['name']."\">".$E['textname']."</label>
                        <div class=\"col-md-9\">
                            <input type=\"text\" id=\"".$E['name']."\" name=\"".$E['name']."\" class=\"form-control input-datepicker\" data-date-format=\"yy-mm-dd\" placeholder=\"yy-mm-dd\" value=\"".$refvar[$F]."\" />
                            <span class=\"help-block\">".$E['helptext']."</span>
                        </div>
                    </div>";


			}
			elseif(preg_match("/^int[0-9]{0,2}$/i",$mytype)){
				$gy.="<div class=\"form-group\">
                        <label class=\"col-md-3 control-label\" for=\"".$E['name']."\">".$E['textname']."</label>
                        <div class=\"col-md-9\">
                            <input type=\"text\" id=\"".$E['name']."\" name=\"".$E['name']."\" class=\"form-control\" placeholder=\"Text\" value=\"".$refvar[$F]."\" />
                            <span class=\"help-block\">".$E['helptext']."</span>
                        </div>
                    </div>";
			}

			elseif(preg_match("/^password[0-9]{0,2}$/i",$mytype)){
				$gy.="<div class=\"form-group\">
                        <label class=\"col-md-3 control-label\" for=\"".$E['name']."\">".$E['textname']."</label>
                        <div class=\"col-md-9\">
                            <input type=\"password\" id=\"".$E['name']."\" name=\"".$E['name']."\" class=\"form-control\" placeholder=\"Text\" value=\"\" />
                            <span class=\"help-block\">".$E['helptext']."</span>
                        </div>
                    </div>";
			}

			elseif(preg_match("/^area([0-9]{0,2})$/",$mytype,$a)){
				if(!is_numeric($a[1]))$a[1]=MAXINPUTWIDTH;
				if($params==""&&$a[1]!=""){
					$a[1]*=12;
					$h=$a[1]*0.5;
					$params=" style='width:100%;height:".$h."px'";
				}
				if($nosize) $params='';
				$gy.="<div class=\"form-group\"> <textarea  name='$F' id='$F' $params  class=\"form-control\" placeholder=\"Content..\"";
				if($ro)$gy.=" disabled='disabled'";
				$gy.=">".$refvar[$F]."</textarea><span class=\"help-block\">".$E['helptext']."</span>
                        </div>
                    </div>";
			}

			if(preg_match("/^time([0-9]{0,2})$/i",$mytype,$a)){
				if(!is_numeric($a[1])){$a[1]=15;}
				$gy.="<select name='$F' class='ui-corner-all' id='$F'";
				if($ro){$gy.=" disabled='disabled'";}
				if($rules['error']!=''){$gy.=" title='".$rules['error']."'";}
				if($rules['help']!=''){$gy.=" alt='".$rules['help']."'";}
				$gy.=">\n";
				if($refvar[$F]==''){
					if($fieldarray['elements'][$F]['defval']!=''){
						$gy.="\n   <option value='0' selected>".$fieldarray['elements'][$F]['defval']."</option>\n";
					}else{
						$gy.="\n   <option value='0' selected>Kérem válasszon</option>\n";
					}
				}
				$o=0;$p=0;
				while($o<24){
					$gy.="<option value='".sprintf('%1$02d:%2$02d:00',$o,$p)."' ";
					if($refvar[$F]==sprintf('%1$02d:%2$02d:00',$o,$p)){$gy.="selected";}
					$gy.=">".sprintf('%1$02d:%2$02d',$o,$p)."</option>";
					$p+=$a[1];
					if($p>=60){$p-=60;$o++;}
				}
				$gy.="</select>";
			}

			elseif($mytype=='selectsql'){
				$gy.="<div class=\"form-group\">
                        <label class=\"col-md-3 control-label\" for=\"".$E['name']."\">".$E['textname']."</label>
                         <div class=\"col-md-9\">
                        <select name='$F' class=\"form-control\" id='$F'";
				if($ro){$gy.=" disabled='disabled'";}
				if($rules['error']!=''){$gy.=" title='".$rules['error']."'";}
				if($rules['help']!=''){$gy.=" alt='".$rules['help']."'";}
				$gy.=">\n";
				$myertek=db_all($fieldarray['elements'][$F]['type_param']);
				if($refvar[$F]==''){
					if($fieldarray['elements'][$F]['defval']!=''){
						$gy.="\n   <option value='0' selected>".$fieldarray['elements'][$F]['defval']."</option>\n";
					}else{
						$gy.="\n   <option value='0' selected>Kérem válasszon</option>\n";
					}
				}
				foreach($myertek as $key => $value){
					if(is_array($value)){
						$subkey="";
						$subval="";
						$i=0;
						foreach($value as $element){
							if($i==0){$subkey=$element;}else{$subval.=$element;}
							$i++;
						}
						$gy.="\n   <option value='".$subkey."'";
						if($subkey == $refvar[$F]){$gy.=" selected";}
						if($subkey == $rules['defval']){$gy.=" selected";}
						if($subkey==$fieldarray['elements'][$F]['defval']){$gy.=" selected";}
						$gy.=">".$subval."</option>\n";
					}
					else{
						$gy.="<option value='$key'";
						if(($key==$refvar[$F])||($key==$fieldarray['elements'][$F]['defval']))$gy.=" selected";
						elseif($mylist==1 && $value === $refvar[$F])$gy.=" selected";
						$gy.=">$value</option>\n";
					}
				}
				$gy.="</select></div><span class=\"help-block\">".$E['helptext']."</span></div>";
			} // select
			elseif($mytype=='selectlist'){
				$gy.="<select name='$F' id='$F'";
				if($ro){$gy.=" disabled='disabled'";}
				if($rules['error']!=''){$gy.=" title='".$rules['error']."'";}
				if($rules['help']!=''){$gy.=" alt='".$rules['help']."'";}
				$gy.=">\n";

				if($rules['sql']!=''){
					$myertek=db_all($fieldarray['elements'][$F]['type_param']);
					if($rules['list']!=''){
						list($firstparam)=explode("|",$rules['list'],1);
						array_unshift($myertek,array("id" => 0, "name" => $firstparam));
					}
				}
				else{
					$myertek=explode(",",$rules['list']);
				}

				foreach($myertek as $key => $value){
					if(is_array($value)){
						$subkey="";
						$subval="";
						$i=0;
						foreach($value as $element){
							if($i==0){$subkey=$element;}else{$subval.=$element;}
							$i++;
						}
						$gy.="\n   <option value='".$subkey."'";
						if($subkey == $refvar[$F]){$gy.=" selected";}
						if($subkey == $rules['defval']){$gy.=" selected";}
						$gy.=">".$subval."</option>\n";
					}
					else{
						$gy.="<option value='$key'";
						if($key==$refvar[$F])$gy.=" selected";
						elseif($mylist==1 && $value === $refvar[$F])$gy.=" selected";
						$gy.=">$value</option>\n";
					}
				}
				$gy.="</select>";
			} // select

		elseif($mytype=='radio'){

			if($rules['sql']!=''){
				$getSql=db_execute($rules['sql']);
				$myertek=db_multirow($getSql);
			}else{
				$myertek=explode(",",$rules['list']);
			}

			foreach($myertek as $key => $value){

				if(is_array($value)){
					$subkey=""; $subval=""; $i=0;
					foreach($value as $v){
						if($i==0)$subkey=$v;
						else
							if($i==1) $subval=$v;
							else $subval.=" ".$v;
						$i++;
					}
					$gy.="<div class='ff_r'><input type='radio' align='absmiddle' name='$F' id='".$F."_".$subkey."' value='$subkey'";
					if($ro)$gy.=" disabled='disabled'";
					if($subkey == $refvar[$F])$gy.=" checked";
					$gy.=">$subval</div>";
				}else{
					$gy.="<div class='ff_r'><input type='radio' align='absmiddle' name='$F' id='".$F."_".$key."' value='$key'";
					if($ro)$gy.=" disabled='disabled'";
					if($key==$refvar[$F])$gy.=" checked";
					elseif($value===$refvar[$F]){$gy.=" checked";}
					$gy.=">$value</div>";
				}
			}
		}

		elseif($mytype=='chkbox'||$mytype=='status'){

			if($rules['sql']!=''){
				$getSql=db_execute($rules['sql']);
				$myertek=db_multirow($getSql);
			}else{
				$myertek=explode(",",$rules['list']);
			}

			foreach($myertek as $key => $value){
				if(is_array($value)){
					$subkey=""; $subval=""; $i=0;
					foreach($value as $v){
						if($i==0)$subkey=$v;
						else
							if($i==1) $subval=$v;
							else $subval.=" ".$v;
						$i++;
					}
					$gy.="<div class=\"form-group\">
					<input type='checkbox' name='".$F;
					if($mytype=='status')$gy.="' id='".$F."' value='1'";
					else $gy.="_".$subkey."' id='".$F."_".$subkey."' value='$subkey'";
					if($ro)$gy.=" disabled='disabled'";
					if(($subkey==$refvar[$F]) || (array_search($subkey,$refvar[$F]) !== false))$gy.=" checked";
					$gy.=" >$subval</div>";
				}else{
					$gy.="<div class=\"form-group\"> <label class=\"switch switch-primary\"><input type='checkbox'   name='".$F;
					
					
					if($mytype=='status'){$gy.="' id='".$F."' value='1'";}
					else{$gy.="_".$key."' id='".$F."_".$key."' value='$key'";}
					if($ro){$gy.=" disabled='disabled'";}
					if($mytype=='status'){
						if(isset($refvar[$F])){
							if($refvar[$F]==1){$gy.=" checked";}
						}
						else{
							if($fieldarray['elements'][$F]['defval']==1 ){
								$gy.=" checked";
							}
						}	
					}

					$gy.="><span></span>".$E['textname']."  NEM/IGEN</label></div>";
				}
			}
		} // if chkbox

		
		elseif($mytype=='datetime'){
			if(($refvar[$F]=='' || $refvar[$F]=='0000-00-00 00:00:00')&&$fieldarray['elements'][$F]['defval']!=''){
				$refvar[$F]=date("Y-m-d H:i:s",strtotime($fieldarray['elements'][$F]['defval']));
			}
#			$refvar[$F]=substr($refvar[$F],0,-3);
			$gy.= "<input style='height:18px;width:120px;' type='text' name='".$F."' id='".$F."' value='".$refvar[$F]."' maxlength='19' size='10'";
			if($ro)$gy.=" disabled='disabled'";
			$gy.=">\n";
			
			$gy.="<script type='text/javascript'>".'$(function(){$('."'#".$F."').datetimepicker(
			{
				dateFormat: 'yy-mm-dd',
				dayNamesMin: ['H', 'K', 'Sz', 'Cs', 'P', 'Szo', 'V'], 
				monthNames: ['Január','Február','Március','Április','Május','Június','Július','Augusztus','Szeptember','Október','November','December']
			}
			);});</script>\n";
		}
	
	

		} // if fieldarray[$F] exists

		if($nextField=="|"){
			$nextField = strtok(",");
		}
		//$gy.="</div>";

    } // End of tokenizer cycle

    $ruleJSarray="";

if($nofc) return $gy.$ruleJSarray; // Ha az a parancs hogy ne zarjuk le a formot
$gy.=" ";
                    
return $gy."</form>	</div>".$ruleJSarray;
}

function showRecord($fieldarray,$refvar,$textelem=""){
        
	$cucc=" ";
	foreach(explode(',',$_SESSION['planetsys']['screendb']) as $f){
		if($f!=""){
			$cucc.="<dt>".$fieldarray['elements'][$f]['textname'].":</dt>";
			if($fieldarray['elements'][$f]['connected']==1){
				if($textelem==""){
					$cucc.="<dd>&nbsp;".db_one("select ".$fieldarray['elements'][$f]['con_table_field1']." from  ".$fieldarray['elements'][$f]['con_table']." where ".$fieldarray['elements'][$f]['con_table_field0']."=".$refvar[$fieldarray['elements'][$f]['name']]."")."</dd>";
				}
				else{
					$cucc.="<dd>&nbsp;".db_one("select ".$fieldarray['elements'][$f]['con_table_fields'][1]." from  ".$fieldarray['elements'][$f]['con_table']." where ".$fieldarray['elements'][$f]['con_table_fields'][0]."=".$refvar[$fieldarray['elements'][$f]['name']]."")."</dd>";
				}
			}
			else{
				if(preg_match("/^area([0-9]{0,2})$/",$fieldarray['elements'][$f]['type'],$a)){
					$cucc.="<dd>&nbsp;".nl2br($refvar[$fieldarray['elements'][$f]['name']])."</dd>";
				}
				elseif($fieldarray['elements'][$f]['type']=='password'){
					$cucc.="<dd>-</dd>";
				}
				else{
					if($fieldarray['elements'][$f]['type']=="status"){
						if($refvar[$fieldarray['elements'][$f]['name']]=='1'){$refvar[$fieldarray['elements'][$f]['name']]="<dd>Igen/aktiv</dd>";}
						if($refvar[$fieldarray['elements'][$f]['name']]=='0'){$refvar[$fieldarray['elements'][$f]['name']]="<dd>Nem/inaktiv</dd>";}
						if($refvar[$fieldarray['elements'][$f]['name']]==''){$refvar[$fieldarray['elements'][$f]['name']]="<dd>Nem/inaktiv</dd>";}
					}
					if($refvar[$fieldarray['elements'][$f]['name']]!=""){
						$cucc.="<dd>".($refvar[$fieldarray['elements'][$f]['name']])."</dd>";
					}
					else{
						$cucc.="<dd>&nbsp;</dd>";
					}
					
				}
			}
			
		}
	}
	$cucc.="";
	return $cucc;
}
function showPdfRecord($fieldarray,$refvar){
	$cucc="";
	foreach(explode(',',$_SESSION['planetsys']['screendb']) as $f){
		if($f!=""){
			if($fieldarray['elements'][$f]['connected']==1){
				$cucc.=$fieldarray['elements'][$f]['textname'].": <b>".db_one("select ".$fieldarray['elements'][$f]['con_table_fileds'][1]." from  ".$fieldarray['elements'][$f]['con_table']." where ".$fieldarray['elements'][$f]['con_table_fileds'][0]."=".$refvar[$fieldarray['elements'][$f]['name']]."")."</b><br />";	
			}
			else{
				if(preg_match("/^area([0-9]{0,2})$/",$fieldarray['elements'][$f]['type'],$a)){
					$cucc.=$fieldarray['elements'][$f]['textname'].": <p>".nl2br($refvar[$fieldarray['elements'][$f]['name']])."</p>";	
				}
				else{
					if($fieldarray['elements'][$f]['type']=="status"){
						if($refvar[$fieldarray['elements'][$f]['name']]=='1'){$refvar[$fieldarray['elements'][$f]['name']]="Igen";}
						if($refvar[$fieldarray['elements'][$f]['name']]=='0'){$refvar[$fieldarray['elements'][$f]['name']]="Nem";}
					}
					if($refvar[$fieldarray['elements'][$f]['name']]!=""){
						$cucc.=$fieldarray['elements'][$f]['textname'].": <b>".nl2br($refvar[$fieldarray['elements'][$f]['name']])."</b><br />";	
					}
					
				}
			}
		}
	}
	return $cucc;
}
function ezDate($d) {
        $ts = time() - strtotime(str_replace("-","/",$d));
        
        if($ts>31536000) $val = round($ts/31536000,0).'&nbsp;éve';
        else if($ts>2419200) $val = round($ts/2419200,0).'&nbsp;hónapja';
        else if($ts>604800) $val = round($ts/604800,0).'&nbsp;hete';
        else if($ts>86400) $val = round($ts/86400,0).'&nbsp;napja';
        else if($ts>3600) $val = round($ts/3600,0).'&nbsp;órája';
        else if($ts>60) $val = round($ts/60,0).'&nbsp;perce';
        else $val = $ts.'&nbsp;mperce';
        
       # if($val>1) $val .= 's';
        return $val;
}
function ezrDate($d) {
	if(strtotime(str_replace("-","/",$d))>time()){
        $ts =strtotime(str_replace("-","/",$d)) - time() ;
		if($ts>31536000) $val = round($ts/31536000,0).'&nbsp;év múlva';
        else if($ts>2419200) $val = round($ts/2419200,0).'&nbsp;hónap múlva';
        else if($ts>604800) $val = round($ts/604800,0).'&nbsp;hét múlva';
        else if($ts>86400) $val = round($ts/86400,0).'&nbsp;nap múlva';
        else if($ts>3600) $val = round($ts/3600,0).'&nbsp;óra múlva';
        else if($ts>60) $val = round($ts/60,0).'&nbsp;perc múlva';
        else $val = $ts.'&nbsp;mperc múlva';

	}
	else{
		 $ts = time() - strtotime(str_replace("-","/",$d));
        
        if($ts>31536000) $val =' lejárt '.round($ts/31536000,0).'&nbsp;éve';
        else if($ts>2419200) $val = ' lejárt '.round($ts/2419200,0).'&nbsp;hónapja';
        else if($ts>604800) $val = ' lejárt '.round($ts/604800,0).'&nbsp;hete';
        else if($ts>86400) $val = ' lejárt '.round($ts/86400,0).'&nbsp;napja';
        else if($ts>3600) $val = ' lejárt '.round($ts/3600,0).'&nbsp;órája';
        else if($ts>60) $val = ' lejárt '.round($ts/60,0).'&nbsp;perce';
        else $val = ' lejárt '.$ts.'&nbsp;mperce';
	}
        
        
       # if($val>1) $val .= 's';
        return $val;
}
function pastorora($ts){
	if($ts>31536000) $val =' tranzakciós idő: '.round($ts/31536000,0).'&nbsp;év';
        else if($ts>2419200) $val = ' tranzakciós idő: '.round($ts/2419200,0).'&nbsp;hónap';
        else if($ts>604800) $val = ' tranzakciós idő: '.round($ts/604800,0).'&nbsp;hét';
        else if($ts>86400) $val = ' tranzakciós idő: '.round($ts/86400,0).'&nbsp;nap';
        else if($ts>3600) $val = ' tranzakciós idő: '.round($ts/3600,0).'&nbsp;óra';
        else if($ts>60) $val = ' tranzakciós idő: '.round($ts/60,0).'&nbsp;perc';
        else $val = ' tranzakciós idő: '.$ts.'&nbsp;mperc';
        return $val;
}
  function bgtoborder($mycolor){
      $red=hexdec(substr($mycolor,0,2));
      $grn=hexdec(substr($mycolor,2,2));
      $blu=hexdec(substr($mycolor,4,2));
      $red=$red-32;
      if($red<0)$red=0;
      $grn=$grn-32;
      if($grn<0)$grn=0;
      $blu=$blu-32;
      if($blu<0)$blu=0;
      $red=dechex($red);
      $grn=dechex($grn);
      $blu=dechex($blu);
      if(strlen($red)==1)$red="0".$red;
      if(strlen($grn)==1)$grn="0".$grn;
      if(strlen($blu)==1)$blu="0".$blu;
      return $red.$grn.$blu;
  }
  
function storemail($cimzett,$texttartalom,$targy){
	
	db_execute("insert into mail_quene (customer_id,fromemail,fromname,email,subject,txtcontent,sendtime,status) values ('".$_SESSION['planetsys']['customer']['id']."','".$_SESSION['planetsys']['customer']['customeremail']."','".$_SESSION['planetsys']['customer']['emailfromname']."','".$cimzett."','".$targy."','".$texttartalom."',NOW(),'1')");	
} 

function utf7_to_utf8($str){
  $Index_64 = array(
      -1,-1,-1,-1, -1,-1,-1,-1, -1,-1,-1,-1, -1,-1,-1,-1,
      -1,-1,-1,-1, -1,-1,-1,-1, -1,-1,-1,-1, -1,-1,-1,-1,
      -1,-1,-1,-1, -1,-1,-1,-1, -1,-1,-1,62, 63,-1,-1,-1,
      52,53,54,55, 56,57,58,59, 60,61,-1,-1, -1,-1,-1,-1,
      -1, 0, 1, 2,  3, 4, 5, 6,  7, 8, 9,10, 11,12,13,14,
      15,16,17,18, 19,20,21,22, 23,24,25,-1, -1,-1,-1,-1,
      -1,26,27,28, 29,30,31,32, 33,34,35,36, 37,38,39,40,
      41,42,43,44, 45,46,47,48, 49,50,51,-1, -1,-1,-1,-1
  );

  $u7len = strlen($str);
  $str = strval($str);
  $p = $err = '';

  for ($i=0; $u7len > 0; $i++, $u7len--){
    $u7 = $str[$i];
    if ($u7 == '&'){
      $i++;
      $u7len--;
      $u7 = $str[$i];
      if ($u7len && $u7 == '-'){$p .= '&';  continue;}
      $ch = 0;
      $k = 10;
      for (; $u7len > 0; $i++, $u7len--){
        $u7 = $str[$i];
        if ((ord($u7) & 0x80) || ($b = $Index_64[ord($u7)]) == -1){break;}  
        if ($k > 0){$ch |= $b << $k;$k -= 6;}
        else{
          $ch |= $b >> (-$k);
          if ($ch < 0x80){
            if(0x20 <= $ch && $ch < 0x7f){return $err; }
			$p .= chr($ch);
          }
          elseif($ch < 0x800){
            $p .= chr(0xc0 | ($ch >> 6));
            $p .= chr(0x80 | ($ch & 0x3f));
          }
          else{
            $p .= chr(0xe0 | ($ch >> 12));
            $p .= chr(0x80 | (($ch >> 6) & 0x3f));
            $p .= chr(0x80 | ($ch & 0x3f));
          }

          $ch = ($b << (16 + $k)) & 0xffff;
          $k += 10;
        }
      }
      if ($ch || $k < 6){return $err;} 
      if (!$u7len || $u7 != '-'){return $err;}
      if ($u7len > 2 && $str[$i+1] == '&' && $str[$i+2] != '-'){return $err;}
    }
    elseif(ord($u7) < 0x20 || ord($u7) >= 0x7f){return $err;}
    else{$p .= $u7;}
  }

  return $p;
}
     
function build_folder_tree(&$arrFolders, $folder, $delm='/', $path=''){
  
  $prefix = '';
  if (!$path) {
    $n_folder = $folder;
    if ($n_folder != $folder) {
      $prefix = substr($n_folder, 0, -strlen($folder));
    }
  }

  $pos = strpos($folder, $delm);

  if ($pos !== false) {
    $subFolders = substr($folder, $pos+1);
    $currentFolder = substr($folder, 0, $pos);

    if (!strlen($subFolders))
      $virtual = false;
    else if (!isset($arrFolders[$currentFolder]))
      $virtual = true;
    else
      $virtual = $arrFolders[$currentFolder]['virtual'];
  }
  else {
    $subFolders = false;
    $currentFolder = $folder;
    $virtual = false;
  }

  $path .= $prefix.$currentFolder;

  if (!isset($arrFolders[$currentFolder])) {
    $arrFolders[$currentFolder] = array(
      'id' => $path,
      'name' => utf7_to_utf8($currentFolder),
      'virtual' => $virtual,
      'folders' => array());
  }
  else
    $arrFolders[$currentFolder]['virtual'] = $virtual;

  if (strlen($subFolders))
    build_folder_tree($arrFolders[$currentFolder]['folders'], $subFolders, $delm, $path.$delm);
}
function showfunctions(){
	$cucc=db_one("select quickfunctions from users where id='".$_SESSION['planetsys']['user_id']."'");
	$myarr=explode(",",$cucc);
	for($i=0;$i<count($myarr);$i++){
		$tfunc=db_row("select * from functions where id='".$myarr[$i]."'");
		if($tfunc['name']!=""){
			echo"<div class=\"tbbutton\" style=\"float:right;\" onclick=\"document.location='/moduls/".$tfunc['menuname']."'\">";
			echo"<div class=\"ui-icon ui-icon-gear picon\" ></div>";
			echo"".$tfunc['name']."";
			echo "</div>";
		}
	}
}
/*
function getprintscreen(modulname,tipus,user,valamiid,boxfelirat){
		$.ajax({
		type:'POST',
		url: '/inc/ajax_getprintscreen.php',
		data:'modulname='+modulname+'&tipus='+tipus+'&user='+user+'&valamiid='+valamiid+'&boxfelirat='+boxfelirat,
		success: function(data){	
			
			$("#infoscreen" ).dialog({title:boxfelirat,height: 480,width: 640,buttons: {
				"PDF-export": function() {exportcontent(modulname,tipus,valamiid);},
				"Nyomtatás": function() {printdata('printdiv');},
				"Bezár": function() {$( this ).dialog( "close" );}},	close: function() {}});
			$("#infoscreen").html(data);
			$("#infoscreen").dialog("open");
		}});
}
*/

function dot($inn){
	return str_replace(",",".",$inn);
}

function getkilo($termek_id){
	$elemrow=db_row("select * from pt_units where pt_id='".$termek_id."' and  unit2_id='1' ");
	if($elemrow['id']!=""){
		$kilo=$elemrow['mesure2'];
	}
	else{
		$elemrow=db_row("select * from pt_units where pt_id='".$termek_id."' and unit1_id='1'  ");
		if($elemrow['id']!=""){
			$kilo=$elemrow['mesure1'];
		}
		else{
				//ide kell valami ami visszaszármaztat, mert ezek szerint nincs alapból kilóra vetitett dolga
		}
	}
	return $kilo;
}

function recterm($termek_id,$termek_kilo,$rendeles_id,$darab){
	
	//echo "új ciklus: ".idchange("product_types","name",$termek_id)."--".$termek_kilo." Kg<br />";
	
		$pt=db_row("select * from product_types where id='".$termek_id."'");
		$pt_agt=db_all("select * from pt_x_agt where pt_id='".$pt['id']."' and operation='elem'");	
		$szorzo=$pt['total_weight']/$termek_kilo;
		
		
		for($k=0;$k<count($pt_agt);$k++){
			db_execute("insert into orders_ag (order_id,pt_id,post_id,agt_id,pt_me,agt_me,agt_sume) 
			values ('".$rendeles_id."','".$pt['id']."','".$pt_agt[$k]['post_id']."','".$pt_agt[$k]['agt_id']."'
			,'".$termek_kilo."','".$pt_agt[$k]['amount']."','".(($pt_agt[$k]['amount']/$szorzo)*$darab)."')");
		}
		
		$pt_pt=db_all("select * from pt_x_pt where pt1_id='".$pt['id']."' and operation='elem'");	
		
		if(count($pt_pt)>0){
			for($k=0;$k<count($pt_pt);$k++){
		//		echo "van benne:".idchange("product_types","name",$pt_pt[$k]['pt2_id'])." ".$pt_pt[$k]['amount']/$szorzo."Kg<br />";
				db_execute("insert into orders_pt (order_id,pt_id,post_id,pt2_id,pt_me,pt2_me,pt2_sume) 
				values ('".$rendeles_id."','".$pt['id']."','".$pt_pt[$k]['post_id']."','".$pt_pt[$k]['pt2_id']."','".$pt_pt[$k]['amount']."','".$pt_pt[$k]['amount']."','".($pt_pt[$k]['amount']/$szorzo*$darab)."')");
				recterm($pt_pt[$k]['pt2_id'],$pt_pt[$k]['amount']/$szorzo,$rendeles_id,$darab);
			}
		}
}

function getkilo2($termek_id){
	$elemrow=db_row("select * from pt_units where pt_id='".$termek_id."' and units_type='2' and unit2_id=1 ");
	
	$kilo=$elemrow['mesure2'];
	return $kilo;
}

function recterm2($termek_id,$termek_kilo,$rendeles_id,$darab,$szalldate){
	$pt=db_row("select * from product_types where id='".$termek_id."'");
	$pt_agt=db_all("select * from pt_x_agt where pt_id='".$pt['id']."' and operation='elem'");	
	$szorzo=$pt['total_weight']/$termek_kilo;
	for($k=0;$k<count($pt_agt);$k++){
		db_execute("insert into orders_ag (order_id,pt_id,post_id,agt_id,pt_me,agt_me,agt_sume) values ('".$rendeles_id."','".$pt['id']."','".$pt_agt[$k]['post_id']."','".$pt_agt[$k]['agt_id']."','".$termek_kilo."','".($pt_agt[$k]['amount']/$szorzo)."','".(($pt_agt[$k]['amount']/$szorzo)*$darab)."','".$szalldate."')");
	}
	$pt_pt=db_all("select * from pt_x_pt where pt1_id='".$pt['id']."' and operation='elem'");	
	if(count($pt_pt)>0){
		for($k=0;$k<count($pt_pt);$k++){
			//echo "--->van benne:".idchange("product_types","name",$pt_pt[$k]['pt2_id'])." ".$pt_pt[$k]['amount']/$szorzo."Kg<br />";
			db_execute("insert into orders_pt (	order_id,pt_id,	post_id,pt2_id,pt_me,pt2_me,pt2_sume,szall_date) values ('".$rendeles_id."','".$pt['id']."','".$pt_pt[$k]['post_id']."','".$pt_pt[$k]['pt2_id']."','".$pt_pt[$k]['amount']."',	'".($pt_pt[$k]['amount']/$szorzo)."','".($pt_pt[$k]['amount']/$szorzo*$darab)."')");
			recterm2($pt_pt[$k]['pt2_id'],$pt_pt[$k]['amount']/$szorzo,$rendeles_id,$darab,$szalldate);
		}
	}
}

function recterm3($termek_id,$termek_kilo,$parent_id,$darab){

	$pt=db_row("select * from product_types where id='".$termek_id."'");
	$pt_agt=db_all("select * from pt_x_agt where pt_id='".$pt['id']."' and operation='elem'");	
	
	//echo "select * from pt_x_agt where pt_id='".$pt['id']."' and operation='elem'\n";
	$szorzo=$pt['total_weight']/$termek_kilo;
	//echo $pt['total_weight']."/".$termek_kilo."\n";
	//echo $szorzo." szorzo\n";
	for($k=0;$k<count($pt_agt);$k++){
		$pt_agt[$k]['amount']=round($pt_agt[$k]['amount'],7);
		$ch=db_row("select * from pt_alapanyaghanyad where parent_id='".$parent_id."' and agt_id='".$pt_agt[$k]['agt_id']."' and status=1");
		if($ch['id']==""){
			db_execute("insert into pt_alapanyaghanyad (parent_id,agt_id,agt_sume) values ('".$parent_id."','".$pt_agt[$k]['agt_id']."','".(($pt_agt[$k]['amount']/$szorzo)*$darab)."')");
			//echo "insert into pt_alapanyaghanyad (parent_id,agt_id,agt_sume) values ('".$parent_id."','".$pt_agt[$k]['agt_id']."','".(($pt_agt[$k]['amount']/$szorzo)*$darab)."')\n";
		}
		else{
			$ujs=$ch['agt_sume']+(($pt_agt[$k]['amount']/$szorzo)*$darab);
			db_execute("update pt_alapanyaghanyad set agt_sume='".$ujs."' where id='".$ch['id']."'");	
			//echo"update pt_alapanyaghanyad set agt_sume='".$ujs."' where id='".$ch['id']."'\n";
		}
	}
	$pt_pt=db_all("select * from pt_x_pt where pt1_id='".$pt['id']."' and operation='elem'");	
	if(count($pt_pt)>0){
		for($k=0;$k<count($pt_pt);$k++){
				recterm3($pt_pt[$k]['pt2_id'],$pt_pt[$k]['amount']/$szorzo,$parent_id,$darab);
		}
	}
}


function gomail($cimzett,$tartalom,$targy){
	
	date_default_timezone_set('Etc/UTC');

		require_once 'PHPMailerAutoload.php';

		$mail = new PHPMailer();
		$mail->CharSet = 'UTF-8';
		$mail->IsSMTP(); // telling the class to use SMTP
		
		$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
												   // 1 = errors and messages
												   // 2 = messages only
		$mail->SMTPAuth   = true;                  // enable SMTP authentication
		$mail->SMTPSecure = "tls";                 // sets the prefix to the servier
		$mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
		$mail->Port       = 587;                   // set the SMTP port for the GMAIL server
		$mail->Username   = "jozsef.szucs@planetstar.hu";  // GMAIL username
		$mail->Password   = "NEEc4r4gu4";            // GMAIL password

		$mail->setFrom('tracking@planetstar.hu', 'Interfood-Európa Kft. - Rendszer Üzenet');

		//Set who the message is to be sent to
		$mail->addAddress($cimzett);
		//Set the subject line
		$mail->Subject = $targy;

		//Replace the plain text body with one created manually
		$mail->Body = $tartalom;
		
		

		//send the message, check for errors
		if (!$mail->send()) {
			echo "Mailer Error: " . $mail->ErrorInfo;
		} else {
			//echo "Message sent!";
		}
	}
	
function gomailhtml($cimzett,$tartalom,$htmltartalom,$targy){
	
	date_default_timezone_set('Etc/UTC');

		require_once 'PHPMailerAutoload.php';

		$mail = new PHPMailer();
		$mail->CharSet = 'UTF-8';
		$mail->IsSMTP(); // telling the class to use SMTP
		
		$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
												   // 1 = errors and messages
												   // 2 = messages only
		$mail->SMTPAuth   = true;                  // enable SMTP authentication
		$mail->SMTPSecure = "tls";                 // sets the prefix to the servier
		$mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
		$mail->Port       = 587;                   // set the SMTP port for the GMAIL server
		$mail->Username   = "jozsef.szucs@planetstar.hu";  // GMAIL username
		$mail->Password   = "NEEc4r4gu4";            // GMAIL password

		$mail->setFrom('tracking@planetstar.hu', 'Interfood-Európa Kft. - Rendszer Üzenet');
		$mail->AddReplyTo('rendeles@interfoodeuropa.hu', 'Interfood-Europa Kft  - Rendelés');
		//Set who the message is to be sent to
		$mail->addAddress($cimzett);
		//Set the subject line
		$mail->Subject = $targy;

		//Replace the plain text body with one created manually
		//$mail->Body = $tartalom;
		$mail->AltBody = $tartalom;
		$mail->MsgHTML($htmltartalom);
		

		//send the message, check for errors
		if (!$mail->send()) {
			//echo "Mailer Error: " . $mail->ErrorInfo;
		} else {
			//echo "Message sent!";
		}
	}
	
	
function generateCode($length=15) {
	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";
	$code = "";
	$clen = strlen($chars) - 1;  
	while (strlen($code) < $length) {
		$code .= $chars[mt_rand(0,$clen)];  
	}
	return $code;
}
	function huf($hufstring){
 $mylen  = strlen($hufstring);
 $myrest = ($mylen %3);
 $outhufstring="";
 if($myrest>0)
    {
	$outhufstring = substr($hufstring,0,$myrest)." ";
    }
 while($myrest<$mylen)
    {
     $outhufstring.=substr($hufstring,$myrest,3)." ";
     $myrest+=3;
    }
 return substr($outhufstring,0,strlen($outhufstring)-1);
}
function gettetel($ag_id,$store_id,$startdate="",$enddate=""){
	
		if(strtotime($startdate)>strtotime($enddate)){
			$startdate='2014-01-01 01:01:01';	
		}
		$dts1="";
		$dts2="";
		if($startdate!=$enddate){
			$dts1=" and ae.rdate between '".$startdate."' and  '".$enddate."' ";	
			$dts2=" and rdate between '".$startdate."' and  '".$enddate."' ";	
		}
	$str="";if($store_id!=""){$str=" and ae.store_id='".$store_id."' ";}
	$st2="";if($store_id!=""){$st2=" and store_id='".$store_id."' ";}
	$tetels=db_all("select ae.ag_id  as ag_id from alapag_events ae  left join alapag_tipus at on (ae.agt_id=at.id) 
		left join alapanyag a on (ae.ag_id=a.id) 
		where  a.consumed='0000-00-00 00:00:00' and ae.ag_id='".$ag_id."' ".$str." ".$dts1." group by ae.ag_id ");
				
		$darab=0;
		for($b=0;$b<count($tetels);$b++){
			$mennyi=db_one("select sum(amount) from alapag_events where   ag_id='".$tetels[$b]['ag_id']."' and (event=1 or event=3 or event=7 or event=6) ".$st2." ".$dts2." ");
			//echo "select sum(amount) from alapag_events where   ag_id='".$tetels[$b]['ag_id']."' and (event=1 or event=3 or event=7 or event=6) ".$st2."";
			$darab=$darab+$mennyi;
		}
		//die();
		return $darab;
}
function getalap($agt_id,$store_id='',$startdate='',$enddate=''){
	
		
		$str="";if($store_id!=""){$str=" and ae.store_id='".$store_id."' ";}
		
		$dts="";if($startdate!=$enddate){
			$dts=" and ae.rdate between '".$startdate."' and  '".$enddate."' ";	
		}
		
		
		$tetels=db_all("select ae.ag_id  as ag_id from alapag_events ae  left join alapag_tipus at on (ae.agt_id=at.id) 
		left join alapanyag a on (ae.ag_id=a.id) 
		where  a.consumed='0000-00-00 00:00:00' and ae.agt_id='".$agt_id."' ".$str." ".$dts." group by ae.ag_id ");

		$daraba=0;
		
		for($b=0;$b<count($tetels);$b++){
			
			$mennyi=gettetel($tetels[$b]['ag_id'],$store_id,$startdate,$enddate);
			$daraba=$daraba+$mennyi;
			
		}
		db_execute("update alapag_tipus set units='".$daraba."' where id='".$agt_id."'");
		return $daraba;
}
function xl2timestamp($xl_date){
	return mktime(0,0,0,1,$xl_date,1900)-86400;
}

function mee($termek_id){
	$ret=array();
	$g1=db_row("select * from pt_units where pt_id='".$termek_id."' and units_type='1' and unit2_id=1 "); //rendel
	$g2=db_row("select * from pt_units where pt_id='".$termek_id."' and units_type='2' and unit2_id=1 "); //gyart
	$m1=db_one("select name from mertekegyseg where id='".$g1['unit1_id']."'");	//rendelegyseg
	$m2=db_one("select name from mertekegyseg where id='".$g2['unit1_id']."'");	 //gyartegyseg
	$ret['rendeles_egyseg']=$m1;
	$ret['gyartas_egyseg']=$m2;
	$ret['rendeles_szorzo']=$g1['mesure2'];
	$ret['gyartas_szorzo']=$g2['mesure2'];
	
	return $ret;
}

function gok($in){
	if($in==1){
		$retval="OK!";
	}
	else{
		$retval="X!";
	}
	return $retval;
}

function getcurrency($cur="EUR"){
	$chck=db_one("select id from curencys where shortname='".$cur."'");
			
	$arfolyam=db_one("select arfolyam from arfolyam where cur_id='".$chck."' order by arfolyam_date desc limit 1");	
	return round($arfolyam,2);
}

function getbullshit(){
	$t1=db_one("select name from texts_bullshits where status=1 and type=1 order by RAND()");	
	$t2=db_one("select name from texts_bullshits where status=1 and type=2 order by RAND()");	
	$t3=db_one("select name from texts_bullshits where status=1 and type=3 order by RAND()");	
	$t4=db_one("select name from texts_bullshits where status=1 and type=4 order by RAND()");	
	$bs=$t1." ".$t2." ".$t3." ".$t4;
	return $bs;
}

function getrandcolor(){
	$elems=array("0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F");
	$a="#".$elems[rand(0,15)].$elems[rand(0,15)].$elems[rand(0,15)].$elems[rand(0,15)].$elems[rand(0,15)].$elems[rand(0,15)];
	return $a;			
}
function gettapadat($id,$type){
	$ret=db_one("select sum(".$type.") from pt_alapanyaghanyad where parent_id='".$id."' and status=1");
	//echo "select sum(".$type.") from pt_alapanyaghanyad where parent_id='".$id."' ";
	return round($ret,3);
}
function borderedparse($articleb){
	
	$pos = strpos($articleb, "[b1]");
	if ($pos === false) {} 
	else {
		$first="<div id='inbbbox' class='bbox-outer-50'><div id='inlins' class='bbox-grey'>";
		$articleb=str_replace("[b1]",$first,$articleb);
	}
	
	$pos = strpos($articleb, "[b2]");
	if ($pos === false) {} 
	else {
		$first="<div id='inbbbox' style='box-outer-100'><div id='inlins' class='bbox-grey'>";
		$articleb=str_replace("[b2]",$first,$articleb);
	}
	
	$pos = strpos($articleb, "[b3]");
	if ($pos === false) {} 
	else {
		$first="<div id='inbbbox' class='bbox-outer-50'><div id='inlins' class='bbox-blue'>";
		$articleb=str_replace("[b3]",$first,$articleb);
	}
	$pos = strpos($articleb, "[b4]");
	if ($pos === false) {} 
	else {
		$first="<div id='inbbbox' class='box-outer-100'><div id='inlins' class='bbox-blue'>";
		$articleb=str_replace("[b4]",$first,$articleb);
	}
	
	$pos = strpos($articleb, "[b5]");
	if ($pos === false) {} 
	else {
		$first="<div id='inbbbox' class='bbox-outer-50'><div id='inlins' class='bbox-green'>";
		$articleb=str_replace("[b5]",$first,$articleb);
	}
	$pos = strpos($articleb, "[b6]");
	if ($pos === false) {} 
	else {
		$first="<div id='inbbbox' class='box-outer-100'><div id='inlins' class='bbox-green'>";
		$articleb=str_replace("[b6]",$first,$articleb);
	}
	$pos = strpos($articleb, "[b7]");
	if ($pos === false) {} 
	else {
		$first="<div id='inbbbox' class='bbox-outer-50'><div id='inlins' class='bbox-green'>";
		$articleb=str_replace("[b7]",$first,$articleb);
	}
	$last="</div></div>";
	$articleb=str_replace("[b9]",$last,$articleb);
	return $articleb;

}
?>
