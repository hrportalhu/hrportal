<?php
define("MAXINPUTWIDTH", 50); // ez azert kell, hogy ne logjanak tulsagosan osszevissza
						   // az input mezok, de be lehessen irni ~100 karaktert is


// validateForm hasznalja konvertalashoz, 2011-02-09 13:58 formatumot eszik
function dt2rd($mydate,$mytime){
    $mydate=str_replace("/","-",str_replace(".","-",$mydate));
    list($y,$m,$d)=explode("-",$mydate);
    list($h,$mi)=explode(":",$mytime);
    if($h=="")$h=0;
    if($mi=="")$mi=0;
    return mktime($h,$mi,0,$m,$d,$y);
}

// Egy sztringbol csak a szamokat tartja meg
function numberize($mystr){
    $gy="";
    $mystr=substr($mystr,0,1024); // Ennel nagyobb szoveggel nem foglalkozunk
    for($i=0;$i<strlen($mystr);$i++)
	if(is_numeric($mystr[$i]))$gy.=$mystr[$i];
    return $gy;
}


# validateForm TESZT!!!
function checkForm($fieldarray){
		$gyujt=array();
		foreach($fieldarray as $name => $values){
			$rules = explode("|",$values);
			$mylabel = array_shift($rules); // Mindig az első helyen kell legyen
			$mytype = array_shift($rules); // Mindig a második helyen kell legyen!
			if(preg_match("/^(.*)\*$/i",$mylabel)){$kotelezo=1;$mylabel=substr($mylabel,0,-1);}else{$kotelezo=0;}
			$fieldarray[$name] = $rules;
			$fieldarray[$name]["req"]=$kotelezo;
			$fieldarray[$name]["label"]=$mylabel;
			$fieldarray[$name]["type"]=$mytype;
		}

	foreach($fieldarray as $name => $v){

	  if($v["req"]){
		if($v["type"]=="datetime"){
			if(strlen(trim($_POST[$name."_date"]))==0||strlen(trim($_POST[$name."_time"]))==0)
				return array(0 => array("type" => "error",
							 "msg" => eee($v["label"]).eee(": kitöltése kötelező!")));
		}else{
			if(strlen(trim($_POST[$name]))==0)
				return array(1=>array("type" => "error",
							 "msg" => eee($v["label"]).eee(": kitöltése kötelező!")));
		}
	  }
	  else{
		if($v["type"]=="datetime"){
			if(strlen(trim($_POST[$name."_date"]))!=0&&strlen(trim($_POST[$name."_time"]))==0)$_POST[$name."_time"]="00:00";
			if(strlen(trim($_POST[$name."_date"]))==0&&strlen(trim($_POST[$name."_time"]))!=0)$_POST[$name."_date"]=date("Y-m-d",strtotime("NOW"));
		}
		else{
			if(strlen(trim($_POST[$name]))==0)continue; // Ha nem kotelezo es ures, akkor nem kell semmit tennunk
			if(strlen(trim(trim($_POST[$name],".-+()_")))==0)continue; // Nem kotelezo maszkolt mezo (FIXME! Az osszes maszk karaktert fel kell ide sorolni)
		}
	  }

	  if(preg_match("/^text([0-9]{0,2})$/i",$v["type"],$a)){
			if(is_numeric($a[1])&&strlen($_POST[$name])>$a[1]){
				$_POST[$name]=substr($_POST[$name],0,$a[1]);
				$gyujt[]=array("type" => "warn",
							   "msg" => eee($v["label"]).eee(": A maximálisan megadható hossz ".$a[1].
							   " karakter. A szöveg le lett rövidítve a maximális hosszra.<br />"));
			}
		}
		elseif(preg_match("/^int([0-9]{0,2})$/i",$v["type"],$a)){
			if(preg_match("/^[0-9]*$/i",$_POST[$name])){
			if(is_numeric($a[1])&&strlen($_POST[$name])>$a[1]){
				$_POST[$name]=substr($_POST[$name],-$a[1]);
				$_POST[$name]=round($_POST[$name],0);
				$gyujt[]=array("type" => "error",
							   "msg" => eee($v["label"]).eee(": Maximálisan ".$a[1].
							   " db. szám adható meg. A szám le lett kerekítve a maximális hosszra.<br />"));
			}
			}else{
			  return array(2=>array("type" => "error", "msg" => eee($v["label"]).eee(": Csak számjegyek adhatók meg!")));
			}
		}
		elseif(preg_match("/^huf([0-9]{0,2})$/i",$v["type"],$a)){
		$_POST[$name]=trim($_POST[$name],",.");
			if(preg_match("/^[0-9]*$/i",$_POST[$name])){
			if(is_numeric($a[1])&&strlen($_POST[$name])>$a[1]){
				$_POST[$name]=substr($_POST[$name],-$a[1]);
				$_POST[$name]=round($_POST[$name],0);
				$gyujt[]=array("type" => "warn", "msg" => eee($v["label"]).eee(": Maximálisan ".$a[1].
				" db. szám adható meg. A szám le lett kerekítve a maximális hosszra.<br />"));
			}
			}else{
			  return array(3=>array("type" => "error", "msg" => eee($v["label"]).eee(": Csak számjegyek adhatók meg!")));
			}
		}
		elseif($v["type"]=="password"){
			if (preg_replace("/[\'\"\`\;$]/","",$_POST[$name]) != $_POST[$name])
			  return array(4=>array("type" => "error", "msg" => eee($v["label"]).
					 eee(": Nem adhatók meg a következő karakterek: Szögletes ".
					 "zárójel, dollárjel, aposztróf, backslash, idézőjel és pontosvessző!")));
		}
		
		elseif($v["type"]=="date"){
		if(!preg_match("/^[12]{1}[0-9]{3}-[0-9]{2}-[0-9]{2}$/",$_POST[$name]))
				return array(5=>array("type" => "error", "msg" => eee($v["label"]).
					   eee(": Nem megfelelő dátum formátum! (ÉÉ--HH-NN)")));

		
		$ev=substr($_POST[$name],0,4);$ho=substr($_POST[$name],5,2);$nap=substr($_POST[$name],8,2);
		
		if(checkdate($ho,$nap,$ev) === false)return array(6 => array("type" => "error",
								"msg" => eee($v["label"]).eee(": Nem megfelelő dátum érték!")));
		}
		/*
		elseif($v["type"]=="datetime"){
			if((trim($_POST[$name."_date"])!=""&&trim($_POST[$name."_time"])=="")||
			(trim($_POST[$name."_date"])==""&&trim($_POST[$name."_time"])!=""))
			return array(7 => array("type" => "error", "msg" => eee($v["label"]).eee(": a dátum és id&#x0151; is kitöltend&#x0151;!")));
		if($v["req"]==0&&strlen(trim(trim($_POST[$name],".-")))==0)continue;
		if(!preg_match("/^[12]{1}[0-9]{3}-[0-9]{2}-[0-9]{2}$/",$_POST[$name."_date"]))eturn array(8 => array("type" => "error", "msg" => eee($v["label"]).eee(": Nem megfelel&#x0151; d&#x00E1;tum form&#x00E1;tum! (&#x00C9;&#x00C9;&#x00C9;&#x00C9;-HH-NN)")));
		$ev=substr($_POST[$name."_date"],0,4);$ho=substr($_POST[$name."_date"],5,2);$nap=substr($_POST[$name."_date"],8,2);
		if(checkdate($ho,$nap,$ev) === false)return array(9 => array("type" => "error",
													 "msg" => eee($v["label"]).eee(": Nem megfelel&#x0151; d&#x00E1;tum &#x00E9;rt&#x00E9;k!")));
		$_POST[$name."_time"]=str_replace("&#58;",":",$_POST[$name."_time"]);
		if(!preg_match("/^[012]{1}[0-9]{1}:[0-5]{1}[0-9]{1}$/",$_POST[$name."_time"]))
				return array(10 => array("type" => "error", "msg" => eee($v["label"]).eee(": Nem megfelel&#x0151; id&#x0151; form&#x00E1;tum! (ÓÓ:PP)")));
			$ora=substr($_POST[$name."_time"],0,2);$perc=substr($_POST[$name."_time"],3,2);
			if(($ora>23||$ora<0)||($perc>59||$perc<0))return array(11 => array("type" => "error",
																   "msg" => eee($v["label"]).eee(": Nem megfelel&#x0151; id&#x0151; érték!")));
		}
		*/ 
	}

	// Most ujra foreach, de most csak a rule-okat nezzuk mar
	foreach($fieldarray as $name => $v){
	 foreach($v as $rn => $R){
		if(is_numeric($rn)){
		  if(strpos($R,":") !== false){
			  list($oper,$aga)=explode(":",$R);
			  if(strpos($aga,"[") !== false){
				$newname=substr($aga,1);
				$newname=substr($newname,0,-1);
				$against = $_POST[$newname];
			  }else{
			$against=$aga;
		  }
		  }else{$oper=$R;}

		  if($oper=="mask"){
				for($i=0;$i<=strlen($_POST[$name])-1;$i++){
			echo $_POST[$name][$i]." ".$against[$i]."<br>";
			if($against[$i]=="9" ){
				if(!preg_match("/^[0-9]{1}$/",$_POST[$name][$i]))
				return array(12 => array("type" => "error", "msg" => $v["label"].eee(" formátuma nem megfelelő! (".($i+1).". karakter)")));
			}
			elseif($against[$i]=="A"){
				if(!preg_match("/^[a-zA-Z\xf6\xc3\xa1\xa9\xc3\xf5\xfa\xfc\xfb\xc1\xc9\xcd\xd3\xd6\xd5\xda\xdc\xdb]{1}$/",$_POST[$name][$i]))
				return array(13 => array("type" => "error", "msg" => $v["label"].eee(" formátuma nem megfelelő! (".($i+1).". karakter)")));
			}
			elseif($against[$i]=="X"){
				if(!preg_match("/^[0-9a-zA-Z\xf6\xc3\xa1\xa9\xc3\xf5\xfa\xfc\xfb\xc1\xc9\xcd\xd3\xd6\xd5\xda\xdc\xdb]{1}$/",$_POST[$name][$i]))
				return array(14 => array("type" => "error", "msg" => $v["label"].eee(" formátuma nem megfelelő! (".($i+1).". karakter)")));
			}
			elseif($against[$i]!=$$_POST[$name][$i]){
				return array(15 => array("type" => "error", "msg" => eee($v["label"]).eee(" formátuma nem megfelelő! (".($i+1).". karakter)")));
			}
			}
		  }
		  elseif($oper=="trim"){
			$_POST[$name]=trim($_POST[$name]);
		  }
		  elseif($oper=="eq"){
			if($_POST[$name]!=$_POST[$newname]){
			  return array(16 => array("type" => "error",
						   "msg" => eee($v["label"]).eee(" és ").eee($fieldarray[$newname]["label"]).eee(" mezők értékének meg kell egyeznie!")));
			}
		  }
		  elseif($oper=="gt"){
			  if($v["type"]=="datetime"){
					if(!$fieldarray[$newname]["req"]&&trim($_POST[$newname."_date"])==""&&trim($_POST[$newname."_time"])=="")continue; // nem kotelezo, nincs kitoltve
					if(!$fieldarray[$name]["req"]&&trim($_POST[$name."_date"])==""&&trim($_POST[$name."_time"])=="")continue;
					// Ha nem kotelezo a masik mezo es ures, nem szorakozunk
					$basedate=dt2rd(trim($_POST[$name."_date"]),trim($_POST[$name."_time"]));
					$chekdate=dt2rd(trim($_POST[$newname."_date"]),trim($_POST[$newname."_time"]));
					if($basedate<=$chekdate)
							return array(17 => array("type" => "error",
										 "msg" => eee($v["label"]).eee(" nagyobb kell legyen, mint ").eee($fieldarray[$newname]["label"])."!"));
			  }else{
					if($_POST[$name]<=$against)
							return array( 18 => array("type" => "error", "msg" => eee($v["label"]).eee(" nagyobb kell legyen, mint ")."$against!"));
			  }
		  }
		  elseif($oper=="gte"){
			  if($v["type"]=="datetime"){
					if(!$fieldarray[$newname]["req"]&&trim($_POST[$newname."_date"])==""&&trim($_POST[$newname."_time"])=="")continue; // nem kotelezo, nincs kitoltve
					if(!$fieldarray[$name]["req"]&&trim($_POST[$name."_date"])==""&&trim($_POST[$name."_time"])=="")continue;
					// Ha nem kotelezo a masik mezo es ures, nem szorakozunk
					$basedate=dt2rd(trim($_POST[$name."_date"]),trim($_POST[$name."_time"]));
					$chekdate=dt2rd(trim($_POST[$newname."_date"]),trim($_POST[$newname."_time"]));
					if($basedate<$chekdate)
							return array(19 => array("type" => "error",
										 "msg" => eee($v["label"]).eee(" nagyobb vagy egyező kell legyen, mint ").eee($fieldarray[$newname]["label"])."!"));
			  }else{
					if($_POST[$name]<$against)
							return array(20 => array("type" => "error",
										 "msg" => eee($v["label"]).eee(" nagyobb vagy egyező kell legyen, mint ")."$against!"));
			  }
		  }
		  elseif($oper=="lt"){
			  if($v["type"]=="datetime"){
					if(!$fieldarray[$newname]["req"]&&trim($_POST[$newname."_date"])==""&&trim($_POST[$newname."_time"])=="")continue; // nem kotelezo, nincs kitoltve
					if(!$fieldarray[$name]["req"]&&trim($_POST[$name."_date"])==""&&trim($_POST[$name."_time"])=="")continue;
					// Ha nem kotelezo a masik mezo es ures, nem szorakozunk
					$basedate=dt2rd(trim($_POST[$name."_date"]),trim($_POST[$name."_time"]));
					$chekdate=dt2rd(trim($_POST[$newname."_date"]),trim($_POST[$newname."_time"]));
					if($basedate>=$chekdate)
							return array(21 => array("type" => "error",
										 "msg" => eee($v["label"]).eee(" kisebb kell legyen, mint ").eee($fieldarray[$newname]["label"])."!"));
			  }else{
					if($_POST[$name]>=$against)
							return array(22 => array("type" => "error", "msg" => eee($v["label"]).eee(" kisebb kell legyen, mint ")."$against!"));
			  }
		  }
		  elseif($oper=="lte"){
			  if($v["type"]=="datetime"){
					if(!$fieldarray[$newname]["req"]&&trim($_POST[$newname."_date"])==""&&trim($_POST[$newname."_time"])=="")continue; // nem kotelezo, nincs kitoltve
					if(!$fieldarray[$name]["req"]&&trim($_POST[$name."_date"])==""&&trim($_POST[$name."_time"])=="")continue;
					// Ha nem kotelezo a masik mezo es ures, nem szorakozunk
					$basedate=dt2rd(trim($_POST[$name."_date"]),trim($_POST[$name."_time"]));
					$chekdate=dt2rd(trim($_POST[$newname."_date"]),trim($_POST[$newname."_time"]));
					if($basedate>$chekdate)
							return array(23 => array("type" => "error",
										 "msg" => eee($v["label"]).eee(" kisebb vagy egyező kell legyen, mint ").eee($fieldarray[$newname]["label"])."!"));
			  }else{
					if($_POST[$name]>$against)
							return array(24 => array("type" => "error", "msg" => eee($v["label"]).eee(" kisebb vagy egyező kell legyen, mint ")."$against!"));
			  }
		  }
		  elseif($oper=="with"){
		// if datetime... FIXME
			if($fieldarray[$name]["req"]&&trim(trim($_POST[$newname],".-+()_"))==""){
					return array(25 => array("type" => "error",
								 "msg" => eee($v["label"]).eee(" mezővel együtt ").eee($fieldarray[$newname]).eee(" is kitöltendő!")));
			}
		  }
	   }
	 }
	}
return $gyujt;
}

// Form generator
// $formname   : A form name és id értékei. A form action-je az aktualis oldal GET-je lesz; a form mindig POST-tal kuld
// $fieldarray : field-ek tombje. Ennek kulcsa a form field valtozo neve, erteke |-pal elálasztott felsorolás, értékei:
//		Az elso ertek MINDIG a mezo megnevezése, akkor is ki kell tenni ha üres. Ha a név mögött csillag van, kötelezo
//		A második érték MINDIG a típus: textXX, intXX, date, passwordXX, select, chkbox, radio, areaXX
//			(még nincs implementálva: datetime, huf, )
//
//		help:Ez a súgó szöveg megjelenik a mezo mellett, ha belelep
//		error:Ez egy hibaüzenet, amit megkap a képernyön, ha nem jól töltötte ki
//
//		gt:100 vagy gt:[stdat]         ==> (száznál vagy az stdat mezonel nagyobb ertekunek kell lennie)
//		gte, lt, lte, eq               ==> Az elozo mintajara nagyobbegyenlo, kisebb, kisebbegyenlo, egyenlo
//		with:[endat]                   ==> Ha egy mezot kitoltunk, egy másikat is kötelezo, de ha egyiket sem toltjuk ki az is OK
//		pwrule:                        ==> Jelszó szabályok (kidolgozás alatt). Csak jelszóhoz alkalmazható
//		list:x,y,z vagy sql:<select>   ==> Selectnél a választható értékek listája
//    defval:xx                      ==> Alapértelmezett érték
//
//		mask:AAA-999           ==> Maszkolt mezo. A=betu, 9=szám, X=betu vagy szam. Minden mas karakter maszk hatarolo, a pelda egy magyar autó rendszám
//
// $values     : Az ertekek tombje (pl. $_POST, vagy adatbazisbol lekerdezett ertekek)
// $cmd        : Form elemek megjelenesi sorrendjenek felsorolasa. Konkret field nev, elvalaszto jelek, gombok, RO es RW atkapcsolo
//		pelda 1: name,|,realname,-,RO,passw,RW,SB ==> nev es valodi nev ket oszlopban, uj sor, read-only jelszo mezo, majd egy submit gomb
//		pelda 2: RO,name,realname,passw,EB,SB     ==> az egesz form readonly, de van rajta "edit" gomb, HA (!) $_SESSION['planet']['user_id'] == $refvar['owner']
//			 CB ==> Cancel button
function genForm($formname,$fieldarray,$refvar,$cmd){
    $ro=0;
    $vb=0;
    $fs=0;
    $nofc=0;
    $nosize=0;
    $ruleJSarray="";
    $gy = "<form id='".$formname."' name='".$formname."' action='".parseget("").
	  "' method='POST' onSubmit='javascript:return false;'><div class='hbox'>"; // automatikus mkhbox

    $nextField = strtok($cmd, ",");
    while ($nextField !== false) {
		$F = $nextField;
		$nextField = strtok(",");
		$rule = NULL; $rules=NULL;

		if($F=='SB'){
			$gy.="<input type='submit' name='Rendben' value='Rendben'";
			if($ro){$gy.=" disabled='disabled'";}
			$gy.=" >";
		}
		elseif($F=='CB'){
			$gy.="<a href='".parseget("")."'>Mégsem</a>"; // Értelemszeruen a Cancel button readonly képernyon is aktív
		}
		elseif($F=='ACB'){
			$gy.="<input type='button' class='fg-button' style='height:20px;font-size:10px;' name='Mégse' value='Mégse' onClick='javascript:window.location.href=window.location.href;' />"; // AJAX eseten
		}
		elseif($F=='EB'){
			if($_SESSION['planet']['user_id'] == $refvar['owner']){
				$gy.="<input type='button' class='fg-button' name='Szerkeszt' value='Szerkeszt' onClick='javascript:f_editable();'>";
		    }
		}
		elseif($F=='-'){
			$gy.="<br />\n";
		}
		elseif($F=='RO'){
			$ro=1;
		}
		elseif($F=='RW'){
			$ro=0;
		}
		elseif($F=='NOFC'){ // No close form!
			$nofc = 1;
		}
		elseif($F=='NOSIZE'){ // No size parameter for input fields
			$nosize = 1;
		}
		elseif(substr($F,0,3)=='FSS'){ // Fieldset start
			if($fs==1){$gy.="</fieldset>";}
			$fs = 1;
			$gy.="<fieldset><legend>".substr($F,4)."</legend>";
		}
		elseif($F=='FSE'){ // Fieldset end
			if($fs==1){$gy.="</fieldset>";}
			$fs = 0;
		}
		elseif(isset($fieldarray[$F])&&$fieldarray[$F]!=''){

			$rule = explode("|",$fieldarray[$F]);
			$mylabel = array_shift($rule); // Mindig az első helyen kell legyen
			$mytype = array_shift($rule); // Mindig a második helyen kell legyen!
			if(preg_match("/^(.*)\*$/i",$mylabel))$kotelezo=1; else $kotelezo=0;
			for($i=0;$i<count($rule);$i++){
				if(strpos($rule[$i],":") !== false){
					list($ka,$ve)=explode(":",$rule[$i]);
					$rules[$ka]=$ve;
				}
			}

			$gy.="<div class='ff";
			if($nextField=='|'){ $gy.=" vbox"; }else{ $gy.=" hbox"; }
			$gy.="'>";

			if($mylabel!=""){
				$gy.="<label for='$F'";
				if($kotelezo){$gy.=" style='field-req'";}
				$gy.=">$mylabel</label>";
			}

			if(preg_match("/^text([0-9]{0,2})$/i",$mytype,$a)){
				if(!is_numeric($a[1])){$a[1]=30;$ml='';}else{$ml=" maxlength='".$a[1]."'";}
				if($a[1]>MAXINPUTWIDTH) $mysize = MAXINPUTWIDTH; else $mysize = $a[1];
				$gy.="<input type='text' name='$F' id='$F' value='".$refvar[$F]."'".$ml;
			  if($nosize) $gy.=" size='".$mysize."'";
				if($kotelezo)$gy.=" class='required'";
				if($ro)$gy.="' disabled='disabled";
				if($rules['error']!='')$gy.=" title='".$rules['error']."'";
				if($rules['help']!='')$gy.=" alt='".$rules['help']."'";
				$gy.=" style='width:250px;'>";
			}

			elseif(preg_match("/^int([0-9]{0,2})$/i",$mytype,$a)){
				if(!is_numeric($a[1])){$a[1]=MAXINPUTWIDTH;$ml='';}else{$ml=" maxlength='".$a[1]."'";}
				$gy.="<input type='text' name='$F' id='$F' value='".numberize($refvar[$F])."'".$ml;
			  if($nosize) $gy.=" size='".$a[1]."'";
			  $gy.=" class='integer";
				if($kotelezo)$gy.=" required";
			  $gy.="'";
				if($ro)$gy.=" disabled='disabled'";
				if($rules['error']!='')$gy.=" title='".$rules['error']."'";
				if($rules['help']!='')$gy.=" alt='".$rules['help']."'";
				$gy.=">";
			}

			elseif(preg_match("/^password([0-9]{0,2})$/i",$mytype,$a)){
				if(!is_numeric($a[1])){$a[1]=MAXINPUTWIDTH;}
				$gy.="<input type='password' name='$F' id='$F' maxlength='".$a[1]."'";
			  if($nosize) $gy.=" size='".$a[1]."'";
			  $gy.=" class='passwd";
				if($kotelezo)$gy.=" required";
				if($ro)$gy.="' disabled='disabled";
				$gy.="'>\n";
			}

			elseif(eregi("^area([0-9]{0,2})$",$mytype,$a)){
				if(!is_numeric($a[1]))$a[1]=MAXINPUTWIDTH;
				if($params==""&&$a[1]!=""){
					$a[1]*=12;
					$h=$a[1]*0.5;
					$params=" style='width:250px;height:".$h."px'";
				}
				//if($nosize) $params='';
				$gy.="<textarea  name='$F' id='$F' $params";
				if($ro)$gy.=" disabled='disabled'";
				$gy.=">".resqlize($refvar[$F])."</textarea>";
			}


	    elseif($mytype=='select'){
			$gy.="<select name='$F' id='$F'";
			if($ro){$gy.=" disabled='disabled'";}
			if($rules['error']!=''){$gy.=" title='".$rules['error']."'";}
			if($rules['help']!=''){$gy.=" alt='".$rules['help']."'";}
			$gy.=">\n";

			if($rules['sql']!=''){
				$getSql=db_execute($rules['sql']);
				$myertek=db_multirow($getSql);
				if($rules['list']!=''){
					list($firstparam)=explode("|",$rules['list'],1);
					array_unshift($myertek,array("id" => 0, "name" => $firstparam));
				}
			}else{
				$myertek=explode(",",$rules['list']);
			}

#		if(!is_array($myertek)){
#			$myertek=split(",",$myertek);
#			$mylist=1;
#		}

			foreach($myertek as $key => $value){
				if(is_array($value)){
					$subkey="";
					$subval="";
					$i=0;
					foreach($value as $element){
						if($i==0){$subkey=$element;}else{$subval.=$element;}
						$i++;
					}
					$gy.="\n   <option value='".$subkey."'";
					if($subkey == $refvar[$F]){$gy.=" selected";}
					if($subkey == $rules['defval']){$gy.=" selected";}
					$gy.=">".$subval."</option>\n";
				}else{
					$gy.="       <option value='$key'";
					if($key==$refvar[$F])$gy.=" selected";
					elseif($mylist==1 && $value === $refvar[$F])$gy.=" selected";
					$gy.=">$value</option>\n";
				}
			}
			$gy.="</select>";
		} // select

		elseif($mytype=='radio'){

			if($rules['sql']!=''){
				$getSql=db_execute($rules['sql']);
				$myertek=db_multirow($getSql);
			}else{
				$myertek=explode(",",$rules['list']);
			}

			foreach($myertek as $key => $value){

				if(is_array($value)){
					$subkey=""; $subval=""; $i=0;
					foreach($value as $v){
						if($i==0)$subkey=$v;
						else
							if($i==1) $subval=$v;
							else $subval.=" ".$v;
						$i++;
					}
					$gy.="<div class='ff_r'><input type='radio' align='absmiddle' name='$F' id='".$F."_".$subkey."' value='$subkey'";
					if($ro)$gy.=" disabled='disabled'";
					if($subkey == $refvar[$F])$gy.=" checked";
					$gy.=">$subval</div>";
				}else{
					$gy.="<div class='ff_r'><input type='radio' align='absmiddle' name='$F' id='".$F."_".$key."' value='$key'";
					if($ro)$gy.=" disabled='disabled'";
					if($key==$refvar[$F])$gy.=" checked";
					elseif($value===$refvar[$F]){$gy.=" checked";}
					$gy.=">$value</div>";
				}
			}
		}

		elseif($mytype=='chkbox'||$mytype=='status'){

			if($rules['sql']!=''){
				$getSql=db_execute($rules['sql']);
				$myertek=db_multirow($getSql);
			}else{
				$myertek=explode(",",$rules['list']);
			}

			foreach($myertek as $key => $value){
				if(is_array($value)){
					$subkey=""; $subval=""; $i=0;
					foreach($value as $v){
						if($i==0)$subkey=$v;
						else
							if($i==1) $subval=$v;
							else $subval.=" ".$v;
						$i++;
					}
					$gy.="<div class='ff_c'><input type='checkbox' align='absmiddle' name='".$F;
					if($mytype=='status')$gy.="' id='".$F."' value='1'";
					else $gy.="_".$subkey."' id='".$F."_".$subkey."' value='$subkey'";
					if($ro)$gy.=" disabled='disabled'";
					if(($subkey==$refvar[$F]) || (array_search($subkey,$refvar[$F]) !== false))$gy.=" checked";
					$gy.=">$subval</div>";
				}else{
					$gy.="<div class='ff_c'><input type='checkbox' align='absmiddle' name='".$F;
					if($mytype=='status')$gy.="' id='".$F."' value='1'";
					else $gy.="_".$key."' id='".$F."_".$key."' value='$key'";
					if($ro)$gy.=" disabled='disabled'";
					if($mytype=='status'){
						if($refvar[$F]==1)$gy.=" checked";
					}else if($key==$refvar[$F])$gy.=" checked";
					if(is_array($refvar[$F])&&(array_search($key,$refvar[$F]) !== false))$gy.=" checked";
					elseif($value===$refvar[$F]){$gy.=" checked";}
					$gy.=">$value</div>";
				}
			}
		} // if chkbox

		elseif($mytype=='date'){
			if(($refvar[$F]=='' || $refvar[$F]=='0000-00-00')&&$rules['defval']!=''){
				$refvar[$F]=date("Y-m-d",strtotime($rules['defval']));
			}
			$gy.= "<input type='text' name='$F' id='$F' value='".$refvar[$F]."' maxlength='10' size='10'";
			if($ro)$gy.=" disabled='disabled'";
			$gy.=">\n<script type='text/javascript'>".'$(function(){$('."'#".$F."').datepicker({dateFormat: 'yy-mm-dd',dayNamesMin: ['V','H', 'K', 'Sz', 'Cs', 'P', 'Szo'], monthNames: ['Január','Február','Március','Április','Május','Június','Július','Augusztus','Szeptember','Október','November','December']});});</script>";
		}

				elseif($mytype=='datetime'){
			
			if(($refvar[$F]=='' || $refvar[$F]=='0000-00-00 00:00:00')&&$rules['defval']!=''){
				$refvar[$F]=date("Y-m-d H:i:s",strtotime($rules['defval']));
			}
			//if()
			$refvar[$F]=substr($refvar[$F],0,-3);
			$gy.= "<input style='height:18px;width:120px;' type='text' name='".$F."' id='".$F."' value='".$refvar[$F]."' maxlength='10' size='10'";
			if($ro)$gy.=" disabled='disabled'";
			$gy.=">\n";
			
			$gy.="<script type='text/javascript'>".'$(function(){$('."'#".$F."').datetimepicker(
			{
				dateFormat: 'yy-mm-dd',
				dayNamesMin: ['H', 'K', 'Sz', 'Cs', 'P', 'Szo', 'V'], 
				monthNames: ['Január','Február','Március','Április','Május','Június','Július','Augusztus','Szeptember','Október','November','December']
			}
			);});</script>\n";
		}

		$gy.="</div>\n"; // div class=ff lezarasa

		$tjsa="";
		if($rules!==NULL)
			foreach($rules as $ka => $ve){
				if($ka=="gt"||$ka=="gte"||$ka=="lt"||$ka=="lte"||$ka=="eq"||$ka=="with"||$ka=="pwrule")
				$tjsa.=$ka.":".$ve.":";
			}
		if($tjsa!="")$ruleJSarray.="#".$F.":".substr($tjsa,0,-1)."|";

		} // if fieldarray[$F] exists

		if($nextField=="|"){
			$nextField = strtok(",");
		}

    } // End of tokenizer cycle
    if($ruleJSarray!="")$ruleJSarray="\n<script type='text/javascript'>\n  var plaform_".$formname."_c='".
	str_replace("[","#",str_replace("]","",$ruleJSarray))."';\n</script>\n";

if($nofc) return $gy.$ruleJSarray; // Ha az a parancs hogy ne zarjuk le a formot
return $gy."</form></div>".$ruleJSarray;
}

function closeForm(){
	return "</form></div>";
}

// Ezzel a fuggvennyel lehet legjobban ellenorizni, hogy az $errmsg
// tartalmaz-e bizonyos kulcsot (error, msg vagy warn). Ez sokkal jobb pontossagot
// ad, mintha azt neznenk, hogy is_empty($errmsg) mert az nem csak errornal jelez be
function check_err($val, $obj){
    if(!is_object($obj)){
        $obj = (object)$obj;
    }
    foreach($obj as $key => $value){
        if(!is_object($value) && !is_array($value)){
            if($value == $val){return true;}
        }else{return check_err($val, $value);}
    }
    return false;
}

?>
