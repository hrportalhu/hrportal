<?php
session_start();
if($_SERVER['HTTPS']){$ServerName="https://";}else{$ServerName="http://";}
$ServerName.=$_SERVER['HTTP_HOST'];
include_once dirname(__FILE__).("/sys.conf");
include_once dirname(__FILE__).("/_functions.php");
?>
<!DOCTYPE html>
<html>
<head>

<title>Add comment</title>
</head>
<body>
	<?
	if($_REQUEST['command']=="new"){
	?>
  <div class='ui-widget-header my-widget-header' >Megjegyzés felvétele</div>

  <form action="#" method="post">
	  <div id="information" style="height:15px;"></div>
    <div id="fields">
      <input name="user" value="<?php echo $_SESSION['planetsys']['user_id'] ?>" type="hidden" />
      <input name="tipus" value="<?=$_REQUEST['tipus'];?>" type="hidden" />
      <input name="valamiid" value="<?=$_REQUEST['valamiid'];?>" type="hidden" />
      <input name="modulname" value="<?=$_REQUEST['modulname'];?>" type="hidden" />
      <input name="boxfelirat" value="<?=$_REQUEST['boxfelirat'];?>" type="hidden" />
      <input name="customer_id" value="<?php echo $_SESSION['planetsys']['customer_id'] ?>" type="hidden" />
      
			<div class="form-group">
                <div class="col-xs-6">
                    <textarea id="content" name="content" rows="5" class="form-control textarea-editor"></textarea>
                </div>
                <div style="clear:both;height:10px;"></div>
    
            </div>
    </div>
    <input value="Rögzít" id="upload_button" type="button" class="btn btn-sm btn-primary" onclick="javascript:addsystemcomment('new','<?=$_REQUEST['modulname'];?>','<?=$_REQUEST['tipus'];?>',<?php echo $_SESSION['planetsys']['user_id'] ?>,<?=$_REQUEST['valamiid'];?>,'<?=$_REQUEST['boxfelirat'];?>','','');" />
    <?
	}
	elseif($_REQUEST['command']=="edit"){
		$comment=db_row("select * from comments where id='".$_REQUEST['comment_id']."'");
		?>
	<div class='ui-widget-header my-widget-header' >Megjegyzés szerkesztése</div>

  <form action="#" method="post">
	  <div id="information" style="height:15px;"></div>
    <div id="fields">
      <input name="user" value="<?php echo $_SESSION['planetsys']['user_id'] ?>" type="hidden" />
      <input name="tipus" value="<?=$_REQUEST['tipus'];?>" type="hidden" />
      <input name="valamiid" value="<?=$_REQUEST['valamiid'];?>" type="hidden" />
      <input name="modulname" value="<?=$_REQUEST['modulname'];?>" type="hidden" />
      <input name="boxfelirat" value="<?=$_REQUEST['boxfelirat'];?>" type="hidden" />
      <input name="customer_id" value="<?php echo $_SESSION['planetsys']['customer_id'] ?>" type="hidden" />
      <div class="form-group">
                <div class="col-xs-6">
                    <textarea id="content" name="content" rows="5" class="form-control textarea-editor" value="<?=$comment['content'];?>"><?=$comment['content'];?></textarea>
                </div>
                <div style="clear:both;height:10px;"></div>
    
            </div>
      
     
    </div>
    
    <input value="Rögzít" id="upload_button" type="button" class="btn btn-sm btn-primary" onclick="javascript:addsystemcomment('edit','<?=$_REQUEST['modulname'];?>','<?=$_REQUEST['tipus'];?>',<?php echo $_SESSION['planetsys']['user_id'] ?>,<?=$_REQUEST['valamiid'];?>,'<?=$_REQUEST['boxfelirat'];?>','','<?=$_REQUEST['comment_id'];?>');" />
	<?
	}
	?>
  </form>
</body>
</html>
