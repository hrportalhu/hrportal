<?php
session_start();
header('Content-Type: text/plain;charset=utf-8');
include("../inc/sys.conf");
include("../inc/_functions.php");



echo"<div id=\"printdiv\" style=\"color:#000;background-color:#fff;\">";
	$user=$_SESSION['planetsys']['user_id'];
	$tipus=$_REQUEST['tipus'];
	$valamiid=$_REQUEST['valamiid'];
	$modul=$_REQUEST['modulname'];

if($tipus=="prodtypespecprint"){
	include("../inc/print_prodtypespecprint.php");
}
elseif($tipus=="prodtypespecprintsmall"){
	include("../inc/print_prodtypespecprintsmall.php");
}
elseif($tipus=="prodtypeosszetevoprint"){
	include("../inc/print_prodtypeosszetevoprint.php");
}
elseif($tipus=="producttrackprint"){
	include("../inc/print_producttrackprint.php");
}
elseif($tipus=="workerprintlist"){
	include("../inc/print_workerprintlist.php");
}
elseif($tipus=="bevetelezesprint"){
	include("../inc/print_bevetelezesprint.php");
}
elseif($tipus=="issueprint"){
	include("../inc/print_issueprint.php");
}
elseif($tipus=="traumaprint"){
	include("../inc/print_traumaprint.php");
}
elseif($tipus=="inaptprint"){
	include("../inc/print_inaptprint.php");
}
elseif($tipus=="visitorprint"){
	include("../inc/print_visitorprint.php");
}
elseif($tipus=="bclprint"){
	include("../inc/print_bclprint.php");
}
elseif($tipus=="traningsprint"){
	include("../inc/print_traningsprint.php");
}
elseif($tipus=="keyeventsprint"){
	include("../inc/print_keyeventsprint.php");
}
elseif($tipus=="scrappingprint"){
	include("../inc/print_scrappingprint.php");
}
elseif($tipus=="ordercheckprint"){
	include("../inc/print_ordercheckprint.php");
}
elseif($tipus=="ordercheckclearprint"){
	include("../inc/print_ordercheckclearprint.php");
}
elseif($tipus=="manufactalapprint"){
	list($manufact_id,$post_id)=explode("-",$valamiid);
	$mf=db_row("select * from manufact_header where id='".$manufact_id."'");
	echo"<div><img src=\"/img/interfood_docuheader.jpg\" style=\"width:670px;\" /></div>";
	echo"<h1 style=\"text-align:center;\">Raktárkiadás<h1>";	
	
	echo"<h4 >".idchange("gw_posts","name",$post_id)." alapanyag lista ".$mf['manufact_date']." napra</h4>";
				echo"<table id=\"general-table\" class=\"table table-striped table-vcenter\">
					<thead>
						<tr>
							<th>Alapanyag megnevezése</th>
							<th>Kiadandó mennyiség</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>";
					$alapanyags=db_all("select * from manufact_ag where post_id='".$post_id."' group by agt_id order by agt_name asc ");
											
							for($a=0;$a<count($alapanyags);$a++){
								$col="";if($alapanyags[$a]['kiadva']==1){
									$col="background-color:yellow;";
								}
					$summ=db_one("select sum(agt_sume) from manufact_ag where post_id='".$post_id."' and mf_id='".$manufact_id."' and agt_id='".$alapanyags[$a]['agt_id']."'");
					if($summ!=""){
							echo"<tr >
							<td>".$alapanyags[$a]['agt_name']."</td>
							<td>".round($summ,5)." Kg</td>
							<td>";
							echo"<div style=\"width:17px;height:17px;border:2px solid #000;\" >";
								
							echo"</div>";
							echo"</td>
							</tr>";
					}
				}
					echo"</tbody> 
					</table>";
	
	
	
	
}
elseif($tipus=="manufactprodprint"){
	list($manufact_id,$post_id)=explode("-",$valamiid);
	$mf=db_row("select * from manufact_header where id='".$manufact_id."'");
	$pts=db_all("select * from manufact_element where post_id='".$post_id."' and mh_id='".$manufact_id."' group by pt_id order by pt_id ");
	echo"<div><img src=\"/img/interfood_docuheader.jpg\" style=\"width:670px;\" /></div>";
	echo"<h1 style=\"text-align:center;\">Termékgyártási lista<h1>";
	
	echo"<h4 >".idchange("gw_posts","name",$post_id)." termékkezelő lista ".$mf['manufact_date']." napra</h4>";
	echo"<table id=\"general-table\" class=\"table table-striped table-vcenter\" >
				<thead>
					<tr>
						<th>Termék elnevezése</th>
						<th>Rendelt mennyiség - rendelése</th>
						<th>Rendelt mennyiség - Kg</th>
						<th>Készleten levő mennyiség</th>
						<th>Hiányzó mennyiség /Kg/</th>
						<th>Gyártani kell</th>
					</tr>
				</thead>
				<tbody>";
			for($a=0;$a<count($pts);$a++){
				$me=mee($pts[$a]['pt_id']);
				echo"<tr>";
				echo"<td><a href=\"modules/goods/producttype/".$pts[$a]['pt_id']."\">".idchange("product_types","name",$pts[$a]['pt_id'])."</a></td>";
				echo"<td>".$pts[$a]['orderbase_amount']."</td>";
				echo"<td>".round($pts[$a]['order_amount'],3)."</td>";
				echo"<td>".$pts[$a]['instock']."</td>";
				echo"<td>".round(($pts[$a]['order_amount']-$pts[$a]['instock']),3)."</td>";
				echo"<td>".$pts[$a]['manufact_amount']." ".$me['gyartas_egyseg']."</td>";
				echo"</tr>";
			}
			  echo"</tbody> </table>";
}
elseif($tipus=="manufactsorbatprint"){
	list($manufact_id,$post_id)=explode("-",$valamiid);
	$mf=db_row("select * from manufact_header where id='".$manufact_id."'");
	$pts=db_all("select * from manufact_element where post_id='".$post_id."' and mh_id='".$manufact_id."' group by pt_id order by pt_id ");
	echo"<div><img src=\"/img/interfood_docuheader.jpg\" style=\"width:670px;\" /></div>";
	echo"<h1 style=\"text-align:center;\">Kálium-szorbát kiadási lista<h1>";
	echo"<h4 >".idchange("gw_posts","name",$post_id)." Kálium-szorbát kiadási lista ".$mf['manufact_date']." napra</h4>";
	$day=$mf['manufact_date']." 23:59:59";
	if($day==date("Y-m-d",time())){
		$day=date("Y-m-d H:i:s",time());
	}
	$summ=db_one("select sum(agt_sume) from manufact_ag where post_id='".$post."' and mf_id='".$manufact_id."' and agt_id='61'");
	$elem=db_row("select ae.*,a.lot as lot,a.mmi as mmi  from alapag_events ae left join alapanyag a on (ae.ag_id=a.id) where ae.event=3 and ae.rdate<='".$day."' and ae.agt_id='61'  order by ae.rdate  desc limit 1");
	//echo "select ae.*,a.lot as lot,a.mmi as mmi  from alapag_events ae left join alapanyag a on (ae.ag_id=a.id) where ae.event=3 and ae.rdate<='".$day."' and ae.agt_id='61' and (a.consumed='0000-00-00 00:00:00' or a.consumed>'".$day."') order by ae.rdate  desc limit 1";
	echo"<table id=\"general-table\" class=\"table table-striped table-vcenter\" >
				<thead>
					<tr>
						<th>Termék elnevezése</th>
						<th>Gyártási mennyiség</th>
						<th>Kálium-szorbát tétele</th>
						<th>Tétel Kálium-szorbát mennyiség</th>
						<th>Kiadva</th>
						
					</tr>
				</thead>
				<tbody>";
			$prods=db_all("select pt_id,pt_name from manufact_ag where post_id='".$post_id."' and agt_id='61' and mf_id='".$manufact_id."' group by pt_id");
			//echo"select pt_id,pt_name from manufact_ag where post_id='".$post_id."' and agt_id='61' and mf_id='".$manufact_id."' group by pt_id\n";
			if(count($prods)>0){
				for($v=0;$v<count($prods);$v++){
					$keveres=db_one("select sum(pt_me) from manufact_ag where post_id='".$post_id."' and agt_id='61' and mf_id='".$manufact_id."' and pt_id='".$prods[$v]['pt_id']."'");
					$sume=db_one("select sum(agt_sume) from manufact_ag where post_id='".$post_id."' and agt_id='61' and mf_id='".$manufact_id."' and pt_id='".$prods[$v]['pt_id']."'");												
					echo"<tr>";
					echo"<td><a href=\"modules/goods/producttype/".$prods[$v]['pt_id']."\">".$prods[$v]['pt_name']."</a></td>";
					echo"<td>".$keveres." keverés</td>";
					echo"<td>".$elem['lot']." tétel - (".$elem['mmi'].")</td>";
					echo"<td>".round($sume/$keveres,5)." kg</td>";
					echo"<td>".$keveres." x ".round($sume/$keveres,5)." Kg<br />azaz ".round($sume,4)." Kg</td>";
		
					echo"</tr>";
				}
			}
			  echo"</tbody> </table>";
			  	echo"<center><table style=\"font-size:15px;width:750px;border-spacing:20px;padding:5px;\">";
		echo"<tr>	
				<td></td>
				<td align=\"center\" valign=\"middle\">............................................</td>
				<td></td>
				<td align=\"center\" valign=\"middle\">&nbsp;</td>
				
		</tr>";	
		echo"<tr>	
				<td align=\"right\">Kiadó:</td>
				<td align=\"center\" valign=\"middle\">Magyar Zoltán</td>
				<td align=\"right\">&nbsp;</td>
				<td align=\"center\" valign=\"middle\">&nbsp;</td>
				
		</tr>";
		
	
	echo"</table></center>";
}
elseif($tipus=="manufactcukorprint"){
	list($manufact_id,$post_id)=explode("-",$valamiid);
	$mf=db_row("select * from manufact_header where id='".$manufact_id."'");
	$pts=db_all("select * from manufact_element where post_id='".$post_id."' and mh_id='".$manufact_id."' group by pt_id order by pt_id ");
	echo"<div><img src=\"/img/interfood_docuheader.jpg\" style=\"width:670px;\" /></div>";
	echo"<h1 style=\"text-align:center;\">Porcukor darálási lista<h1>";
	echo"<h4 >".idchange("gw_posts","name",$post_id)." Porcukor darálási lista ".$mf['manufact_date']." napra</h4>";
	$day=$mf['manufact_date']." 23:59:59";
	if($day==date("Y-m-d",time())){
		$day=date("Y-m-d H:i:s",time());
	}
	$summ=db_one("select sum(agt_sume) from manufact_ag where post_id='".$post."' and mf_id='".$manufact_id."' and agt_id='15'");
	$elem=db_row("select ae.*,a.lot as lot,a.mmi as mmi  from alapag_events ae left join alapanyag a on (ae.ag_id=a.id) where ae.event=3 and ae.rdate<='".$day."' and ae.agt_id='15'  order by ae.rdate  desc limit 1");
	//echo "select ae.*,a.lot as lot,a.mmi as mmi  from alapag_events ae left join alapanyag a on (ae.ag_id=a.id) where ae.event=3 and ae.rdate<='".$day."' and ae.agt_id='61' and (a.consumed='0000-00-00 00:00:00' or a.consumed>'".$day."') order by ae.rdate  desc limit 1";
	echo"<table id=\"general-table\" class=\"table table-striped table-vcenter\" >
				<thead>
					<tr>
						<th>Termék elnevezése</th>
						<th>Gyártási mennyiség</th>
						<th>Cukor tétele</th>
						<th>Cukor mennyiség</th>
						<th>Darálva</th>
						
					</tr>
				</thead>
				<tbody>";
			$prods=db_all("select pt_id,pt_name from manufact_ag where post_id='".$post_id."' and agt_id='15' and mf_id='".$manufact_id."' group by pt_id");
			//echo"select pt_id,pt_name from manufact_ag where post_id='".$post_id."' and agt_id='61' and mf_id='".$manufact_id."' group by pt_id\n";
			if(count($prods)>0){
				for($v=0;$v<count($prods);$v++){
					$keveres=db_one("select sum(pt_me) from manufact_ag where post_id='".$post_id."' and agt_id='15' and mf_id='".$manufact_id."' and pt_id='".$prods[$v]['pt_id']."'");
					$sume=db_one("select sum(agt_sume) from manufact_ag where post_id='".$post_id."' and agt_id='15' and mf_id='".$manufact_id."' and pt_id='".$prods[$v]['pt_id']."'");												
					echo"<tr>";
					echo"<td><a href=\"modules/goods/producttype/".$prods[$v]['pt_id']."\">".$prods[$v]['pt_name']."</a></td>";
					echo"<td>".$keveres." </td>";
					echo"<td>".$elem['lot']."-as tétel - (".$elem['mmi'].")</td>";
					echo"<td>".round($sume/$keveres,5)." kg</td>";
					echo"<td>".$keveres." x ".round($sume/$keveres,5)." Kg<br />azaz ".round($sume,4)." Kg</td>";
		
					echo"</tr>";
				}
			}
			  echo"</tbody> </table>";
			  	echo"<center><table style=\"font-size:15px;width:750px;border-spacing:20px;padding:5px;\">";
		echo"<tr>	
				<td></td>
				<td align=\"center\" valign=\"middle\">............................................</td>
				<td></td>
				<td align=\"center\" valign=\"middle\">&nbsp;</td>
				
		</tr>";	
		echo"<tr>	
				<td align=\"right\">Kiadó:</td>
				<td align=\"center\" valign=\"middle\">Magyar Zoltán</td>
				<td align=\"right\">&nbsp;</td>
				<td align=\"center\" valign=\"middle\">&nbsp;</td>
				
		</tr>";
		
	
	echo"</table></center>";
}
elseif($tipus=="prodtyperecepturaprint"){
	$p=db_row("select * from product_types where id='".$valamiid."'");
	echo"<div><img src=\"/img/interfood_docuheader.jpg\" style=\"width:670px;\" /></div>";
	echo"<h1 style=\"text-align:center;\">Receptúra<h1>";	
	echo"<div style=\"font-size:15px;\">Termék neve: ".$p['name']."</div>";
	echo"<div style=\"font-size:15px;\">Fogyasztható: ".$p['mmi']." nap</div>";
	/*
	if(($p['tarol_hutve']==1) && ($p['tarol_szaraz']==1)){
		$tarol="0-5°C fok között, száraz, napfénytől védett helyen";
	}elseif($p['tarol_fagyasztva']==1){
		$tarol="-18°C fok alatt, fagyasztva";
	}elseif($p['tarol_hutve']==1){
		$tarol="0-5°C fok között";
	}elseif($p['tarol_szaraz']==1){
		$tarol="száraz, napfénytől védett helyen";
	}else{
		$tarol="nem meghatározott módon";
	}
	
	echo"<div style=\"font-size:15px;\">Tárolási hőmérséklet: ".$tarol."</div>";//szöveg
	*/ 
	if($p['halfproduct']=="1"){
		echo"<div style=\"font-size:15px;\">Besorolás: félkész / késztermék: Félkésztermék</div>";
	}
	else{
		echo"<div style=\"font-size:15px;\">Besorolás: félkész / késztermék: Késztermék</div>";
		}


	echo"<div style=\"clear:both;height:20px;\"></div>";
	$pic=db_one("select filename from files where modul='goods' and type='producttype' and type_id='".$valamiid."' order by cover desc");
	if($pic!=""){echo"<div  style=\"text-align:center;\"><img src=\"http://if.plsys.hu/".$pic."\" alt=\"avatar\" width=\"350px;\" ></div>";}
	echo"<div style=\"clear:both;height:20px;\"></div>";
	////
	$elemid=$valamiid;
	$myproducttype=db_row("select * from product_types where id='".$valamiid."'");
		$R=db_all("SELECT pxp.id, pxp.pt2_id as oid,pxp.amount,pxp.operation,pxp.post_id as post_id, 't' as type, pt.name,pt.fajlagos_ar AS ar, pt.id as elemid FROM pt_x_pt pxp
			LEFT JOIN product_types pt ON (pt.id = pxp.pt2_id)
			WHERE pxp.pt1_id = '$elemid' AND 
			pxp.archdat = '0000-00-00 00:00:00'
		  UNION
		  SELECT pxa.id, pxa.agt_id as oid,pxa.amount,pxa.operation,pxa.post_id as post_id,'a' as type, agt.name,agt.baseprice AS ar, agt.id as elemid FROM pt_x_agt pxa
			LEFT JOIN alapag_tipus agt ON (agt.id = pxa.agt_id)
			WHERE pxa.pt_id = '$elemid' AND 
			pxa.archdat = '0000-00-00 00:00:00'
		  ORDER BY name ASC");

	$menu=db_all("SELECT id,name,'t' AS type FROM product_types WHERE status = 1
			UNION 
			  SELECT id,name,'a' AS type FROM alapag_tipus WHERE status = 1
			ORDER BY name ASC");
							
						
								echo"
									<table  id=\"general-table\" class=\"table table-striped table-vcenter\">
									<thead>
										<tr>
											<th style=\"width:220px;\">Tétel</th>
											<th>Mennyiség</th>
											<th >Poszt</th>
											
											<th >Össz mee (Kg)</th>
										</tr>
									</thead>
								<tbody style=\"font-size:15px;\">";

							
							$i=1; $tottotprice=0; $tottotweight=0;
							$posts=db_all("select id,name from gw_posts where status=1 order by name");
							for($i=0;$i<count($R);$i++){

								if($R[$i]['type']=="t"){
									echo"<td>".$R[$i]['name']." (".$R[$i]['type']."".$R[$i]['oid'].")</td>";
								}else{
									echo"<td>".$R[$i]['name']." (".$R[$i]['type']."".$R[$i]['oid'].")</td>";
								}	
									echo"<td>".round((isset($R[$i]['amount'])?$R[$i]['amount']:"0"),3)."</td>";
									echo"<td>".idchange("gw_posts","name",$R[$i]['post_id'])."</td>";
									
									echo"<td style=\"text-align:right;\">".round($R[$i]['amount'],3)." Kg</td>";
									
									
									$tottotprice+=round($R[$i]['ar']*$R[$i]['amount'],1);
									$tottotweight+=$R[$i]['amount'];
								echo"</tr>";
							}
							echo"
							<tr>
								<td  colspan=\"3\">&nbsp;</td>
								<td style=\"text-align:right;\">".round($tottotweight,6)." Kg</td>

							</tr>";
							echo"
							<tr>
								<td  colspan=\"3\">TOTAL nettó - Különböző százalékos veszteségekkel számolva</td>
								<td style=\"text-align:right;\">".round($myproducttype['total_nweight'],3)." Kg (<small>".round($myproducttype['total_nweight'],5)." Kg</small>)</td>

							</tr>";

							echo"</tbody>";
							echo"</table>";	
							
							echo"<h3>Darabáru</h3>";
								echo"<div class=\"table-responsive\">
								<table class=\"table table-vcenter table-striped\">
									<thead>
										<tr>
											<th style=\"width: 100px;\" class=\"text-center\"><i class=\"gi gi-disk_save\"></i></th>
											<th>Termék</th>
											<th>Darabszám</th>
ó										</tr>
									</thead>
									<tbody  id=\"dacont\">";
									$darabs=db_all("select * from pt_darabaru where pt_id='".$myproducttype['id']."' order by status desc");
										if(count($darabs)>0){
											for($i=0;$i<count($darabs);$i++){
												$st="Lejárt";if($darabs[$i]['status']==1){$st="Aktív";}
												echo"<tr>";
												echo"<td style=\"width: 100px;\" class=\"text-center\">".$darabs[$i]['id']."</td>";
												echo"<td>".idchange("alapag_tipus","name",$darabs[$i]['agt_id'])."</div></td>";
												echo"<td>".round($darabs[$i]['darab'],3)."</td>";
												echo"</tr>";
											}
										}
									echo"</tbody>
								</table>		
								</div>	
								
								<h3>Szorzók</h3>
								<div class=\"table-responsive\">
								<table class=\"table table-vcenter table-striped\">
									<thead>
										<tr>
											<th style=\"width: 100px;\" class=\"text-center\"><i class=\"gi gi-disk_save\"></i></th>
											<th>Típus</th>
											<th>Elnevezés</th>
											<th>Százalék</th>
											</tr>
									</thead>
									<tbody  id=\"szcont\">";
									$darabs=db_all("select * from pt_szorzok where pt_id='".$myproducttype['id']."' order by status desc");
										if(count($darabs)>0){
											for($i=0;$i<count($darabs);$i++){
												$st="Lejárt";if($darabs[$i]['status']==1){$st="Aktív";}
												if($darabs[$i]['szorzotipus']=="weight"){
													$szorzat=$szorzat+$darabs[$i]['amount'];
												}
												echo"<tr>";
												echo"<td style=\"width: 100px;\" class=\"text-center\">".$darabs[$i]['id']."</td>";
												echo"<td>".$darabs[$i]['szorzotipus']."</div></td>";
												echo"<td>".$darabs[$i]['name']."</td>";
												echo"<td>".$darabs[$i]['amount']." %</td>";
												
												
												echo"</tr>";
											}
										}
									echo"</tbody>
								</table>		
								</div>";	

	
	
}
else{
	$R=db_row("SELECT * FROM ".$_SESSION['planetsys']['actpage']['scrtable']." WHERE id = '".$_REQUEST['valamiid']."' ");
	echo"<div style=\"margin-top:1px;\"><div><span style=\"font-weight:bold;font-size:14px;\">Adatok megtekintése</span></div>";
	echo"<div style=\"margin-top:1px;padding:5px;\"><div>Sorszám: <span style=\"font-weight:bold;font-size:14px;\">".$_REQUEST['valamiid']."</span></div>".showRecord($_SESSION['planetsys']['actpage'],$R)."<br />";
	echo"<div style=\"font-weight:bold;font-size:14px;\">Hozzászólások, megjegyzések</div>";
	echo"<div>";
		$comments=db_all("select * from comments where modul='".$modul."' and customer_id='".$_SESSION['planetsys']['customer_id']."' and type='".$tipus."' and type_id='".$valamiid."' and status='1' order by id desc");
		if(count($comments)>0){
			for($i=0;$i<count($comments);$i++){
				echo"<div style=\"margin:10px;border:1px solid #ccc;padding:5px;\">";
				echo"<div  class=\"ui-widget-header my-widget-header\" style=\"background:none;border:1px solid #ccc;color:#000;\">";
				echo"<div style=\"width:50%;float:left;font-weight:bold;\">".idchange("users","realname",$comments[$i]['user_id'])."</div>";
				echo"<div style=\"width:50%;float:left;text-align:center;\" >".$comments[$i]['rdate']." - ".ezDate($comments[$i]['rdate'])." </div>";
				echo"<div class=\"clear5\"></div>";			
				echo"</div>";			
				echo"<div class=\"clear5\"></div>".stripslashes(nl2br($comments[$i]['content']))."<div class=\"clear5\"></div>";
				echo"</div>";
				echo"<div class=\"clear\"></div>";		
			}
		}
		else{
			echo"<div style=\"text-align:center;font-weight:bold;margin-top:10px;margin-bottom:10px;\">Még nincs feltöltve hozzászólás</div>";	
		}
		echo"</div>";
}		
echo"</div>";
mysql_close($connid);

?>

