<?php
session_start();
include("../inc/sys.conf");
include("../inc/_functions.php");
//header('Content-Type: text/xml;charset=utf-8');
	$user=$_SESSION['planetsys']['user_id'];
	$tipus=$_REQUEST['tipus'];
	$valamiid=$_REQUEST['valamiid'];
	$modul=$_REQUEST['modulname'];
	$size=0;
	if(isset($_SESSION['planetsys']['size'])){
		$size=$_SESSION['planetsys']['size'];
	}
	$files=db_all("select * from files where modul='".$modul."'  and type='".$tipus."' and type_id='".$valamiid."' and status='1' order by cover desc","0");
	$groups=db_all("select id,name from groups where status=1 order by name");
?>
 <div class="col-lg-<?=$size;?>">


            <!-- Product Images Block -->
            <div class="block">
                <!-- Product Images Title -->
                <div class="block-title">
                    <h2><i class="fa fa-picture-o"></i> <strong>Tartalmak </strong> listája</h2>
                </div>
                <!-- END Product Images Title -->

                <table class="table table-bordered table-striped table-vcenter">
                    <tbody>
						<?
							for($i=0;$i<count($files);$i++){
								if($files[$i]['specazonosito']==""){
									$speckode=generateCode(10).date("YmdHis",time()).generateCode(10);
									db_execute("update files set specazonosito='".$speckode."' where id='".$files[$i]['id']."'");	
								}
								$files[$i]['filename']=str_replace("../","",$files[$i]['filename']);
								$files[$i]['filename']=$_SESSION['servername']."/".$files[$i]['filename'];
								
						?>
                        <tr>
                            <td style="width: 20%;">
								<?
								if($files[$i]['doctype']=="image/jpeg" || $files[$i]['doctype']=="image/gif" || $files[$i]['doctype']=="image/png"){
									echo"<a href=\"".$files[$i]['filename']."\" data-toggle=\"lightbox-image\" >
										<img src=\"".$files[$i]['filename']."\" alt=\"\" class=\"img-responsive center-block\" style=\"max-width: 80px;\">
									</a>";
								}
								else{
									echo "<center><a href=\"".$files[$i]['filename']."\" ><i class=\"gi gi-file fa-5x text-info \"></i></a></center>";
								}
                                ?>
                            </td>
                            <td class="text-left">
								<?
								
								echo"<a href=\"".$files[$i]['filename']."\" >".$files[$i]['title']." (".$files[$i]['name'].")</a><br />".format_bytes($files[$i]['filesize'])."";
								
									if($files[$i]['user_id']==$_SESSION['planetsys']['user_id'] || $_SESSION['planetsys']['group_id']==1){
										
									}
									 echo"<br />";
										
										echo"<div onclick=\"javascript:tl('fi_".$files[$i]['id']."');\" style=\"cursor:pointer;\">Részletek >> </div>";
										echo"<div id=\"fi_".$files[$i]['id']."\" style=\"display:none;\">";
												echo"<div class=\"form-group\">
													<label class=\"col-md-3 control-label\" for=\"title_".$files[$i]['id']."\"> Elnevezés:</label>
													<div class=\"col-md-9\">
														<input type=\"text\" id=\"title_".$files[$i]['id']."\" name=\"title_".$files[$i]['id']."\" class=\"form-control\" value=\"".$files[$i]['title']."\"  />																		
													</div>				
												</div>";
												echo"<div class=\"form-group\">
													<label class=\"col-md-3 control-label\" for=\"megjegyzes_".$files[$i]['id']."\">Megjegyzés:</label>
													<div class=\"col-md-9\">
														<input type=\"text\" id=\"megjegyzes_".$files[$i]['id']."\" name=\"megjegyzes_".$files[$i]['id']."\" class=\"form-control\" value=\"".$files[$i]['megjegyzes']."\"  />																		
													</div>				
												</div>";
										
												echo"<div class=\"form-group\">
													<label class=\"col-md-3 control-label\" for=\"revision_".$files[$i]['id']."\">Revizió:</label>
													<div class=\"col-md-9\">
														<input type=\"text\" id=\"revision_".$files[$i]['id']."\" name=\"revision_".$files[$i]['id']."\" class=\"form-control\" value=\"".$files[$i]['revision']."\"  />																		
													</div>				
												</div>";
										
											echo" <label class=\"switch switch-primary\" >";
											$ch=0;if($files[$i]['cover']==1){ $ch=" checked "; }
											echo"<input  type=\"checkbox\" id=\"cover_".$files[$i]['id']."\" name=\"cover_".$files[$i]['id']."\" value=\"cover_".$files[$i]['id']."\" ".$ch."><span></span>";
											echo"</label>Borító - kiemelt kép<br />";
										
											echo" <label class=\"switch switch-primary\" >";
											$ch3=0;if($files[$i]['ptitle']==1){ $ch3=" checked "; }
											echo"<input  type=\"checkbox\" id=\"ptitle_".$files[$i]['id']."\" name=\"ptitle_".$files[$i]['id']."\" value=\"ptitle_".$files[$i]['id']."\" ".$ch3."><span></span>";
											echo"</label>Logó<br />";
										
											echo" <label class=\"switch switch-primary\" >";
											$ch4=0;if($files[$i]['pspec']==1){ $ch4=" checked "; }
											echo"<input  type=\"checkbox\" id=\"pspec_".$files[$i]['id']."\" name=\"pspec_".$files[$i]['id']."\" value=\"pspec_".$files[$i]['id']."\" ".$ch4."><span></span>";
											echo"</label>Támogató logó /max: 200x80px/<br />";
										/*
											echo" <label class=\"switch switch-primary\" >";
											$ch5=0;if($files[$i]['pmibiz']==1){ $ch5=" checked "; }
											echo"<input  type=\"checkbox\" id=\"pmibiz_".$files[$i]['id']."\" name=\"pmibiz_".$files[$i]['id']."\" value=\"pmibiz_".$files[$i]['id']."\" ".$ch5."><span></span>";
											echo"</label>Minőségi bizonyítvány<br />";
										
											echo" <label class=\"switch switch-primary\" >";
											$ch6=0;if($files[$i]['pbizta']==1){ $ch6=" checked "; }
											echo"<input  type=\"checkbox\" id=\"pbizta_".$files[$i]['id']."\" name=\"pbizta_".$files[$i]['id']."\" value=\"pmibiz_".$files[$i]['id']."\" ".$ch6."><span></span>";
											echo"</label>Biztonsági Adatlap<br />";
										
											echo" <label class=\"switch switch-primary\" >";
											$ch7=0;if($files[$i]['pgmo']==1){ $ch7=" checked "; }
											echo"<input  type=\"checkbox\" id=\"pgmo_".$files[$i]['id']."\" name=\"pgmo_".$files[$i]['id']."\" value=\"pgmo_".$files[$i]['id']."\" ".$ch7."><span></span>";
											echo"</label>GMO Nyilatkozat<br />";
										
											echo" <label class=\"switch switch-primary\" >";
											$ch8=0;if($files[$i]['p1935']==1){ $ch8=" checked "; }
											echo"<input  type=\"checkbox\" id=\"p1935_".$files[$i]['id']."\" name=\"p1935_".$files[$i]['id']."\" value=\"p1935_".$files[$i]['id']."\" ".$ch8."><span></span>";
											echo"</label>1935 számú rendelet - Élelmiszerrel érintkező csomagoló anyagok nyilatkozata<br />";
										
											echo" <label class=\"switch switch-primary\" >";
											$ch9=0;if($files[$i]['pbesz']==1){ $ch9=" checked "; }
											echo"<input  type=\"checkbox\" id=\"pbesz_".$files[$i]['id']."\" name=\"pbesz_".$files[$i]['id']."\" value=\"pbesz_".$files[$i]['id']."\" ".$ch9."><span></span>";
											echo"</label>Beszállítói nyilatkozat<br />";
											
											echo" <label class=\"switch switch-primary\" >";
											$ch10=0;if($files[$i]['veszelyelemzes']==1){ $ch10=" checked "; }
											echo"<input  type=\"checkbox\" id=\"veszelyelemzes_".$files[$i]['id']."\" name=\"veszelyelemzes_".$files[$i]['id']."\" value=\"veszelyelemzes_".$files[$i]['id']."\" ".$ch10."><span></span>";
											echo"</label>Veszélyelemzés<br />";
											
											echo" <label class=\"switch switch-primary\" >";
											$ch11=0;if($files[$i]['grafikon']==1){ $ch11=" checked "; }
											echo"<input  type=\"checkbox\" id=\"grafikon_".$files[$i]['id']."\" name=\"grafikon_".$files[$i]['id']."\" value=\"grafikon_".$files[$i]['id']."\" ".$ch11."><span></span>";
											echo"</label>Grafikon<br />";
											
											echo" <label class=\"switch switch-primary\" >";
											$ch12=0;if($files[$i]['torveny']==1){ $ch12=" checked "; }
											echo"<input  type=\"checkbox\" id=\"torveny_".$files[$i]['id']."\" name=\"torveny_".$files[$i]['id']."\" value=\"torveny_".$files[$i]['id']."\" ".$ch12."><span></span>";
											echo"</label>Törvény, jogszabály, rendelet<br />";
											*/
											
											echo" <label class=\"switch switch-primary\" >";
											$ch2=0;if($files[$i]['shared']==1){ $ch2=" checked "; }
											echo"<input  type=\"checkbox\" id=\"shared_".$files[$i]['id']."\" name=\"shared_".$files[$i]['id']."\" value=\"shared_".$files[$i]['id']."\" ".$ch2."><span></span>";
											echo"</label>Megosztás<br />";
											
											echo"<div>Direkt link: <a href=\"".$_SESSION['servername']."/files/".$files[$i]['specazonosito']."\" target=\"_blank\">".$_SESSION['servername']."/files/".$files[$i]['specazonosito']."</a></div>";
											echo"<div>Megosztás csoportnak:</div>";
											$filestring="";
											if($files[$i]['shared_option']!=''){
												$sp=json_decode($files[$i]['shared_option'],1);
												$gr=$sp['groups'];	
											}
											//print_r($gr);
											for($a=0;$a<count($groups);$a++){
												echo" <label class=\"switch switch-primary\" >";
												$ch2="";
												if(in_array($groups[$a]['id'],$gr)){
													$ch2=" checked "; 
												}
												echo"<input  type=\"checkbox\" id=\"file_".$files[$i]['id']."_".$groups[$a]['id']."\" name=\"file_".$files[$i]['id']."_".$groups[$a]['id']."\" value=\"file_".$files[$i]['id']."_".$groups[$a]['id']."\" ".$ch2."><span></span>";
												echo"</label>".$groups[$a]['name']."<br />";
												$filestring.=$groups[$a]['id']."-";
											}
											echo"<input type=\"hidden\" id=\"filestring_".$files[$i]['id']."\" value=\"".$filestring."\"/>";
											echo"</div>";
                                ?>
                               
                            </td>
                            <td class="text-center">
								<?
									if($files[$i]['user_id']==$_SESSION['planetsys']['user_id'] || $_SESSION['planetsys']['group_id']==1){
										echo"<a href=\"javascript:savefiledata('".$modul."','".$tipus."','".$valamiid."','".$files[$i]['id']."'); \" class=\"btn btn-xs btn-info\"><i class=\"fa fa-floppy-o\"></i> Ment</a><br /><br />";
										echo"<a href=\"javascript:deluploadedfile('".$modul."','".$tipus."','".$valamiid."','".$files[$i]['id']."'); \" class=\"btn btn-xs btn-danger\"><i class=\"fa fa-trash-o\"></i> Töröl</a>";
									}
                                ?>
                            </td>
                        </tr>
                        <?
					}	
                        ?>
                    
                    </tbody>
                </table>
                 <button onclick="showfilelist('<?=$modul;?>','<?=$tipus;?>','<?=$valamiid;?>')">Frissít</button>  
            </div>
        </div>
<?
	
	mysql_close($connid);	
?>
