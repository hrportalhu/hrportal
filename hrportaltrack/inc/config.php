<?php
/**
 * config.php
 *
 * Author: pixelcave
 *
 * Configuration file. It contains variables used in the template as well as the primary navigation array from which the navigation is created
 *
 */

/* Template variables */

$template = array(
    'name'          => 'HRPortal Admin rendszer',
    'version'       => '1.00',
    'author'        => 'planetstar',
    'robots'        => 'noindex, nofollow',
    'title'         => 'HRPortal Admin rendszer',
    'description'   => 'HRPortal Admin rendszer',
    'header_navbar' => 'navbar-inverse',
    'header'        => '',
    'sidebar'       => 'sidebar-partial sidebar-visible-lg sidebar-no-animations',
    'footer'       => 'footer-fixed',
    'main_style'    => 'style-alt',
    'theme'         => $_SESSION['planetsys']['design'],
    'header_content'=> '',
    'active_page'   => basename($_SERVER['PHP_SELF']),
    'workpage'   =>  $innerurl,
);
$template['page_preloader'] = true;
$primary_nav = array(
    array(
        'name'  => 'Összefoglaló oldal',
        'url'   => 'index.php',
        'icon'  => 'gi gi-home'
    ),
    array(
        'name'  => 'Alkalmazások',
        'opt'   => '<a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"><i class="gi gi-settings"></i></a>' .
                   '<a href="javascript:void(0)" data-toggle="tooltip" title="Create the most amazing pages with the widget kit!"><i class="gi gi-lightbulb"></i></a>',
        'url'   => 'header',
    )
);



$fomenuk=db_all("select * from ".$tableprefix."functions where parent='2' and status != '0' order by menuorder asc");
	for($i=0;$i<count($fomenuk);$i++){
		if(priv($fomenuk[$i]['funcname'],'')){
			$myfunc=array();
			$myfunc['name']=$fomenuk[$i]['name'];
			$myfunc['opt']= '<a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"><i class="gi gi-settings"></i></a>';
			$myfunc['icon']=$fomenuk[$i]['icon'];
			
			
			$funkcik=db_all("select * from ".$tableprefix."functions where parent='".$fomenuk[$i]['id']."' and status != '0' and menuname!='' order by menuorder");
			if(count($funkcik)>0){
				for($z=0;$z<count($funkcik);$z++){
					if(priv($funkcik[$z]['funcname'],'')){
					$subfuctions=db_all("select * from ".$tableprefix."functions where parent='".$funkcik[$z]['id']."' and status != '0' and subfunction_code='' order by menuorder");
					$myfunca=array();
					
						if(count($subfuctions)>0){
							
							$myfunca['name']=$funkcik[$z]['name'];	
							for($a=0;$a<count($subfuctions);$a++){
								if(priv($subfuctions[$a]['funcname'],'')){
									$mysubfunca=array();
									$mysubfunca['name']=$subfuctions[$a]['name']	;
									$mysubfunca['url']="/modules/".$fomenuk[$i]['menuname']."/".$funkcik[$z]['menuname']."/".$subfuctions[$a]['menuname'];
									$mysubfunca['icon']=$subfuctions[$a]['icon'];
									$myfunca['sub'][]=$mysubfunca;	
								}
							}
							$myfunca['icon']=$funkcik[$z]['icon'];
							 
						}
						else{
							$myfunca['name']=$funkcik[$z]['name'];
							$myfunca['url']= "/modules/".$fomenuk[$i]['menuname']."/".$funkcik[$z]['menuname'];
							$myfunca['icon']=$funkcik[$z]['icon'];
						}
						
						$myfunc['sub'][]=$myfunca;
					
				}
				}
			}
			$primary_nav[]=$myfunc;
		}
	}



