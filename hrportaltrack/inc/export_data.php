<?php
session_start();
include_once dirname(__FILE__).("/sys.conf");
include_once dirname(__FILE__).("/_functions.php");
$screen=$_SESSION['planetsys']['actpage'];
if($_REQUEST['format']=="csv"){
	header("Content-type: text/csv");
	header("Content-Disposition: attachment; filename=file.csv");
	header("Pragma: no-cache");
	header("Expires: 0");
	foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
		echo "\"".iconv("UTF-8","Windows-1252",$screen['elements'][$f]['textname'])."\",";
	}
	echo "\n";			
	for($i=0;$i<count($_SESSION['planetsys']['exportdata']['aaData']);$i++){
		for($k=0;$k<count($_SESSION['planetsys']['exportdata']['aaData'][$i]);$k++){
			echo "\"".iconv("UTF-8","Windows-1252",$_SESSION['planetsys']['exportdata']['aaData'][$i][$k])."\",";	
		}
		echo "\n";	
	}
}
elseif($_REQUEST['format']=="xls"){
	require_once('Excel/Worksheet.php');
	require_once('Excel/Workbook.php');
	@touch('adat.xls');
	HeaderingExcel('adat.xls');
	$workbook = new Workbook("-"); 
	$worksheet1 =& $workbook->add_worksheet('1. oldal');
 
	for($i=0;$i<count($_SESSION['planetsys']['exportdata']['aaData']);$i++){
		for($k=0;$k<count($_SESSION['planetsys']['exportdata']['aaData'][$i]);$k++){
			$name=htmlspecialchars_decode($_SESSION['planetsys']['exportdata']['aaData'][$i][$k]);
			$worksheet1->write($i, $k, mychardecode($name));
		}
	}
  $workbook->close();
}
elseif($_REQUEST['format']=="pdf"){
	require_once('tcpdf/config/lang/hun.php');
	require_once('tcpdf/tcpdf.php');
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('PlanetSYS');
	$pdf->SetTitle('export Table');
	$pdf->setPrintHeader(false);
	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	$pdf->setLanguageArray($l);
	$pdf->setFontSubsetting(true);
	$pdf->SetFont('freeserif', '', 10);
	$pdf->AddPage('L', 'A4');
	$html="";
	$html.="<h1>".$_REQUEST['title']."</h1>";
	$html.="<table><tr style=\"background-color:#ccccc;color:#000000;\">";
		foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
			$html.="<td>".$screen['elements'][$f]['textname']."</td>";
		}
		$html.="</tr>";
		for($i=0;$i<count($_SESSION['planetsys']['exportdata']['aaData']);$i++){
			$color="#ffffff";
			if($i/2==floor($i/2)){
				$color="#eeeeee";
			}
			$html.="<tr style=\"background-color:".$color."\">";			
			for($k=0;$k<count($_SESSION['planetsys']['exportdata']['aaData'][$i]);$k++){
				$html.="<td>".$_SESSION['planetsys']['exportdata']['aaData'][$i][$k]."</td>";	
			}
			$html.="</tr>";	
		}
	$html.="</table>";
	$pdf->writeHTML($html, true, 0, true, 0);
	$pdf->lastPage();
	$pdf->Output('pdfexport.pdf', 'D');
}
function mychardecode($ts){

$ts=utf8_decode($ts);
$ein = array("&#x00C9;","&Eacute;","&eacute;","&#x00E9;");
$eout  = array("É","É","é","é");
$ts = str_replace($ein, $eout, $ts);

$oin = array("&oacute;","Oacute;","&#x00F3;","&#x00D3;");
$oout  = array("ó","Ó","ó","Ó");
$ts = str_replace($oin, $oout, $ts);

$ooin = array("&#x00D6;","&#x00F6;","&Ouml;","ouml;");
$ooout  = array("Ö","ö","Ö","ö");
$ts = str_replace($ooin, $ooout, $ts);

$oein = array("&#x0151;","&#x0150;");
$oeout  = array("ő","Ő");
$ts = str_replace($oein, $oeout, $ts);

$uin = array("&#x00DC;","&#x00FC;","&#x00FA;","&#x00DA;","&uuml;");
$iout  = array("ü","Ü","ú","Ú","ú");
$ts = str_replace($uin, $uout, $ts);

$ain = array("&aacute;","&Aacute;","&#x00C1;","&#x00E1;");
$aout  = array("á","Á","Á","á");
$ts = str_replace($ain, $aout, $ts);

$iin= array
("&iacute;","&Iacute;");
$iout=array("í","Í;");
$ts = str_replace($iin, $iout, $ts);

return $ts;
}

  function HeaderingExcel($filename) {
      header("Content-type: application/vnd.ms-excel");
      header("Content-Disposition: attachment; filename=$filename" );
      header("Expires: 0");
      header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
      header("Pragma: public");
      }
mysql_close($connid);
?>
