
	<div id="login-container" class="animation-fadeIn">    
		<div class="login-title text-center">        
			<h1><i class="gi gi-shop"></i>&nbsp;&nbsp;<strong><?php echo $template['name']; ?></strong><br><small><strong>Bejelentkezés</strong> </small></h1>    
		</div>
    
    <div class="block remove-margin">        
		<div id="form-logina" class="form-horizontal form-bordered form-control-borderless">            
			<div class="form-group">                
				<div class="col-xs-12">                    
					<div class="input-group">                        
						<span class="input-group-addon"><i class="gi gi-envelope"></i></span>
						<?
						//	if(isset($_COOKIE['if_psys_hu_rememberme'])){
								
						//		echo"<input type=\"text\" id=\"unev\" name=\"unev\" class=\"form-control input-lg\" value=\"".$_COOKIE['if_psys_hu_rememberme']."\">";
						//	}
						//	else{
								echo"<input type=\"text\" id=\"unev\" name=\"unev\" class=\"form-control input-lg\" placeholder=\"login\">";	
						//	}
						?>                        
						
					</div>                
				</div>            
			</div>            
			<div class="form-group">                
				<div class="col-xs-12">                    
					<div class="input-group">                        
						<span class="input-group-addon"><i class="gi gi-asterisk"></i></span>                        
						<input type="password" id="jels" name="jels" class="form-control input-lg" placeholder="Jelszó" >                    
					</div>                
				</div>            
			</div>            
			<div class="form-group form-actions">                
				<div class="col-xs-4">                    
					<label class="switch switch-primary" data-toggle="tooltip" title="Megjegyez?">                        
						<input type="checkbox" id="remember" name="remember" checked>                        
						<span></span>                    
					</label>                
				</div>                
				<div class="col-xs-8 text-right">                    
					<button id="submitbutton" class="btn btn-sm btn-primary" ><i class="fa fa-angle-right"></i> Belépés</button>                
				</div>
			</div>		
		</div>    
	</div>
</div>
     <?php include 'inc/template_scripts.php'; ?>
     <script src="js/pages/login.js"></script><script>$(function(){ Login.init(); });</script>

