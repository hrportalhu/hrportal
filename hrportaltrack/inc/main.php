<?php include 'inc/page_head.php'; ?>
<?

	//$szoveg=db_one("select name from texts order by RAND() limit 1");	
	$quickfunc=db_all("select * from functions where featured='1'");
	$szoveg=getbullshit();
	
?>
  <!-- Page content -->
                    <div id="page-content">
                        <!-- Dashboard Header -->
                        <!-- For an image header add the class 'content-header-media' and an image as in the following example -->
                        <div class="content-header content-header-media">
                            <div class="header-section">
                                <div class="row">
                                    <!-- Main Title (hidden on small devices for the statistics to fit) -->
                                   <div class="col-md-4 col-lg-6 hidden-xs hidden-sm">
										<h1>Helló <strong><?=$_SESSION['planetsys']['realname'];?></strong><br>Ime egy lélekemelő:<br /><small><? echo $szoveg;?> !</small></h1>
									</div>
                                    <!-- END Main Title -->

                                    <!-- Top Stats -->
                                    <div class="col-md-8 col-lg-6">
                                        <div class="row text-center">
                                            <div class="col-xs-4 col-sm-3">
                                                <h2 class="animation-hatch">
                                                    <strong><?=getcurrency();?> HUF</strong><br>
                                                    <small><i class="fa fa-thumbs-o-up"></i>1 EUR</small>
                                                </h2>
                                            </div>
                                            <div class="col-xs-4 col-sm-3">
                                                <h2 class="animation-hatch">
                                                    <strong>16</strong><br>
                                                    <small><i class="fa fa-heart-o"></i>Új megrendelés</small>
                                                </h2>
                                            </div>
                                            <div class="col-xs-4 col-sm-3">
                                                <h2 class="animation-hatch">
                                                    <strong>13</strong><br>
                                                    <small><i class="fa fa-calendar-o"></i> Esemény</small>
                                                </h2>
                                            </div>
                                            <!-- We hide the last stat to fit the other 3 on small devices -->
                                            <div class="col-sm-3 hidden-xs">
                                                <h2 class="animation-hatch">
                                                    <strong>7&deg; C</strong><br>
                                                    <small><i class="fa fa-map-marker"></i> Szeged</small>
                                                </h2>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Top Stats -->
                                </div>
                            </div>
                            <!-- For best results use an image with a resolution of 2560x248 pixels (You can also use a blurred image with ratio 10:1 - eg: 1000x100 pixels - it will adjust and look great!) -->
                            <img src="img/placeholders/headers/dashboard_header.jpg" alt="header image" class="animation-pulseSlow">
                        </div>
                        <!-- END Dashboard Header -->

                      

    
    <div class="row">
        <div class="col-sm-6">
            <!-- Classic Chart Block -->
            <div class="block full">
                <!-- Classic Chart Title -->
                <div class="block-title">
                    <h2><strong>Készlet </strong> felhasználás</h2>
                </div>
                <!-- END Classic Chart Title -->

                <!-- Classic Chart Content -->
                <!-- Flot Charts (initialized in js/pages/compCharts.js), for more examples you can check out http://www.flotcharts.org/ -->
                <div id="chart-classic" class="chart"></div>
                <!-- END Classic Chart Content -->
            </div>
            <!-- END Classic Chart Block -->
        </div>
        <div class="col-sm-6">
            <!-- Bars Chart Block -->
            <div class="block full">
                <!-- Bars Chart Title -->
                <div class="block-title">
                    <h2><strong>Megrendelés</strong> Állomány</h2>
                </div>
                <!-- END Bars Chart Title -->

                <!-- Bars Chart Content -->
                <!-- Flot Charts (initialized in js/pages/compCharts.js), for more examples you can check out http://www.flotcharts.org/ -->
                <div id="chart-bars" class="chart"></div>
                <!-- END Bars Chart Content -->
            </div>
            <!-- END Bars Chart Block -->
        </div>
    </div>
    <!-- END Classic and Bars Chart -->


    <!-- Pie and Stacked Chart -->
    <div class="row">
        <div class="col-sm-6">
            <!-- Pie Chart Block -->
            <div class="block full">
                <!-- Pie Chart Title -->
                <div class="block-title">
                    <h2><strong>Megrendelés</strong> eloszlás</h2>
                </div>
                <!-- END Pie Title -->

                <!-- Pie Chart Content -->
                <div id="chart-pie" class="chart"></div>
                <!-- END Pie Chart Content -->
            </div>
            <!-- END Pie Chart Block -->
        </div>
        <div class="col-sm-6">
            <!-- Stacked Chart Block -->
            <div class="block full">
                <!-- Stacked Chart Title -->
                <div class="block-title">
                    <h2><strong>Kiadás</strong> eloszlás</h2>
                </div>
                <!-- END Stacked Chart Title -->

                <!-- Stacked Chart Content -->
                <!-- Flot Charts (initialized in js/pages/compCharts.js), for more examples you can check out http://www.flotcharts.org/ -->
                <div id="chart-stacked" class="chart"></div>
                <!-- END Stacked Chart Content -->
            </div>
            <!-- END Stacked Chart Block -->
        </div>
    </div>
  
    <!-- Live Chart Block -->
    <div class="block full">
        <!-- Bars Chart Title -->
        <div class="block-title">
            <div class="block-options pull-right">
                <span id="chart-live-info" class="label label-primary">%</span>
                <span class="label label-danger animation-pulse">Élő..</span>
            </div>
            <h2><strong>Aktivitás</strong> </h2>
        </div>
        <!-- END Bars Chart Title -->

        <!-- Bars Chart Content -->
        <!-- Flot Charts (initialized in js/pages/compCharts.js), for more examples you can check out http://www.flotcharts.org/ -->
        <div id="chart-live" class="chart"></div>
        <!-- END Bars Chart Content -->
    </div>
    <!-- END Live Chart Block -->                      
                        
                    </div>
                    <!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>

<!-- Remember to include excanvas for IE8 chart support -->
<!--[if IE 8]><script src="js/helpers/excanvas.min.js"></script><![endif]-->

<?php include 'inc/template_scripts.php'; ?>
        <!-- Google Maps API + Gmaps Plugin, must be loaded in the page you would like to use maps (Remove 'http:' if you have SSL) -->

<script src="js/pages/compCharts.js"></script>
<script>$(function(){ CompCharts.init(); });</script>
