<?php
/**
 * page_head.php
 *
 * Author: pixelcave
 *
 * Header and Sidebar of each page
 *
 */
?>
<!-- Page Wrapper -->
<!-- In the PHP version you can set the following options from inc/config file -->
<!--
    Available classes:

    'page-loading'      enables page preloader
-->
<div id="page-wrapper"<?php if ($template['page_preloader']) { echo ' class="page-loading"'; } ?>>
    <!-- Preloader -->
    <!-- Preloader functionality (initialized in js/app.js) - pageLoading() -->
    <!-- Used only if page preloader is enabled from inc/config (PHP version) or the class 'page-loading' is added in #page-wrapper element (HTML version) -->
    <div class="preloader themed-background-dark">
        <h1 class="push-top-bottom text-center" style="color:#fff;"><strong>MG-</strong>System</h1>
        <div class="inner">
            <h3 class="text-light visible-lt-ie9 visible-lt-ie10" style="color:#fff;" ><strong>A kért oldal felépítése folyamataban van...</strong></h3>
            <div class="preloader-spinner hidden-lt-ie9 hidden-lt-ie10"></div>
        </div>
    </div>
    <!-- END Preloader -->
<!-- Page Container -->
<!-- In the PHP version you can set the following options from inc/config.php file -->
<!--
    Available #page-container classes:

    '' (None)                                       for a full main and alternative sidebar hidden by default (> 991px)

    'sidebar-visible-lg'                            for a full main sidebar visible by default (> 991px)
    'sidebar-partial'                               for a partial main sidebar which opens on mouse hover, hidden by default (> 991px)
    'sidebar-partial sidebar-visible-lg'            for a partial main sidebar which opens on mouse hover, visible by default (> 991px)

    'sidebar-alt-visible-lg'                        for a full alternative sidebar visible by default (> 991px)
    'sidebar-alt-partial'                           for a partial alternative sidebar which opens on mouse hover, hidden by default (> 991px)
    'sidebar-alt-partial sidebar-alt-visible-lg'    for a partial alternative sidebar which opens on mouse hover, visible by default (> 991px)

    'sidebar-partial sidebar-alt-partial'           for both sidebars partial which open on mouse hover, hidden by default (> 991px)

    'sidebar-no-animations'                         add this as extra for disabling sidebar animations on large screens (> 991px) - Better performance with heavy pages!

    'style-alt'                                     for an alternative main style (without it: the default style)
    'footer-fixed'                                  for a fixed footer (without it: a static footer)

    'header-fixed-top'                              has to be added only if the class 'navbar-fixed-top' was added on header.navbar
    'header-fixed-bottom'                           has to be added only if the class 'navbar-fixed-bottom' was added on header.navbar
-->
<?php
    $page_classes = '';

    if ($template['header'] == 'navbar-fixed-top') {
        $page_classes = 'header-fixed-top';
    } else if ($template['header'] == 'navbar-fixed-bottom') {
        $page_classes = 'header-fixed-bottom';
    }

    if ($template['sidebar']) {
        $page_classes .= (($page_classes == '') ? '' : ' ') . $template['sidebar'];
    }

    if ($template['main_style'] == 'style-alt')  {
        $page_classes .= (($page_classes == '') ? '' : ' ') . 'style-alt';
    }

    if ($template['footer'] == 'footer-fixed')  {
        $page_classes .= (($page_classes == '') ? '' : ' ') . 'footer-fixed';
    }
?>
<div id="page-container"<?php if ($page_classes) { echo ' class="' . $page_classes . '"'; } ?> >

            

    <!-- Main Sidebar -->
    <div id="sidebar">
        <!-- Wrapper for scrolling functionality -->
        <div class="sidebar-scroll">
            <!-- Sidebar Content -->
            <div class="sidebar-content">
                <!-- Brand -->
                <a href="index.php" class="sidebar-brand">
                    <i class="gi gi-pen"></i><strong>HR</strong>Portal.hu
                </a>
                <!-- END Brand -->

                <!-- User Info -->
                <div class="sidebar-section sidebar-user clearfix">
                    <div class="sidebar-user-avatar">
                        <a href="/system/mysetup">
                            <?
								if(is_file("modules/system/system_files/".$_SESSION['planetsys']['user_id']."/userfile.jpg")){
									echo"<img src=\"/modules/system/system_files/".$_SESSION['planetsys']['user_id']."/userfile.jpg\" alt=\"avatar\">";
								}
								else{
									echo"<img src=\"img/placeholders/avatars/avatar.jpg\" alt=\"avatar\">";
								}
                            ?>
                        </a>
                    </div>
                    <div class="sidebar-user-name"><?=$_SESSION['planetsys']['realname'];?></div>
                    <div class="sidebar-user-links">
                        <a href="/modules/core/mysetup" data-toggle="tooltip" data-placement="bottom" title="Profile"><i class="gi gi-user"></i></a>
                        <a href="page_ready_inbox.php" data-toggle="tooltip" data-placement="bottom" title="Messages"><i class="gi gi-envelope"></i></a>
                        
                        <a href="/logout/yes" data-toggle="tooltip" data-placement="bottom" title="Logout"><i class="gi gi-exit"></i></a>
                    </div>
                </div>
                <!-- END User Info -->

 <!-- Theme Colors -->
                    <!-- Change Color Theme functionality can be found in js/app.js - templateOptions() -->
                    <ul class="sidebar-section sidebar-themes clearfix sidebar-nav-mini-hide">
                        <!-- You can also add the default color theme-->
                        <li  class="active">
                            <a href="javascript:setdesign('default');" class="themed-background-dark-default themed-border-default" data-theme="default" data-toggle="tooltip" title="Default Blue"></a>
                        </li>
                        
                        <li>
                            <a href="javascript:setdesign('night');" class="themed-background-dark-night themed-border-night" data-theme="css/themes/night.css" data-toggle="tooltip" title="Night"></a>
                        </li>
                        <li>
                            <a href="javascript:setdesign('amethyst');" class="themed-background-dark-amethyst themed-border-amethyst" data-theme="css/themes/amethyst.css" data-toggle="tooltip" title="Amethyst"></a>
                        </li>
                        <li>
                            <a href="javascript:setdesign('modern');" class="themed-background-dark-modern themed-border-modern" data-theme="css/themes/modern.css" data-toggle="tooltip" title="Modern"></a>
                        </li>
                        <li>
                            <a href="javascript:setdesign('autumn');" class="themed-background-dark-autumn themed-border-autumn" data-theme="css/themes/autumn.css" data-toggle="tooltip" title="Autumn"></a>
                        </li>
                        <!--<li>
                            <a href="javascript:void(0)" class="themed-background-dark-flatie themed-border-flatie" data-theme="css/themes/flatie.css" data-toggle="tooltip" title="Flatie"></a>
                        </li>-->
                        <li>
                            <a href="javascript:setdesign('spring');" class="themed-background-dark-spring themed-border-spring" data-theme="css/themes/spring.css" data-toggle="tooltip" title="Spring"></a>
                        </li>
                        <li>
                            <a href="javascript:setdesign('fancy');" class="themed-background-dark-fancy themed-border-fancy" data-theme="css/themes/fancy.css" data-toggle="tooltip" title="Fancy"></a>
                        </li>
                        <li>
                            <a href="javascript:setdesign('fire');" class="themed-background-dark-fire themed-border-fire" data-theme="css/themes/fire.css" data-toggle="tooltip" title="Fire"></a>
                        </li>
                        <li>
                            <a href="javascript:setdesign('coral');" class="themed-background-dark-coral themed-border-coral" data-theme="css/themes/coral.css" data-toggle="tooltip" title="Coral"></a>
                        </li>
                        <li>
                            <a href="javascript:setdesign('lake');" class="themed-background-dark-lake themed-border-lake" data-theme="css/themes/lake.css" data-toggle="tooltip" title="Lake"></a>
                        </li>
                        <li>
                            <a href="javascript:setdesign('forest');" class="themed-background-dark-forest themed-border-forest" data-theme="css/themes/forest.css" data-toggle="tooltip" title="Forest"></a>
                        </li>
                        <li>
                            <a href="javascript:setdesign('waterlily');" class="themed-background-dark-waterlily themed-border-waterlily" data-theme="css/themes/waterlily.css" data-toggle="tooltip" title="Waterlily"></a>
                        </li>
                        <li>
                            <a href="javascript:setdesign('emerald');" class="themed-background-dark-emerald themed-border-emerald" data-theme="css/themes/emerald.css" data-toggle="tooltip" title="Emerald"></a>
                        </li>
                        <li>
                            <a href="javascript:setdesign('blackberry');" class="themed-background-dark-blackberry themed-border-blackberry" data-theme="css/themes/blackberry.css" data-toggle="tooltip" title="Blackberry"></a>
                        </li>
                    </ul>
                    <!-- END Theme Colors -->
				<?
					
				?>
                <?php if ($primary_nav) { ?>
                <!-- Sidebar Navigation -->
                <ul class="sidebar-nav">
                    <?php foreach( $primary_nav as $key => $link ) {
                        $link_class = '';
                        $li_active  = '';
                        $menu_link  = '';

                        // Get 1st level link's vital info
                        $url        = (isset($link['url']) && $link['url']) ? $link['url'] : '#';
                        $active     = (isset($link['url']) && ($template['workpage'] == $link['url'])) ? ' active' : '';
                        $icon       = (isset($link['icon']) && $link['icon']) ? '<i class="' . $link['icon'] . ' sidebar-nav-icon"></i>' : '';

                        // Check if the link has a submenu
                        if (isset($link['sub']) && $link['sub']) {
                            // Since it has a submenu, we need to check if we have to add the class active
                            // to its parent li element (only if a 2nd or 3rd level link is active)
                            foreach ($link['sub'] as $sub_link) {
                                if (in_array("/".$template['workpage'], $sub_link)) {
                                    $li_active = ' class="active"';
                                    break;
                                }

                                // 3rd level links
                                if (isset($sub_link['sub']) && $sub_link['sub']) {
                                    foreach ($sub_link['sub'] as $sub2_link) {
                                        if (in_array("/".$template['workpage'], $sub2_link)) {
                                            $li_active = ' class="active"';
                                            break;
                                        }
                                    }
                                }
                            }

                            $menu_link = 'sidebar-nav-menu';
                        }

                        // Create the class attribute for our link
                        if ($menu_link || $active) {
                            $link_class = ' class="'. $menu_link . $active .'"';
                        }
                    ?>
                    <?php if ($url == 'header') { // if it is a header and not a link ?>
                    <li class="sidebar-header">
                        <?php if (isset($link['opt']) && $link['opt']) { // If the header has options set ?>
                        <span class="sidebar-header-options clearfix"><?php echo $link['opt']; ?></span>
                        <?php } ?>
                        <span class="sidebar-header-title"><?php echo $link['name']; ?></span>
                    </li>
                    <?php } else { // If it is a link ?>
                    <li<?php echo $li_active; ?>>
                        <a href="<?php echo $url; ?>"<?php echo $link_class; ?>><?php if (isset($link['sub']) && $link['sub']) { // if the link has a submenu ?><i class="fa fa-angle-left sidebar-nav-indicator"></i><?php } echo $icon . $link['name']; ?></a>
                        <?php if (isset($link['sub']) && $link['sub']) { // if the link has a submenu ?>
                        <ul>
                            <?php foreach ($link['sub'] as $sub_link) {
								
                                $link_class = '';
                                $li_active = '';
                                $submenu_link = '';

                                // Get 2nd level link's vital info
                                $url        = (isset($sub_link['url']) && $sub_link['url']) ? $sub_link['url'] : '#';
                                $active     = (isset($sub_link['url']) && ("/".$template['workpage'] == $sub_link['url'])) ? ' active' : '';
								$icon       = (isset($sub_link['icon']) && $sub_link['icon']) ? '<i class="' . $sub_link['icon'] . ' sidebar-nav-icon"></i>' : '';
                                // Check if the link has a submenu
                                
                                if (isset($sub_link['sub']) && $sub_link['sub']) {
                                    // Since it has a submenu, we need to check if we have to add the class active
                                    // to its parent li element (only if a 3rd level link is active)
                                   // echo"<pre>";
                                   // print_r($sub_link);
                                    //echo"</pre>";
                                    for($a=0;$a<count($sub_link['sub']);$a++){
										
										if($sub_link['sub'][$a]['url']=="/".$template['workpage']){
											$li_active = ' class="active"';
                                            break;
										}
									}
                                     $submenu_link = 'sidebar-nav-submenu';
                                }
								if ($submenu_link || $active) {
                                    $link_class = ' class="'. $submenu_link . $active .'"';
                                }
                             ?>
                            <li<?php echo $li_active; ?>>
                                <a href="<?php echo $url; ?>"<?php echo $link_class; ?>><?php if (isset($sub_link['sub']) && $sub_link['sub']) { ?><i class="fa fa-angle-left sidebar-nav-indicator"></i><?php } echo $icon.$sub_link['name']; ?></a>
                                 <?php if (isset($sub_link['sub']) && $sub_link['sub']) { ?>
                                    <ul>
                                        <?php foreach ($sub_link['sub'] as $sub2_link) {
                                            // Get 3rd level link's vital info
                                            $url    = (isset($sub2_link['url']) && $sub2_link['url']) ? $sub2_link['url'] : '#';
                                            $active = (isset($sub2_link['url']) && ("/".$template['workpage'] == $sub2_link['url'])) ? ' class="active"' : '';
                                        ?>
                                        <li <?php echo $li_active; ?>>
                                       
                                            <a href="<?php echo $url; ?>"<?php echo $active ?>><?php echo $sub2_link['name']; ?></a>
                                        </li>
                                        <?php } ?>
                                    </ul>
                                <?php } ?>
                            </li>
                            <?php } ?>
                        </ul>
                        <?php } ?>
                    </li>
                    <?php } ?>
                    <?php } ?>
                </ul>
                <!-- END Sidebar Navigation -->
                <?php } ?>

                <!-- Sidebar Notifications -->
                <div class="sidebar-header">
                    <span class="sidebar-header-options clearfix">
                        <a href="javascript:getloggerdata();" data-toggle="tooltip" title="Frissítés"><i class="gi gi-refresh"></i></a>
                    </span>
                    <span class="sidebar-header-title">Aktivitás</span>
                </div>
                <div class="sidebar-section" id="activity-list"></div>
                <!-- END Sidebar Notifications -->
            </div>
            <!-- END Sidebar Content -->
        </div>
        <!-- END Wrapper for scrolling functionality -->
    </div>
    <!-- END Main Sidebar -->

    <!-- Main Container -->
    <div id="main-container">

        <header class="navbar<?php if ($template['header_navbar']) { echo ' ' . $template['header_navbar']; } ?><?php if ($template['header']) { echo ' '. $template['header']; } ?>">
            <?php if ( $template['header_content'] == 'horizontal-menu' ) { // Horizontal Menu Header Content ?>
            <!-- Navbar Header -->
            <div class="navbar-header">
                <!-- Horizontal Menu Toggle + Alternative Sidebar Toggle Button, Visible only in small screens (< 768px) -->
                <ul class="nav navbar-nav-custom pull-right visible-xs">
                    <li>
                        <a href="javascript:void(0)" data-toggle="collapse" data-target="#horizontal-menu-collapse">Menu</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar-alt');">
                            <i class="gi gi-share_alt"></i>
                            <span class="label label-primary label-indicator animation-floating">4</span>
                        </a>
                    </li>
                </ul>
                <!-- END Horizontal Menu Toggle + Alternative Sidebar Toggle Button -->

                <!-- Main Sidebar Toggle Button -->
                <ul class="nav navbar-nav-custom">
                    <li>
                        <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');">
                            <i class="fa fa-bars fa-fw"></i>
                        </a>
                    </li>
                </ul>
                <!-- END Main Sidebar Toggle Button -->
            </div>
            <!-- END Navbar Header -->

            <!-- Alternative Sidebar Toggle Button, Visible only in large screens (> 767px) -->
            <ul class="nav navbar-nav-custom pull-right hidden-xs">
                <li>
                    <!-- If you do not want the main sidebar to open when the alternative sidebar is closed, just remove the second parameter: App.sidebar('toggle-sidebar-alt'); -->
                    <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar-alt', 'toggle-other');">
                        <i class="gi gi-share_alt"></i>
                        <span class="label label-primary label-indicator animation-floating">4</span>
                    </a>
                </li>
            </ul>
            <!-- END Alternative Sidebar Toggle Button -->

           
            <?php } else { // Default Header Content  ?>
            <!-- Left Header Navigation -->
            <ul class="nav navbar-nav-custom">
                <!-- Main Sidebar Toggle Button -->
                <li>
                    <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');">
                        <i class="fa fa-bars fa-fw"></i>
                    </a>
                </li>
                <!-- END Main Sidebar Toggle Button -->

                <!-- Template Options -->
                <!-- Change Options functionality can be found in js/app.js - templateOptions() -->
                <li class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="gi gi-settings"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-custom dropdown-options">
                        <li class="dropdown-header text-center">Fejléc stílus</li>
                        <li>
                            <div class="btn-group btn-group-justified btn-group-sm">
                                <a href="javascript:void(0)" class="btn btn-primary" id="options-header-default">Világos</a>
                                <a href="javascript:void(0)" class="btn btn-primary" id="options-header-inverse">Sötés</a>
                            </div>
                        </li>
                        <li class="dropdown-header text-center">Oldal stílus</li>
                        <li>
                            <div class="btn-group btn-group-justified btn-group-sm">
                                <a href="javascript:void(0)" class="btn btn-primary" id="options-main-style">Alap</a>
                                <a href="javascript:void(0)" class="btn btn-primary" id="options-main-style-alt">Alternativ</a>
                            </div>
                        </li>
                        <li class="dropdown-header text-center">Oldal elrendezés</li>
                        <li>
                            <button class="btn btn-sm btn-block btn-primary" id="options-header-top">Fix Oldal/fejléc (Fent)</button>
                            <button class="btn btn-sm btn-block btn-primary" id="options-header-bottom">Fix Oldal/fejléc (Lent)</button>
                        </li>
                        <li class="dropdown-header text-center">Lábléc</li>
                        <li>
                            <div class="btn-group btn-group-justified btn-group-sm">
                                <a href="javascript:void(0)" class="btn btn-primary" id="options-footer-static">Alap</a>
                                <a href="javascript:void(0)" class="btn btn-primary" id="options-footer-fixed">Fix</a>
                            </div>
                        </li>
                    </ul>
                </li>

                <li class="dropdown">
					<a href="#modal-regular-help" data-toggle="modal">
						<i class="gi gi-circle_question_mark"></i>
					</a>
				</li>
                <?
               // echo $template['workpage']."_headers.php";
              
				$temppage=$template['workpage'];
				if($template['workpage']!="modules/docusystem/documents"){
					$temppage=str_replace("system","inc", $temppage);
				}
				
				if(is_file($temppage."_header.php")){
					include($temppage."_header.php");
				}
				elseif($template['workpage']=="modules/sales/desks"){
                ?>  
					<li class="dropdown">
						<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
							<i class="gi gi-cogwheels"></i>
						</a>
						<ul class="dropdown-menu dropdown-custom dropdown-options">
							<li class="dropdown-header text-center">Beállítások</li>
							<li>
								<form name="setupform" id="setupform">
								
								<?
								$lq="select * from listoptions where user_id='".$_SESSION['planetsys']['user_id']."' and modulname='".$_SESSION['planetsys']['actpage']['modulname']."' and funcname='".$_SESSION['planetsys']['actpage']['funcname']."'";
								$listoptions=db_row($lq);
								$listelemets=explode(",",$listoptions['screenlist']);
								foreach($_SESSION['planetsys']['actpage']['elements'] as $record){
									if(in_array($record['name'],$listelemets)){	$ch=" checked=\"checked\" ";}else{$ch=" ";}
									echo"<div ><input type=\"checkbox\" id=\"AAA_".$record['name']."\" name=\"AAA_".$record['name']."\" ".$ch." onclick=\"javascript:savesetupscreen();\" />&nbsp;".$record['textname']."</div>";
								}
								?>
								<div class="clear"></div>
								</form>
							
		
							</li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="/<?=$template['workpage'];?>/new " >
							<i class="gi gi-bomb" title="New"></i>
						</a>	
					</li>
                <?
				}
				elseif($template['workpage']=="modules/sales/sales"){
                ?>  
					<li class="dropdown">
						<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
							<i class="gi gi-cogwheels"></i>
						</a>
						<ul class="dropdown-menu dropdown-custom dropdown-options">
							<li class="dropdown-header text-center">Beállítások</li>
							<li>
								<form name="setupform" id="setupform">
								
								<?
								$lq="select * from listoptions where user_id='".$_SESSION['planetsys']['user_id']."' and modulname='".$_SESSION['planetsys']['actpage']['modulname']."' and funcname='".$_SESSION['planetsys']['actpage']['funcname']."'";
								$listoptions=db_row($lq);
								$listelemets=explode(",",$listoptions['screenlist']);
								foreach($_SESSION['planetsys']['actpage']['elements'] as $record){
									if(in_array($record['name'],$listelemets)){	$ch=" checked=\"checked\" ";}else{$ch=" ";}
									echo"<div ><input type=\"checkbox\" id=\"AAA_".$record['name']."\" name=\"AAA_".$record['name']."\" ".$ch." onclick=\"javascript:savesetupscreen();\" />&nbsp;".$record['textname']."</div>";
								}
								?>
								<div class="clear"></div>
								</form>
							
		
							</li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="/<?=$template['workpage'];?>/new " >
							<i class="gi gi-bomb" title="New"></i>
						</a>	
					</li>
                <?
				}
                ?>
                <!-- END Template Options -->
            </ul>
            <!-- END Left Header Navigation -->
	<?
			echo"<div id=\"modal-regular-export\" class=\"modal\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">
					<div class=\"modal-dialog\">
						<div class=\"modal-content\">
							<div class=\"modal-header\">
								<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
								<h3 class=\"modal-title\">Exportálás</h3>
							</div>
							<div class=\"modal-body\">";
								
							echo"<div style=\"width:140px;float:left;\">Formátum:</div>";
								echo"<div style=\"width:400px;float:left;\"><select id=\"format\">";
									echo"<option value=\"csv\">CSV</option>";
									echo"<option value=\"pdf\">PDF</option>";
									echo"<option value=\"xls\">XLS</option>";
									echo"<option value=\"xml\">XML</option>";
								echo"</select></div>";
								echo"<div style=\"clear:both;height:1px;\"></div>";
								echo"<div style=\"width:140px;float:left;\">Fejléc cím /Csak PDF/:</div>";
								echo"<div style=\"width:400px;float:left;\"><input type=\"text\" id=\"title\" value=\"\" /></div>";

																	
							echo"</div>
							<div class=\"modal-footer\">
								<button type=\"button\" class=\"btn btn-sm btn-warning\" onclick=\"javascript:exportdata()\" >Exportál</button>
								<button type=\"button\" class=\"btn btn-sm btn-default\" data-dismiss=\"modal\">Bezár</button>
							</div>
						</div>
					</div>
				</div>";	
							
				echo"<div id=\"modal-regular-help\" class=\"modal\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">
					<div class=\"modal-dialog\">
						<div class=\"modal-content\">
							<div class=\"modal-header\">
								<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
								<h3 class=\"modal-title\">Segítségnyújtó</h3>
							</div>
							<div class=\"modal-body\">";
							$tt=str_replace("/","_",$temppage);
							$getcontent=db_one("select tartalom from content where otherazon='".$tt."'");
							if($getcontent!=""){
								echo $getcontent;
							}
							else{
								echo"<p>Ehez az oldalhoz nem tartozik sugó!</p>";	
							}
							
																	
							echo"</div>
							<div class=\"modal-footer\">
								
								<button type=\"button\" class=\"btn btn-sm btn-default\" data-dismiss=\"modal\">Bezár</button>
							</div>
						</div>
					</div>
				</div>";	
							
							
							
			
			echo"<div id=\"modal-regular-print\" class=\"modal\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\" >
					<div class=\"modal-dialog\" style=\"width:1000px;\">
						<div class=\"modal-content\">
							<div class=\"modal-header\">
								<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
								<h3 class=\"modal-title\">Nyomtatás</h3>
							</div>
							<div class=\"modal-body\" id=\"printdiv\" style=\"height:700px;overflow:auto;\"></div>
							<div class=\"modal-footer\">
								<button type=\"button\" class=\"btn btn-sm btn-warning\" onclick=\"javascript:printdata('printdiv');\" >Nyomtat</button>
								<button type=\"button\" class=\"btn btn-sm btn-default\" data-dismiss=\"modal\">Bezár</button>
							</div>
						</div>
					</div>
				</div>";	
							
							
							?>


            <?php } ?>
        </header>
        <!-- END Header -->

                 
