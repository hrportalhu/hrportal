<?php
session_start();
include_once dirname(__FILE__).("/sys.conf");
include_once dirname(__FILE__).("/_functions.php");
$printscreen=$_SESSION['planetsys']['printscreen'];


	require_once('tcpdf/config/lang/hun.php');
	require_once('tcpdf/tcpdf.php');
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('PlanetSYS');
	$pdf->SetTitle('export Table');
	$pdf->setPrintHeader(false);
	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	$pdf->setLanguageArray($l);
	$pdf->setFontSubsetting(true);
	$pdf->SetFont('freeserif', '', 10);
	$pdf->AddPage('P', 'A4');
	$html=$printscreen;
	
	$pdf->writeHTML($html, true, 0, true, 0);
	$pdf->lastPage();
	$pdf->Output('pdfexport.pdf', 'D');


mysql_close($connid);
?>
