<!DOCTYPE html>
<!--[if IE 8]> 
       <html class="no-js lt-ie9"> 
 <![endif]-->
 <!--[if gt IE 8]><!--> 
	<html class="no-js"> 
<!--<![endif]-->    
<head>        
	<meta charset="utf-8">        
	<base href="<?=$ServerName;?>"> 
	<title><?php echo $template['title'] ?></title>        
	<meta name="description" content="<?php echo $template['description'] ?>">		
	<meta name="author" content="<?php echo $template['author'] ?>">        
	<meta name="robots" content="<?php echo $template['robots'] ?>">        
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">        
	<link rel="shortcut icon" href="img/favicon.ico">        
	<link rel="apple-touch-icon" href="img/icon57.png" sizes="57x57">        
	<link rel="apple-touch-icon" href="img/icon72.png" sizes="72x72">        
	<link rel="apple-touch-icon" href="img/icon76.png" sizes="76x76">        
	<link rel="apple-touch-icon" href="img/icon114.png" sizes="114x114">        
	<link rel="apple-touch-icon" href="img/icon120.png" sizes="120x120">        
	<link rel="apple-touch-icon" href="img/icon144.png" sizes="144x144">        
	<link rel="apple-touch-icon" href="img/icon152.png" sizes="152x152">   
	<link rel="stylesheet" type="text/css" href="css/clock_styles.css" />
	<link rel="stylesheet" type="text/css" href="js/jquery.tzineClock/jquery.tzineClock.css" />     
	<link rel="stylesheet" href="css/jquery-ui.min.css">        
	<link rel="stylesheet" href="css/bootstrap.min.css">        
	<link rel="stylesheet" href="css/plugins.css">        
	<link rel="stylesheet" href="css/main.css">		
	<?php if ($template['theme']) { ?>
		<link id="theme-link" rel="stylesheet" href="css/themes/<?php echo $template['theme']; ?>.css"><?php } 
	?>        
	<link rel="stylesheet" href="css/themes.css">        
	<script src="js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
    <script type="text/javascript">	var ServerName="<?=$ServerName;?>";	</script>
		
 
          
        </head>    
		<body style="background-color:transparent;margin:0px;padding:5px;">
   

   <div id="infoscreen"></div>
    <div id="sysmsg"></div>
