var App = function() {
    var page, pageContent, header, footer, sidebar, sidebarAlt, sScroll;
    var uiInit = function() {
        page            = $('#page-container');
        pageContent     = $('#page-content');
        header          = $('header');
        footer          = $('#page-content + footer');
        sidebar         = $('#sidebar');
        sidebarAlt      = $('#sidebar-alt');
        sScroll         = $('.sidebar-scroll');
        var yearCopy = $('#year-copy'), d = new Date();
        if (d.getFullYear() === 2014) { yearCopy.html('2014'); } else { yearCopy.html('2014-' + d.getFullYear().toString().substr(2,2)); }
        $('[data-toggle="tabs"] a, .enable-tabs a').click(function(e){ e.preventDefault(); $(this).tab('show'); });
        $('[data-toggle="tooltip"], .enable-tooltip').tooltip({container: 'body', animation: false});
        $('[data-toggle="popover"], .enable-popover').popover({container: 'body', animation: true});
        $('[data-toggle="lightbox-image"]').magnificPopup({type: 'image', image: {titleSrc: 'title'}});
        $('.input-datepicker, .input-daterange').datepicker({
			weekStart: 1,
			closeText: 'bezárás',
			prevText: '&laquo;&nbsp;vissza',
			nextText: 'előre&nbsp;&raquo;',
			currentText: 'ma',
			monthNames: ['Január', 'Február', 'Március', 'Április', 'Május', 'Június',
			'Július', 'Augusztus', 'Szeptember', 'Október', 'November', 'December'],
			monthNamesShort: ['Jan', 'Feb', 'Már', 'Ápr', 'Máj', 'Jún',
			'Júl', 'Aug', 'Szep', 'Okt', 'Nov', 'Dec'],
			dayNames: ['Vasárnap', 'Hétfö', 'Kedd', 'Szerda', 'Csütörtök', 'Péntek', 'Szombat'],
			dayNamesShort: ['Vas', 'Hét', 'Ked', 'Sze', 'Csü', 'Pén', 'Szo'],
			dayNamesMin: ['V', 'H', 'K', 'Sze', 'Cs', 'P', 'Szo'],
			weekHeader: 'Hé',
			changeYear: 1,
			dateFormat: 'yy-mm-dd',
			firstDay: 1,
			isRTL: false,
			showMonthAfterYear: false,
			yearSuffix: ''
		});
        $('.input-datepicker-close').datepicker({weekStart: 1}).on('changeDate', function(e){ $(this).datepicker('hide'); });
    };
    var pageLoading = function(){ var pageWrapper = $('#page-wrapper'); if (pageWrapper.hasClass('page-loading')) { pageWrapper.removeClass('page-loading'); } };
    var getWindowWidth = function(){   return window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth; };
    return { init: function() {uiInit(); pageLoading(); }};
}();
$(function(){ App.init(); });
