/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */

	
var alapagleltarDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/goods/alapagleltar/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"bDestroy": true,
				"aaSorting": [[ 0, "desc" ]],
				"iDisplayLength": 10,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/goods/ajax_alapagleltar_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });



            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveeditscreenalapagleltars(elemid){
	var valid = f_c($('#editfiling'));
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_alapagleltar_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editalapagleltar').serialize(),'');
	}
}


function savenewscreenalapagleltar(){
	var valid = f_c($('#newalapagleltar'));
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_alapagleltar_data.php','newsubmit=1&' + $('#newalapagleltar').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_alapagleltar_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}

function getleltar(elemid,targetdiv){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_getleltar.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#'+targetdiv).html(data);
			
		}
	});	
}

function letarok(elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_letarok.php',
		data:'elemid='+elemid+'&exp='+$('#exp_'+elemid).val()+'&found='+$('#found_'+elemid).val(),
		success: function(data){
			$('#let_'+elemid).css("background-color", "yellow");
			
		}
	});	
}

function closeleltar(elemid){
	if(confirm("Biztos lezárod a leltárt?")){
		$.ajax({
			type:'POST',
			url: ServerName +'/modules/goods/ajax_closeleltar.php',
			data:'elemid='+elemid,
			success: function(data){
				document.location="/modules/goods/alapagleltar";
				
			}
		});	
	}
}
