/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */

	
var dailycloseDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/sales/dailyclose/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"aaSorting": [[ 0, "desc" ]],
				"iDisplayLength": 10,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/sales/ajax_dailyclose_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });



            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveeditscreendailycloses(elemid){
	var valid = f_c($('#editdailyclose'));
	if(valid){
		f_xml(ServerName +'/modules/sales/ajax_dailyclose_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editdailyclose').serialize(),'');
	}
}


function savenewscreendailyclose(){
	var valid = f_c($('#newdailyclose'));
	if(valid){
		f_xml(ServerName +'/modules/sales/ajax_dailyclose_data.php','newsubmit=1&' + $('#newdailyclose').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/sales/ajax_dailyclose_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}

