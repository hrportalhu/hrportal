/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */

	
var issueDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/goods/issue/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"bDestroy": true,
				"aaSorting": [[ 0, "desc" ]],
				"iDisplayLength": 50,
                "aLengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/goods/ajax_issue_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });



            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveeditscreenworkerss(elemid){
	var valid = f_c($('#editworkers'));
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_issue_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editworkers').serialize(),'');
	}
}


function savenewscreenworkers(){
	var valid = f_c($('#newworkers'));
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_issue_data.php','newsubmit=1&' + $('#newworkers').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_issue_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}


function addissuelement() {
		if($('#newitem').val()!=''){
		var myni=$('#newitem').val().match(/([0-9]*)\)$/);

		f_xml(ServerName +'/modules/goods/ajax_goods_issue.php','view=edit&newsubmit=1&' +
			'&newitem=' + myni[1] +
			'&amount=' + $('#amount').val() +
			'&munkahely=' + $('#munkahely').val() +
			'&tetel=' + $('#tetel').val() +
			'&rdate=' + $('#rdate').val(),
			'newitemlist');
		}
		$('#newitem').val('');
		$('#amount').val('');
		$('#tetel').html('<option value="0">Kérlek válassz</option>');
		
}


function editissuelement(){
		f_xml(ServerName +'/modules/goods/ajax_goods_issue.php','view=edit&editsubmit=1' +
			'&amount=' + $('#amount').val() +
			'&munkahely=' + $('#munkahely').val() +
			'&tetel=' + $('#tetel').val() +
			'&aid=' + $('#aid').val() +
			'&rdate=' + $('#rdate').val(),
			'newitemlist');
		
		$('#newitem').val('');
		$('#amount').val('');
		$('#tetel').html('<option value="0">Kérlek válassz</option>');
		
}

function backissuelement(){
	//alert($('#maxmennyi').val()+'>'+$('#amount2').val());
		if(parseFloat(ds($('#maxmennyi').val())) >= parseFloat(ds($('#amount2').val()))){
			if(confirm("Biztos vissza szeretnéd venni?")){
				f_xml(ServerName +'/modules/goods/ajax_goods_issue.php','view=edit&backsubmit=1' +
					'&amount=' + $('#amount2').val() +
					'&munkahely=' + $('#munkahely2').val() +
					'&store=' + $('#store2').val() +
					'&tetel=' + $('#tetel2').val() +
					'&aid=' + $('#aid2').val() +
					'&rdate=' + $('#rdate2').val(),
					'newitemlist');
				
				$('#newitem').val('');
				$('#amount').val('');
				$('#tetel').html('<option value="0">Kérlek válassz</option>');
			}
		}
		
}



function delissuelement(elemid){
	f_xml(ServerName +'/modules/goods/ajax_goods_issue.php','del='+elemid,'newitemlist');
}

$("#newitem").autocomplete({
	 source: ServerName+"/modules/goods/ajax_goods_issue_term.php",
	 minLength: 0,
	 select: function(event, ui) {
			$("#newitem").val(ui.item.value);
			getkiadlot(ui.item.id);
		}
});


function getkiadlot(elemid){
		$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_goods_issue_getkiadlot.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#tetel').html(data);
		}});
	
}

function saveissue(){
	$('#newitem').val('');
		$('#amount').val('');
		if(confirm('Biztosan rögzíti?')){
			f_xml(ServerName +'/modules/goods/ajax_goods_issue.php','finalsubmit=1','newitemlist');
		}
}

function showissueelement(targetdiv){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_goods_issue_show.php',
		data:'elemid=1',
		success: function(data){
			$('#'+targetdiv).html(data);
		}});
}
