/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */

	
var MaterialsDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/goods/materials/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"aaSorting": [[ 0, "desc" ]],
				"iDisplayLength": 10,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/goods/ajax_materials_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });



            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveeditscreenmaterialss(elemid){
	var valid = f_c($('#editfiling'));
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_materials_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editmaterials').serialize(),'');
	}
}


function savenewscreenmaterials(){
	var valid = f_c($('#newmaterials'));
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_materials_data.php','newsubmit=1&' + $('#newmaterials').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_materials_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}


function addbeszelement(elemid,targetdiv){
	if($('#gyarto_id').val()!="0" ){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_addbeszelement.php',
		data:'newsubmit=1&elemid='+elemid+'&'+$('#newelementform').serialize(),
		success: function(data){
			showbeszelement(elemid,targetdiv);
		}});
	}
	else{
		alert("Üres tételt nem lehet betölteni!");
	}
}

function showbeszelement(elemid,targetdiv){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_showbeszelement.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#'+targetdiv).html(data);
		}});
}

function showalapagstory(elemid,targetdiv){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_showalapagstory.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#'+targetdiv).html(data);
		}});
}

function delbeszelement(delid,elemid){
	if(confirm("Biztos törlöd ezt a beszállítót?")){	
		$.ajax({
			type:'POST',
			url: ServerName +'/modules/goods/ajax_delbeszelemet.php',
			data:'delsubmit=1&elemid='+elemid+'&delid='+delid,
			success: function(data){
				document.location='/modules/goods/materials/'+elemid;
			}});
		}
	
}
