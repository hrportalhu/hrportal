/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */
var products_kategDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/goods/products_kateg/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"aaSorting": [[ 0, "desc" ]],
				"iDisplayLength": 30,
				"bDestroy": true,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/goods/ajax_products_kateg_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					aoData.push({"name": "filtergroup_id", "value":$("#filtergroup_id").val() });
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });



            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveeditscreenproducts_kategs(elemid){
	var valid = f_c($('#editproducts_kateg'));
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_products_kateg_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editproducts_kateg').serialize(),'');
	}
}


function savenewscreenproducts_kateg(){
	var valid = f_c($('#newproducts_kateg'));
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_products_kateg_data.php','newsubmit=1&' + $('#newproducts_kateg').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_products_kateg_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}
