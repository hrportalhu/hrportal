/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */

	
var salesDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/sales/sales/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"aaSorting": [[ 0, "desc" ]],
				"iDisplayLength": 10,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/sales/ajax_sales_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });



            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
            
            
        }
    };
}();

function saveeditscreensaless(elemid){
	var valid = f_c($('#editsales'));
	if(valid){
		f_xml(ServerName +'/modules/sales/ajax_sales_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editsales').serialize(),'');
	}
}


function savenewscreensales(){
	var valid = f_c($('#newsales'));
	if(valid){
		f_xml(ServerName +'/modules/sales/ajax_sales_data.php','newsubmit=1&' + $('#newsales').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/sales/ajax_sales_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}

function addaruelement(elemid,targetdiv){
	if($('#agt_id').val()!="" && $('#mennyiseg').val()!="" ){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/sales/ajax_addaruelement.php',
		data:'newsubmit=1&elemid='+elemid+'&'+$('#newelementform').serialize(),
		success: function(data){
			showaruelement(elemid,targetdiv);
		}});
	}
	else{
		alert("Üres tételt nem lehet betölteni!");
	}
}

function delaruetelemet(elemid,headerid){
	if(confirm("Biztos törlöd ezt a tételt a bevételezésből")){	
		$.ajax({
			type:'POST',
			url: ServerName +'/modules/sales/ajax_delaruetelemet.php',
			data:'delsubmit=1&elemid='+elemid,
			success: function(data){
				document.location='/modules/sales/sales/'+headerid;
			}});
		}
	
}


function showaruelement(elemid,targetdiv){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/sales/ajax_showaruelement.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#'+targetdiv).html(data);
		}});
}

function getfilteredproducts(){
		$.ajax({
		type:'POST',
		url: ServerName +'/modules/sales/ajax_getfilteredproducts.php',
		data:'elemid='+$('#topkateg').val(),
		success: function(data){
			$('#product').html(data);
		}});
}
