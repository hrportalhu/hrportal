/*
 *  Document   : login.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Login page
 */

var Login = function() {

    return {
        init: function() {
            /* Login & Register show-hide */
            var formLogin       = $('#form-login'),
                formRegister    = $('#form-register');
			
           // $('#link-login').click(function(){ formLogin.slideUp(250); formRegister.slideDown(250, function(){$('input').placeholder();}); });
           // $('#link-register').click(function(){ formRegister.slideUp(250); formLogin.slideDown(250, function(){$('input').placeholder();}); });
            $('#submitbutton').click(function(){ auth_dologin();});
            //$('#jels').change(function(){ auth_dologin();});
			if($('#unev').val()!=''){
				$('#jels').focus();
			}
            // If the link includes the hashtag register, show the register form instead of login
            if (window.location.hash === '#register') {
                formLogin.hide();
                formRegister.show();
            }
        }
    };
}();
var Login2 = function() {  return {init: function() { var formLogin       = $('#form-login2'), formRegister    = $('#form-register'); $('#submitbutton').click(function(){ auth_dologin2();}); $('#ljels').change(function(){ auth_dologin2();}); if (window.location.hash === '#register') { formLogin.hide(); formRegister.show();   } }};}();

var mit;
function setloginnum(mit){
	if(mit=="del"){
		$('#ljels').val('');
	}
	else{
		var cucc=	$('#ljels').val();
		var newcucc=cucc+mit;
		$('#ljels').val(newcucc);
	}
}
