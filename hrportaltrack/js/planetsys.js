// Planet Star form checking routines
// Copyright (c) 2004 - 2010
// require(jq)
var requestVan = 0;



var Plaform = {

    Params : {},

    init : function(json){

	$('.error').remove();
	if(!jQuery.isEmptyObject(json)){
	$.each(json,function(i,v){
	    if ((typeof v.promptvalid !== 'undefined' && v.promptvalid.length>0)||(typeof v.format !== 'undefined' && v.format.length>0)){
		$('#' + i).blur(function(){Plaform.valid($(this))});
	    }

	    if((typeof v.help !== 'undefined' && v.help.length>0)||(typeof v.format !== 'undefined' && v.format.length>0)){
		$('#' + i).focus(function(){Plaform.focus($(this))});
	    }
	});
	Plaform.Params = json;
	}

    },

    errmsg : function(id,obj){
		if(typeof eval("Plaform.Params." + id + "." + obj) !== 'undefined' && eval("Plaform.Params." + id + "." + obj + ".length") > 0){
					$('#'+id).parent().find(".error, .tip").remove();
						$('<span class="error">' + eval("Plaform.Params." + id + "." + obj) + '</span>')
			    			.appendTo($('#'+id).parent())
			    			.click(function(){ $(this).remove();});
					}else{
						alert(id + " + " + obj + " - No error message found");
					}
    },

    valid : function(myel){

	if(typeof myel !== 'object'){myel = $('#' + myel);}
	var id=myel.attr('id');
	myel.removeClass('over');


	if(myel.hasClass("required")&&myel.val()==''){
		Plaform.errmsg(id,'req_text');
		return false;
	}else{
		myel.parent().find(".error").remove();
	}

	if(typeof eval("Plaform.Params." + id + ".min") !== 'undefined' && eval("Plaform.Params." + id + ".min").length > 0){

		var min = eval("Plaform.Params." + id + ".min");
		if($('#'+min).length > 0){
			min = $('#'+min).val();
		}else{
			min = parseInt(min);
		}
		if((eval("Plaform.Params." + id + ".type")=='text' && myel.val().length < min) ||
		    (eval("Plaform.Params." + id + ".type")=='area' && myel.val().length < min) ||
		   (eval("Plaform.Params." + id + ".type")=='int' && parseInt(myel.val()) < min) ||
		   (eval("Plaform.Params." + id + ".type")=='dec' && parseFloat(myel.length) < min)
		){
			Plaform.errmsg(id,'min_text');
		}else if((eval("Plaform.Params." + id + ".type")=='date') ||
			(eval("Plaform.Params." + id + ".type")=='datetime')
		){
			a = new Date(min);
			if(typeof(myel) == 'object')
				b = new Date(myel.val());
			else
				b = new Date(myel);
			if (/Invalid.*|NaN/.test(a)||/Invalid.*|NaN/.test(b))
				Plaform.errmsg(id,'min_text_invalid');
			if(a < b)
				Plaform.errmsg(id,'min_text');
		}
	}
	if(typeof eval("Plaform.Params." + id + ".max") !== 'undefined' && eval("Plaform.Params." + id + ".max").length > 0){

		var max = eval("Plaform.Params." + id + ".max");
		if($('#'+max).length > 0){
			max = $('#'+max).val();
		}else{
			max = parseInt(max);
		}

		if((eval("Plaform.Params." + id + ".type")=='text' && myel.val().length > max) ||
		    (eval("Plaform.Params." + id + ".type")=='area' && myel.val().length > max) ||
		   (eval("Plaform.Params." + id + ".type")=='int' && parseInt(myel.val()) > max) ||
		   (eval("Plaform.Params." + id + ".type")=='dec' && parseFloat(myel.length) > max)
		){
			Plaform.errmsg(id,'max_text');
		}else if((eval("Plaform.Params." + id + ".type")=='date') ||
			(eval("Plaform.Params." + id + ".type")=='datetime')
		){
			a = new Date(max);
			if(typeof(myel) == 'object')
				b = new Date(myel.val());
			else
				b = new Date(myel);
			if (/Invalid.*|NaN/.test(a)||/Invalid.*|NaN/.test(b))
				Plaform.errmsg(id,'max_text_invalid');
			if(a > b)
				Plaform.errmsg(id,'max_text');
		}
	}

	if(typeof eval("Plaform.Params." + id + ".equal") !== 'undefined' && eval("Plaform.Params." + id + ".equal").length > 0){

		var equal = eval("Plaform.Params." + id + ".equal");
		if(typeof $('#'+equal)==='object'){
			equal = $('#'+equal).val();
		}

		if((eval("Plaform.Params." + id + ".type")=='text' && myel.val().length != equal) ||
		    (eval("Plaform.Params." + id + ".type")=='area' && myel.val().length != equal) ||
		   (eval("Plaform.Params." + id + ".type")=='int' && parseInt(myel.val()) != equal) ||
		   (eval("Plaform.Params." + id + ".type")=='dec' && parseFloat(myel.length) != equal)
		){
			Plaform.errmsg(id,'equal_text');
		}
	}


	if(typeof eval("Plaform.Params." + id + ".format") !== 'undefined' && eval("Plaform.Params." + id + ".format").length > 0){
		var myformat=eval("Plaform.Params." + id + ".format");
		var oldval = myel.val();
		var newval = '';

		for(i=0,j=0;i<=myformat.length;i++){

			if(myformat.charAt(i)=='A'){
				if(/^[ \r\n<>~%*$Â§#&/a-zA-Z,-.,!?_:()ĂˇĂ©Ă­ĂłĂşĂ¶ĂĽĂĂ‰ĂŤĂ“ĂšĂ–Ăś+@;=\u0151\u0150\u0171\u0170\u00f5\u00d5\u00db\u00fb\u00c4\u00e4\u00df\u00fd]*$/.test(oldval.charAt(j))){
					newval+=oldval.charAt(j++);
				}else{
					Plaform.errmsg(id,'format_text');
					return false;
				}
			}else if(myformat.charAt(i)=='9'){
				if(/^[0-9]*$/.test(oldval.charAt(j))){
					newval+=oldval.charAt(j++);
				}else{
					Plaform.errmsg(id,'format_text');
					return false;
				}
			}else if(myformat.charAt(i)=='X'){
				if(/^[ \r\n<>~%*$Â§#&/A-ZZa-z0-9,-.,!?_:()ĂˇĂ©Ă­ĂłĂşĂ¶ĂĽĂĂ‰ĂŤĂ“ĂšĂ–Ăś+@;=\u0151\u0150\u0171\u0170\u00f5\u00d5\u00db\u00fb\u00c4\u00e4\u00df\u00fd]*$/.test(oldval.charAt(j))){
					newval+=oldval.charAt(j++);
				}else{
					Plaform.errmsg(id,'format_text');
					return false;
				}
			}else if(myformat.charAt(i)!=''){
				newval+=myformat.charAt(i);
			}
		}
		if(newval.length!=myformat.length){
					Plaform.errmsg(id,'format_text');
					return false;
		}
		myel.val(newval);
	}



	return true;
    },

    focus : function(myel){
		var id=myel.attr('id');

		myel.addClass('ff-over');
		$('.tip').remove();
		if(typeof myel.attr('alt') !== 'undefined'){
			if(myel.attr('alt') != ''){
				$('<span class="tip">'+ myel.attr('alt') +'</span>')
				.appendTo(myel.parent())
				.fadeIn('fast');
			}
		}

	if(myel.val() != '' && typeof eval("Plaform.Params." + id + ".format") !== 'undefined' && eval("Plaform.Params." + id + ".format").length > 0){
		var myformat=eval("Plaform.Params." + id + ".format");
		var oldval = myel.val();
		var newval = '';

		for(i=0,j=0;i<=myformat.length;i++){
			if(myformat.charAt(i)=='A'||myformat.charAt(i)=='X'||myformat.charAt(i)=='9'){
				newval+=oldval.charAt(i);
			}
		}
		myel.val(newval);
	}

    },


    fv : function(myobj){
	$('input, select', myobj).each(function(){
		Plaform.valid($(this));
	});


    },


};




function checkEmail(emailstr){emailRegExp = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.([a-z]){2,4})$/;if(emailRegExp.test(emailstr)){return true;}else{return false;}}

function f_isEmpty(myobj){
    if(typeof(myobj) == 'object')
		return (myobj.val() == "");
    return false;
}

function f_isTime(myobj){
    return /^ *$|^[0-9]{2}:[0-9]{2}$/.test(myobj.val());
}

function f_isText(myobj){
    return /^[ \r\n<>~%*$Â§#&/A-z0-9,-.,!?_:()ĂˇĂ©Ă­ĂłĂşĂ¶ĂĽĂĂ‰ĂŤĂ“ĂšĂ–Ăś+@;=\u0151\u0150\u0171\u0170\u00f5\u00d5\u00db\u00fb\u00c4\u00e4\u00df\u00fd]*$/.test(myobj.val());
}

function f_isMobil(myobj){
    return /^ *$|^[237]0[0-9]{7}$/.test(myobj.val());
}

function f_isHuf(myobj){
    return /^ *$|^[0-9 ,\.]*$/.test(myobj.val());
}

// f_gt = greater than an object's val or a concrete value
function f_gt(myobj,myval){
    if(typeof(myval) == 'object')
		return (myobj.val() > myval.val());
    else
		return (myobj.val() > myval);
}

// f_gte = greater than or equal to an object's val or a concrete value
function f_gte(myobj,myval){
    if(typeof(myval) == 'object')
		return (myobj.val() >= myval.val());
    else
		return (myobj.val() >= myval);
}

// f_gt_d = date is greater than an object's val or a concrete value
function f_gt_d(myobj,myval){
    if(typeof(myobj) == 'object')
		a = new Date(myobj.val());
    else
		a = new Date(myobj);
    if(typeof(myval) == 'object')
		b = new Date(myval.val());
    else
		b = new Date(myval);
    if (/Invalid.*|NaN/.test(a)||/Invalid.*|NaN/.test(b))return false;
    return (a > b);
}

// f_gte_d = date is greater than or equal to an object's val or a concrete value
function f_gte_d(myobj,myval){
    if(typeof(myobj) == 'object')
		a = new Date(myobj.val());
    else
		a = new Date(myobj);
    if(typeof(myval) == 'object')
		b = new Date(myval.val());
    else
		b = new Date(myval);
    if (/Invalid.*|NaN/.test(a)||/Invalid.*|NaN/.test(b))return false;
    return (a >= b);
}

// f_lt = lesser than an object's val or a concrete value
function f_lt(myobj,myval){
    if(typeof(myval) == 'object')
		return (myobj.val() < myval.val());
    else
		return (myobj.val() > myval);
}

// f_lte = lesser than or equal to an object's val or a concrete value
function f_lte(myobj,myval){
    if(typeof(myval) == 'object')
		return (myobj.val() <= myval.val());
    else
		return (myobj.val() <= myval);
}

// f_lt_d = date is lesser than an object's val or a concrete value
function f_gt_d(myobj,myval){
    if(typeof(myobj) == 'object')
		a = new Date(myobj.val());
    else
		a = new Date(myobj);
    if(typeof(myval) == 'object')
		b = new Date(myval.val());
    else
		b = new Date(myval);
    if (/Invalid.*|NaN/.test(a)||/Invalid.*|NaN/.test(b))return false;
    return (a < b);
}

// f_gte_d = date is lesser than or equal to an object's val or a concrete value
function f_gte_d(myobj,myval){
    if(typeof(myobj) == 'object')
		a = new Date(myobj.val());
    else
		a = new Date(myobj);
    if(typeof(myval) == 'object')
		b = new Date(myval.val());
    else
		b = new Date(myval);
    if (/Invalid.*|NaN/.test(a)||/Invalid.*|NaN/.test(b))return false;
    return (a <= b);
}

// f_eq = a form field value is equal to another value
function f_eq(myobj,myval){
    if(typeof(myval) == 'object')
		return (myobj.val() == myval.val());
    else
		return (myobj.val() == myval);
}

// Same as f_eq but for dates
function f_eq_d(myobj,myval){
    if(typeof(myobj) == 'object')
		a = new Date(myobj.val());
    else
		a = new Date(myobj);
    if(typeof(myval) == 'object')
		b = new Date(myval.val());
    else
		b = new Date(myval);
    if (/Invalid.*|NaN/.test(a)||/Invalid.*|NaN/.test(b))return false;
    return (a == b);
}

// With function to check if two form field must be filled out together
function f_w(myobj,myval){
//    alert(myobj.is('input:checkbox'));
    return (!f_isEmpty(myobj)&&!f_isEmpty(myval))||(f_isEmpty(myobj)&&f_isEmpty(myval));
}

// Basic form utilities
function f_b(myobj){

// Labels
//	$('input').focus(function(){
//		$(this).addClass("over");
//		if($(this).attr("alt") !== undefined){
//			var tip = $(this).attr("alt");
//			if(tip != ''){
//				var parent = $(this).parent();
//				$('<span class="tip">'+ tip +'</span>')
//					.appendTo(parent)
//					.fadeIn('fast');
//			}
//		}
//	}).blur(function(){
//		$('.error',$(this).parent()).remove();
//		$('.tip').remove();
//		$(this).removeClass("over");
//	});

// FIXME
//	if(typeof(myobj)=='string'){
//		rules=myobj.split('|');
//		for(i=0;i<rules.length-1;i++){
//			rule=rules[i].split(":");
//			if(rule[1]=='gte')
//				alert("a");
////			$(rule[0]).fadeIn('fast');
////.blur(function(){
////				f_gt($(rule[0]),rule[2]);
////			});
//		}
//    }
}

// Called at form validation
function f_c(myobj){

// Basic checks
var errmsg = [];
valid = true;

	$('.error', myobj).remove();
	$('.required', myobj).each(function(){
		var parent = $(this).parent();
		if( $(this).val() == '' ){
			var msg = $(this).attr('title');
			if (msg == '' || msg == undefined) { msg = 'KĂ¶telezĹ‘ mezĹ‘!'; }
			if(errmsg[$(this).attr("id")]==undefined){
				$('<span class="error">'+ msg +'</span>')
					.appendTo(parent)
					.fadeIn('fast')
					.click(function(){ $(this).remove(); })
				errmsg[$(this).attr("id")]="1";
			}
			valid = false;
		};
	});

	$('.date', myobj).each(function(){
		if (/Invalid.*|NaN/.test(new Date($(this).val()))){
			var parent = $(this).parent();
			if(errmsg[$(this).attr("id")]==undefined){
				$('<span class="error">Nem megfelelĹ‘ dĂˇtum formĂˇtum!</span>')
					.appendTo(parent)
					.fadeIn('fast')
					.click(function(){ $(this).remove(); })
				errmsg[$(this).attr("id")]="d";
			}
			valid = false;
		}
	});

	$('.integer', myobj).each(function(){
		if (!/^\d*$/.test($(this).val())){
			var parent = $(this).parent();
			if(errmsg[$(this).attr("id")]==undefined){
				$('<span class="error">Csak számok adhatók meg!</span>')
					.appendTo(parent)
					.fadeIn('fast')
					.click(function(){ $(this).remove(); });
				errmsg[$(this).attr("id")]="i";
			}
			valid = false;
		}
	});

	$('.decimal', myobj).each(function(){
		if (!/^(\d|,)*$/.test($(this).val())){
			var parent = $(this).parent();
			if(errmsg[$(this).attr("id")]==undefined){
				$('<span class="error">Csak szĂˇmok vagy vesszĹ‘ adhatĂł meg!</span>')
					.appendTo(parent)
					.fadeIn('fast')
					.click(function(){ $(this).remove(); });
				errmsg[$(this).attr("id")]="i";
			}
			valid = false;
		}
	});


	$('.email', myobj).each(function(){
		if (!/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test($(this).val())){
			var parent = $(this).parent();
			if(errmsg[$(this).attr("id")]==undefined){
				$('<span class="error">Nem megfelelő email formátum!</span>')
					.appendTo(parent)
					.fadeIn('fast')
					.click(function(){ $(this).remove(); })
					errmsg[$(this).attr("id")]="e";
			}
			valid = false;
		}
	});

	$('.url', myobj).each(function(){
		if (!/^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test($(this).val())){
//'
			var parent = $(this).parent();
			if(errmsg[$(this).attr("id")]==undefined){
				$('<span class="error">Nem megfelelő URL formátum!</span>')
					.appendTo(parent)
					.fadeIn('fast')
					.click(function(){ $(this).remove(); })
				errmsg[$(this).attr("id")]="e";
			}
			valid = false;
		}
	});

	return valid;
}

function f_xml(ajaxFile,myData,parentTag){
 if(!ajaxFile)return false;
 if(requestVan==1)return;
 requestVan = 1;
 $.ajax({
    url: ajaxFile,
    type: "POST",
    data: myData,
    cache: false,
    success: function(data){
		if(typeof(data) != 'object'){
			alert("Hiba a kérés feldolgozása közben.");
			alert(data);
			requestVan = 0;
		}
		else{

      if(typeof(parentTag) != 'object'){ parentTag = $("#" + parentTag);}
			parentTag.empty();
			if($("status", data).text() != 0){
				$(data).find("error").each(function(){
					 $.bootstrapGrowl('<h4>Hiba</h4> <p>'+ $(this).text() +'</p>', {type: 'danger', delay: 10000, allow_dismiss: true });
					
					
				});
			}
			$(data).find("warn").each(function(){
				 $.bootstrapGrowl('<h4>Figyelem</h4> <p>'+ $(this).text() +'</p>', {type: 'warning', delay: 5000, allow_dismiss: true });
				
			});
			$(data).find("msg").each(function(){
				
				$.bootstrapGrowl('<h4>Üzenet</h4> <p>'+ $(this).text() +'</p>', {type: 'info', delay: 2000, allow_dismiss: true });
			});
			var table  = $("fields", data).text();
			parentTag.append(table);
			f_b();
			requestVan = 0;
			var table  = $("actions", data).text();
			if(table != '') eval(table);


		}
    },
    error: function(jqXHR,textStatus,errorThrown){
	if(textStatus == 'error') alert(errorThrown);
    }
 });
 return false;
}

// Same as above but without any checks
function f_html(ajaxFile,myData,parentTag){
 if(!ajaxFile)return false;

 $.ajax({
    url: ajaxFile,
    type: "POST",
    data: myData,
    cache: false,
    success: function(data){
		$("#" + parentTag).empty();
		$("#" + parentTag).append(data);
    }
 });

}




// Cookie handling by Planet Star
// extending JQuery
// set cookie:
// $.cookie('cookie', 'equals this');
// $.cookie('cookie', 'equals this', { expires: 24 * 7, path: '/', domain: 'planetstar.hu', secure: true });
// get cookie:
// alma = $.cookie('cookie');

jQuery.cookie = function(name, value, options) {
    if (typeof value != 'undefined') { // so we set the cookie
        options = options || {};
        if (value === null) {
            value = '';
            options.expires = -1;
        }
        var path = options.path ? '; path=' + (options.path) : '';
        var domain = options.domain ? '; domain=' + (options.domain) : '';
        var secure = options.secure ? '; secure' : '';

        var expires = '';
        if (options.expires) {
            var mydate = new Date();
            mydate.setTime(mydate.getTime() + (options.expires * 3600 * 1000));
            expires = '; expires=' + mydate.toUTCString();
        }
        document.cookie = name + '=' + encodeURIComponent(value) + expires + path + domain + secure;
    } else { // no value given, we get the cookie
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
};
var modulname,tipus,user,valamiid,boxfelirat,fileid;
function getupload(modulname,tipus,user,valamiid,boxfelirat){

	$.ajax({
		type:'POST',
		url: '/inc/ajax_getupload.php',
		data:'modulname='+modulname+'&tipus='+tipus+'&user='+user+'&valamiid='+valamiid+'&boxfelirat='+boxfelirat,
		success: function(data){	
				
			$("#filecommander").html(data);

		}});
}

function getcomment(modulname,tipus,user,valamiid,boxfelirat){
	$.ajax({
		type:'POST',
		url: '/inc/ajax_getcomment.php',
		data:'command=new&modulname='+modulname+'&tipus='+tipus+'&user='+user+'&valamiid='+valamiid+'&boxfelirat='+boxfelirat,
		success: function(data){	
			
			$("#commentcommander").html(data);
			
		}});
}

function deluploadedfile(modulname,tipus,valamiid,fileid){
	if(confirm("Biztos törlöd ezt a fájlt?")){
		$.ajax({
		type:'POST',
		url: '/inc/ajax_deluploadedfile.php',
		data:'modulname='+modulname+'&tipus='+tipus+'&valamiid='+valamiid+'&fileid='+fileid,
		success: function(data){	
			showfilelist(modulname,tipus,valamiid);
		}});
	}	
}

function setcoverfile(modulname,tipus,valamiid,fileid){
		$.ajax({
		type:'POST',
		url: '/inc/ajax_setcoverfile.php',
		data:'modulname='+modulname+'&tipus='+tipus+'&valamiid='+valamiid+'&fileid='+fileid,
		success: function(data){	
			showfilelist(modulname,tipus,valamiid);
		}});
}

function setptitlefile(modulname,tipus,valamiid,fileid){
		$.ajax({
		type:'POST',
		url: '/inc/ajax_setptitlefile.php',
		data:'modulname='+modulname+'&tipus='+tipus+'&valamiid='+valamiid+'&fileid='+fileid,
		success: function(data){	
			showfilelist(modulname,tipus,valamiid);
		}});
}

function savefiledata(modulname,tipus,valamiid,fileid){
	var title=$('#title_'+fileid).val();
	var megjegyzes=$('#megjegyzes_'+fileid).val();
	var revision=$('#revision_'+fileid).val();
	var shared=0;if($("#shared_"+fileid).prop('checked')){shared=1;}
	var cover=0;if($("#cover_"+fileid).prop('checked')){cover=1;}
	var ptitle=0;if($("#ptitle_"+fileid).prop('checked')){ptitle=1;}
	var pspec=0;if($("#pspec_"+fileid).prop('checked')){pspec=1;}
	var pmibiz=0;if($("#pmibiz_"+fileid).prop('checked')){pmibiz=1;}
	var pbizta=0;if($("#pbizta_"+fileid).prop('checked')){pbizta=1;}
	var pgmo=0;if($("#pgmo_"+fileid).prop('checked')){pgmo=1;}
	var p1935=0;if($("#p1935_"+fileid).prop('checked')){p1935=1;}
	var pbesz=0;if($("#pbesz_"+fileid).prop('checked')){pbesz=1;}
	var veszelyelemzes=0;if($("#veszelyelemzes_"+fileid).prop('checked')){pbesz=1;}
	var grafikon=0;if($("#grafikon_"+fileid).prop('checked')){pbesz=1;}
	var torveny=0;if($("#torveny_"+fileid).prop('checked')){pbesz=1;}
	
	var groupArray="";
	if($("#filestring_"+fileid).val()!= undefined){	groupArray = $("#filestring_"+fileid).val().split('-'); }
	var urladdon="";	
	if(groupArray.length>0){
		for(i=0;i<groupArray.length;i++){
			if($("#file_"+fileid+"_"+groupArray[i]).prop('checked')){urladdon+="&group_"+groupArray[i]+"=1";}
			else{urladdon+="&group_"+groupArray[i]+"=0";}
		}
	}
	$.ajax({
		type:'POST',
		url: '/inc/ajax_savefiledata.php',
		data:
			'modulname='+modulname+
			'&tipus='+tipus+
			'&valamiid='+valamiid+
			'&title='+title+
			'&megjegyzes='+megjegyzes+
			'&revision='+revision+
			'&cover='+cover+
			'&shared='+shared+
			'&ptitle='+ptitle+
			'&pspec='+pspec+
			'&pmibiz='+pmibiz+
			'&pbizta='+pbizta+
			'&pgmo='+pgmo+
			'&p1935='+p1935+
			'&pbesz='+pbesz+
			'&veszelyelemzes='+veszelyelemzes+
			'&grafikon='+grafikon+
			'&torveny='+torveny+
			'&fileid='+fileid+'&'+urladdon,
			success: function(data){	
			showfilelist(modulname,tipus,valamiid);
		}});
}

var url;
function show_usertopmenu(url){
	$.ajax({
		type:'POST',
		url: '/inc/ajax_show_usertopmenu.php',
		data:'url='+url,
		success: function(data){	
			$("#infoscreen").html(data);
		}});
}
var targetdiv;
function showfilelist(modulname,tipus,valamiid){
	$("#filelists").html("&nbsp;");
	$.ajax({type:'POST',url: '../inc/ajax_showfilelist.php',data:'modulname='+modulname+'&tipus='+tipus+'&valamiid='+valamiid,success: function(data){	$("#filelists").html(data); 
		$('[data-toggle="lightbox-image"]').magnificPopup({type: 'image', image: {titleSrc: 'title'}});	}});		
}
var targetdiv;
function showfilelist2(modulname,tipus,valamiid,targetdiv){
	$("#"+targetdiv).html("&nbsp;");
	$.ajax({type:'POST',url: '../inc/ajax_showfilelist.php',data:'modulname='+modulname+'&tipus='+tipus+'&valamiid='+valamiid,success: function(data){	$("#"+targetdiv).html(data);	}});		
}

var Psys = {

    showtable: function(func,myheight){

	var oTable;
	var cusArray="";

	if($("#stext").val()!=""){
		$("#example_filter").val=($("#stext").val());
	}

	if($("#cussum").val()!= undefined){  cusArray = $("#cussum").val().split('-'); }

	var oTable= $('#example').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		"bProcessing": true,
		"bServerSide": true,
		"oLanguage": {"sUrl": ServerName + "/inc/hu_HU.txt"},
		"iDisplayLength": 20,
			"aLengthMenu": [[10, 20, 100, -1], [10, 20, 100, "All"]],
			"aaSorting": [[ 1, "asc" ]],
			"bDestroy": true,
			"aoColumnDefs": [
				{ "sWidth": "30px",  "aTargets": [ 0 ] },
				{ "sClass": "center",  "aTargets": [ 0 ] },
			],
			"sScrollY": myheight,
			"bScrollCollapse": true,
			"oSearch": {"sSearch": $("#stext").val()},
			"sAjaxSource": ServerName + "/inc/ajax_common_list.php",
			"fnServerData": function(sSource, aoData, fnCallback){
				aoData.push({"name": "cussum", "value": $("#cussum").val()});

			if(cusArray.length>0){
				for(i=0;i<cusArray.length;i++){
					if($("#cus_"+cusArray[i]).attr('checked')){ aoData.push({"name": $("#cus_"+cusArray[i]).val(), "value":"1"});}
					else{aoData.push({"name": $("#cus_"+cusArray[i]).val(), "value":"0"});}
				}
			}
			$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
			}
		});

		$('#example tbody').css('max-height', myheight + 'px');
		$('#example tbody tr').live('hover', function() {/**/} );

		$('#example tbody tr').live('click', function(){
			var aData = oTable.fnGetData( this );
			var iId = aData[0];
			Psys.popup(iId,func,'text');
			if ( $(this).hasClass('row_selected') ) {
				$(this).removeClass('row_selected');
			}else{
				oTable.$('tr.row_selected').removeClass('row_selected');
				$(this).addClass('row_selected');
			}
		});
    },

    popup: function(elemid,func,mode){

	$('#infoscreen').dialog({
		modal: true,height: wh,width: 960,
		    buttons: {
			"Bezár": function() {
				location.reload(true);}
		    }
	});
	f_xml('/ajax/' + func  + '/data/','view=' + mode + '&id=' + elemid,'infoscreen');
    },

    setupscreen: function(func,w,h,tit){

		$('#infoscreen').dialog({
			modal: true,
			title:tit,
			height: h,width: w,
				buttons:{
				"Bezár": function(){
					location.reload(true);
				},
				"Mentés": function(){
					$('#editscreen').submit();
				}
				},
		});
		f_xml('/ajax/'+func+'/setupscreen/','screen=' + func,'infoscreen');
    },
    savesetupscreen: function(elemid,func){f_xml('/ajax/' + func  + '/data/','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize()); },
	savedireditdata: function(elemid,func){
		var elemch=0; 
		var elemval="";
		if($("#chdir_"+elemid).attr('checked')){elemch=1;}
		elemval=$("#valdir_"+elemid).val();
		f_xml('/ajax/' + func  + '/data/','command=savesetupscreen&viewtype=direditdata&elemid='+elemid+'&elemch='+elemch+'&elemval='+elemval,'mappafilter');
	},
	savenewdirdata:function(func){var elemch=0; var elemval="";if($("#chdir_new").attr('checked')){elemch=1;}elemval=$("#dir_new").val();f_xml('/ajax/' + func  + '/data/','command=savesetupscreen&viewtype=dirnewdata&elemch='+elemch+'&elemval='+elemval,'mappafilter');return false;},
};


function getprintscreen(modulname,tipus,valamiid){
		$.ajax({
		type:'POST',
		url: '/inc/ajax_getprintscreen.php',
		data:'modulname='+modulname+'&tipus='+tipus+'&valamiid='+valamiid,
		success: function(data){	
			$("#printdiv").html(data);
			
		}});
}

function getexportscreen(tipus){
		$.ajax({
		type:'POST',
		url: '/inc/ajax_getexportscreen.php',
		data:'tipus='+tipus,
		success: function(data){	
			
			$("#infoscreen" ).dialog({title:'Exportálás',height: 250,width: 440,buttons: {"Exportálás": function() {exportdata(tipus);},"Bezár": function() {$( this ).dialog( "close" );}},	close: function() {}});
			$("#infoscreen").html(data);
			$("#infoscreen").dialog("open");
		}});
}

function getsystemscreenwidth(){
		$.ajax({
			type:'POST',
			url: '/inc/ajax_getsystemscreenwidth.php',
			data:'swidth='+$(window).width()+'&sheight='+$(window).height(),
			success: function(data){}
		});
}

function exportdata(){
	
	document.location='/inc/export_data.php?format='+$("#format").val()+'&title='+$("#title").val()+'&iconv='+$("#iconv").val();
}

function exportcontent(modulname,tipus,valamiid){
	$("#infoscreen").dialog("close");
	document.location='/inc/export_content.php?modulname='+modulname+'&tipus='+tipus+'&valamiid='+valamiid;
}


function printdata(crtlid){
	var ctrlcontent = document.getElementById(crtlid);
	var printscreen = window.open('','','left=1,top=1,width=1,height=1,toolbar=0,scrollbars=0,status=0​,titlebar=no,location=no');
	printscreen.document.write(ctrlcontent.innerHTML);
	printscreen.document.close();
	printscreen.focus();
	printscreen.print();
	printscreen.close();
}

var whatids;
function PrintWoucher(whatids){
	//alert(whatids);
	frames[whatids].focus();
	frames[whatids].print();
}

function getloginscreen(optmsg){
	$('#loginscreen').dialog({
		modal: true,height: '250',width:'350', title: 'Belépés',
		position: { my: 'left bottom', at: 'right', of: '#frame'  },
		buttons: {"Belépés":function(){auth_dologin();},"Mégsem":function(){ $(this).dialog("close"); }}
	});
	if(optmsg!='') {$("#authscreen_error").append(optmsg);}
	if($.cookie('planet_rememberme')!==null&&$.cookie('planet_rememberme')!==''){
		$('#unev').val($.cookie('planet_rememberme'));
		$('#remember').attr('checked',true);
		$('#jels').focus();
	}else{
		$('#unev').focus();
	}
	$("#loginscreen").dialog("open");
}

function auth_dologin(){
	var email=$('#unev').val();
	var pass=$('#jels').val();
	var checked=0;
	
	if($('#remember').attr('checked')=='checked'){
		$.cookie('if_psys_hu_rememberme', email, { expires: 24 * 365 });
		checked=1;
	}
	else{
		$.cookie('if_psys_hu_rememberme', '0', { expires: -1 });
	}
	if(email!='' && checkEmail(email)==true){
		$('#sysmsg').html('');
		if(pass!='' ){
            $("#jels").value='';
	
			f_xml(ServerName+'/inc/_ajax_auth_local.php','email='+email+'&pass='+pass+'&checked='+checked,'authscreen_error');

		}else{
			$('#sysmsg').html('Minden mező kitöltése kötelező!');
		}

	}
	else{
		$('#sysmsg').html('A helyes formátumú email cím kötelező!');

	}
}
function auth_dologin2(){
	var pass=$('#ljels').val();
	if(pass!='' ){
		$("#ljels").value='';
		f_xml(ServerName+'/inc/_ajax_auth_local2.php','pass='+pass,'authscreen_error');
	}else{
		$('#sysmsg').html('Minden mező kitöltése kötelező!');
	}
}

function swRemMe(me){
	if(!me.attr('checked')){
		$.cookie('planet_rememberme', 'f', { expires: -3 * 24 });
	}
	return false;
}




function in_array (needle, haystack, argStrict) {
    var key = '', strict = !!argStrict; 
    if(strict){
        for (key in haystack){
            if (haystack[key]===needle){
                    return true;
	    }
        }
    }else{
        for (key in haystack){
            if (haystack[key] == needle){
            	return true;
            }
        }
    }
    return false;
}

var targetdiv;
//function showfilelist(modulname,tipus,user,valamiid,boxfelirat,targetdiv){
//	$("#filelists").html("&nbsp;");
//	$.ajax({type:'POST',url: '../inc/ajax_showfilelist.php',data:'modulname='+modulname+'&tipus='+tipus+'&user='+user+'&valamiid='+valamiid+'&boxfelirat='+boxfelirat,success: function(data){	$("#filelists").html(data);	}});		
//}

function showstory(elemid){
	$.ajax({
		type:'POST',
		url: '/inc/ajax_showstory.php',
		data:'elemid='+elemid,
		success: function(data){	
			$("#storydiv").html(data);

		}});
}
function showcommentlist(modulname,tipus,user,valamiid,boxfelirat,targetdiv){
	$.ajax({
		type:'POST',
		url: '/inc/ajax_showcommentlist.php',
		data:'modulname='+modulname+'&tipus='+tipus+'&user='+user+'&valamiid='+valamiid+'&boxfelirat='+boxfelirat,
		success: function(data){	
			$("#commentlists").html(data);

		}});
}
function showcommentlist2(modulname,tipus,user,valamiid,boxfelirat,targetdiv){
	$.ajax({
		type:'POST',
		url: '/inc/ajax_showcommentlist.php',
		data:'modulname='+modulname+'&tipus='+tipus+'&user='+user+'&valamiid='+valamiid+'&boxfelirat='+boxfelirat,
		success: function(data){	
			$("#"+targetdiv).html(data);

		}});
}
var comment_id;
function editcomment(modulname,tipus,user,valamiid,comment_id){
	$.ajax({
		type:'POST',
		url: '/inc/ajax_getcomment.php',
		data:'command=edit&comment_id='+comment_id+'&modulname='+modulname+'&tipus='+tipus+'&user='+user+'&valamiid='+valamiid+'&boxfelirat=nulla',
		success: function(data){	
			$("#commentcommander").html(data);

		}});
}
function setdesign(elemid){
	$.ajax({
		type:'POST',
		url: '/inc/ajax_setdesign.php',
		data:'design='+elemid,
		success: function(data){
				
		}});
}
function delcomment(modulname,tipus,user,valamiid,comment_id){
	if(confirm("Biztos törölni szeretné a hozzászólást")){
	$.ajax({
		type:'POST',
		url: '/inc/ajax_addsystemcomment.php',
		data:'command=del&comment_id='+comment_id+'&modulname='+modulname+'&tipus='+tipus+'&user='+user+'&valamiid='+valamiid+'&boxfelirat=nulla',
		success: function(data){	
			showcommentlist(modulname,tipus,user,valamiid,'','');
			
		}});
	}	
}
function addsystemcomment(command,modulname,tipus,user,valamiid,boxfelirat,targetdiv,comment_id){
	$.ajax({
		type:'POST',
		url: '/inc/ajax_addsystemcomment.php',
		data:'command='+command+'&modulname='+modulname+'&tipus='+tipus+'&user='+user+'&valamiid='+valamiid+'&boxfelirat='+boxfelirat+'&content='+$("#content").val()+'&comment_id='+comment_id,
		success: function(data){	
			$("#content").val('');
			$("#commentlists").html('');
			showcommentlist(modulname,tipus,user,valamiid,boxfelirat,targetdiv);

		}});
	
}


var str;
function ds(str){
	return str.replace(",","."); 
}

var txt;
function sendalert(txt){
	alert(txt);	
}

function tl( whichLayer ){

  var elem, vis;
  if( document.getElementById ) // this is the way the standards work
	elem = document.getElementById( whichLayer );
  else if( document.all ) // this is the way old msie versions work
	elem = document.all[whichLayer];
  else if( document.layers ) // this is the way nn4 works
	elem = document.layers[whichLayer];
  vis = elem.style;
  // if the style.display value is blank we try to figure it out here
  if(vis.display==''&&elem.offsetWidth!=undefined&&elem.offsetHeight!=undefined)
	vis.display = (elem.offsetWidth!=0&&elem.offsetHeight!=0)?'block':'none';
  vis.display = (vis.display==''||vis.display=='block')?'none':'block';
}
var grtype;
var grmodule;
var module;
var func;
var valamiid1;
var valamiid2;
var startdate;
var enddate;
var period;
var data1;
var peopleHTML;
var myxaxis;
var data2;
var title;
var mylabel1;
var mylabel2;
function getgraph(grtype,grmodule,module,func,valamiid1,valamiid2,startdate,enddate,period){
	$.ajax({
		type:'POST',
		url: ServerName +'/inc/ajax_getgraph.php',
		dataType: 'json',
		data:
		'grmodule='+grmodule+
		'&module='+module+
		'&func='+func+
		'&valamiid1='+valamiid1+
		'&valamiid2='+valamiid2+
		'&startdate='+startdate+
		'&enddate='+enddate+
		'&period='+period+
		'&grtype='+grtype,
		success: function(data){
			if(grtype==1){
				//Alapanyag beszerzési mennyiség
				data1 = data.dataarray1; 
				data2 =data.dataarray2; 
				myxaxis =data.dataxaxis; 
				mylabel1 =data.label1; 
				mylabel2 =data.label2; 
				$('#charttit').html(data.title);
				if(grmodule=='classic'){
					$('#chartom').width(data.width+"px");
					CompChartsClassic.init();
				}
			}
			if(grtype==2){
				//vezetői lista munkahelyi lebontásban
				data1 = data.dataarray1; 
				data2 =data.dataarray2; 
				myxaxis =data.dataxaxis; 
				mylabel1 =data.label1; 
				mylabel2 =data.label2; 
				$('#charttit').html(data.title);
				if(grmodule=='classic'){
					$('#chartom').width(data.width+"px");
					CompChartsClassic.init();
				}
			}
			if(grtype==3){
				//vezetői lista dátum lebontásban, torta diagram
				data1 = data.dataarray1; 
				data2 = data.dataarray2; 
				myxaxis = data.dataxaxis; 
				mylabel1 ='Időszaki lebontás poszt termelékenység szerint'; 
				mylabel2 =data.label2; 
				$('#charttit').html(data.title);
				if(grmodule=='pie'){
					$('#chartom').width("350px");
					CompChartsPie.init();
				}
			}
		}});
}
var CompChartsClassic = function() {
    return {
        init: function() {
           var random;
            function getRandomInt(min, max) {
                return Math.floor(Math.random() * (max - min + 1)) + min;
            }
            var chartClassic = $('#chartom');
                $.plot(chartClassic,
                [
                    {
                        label: mylabel1,
                        data:  data1,
                        lines: {show: true, fill: true, fillColor: {colors: [{opacity: 0.25}, {opacity: 0.25}]}},
                        points: {show: true, radius: 6}
                    },
                    {
                        label: mylabel2,
                        data: data2,
                        lines: {show: true, fill: true, fillColor: {colors: [{opacity: 0.15}, {opacity: 0.15}]}},
                        points: {show: true, radius: 6}
                    }
                ],
                {
                    colors: ['#3498db', '#333333'],
                    legend: {show: true, position: 'nw', margin: [15, 10]},
                    grid: {borderWidth: 0, hoverable: true, clickable: true},
                    yaxis: {ticks: 4, tickColor: '#eeeeee'},
                    xaxis: {ticks: myxaxis, tickColor: '#ffffff'}
                }
            );
            var previousPoint = null, ttlabel = null;
            chartClassic.bind('plothover', function(event, pos, item) {
                if (item) {
                    if (previousPoint !== item.dataIndex) {
                        previousPoint = item.dataIndex;
                        $('#chart-tooltip').remove();
                        var x = item.datapoint[0], y = item.datapoint[1];
                        if (item.seriesIndex === 1) {
                            ttlabel = '<strong>' + y + '</strong>';
                        } else {
                            ttlabel = '<strong>' + y + '</strong>';
                        }
                        $('<div id="chart-tooltip" class="chart-tooltip">' + ttlabel + '</div>').css({top: item.pageY - 45, left: item.pageX + 5}).appendTo("body").show();
                    }
                }
                else {
                    $('#chart-tooltip').remove();
                    previousPoint = null;
                }
            });
        }
    };
}();

var CompChartsBars = function() {

    return {
        init: function() {
            var random;
            function getRandomInt(min, max) {
                return Math.floor(Math.random() * (max - min + 1)) + min;
            }
            var chartBars = $('#chartom');
            var dataSales2 = [[1, 150], [3, 200], [5, 250], [7, 300], [9, 420], [11, 350], [13, 450], [15, 600], [17, 580], [19, 810], [21, 1120]];

            var chartMonths = [[1, 'Jan'], [2, 'Feb'], [3, 'Mar'], [4, 'Apr'], [5, 'May'], [6, 'Jun'], [7, 'Jul'], [8, 'Aug'], [9, 'Sep'], [10, 'Oct'], [11, 'Nov'], [12, 'Dec']];
			
            $.plot(chartBars,
                [
                    {
                        label: 'Eladások',
                        data: dataSales2,
                        bars: {show: true, lineWidth: 0, fillColor: {colors: [{opacity: 0.5}, {opacity: 0.5}]}}
                    }
                ],
                {
                    colors: ['#9b59b6'],
                    legend: {show: true, position: 'nw', margin: [15, 10]},
                    grid: {borderWidth: 0},
                    yaxis: {ticks: 4, tickColor: '#eeeeee'},
                    xaxis: {ticks: 10, tickColor: '#ffffff'}
                }
            );

        }
    };
}();

var CompChartsStacked = function() {

    return {
        init: function() {
             var random;
           function getRandomInt(min, max) {
                return Math.floor(Math.random() * (max - min + 1)) + min;
            }
            var chartStacked = $('#chartom');
            var dataEarnings = [[1, 1560], [2, 1650], [3, 1320], [4, 1950], [5, 1800], [6, 2400], [7, 2100], [8, 2550], [9, 3300], [10, 3900], [11, 4200], [12, 4500]];
            var dataSales = [[1, 500], [2, 420], [3, 480], [4, 350], [5, 600], [6, 850], [7, 1100], [8, 950], [9, 1220], [10, 1300], [11, 1500], [12, 1700]];
            var chartMonths = [[1, 'Jan'], [2, 'Feb'], [3, 'Mar'], [4, 'Apr'], [5, 'May'], [6, 'Jun'], [7, 'Jul'], [8, 'Aug'], [9, 'Sep'], [10, 'Oct'], [11, 'Nov'], [12, 'Dec']];
			
            // Stacked Chart
            $.plot(chartStacked,
                [{label: 'Eladás', data: dataSales}, {label: 'Előleg', data: dataEarnings}],
                {
                    colors: ['#f1c40f', '#f39c12'],
                    series: {stack: true, lines: {show: true, fill: true}},
                    lines: {show: true, lineWidth: 0, fill: true, fillColor: {colors: [{opacity: 0.75}, {opacity: 0.75}]}},
                    legend: {show: true, position: 'nw', margin: [15, 10], sorted: true},
                    grid: {borderWidth: 0},
                    yaxis: {ticks: 4, tickColor: '#eeeeee'},
                    xaxis: {ticks: chartMonths, tickColor: '#ffffff'}
                }
            );
        }
    };
}();

var CompChartsPie = function() {

    return {
        init: function() {
           
            // Randomize easy pie charts values
            var random;

            $('.toggle-pies').click(function() {
                $('.pie-chart').each(function() {
                    random = getRandomInt(1, 100);
                    $(this).data('easyPieChart').update(random);
                    $(this).find('span').text(random + '%');
                });
            });

            // Get random number function from a given range
            function getRandomInt(min, max) {
                return Math.floor(Math.random() * (max - min + 1)) + min;
            }
            var chartPie = $('#chartom');
            // Pie Chart
            $.plot(chartPie, data1,
                {
                    colors: data2,
                    legend: {show: false},
                    series: {
                        pie: {
                            show: true,
                            radius: 1,
                            label: {
                                show: true,
                                radius: 3 / 4,
                                formatter: function(label, pieSeries) {
                                    return '<div class="chart-pie-label">' + label + '<br>' + Math.round(pieSeries.percent) + '%</div>';
                                },
                                background: {opacity: 0.75, color: '#000000'}
                            }
                        }
                    }
                }
            );

        }
    };
}();


	$.datepicker.regional['hu'] = {
		closeText: 'bezárás',
		prevText: '&laquo;&nbsp;vissza',
		nextText: 'előre&nbsp;&raquo;',
		currentText: 'ma',
		monthNames: ['Január', 'Február', 'Március', 'Április', 'Május', 'Június',
		'Július', 'Augusztus', 'Szeptember', 'Október', 'November', 'December'],
		monthNamesShort: ['Jan', 'Feb', 'Már', 'Ápr', 'Máj', 'Jún',
		'Júl', 'Aug', 'Szep', 'Okt', 'Nov', 'Dec'],
		dayNames: ['Vasárnap', 'Hétfö', 'Kedd', 'Szerda', 'Csütörtök', 'Péntek', 'Szombat'],
		dayNamesShort: ['Vas', 'Hét', 'Ked', 'Sze', 'Csü', 'Pén', 'Szo'],
		dayNamesMin: ['V', 'H', 'K', 'Sze', 'Cs', 'P', 'Szo'],
		weekHeader: 'Hé',
		dateFormat: 'yy-mm-dd',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	
