<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors','Off');
define("DB_SERVER","localhost");
define("DB_USER","joee");
define("DB_PASSWORD","6rometheus");
define("DB_NAME","if_plsys_hu");
define("DB_TABLEPREFIX","");
include_once dirname(__FILE__).("/../inc/_functions.php");
if(isset($_SERVER['HTTPS'])){$ServerName="https://";}else{$ServerName="http://";}
$ServerName.=$_SERVER['HTTP_HOST'];$_SESSION['servername']=$ServerName;
$URI = (isset($_SERVER['REQUEST_URI']) ? substr($_SERVER['REQUEST_URI'], 1) : '');
$uritags=explode("/",$URI);


$ppage="/".$uritags[0];
$subpage="/".$uritags[0]."/".$uritags[1];
$subsubpage="/".$uritags[0]."/".$uritags[1]."/".$uritags[2];


$urinum=count($uritags);
$lasttag=$uritags[($urinum-1)];
$lastttaglen=strlen($lasttag)+1;
if(is_numeric($lasttag) || $lasttag=="new" || $lasttag=="alllist" ){
	$innerurl=substr($URI,0,-$lastttaglen);	
}
else{
	$innerurl=$URI;	
}

$PAGE = ($uritags[0]!='' ? $uritags[0] : '');
$tableprefix = DB_TABLEPREFIX;



if(isset($_GET['logout']) && $_GET['logout']=="yes"){
	unset($_SESSION['planetsys']);
	echo"<script type=\"text/javascript\">document.location='/mobile/index.php';</script>";	
}

$template = array(
    'name'          => 'InterfoodTrack',
    'version'       => '0.3',
    'author'        => 'planetstar',
    'robots'        => 'noindex, nofollow',
    'title'         => 'InterfoodTrack',
    'description'   => 'InterfoodTrack',
    'header_navbar' => 'navbar-inverse',
    'header'        => '',
    'sidebar'       => 'sidebar-partial sidebar-visible-lg sidebar-no-animations',
    'footer'       => 'footer-fixed',
    'main_style'    => 'style-alt',
    'theme'         => 'default',
    'header_content'=> '',
    'active_page'   => basename($_SERVER['PHP_SELF']),
    'workpage'   =>  $innerurl,
);
$template['page_preloader'] = true;
$template['header'] = ''; 
$template['sidebar'] = 'sidebar-mini sidebar-visible-lg-mini sidebar-no-animations';
?>
<!DOCTYPE html>
<!--[if IE 8]> 
       <html class="no-js lt-ie9"> 
 <![endif]-->
 <!--[if gt IE 8]><!--> 
	<html class="no-js"> 
<!--<![endif]-->    
<head>        
	<meta charset="utf-8">        
	<base href="<?=$ServerName;?>"> 
	<title><?php echo $template['title'] ?></title>        
	<meta name="description" content="<?php echo $template['description'] ?>">		
	<meta name="author" content="<?php echo $template['author'] ?>">        
	<meta name="robots" content="<?php echo $template['robots'] ?>">        
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">        
	<link rel="shortcut icon" href="img/favicon.ico">        
	<link rel="stylesheet" type="text/css" href="css/clock_styles.css" />
	<link rel="stylesheet" type="text/css" href="js/jquery.tzineClock/jquery.tzineClock.css" />     
	<link rel="stylesheet" href="css/jquery-ui.min.css">        
	<link rel="stylesheet" href="css/bootstrap.min.css">       
	<link rel="stylesheet" href="css/main2.css">		
	<?php if ($template['theme']) { ?>
		<link id="theme-link" rel="stylesheet" href="css/themes/<?php echo $template['theme']; ?>.css"><?php } 
	?>        
	<link rel="stylesheet" href="css/themes.css">        
	 <script type="text/javascript">	var ServerName="<?=$ServerName;?>";	</script>
	<script src="js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
	 <script src="js/jquery-min.js"></script>
<?
if(isset($_SESSION['planetsys']['worker_id']) && $_SESSION['planetsys']['worker_id']!='' && $_SESSION['planetsys']['worker_id']!=0 ){
?>
 
   <script>
   	$(document).on('DOMContentLoaded', function () {
            var canvas = document.getElementById("canvas"),
                context = canvas.getContext("2d"),
                video = document.getElementById("video"),
                videoObj = { "video": true },
                image_format= "jpeg",
                jpeg_quality= 100,
                errBack = function(error) {
                    console.log("Video capture error: ", error.code); 
                };
            if(navigator.getUserMedia) { navigator.getUserMedia(videoObj, function(stream) {video.src = stream; video.play(); }, errBack);    } 
            else if(navigator.webkitGetUserMedia) {navigator.webkitGetUserMedia(videoObj, function(stream){  video.src = window.URL.createObjectURL(stream); video.play();}, errBack);  } 
            else if(navigator.mozGetUserMedia) { navigator.mozGetUserMedia(videoObj, function(stream){ video.src = window.URL.createObjectURL(stream); video.play(); }, errBack); }
        });

		function setcsempepost(post_id) {
			var canvas = document.getElementById("canvas"),
			context = canvas.getContext("2d");
			context.drawImage(video, 0, 0, 320, 240);
			saveworkstart(post_id);
			var dataUrl = canvas.toDataURL("image/jpeg", 1);
			$('#worktimein').html('<center><i class="fa fa-sun-o fa-spin fa-4x"></i></center>');
                $.ajax({type: "POST",url: "/mobile/upload.php", data: { imgBase64: dataUrl  }}).done(function(msg) { console.log("saved"); setTimeout( function() { logout()}, 1000); });
		};
		
		var timoutWarning = 60000; // Display warning in 1 Mins.
		var timoutNow = 75000; // Timeout in 75 sec
		var warningTimer;
		var timeoutTimer;
		function StartTimers() { warningTimer = setTimeout("IdleWarning()", timoutWarning);	timeoutTimer = setTimeout("IdleTimeout()", timoutNow);}
		function ResetTimers() { clearTimeout(warningTimer);clearTimeout(timeoutTimer);	StartTimers();$("#timerscreen").dialog('close');}
		function IdleWarning() {$("#timerscreen").dialog({modal: true});}
		function IdleTimeout() {logout();}
		StartTimers();
   </script>
	<?
}
	?>	
</head>    
<?
//if(!isset($_SESSION['planetsys']['user_id']) || $_SESSION['planetsys']['user_id']=='' || $_SESSION['planetsys']['user_id']==0 || !isset($_SESSION['planetsys']['worker_id']) || $_SESSION['planetsys']['worker_id']=='' || $_SESSION['planetsys']['worker_id']==0 ){
//	echo"<body style='background:url(img/placeholders/headers/planetrack_bg.jpg) no-repeat #fff;background-size: 100% auto;'>";
//}
//else{
//	echo"<body>";
//}	
echo"<body>";
?>        

<div id="infoscreen"></div>
<div id="timerscreen" style="display:none;">15 másodperc múlva a rendszer automatikusan kilép</div>
<div id="sysmsg"></div>
<?
	
if(!isset($_SESSION['planetsys']['worker_id']) || $_SESSION['planetsys']['worker_id']=='' || $_SESSION['planetsys']['worker_id']==0 ){
	?>
	<div id="login-container2" class="animation-fadeIn" >    
		<div class="login-title text-center">        
			<h1><strong>Bejelentkezés</strong></h1>    
		</div>
		<div class="block " style="width:730px;margin:0 auto;">        
			<div id="form-login2" class="form-horizontal form-bordered form-control-borderless">            
				
				<div class="form-group text-center">                
					<div class="text-center">                    
						<div class="input-group text-center" style="width:700px;margin: 0 auto;">                        
												 
							<input type="text" id="ljels" name="ljels" class="form-control input-lg" style="width:700px;font-size:35px;"  readonly />                    
						</div>                
					</div>            
				</div>  
				<div class="form-group">                
					<div class="text-center" >                    
						<button class="btn btn-lg btn-info" style="font-size:70px;" onclick="javascript:setloginnum(1);">1</button>
						<button class="btn btn-lg btn-info" style="font-size:70px;" onclick="javascript:setloginnum(2);">2</button>
						<button class="btn btn-lg btn-info" style="font-size:70px;" onclick="javascript:setloginnum(3);">3</button>
						<button class="btn btn-lg btn-info" style="font-size:70px;" onclick="javascript:setloginnum(4);">4</button>
						<button class="btn btn-lg btn-info" style="font-size:70px;" onclick="javascript:setloginnum(5);">5</button>
						<button class="btn btn-lg btn-info" style="font-size:70px;" onclick="javascript:setloginnum(6);">6</button>
						<button class="btn btn-lg btn-info" style="font-size:70px;" onclick="javascript:setloginnum(7);">7</button>
						<button class="btn btn-lg btn-info" style="font-size:70px;" onclick="javascript:setloginnum(8);">8</button>
						<button class="btn btn-lg btn-info" style="font-size:70px;" onclick="javascript:setloginnum(9);">9</button>
						<button class="btn btn-lg btn-info" style="font-size:70px;" onclick="javascript:setloginnum(0);">0</button>
						<button class="btn btn-lg btn-info" style="font-size:70px;" onclick="javascript:setloginnum('del');">Töröl</button>
					</div>            
							  
				</div>                 
				<div class="form-group form-actions">                
					<div class="text-center">                    
						<button id="submitbutton" class="btn btn-lg btn-primary" style="font-size:50px;"><i class="fa fa-angle-right"></i> Belépés</button>                
					</div>
				</div>
				   
				   <div style="clear:both;"></div>	
			</div> 
		</div>
	</div>

	
	<?
}
else{
	?>
	<div id="page-wrapper"<?php if ($template['page_preloader']) { echo ' class="page-loading"'; } ?>>
    <div class="preloader themed-background-dark">
        <h1 class="push-top-bottom text-center" style="color:#fff;"><strong>InterfoodT</strong>rack</h1>
        <div class="inner">
            <h3 class="text-light visible-lt-ie9 visible-lt-ie10" style="color:#fff;" ><strong>A kért oldal felépítése folyamataban van...</strong></h3>
            <div class="preloader-spinner hidden-lt-ie9 hidden-lt-ie10"></div>
        </div>
    </div>


<div id="page-container" >
    <div id="sidebar">
        <div class="sidebar-scroll">
            <div class="sidebar-content">
                <a href="l.php" class="sidebar-brand">
                    <i class="gi gi-birthday_cake"></i>
                </a>
            </div>
        </div>
    </div>
    <div id="main-container">

     <header class="navbar<?php if ($template['header_navbar']) { echo ' ' . $template['header_navbar']; } ?><?php if ($template['header']) { echo ' '. $template['header']; } ?>">
	<div style="text-align:center;font-size:20px; font-weight:bold;color:#fff;line-height:40px;">Üdv <?=$_SESSION['planetsys']['worker_name'];?> !</div>
        </header>
		<div id="page-content" >
			
			<div class="content-header" id="commander" style="display:block();">
				<ul class="nav-horizontal text-center" style="font-size:2em;min-height:600px;" >
					<li >
						<a href="javascript:setworkstart();"><i class="gi gi-clock"></i> Poszt tevékenység</a>
					</li>
					<li>
						<a href="javascript:showworklist();"><i class="gi gi-list"></i> Tevékenységlista</a>
					</li>
					<li>
						<a href="javascript:logout();"><i class="fa fa-paper-plane"></i> Kilépés</a>
					</li>
				</ul>
				<!--<div id="fancyClock"></div>-->
				<div style="clear:both;"></div>
				<div class="camcontent" style="display:none;">
					<video id="video"></video>
					<canvas id="canvas" width="320" height="240"></canvas>
				</div>
				<div style="clear:both;"></div>	  
			</div>
			<div id="worktimein" style="display:none;">
				<?
				$hol=db_all("select id, name,icon from gw_posts where status=1 order by name");
		echo"<div class=\"col-md-2\"  style=\"cursor:pointer;\" onclick=\"javascript:setcsempepost('1000001');\" >";
			echo"<div class=\"block\"  style=\"height:160px;overflow:hidden;\">";
				echo" <div class=\"block-title\" style=\"height:60px;text-align:center;\"><h3><strong>Poszt tevékenység</strong> Vége</h3></div>";
				echo"<p class=\"text-center\" ><i class=\"gi gi-exit fa-5x text-info\"></i></p>";
			echo"</div>";
		echo"</div>";
		for($i=0;$i<count($hol);$i++){
			echo"<div class=\"col-md-2\" onclick=\"javascript:setcsempepost('".$hol[$i]['id']."');\" style=\"cursor:pointer;\" >";
				echo"<div class=\"block\" style=\"height:160px;overflow:hidden;\">";
					echo" <div class=\"block-title\" style=\"height:60px;text-align:center;\"><h3><strong>".$hol[$i]['name']."</strong></h3></div>";
					echo"<p class=\"text-center\" ><i class=\"".$hol[$i]['icon']." fa-5x text-info\"></i></p>";
				echo"</div>";
			echo"</div>";
		}
		echo "<div class=\"form-group form-actions\">
		<div class=\"col-md-9 \">";
			echo"<button type=\"button\" class=\"btn btn-lg btn-primary\" id=\"saveeditbutton\" onclick=\"cancelmob();\">Mégse</button>
		</div>
		</div>";
				?>
		<div style="clear:both;"></div>
			</div>
			<div id="worktimelist" style="display:none;">
				
			<div class="block">
				<div class="table-responsive">
					<table class="table table-vcenter table-striped">
						<thead>
							<tr>
								<th style="width: 100px;" class="text-center"><i class="gi gi-disk_save"></i></th>
								<th>Poszt</th>
								<th>Nap</th>
								<th>-tól</th>
								<th>-ig</th>
							</tr>
						</thead>
						<tbody>
							<?
							$workers=db_all("select * from gw_shift_x_worker where worker_id='".$_SESSION['planetsys']['worker_id']."' order by workday desc, starttime desc");
							
							for($i=0;$i<count($workers);$i++){
								
								echo"<tr>";
								echo"<td>".$workers[$i]['id']."</td>";
								echo"<td>".idchange("gw_posts","name",$workers[$i]['post_id'])."</td>";
								echo"<td>".$workers[$i]['workday']."</td>";
								echo"<td>";
									echo substr($workers[$i]['starttime'],11,5);
								echo"</td>";
								echo"<td>";
									 echo substr($workers[$i]['endtime'],11,5);
								echo"</td>";
								echo"</tr>";
							}
							?>
						</tbody>
					</table>		
				</div>
				<?
				echo "<div class=\"form-group form-actions\">
					<div class=\"col-md-9 \">
						<button type=\"button\" class=\"btn btn-lg btn-primary\" id=\"saveeditbutton\" onclick=\"cancelmob();\">Vissza</button>
					</div>
				</div>";
				?>
				<div style="clear:both;height:10px;"></div>
				
			</div>		
		<div style="clear:both;height:10px;"></div>
	<?
}
?>
<script>!window.jQuery && document.write(unescape('%3Cscript src="js/vendor/jquery-1.11.0.min.js"%3E%3C/script%3E'));</script>
<!-- Bootstrap.js, Jquery plugins and Custom JS code -->
<script src="js/vendor/bootstrap.min.js"></script>
<script src="js/plugins2.js"></script>
<script src="js/app2.js"></script>
<script src="js/jquery-ui.min.js"></script>
 
<script>
	var mit;
	function setloginnum(mit){if(mit=="del"){$('#ljels').val('');}else{var cucc=	$('#ljels').val();var newcucc=cucc+mit;	$('#ljels').val(newcucc);}}
	function auth_dologin2(){
		if($('#ljels').val()==''){
			document.location='/mobile/index.php';
		}
		else{
			var pass=$('#ljels').val();
			$('#login-container2').html('<center><i class="fa fa-sun-o fa-spin fa-4x"></i></center>');
			if(pass!='' ){$("#ljels").value='';	$.ajax({type:'POST',url: '/mobile/_ajax_auth_local2.php',data:'pass='+pass,	success: function(data){window.location.reload();}});}else{	
				document.location='/mobile/index.php';	
			}
		}
	}
	function logout(){ 	document.location='/mobile/index.php?logout=yes';}
	function setworkstart(){ $("#commander").hide();	$("#worktimelist").hide();$("#worktimein").show();}
	function cancelmob(){ $("#commander").show(); $("#worktimein").hide(); 	$("#worktimelist").hide(); }
	function showworklist(){ $("#commander").hide();$("#worktimein").hide();$("#worktimelist").show();}
	var post_id;
	function saveworkstart(post_id){$.ajax({type:'POST',url: '/modules/mobile/ajax_saveworkstart.php',data:'post_id='+post_id,success: function(data){console.log("must be quit");}});}
	function setworkend(){$.ajax({type:'POST',url: '/modules/mobile/ajax_setworkend.php',	data:'post_id=1',	success: function(data){ setTimeout( function() { logout()}, 1000);	}});}
	var Login2 = function() {  return {
		init: function() { var formLogin       = $('#form-login2'), formRegister    = $('#form-register'); $('#submitbutton').click(function(){ auth_dologin2();}); $('#ljels').change(function(){ auth_dologin2();}); if (window.location.hash === '#register') { formLogin.hide(); formRegister.show();   } }};}();
	$(function(){ 	Login2.init(); 	});
</script>
</body></html>
