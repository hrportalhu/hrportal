<?
    
    //$screen=getscreenelems('bookkeeping','pays');
    include dirname(__FILE__).("/inc.pays.php");
   

    naplo('info','user','Fizetési módok betöltése');
    $_SESSION['planetsys']['actpage']=$screen;

?>
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Fizetési módok
            </h1>
        </div>
    </div>
<?

		if(isset($uritags[3]) && $uritags[3]!=""){
			if($uritags[3]=="new"){
				?>
				<!-- Ez egy elem  -->		
				<ul class="breadcrumb breadcrumb-top">
					<li><a href="/">Rendszer</a></li>
					<li><a href="/modules/bookkeeping/pays">Fizetési módok</a></li>
					<li><a href="/new">Új fizetési mód felvitele</a></li>
				</ul>
				<div class="block full">
				<?
					echo "<div style=\"margin-top:1px;\">
							<div class='inputelem'>
								<span></span>
								<span style=\"font-size:14px;\">Új fizetési mód</span>
							</div>".genFormJ("newpays",$_SESSION['planetsys']['actpage'],"",$_SESSION['planetsys']['screencmd']).	"
							<div class=\"form-group form-actions\">
								<div class=\"col-md-9 col-md-offset-3\">
									<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"savenewbutton\" onclick=\"savenewscreenpays();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
								</div>
							</div>
							<div style=\"clear:both;\"></div></div>	";
				?>
					</div>	
				</div>	
				<?php include 'inc/page_footer.php'; ?>
				<?php include 'inc/template_scripts.php'; ?>
				<script src="modules/bookkeeping/js/inc_pays.js"></script>
				<?php include 'inc/template_end.php'; ?>	
				<?	
			}
			else{
				
				//echo "select * from functions where id='".$uritags[2]."'";
				$mypays=db_row("select * from bookkeepsystem_payments where id='".$uritags[3]."'");
				if($mypays['id']==''){
					die("Helytelen hozzáférés!");
				}
				else{
	?>
				<!-- Ez egy elem  -->		
				<ul class="breadcrumb breadcrumb-top">
					<li><a href="/">Rendszer</a></li>
					<li><a href="moduls/bookkeeping/pays">Fizetési módok</a></li>
					<li><a href="/<?=$URI;?>"><?=$mypays['name'];?></a></li>
				</ul>
				<div class="block full">
								   
						<!-- Block Tabs Title -->

						<div class="block-title">
							<h2><strong><?=$mypays['name'];?></strong> fizetési mód oldala</h2>
							<ul class="nav nav-tabs" data-toggle="tabs">
								<li class="active"><a href="#tab-1" data-toggle="tooltip" title="view"><i class="gi gi-eye_open"></i></a></li>
								<li><a href="#tab-2" data-toggle="tooltip" title="edit"><i class="gi gi-pencil"></i></a></li>
								<li><a href="#tab-3" data-toggle="tooltip" title="upload"><i class="gi gi-floppy_open"></i></a></li>
								<li><a href="#tab-4" data-toggle="tooltip" title="comments"><i class="fa fa-comments-o"></i></a></li>
								<li><a href="#tab-5" data-toggle="tooltip" title="print"><i class="fa fa-print"></i></a></li>
								
							</ul>
						</div>
						<!-- END Block Tabs Title -->

						<!-- Tabs Content -->
						<div class="tab-content">

							<div class="tab-pane active" id="tab-1">
								<div class="col-md-6">
									<dl class="dl-horizontal">
										<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$mypays['id']); ?></dd>
										<?
											echo showRecord($_SESSION['planetsys']['actpage'],$mypays,"1")."<br />";
										?>
									</dl>	
								</div>
								<div style="clear:both;height:1px;"></div>

							</div>
							<div class="tab-pane" id="tab-2">
								<?
									echo "<div style=\"margin-top:1px;\">
											<div class='inputelem'>
												<span>Sorszám: </span>
												<span style=\"font-size:14px;\">".sprintf('%1$08d',$mypays['id'])."</span>
											</div>".genFormJ("editpays",$_SESSION['planetsys']['actpage'],$mypays,$_SESSION['planetsys']['screencmd']).	"
											<div class=\"form-group form-actions\">
												<div class=\"col-md-9 col-md-offset-3\">
													<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditscreenpayss(".$mypays['id'].")\"><i class=\"fa fa-angle-right\"></i> Ment</button>
												</div>
											</div>
											</div>
											<div style=\"clear:both;\"></div></div>";
								?>
							</div>
							<div class="tab-pane" id="tab-3">
									<?
									echo "<div>".uploadplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mypays['id'],"Dokumentum feltöltés")."</div>";
								   ?> 
							</div>
							<div class="tab-pane" id="tab-4">
									<?
										echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mypays['id'],"Hozzászólások")."</div>";
									?>
									
							</div>
							</div>
							<div class="tab-pane" id="tab-5">
								<div id="printdiv"></div>
							
							</div>
					
						</div>
						<!-- END Tabs Content -->
					</div>
					<!-- END Block Tabs -->
						
				</div>
				
				<?php include 'inc/page_footer.php'; ?>
				<?php include 'inc/template_scripts.php'; ?>
				<script src="modules/bookkeeping/js/inc_pays.js"></script>

				<script >
					showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mypays['id'];?>,'Dokumentum feltöltés','filelists');
					showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mypays['id'];?>,'Dokumentum feltöltés','filelists');
								
			
				</script>
				<script>$(function(){ paysDatatables.init(); });</script>	
			

				<?php include 'inc/template_end.php'; ?>		
	<?
				}
			}
		}
		else{
			//print_r($_SESSION['planetsys'])
?>    
	<!-- Ez a lista  -->
				<ul class="breadcrumb breadcrumb-top">
					<li><a href="/">Rendszer</a></li>
					<li><a href="/<?=$URI;?>">Fizetési módok</a></li>
				</ul>
				<!-- END Datatables Header -->

				<!-- Datatables Content -->
				<div class="block full">
					<div class="block-title">
						<h2><strong>Fizetési</strong>  módok</h2>
					</div>
					

					<div class="table-responsive">
						<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
							<thead>
								<tr>
									<?
										foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
											if($screen['elements'][$f]['textname']!=""){
												echo"<th>".$screen['elements'][$f]['textname']."</th>";
											}
										}
										//echo"<th>Művelet</th>";
										?>
								</tr>
							</thead>
							<tbody ></tbody>
						</table>
					</div>
				</div>
				<!-- END Datatables Content -->
			</div>
			<!-- END Page Content -->



			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>

			<!-- Load and execute javascript code used only in this page -->
			<script src="modules/bookkeeping/js/inc_pays.js"></script>
			<script>$(function(){ paysDatatables.init(); });</script>

			<?php include 'inc/template_end.php'; ?>
<?
}
?>

