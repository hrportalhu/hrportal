<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Helprendszer
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/core/content">Segítségnyújtó </a></li>
				<li><a href="/new">Új segítség szöveg</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új segítség szöveg</strong></h2>
				</div>
			<?
				echo "<form id=\"newcontent\" name=\"newcontent\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";

						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"name\">Tartalom neve:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" placeholder=\"Tartalom elnevezése\" />																		
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"otherazon\">Spaciális hivatkozás:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"otherazon\" name=\"otherazon\" class=\"form-control\" placeholder=\"Spaciális hivatkozás\" />																		
							</div>
						</div>";
						

						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"page\">Oldal:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"page\" name=\"page\" class=\"form-control\" placeholder=\"Oldal\" />																		
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"func\">Funkció:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"func\" name=\"func\" class=\"form-control\" placeholder=\"Funkció\" />																		
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"modul\">Modul:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"modul\" name=\"modul\" class=\"form-control\" placeholder=\"Modul\" />																		
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"tartalom\">Tartalom:</label>
							
								<div class=\"col-md-9\">
							<textarea id=\"tartalom\" name=\"tartalom\" class=\"ckeditor\" placeholder=\"Tartalom...\"></textarea>
						</div>
							
						</div>";
						
						echo "<div class=\"form-group\" >
							<label class=\"col-md-3 control-label\" for=\"status\">Státusz:</label>
							<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span></label>
						</div>";
						
						echo "<div style=\"clear:both;height:10px;\"></div>";
						
						echo "<div class=\"form-group form-actions\">
							<div class=\"col-md-9 col-md-offset-3\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewcontent();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>";
						
				echo "</div>
				</form>";
			?>
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<script src="js/ckeditor/ckeditor.js"></script>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/core/js/inc_content.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?	
		}
		else{
			
			//echo "select * from functions where id='".$uritags[2]."'";
			$mycontent=db_row("select * from content where id='".$uritags[3]."'");
			if($mycontent['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="modules/core/content">Szöveg</a></li>
				<li><a href="/<?=$URI;?>"><?=$mycontent['name'];?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->
					<div class="block-title">
						<h2><strong><?=$mycontent['name'];?></strong> Szöveg oldal</h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
							if(priv("text","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("text","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							if(priv("text","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("text","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
							if(priv("text","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}	
							?>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="col-md-6">
								<dl class="dl-horizontal">
									<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$mycontent['id']); ?></dd>
									<dt>Név:</dt><dd><? echo $mycontent['name']; ?></dd>
									<dt>Modul:</dt><dd><? echo $mycontent['modul']; ?></dd>
									<dt>Funkció:</dt><dd><? echo $mycontent['func']; ?></dd>
									<dt>Oldal:</dt><dd><? echo $mycontent['page']; ?></dd>
									<dt>Speciális hivatkozás:</dt><dd><? echo $mycontent['otherazon']; ?></dd>
									<dt>Tartalom:</dt><dd><? echo $mycontent['tartalom']; ?></dd>
									<dt>Státusz:</dt><dd><? if($mycontent['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
								</dl>	
							</div>
							<div class="col-md-6">
								<?php
									$pic=db_one("select filename from files where modul='core' and type='content' and type_id='".$mycontent['id']."' order by cover desc");
									if(is_file($pic)){
										echo"<div class=\"col-sm-3\"><img src=\"/".$pic."\" alt=\"avatar\" width=\"450px\"></div>";
									}
									else{
										echo"<img src=\"img/placeholders/avatars/avatar9@2x.jpg\" alt=\"avatar\">";
									}
								?>
							</div>
							<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-2">
						<?
							echo"<form id=\"editcontent\" name=\"editcontent\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
							echo "<div style=\"margin-top:1px;\">";
							
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"name\">Új szöveg:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" value=\"".$mycontent['name']."\" />																		
									</div>
								</div>";
								echo "<div class=\"form-group\">
										<label class=\"col-md-3 control-label\" for=\"otherazon\">Spaciális hivatkozás:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"otherazon\" name=\"otherazon\" class=\"form-control\" value=\"".$mycontent['otherazon']."\"  />																		
										</div>
									</div>";
									

									echo "<div class=\"form-group\">
										<label class=\"col-md-3 control-label\" for=\"page\">Oldal:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"page\" name=\"page\" class=\"form-control\" value=\"".$mycontent['page']."\" />																		
										</div>
									</div>";
									
									echo "<div class=\"form-group\">
										<label class=\"col-md-3 control-label\" for=\"func\">Funkció:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"func\" name=\"func\" class=\"form-control\" value=\"".$mycontent['func']."\" />																		
										</div>
									</div>";
									
									echo "<div class=\"form-group\">
										<label class=\"col-md-3 control-label\" for=\"modul\">Modul:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"modul\" name=\"modul\" class=\"form-control\" value=\"".$mycontent['modul']."\" />																		
										</div>
									</div>";
									echo "<div class=\"form-group\">
										<label class=\"col-md-3 control-label\" for=\"tartalom\">Tartalom:</label>
										
											<div class=\"col-md-9\">
										<textarea id=\"tartalom\" name=\"tartalom\" class=\"ckeditor\" >".$mycontent['tartalom']."</textarea>
									</div>
										
									</div>";
								echo "<div class=\"form-group\" >
									<label class=\"col-md-3 control-label\" for=\"status\">Státusz:</label>";
									$st="";if($mycontent['status']==1){ $st=" checked "; }
									echo "<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\"".$st." ><span></span></label>
								</div>";
								
								echo "<div class=\"form-group form-actions\">
									<div class=\"col-md-9 col-md-offset-3\">
										<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditcontent(".$mycontent['id'].")\"><i class=\"fa fa-angle-right\"></i> Ment</button>
									</div>
								</div>";
									
							echo "</div>
							</form>";
						?>
						</div>
						<div class="tab-pane" id="tab-3">
								<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mycontent['id'],"Dokumentum feltöltés")."</div>";
								?> 
								<div id="filelists"></div>
								<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mycontent['id'],"Hozzászólások")."</div>";
								?>
						</div>
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>
				
					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
					
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<script src="js/ckeditor/ckeditor.js"></script>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/core/js/inc_content.js"></script>

			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mycontent['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$mycontent['id'];?>,'Dokumentum feltöltés','filelists');
			</script>
			
		
			<?php include 'inc/template_end.php'; ?>		
	<?
			}
		}
	}
	else{
		//print_r($_SESSION['planetsys'])
	?>    
	<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Szöveg</a></li>
			</ul>
			<!-- END Datatables Header -->

			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Hibakezelő</strong> lista</h2>
				</div>
				

				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							//echo"<th>Művelet</th>";
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/core/js/inc_content.js"></script>
		<script>$(function(){ contentDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>
