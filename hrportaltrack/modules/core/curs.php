<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Pénznemek
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/core/curs">Pénznemek</a></li>
				<li><a href="/new">Új pénznem felvitele</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új pénznem felvitele</strong></h2>
				</div>
			<?
				echo "<form id=\"newcurs\" name=\"newcurs\"  method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";
						
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"name\">Pénznem:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" placeholder=\"Pénznem\"  />																		
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"shortname\">Rövid pénznem:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"shortname\" name=\"shortname\" class=\"form-control\" placeholder=\"Rövid pénznem\"  />																		
						</div>
					</div>";
					
					echo "<div class=\"form-group\" >
						<label class=\"col-md-3 control-label\" for=\"base\">Alappénznem:</label>
						<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"base\" name=\"base\" checked><span></span></label>
					
						<label class=\"col-md-3 control-label\" for=\"status\">Státusz:</label>
						<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span></label>
					</div>";
					
					echo "<div style=\"clear:both;height:10px;\"></div>
					<div class=\"form-group form-actions\">
						<div class=\"col-md-9 col-md-offset-3\">
							<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewcurs();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
						</div>
					</div>";
				echo "</div>
				</form>";
			?>
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/core/js/inc_curs.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?	
		}
		else{
			
			$mycurs=db_row("select * from curencys where id='".$uritags[3]."'");
			if($mycurs['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="modules/core/curs">Pénznemek</a></li>
				<li><a href="/<?=$URI;?>"><?=$mycurs['name'];?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->
					<div class="block-title">
						<h2><strong><?=$mycurs['name'];?></strong> pénznem oldala</h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
							if(priv("curs","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("curs","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							if(priv("curs","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("curs","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
							if(priv("curs","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}
							?>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
						<div class="col-md-6">
							<dl class="dl-horizontal">
								<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$mycurs['id']); ?></dd>
								<dt>Pénznem:</dt><dd><? echo $mycurs['name']; ?></dd>
								<dt>Rövid pénznem:</dt><dd><? echo $mycurs['shortname']; ?></dd>
								<dt>Alappénznem:</dt><dd><? if($mycurs['base']==1){echo"Igen";}else{echo"Nem";} ?></dd>
								<dt>Státusz:</dt><dd><? if($mycurs['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
							</dl>	
						</div>
						<div class="col-md-6">
							<?php
								$pic=db_one("select filename from files where modul='core' and type='curs' and type_id='".$mycurs['id']."' order by cover desc");
								if(is_file($pic)){
									echo"<div class=\"col-sm-3\"><img src=\"/".$pic."\" alt=\"avatar\" width=\"450px\"></div>";
								}
								else{
									echo"<img src=\"img/placeholders/avatars/avatar9@2x.jpg\" alt=\"avatar\">";
								}
							?>
						</div>
						<div style="clear:both;height:1px;"></div>

					</div>
						<div class="tab-pane" id="tab-2">
						<?
			
						echo "<form id=\"editcurs\" name=\"editcurs\"  method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
						echo "<div style=\"margin-top:1px;\">";
								
							echo "<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"name\">Pénznem:</label>
								<div class=\"col-md-9\">
									<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" value=\"".$mycurs['name']."\"/>																		
								</div>
							</div>";
							
							echo "<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"shortname\">Rövid pénznem:</label>
								<div class=\"col-md-9\">
									<input type=\"text\" id=\"shortname\" name=\"shortname\" class=\"form-control\" value=\"".$mycurs['shortname']."\"/>																		
								</div>
							</div>";
							
							echo "<div class=\"form-group\" >
								<label class=\"col-md-3 control-label\" for=\"status\">Alappénznem:</label>";
								$ba="";if($mycurs['base']==1){ $ba=" checked "; }
								echo "<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"base\" name=\"base\"".$ba." ><span></span></label>

								<label class=\"col-md-3 control-label\" for=\"status\">Státusz:</label>";
								$st="";if($mycurs['status']==1){ $st=" checked "; }
								echo "<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\"".$st." ><span></span></label>
							</div>";
							
							echo "<div style=\"clear:both;height:10px;\"></div>
							<div class=\"form-group form-actions\">
								<div class=\"col-md-9 col-md-offset-3\">
									<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditcurs('".$mycurs['id']."');\"><i class=\"fa fa-angle-right\"></i> Ment</button>
								</div>
							</div>";
							
						echo"</div>
						</form>";
						?>
						</div>
						<div class="tab-pane" id="tab-3">
								<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mycurs['id'],"Dokumentum feltöltés")."</div>";
								?> 
							   <div id="filelists"></div>
							   <div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
								<?
									echo "<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mycurs['id'],"Hozzászólások")."</div>";
								?>
						</div>
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>
				
					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
					
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/core/js/inc_curs.js"></script>

			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mycurs['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',,<?=$mycurs['id'];?>,'Dokumentum feltöltés','filelists');
			</script>
			<script>$(function(){ cursDatatables.init(); });</script>	

			<?php include 'inc/template_end.php'; ?>		
		<?
			}
		}
	}
	else{
		//print_r($_SESSION['planetsys'])
		?>    
		<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Pénznemek</a></li>
			</ul>
			<!-- END Datatables Header -->

			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Mappa</strong> lista</h2>
				</div>

				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
								<?
								foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
									if($screen['elements'][$f]['textname']!=""){
										echo"<th>".$screen['elements'][$f]['textname']."</th>";
									}
								}
								//echo"<th>Művelet</th>";
								?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/core/js/inc_curs.js"></script>
		<script>$(function(){ cursDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>
