<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-parents"></i>Funkciókezelő	
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/core/functions">Funkciók</a></li>
				<li><a href="/new">Új funkció felvitele</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új felhasználó felvitele</strong></h2>
				</div>
			<?
				$parents=db_all("select id, name from functions where status=1 order by name");
				
				echo "<form id=\"newfunction\" name=\"newfunction\"  method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";

						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"name\">Funkció neve</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" placeholder=\"Funkció neve\" />
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"menuname\">Menü neve</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"menuname\" name=\"menuname\" class=\"form-control\" placeholder=\"Menü neve\" />
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"funcname\">Művelet neve</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"funcname\" name=\"funcname\" class=\"form-control\" placeholder=\"Művelet neve\" />
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"menuorder\">Menü sorrend</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"menuorder\" name=\"menuorder\" class=\"form-control\" placeholder=\"Menü sorrend\" />
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"path\">Elérési út</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"path\" name=\"path\" class=\"form-control\" placeholder=\"Elérési út\" />
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"parent\">Szülő menü:</label>
							<div class=\"col-md-9\">
								<select id=\"parent\" name=\"parent\" class=\"form-control\" placeholder=\"Szülő menü\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($parents);$i++){
									echo"<option value=\"".$parents[$i]['id']."\" >".$parents[$i]['name']."</option>";	
								}
								echo"</select>
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"icon\">Ikon</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"icon\" name=\"icon\" class=\"form-control\" placeholder=\"Ikon\" />
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"menupath\">Menü elérési út</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"menupath\" name=\"menupath\" class=\"form-control\" placeholder=\"Menü elérési út\" />
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"subfunction_name\">Alfunkció</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"subfunction_name\" name=\"subfunction_name\" class=\"form-control\" placeholder=\"Alfunkció\" />
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"subfunction_code\">Alfunkció kód</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"subfunction_code\" name=\"subfunction_code\" class=\"form-control\" placeholder=\"Alfunkció kód\" />
							</div>
						</div>";
						
						echo "<div class=\"form-group\" >
							<label class=\"col-md-3 control-label\" for=\"status\">Státusz:</label>
							<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span></label>
						</div>";
						
						echo "<div style=\"clear:both;height:10px;\"></div>";
						
						echo "<div class=\"form-group form-actions\">
							<div class=\"col-md-9 col-md-offset-3\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewfunctions();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>";
						
					echo "</div>
				</form>";
			?>
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="/modules/core/js/inc_functions.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?
		}
		else{
			$myfunctions=db_row("select * from functions where id='".$uritags[3]."'");
			if($myfunctions['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/core/functions">Funkciók</a></li>
				<li><a href="/<?=$URI;?>"><?=$myfunctions['name'];?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->
					<div class="block-title">
						<h2><strong><?=$myfunctions['name'];?></strong> Funkció oldala</h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<li class="active"><a href="#tab-1" data-toggle="tooltip" title="view"><i class="gi gi-eye_open"></i></a></li>
							<li><a href="#tab-2" data-toggle="tooltip" title="edit"><i class="gi gi-pencil"></i></a></li>
							<li><a href="#tab-3" data-toggle="tooltip" title="upload"><i class="gi gi-floppy_open"></i></a></li>
							<li><a href="#tab-4" data-toggle="tooltip" title="comments"><i class="fa fa-comments-o"></i></a></li>
							<li><a href="#tab-5" data-toggle="tooltip" title="print"><i class="fa fa-print"></i></a></li>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="col-md-6">
								<dl class="dl-horizontal">
									<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$myfunctions['id']); ?></dd>
									<dt>Néve:</dt><dd><? echo $myfunctions['name']; ?></dd>
									<dt>Menü neve:</dt><dd><? echo $myfunctions['menuname']; ?></dd>
									<dt>Művelet neve:</dt><dd><? echo $myfunctions['funcname']; ?></dd>
									<dt>Menü sorrend:</dt><dd><? echo $myfunctions['menuorder']; ?></dd>
									<dt>Elérési út:</dt><dd><? echo $myfunctions['path']; ?></dd>
									<dt>Szülő menü:</dt><dd><? echo idchange("functions","name",$myfunctions['parent']); ?></dd>
									<dt>Ikon:</dt><dd><? echo $myfunctions['icon']; ?></dd>
									<dt>Menü elérési út:</dt><dd><? echo $myfunctions['menupath']; ?></dd>
									<dt>Alfunkció:</dt><dd><? echo $myfunctions['subfunction_name']; ?></dd>
									<dt>Alfunkció kódja:</dt><dd><? echo $myfunctions['subfunction_code']; ?></dd>
									<dt>Státusz:</dt><dd><? if($myfunctions['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
								</dl>	
							</div>
							<div class="col-md-6">
								<?php
									$pic=db_one("select filename from files where modul='core' and type='functions' and type_id='".$myfunctions['id']."' order by cover desc");
									if(is_file($pic)){
										echo"<div class=\"col-sm-3\"><img src=\"/".$pic."\" alt=\"avatar\" width=\"450px\"></div>";
									}
									else{
										echo"<img src=\"img/placeholders/avatars/avatar9@2x.jpg\" alt=\"avatar\">";
									}
								?>
							</div>
							<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-2">
							<?
							$parents=db_all("select id, name, subfunction_name from functions where status=1 order by name");
							
							echo"<form id=\"editfunction\" name=\"editfunction\"  method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
								echo "<div style=\"margin-top:1px;\">";
										echo "<div class=\"form-group\">
											<label class=\"col-md-3 control-label\" for=\"id\">Sorszám: </label>
											<div class=\"col-md-9\">
												<input type=\"text\" id=\"id\" name=\"id\" class=\"form-control\" value=\"".$myfunctions['id']."\" disabled >
											</div>
										</div>";
										
										echo "<div class=\"form-group\">
											<label class=\"col-md-3 control-label\" for=\"name\">Funkció neve</label>
											<div class=\"col-md-9\">
												<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" value=\"".$myfunctions['name']."\" >
											</div>
										</div>";
										
										echo "<div class=\"form-group\">
											<label class=\"col-md-3 control-label\" for=\"menuname\">Menü neve</label>
											<div class=\"col-md-9\">
												<input type=\"text\" id=\"menuname\" name=\"menuname\" class=\"form-control\" value=\"".$myfunctions['menuname']."\" >
											</div>
										</div>";
										
										echo "<div class=\"form-group\">
											<label class=\"col-md-3 control-label\" for=\"funcname\">Művelet neve</label>
											<div class=\"col-md-9\">
												<input type=\"text\" id=\"funcname\" name=\"funcname\" class=\"form-control\" value=\"".$myfunctions['funcname']."\" >
											</div>
										</div>";
										
										echo "<div class=\"form-group\">
											<label class=\"col-md-3 control-label\" for=\"menuorder\">Menü sorrend</label>
											<div class=\"col-md-9\">
												<input type=\"text\" id=\"menuorder\" name=\"menuorder\" class=\"form-control\" value=\"".$myfunctions['menuorder']."\" >
											</div>
										</div>";
										
										echo "<div class=\"form-group\">
											<label class=\"col-md-3 control-label\" for=\"path\">Elérési út</label>
											<div class=\"col-md-9\">
												<input type=\"text\" id=\"path\" name=\"path\" class=\"form-control\" value=\"".$myfunctions['path']."\" >
											</div>
										</div>";
										
										echo "<div class=\"form-group\">
											<label class=\"col-md-3 control-label\" for=\"parent\">Szülő menü:</label>
											<div class=\"col-md-9\">
												<select id=\"parent\" name=\"parent\" class=\"form-control\" value=\"".$functions['parent']."\" >
												<option value=\"0\">Kérlek válassz</option>";
												for($i=0;$i<count($parents);$i++){
													$sel="";if($parents[$i]['id']==$myfunctions['parent']){$sel=" selected ";}
													if($parents[$i]['subfunction_name']!=""){
														echo"<option value=\"".$parents[$i]['id']."\" ".$sel.">".$parents[$i]['name']." -> ".$parents[$i]['subfunction_name']." </option>";	
													}
													else{
														echo"<option value=\"".$parents[$i]['id']."\" ".$sel.">".$parents[$i]['name']." </option>";	
													}
												}
												echo"</select>
											</div>
										</div>";
										
										echo "<div class=\"form-group\">
											<label class=\"col-md-3 control-label\" for=\"icon\">Ikon</label>
											<div class=\"col-md-9\">
												<input type=\"text\" id=\"icon\" name=\"icon\" class=\"form-control\" value=\"".$myfunctions['icon']."\" >
											</div>
										</div>";
										
										echo "<div class=\"form-group\">
											<label class=\"col-md-3 control-label\" for=\"menupath\">Menü elérési út</label>
											<div class=\"col-md-9\">
												<input type=\"text\" id=\"menupath\" name=\"menupath\" class=\"form-control\" value=\"".$myfunctions['menupath']."\" >
											</div>
										</div>";
										
										echo "<div class=\"form-group\">
											<label class=\"col-md-3 control-label\" for=\"subfunction_name\">Alfunkció</label>
											<div class=\"col-md-9\">
												<input type=\"text\" id=\"subfunction_name\" name=\"subfunction_name\" class=\"form-control\" value=\"".$myfunctions['subfunction_name']."\" />
											</div>
										</div>";
										
										echo "<div class=\"form-group\">
											<label class=\"col-md-3 control-label\" for=\"subfunction_code\">Alfunkció kód</label>
											<div class=\"col-md-9\">
												<input type=\"text\" id=\"subfunction_code\" name=\"subfunction_code\" class=\"form-control\" value=\"".$myfunctions['subfunction_code']."\" />
											</div>
										</div>";
														
									echo "<div class=\"form-group\" >
										<label class=\"col-md-3 control-label\" for=\"status\">Státusz:</label>";
										$st="";if($myfunctions['status']==1){ $st=" checked "; }
										echo "<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\"".$st." ><span></span></label>
									</div>";
										
										echo "<div style=\"clear:both;height:10px;\"></div>";
										
										echo "<div class=\"form-group form-actions\">
											<div class=\"col-md-9 col-md-offset-3\">
												<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditfunctions(".$myfunctions['id'].")\"><i class=\"fa fa-angle-right\"></i> Ment</button>
												<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveneweditfunctions\" onclick=\"savenewfunc(".$myfunctions['id'].");\"><i class=\"fa fa-angle-right\"></i> Ment újként</button>
											</div>
										</div>";
										
								echo "</div>
								</form>";
							?>
					
						</div>
						<div class="tab-pane" id="tab-3">
							<?
								echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['functions_id'],$myfunctions['id'],"Dokumentum feltöltés")."</div>";
							?> 
							<div id="filelists"></div>
							<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
							<?
								echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['functions_id'],$myfunctions['id'],"Hozzászólások")."</div>";
							?>
						</div>
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>

					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
			</div>
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="/modules/core/js/inc_functions.js"></script>
			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>','',<?=$myfunctions['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>',<?=$myfunctions['id'];?>,'Dokumentum feltöltés','filelists');
			</script>
			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
?>    
<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Funkciókezelő</a></li>
			</ul>
			<!-- END Datatables Header -->

			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Funkció</strong> lista</h2>
				</div>
				
				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
								<?
									foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
										if($screen['elements'][$f]['textname']!=""){
											echo"<th>".$screen['elements'][$f]['textname']."</th>";
										}
									}
								?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/core/js/inc_functions.js"></script>
		<script>$(function(){ FunctionsDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>
