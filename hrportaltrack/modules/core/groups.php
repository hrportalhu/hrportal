<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-parents"></i>Csoportkezelő	
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/core/groups">Csoportok</a></li>
				<li><a href="/new">Új csoport felvitele</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új csoport felvitele</strong></h2>
				</div>
			<?
				echo "<form id=\"newgroup\" name=\"newgroup\"  method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"name\">Csoport neve</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" placeholder=\"Csoport neve\" />
							</div>
						</div>";
						
						echo "<div class=\"form-group\" >
							<label class=\"col-md-3 control-label\" for=\"status\">Státusz:</label>
							<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span></label>
						</div>";
						
						echo "<div style=\"clear:both;height:10px;\"></div>";
						
						echo "<div class=\"form-group form-actions\">
							<div class=\"col-md-9 col-md-offset-3\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"savenewscreengroup\" onclick=\"savenewgroup();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>";
						
					echo "</div>
				</form>";
			?>
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="/modules/core/js/inc_groups.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?	
		}
		else{
			$mygroups=db_row("select * from groups where id='".$uritags[3]."'");
			if($mygroups['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/core/groups">Csoportok</a></li>
				<li><a href="/<?=$URI;?>"><?=$mygroups['name'];?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->
					<div class="block-title">
						<h2><strong><?=$mygroups['name'];?></strong> Csoport oldala</h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
							if(priv("groups","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("groups","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							if(priv("groups","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("groups","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
							if(priv("groups","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}
							?>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="col-md-6">
								<dl class="dl-horizontal">
									<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$mygroups['id']); ?></dd>
									<dt>Néve:</dt><dd><? echo $mygroups['name']; ?></dd>
									<dt>Státusz:</dt><dd><? if($mygroups['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
								</dl>	
							</div>
							<div class="col-md-6">
								<?php
									$pic=db_one("select filename from files where modul='core' and type='groups' and type_id='".$mygroups['id']."' order by cover desc");
									if(is_file($pic)){
										echo"<div class=\"col-sm-3\"><img src=\"/".$pic."\" alt=\"avatar\" width=\"450px\"></div>";
									}
									else{
										echo"<img src=\"img/placeholders/avatars/avatar9@2x.jpg\" alt=\"avatar\">";
									}
								?>
							</div>
							<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-2">
							<?
							echo"<form id=\"editgroup\" name=\"editgroup\"  method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
								echo "<div style=\"margin-top:1px;\">";
										echo "<div class=\"form-group\">
											<label class=\"col-md-3 control-label\" for=\"id\">Sorszám: </label>
											<div class=\"col-md-9\">
												<input type=\"text\" id=\"id\" name=\"id\" class=\"form-control\" value=\"".$mygroups['id']."\" disabled >
											</div>
										</div>";
										
										echo "<div class=\"form-group\">
											<label class=\"col-md-3 control-label\" for=\"name\">Csoport neve</label>
											<div class=\"col-md-9\">
												<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" value=\"".$mygroups['name']."\" >
											</div>
										</div>";

										echo "<div class=\"form-group\" >
											<label class=\"col-md-3 control-label\" for=\"status\">Státusz:</label>";
											$st="";if($mygroups['status']==1){ $st=" checked "; }
											echo "<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\"".$st." ><span></span></label>
										</div>";
										
										echo "<div style=\"clear:both;height:10px;\"></div>";
										
										echo "<div class=\"form-group form-actions\">
											<div class=\"col-md-9 col-md-offset-3\">
												<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditscreengroups(".$mygroups['id'].")\"><i class=\"fa fa-angle-right\"></i> Ment</button>
											</div>
										</div>";
							echo "</div>
							</form>";
							?>
						</div>
						<div class="tab-pane" id="tab-3">
								<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mygroups['id'],"Dokumentum feltöltés")."</div>";
								?> 
								<div id="filelists"></div>
								<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mygroups['id'],"Hozzászólások")."</div>";
								?>
						</div>
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>

					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
			</div>
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="/modules/core/js/inc_groups.js"></script>
			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>','',<?=$mygroups['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>',<?=$mygroups['id'];?>,'Dokumentum feltöltés','filelists');
			</script>
			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
?>    
<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Csoport kezelő</a></li>
			</ul>
			<!-- END Datatables Header -->

			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Csoport</strong> lista</h2>
				</div>

				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->
		
		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/core/js/inc_groups.js"></script>
		<script>$(function(){ GroupsDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>
