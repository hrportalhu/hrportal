<?php
$most=date('Y-m-d',time());

$screen=array(
	"elements"=>array(
		"id"=>array(
			"name"=>"id",	
			"textname"=>"Sorszám",	
			"type"=>"text50",	
			"type_param"=>"",	
			"help"=>"",	
			"defval"=>"",	
			"connected"=>0,	
			"con_table"=>"",	
			"con_table_shortname"=>"",	
			"con_table_fields"=>"",	
			"width"=>"",	
			"deflist"=>0,	
			"scrdb"=>0,	
			"scrcmd"=>1	
		),
		"name"=>array(
			"name"=>"name",	
			"textname"=>"Szöveg neve",	
			"type"=>"text50",	
			"type_param"=>"",	
			"help"=>"",	
			"defval"=>"",	
			"connected"=>0,	
			"con_table"=>"",	
			"con_table_shortname"=>"",	
			"con_table_fields"=>"",	
			"width"=>"",	
			"deflist"=>1,	
			"scrdb"=>1,	
			"scrcmd"=>1	
		),
		"status"=>array(
			"name"=>"status",	
			"textname"=>"Státusz",	
			"type"=>"status",	
			"type_param"=>"",	
			"help"=>"",	
			"defval"=>"",	
			"connected"=>0,	
			"con_table"=>"",	
			"con_table_shortname"=>"",	
			"con_table_fields"=>"",	
			"width"=>"",	
			"deflist"=>0,	
			"scrdb"=>1,	
			"scrcmd"=>1	
		)
	),
	"tableprefix"=>"",
	"modulname"=>"core",
	"funcname"=>"content",
	"mylimit"=>"20",
	"mywidth"=>"",
	"scrtable"=>"content",
	"scrtable_shortname"=>"c",
);

getlistoptions($screen);
$screen['screenlist']=$_SESSION['planetsys']['listoption']['screenlist'];
$screen['screendb']=$_SESSION['planetsys']['screendb'];

?>
