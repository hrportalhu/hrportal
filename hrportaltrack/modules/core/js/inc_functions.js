/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */

var FunctionsDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/core/functions/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"aaSorting": [[ 0, "desc" ]],
				"iDisplayLength": 30,
				"bDestroy": true,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/core/ajax_functions_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					aoData.push({"name": "filterfunc_id", "value":$("#filterfunc_id").val() });
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });
            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveeditfunctions(elemid){
	var valid = f_c($('#editfunction'));
	if(valid){
		f_xml(ServerName +'/modules/core/ajax_functions_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editfunction').serialize(),'');
	}
}

function savenewfunc(elemid){
	if(confirm("Biztos létrehozza újként a funkciót")){
		var valid = f_c($('#editfunction'));
		if(valid){
			f_xml(ServerName +'/modules/core/ajax_functions_data.php','view=newedit&id=' + elemid +'&neweditsubmit='+elemid+'&' + $('#editfunction').serialize(),'');
		}
	}
}

function savenewfunctions(){
	var valid = f_c($('#newfunction'));
	if(valid){
		f_xml(ServerName +'/modules/core/ajax_functions_data.php','newsubmit=1&' + $('#newfunction').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/core/ajax_functions_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}
