<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-parents"></i>Naplók
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){

			$mylogs=db_row("select * from logs where id='".$uritags[3]."'");
			if($mylogs['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/core/logs">Naplók</a></li>
				<li><a href="/<?=$URI;?>"><?=$mylogs['id'];?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->
					<div class="block-title">
						<h2><strong><?=$mylogs['id'];?></strong> naplóbejegyzés oldala</h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<li class="active"><a href="#tab-1" data-toggle="tooltip" title="view"><i class="gi gi-eye_open"></i></a></li>
							<li><a href="#tab-3" data-toggle="tooltip" title="upload"><i class="gi gi-floppy_open"></i></a></li>
							<li><a href="#tab-4" data-toggle="tooltip" title="comments"><i class="fa fa-comments-o"></i></a></li>
							<li><a href="#tab-5" data-toggle="tooltip" title="print"><i class="fa fa-print"></i></a></li>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="col-md-6">
								<dl class="dl-horizontal">
									<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$mylogs['id']); ?></dd>
									<dt>Felhasználó:</dt><dd><? echo $mylogs['user_name']; ?></dd>
									<dt>URL:</dt><dd><? echo $mylogs['url']; ?></dd>
									<dt>IP cím:</dt><dd><? echo $mylogs['ip']; ?></dd>
									<dt>Dátum:</dt><dd><? echo $mylogs['rdate']; ?></dd>
									<dt>Naplózási szint:</dt><dd><? echo $mylogs['loglevel']; ?></dd>
									<dt>Naplózás típusa:</dt><dd><? echo $mylogs['logtype']; ?></dd>
									<dt>Naplóbejegyzés:</dt><dd><? echo $mylogs['message']; ?></dd>
									<dt>Státusz:</dt><dd><? if($mylogs['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
								</dl>	
							</div>
							<div class="col-md-6">
								<?php
									$pic=db_one("select filename from files where modul='core' and type='logs' and type_id='".$mylogs['id']."' order by cover desc");
									if(is_file($pic)){
										echo"<div class=\"col-sm-3\"><img src=\"/".$pic."\" alt=\"avatar\" width=\"450px\"></div>";
									}
									else{
										echo"<img src=\"img/placeholders/avatars/avatar9@2x.jpg\" alt=\"avatar\">";
									}
								?>
							</div>
							<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-3">
								<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mylogs['id'],"Dokumentum feltöltés")."</div>";
								?> 
							   <div id="filelists"></div>
							   <div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mylogs['id'],"Hozzászólások")."</div>";
								?>
						</div>
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>

					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
			</div>
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="/modules/core/js/inc_logs.js"></script>
			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>','',<?=$mylogs['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$mylogs['id'];?>,'Dokumentum feltöltés','filelists');
			</script>
			<?php include 'inc/template_end.php'; ?>		
<?
			}
	}
	else{
?>    
	<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Napló kezelő</a></li>
			</ul>
			<!-- END Datatables Header -->

			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Bejegyzés</strong> lista</h2>
				</div>

				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/core/js/inc_logs.js"></script>
		<script>$(function(){ LogsDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>
