<?
	if(isset($_POST['realname']) && $_POST['realname']!=""){
		db_execute("update users set realname='".$_POST['realname']."',email='".$_POST['email']."' ,telnum='".$_POST['telnum']."' where id='".$_SESSION['planetsys']['user_id']."'");	
		//echo "update users set realname='".$_POST['realname']."',email='".$_POST['email']."' ,telnum='".$_POST['telnum']."' where id='".$_SESSION['planetsys']['user_id']."'";
		if($_POST['pass']!="" && strlen($_POST['pass'])>4){
			db_execute("update users set pass='".md5($_POST['pass'])."' where id='".$_SESSION['planetsys']['user_id']."'");	
		}
	}
?>

<?
if(isset($_FILES['file'])){
	$mypath=dirname(__FILE__).("/../../modules/system/system_files/".$_SESSION['planetsys']['user_id']);
	if(is_dir($mypath)){}else{mkdir($mypath,0755);}
	if (isset($_FILES['file']) ){
		$file = $_FILES['file']['tmp_name'];
		$newfilename=	generateCode(10)."userfile.jpg";
		move_uploaded_file($_FILES['file']['tmp_name'],$mypath."/".$newfilename);
	}
}
    naplo('info','user','Személyi beállítás kezelő  betöltése');
    $_SESSION['planetsys']['actpage']=$screen;
    $myuser=db_row("select * from users where id='".$_SESSION['planetsys']['user_id']."'");
?>
<!-- Page content -->
<div id="page-content">

    <!-- Product Edit Content -->
    <div class="row">
        <div class="col-lg-6">
            <!-- General Data Block -->
            <div class="block">
                <!-- General Data Title -->
                <div class="block-title">
                    <h2><i class="fa fa-pencil"></i> <strong>Felhasználói</strong> adataim</h2>
                </div>
                <!-- END General Data Title -->

                 <!-- Basic Form Elements Content -->
                <form action="/modules/core/mysetup" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" >
                    
                    <div class="form-group">
                        <label class="col-md-3 control-label">Felhasználónév</label>
                        <div class="col-md-9">
                            <p class="form-control-static"><?=$myuser['login'];?></p>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="realname">Név</label>
                        <div class="col-md-9">
                            <input type="text" id="realname" name="realname" class="form-control" placeholder="Valódi név" value="<?=$myuser['realname'];?>" >
                            <span class="help-block">Hogyan hívhatunk</span>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="email">E-mail cím</label>
                        <div class="col-md-9">
                            <input type="email" id="email" name="email" class="form-control" placeholder="E-mail cím" value="<?=$myuser['email'];?>">
                            <span class="help-block">Az e-mail címet ide kell írni</span>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="pass">Jelszó</label>
                        <div class="col-md-9">
                            <input type="password" id="pass" name="pass" class="form-control" placeholder="Jelszó">
                            <span class="help-block">Csak akkor írj ide jelszót, ha szeretnéd megváltoztatni a régit</span>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="telnum">Telefonszám</label>
                        <div class="col-md-9">
                            <input type="text" id="telnum" name="telnum" class="form-control" placeholder="Telefonszám" value="<?=$myuser['telnum'];?>">
                            <span class="help-block">Telefonszámod</span>
                        </div>
                    </div>
                   
                    <div class="form-group form-actions">
                        <div class="col-md-9 col-md-offset-3">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Ment</button>
                            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                        </div>
                    </div>
                </form>
                <!-- END Basic Form Elements Content -->
            </div>
            <!-- END General Data Block -->
        </div>
        <div class="col-lg-6">

		<?
			echo "<div>".uploadplace_new("core","users",$_SESSION['planetsys']['user_id'],$_SESSION['planetsys']['user_id'],"Dokumentum feltöltés","12")."</div>";
		?> 
		<div style="clear:both;height:1px;"></div>
		<div id="filelists"></div>
		<div style="clear:both;height:1px;"></div>

        </div>
    </div>
    <!-- END Product Edit Content -->
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>

<script >			
	howfilelist('core','users',<?=$_SESSION['planetsys']['user_id'];?>,'Dokumentum feltöltés','filelists');
</script>

<script src="js/pages/inc_mysetup.js"></script>
<?php include 'inc/template_end.php'; ?>	
