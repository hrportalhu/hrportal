<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-parents"></i>Felhasználókezelő	
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/core/users">Felhasználó</a></li>
				<li><a href="/new">Új felhasználó felvitele</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új felhasználó felvitele</strong></h2>
				</div>
			<?
				$groups=db_all("select id, name from groups where status=1 order by name");
				
				echo "<form id=\"newuser\" name=\"newuser\"  method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";
		
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"login\">Bejelentkező E-mail cím:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"login\" name=\"login\" class=\"form-control\" placeholder=\"Bejelentkezéshez használt E-mail cím\"  />																		</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"pass\">Jelszó:</label>
							<div class=\"col-md-9\">
								<input type=\"password\" id=\"pass\" name=\"pass\" class=\"form-control\" placeholder=\"Felhasználó jelszó\"  />																		</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"realname\">Név:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"realname\" name=\"realname\" class=\"form-control\" placeholder=\"Felhasználó neve\" />						
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"group_id\">Csoport:</label>
							<div class=\"col-md-9\">
								<select id=\"group_id\" name=\"group_id\" class=\"form-control\" placeholder=\"Felhasználó csoportja\"   >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($groups);$i++){
									echo"<option value=\"".$groups[$i]['id']."\" >".$groups[$i]['name']."</option>";	
								}
								echo"</select>
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"telnum\">Telefonszám:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"telnum\" name=\"telnum\" class=\"form-control\" placeholder=\"Felhasználó telefonszáma\"  />
								
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"email\">E-mail:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"email\" name=\"email\" class=\"form-control\" placeholder=\"Felhasználó E-mail címe\"  />
								
							</div>
						</div>";
						
						echo "<div class=\"form-group\" >
							<label class=\"col-md-3 control-label\" for=\"address\">Adminisztátor:</label>
							<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"customeradmin\" name=\"customeradmin\" checked><span></span></label>
							
							<label class=\"col-md-3 control-label\" for=\"address\">Szuper adminisztátor:</label>
							<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"superadmin\" name=\"superadmin\" checked><span></span></label>
							
							<label class=\"col-md-3 control-label\" for=\"address\">Státusz:</label>
							<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span></label>
						</div>";
						
						echo "<div style=\"clear:both;height:10px;\"></div>";
						
						echo "<div class=\"form-group form-actions\">
							<div class=\"col-md-9 col-md-offset-3\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewusers();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>";
						
				echo "</div>
				</form>";
			?>
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/core/js/inc_users.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?	
		}
		else{
			$myusers=db_row("select * from users where id='".$uritags[3]."'");
			if($myusers['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/core/users">Felhasznaló</a></li>
				<li><a href="/<?=$URI;?>"><?=$myusers['name'];?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->
					<div class="block-title">
						<h2><strong><?=$myusers['realname'];?></strong> Felhasználó oldala</h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
							if(priv("users","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("users","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							if(priv("users","privacy")){echo"<li><a href=\"#tab-6\" data-toggle=\"tooltip\" title=\"privacy\"><i class=\"gi gi-keys\"></i></a></li>";}
							if(priv("users","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("users","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
							if(priv("users","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}
							?>
							
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="col-md-6">
								<dl class="dl-horizontal">
									<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$myusers['id']); ?></dd>
									<dt>Néve:</dt><dd><? echo $myusers['realname']; ?></dd>
									<dt>Csoport:</dt><dd><? echo $myusers['group_id']; ?></dd>
									<dt>Telefonszám:</dt><dd><? echo $myusers['telnum']; ?></dd>
									<dt>E-mail:</dt><dd><? echo $myusers['email']; ?></dd>
									<dt>Adminisztrátor:</dt><dd><? if($myusers['customeradmin']==1){echo"Igen";}else{echo"Nem";} ?></dd>
									<dt>Szuper adminisztrátor:</dt><dd><? if($myusers['superadmin']==1){echo"Igen";}else{echo"Nem";} ?></dd>
									<dt>Státusz:</dt><dd><? if($myusers['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
								</dl>	
							</div>
							<div class="col-md-6">
								<?php
									$pic=db_one("select filename from files where modul='core' and type='users' and type_id='".$myusers['id']."' order by cover desc");
									if(is_file($pic)){
										echo"<div class=\"col-sm-3\"><img src=\"/".$pic."\" alt=\"avatar\" width=\"450px\"></div>";
									}
									else{
										echo"<img src=\"img/placeholders/avatars/avatar9@2x.jpg\" alt=\"avatar\">";
									}
								?>
							</div>
							<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-2">
					
							<?
							$groups=db_all("select id, name from groups where status=1 order by name");
							
							echo"<form id=\"edituser\" name=\"edituser\"  method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
								echo "<div style=\"margin-top:1px;\">";
								
										echo "<div class=\"form-group\">
											<label class=\"col-md-3 control-label\" for=\"id\">Azonosító: </label>
											<div class=\"col-md-9\">
												<input type=\"text\" id=\"id\" name=\"id\" class=\"form-control\" value=\"".$myusers['id']."\" disabled >
											</div>
										</div>";
										
										echo "<div class=\"form-group\">
											<label class=\"col-md-3 control-label\" for=\"login\">Bejelentkező E-mail cím:</label>
											<div class=\"col-md-9\">
												<input type=\"text\" id=\"login\" name=\"login\" class=\"form-control\" value=\"".$myusers['login']."\" disabled >																		</div>
										</div>";
										
										echo "<div class=\"form-group\">
											<label class=\"col-md-3 control-label\" for=\"realname\">Név:</label>
											<div class=\"col-md-9\">
												<input type=\"text\" id=\"realname\" name=\"realname\" class=\"form-control\" value=\"".$myusers['realname']."\" >						
											</div>
										</div>";
										
										echo "<div class=\"form-group\">
											<label class=\"col-md-3 control-label\" for=\"group_id\">Csoport:</label>
											<div class=\"col-md-9\">
												<select id=\"group_id\" name=\"group_id\" class=\"form-control\" value=\"".$myusers['name']."\" >
												<option value=\"0\">Kérlek válassz</option>";
												for($i=0;$i<count($groups);$i++){
													$sel="";if($groups[$i]['id']==$myusers['group_id']){$sel=" selected ";}
													echo"<option value=\"".$groups[$i]['id']."\" ".$sel.">".$groups[$i]['name']."</option>";	
												}
												echo"</select>
											</div>
										</div>";
										
										echo "<div class=\"form-group\">
											<label class=\"col-md-3 control-label\" for=\"telnum\">Telefonszám:</label>
											<div class=\"col-md-9\">
												<input type=\"text\" id=\"telnum\" name=\"telnum\" class=\"form-control\" value=\"".$myusers['telnum']."\" >
											</div>
										</div>";
										
										echo "<div class=\"form-group\">
											<label class=\"col-md-3 control-label\" for=\"email\">E-mail:</label>
											<div class=\"col-md-9\">
												<input type=\"text\" id=\"email\" name=\"email\" class=\"form-control\" value=\"".$myusers['email']."\" >
											</div>
										</div>";
										
										echo "<div class=\"form-group\" >";
											echo "<label class=\"col-md-3 control-label\" for=\"customeradmin\">Adminisztátor:</label>";
											$ch1="";if($myusers['customeradmin']==1){ $ch1=" checked "; }
											echo "<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"customeradmin\" name=\"customeradmin\"".$ch1." ><span></span></label>";
											
											echo "<label class=\"col-md-3 control-label\" for=\"superadmin\">Szuper adminisztátor:</label>";
											$ch2="";if($myusers['superadmin']==1){ $ch2=" checked "; }
											echo "<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"superadmin\" name=\"superadmin\"".$ch2." ><span></span></label>";
											
											echo "<label class=\"col-md-3 control-label\" for=\"status\">Státusz:</label>";
											$st="";if($myusers['status']==1){ $st=" checked "; }
											echo "<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\"".$st." ><span></span></label>";
										echo "</div>";
										
										echo "<div style=\"clear:both;height:10px;\"></div>";
										
										echo "<div class=\"form-group form-actions\">
											<div class=\"col-md-9 col-md-offset-3\">
												<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditusers(".$myusers['id'].")\"><i class=\"fa fa-angle-right\"></i> Ment</button>
											</div>
										</div>";
										
									echo "</div>
								</form>";
							?>
					
						</div>
						<div class="tab-pane" id="tab-3">
								<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$myusers['id'],"Dokumentum feltöltés")."</div>";
								?>
							   <div id="filelists"></div>
							<div style="clear:both;height:1px;"></div> 
						</div>
						
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$myusers['id'],"Hozzászólások")."</div>";
								?>
						</div>
						
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>
						
						<div class="tab-pane" id="tab-6">
							<div id="privacydiv">
								<?
									echo getuserright($myusers['id']);
								?>
							</div>
						</div>

					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/core/js/inc_users.js"></script>
			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>','',<?=$myusers['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$myusers['id'];?>,'Dokumentum feltöltés','filelists');
			</script>
			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
?>    
<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Felhasználó kezelő</a></li>
			</ul>
			<!-- END Datatables Header -->

			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Felhasználó</strong> lista</h2>
				</div>

				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/core/js/inc_users.js"></script>
		<script>$(function(){ UsersDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>
