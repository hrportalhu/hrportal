<?
include dirname(__FILE__).("/inc.users.php");
naplo('info','users','Felhasználó betöltése');
$_SESSION['planetsys']['actpage']=$screen;
if(priv("users","setup")){
?>
<li class="dropdown">
	<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
		<i class="gi gi-cogwheels"></i>
	</a>
	<ul class="dropdown-menu dropdown-custom dropdown-options">
		<li class="dropdown-header text-center">Beállítások</li>
		<li>
			<form name="setupform" id="setupform">
			
			<?
			$lq="select * from listoptions where user_id='".$_SESSION['planetsys']['user_id']."' and modulname='".$_SESSION['planetsys']['actpage']['modulname']."' and funcname='".$_SESSION['planetsys']['actpage']['funcname']."'";
			$listoptions=db_row($lq);
			$listelemets=explode(",",$listoptions['screenlist']);
			foreach($_SESSION['planetsys']['actpage']['elements'] as $record){
				if(in_array($record['name'],$listelemets)){	$ch=" checked=\"checked\" ";}else{$ch=" ";}
				echo"<div ><input type=\"checkbox\" id=\"AAA_".$record['name']."\" name=\"AAA_".$record['name']."\" ".$ch." onclick=\"javascript:savesetupscreen();\" />&nbsp;".$record['textname']."</div>";
			}
			?>
			<div class="clear"></div>
			</form>
			
		</li>
	</ul>
</li>
<?
}
if(priv("users","export")){
?>
<li class="dropdown">
		<a href="#modal-regular-export" data-toggle="modal">
			<i class="fa fa-briefcase" title="export"></i>
		</a>
	
</li>
<?
}
if(priv("users","filter")){
?>
<li class="dropdown">
<a href="#modal-regular-filter" data-toggle="modal">
	<i class="fa fa-filter" title="filter"></i>
</a>
</li>
<?
}
if(priv("users","new")){
?>
<li class="dropdown">
	<a href="/<?=$template['workpage'];?>/new " >
		<i class="gi gi-bomb" title="New"></i>
	</a>	
</li>
<?
}
?>
<form  id="newelementform" id="newelementform" class="form-horizontal form-bordered" onsubmit="return false;">
<?
	echo"<div id=\"modal-regular-filter\" class=\"modal\" tabindex=\"-2\" role=\"dialog\" aria-hidden=\"true\">
			<div class=\"modal-dialog\">
				<div class=\"modal-content\">
					<div class=\"modal-header\">
						<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
						<h3 class=\"modal-title\">Szűrés</h3>
					</div>
					<div class=\"modal-body\">";
						
					$groups=db_all("SELECT id,name FROM groups where  status='1' ORDER by name");
					?>
					<div class="form-group" >
						<label class="col-md-3 control-label" for="filtergroup_id">Csoportok</label>
						<div class="col-md-9">
							<select id="filtergroup_id" name="filtergroup_id" class="form-control" >
								<option value="0">Kérlek válassz</option>
								<?
									for($i=0;$i<count($groups);$i++){
										echo"<option value=\"".$groups[$i]['id']."\">".$groups[$i]['name']."</option>";	
									}
								?>
							</select>
						</div>
					</div>
					<div style="clear:both;height:5px;"></div>
					<?										
					echo"</div>
					<div class=\"modal-footer\">
						<button type=\"button\" class=\"btn btn-sm btn-warning\" onclick=\"javascript:UsersDatatables.init();\" >Szűr</button>
						<button type=\"button\" class=\"btn btn-sm btn-default\" data-dismiss=\"modal\">Bezár</button>
					</div>
				</div>
			</div>
		</div>";	
?>
</form>
