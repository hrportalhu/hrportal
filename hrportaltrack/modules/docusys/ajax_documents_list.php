<?php
session_start();
header("Cache-control: private");
header('Content-Type: text/plain;charset=utf-8');
include_once dirname(__FILE__).("/../../inc/sys.conf");
include_once dirname(__FILE__).("/../../inc/_functions.php");

$screearray=explode(",",$_SESSION['planetsys']['actpage']['screenlist']);

$direlement=explode("-",$_REQUEST['dirsum']);
$dirfilter="";
for($z=0;$z<count($direlement);$z++){
	if($_REQUEST['dir_'.$direlement[$z]]=="1"){ 
		if($dirfilter==""){$dirfilter.="and ("; }
		$dirfilter.=" ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".dirs_id='".$direlement[$z]."' or";	
	}	
}
if($dirfilter!=""){$dirfilter=substr($dirfilter,0,-3).") ";	}

$curelement=explode("-",$_REQUEST['cursum']);
$curfilter="";
for($z=0;$z<count($curelement);$z++){
	if($_REQUEST['cur_'.$curelement[$z]]=="1"){ 
		if($curfilter==""){$curfilter.="and ("; }
		$curfilter.=" ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".curs_id='".$curelement[$z]."' or";	
	}	
}
if($curfilter!=""){$curfilter=substr($curfilter,0,-3).") ";	}

$catelement=explode("-",$_REQUEST['catsum']);
$catfilter="";
for($z=0;$z<count($catelement);$z++){
	if($_REQUEST['cat_'.$catelement[$z]]=="1"){ 
		if($catfilter==""){$catfilter.="and ("; }
		$catfilter.=" ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".categories_id='".$catelement[$z]."' or";	
	}	
}
if($catfilter!=""){$catfilter=substr($catfilter,0,-3).") ";	}

$szlafilter="";
$csszlafilter="";
if($_REQUEST['csszla']==1){
	$csszlafilter.=" and ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".is_invoice='1' ";	
	$cskfszlafilter="";
	if($_REQUEST['cskfszla']==1){
		$cskfszlafilter.=" and ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".fizetve='1'  ";	
	}
	$csknfszlafilter="";
	if($_REQUEST['csknfszla']==1){
		$csknfszlafilter.=" and ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".fizetve='0' ";	
	}
	$cslhszlafilter="";
	if($_REQUEST['cslhszla']==1){
		$cslhszlafilter.=" and ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".hatarido<NOW() and ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".hatarido!='0000-00-00 00:00:00'";	
	}
}
else{
	$cskfszlafilter="";
	if($_REQUEST['cskfszla']==1){
		$cskfszlafilter.=" and ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".fizetve='1' and ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".is_invoice='1' ";	
	}
	$csknfszlafilter="";
	if($_REQUEST['csknfszla']==1){
		$csknfszlafilter.=" and ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".fizetve='0' and ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".is_invoice='1' ";	
	}
	$cslhszlafilter="";
	if($_REQUEST['cslhszla']==1){
		$cslhszlafilter.=" and ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".hatarido<NOW() and ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".hatarido!='0000-00-00 00:00:00'";	
	}
}

$szlafilter=$csszlafilter.$cskfszlafilter.$csknfszlafilter.$cslhszlafilter;

//Tábla, mező olvasási definíciók
	$selee="";
	$joiele=" from ".$_SESSION['planetsys']['actpage']['scrtable']." ".$_SESSION['planetsys']['actpage']['scrtable_shortname']." ";
	for($i=0;$i<count($screearray);$i++){
		if($_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['connected']=="1"){
			
			$selee.=" ".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['con_table_shortname'].".".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['con_table_fields'][1]." as ".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['name'].",";
			$joiele.=" LEFT JOIN ".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['con_table']." ".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['con_table_shortname']." on (".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['name']."=".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['con_table_shortname'].".".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['con_table_fields'][0]."  )";
		}
		else{
			$selee.=" ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['name']." as ".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['name'].",";		
		}
	}
	$selee=substr($selee,0,-1);

//Limit definíció
	$sLimit = "";
	if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){$sLimit = "LIMIT ".mysql_real_escape_string( $_GET['iDisplayStart'] ).", ".mysql_real_escape_string( $_GET['iDisplayLength'] );}

//Rendezés
	$sOrder = "";
	if ( isset( $_GET['iSortCol_0'] ) )	{
		$sOrder = "ORDER BY  ";
		if($_SESSION['planetsys']['actpage']['elements'][$screearray[$_GET['iSortCol_0']]]['conected']=="1"){
			$sOrder.=" ".$_SESSION['planetsys']['actpage']['elements'][$screearray[$_GET['iSortCol_0']]]['con_table_shortname'].".".$_SESSION['planetsys']['actpage']['elements'][$screearray[$_GET['iSortCol_0']]]['con_table_fields'][1]."";	
		}
		else{
			$sOrder.=" ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".".$screearray[$_GET['iSortCol_0']]."";	
		}
		if($_GET['sSortDir_0']=="asc"){$sOrder.=" asc ";}else{$sOrder.=" desc ";}	
	}
//keresés
	$sWhere =  " ";
	if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
		$sWhere.= " and ( ";
		for($i=0;$i<count($screearray);$i++){
			if($_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['conected']=="1"){
				$sWhere.=" ".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['con_table_shortname'].".".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['con_table_fields'][1]." LIKE  '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
			}
			else{
				$sWhere.=" ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".".$screearray[$i]." LIKE  '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
			}
		}
		$sWhere=substr($sWhere,0,-4);
		$sWhere.= ")";
	}
	
	$sWhere2= "";	
	if($_REQUEST['partner_id']!="0"){
		$sWhere2.= " and ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".partner_id='".$_REQUEST['partner_id']."' ";
	}

	$sWhere3="";
	if($_REQUEST['startdate']!=$_REQUEST['enddate']){
		if($_REQUEST['dats']==0){$sWhere3=" and d.rdate between '".$_REQUEST['startdate']."' and '".$_REQUEST['enddate']."' ";}
		if($_REQUEST['dats']==1){$sWhere3=" and d.erkezes between '".$_REQUEST['startdate']."' and '".$_REQUEST['enddate']."' ";}
		if($_REQUEST['dats']==2){$sWhere3=" and d.hatarido between '".$_REQUEST['startdate']."' and '".$_REQUEST['enddate']."' ";}
		
	}
	
	$elems=db_all("select ".$selee." ".$joiele." where ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".id>0 ".$szlafilter."  ".$sWhere2." ".$sWhere3." ".$sWhere." ".$dirfilter." ".$curfilter." ".$catfilter." ".$sOrder." ".$sLimit."");
	$sumforint=db_one("select sum(osszeg) from ".$_SESSION['planetsys']['actpage']['scrtable']." ".$_SESSION['planetsys']['actpage']['scrtable_shortname']." where ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".id>0  ".$sWhere2." ".$sWhere3." ".$szlafilter." ".$dirfilter." ".$curfilter."  ".$catfilter." ".$sOrder." ");
//echo "select ".$selee." ".$joiele." where ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".id>0 ".$szlafilter."  ".$sWhere2." ".$sWhere3." ".$sWhere." ".$dirfilter." ".$curfilter." ".$catfilter." ".$sOrder." ".$sLimit."";
	$elemdb=db_one("select count(id) from ".$_SESSION['planetsys']['actpage']['scrtable']." ".$_SESSION['planetsys']['actpage']['scrtable_shortname']." where ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".id>0  ".$sWhere2." ".$sWhere3." ".$szlafilter." ".$dirfilter." ".$curfilter."  ".$catfilter." ".$sOrder." ");
	$iFilteredTotal =  count($elems);
	$iTotal = db_one("SELECT COUNT(id)	FROM  ".$_SESSION['planetsys']['actpage']['scrtable']." ".$_SESSION['planetsys']['actpage']['scrtable_shortname']." where ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".id>0 ");
	$output = array("sEcho" => intval($_GET['sEcho']),"iTotalRecords" => $iTotal,"iTotalDisplayRecords" =>  $elemdb,"aaData" => array(),"sumforint"=>$sumforint);
	
	for($k=0;$k<count($elems);$k++){
		$row = array();
		//$row[] = $elems[$k]['id'];
		
		for($z=0;$z<count($screearray);$z++){
			if($screearray[$z]=="reszfizetes"){if($elems[$k][$screearray[$z]]==1){$elems[$k][$screearray[$z]]="Igen";}else{$elems[$k][$screearray[$z]]="Nem";}}
			if($screearray[$z]=="fizetve"){if($elems[$k][$screearray[$z]]==1){$elems[$k][$screearray[$z]]="Igen";}else{$elems[$k][$screearray[$z]]="Nem";}}
			if($screearray[$z]=="status"){if($elems[$k][$screearray[$z]]==1){$elems[$k][$screearray[$z]]="Aktiv";}else{$elems[$k][$screearray[$z]]="Inaktív";}}
			if($screearray[$z]=="megjegyzes"){if($elems[$k][$screearray[$z]]!=""){$elems[$k][$screearray[$z]]=substr($elems[$k][$screearray[$z]],0,40);	}}
			if($screearray[$z]=="osszeg"){if($elems[$k][$screearray[$z]]!=""){$elems[$k][$screearray[$z]]="<div style=\"width:100px;float:right;text-align:right;\">".huf($elems[$k][$screearray[$z]])."</div>";	}}
			$row[] = $elems[$k][$screearray[$z]];
		}
		$output['aaData'][] = $row;
	}
	
$_SESSION['planetsys']['exportdata'] = $output ;	

echo json_encode( $output );
mysql_close($connid);
?>
