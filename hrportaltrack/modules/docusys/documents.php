<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Iktató rendszer
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/docusys/documents">Iktató</a></li>
				<li><a href="/new">Új dokumentum felvitele</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új dokumentum felvitele</strong></h2>
				</div>
			<?
			
			$doccats=db_all("select id, name from docusystem_categories where status=1 order by name");
			$docdirs=db_all("select id, name from docusystem_dirs where status=1 order by name");
			$doccurs=db_all("select id, name from curencys where status=1 order by name");
			$doccty=db_all("select id, name from cost_category where status=1 order by name");
			$ceg=db_all("select id, name,bankacc from partners where status=1 order by name");
				
			echo "<form id=\"newdocuments\" name=\"newdocuments\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";
				
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"categories_id\">Típus:</label>
						<div class=\"col-md-9\">
							<select id=\"categories_id\" name=\"categories_id\" class=\"form-control\" placeholder=\"Beosztása\" >
							<option value=\"0\">Kérlek válassz</option>";
							for($i=0;$i<count($doccats);$i++){
								echo "<option value=\"".$doccats[$i]['id']."\" >".$doccats[$i]['name']."</option>";	
							}
							echo "</select>
						</div>
					</div>";
					echo "<input type=\"hidden\" name=\"dirs_id\" id=\"dirs_id\" value=\"0\"/>";
					echo "<input type=\"hidden\" name=\"curs_id\" id=\"curs_id\" value=\"1\"/>";
					echo "<input type=\"hidden\" name=\"lejarat\" id=\"lejarat\" value=\"0000-00-00\"/>";
					/*
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"dirs_id\">Mappa:</label>
						<div class=\"col-md-9\">
							<select id=\"dirs_id\" name=\"dirs_id\" class=\"form-control\" placeholder=\"Mappa\" >
							<option value=\"0\">Kérlek válassz</option>";
							for($i=0;$i<count($docdirs);$i++){
								echo "<option value=\"".$docdirs[$i]['id']."\" >".$docdirs[$i]['name']."</option>";	
							}
							echo "</select>
						</div>
					</div>";
					*/ 
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"doccty\">Költséghely:</label>
						<div class=\"col-md-9\">
							<select id=\"doccty\" name=\"doccty\" class=\"form-control\"  >
							<option value=\"0\">Kérlek válassz</option>";
							for($i=0;$i<count($doccty);$i++){
								echo "<option value=\"".$doccty[$i]['id']."\" >".$doccty[$i]['name']."</option>";	
							}
							echo "</select>
						</div>
					</div>";
					/*
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"curs_id\">Deviza-Pénznem:</label>
						<div class=\"col-md-9\">
							<select id=\"curs_id\" name=\"curs_id\" class=\"form-control\" placeholder=\"Deviza-Pénznem\" >
							<option value=\"0\">Kérlek válassz</option>";
							for($i=0;$i<count($doccurs);$i++){
								echo "<option value=\"".$doccurs[$i]['id']."\" >".$doccurs[$i]['name']."</option>";	
							}
							echo "</select>
						</div>
					</div>";
					*/
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"partner_id\">Partner:</label>
						<div class=\"col-md-9\">
							<select id=\"partner_id\" name=\"partner_id\" class=\"form-control\" placeholder=\"Partner\" >
							<option value=\"0\">Kérlek válassz</option>";
							for($i=0;$i<count($ceg);$i++){
								echo "<option value=\"".$ceg[$i]['id']."\" >".$ceg[$i]['name']." (".$ceg[$i]['bankacc'].")</option>";	
							}
							echo "</select>
						</div>
					</div>";
					
					
					echo "<div class=\"form-group\">
					<label class=\"col-md-3 control-label\" for=\"name\">Egyedi sorszám:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" value=\"DOC-".date("Ymd",time())."-".generateCode(7)."\" />																		
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
					<label class=\"col-md-3 control-label\" for=\"szlaname\">Számla száma:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"szlaname\" name=\"szlaname\" class=\"form-control\" placeholder=\"Számla száma\" />																		
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
					<label class=\"col-md-3 control-label\" for=\"erkezes\">Érkezés:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"erkezes\" name=\"erkezes\" class=\"form-control input-datepicker\" placeholder=\"YYYY-MM-DD\" />																		
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
					<label class=\"col-md-3 control-label\" for=\"hatarido\">Fizetési határidő:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"hatarido\" name=\"hatarido\" class=\"form-control input-datepicker\" placeholder=\"YYYY-MM-DD\" />																		
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
					<label class=\"col-md-3 control-label\" for=\"teljesites\">Teljesítés ideje:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"teljesites\" name=\"teljesites\" class=\"form-control input-datepicker\" placeholder=\"YYYY-MM-DD\" />																		
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
					<label class=\"col-md-3 control-label\" for=\"kiallitas\">Kiállítás ideje:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"kiallitas\" name=\"kiallitas\" class=\"form-control input-datepicker\" placeholder=\"YYYY-MM-DD\" />																		
						</div>
					</div>";
					/*
					echo "<div class=\"form-group\">
					<label class=\"col-md-3 control-label\" for=\"lejarat\">Lejárat ideje:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"lejarat\" name=\"lejarat\" class=\"form-control input-datepicker\" placeholder=\"YYYY-MM-DD\" />																		
						</div>
					</div>";
					*/
					echo "<div class=\"form-group\">
					<label class=\"col-md-3 control-label\" for=\"fizetve_date\">Kifizetes ideje:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"fizetve_date\" name=\"fizetve_date\" class=\"form-control input-datepicker\" placeholder=\"YYYY-MM-DD\" />
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
					<label class=\"col-md-3 control-label\" for=\"osszeg\">Összeg:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"osszeg\" name=\"osszeg\" class=\"form-control\" placeholder=\"Összeg\" />																		
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
					<label class=\"col-md-3 control-label\" for=\"megjegyzes\">Megjegyzés:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"megjegyzes\" name=\"megjegyzes\" class=\"form-control\" placeholder=\"Megjegzés\" />																		
						</div>
					</div>";
					
					echo "<div class=\"form-group\" >
						<label class=\"col-md-3 control-label\" for=\"fizetve\">Fizetve:</label>
						<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"fizetve\" name=\"fizetve\" ><span></span></label>

						<label class=\"col-md-3 control-label\" for=\"reszfizetes\">Reszfizetes:</label>
						<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"reszfizetes\" name=\"reszfizetes\" ><span></span></label>

						<label class=\"col-md-3 control-label\" for=\"status\">Státusz:</label>
						<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span></label>
					</div>";
					
					echo "<div style=\"clear:both;height:10px;\"></div>";
					
					echo "<div class=\"form-group form-actions\">
						<div class=\"col-md-9 col-md-offset-3\">
							<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewdocuments();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
						</div>
					</div>";
					
				echo "</div>
			</form>";
			?>	
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/docusys/js/inc_documents.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?	
		}
		else{
			
			//echo "select * from functions where id='".$uritags[2]."'";
			$mydocuments=db_row("select * from docusystem_documents where id='".$uritags[3]."'");
			if($mydocuments['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="modules/docusys/documents">Dokumentum</a></li>
				<li><a href="/<?=$URI;?>"><?=$mydocuments['name'];?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->

					<div class="block-title">
						<h2><strong><?=$mydocuments['name'];?></strong> dokumentum oldala</h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
							if(priv("documents","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("documents","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							if(priv("documents","kontir")){echo"<li><a href=\"#tab-6\" data-toggle=\"tooltip\" title=\"kontir\"><i class=\"fa fa-money\"></i></a></li>";}
							if(priv("documents","reszfizetes")){echo"<li><a href=\"#tab-7\" data-toggle=\"tooltip\" title=\"reszfizetes\"><i class=\"fa fa-usd\"></i></a></li>";}
							if(priv("documents","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("documents","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}								
							if(priv("documents","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}
							?>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="col-md-6">
								<dl class="dl-horizontal">
									<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$mydocuments['id']); ?></dd>
									<dt>Típus:</dt><dd><? echo idchange("docusystem_categories","name",$mydocuments['categories_id']); ?></dd>
									<dt>Költséghely:</dt><dd><? echo idchange("cost_category","name",$mydocuments['doccty']); ?></dd>
									<!--<dt>Mappa:</dt><dd><? echo idchange("docusystem_dirs","name",$mydocuments['dirs_id']); ?></dd>-->
									<!--<dt>Deviza-Pénznem:</dt><dd><? echo idchange("curencys","name",$mydocuments['curs_id']); ?></dd>-->
									<dt>Partner:</dt><dd><? echo idchange("partners","name",$mydocuments['partner_id']); ?></dd>
									<dt>Egyedi sorszám:</dt><dd><? echo $mydocuments['name']; ?></dd>
									<dt>Számla száma:</dt><dd><? echo $mydocuments['szlaname']; ?></dd>
									<dt>Érkezés:</dt><dd><? echo $mydocuments['erkezes']; ?></dd>
									<dt>Fizetési határidő:</dt><dd><? echo $mydocuments['hatarido']; ?></dd>
									<dt>Teljesítés ideje:</dt><dd><? echo $mydocuments['teljesites']; ?></dd>
									<dt>Kiállítás ideje:</dt><dd><? echo $mydocuments['kiallitas']; ?></dd>
									<dt>Lejárat ideje:</dt><dd><? echo $mydocuments['lejarat']; ?></dd>
									<dt>Kifizetés ideje:</dt><dd><? echo $mydocuments['fizetve_date']; ?></dd>
									<dt>Osszeg:</dt><dd><? echo $mydocuments['osszeg']; ?></dd>
									<dt>Megjegyzes:</dt><dd><? echo $mydocuments['megjegyzes']; ?></dd>
									<dt>Fizetve:</dt><dd><? if($mydocuments['fizetve']==1){echo"Igen";}else{echo"Nem";} ?></dd>
									<dt>Részfizetés:</dt><dd><? if($mydocuments['reszfizetes']==1){echo"Igen";}else{echo"Nem";} ?></dd>
									<dt>Státusz:</dt><dd><? if($mydocuments['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
								</dl>	
							</div>
							<div class="col-md-6">
								<?php
									$pic=db_one("select filename from files where modul='docusys' and type='document' and type_id='".$mydocuments['id']."' order by cover desc");
									if(is_file($pic)){
										echo"<div class=\"col-sm-3\"><img src=\"/".$pic."\" alt=\"avatar\" width=\"450px\"></div>";
									}
									else{
										echo"<img src=\"img/placeholders/avatars/avatar9@2x.jpg\" alt=\"avatar\">";
									}
								?>
							</div>
							<div style="clear:both;height:1px;"></div>
						</div>
							<div class="tab-pane" id="tab-2">
							<?
							
							$doccats=db_all("select id, name from docusystem_categories where status=1 order by name");
							$docdirs=db_all("select id, name from docusystem_dirs where status=1 order by name");
							$doccurs=db_all("select id, name from curencys where status=1 order by name");
							$ceg=db_all("select id, name,bankacc from partners where status=1 order by name");
							$doccty=db_all("select id, name from cost_category where status=1 order by name");
							echo "<form id=\"editdocuments\" name=\"editdocuments\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
								echo "<div style=\"margin-top:1px;\">";
								
									echo "<div class=\"form-group\">
										<label class=\"col-md-3 control-label\" for=\"categories_id\">Típus:</label>
										<div class=\"col-md-9\">
											<select id=\"categories_id\" name=\"categories_id\" class=\"form-control\" placeholder=\"Beosztása\" >
											<option value=\"0\">Kérlek válassz</option>";
											for($i=0;$i<count($doccats);$i++){
												$ch1="";if($mydocuments['categories_id']==$doccats[$i]['id']){$ch1=" selected "; }
												echo"<option value=\"".$doccats[$i]['id']."\" ".$ch1.">".$doccats[$i]['name']."</option>";	
											}
											echo"</select>
										</div>
									</div>";
								echo "<input type=\"hidden\" name=\"dirs_id\" id=\"dirs_id\" value=\"0\"/>";
								echo "<input type=\"hidden\" name=\"curs_id\" id=\"curs_id\" value=\"1\"/>";
								echo "<input type=\"hidden\" name=\"lejarat\" id=\"lejarat\" value=\"0000-00-00\"/>";
									/*
									echo "<div class=\"form-group\">
										<label class=\"col-md-3 control-label\" for=\"dirs_id\">Mappa:</label>
										<div class=\"col-md-9\">
											<select id=\"dirs_id\" name=\"dirs_id\" class=\"form-control\" placeholder=\"Mappa\" >
											<option value=\"0\">Kérlek válassz</option>";
											for($i=0;$i<count($docdirs);$i++){
												$ch2="";if($mydocuments['dirs_id']==$docdirs[$i]['id']){$ch2=" selected "; }
												echo"<option value=\"".$docdirs[$i]['id']."\" ".$ch2.">".$docdirs[$i]['name']."</option>";	
											}
											echo"</select>
										</div>
									</div>";
									*/
									echo "<div class=\"form-group\">
										<label class=\"col-md-3 control-label\" for=\"doccty\">Költséghely:</label>
										<div class=\"col-md-9\">
											<select id=\"doccty\" name=\"doccty\" class=\"form-control\"  >
											<option value=\"0\">Kérlek válassz</option>";
											for($i=0;$i<count($doccty);$i++){
												$ch2="";if($mydocuments['doccty']==$doccty[$i]['id']){$ch2=" selected "; }
												echo "<option value=\"".$doccty[$i]['id']."\" >".$doccty[$i]['name']."</option>";	
											}
											echo "</select>
										</div>
									</div>";
									/*
									echo "<div class=\"form-group\">
										<label class=\"col-md-3 control-label\" for=\"curs_id\">Deviza-Pénznem:</label>
										<div class=\"col-md-9\">
											<select id=\"curs_id\" name=\"curs_id\" class=\"form-control\" placeholder=\"Deviza-Pénznem\" >
											<option value=\"0\">Kérlek válassz</option>";
											for($i=0;$i<count($doccurs);$i++){
												$ch3="";if($mydocuments['curs_id']==$doccurs[$i]['id']){$ch3=" selected "; }
												echo"<option value=\"".$doccurs[$i]['id']."\" ".$ch3.">".$doccurs[$i]['name']."</option>";	
											}
											echo"</select>
										</div>
									</div>";
									*/
									echo "<div class=\"form-group\">
										<label class=\"col-md-3 control-label\" for=\"partner_id\">Partner:</label>
										<div class=\"col-md-9\">
											<select id=\"partner_id\" name=\"partner_id\" class=\"form-control\" placeholder=\"Partner\" >
											<option value=\"0\">Kérlek válassz</option>";
											for($i=0;$i<count($ceg);$i++){
												$ch4="";if($mydocuments['partner_id']==$ceg[$i]['id']){$ch4=" selected "; }
												echo"<option value=\"".$ceg[$i]['id']."\" ".$ch4." >".$ceg[$i]['name']." (".$ceg[$i]['bankacc'].")</option>";	
											}
											echo"</select>
										</div>
									</div>";
									
									echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"name\">Egyedi sorszám:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" placeholder=\"Egyedi sorszám\"  value=\"".$mydocuments['name']."\"/>																		
										</div>
									</div>";
									
									echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"szlaname\">Számla száma:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"szlaname\" name=\"szlaname\" class=\"form-control\" placeholder=\"Számla száma\"  value=\"".$mydocuments['szlaname']."\"/>																		
										</div>
									</div>";
									
									echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"erkezes\">Érkezés:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"erkezes\" name=\"erkezes\" class=\"form-control input-datepicker\" placeholder=\"Érkezés \" value=\"".$mydocuments['erkezes']."\" />																		
										</div>
									</div>";
									
									echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"hatarido\">Fizetési határidő:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"hatarido\" name=\"hatarido\" class=\"form-control input-datepicker\" placeholder=\"Fizetési határidő \"  value=\"".$mydocuments['hatarido']."\"/>																		
										</div>
									</div>";
									
									echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"teljesites\">Teljesítés ideje:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"teljesites\" name=\"teljesites\" class=\"form-control input-datepicker\" placeholder=\"Teljesítés ideje \"  value=\"".$mydocuments['teljesites']."\"/>																		
										</div>
									</div>";
									
									echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"kiallitas\">Kiállítás ideje:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"kiallitas\" name=\"kiallitas\" class=\"form-control input-datepicker\" placeholder=\"Kiállítás ideje \"  value=\"".$mydocuments['kiallitas']."\"/>																		
										</div>
									</div>";
									/*
									echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"lejarat\">Lejárat ideje:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"lejarat\" name=\"lejarat\" class=\"form-control input-datepicker\" placeholder=\"Lejárat ideje \" value=\"".$mydocuments['lejarat']."\" />																		
										</div>
									</div>";
									*/
									echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"fizetve_date\">Kifizetes ideje:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"fizetve_date\" name=\"fizetve_date\" class=\"form-control input-datepicker\" placeholder=\"Kifizetes ideje \"  value=\"".$mydocuments['fizetve_date']."\"/>																		
										</div>
									</div>";
									
									echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"osszeg\">Összeg:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"osszeg\" name=\"osszeg\" class=\"form-control\" placeholder=\"Összeg\" value=\"".$mydocuments['osszeg']."\" />																		
										</div>
									</div>";
									
									echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"megjegyzes\">Megjegyzés:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"megjegyzes\" name=\"megjegyzes\" class=\"form-control\" placeholder=\"Megjegyzés\"  value=\"".$mydocuments['megjegyzes']."\"/>																		
										</div>
									</div>";
									
									echo "<div class=\"form-group\" >
										<label class=\"col-md-3 control-label\" for=\"fizetve\">Fizetve:</label>";
										$ch1="";if($mydocuments['fizetve']==1){ $ch1=" checked "; }
										echo "<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"fizetve\" name=\"fizetve\"".$ch1." ><span></span></label>

										<label class=\"col-md-3 control-label\" for=\"reszfizetes\">Reszfizetes:</label>";
										$ch2="";if($mydocuments['reszfizetes']==1){ $ch2=" checked "; }
										echo "<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"reszfizetes\" name=\"reszfizetes\"".$ch2." ><span></span></label>

										<label class=\"col-md-3 control-label\" for=\"status\">Státusz:</label>";
										$st="";if($mydocuments['status']==1){ $st=" checked "; }
										echo "<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\"".$st." ><span></span></label>
									</div>";
									
									echo "<div style=\"clear:both;height:10px;\"></div>";
									
									echo "<div class=\"form-group form-actions\">
										<div class=\"col-md-9 col-md-offset-3\">
											<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditdocuments('".$mydocuments['id']."')\"><i class=\"fa fa-angle-right\"></i> Ment</button>
										</div>
									</div>";
									
								echo "</div>
							</form>";
						?>
						</div>

						<div class="tab-pane" id="tab-3">
								<?
									echo "<div >".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mydocuments['id'],"Dokumentum feltöltés")."</div>";
								?> 
							   <div id="filelists"></div>
							   <div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mydocuments['id'],"Hozzászólások")."</div>";
								?>
						</div>
				
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>
						
						<div class="tab-pane" id="tab-6">
							 <div class="block hidden-lt-ie9 <?=$adc;?>" >
								<!-- Switches Title -->
								<div class="block-title">
									<h2><strong>Új afa bontás</strong> </h2>
								</div>
								<!-- END Switches Title -->
								<?
									$afas=db_all("select kulcs,name from bookkeepsystem_vats where status=1 order by name");
								?>
								<!-- Switches Content -->
								<form  id="newelementformafa" id="newelementformafa" class="form-horizontal form-bordered" onsubmit="return false;">
									<div class="form-group" style="width:300px;float:left;">
										<label class="col-md-3 control-label" for="netto">Nettó</label>
										<div class="col-md-9">
											<input type="text" id="netto" name="netto" class="form-control" value="" />
										</div>
									</div>
									<div class="form-group" style="width:300px;float:left;">
										<label class="col-md-3 control-label" for="afa">Áfa mérték</label>
										<div class="col-md-9">
											<select id="afa" name="afa" class="form-control"  >
												<option value="0">Kérlek válassz</option>
												<?
													for($i=0;$i<count($afas);$i++){
														echo"<option value=\"".$afas[$i]['kulcs']."\">".$afas[$i]['name']."</option>";
													}
												?>
											</select>
										</div>
									</div>
									<div class="form-group" style="width:300px;float:left;">
										<label class="col-md-3 control-label" for="afaosszeg">Áfa összeg</label>
										<div class="col-md-9">
											<input type="text" id="afaosszeg" name="afaosszeg" class="form-control"  value="" />
										</div>
									</div>
									<div class="form-group" style="width:300px;float:left;">
										<label class="col-md-3 control-label" for="brutto">Bruttó</label>
										<div class="col-md-9">
											<input type="text" id="brutto" name="brutto" class="form-control" value="" />
										</div>
									</div>
									<div style="clear:both;"></div>
									<div style="clear:both;height:10px;"></div>
									<div class="col-md-9 col-md-offset-0">
										<button type="button" class="btn btn-sm btn-primary" id="savenewbutton" onclick="javascript:addafaelement(<?=$mydocuments['id'];?>,'elementdiv2');"><i class="fa fa-angle-right"></i> Ment</button>
									</div>
									<div style="clear:both;"></div>
								</form>	
								<div style="clear:both;height:10px;"></div>
					
							</div>
							<div class="block hidden-lt-ie9">
								<!-- Switches Title -->
								<div class="block-title">
									<h2><strong>Kontírlista</strong> </h2>
								</div>
								<!-- END Switches Title -->

								<div id="elementdiv2"></div>
							</div>
						
						</div>
						<div class="tab-pane" id="tab-7">
							 <div class="block hidden-lt-ie9 <?=$adc;?>" >
								<!-- Switches Title -->
								<div class="block-title">
									<h2><strong>Új részlet</strong> </h2>
								</div>
								<!-- END Switches Title -->
								
								<form  id="newelementformreszlet" id="newelementformreszlet" class="form-horizontal form-bordered" onsubmit="return false;">
								<?
									$doc=db_row("select * from docusystem_documents where id='".$mydocuments['id']."'");
									$eddig=db_one("select sum(kifizetett_osszeg) from docusystem_billing where document_id='".$mydocuments['id']."'");

									echo"<input type=\"hidden\" id=\"alpenz\" value=\"".$doc['osszeg']."\" />";
									echo"<input type=\"hidden\" id=\"befiz\" value=\"".$eddig."\" />";
								?>
									<div class="form-group" style="width:300px;float:left;">
										<label class="col-md-3 control-label" for="reszosszeg">Részösszeg</label>
										<div class="col-md-9">
											<input type="text" id="reszosszeg" name="reszosszeg" class="form-control"  value="" />
										</div>
									</div>
									<div class="form-group" style="width:300px;float:left;">
										<label class="col-md-3 control-label" for="reszdatum">Dátum</label>
										<div class="col-md-9">
											<input type="text" id="reszdatum" name="reszdatum" class="form-control input-datepicker"  />
										</div>
									</div>
									<div style="clear:both;"></div>
									<div style="clear:both;height:10px;"></div>
									<div class="col-md-9 col-md-offset-0">
										
										<button type="button" class="btn btn-sm btn-primary" id="savenewbutton" onclick="javascript:addreszlet(<?=$mydocuments['id'];?>,'elementdiv3');"><i class="fa fa-angle-right"></i> Ment</button>
									</div>
									<div style="clear:both;"></div>
								</form>	
								<div style="clear:both;height:10px;"></div>
					
							</div>
							<div class="block hidden-lt-ie9">
								<!-- Switches Title -->
								<div class="block-title">
									<h2><strong>Részletek listája</strong> </h2>
								</div>
								<!-- END Switches Title -->

								<div id="elementdiv3"></div>
							</div>
						
						</div>
				
					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
					
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/docusys/js/inc_documents.js"></script>

			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mydocuments['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$mydocuments['id'];?>,'Dokumentum feltöltés','filelists');
				showafaelement('<?=$mydocuments['id'];?>','elementdiv1');
				showafaelement('<?=$mydocuments['id'];?>','elementdiv2');
				showreszlet('<?=$mydocuments['id'];?>','elementdiv3');
			</script>
			<script>$(function(){ documentsDatatables.init(); });</script>	
		

			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
		//print_r($_SESSION['planetsys'])
?>    
<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Dokumentumok</a></li>
			</ul>
			<!-- END Datatables Header -->

			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Dokumentum</strong> lista</h2>
				</div>

				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
								<?
								foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
									if($screen['elements'][$f]['textname']!=""){
										echo"<th>".$screen['elements'][$f]['textname']."</th>";
									}
								}
								//echo"<th>Művelet</th>";
								?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/docusys/js/inc_documents.js"></script>
		<script>$(function(){ documentsDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>
