<?
include dirname(__FILE__).("/inc.documents.php");
naplo('info','documents','Iktató betöltése');
$_SESSION['planetsys']['actpage']=$screen;
if(priv("documents","setup")){
?>
<li class="dropdown">
	<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
		<i class="gi gi-cogwheels"></i>
	</a>
	<ul class="dropdown-menu dropdown-custom dropdown-options">
		<li class="dropdown-header text-center">Beállítások</li>
		<li>
			<form name="setupform" id="setupform">
			
			<?
			$lq="select * from listoptions where user_id='".$_SESSION['planetsys']['user_id']."' and modulname='".$_SESSION['planetsys']['actpage']['modulname']."' and funcname='".$_SESSION['planetsys']['actpage']['funcname']."'";
			$listoptions=db_row($lq);
			$listelemets=explode(",",$listoptions['screenlist']);
			foreach($_SESSION['planetsys']['actpage']['elements'] as $record){
				if(in_array($record['name'],$listelemets)){	$ch=" checked=\"checked\" ";}else{$ch=" ";}
				echo"<div ><input type=\"checkbox\" id=\"AAA_".$record['name']."\" name=\"AAA_".$record['name']."\" ".$ch." onclick=\"javascript:savesetupscreen();\" />&nbsp;".$record['textname']."</div>";
			}
			?>
			<div class="clear"></div>
			</form>
			
		</li>
	</ul>
</li>
<?
}
if(priv("documents","export")){
?>
<li class="dropdown">
	<a href="#modal-regular-export" data-toggle="modal">
		<i class="fa fa-briefcase" title="export"></i>
	</a>
</li>
<?
}
if(priv("documents","filter")){
?>
<li class="dropdown">
	<a href="#modal-regular-filter" data-toggle="modal">
		<i class="fa fa-filter" title="filter"></i>
	</a>
</li>
<?
}
if(priv("documents","new")){
?>
<li class="dropdown">
	<a href="/<?=$template['workpage'];?>/new " >
		<i class="gi gi-bomb" title="New"></i>
	</a>	
</li>
<?
}
?>
<form  id="newelementform" id="newelementform" class="form-horizontal form-bordered" onsubmit="return false;">
<?
	echo"<div id=\"modal-regular-filter\" class=\"modal\" tabindex=\"-2\" role=\"dialog\" aria-hidden=\"true\">
			<div class=\"modal-dialog\">
				<div class=\"modal-content\">
					<div class=\"modal-header\">
						<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
						<h3 class=\"modal-title\">Szűrés</h3>
					</div>
					<div class=\"modal-body\">";
						
					echo"<ul class=\"nav nav-tabs\" data-toggle=\"tabs\">";
						echo"<li class=\"active\"><a href=\"#filter-1\" data-toggle=\"tooltip\" title=\"Általános\"><i class=\"gi gi-alarm\"></i></a></li>";
						echo"<li><a href=\"#filter-5\" data-toggle=\"tooltip\" title=\"Számla\"><i class=\"gi gi-notes\"></i></a></li>";
						echo"<li><a href=\"#filter-2\" data-toggle=\"tooltip\" title=\"Mappák\"><i class=\"gi gi-book\"></i></a></li>";
						echo"<li><a href=\"#filter-3\" data-toggle=\"tooltip\" title=\"Kategóriák\"><i class=\"gi gi-construction_cone\"></i></a></li>";
						echo"<li><a href=\"#filter-4\" data-toggle=\"tooltip\" title=\"Pénznemek\"><i class=\"gi gi-coins\"></i></a></li>";
					echo"</ul>";
					
					$partners=db_all("SELECT id,name FROM partners where  status='1'  ORDER by name");
					$categories=db_all("SELECT id,name FROM docusystem_categories where status='1'  ORDER by name");
					$dirs=db_all("SELECT id,name FROM docusystem_dirs where  status='1'  ORDER by name");
					$curs=db_all("SELECT id,name FROM docusystem_curs where  status='1'  ORDER by name");
					?>
					<div class="tab-content">

						<div class="tab-pane active" id="filter-1">
							<div class="form-group" >
								<label class="col-md-3 control-label" for="partnerselect">Partner</label>
								<div class="col-md-9">
									<select id="partnerselect" name="partnerselect" class="form-control"  onchange="javascript:documentsDatatables.init();">
										<option value="0">Kérlek válassz</option>
										<?
											for($i=0;$i<count($partners);$i++){
												echo"<option value=\"".$partners[$i]['id']."\">".$partners[$i]['name']."</option>";	
											}
										?>
									</select>
								</div>
							</div>
							<div class="form-group" >
								<label class="col-md-3 control-label" for="dats">Dátum választó</label>
								<div class="col-md-9">
									<select id="dats" name="dats" class="form-control"  >
										<option value="0">Rögzítés ideje</option>
										<option value="1">Érkezés ideje</option>
										<option value="2">Fizetés határideje</option>
									</select>
								</div>
							</div>
							<div class="form-group" >
								<label class="col-md-3 control-label" for="dats">Dátumok</label>
							<div class="col-md-3">
								<input type="text" id="startdate" name="startdate" value="<?=date("Y-m-d",time());?>" class="form-control input-datepicker"/>
							</div>
							<div class="col-md-3">
								<input type="text" id="enddate" name="enddate" value="<?=date("Y-m-d",time());?>"  class="form-control input-datepicker" />
							</div>
							<div class="col-md-9">
								
							</div>
								
							</div>
							
							<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane " id="filter-2">
							<div class="form-group" >
								<label class="col-md-3 control-label" for="partnerselect">Mappák</label>
								<div style="clear:both;height:10px;"></div>
								<div class="col-md-9">
									<?
										$mystring="";
										$dirstring="";
										for($i=0;$i<count($dirs);$i++){
											echo"<label class=\"switch switch-primary\" style=\"width:180px;float:left;\"><input type=\"checkbox\" id=\"dir_".$dirs[$i]['id']."\" name=\"dir_".$dirs[$i]['id']."\" value=\"dir_".$dirs[$i]['id']."\" /><span></span>".$dirs[$i]['name']."</label>";
											$dirstring.=$dirs[$i]['id']."-";
										}
										$mystring=substr($dirstring,0,-1);
										echo"<input type=\"hidden\" id=\"dirsum\" value=\"".$dirstring."\"/>";
									?>
								</div>
							</div>
							<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane " id="filter-3">
							<div class="form-group" >
								<label class="col-md-3 control-label" for="partnerselect">Kategóriák</label>
								<div style="clear:both;height:10px;"></div>
								<div class="col-md-9">
									<?
										$mystring="";
										$catstring="";
										for($i=0;$i<count($categories);$i++){
											echo"<label class=\"switch switch-primary\" style=\"width:180px;float:left;\"><input type=\"checkbox\" id=\"cat_".$categories[$i]['id']."\" name=\"cat_".$categories[$i]['id']."\" value=\"cat_".$categories[$i]['id']."\" /><span></span>".$categories[$i]['name']."</label>";
											$catstring.=$categories[$i]['id']."-";
										}
										$mystring=substr($catstring,0,-1);
										echo"<input type=\"hidden\" id=\"catsum\" value=\"".$catstring."\"/>";
									?>
								</div>
							</div>
							<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane " id="filter-4">
							<div class="form-group" >
								<label class="col-md-3 control-label" for="partnerselect">Pénznemek</label>
								<div style="clear:both;height:10px;"></div>
								<div class="col-md-9">
									<?
										$mystring="";
										$curstring="";
										for($i=0;$i<count($curs);$i++){
											echo"<label class=\"switch switch-primary\" style=\"width:180px;float:left;\"><input type=\"checkbox\" id=\"cur_".$curs[$i]['id']."\" name=\"cur_".$curs[$i]['id']."\" value=\"cur_".$curs[$i]['id']."\" /><span></span>".$curs[$i]['name']."</label>";
											$curstring.=$curs[$i]['id']."-";
										}
										$mystring=substr($curstring,0,-1);
										echo"<input type=\"hidden\" id=\"cursum\" value=\"".$curstring."\"/>";
									?>
								</div>
							</div>
							<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane " id="filter-5">
							<div class="form-group" >
								<label class="col-md-3 control-label" for="partnerselect">Számla szűrés</label>
								<div style="clear:both;height:10px;"></div>
								<div >
									<?
									echo"<label class=\"switch switch-primary\" style=\"width:250px;float:left;\"><input type=\"checkbox\" id=\"csszla\" name=\"csszla\" value=\"csszla\" onclick=\"javascript:documentsDatatables.init();\"/><span></span>Csak számlák</label><div style=\"clear:both;height:1px;\"></div>";
									echo"<label class=\"switch switch-primary\" style=\"width:250px;float:left;\"><input type=\"checkbox\" id=\cskfszla\" name=\"cskfszla\" value=\"cskfszla\" onclick=\"javascript:documentsDatatables.init();\"/><span></span>Csak kifizetett számlák</label><div style=\"clear:both;height:1px;\"></div>";
									echo"<label class=\"switch switch-primary\" style=\"width:250px;float:left;\"><input type=\"checkbox\" id=\"csknfszla\" name=\"csknfszla\" value=\"csknfszla\" onclick=\"javascript:documentsDatatables.init();\"/><span></span>Csak ki nem fizetett számlák</label><div style=\"clear:both;height:1px;\"></div>";
									echo"<label class=\"switch switch-primary\" style=\"width:250px;float:left;\"><input type=\"checkbox\" id=\"cslhszla\" name=\"cslhszla\" value=\"cslhszla\" onclick=\"javascript:documentsDatatables.init();\"/><span></span>Csak lejárt számlák</label><div style=\"clear:both;height:1px;\"></div>";
									?>
								</div>
							</div>
							<div style="clear:both;height:1px;"></div>
						</div>
					
					</div>	
					
					<?										
					echo"</div>
					<div class=\"modal-footer\">
						<button type=\"button\" class=\"btn btn-sm btn-warning\" onclick=\"javascript:documentsDatatables.init();\" >Szűr</button>
						<button type=\"button\" class=\"btn btn-sm btn-default\" data-dismiss=\"modal\">Bezár</button>
					</div>
				</div>
			</div>
		</div>";	
?>
</form>
