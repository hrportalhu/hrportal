/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */
	
var documentsDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

			$('#example-datatable tbody').on('click', 'tr', function () {
			var id = $('td', this).eq(0).text();
				document.location='/modules/docusys/documents/'+id;
			} );
			
			var dirArray="";var catArray="";var curArray="";
			if($("#dirsum").val()!= undefined){
				dirArray = $("#dirsum").val().split('-'); 
			}
			if($("#catsum").val()!= undefined){
				catArray = $("#catsum").val().split('-'); 
			}
			if($("#cursum").val()!= undefined){
				curArray = $("#cursum").val().split('-'); 
			}

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"aaSorting": [[ 0, "desc" ]],
				"iDisplayLength": 30,
				"bDestroy": true,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/docusys/ajax_documents_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
						aoData.push({"name": "partner_id", "value":$("#partnerselect").val() },
								{"name": "dirsum", "value": $("#dirsum").val() } ,
								{"name": "cursum", "value": $("#cursum").val() } ,
								{"name": "catsum", "value": $("#catsum").val() } 
								);
						
						if(catArray.length>0){
							for(i=0;i<catArray.length;i++){
								if($("#cat_"+catArray[i]).prop('checked')){
									aoData.push({"name": $("#cat_"+catArray[i]).val(), "value":"1"});
								}
								else{
									aoData.push({"name": $("#cat_"+catArray[i]).val(), "value":"0"});
								}
							}
						}
						
						if(dirArray.length>0){
							for(i=0;i<dirArray.length;i++){
								
								
								
								if($("#dir_"+dirArray[i]).prop('checked')){
									aoData.push({"name": $("#dir_"+dirArray[i]).val(), "value":"1"});
								}
								else{
									aoData.push({"name": $("#dir_"+dirArray[i]).val(), "value":"0"});
								}
							}
						}
						
						if(curArray.length>0){
							for(i=0;i<curArray.length;i++){
								if($("#cur_"+curArray[i]).prop('checked')){
									aoData.push({"name": $("#cur_"+curArray[i]).val(), "value":"1"});
								}
								else{
									aoData.push({"name": $("#cur_"+curArray[i]).val(), "value":"0"});
								}
							}
						}
			
				if($("#csszla").attr('checked')){aoData.push({"name": "csszla", "value":"1"});}else{aoData.push({"name": "csszla", "value":"0"});}
				if($("#cskfszla").attr('checked')){aoData.push({"name": "cskfszla", "value":"1"});}else{aoData.push({"name": "cskfszla", "value":"0"});}
				if($("#csknfszla").attr('checked')){aoData.push({"name": "csknfszla", "value":"1"});}else{aoData.push({"name": "csknfszla", "value":"0"});}
				if($("#cslhszla").attr('checked')){aoData.push({"name": "cslhszla", "value":"1"});}else{aoData.push({"name": "cslhszla", "value":"0"});}
		
				aoData.push({"name": "startdate", "value":$("#startdate").val()});
				aoData.push({"name": "enddate", "value":$("#enddate").val()});
				aoData.push({"name": "dats", "value":$("#dats").val()});
		
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
					
				}
            });
            
            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveeditdocuments(elemid){
	var valid = f_c($('#editdocuments'));
	if(valid){
		f_xml(ServerName +'/modules/docusys/ajax_documents_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editdocuments').serialize(),'');
	}
}

function savenewdocuments(){
	var valid = f_c($('#newdocuments'));
	if(valid){
		f_xml(ServerName +'/modules/docusys/ajax_documents_data.php','newsubmit=1&' + $('#newdocuments').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/docusys/ajax_documents_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}

function addafaelement(elemid,targetdiv){
	if($('#netto').val()!="" && $('#afa').val()!="0" ){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/docusys/ajax_addafaelement.php',
		data:'newsubmit=1&elemid='+elemid+'&'+$('#newelementformafa').serialize(),
		success: function(data){			
			showafaelement(elemid,targetdiv);
			$('#netto').val('');
			$('#brutto').val('');
		}});
	}
	else{
		alert("Üres tételt nem lehet betölteni!");
	}
}

function showafaelement(elemid,targetdiv){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/docusys/ajax_showafaelement.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#'+targetdiv).html(data);
		}});
}

function addreszlet(elemid,targetdiv){
	if($('#reszosszeg').val()!="" && $('#reszdatum').val()!="" ){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/docusys/ajax_addreszlet.php',
		data:'newsubmit=1&elemid='+elemid+'&'+$('#newelementformreszlet').serialize(),
		success: function(data){			
			showreszlet(elemid,targetdiv);
			$('#reszosszeg').val('');
			$('#reszdatum').val('');
		}});
	}
	else{
		alert("Üres tételt nem lehet betölteni!");
	}
}

function delreszfizeteselem(elemid,delid,targetdiv){
	if(confirm("Biztos töröljem a kiválasztott elemet?")){
			$.ajax({
		type:'POST',
		url: ServerName +'/modules/docusys/ajax_delreszfizeteselem.php',
		data:'delid='+delid,
		success: function(data){			
			showreszlet(elemid,targetdiv);
			
		}});	
	}
}

function showreszlet(elemid,targetdiv){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/docusys/ajax_showreszlet.php',
		data:'elemid='+elemid+'&targetdiv='+targetdiv,
		success: function(data){
			$('#'+targetdiv).html(data);
		}});
}
