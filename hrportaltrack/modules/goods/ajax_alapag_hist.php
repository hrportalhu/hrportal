<?php
$b=db_all("select * from alapag_events where ag_id='".$mymatlist['id']."' order by id desc");
$summennyiseg=db_one("select sum(amount) from alapag_events where ag_id='".$mymatlist['id']."' and (event=1 or event=3 or event=7 or event=6)");
?>

<div >
	
	<form  id="asztalform" id="asztalform" class="form-horizontal form-bordered" onsubmit="return false;">
		<table class="table table-vcenter table-striped">
			<thead>
				<tr>
					<th>Dátum</th>
					<th>Esemény</th>
					<th>Mennyiség</th>
					<th>Tudnivalók</th>
				</tr>
			</thead>
			<tbody>
				<?php
					for($i=0;$i<count($b);$i++){
						$estxt="";
						$tutext="";
						$ag=db_row("select * from alapanyag where id='".$b[$i]['ag_id']."'");
						$agf=db_row("select * from alapanyag_fejlec where id='".$ag['agf_id']."'");
						$alap=db_row("select * from alapag_tipus where id='".$ag['agt_id']."'");
						$mertek=idchange("mertekegyseg","name",$alap['measure']);
						
						if($b[$i]['event']==1){
							$estxt="Alapanyag bevételezes";
							$tutext.="Bevételi szám: <a href=\"/modules/goods/bevetelezes/".$agf['id']."\">".$agf['bev_szlaszam']."</a> LOT:<a href=\"/modules/goods/alapag/".$ag['id']."\">".$ag['lot']." - (".$ag['mmi'].")</a>";
						}
						elseif($b[$i]['event']==2){
							$estxt="Alapanyag bevételezési törlés, valamilyen hiba miatt!";
							$tutext.="Bevételi szám: <a href=\"/modules/goods/bevetelezes/".$agf['id']."\">".$agf['bev_szlaszam']."</a> LOT:<a href=\"/modules/goods/alapag/".$ag['id']."\">".$ag['lot']." - (".$ag['mmi'].")</a>";
						}
						elseif($b[$i]['event']==3){
							$estxt="Raktár kiadás posztra vagy másik raktárba";
							$tutext.="Kiadó raktár: <a href=\"/modules/goods/stores/".$b[$i]['store_id']."\" >".idchange("stores","name",$b[$i]['store_id'])."</a> LOT:<a href=\"/modules/goods/alapag/".$ag['id']."\">".$ag['lot']." - (".$ag['mmi'].")</a>";
						}
						elseif($b[$i]['event']==5){
							$estxt="Poszt bevételezés raktárból";
							$tutext.="Bevételező poszt: <a href=\"/modules/goodworkers/posts/".$b[$i]['post_id']."\" >".idchange("gw_posts","name",$b[$i]['post_id'])."</a> LOT:<a href=\"/modules/goods/alapag/".$ag['id']."\">".$ag['lot']." - (".$ag['mmi'].")</a>";
						}
						elseif($b[$i]['event']==7){
							$estxt="Raktár visszavét posztról";
							$tutext.="
							Viasszaadó poszt: <a href=\"/modules/goodworkers/posts/".$b[$i]['post_id']."\" >".idchange("gw_posts","name",$b[$i]['post_id'])."</a> 
							Fogadó raktár : <a href=\"/modules/goods/stores/".$b[$i]['store_id']."\" >".idchange("stores","name",$b[$i]['stores_id'])."</a> 
							LOT:<a href=\"/modules/goods/alapag/".$ag['id']."\">".$ag['lot']."</a>";
						}
						elseif($b[$i]['event']==8){
							$estxt="Poszt visszadás raktárnak";
							$tutext.="
							Viasszaadó poszt: <a href=\"/modules/goodworkers/posts/".$b[$i]['post_id']."\" >".idchange("gw_posts","name",$b[$i]['post_id'])."</a> 
							Fogadó raktár : <a href=\"/modules/goods/stores/".$b[$i]['store_id']."\" >".idchange("stores","name",$b[$i]['stores_id'])."</a> 
							LOT:<a href=\"/modules/goods/alapag/".$ag['id']."\">".$ag['lot']."</a>";
						}
						elseif($b[$i]['event']==6){
							$estxt="Raktárkorrekció leltárral";
							$tutext.="Leltározott raktár: <a href=\"/modules/goods/stores/".$b[$i]['store_id']."\" >".idchange("stores","name",$b[$i]['store_id'])."</a> LOT:<a href=\"/modules/goods/alapag/".$ag['id']."\">".$ag['lot']." - (".$ag['mmi'].")</a>";
						}
						elseif($b[$i]['event']==10){
							$estxt="Felhasználás a poszton";
							$tutext.="Felhasználó poszt: <a href=\"/modules/goodworkers/posts/".$b[$i]['post_id']."\" >".idchange("gw_posts","name",$b[$i]['post_id'])."</a> ";
						}
						
						echo"<tr>";
						
						echo"<td>".$b[$i]['rdate']."</td>";
						echo"<td>".$estxt."</td>";
						echo"<td>".huf(round($b[$i]['amount'],2))." ".$mertek."</td>";
						echo"<td>".$tutext."</td>";
						echo"</tr>";
					}
				?>
			</tbody>
		</table>
	</form>	
	</div>
	<div style="clear:both;height:10px;"></div>
 <div class="col-sm-6 col-lg-3">
		<!-- Widget -->
		<a href="#" class="widget widget-hover-effect1">
			<div class="widget-simple" style="width:300px;">
				<div class="widget-icon pull-left themed-background animation-fadeIn" style="width:300px;">
					Összesen <?=round($summennyiseg,2);?> Kg
				</div>
			</div>
		</a>
		<!-- END Widget -->
	</div>
	<div style="clear:both;height:10px;"></div>
