<?php

$html="";	

function contentsList($myid,$mydate,$mylist="",$level=0){

    if($level>30)return;

	$agt=db_all("SELECT a.agt_id,b.name,a.amount FROM pt_x_agt a LEFT JOIN alapag_tipus b ON (a.agt_id = b.id) 			WHERE pt_id = '".$myid."' AND amount > 0 AND archdat='0000-00-00 00:00:00'");
	foreach($agt as $ak=>$av){
		if($av['amount']!='0.00'){
		// 1. Megvan az alapanyag-tipus.
			$mylist.="<li><h2>".$av['name']." (".$av['amount']." kg)</h2>";
		// 2. Megkeressuk, hogy ebbol a tipusbol aznap mi volt keszleten
			$alapag=db_all("SELECT a.*,b.bev_ido,c.shortname FROM alapanyag a 
					LEFT JOIN alapanyag_fejlec b ON (a.agf_id = b.id) LEFT JOIN partners c ON (b.partner_id = c.id) WHERE 
					a.agt_id = '".$av['agt_id']."' AND 
					a.created < '".$mydate."' AND
					(a.consumed > '".$mydate."' OR a.consumed IS NULL)
					ORDER BY a.id");
			if(count($alapag)>0){
			    $mylist.="<ul class='alapagul'>";
			    foreach($alapag as $alapk=>$alapv){
			    $mylist.="<li>";
			    if($alapv['alias']!=''&&$alapv['alias']!=$av['name']){$mylist.="Név: ".$alapv['alias']."<br />";}
			    $mylist.="LOT: ".$alapv['lot']."<br />MMI: ".$alapv['mmi']."<br />Szállító: ".
				$alapv['shortname']."</li>";
			    }
			    $mylist.="</ul>";
			}else{
			    $mylist.="Nem található kiadott alapanyag.";
			}
			$mylist.="</li>";
		}
	}
	$pt=db_all("SELECT a.pt2_id,a.amount,b.name,b.amount,b.egyseg FROM pt_x_pt a LEFT JOIN product_types b ON (a.pt2_id = b.id) 
			WHERE pt1_id = '".$myid."' AND a.amount > 0 AND archdat='0000-00-00 00:00:00' ORDER BY a.amount DESC");
	foreach($pt as $pk=>$pv){
		if($pv['amount']!='0.00'){
		// Megvan a termek
			$mylist.="<li class='termekli'><h2>".$pv['name']." (".$pv['amount']." kg)</h2>";
		// Volt-e aznap lebego termek ebbol a tipusbol? Egyelore eleg ennyi
			$floatingproducts=db_all("SELECT name,code,amount FROM products WHERE pt_id = '".$pv['pt2_id']."'
					AND created < '".$mydate."' AND
					(consumed > '".$mydate."' OR consumed IS NULL)
					ORDER BY id");
			if(count($floatingproducts)>0){
				foreach($floatingproducts as $fpk=>$fpv){
					$mylist.="Termék: ".$fpv['name']."<br />";
					if($pv['amount']>$fpv['amount']){ // Tobbet hasznaltunk el, mint amennyi papiron rendelkezesre allt a felkesz termekbol
						$mylist.="<span style='color:red;'>Mennyiség: ".$fpv['amount']." kg<br />Kód:".$fpv['code']."</span>";
					}else{
						$mylist.="Mennyiség: ".$fpv['amount']." kg<br />Kód:".$fpv['code'];
					}
				}
			}else{
				$mylist.="Nem található ".substr($mydate,0,10)."-n legyártott termék.";
			}
		}
		$mylist.="<ul>";
		$mylist=contentsList($pv['pt2_id'],$mydate,$mylist,++$level);
		$mylist.="</ul>";
		$mylist.="</li>";

	}
	return $mylist;
}

	$A=db_row("SELECT a.*,b.store_id,b.szall_ido,b.bev_ido,c.shortname,b.driversname,b.numberplate,d.realname,e.name AS storename FROM alapanyag a
			LEFT JOIN alapanyag_fejlec b ON (a.agf_id = b.id)
			LEFT JOIN partners c ON (b.partner_id = c.id)
			LEFT JOIN users d ON (b.uid = d.id)
			LEFT JOIN stores e ON (b.store_id = e.id)
			WHERE a.id = '".$mymatlist['id']."'");
// Ezt az alapanyagot kell megkeresnunk az osszes termékben, amely abban az idoszakban keszult, amikor ezt az alapanyagot felhasznaltak.
	if($A['consumed']==''){ $A['consumed']=date("Y-m-d H:I:s",strtotime("now")); } // Ha meg nem hasznaltuk fel, akkor a jelen pillanatig elkeszult termekeket fogjuk nezni
	if($A['alias']==''){ $A['alias']=db_one("SELECT name FROM alapag_tipus WHERE id = '".$A['agt_id']."'"); }

	$html.="<div class='shtrack'><h2>Követés - ".$A['alias']."</h2>".
	     "Készletre vétel időpontja: ".$A['created']."<br>".
	     "Szállító: ".$A['shortname']."(".$A['driversname'].", rendszám ".$A['numberplate'].")<br />".
	     "Készletre vette: JOee<br />".
	     "Raktár: Készáru<br />".
	     "Még készleten: ".$A['units']." kg<br />";

	$md=date("Y-m-d H:i:s",strtotime($A['created'])+(86400*5));
	if($A['consumed']=="0000-00-00 00:00:00"){
			$A['consumed']=date("Y-m-d H:i:s",time());
	}

	$T=db_all("SELECT a.*,b.gwp_id  FROM products a LEFT JOIN product_types b ON (a.pt_id = b.id) WHERE created > '".$A['created']."' AND created < '".$md."'  order by created desc,endproduct desc");

	$html.="<h2>Az alábbi termékek készülhettek belőle</h2><div class='trackdiv'>";
	$agid="a".$A['agt_id'].":";
	$html.="<ul>";
	for($i=0;$i<count($T);$i++){
		$vane=db_one("select id from pt_alapanyaghanyad where parent_id='".$T[$i]['pt_id']."' and agt_id='".$mymatlist['agt_id']."'");
		
		if($vane!=""){
			$html.="<li><a href='/modules/goods/products/".$T[$i]['id']."'>".$T[$i]['name']." (".round($T[$i]['amount'],2)." kg)</a><br />".
				"Termék azonosító: ".$T[$i]['code']."<br />".
				"Munkahely: ".db_one("SELECT name FROM gw_posts WHERE id = '".$T[$i]['gwp_id']."'")."<br />".
				"Készítve: ".substr($T[$i]['created'],0,10)."<br />".
				
				"</li>";
		}
	}
	
	$html.="</ul></div></div>";

#header('Content-Type: text/xml;charset=utf-8');
# echo eksemel(0,'',$html,$js);

echo $html;

?>
