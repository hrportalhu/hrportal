<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors','On');
header("Cache-control: private");
header('Content-Type: text/text;charset=utf-8');
include_once dirname(__FILE__).("/../../inc/sys.conf");
include_once dirname(__FILE__).("/../../inc/_functions.php");

$letar=db_row("select * from alapagleltar_fejlec where id='".$_REQUEST['elemid']."'");
$letar_elemets=db_all("select 
							al.*,
							at.name as tname,
							a.lot as lot,
							a.mmi as mmi,
							at.measure as me 
							from alapagleltar al 
								left join alapanyag a on (al.ag_id=a.id) 
								left join alapag_tipus at on (al.agt_id=at.id) 
							where al.aglf_id='".$_REQUEST['elemid']."' order by at.name asc");

?>
	<div class="table-responsive">
		<table class="table table-vcenter table-striped">
			<thead>
				<tr>
					<th style="width: 100px;" class="text-center"><i class="gi gi-disk_save"></i></th>
					<th>Alapanyag</th>
					<th>Alapanyag típus</th>
					<th>Rendszerben</th>
					<th>Készleten</th>
					
					<th>Magyarázat</th>
					<th style="width: 50px;" class="text-center">Művelet</th>
				</tr>
			</thead>
			<tbody>
				<?

					$row[]="Sorszám";
					$row[]="Alapanyag Név";
					$row[]="Rendszerben talált mennyiség";
					$row[]="Ténylegesen talált mennyiség";
					$row[]="Eltérés";
					$row[]="Mértékegység";
					$output['aaData'][] = $row;
				for($i=0;$i<count($letar_elemets);$i++){
					$row = array();
					$col="";if($letar_elemets[$i]['ok']==1){
						$col="background-color:yellow;";
					}
					echo"<tr >

						<td>".$letar_elemets[$i]['id']."</a></td>
						<td><a href=\"/modules/goods/materials/".$letar_elemets[$i]['agt_id']."\">".$letar_elemets[$i]['tname']."</a></td>
						<td>LOT:".$letar_elemets[$i]['lot']." MMI:".$letar_elemets[$i]['mmi']."</td>
						<td>".round($letar_elemets[$i]['system_amount'],3)."</td>
						<td >
							<div class=\"input-group\">
								<input type=\"text\" id=\"found_".$letar_elemets[$i]['id']."\" name=\"found_".$letar_elemets[$i]['id']."\"  class=\"form-control\" value=\"".round($letar_elemets[$i]['found_amount'],3)."\" />
								<span class=\"input-group-addon\">".idchange("mertekegyseg","name",$letar_elemets[$i]['me'])."</span>
							</div>
						</td>
						
						<td><input type=\"text\" id=\"exp_".$letar_elemets[$i]['id']."\" name=\"exp_".$letar_elemets[$i]['id']."\"  class=\"form-control\" value=\"".$letar_elemets[$i]['explanation']."\" /></td>";
						echo"<td class=\"text-center\">";
						if($letar['closed']==0){
							echo"<div class=\"btn-group btn-group-xs\" >";
								echo"<a href=\"javascript:letarok('".$letar_elemets[$i]['id']."');\" data-toggle=\"tooltip\" title=\"Jóváhagy\"  class=\"btn btn-info\"><i class=\"gi gi-floppy_saved\"></i></a>";
							echo"<div id=\"let_".$letar_elemets[$i]['id']."\" style=\"width:5px;float:left;height:22px;".$col."\">&nbsp;</div>";
							echo"</div>";
						}	
							
						echo"</td>
					</tr>";
					
					$row[]=$letar_elemets[$i]['id'];
					$row[]=$letar_elemets[$i]['tname']." (LOT:".$letar_elemets[$i]['lot']." MMI:".$letar_elemets[$i]['mmi'].")";
					$row[]=round($letar_elemets[$i]['system_amount'],3);
					$row[]=round($letar_elemets[$i]['found_amount'],3);
					$row[]=round($letar_elemets[$i]['system_amount'],3)-round($letar_elemets[$i]['found_amount'],3);
					$row[]=idchange("mertekegyseg","name",$letar_elemets[$i]['me']);
					$output['aaData'][] = $row;
				}
				?>

			</tbody>
		</table>
	</div>
	<div style="clear:both;height:10px;"></div>
	<?
	if($letar['closed']==0){
	?>
	<div class="col-md-9 col-md-offset-0">
		<button type="button" class="btn btn-sm btn-primary" id="savenewbutton" onclick="javascript:closeleltar(<?=$_REQUEST['elemid'];?>);"><i class="fa fa-angle-right"></i> Lezár	</button>
	</div>
	<?
}
	?>
	<div style="clear:both;height:10px;"></div>
<?
$_SESSION['planetsys']['exportdata'] = $output ;
mysql_close($connid);
?>
