<?php
session_start();
if(!isset($_SESSION['planetsys']['user_id']) || $_SESSION['planetsys']['user_id']=='' || $_SESSION['planetsys']['user_id']==0 ){
	Header( "Location: '/'" ); 
}
error_reporting(E_ALL);
ini_set('display_errors','Off');
header("Cache-control: private");
header('Content-Type: text/xml;charset=utf-8');
include_once dirname(__FILE__).("/../../inc/sys.conf");
include_once dirname(__FILE__).("/../../inc/_functions.php");
header('Content-Type: text/xml;charset=utf-8');
$saved = 1;
$errmsg = array();
$html="";
$statuscode = 0;

if(!isset($_REQUEST['editsubmit'])){$_REQUEST['editsubmit']=0;}
if(!isset($_REQUEST['backsubmit'])){$_REQUEST['backsubmit']=0;}
if($_REQUEST['newsubmit']==1){
	$most=date("H:i:s",time());
	$transcode=date("Ymdhis",time()).generateCode(10);
	$statuscode=0;
	list($tetel,$raktar)=explode("-",$_POST['tetel']);
	$_POST['amount']=str_replace(",",".",$_POST['amount']); // Magyarok eloszeretettel vesszot adnak meg tort erteknek
	$item=db_row("SELECT * FROM alapanyag WHERE id = '".$tetel."' ORDER BY mmi ASC");
	$mennyi=db_one("select sum(amount) from alapag_events where store_id='".$raktar."' and ag_id='".$tetel."' and (event=1 or event=3 or event=7 or event=6) ");
	if($mennyi>=$_POST['amount']){
		db_execute("UPDATE alapag_tipus SET units = '".($item['units']-$_POST['amount'])."' WHERE id = '".$item['agt_id']."'");
		$u=db_execute("UPDATE alapanyag SET units = '".($item['units']-$_POST['amount'])."' WHERE id = '".$item['id']."'");
		naplo('info','goods','Alapanyag csökkent. ID: '.$item['id'].", mennyiség: ".($item['units']-$_POST['amount']).", raktáros: ".$_SESSION['planetsys']['realname']);
		$e=db_execute("INSERT INTO alapag_events(ag_id,agt_id,transaction_code,store_id,post_id,rdate,event,params,amount)VALUES('".$item['id']."','".$item['agt_id']."','".$transcode."','".$raktar."','".$_POST['munkahely']."','".$_POST['rdate']." ".$most."','3','-".$_POST['amount']."','-".$_POST['amount']."')");
		$e=db_execute("INSERT INTO alapag_events(ag_id,agt_id,transaction_code,store_id,post_id,rdate,event,params,amount)VALUES('".$item['id']."','".$item['agt_id']."','".$transcode."','".$raktar."','".$_POST['munkahely']."','".$_POST['rdate']." ".$most."','5','".$_POST['amount']."','".$_POST['amount']."')");
		$js="issueDatatables.init();";
	}
	else{
		$errmsg[]=eeea("error","Ezt a mennyiséget nem lehet kiadni, mert ebben a raktárban nincs ekkora készlet! Maximálisan kiadható mennyiség: ".$mennyi);
		$js=" sendalert('Ezt a mennyiséget nem lehet kiadni, mert ebben a raktárban nincs ekkora készlet! Maximálisan kiadható mennyiség:".$mennyi."')";
	}

}
if($_REQUEST['editsubmit']==1){
	$event1=db_row("select * from alapag_events where id='".$_REQUEST['aid']."'");
	$event2=db_row("select * from alapag_events where transaction_code='".$event1['transaction_code']."' and id!='".$event1['id']."'");
	
	
	$_POST['amount']=str_replace(",",".",$_POST['amount']); // Magyarok eloszeretettel vesszot adnak meg tort erteknek
	$item=db_row("SELECT * FROM alapanyag WHERE id = '".$_POST['tetel']."' ORDER BY mmi ASC");
	
	db_execute("UPDATE alapag_tipus SET units = '".($item['units']-$_POST['amount'])."' WHERE id = '".$item['agt_id']."'");
	$u=db_execute("UPDATE alapanyag SET units = '".($item['units']-$_POST['amount'])."' WHERE id = '".$item['id']."'");
	
	naplo('info','goods','Alapanyag módosult. ID: '.$item['id'].", mennyiség: ".($item['units']-$_POST['amount']).", raktáros: ".$_SESSION['planetsys']['realname']);
	
	db_execute("update alapag_events set post_id='".$_POST['munkahely']."',rdate='".$_POST['rdate']."',params='-".$_POST['amount']."',amount='-".$_POST['amount']."' where id='".$event1['id']."'");
	db_execute("update alapag_events set post_id='".$_POST['munkahely']."',rdate='".$_POST['rdate']."',params='".$_POST['amount']."',amount='".$_POST['amount']."' where id='".$event2['id']."'");
	//echo "update alapag_events set post_id='".$_POST['munkahely']."',rdate='".$_POST['rdate']."',params='-".$_POST['amount']."',amount='-".$_POST['amount']."' where id='".$event1['id']."'";
	
	$js="document.location='/modules/goods/issue/".$_REQUEST['aid']."'";
		
}
if($_REQUEST['backsubmit']==1){
	$event1=db_row("select * from alapag_events where id='".$_REQUEST['aid']."'");
	$event2=db_row("select * from alapag_events where transaction_code='".$event1['transaction_code']."'");
	$transcode=date("Ymdhis",time()).generateCode(10);
	

	$statuscode=0;
	$_POST['amount']=str_replace(",",".",$_POST['amount']); // Magyarok eloszeretettel vesszot adnak meg tort erteknek
	$item=db_row("SELECT * FROM alapanyag WHERE id = '".$_POST['tetel']."' ORDER BY mmi ASC");
	
	db_execute("UPDATE alapag_tipus SET units = '".($item['units']+$_POST['amount'])."' WHERE id = '".$item['agt_id']."'");
	$u=db_execute("UPDATE alapanyag SET units = '".($item['units']+$_POST['amount'])."' WHERE id = '".$item['id']."'");
	
	naplo('info','goods','Alapanyag visszavét. ID: '.$item['id'].", mennyiség: ".($item['units']-$_POST['amount']).", raktáros: ".$_SESSION['planetsys']['realname']);
	$e=db_execute("INSERT INTO alapag_events(ag_id,agt_id,transaction_code,store_id,post_id,rdate,event,params,amount)VALUES('".$item['id']."','".$item['agt_id']."','".$transcode."','".$_POST['store']."','".$_POST['munkahely']."','".$_POST['rdate']." ".$most."','7','".$_POST['amount']."','".$_POST['amount']."')");
	$e=db_execute("INSERT INTO alapag_events(ag_id,agt_id,transaction_code,store_id,post_id,rdate,event,params,amount)VALUES('".$item['id']."','".$item['agt_id']."','".$transcode."','".$_POST['store']."','".$_POST['munkahely']."','".$_POST['rdate']." ".$most."','8','-".$_POST['amount']."','-".$_POST['amount']."')");

	$js="document.location='/modules/goods/issue/'";
		
}

echo eksemel($statuscode,$errmsg,$html,$js);

?>
