<?php
session_start();
header("Cache-control: private");
header('Content-Type: text/plain;charset=utf-8');
include_once dirname(__FILE__).("/../../inc/sys.conf");
include_once dirname(__FILE__).("/../../inc/_functions.php");

//Tábla, mező olvasási definíciók
	$qri="select 
					ae.id as id, 
					ae.agt_id as agt_id,
					ae.ag_id as ag_id, 
					ae.amount as amount, 
					ae.transaction_code as transcode, 
					ae.store_id as store_id,
					ae.post_id as post_id,
					ae.event as event,
					ae.rdate as rdate,
					agt.name as name,
					a.lot as lot,
					a.mmi as mmi,
					a.created as created,
					gwp.name as postname,
					st.name as storename
						from alapag_events ae 
						left join alapanyag a on(ae.ag_id=a.id)
						left join alapag_tipus agt on (ae.agt_id=agt.id)
						left join gw_posts gwp on (ae.post_id=gwp.id)
						left join stores st on (ae.store_id=st.id)
					where 
					(ae.event='3' or ae.event='7')  ";
					

	$qri2="select 
					count(*) 
						from alapag_events ae 
						left join alapanyag a on(ae.ag_id=a.id)
						left join alapag_tipus agt on (ae.agt_id=agt.id)
						left join gw_posts gwp on (ae.post_id=gwp.id)
						left join stores st on (ae.store_id=st.id)
					where 
					(ae.event='3' or ae.event='7') ";
					


//Limit definíció
	$sLimit = "";
	if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){$sLimit = "LIMIT ".mysql_real_escape_string( $_GET['iDisplayStart'] ).", ".mysql_real_escape_string( $_GET['iDisplayLength'] );}

//Rendezés
	$sOrder = "";
	if ( isset( $_GET['iSortCol_0'] ) ){
		$sOrder = "ORDER BY ";
		if($_GET['iSortCol_0']=="0"){$sOrder.=" ae.id ";}
		if($_GET['iSortCol_0']=="1"){$sOrder.=" agt.name ";}
		if($_GET['iSortCol_0']=="2"){$sOrder.=" amount ";}
		if($_GET['iSortCol_0']=="3"){$sOrder.=" a.lot ";}
		if($_GET['iSortCol_0']=="4"){$sOrder.=" a.mmi ";}
		if($_GET['iSortCol_0']=="5"){$sOrder.=" a.created ";}
		if($_GET['iSortCol_0']=="6"){$sOrder.=" gwp.name ";}
		if($_GET['sSortDir_0']=="asc"){$sOrder.=" asc ";}else{$sOrder.=" desc ";}
	}
//keresés
	$sWhere =  " ";
	if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
		$sWhere.= " and (
						ae.agt_id LIKE  '%".mysql_real_escape_string( $_GET['sSearch'] )."%' or 
						ag_id LIKE  '%".mysql_real_escape_string( $_GET['sSearch'] )."%' or 
						amount LIKE  '%".mysql_real_escape_string( $_GET['sSearch'] )."%' or 
						ae.agt_id LIKE  '%".mysql_real_escape_string( $_GET['sSearch'] )."%' or 
						agt.name LIKE  '%".mysql_real_escape_string( $_GET['sSearch'] )."%' or 
						a.lot LIKE  '%".mysql_real_escape_string( $_GET['sSearch'] )."%' or 
						gwp.name LIKE  '%".mysql_real_escape_string( $_GET['sSearch'] )."%' or 
						st.name LIKE  '%".mysql_real_escape_string( $_GET['sSearch'] )."%'  
						) ";
	}
	$sWhere2="";

	$elems=db_all($qri." ".$sWhere."  ".$sOrder." ".$sLimit."");
	$elemdb=db_one($qri2." ".$sWhere);
	$iFilteredTotal =  count($elems);
	$iTotal = db_one($qri2);
	$output = array("sEcho" => intval($_GET['sEcho']),"iTotalRecords" => $iTotal,"iTotalDisplayRecords" =>  $elemdb,"aaData" => array());

	for($k=0;$k<count($elems);$k++){
		$row = array();
		$row[] = $elems[$k]['id'];
		$row[] = "<a href=\"/modules/goods/materials/".$elems[$k]['agt_id']."\">".$elems[$k]['name']."</a>";
		$row[] = round($elems[$k]['amount'],3);
		$row[] = $elems[$k]['lot'];
		$row[] = $elems[$k]['mmi'];
		$row[] = $elems[$k]['rdate'];
		$row[] = $elems[$k]['postname'];
		$row[] = "<a href=\"/modules/goods/stores/".$elems[$k]['store_id']."\">".$elems[$k]['storename']."</a>";
		if($elems[$k]['event']=="3"){
			$event="Kiadás raktárból";	
		}
		else{
			$event="Visszavétel raktárba";	
		}
		$row[] = $event;
		$output['aaData'][] = $row;
	}
$_SESSION['planetsys']['exportdata'] = $output ;
echo json_encode( $output );
mysql_close($connid);	
?>
