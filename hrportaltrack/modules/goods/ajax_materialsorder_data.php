<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors','Off');
header("Cache-control: private");
header('Content-Type: text/xml;charset=utf-8');
include_once dirname(__FILE__).("/../../inc/sys.conf");
include_once dirname(__FILE__).("/../../inc/_functions.php");

$myname = 'keyplace';
$saved = 1;
$errmsg = array();
$statuscode = 0;

if($_POST['newsubmit']=='1'){
	$check=db_one("SELECT id FROM materials_orders_header WHERE order_date = '".$_POST['order_date']."' and want_date='".$_POST['want_date']."' ");
	if($check==""){
		$st=1;
		
		$q="insert into materials_orders_header 
			(order_date,want_date,user_id,status) 
			value 
			('".$_POST['order_date']."','".$_POST['want_date']."','".$_SESSION['planetsys']['user_id']."','".$st."')";
		$insid=db_execute($q);
		if($insid==1){
			$errmsg[]=eeea("msg","Sikeres felvitel.");
			$_POST['id']=mysql_insert_id();
			$_POST['view']='edit';
			naplo('info','user',"Új megrendelés rögzítve. Elérése: http://if.plsys.hu/modules/goods/materialsorder/".$_POST['id']."","goods","materialsorder",$_POST['id'],addslashes($q));
			$js="document.location='modules/goods/materialsorder/".$_POST['id']."'";
		}
		else{
			$errmsg[]=eeea("error","Felvitel sikertelen!".$q.")");
			$saved=0;
			$statuscode = 1;
			$_POST['view']='edit';
		}
	}
	else{
		$errmsg[]=eeea("error","Ilyen elem már létezik!");
		$saved=0;
		$statuscode = 1;
		$_POST['new']=1;
	}
}
elseif(is_numeric($_POST['editsubmit'])){
	$saved=0;
	$_POST['edit']=$_POST['editsubmit'];
	
	
	$q="UPDATE materials_orders_header SET 
				order_date='".$_POST['order_date']."',
				want_date='".$_POST['want_date']."' WHERE id = '".$_POST['editsubmit']."'";
	$updid=db_execute($q);
				
				
	naplo('info','user',"Megrendelés módosítva","goods","materialsorder",$_POST['editsubmit'],addslashes($q));			
	if($updid==1){
		$errmsg[]=eeea("msg","Sikeres módosítás");
	}
	else{
		$errmsg[]=eeea("error","Sikertelen változtatás!");
		$saved=0;
		$statuscode = 1;
	}
}
elseif(is_numeric($_POST['neweditorderadat'])){
//$rend=db_row("select * from materials_orders where orderheader_id='".$_REQUEST['elemid']."' and agt_id='".$_REQUEST['edit_id']."'");

	$o=db_row("select * from materials_orders_header where id='".$_REQUEST['elemid']."' ");
	$agt=db_row("select * from alapag_tipus where id='".$_REQUEST['edit_id']."'");
	$check=db_one("select id from materials_orders where orderheader_id='".$_REQUEST['elemid']."' and agt_id='".$_REQUEST['edit_id']."' and status>0");
	if($check=="" && $o['last_ins']=='0000-00-00 00:00:00'){
		$q="insert into materials_orders (orderheader_id,agt_id,partner_id,me,mee,order_date,want_date,order_user_id) 
		values ('".$_REQUEST['elemid']."','".$_REQUEST['edit_id']."','".$agt['dealer_id']."','".dot($_REQUEST['me'])."','".$_REQUEST['mee']."','".$o['order_date']."','".$o['want_date']."','".$_SESSION['planetsys']['user_id']."')";
		db_execute($q);
		naplo('info','user',"Rendelés tétel rögzítve: ".$agt['name']." - ".$_REQUEST['me']." - ".idchange("mertekegyseg","name",$_REQUEST['mee'])." ","goods","materialsorder",$_REQUEST['elemid'],addslashes($q));			
	}
	else{
		$q="update materials_orders set me='".dot($_REQUEST['me'])."' where id='".$check."'";
		db_execute($q);
		
		naplo('info','user',"Rendelés tétel módosítva: ".$agt['name']." - ".$_REQUEST['me']." - ".idchange("mertekegyseg","name",$_REQUEST['mee'])." ","goods","materialsorder",$_REQUEST['elemid'],addslashes($q));			
		$errmsg[]=eeea("msg","Sikeres módosítás");
	}
	$js="$('#ordrow_'+".$_REQUEST['edit_id'].").html('Rögzítve')";
}
elseif(is_numeric($_POST['aproveorderadat'])){


	$o=db_row("select * from materials_orders_header where id='".$_REQUEST['elemid']."' ");
	$agt=db_row("select * from alapag_tipus where id='".$_REQUEST['edit_id']."'");
	$check=db_one("select id from materials_orders where orderheader_id='".$_REQUEST['elemid']."' and agt_id='".$_REQUEST['edit_id']."' and status>0");
	$spec=db_row("select * from alapag_tipus_besz where id='".$_REQUEST['spec']."'");
	$partner=db_row("select * from partners where id='".$spec['beszallito_id']."'");
	$ordercode=$agt['id']."-".$check."-".$partner['id']."-".date("Ymd",time());
	$q="update materials_orders set me='".dot($_REQUEST['me'])."', partner_id='".$spec['beszallito_id']."',aprove=CURDATE(),aprove_user='".$_SESSION['planetsys']['user_id']."',mail_address='".$partner['email']."',ordercode_element='".$ordercode."' where id='".$check."'";
	db_execute($q);
	$errmsg[]=eeea("msg","Sikeres jóváhagyás");
	
	
	naplo('info','user',"Rendelés tétel jóváhagyva: ".$agt['name']." - ".$_REQUEST['me']." ","goods","materialsorder",$_REQUEST['elemid'],addslashes($q));			
	$js="$('#oordrow_'+".$_REQUEST['edit_id'].").html('Jóváhagyva')";
}
elseif(is_numeric($_POST['delorderadat'])){


	$o=db_row("select * from materials_orders_header where id='".$_REQUEST['elemid']."' ");
	$agt=db_row("select * from alapag_tipus where id='".$_REQUEST['edit_id']."'");
	$check=db_one("select id from materials_orders where orderheader_id='".$_REQUEST['elemid']."' and agt_id='".$_REQUEST['edit_id']."' and status>0");
	$spec=db_row("select * from alapag_tipus_besz where id='".$_REQUEST['spec']."'");
	$partner=db_row("select * from partners where id='".$spec['beszallito_id']."'");
	$ordercode=$agt['id']."-".$check."-".$partner['id']."-".date("Ymd",time());
	$q="delete from materials_orders  where id='".$check."'";
	//echo $q;
	db_execute($q);
	$errmsg[]=eeea("msg","Törlés");
	
	
	naplo('info','user',"Rendelés tétel törlés: ".$agt['name']." - ".$_REQUEST['me']." ","goods","materialsorder",$_REQUEST['elemid'],addslashes($q));			
	$js="document.location='/modules/goods/materialsorder/".$_REQUEST['elemid']."/tab-7'";
}
elseif(is_numeric($_POST['chorderpartner'])){


	$o=db_row("select * from materials_orders_header where id='".$_REQUEST['order_id']."' ");
	$agt=db_row("select * from alapag_tipus where id='".$_REQUEST['agt_id']."'");
	$check=db_one("select id from materials_orders where orderheader_id='".$_REQUEST['order_id']."' and agt_id='".$_REQUEST['agt_id']."' and status>0");
	$spec=db_row("select * from alapag_tipus_besz where id='".$_REQUEST['newpartner']."'");
	$partner=db_row("select * from partners where id='".$spec['beszallito_id']."'");
	$ordercode=$agt['id']."-".$check."-".$partner['id']."-".date("Ymd",time());
	$q="update materials_orders set partner_id='".$spec['beszallito_id']."',mail_address='".$partner['email']."'  where id='".$check."'";
	//echo $q;
	db_execute($q);
	$errmsg[]=eeea("msg","Beszállító módosítás");
	
	
	naplo('info','user',"Rendelés tétel módosítás: ".$agt['name']." - ".$_REQUEST['me']." ","goods","materialsorder",$_REQUEST['elemid'],addslashes($q));			
	$js="document.location='/modules/goods/materialsorder/".$_REQUEST['order_id']."/tab-7'";
}
elseif(is_numeric($_POST['aproveallorderadat'])){
	$o=db_row("select * from materials_orders_header where id='".$_REQUEST['elemid']."' ");
	$partner=db_row("select * from partners where id='".$_REQUEST['partner_id']."'");
	$cuccok=db_all("select * from materials_orders where partner_id='".$_REQUEST['partner_id']."' and orderheader_id='".$_REQUEST['elemid']."'");
	
	for($i=0;$i<count($cuccok);$i++){
		$ordercode=$cuccok[$i]['agt_id']."-".$cuccok[$i]['id']."-".$partner['id']."-".date("Ymd",time());
		$q="update materials_orders set aprove=CURDATE(),aprove_user='".$_SESSION['planetsys']['user_id']."',mail_address='".$partner['email']."',ordercode_element='".$ordercode."' where id='".$cuccok[$i]['id']."'";
		db_execute($q);	
		naplo('info','user',"Rendelés tétel jóváhagyva: ".idchange("alapag_tipus","name",$cuccok[$i]['agt_id'])."  ","goods","materialsorder",$_REQUEST['elemid'],addslashes($q));			
	}
	$errmsg[]=eeea("msg","Sikeres jóváhagyás");
	$js="document.location='/modules/goods/materialsorder/".$_REQUEST['elemid']."/tab-7'";
}
elseif(is_numeric($_POST['closematerialsorder'])){
	$q="update materials_orders_header set last_ins=NOW() where id='".$_REQUEST['order_id']."'";
	db_execute($q);
	naplo('info','user',"Rendelés rögztítése lezárva","goods","materialsorder",$_REQUEST['order_id'],addslashes($q));			
	
	$targy="Rendelés jóváhagyásra vár. NO: ORD-".sprintf('%1$08d',$_REQUEST['order_id'])." - ".date("Y.m.d",time());
	$tartalom="Kedves Kata,\r\nZolika egy új rendeléssel kedveskedett neked, amit jóvá kellene hagynod. A rendelést a http://if.plsys.hu/modules/goods/materialsorder/".$_REQUEST['order_id']." oldalon találod.\r\nKellemes napot :)";
	$cimzett="jozsef.szucs@planetstar.hu";
	gomail($cimzett,$tartalom,$targy);
	$cimzett="onagykatalin@interfoodeuropa.hu";
	gomail($cimzett,$tartalom,$targy);
	$errmsg[]=eeea("msg","Sikeres felvitel lezárás");
	$js="document.location='/modules/goods/materialsorder/".$_REQUEST['order_id']."/tab-6'";
	//$js="";
}
elseif(is_numeric($_POST['testordermail'])){
	$tetels=db_all("select * from materials_orders where partner_id='".$_REQUEST['partner_id']."' and orderheader_id='".$_REQUEST['order_id']."'");
	$orderhead=db_row("select * from materials_orders_header where id='".$_REQUEST['order_id']."'");
	$targy="Rendelés Interfood-Europa Kft. NO: ORD-".sprintf('%1$08d',$_REQUEST['order_id'])." - ".date("Y.m.d",time());

	$tartalom="
Tisztelt Partnerünk!\r\n
A ORD-".sprintf('%1$08d',$_REQUEST['order_id'])." sorszámú rendelésünket alább láthatja!\r\n
Kért szállítási nap: ".$orderhead['want_date']."\r\n
Szeretném tájékoztatni, hogy az árut 6-11 között tudjuk átvenni, időkapun kívüli időpontban a Raktárunk visszautasíthatja az átvételt!\r\nTermékspecifikációtól eltérő és lejárat közeli alapanyagot nem áll módunkban átvenni!\r\n\r\n";
$htmltartalom="
<h3>Tisztelt Partnerünk!</h3>
A ORD-".sprintf('%1$08d',$_REQUEST['order_id'])." rendelésünket alább láthatja!<br />
Kért szállítási nap: <b>".$orderhead['want_date']."</b><br />
Szeretném tájékoztatni, hogy az árut <b>6-11</b> között tudjuk átvenni, időkapun kívüli időpontban a Raktárunk visszautasíthatja az átvételt!\r\nTermékspecifikációtól eltérő és lejárat közeli alapanyagot nem áll módunkban átvenni!<br /><br /><b>Rendelés:</b>";
$htmltartalom.="<table style=\"border: 1px solid black; border-collapse: collapse;\">";
for($i=0;$i<count($tetels);$i++){
$tartalom.="".idchange("alapag_tipus","name",$tetels[$i]['agt_id'])."\t-|-\t".$tetels[$i]['me']." ".idchange("mertekegyseg","name",$tetels[$i]['mee'])."\t-|-\tRendelés kód: ".$tetels[$i]['ordercode_element']."\r\n";	
$htmltartalom.="<tr style=\"border: 1px solid black; border-collapse: collapse;\"><td style=\"border: 1px solid black; border-collapse: collapse;\">".idchange("alapag_tipus","name",$tetels[$i]['agt_id'])."</td><td style=\"border: 1px solid black; border-collapse: collapse;text-align:right;\">".$tetels[$i]['me']." ".idchange("mertekegyseg","name",$tetels[$i]['mee'])."</td><td style=\"border: 1px solid black; border-collapse: collapse;\">Rendelés kód: ".$tetels[$i]['ordercode_element']."</td></tr>";	
}
$tartalom.="
Kérjük, rendelésünk visszaigazolását a ORD-".sprintf('%1$08d',$_REQUEST['order_id'])." rendelésszámra való hivatkozással !\r\n
Köszönjük!\r\nÜdvözlettel, Interfood-Europa Kft.\r\n";
$htmltartalom.="</table><br />
Kérjük, rendelésünk visszaigazolását a ORD-".sprintf('%1$08d',$_REQUEST['order_id'])." rendelésszámra való hivatkozással !<br />
Köszönjük!<br />Üdvözlettel, Interfood-Europa Kft.<br />";
	
	
	$cimzett="jozsef.szucs@planetstar.hu";
	gomailhtml($cimzett,$tartalom,$htmltartalom,$targy);
	naplo('info','user',"Tesztlevél küldése","goods","materialsorder",$_REQUEST['order_id'],"");			
$js="document.location='/modules/goods/materialsorder/".$_REQUEST['elemid']."/tab-7'";
//	$cimzett="onagykatalin@interfoodeuropa.hu";
//	gomailhtml($cimzett,$tartalom,$htmltartalom,$targy);
	
}
elseif(is_numeric($_POST['sendordermail'])){
	$tetels=db_all("select * from materials_orders where partner_id='".$_REQUEST['partner_id']."' and orderheader_id='".$_REQUEST['order_id']."'");
	$orderhead=db_row("select * from materials_orders_header where id='".$_REQUEST['order_id']."'");
	$targy="Rendelés Interfood-Europa Kft. NO: ORD-".sprintf('%1$08d',$_REQUEST['order_id'])." - ".date("Y.m.d",time());

	$tartalom="
Tisztelt Partnerünk!\r\n
A ORD-".sprintf('%1$08d',$_REQUEST['order_id'])." sorszámú rendelésünket alább láthatja!\r\n
Kért szállítási nap: ".$orderhead['want_date']."\r\n
Szeretném tájékoztatni, hogy az árut 6-11 között tudjuk átvenni, időkapun kívüli időpontban a Raktárunk visszautasíthatja az átvételt!\r\nTermékspecifikációtól eltérő és lejárat közeli alapanyagot nem áll módunkban átvenni!\r\n\r\n";
$htmltartalom="
<h3>Tisztelt Partnerünk!</h3>
A ORD-".sprintf('%1$08d',$_REQUEST['order_id'])." rendelésünket alább láthatja!<br />
Kért szállítási nap: <b>".$orderhead['want_date']."</b><br />
Szeretném tájékoztatni, hogy az árut <b>6-11</b> között tudjuk átvenni, időkapun kívüli időpontban a Raktárunk visszautasíthatja az átvételt!\r\nTermékspecifikációtól eltérő és lejárat közeli alapanyagot nem áll módunkban átvenni!<br /><br /><b>Rendelés:</b>";
$htmltartalom.="<table style=\"border: 1px solid black; border-collapse: collapse;\">";
for($i=0;$i<count($tetels);$i++){
$tartalom.="".idchange("alapag_tipus","name",$tetels[$i]['agt_id'])."\t-|-\t".$tetels[$i]['me']." ".idchange("mertekegyseg","name",$tetels[$i]['mee'])."\t-|-\tRendelés kód: ".$tetels[$i]['ordercode_element']."\r\n";	
$htmltartalom.="<tr style=\"border: 1px solid black; border-collapse: collapse;\"><td style=\"border: 1px solid black; border-collapse: collapse;\">".idchange("alapag_tipus","name",$tetels[$i]['agt_id'])."</td><td style=\"border: 1px solid black; border-collapse: collapse;text-align:right;\">".$tetels[$i]['me']." ".idchange("mertekegyseg","name",$tetels[$i]['mee'])."</td><td style=\"border: 1px solid black; border-collapse: collapse;\">Rendelés kód: ".$tetels[$i]['ordercode_element']."</td></tr>";	
}
$tartalom.="
Kérjük, rendelésünk visszaigazolását a ORD-".sprintf('%1$08d',$_REQUEST['order_id'])." rendelésszámra való hivatkozással !\r\n
Köszönjük!\r\nÜdvözlettel, Interfood-Europa Kft.\r\n";
$htmltartalom.="</table><br />
Kérjük, rendelésünk visszaigazolását a ORD-".sprintf('%1$08d',$_REQUEST['order_id'])." rendelésszámra való hivatkozással !<br />
Köszönjük!<br />Üdvözlettel, Interfood-Europa Kft.<br />";
	
	
	$cimzett="jozsef.szucs@planetstar.hu";
	gomailhtml($cimzett,$tartalom,$htmltartalom,$targy);	
	$q="update materials_orders set mail_send=1 where  partner_id='".$_REQUEST['partner_id']."' and orderheader_id='".$_REQUEST['order_id']."'";
	db_execute($q);
	naplo('info','user',"Megrendelő levél kiküldve","goods","materialsorder",$_REQUEST['order_id'],addslashes($q));			

	$js="document.location='/modules/goods/materialsorder/".$_REQUEST['elemid']."/tab-7'";
}

if($_REQUEST['command']=="savesetupscreen"){
	if($_REQUEST['viewtype']=="setupscreen"){
		$elemek="";
		foreach($_POST as $nev => $defs){
			if(substr($nev,0,4)=="AAA_"){
				$elemek.=substr($nev,4).",";	
			}
		}
		$elemek=substr($elemek,0,-1);
		db_execute("update listoptions set pagelimit='".$_REQUEST['mylimit']."',screenlist='".$elemek."' where user_id='".$_SESSION['planetsys']['user_id']."' and modulname='".$_SESSION['planetsys']['actpage']['modulname']."' and funcname='".$_SESSION['planetsys']['actpage']['funcname']."'");
		$js.="keyplaceDatatables.init();";
	}
}

echo eksemel($statuscode,$errmsg,$html,$js);

?>
