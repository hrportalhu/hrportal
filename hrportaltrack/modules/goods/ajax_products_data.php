<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors','Off');
header("Cache-control: private");
header('Content-Type: text/xml;charset=utf-8');
include_once dirname(__FILE__).("/../../inc/sys.conf");
include_once dirname(__FILE__).("/../../inc/_functions.php");

$myname = 'products';
$saved = 1;
$errmsg = array();
$statuscode = 0;

function recurseContents($myid,$mylist="",$level=0){

    if($level>30)return;

    if($level==0){ // Termekben szereplo alapanyagot, es termekben szereplo termeket sem viszunk fel! (Nem is kene rekurzio!)
		$agt=db_all("SELECT agt_id,amount FROM pt_x_agt WHERE pt_id = '".$myid."' AND amount > 0 AND archdat='0000-00-00 00:00:00'");
		foreach($agt as $ak=>$av){
			if($av['amount']!='0.00'){ $mylist.="a".$av['agt_id'].":".$av['amount']."|"; }
		}

		$pt=db_all("SELECT pt2_id,amount FROM pt_x_pt WHERE pt1_id = '".$myid."' AND amount > 0 AND archdat='0000-00-00 00:00:00' ORDER BY amount DESC");
		foreach($pt as $pk=>$pv){
			if($pv['amount']!='0.00'){ $mylist.="p".$pv['pt2_id'].":".$pv['amount']."|"; }
			$mylist=recurseContents($pv['pt2_id'],$mylist,++$level);
		}
    }

    return $mylist;
}


if($_POST['newsubmit']=='1'){
	$cont=recurseContents($_POST['mypt_id']);
	$check=db_one("SELECT id FROM products WHERE name = '".$_POST['name']."'");
	//if($check==""){
		$me=mee($_POST['mypt_id']);
		$pt=db_row("select * from product_types where id='".$_POST['mypt_id']."'");
		$tw=$_POST['amount']*$me['gyartas_szorzo'];
		$st=0;if(isset($_POST['status']) && $_POST['status']=="on"){$st=1;}
		$insid=db_execute("insert into products (pt_id,name,contents,code,amount,created,store_id,total_weight,endproduct,status) 
		value ('".addslashes($_POST['mypt_id'])."','".$pt['name']."','".$cont."','".addslashes($_POST['code'])."','".addslashes($_POST['amount'])."','".addslashes($_POST['created'])."','".addslashes($_POST['store_id'])."','".$tw."','".$pt['endproduct']."','".$st."')");
		
		if($insid==1){
			$errmsg[]=eeea("msg","Sikeres felvitel.");
			$_POST['id']=mysql_insert_id();
			$_POST['view']='edit';
			$js="document.location='modules/goods/products/".$_POST['id']."'";
		}
		else{
			$errmsg[]=eeea("error","Felvitel sikertelen!".$q.")");
			$saved=0;
			$statuscode = 1;
			$_POST['view']='edit';
		}
	//}
	//else{
	//	$errmsg[]=eeea("error","Ilyen nevű felhasználó már létezik!");
	//	$saved=0;
	//	$statuscode = 1;
	//	$_POST['new']=1;
	//}
}

if($_REQUEST['command']=="savesetupscreen"){
	if($_REQUEST['viewtype']=="setupscreen"){
		$elemek="";
		foreach($_POST as $nev => $defs){
			if(substr($nev,0,4)=="AAA_"){
				$elemek.=substr($nev,4).",";	
			}
		}
		$elemek=substr($elemek,0,-1);
		db_execute("update listoptions set pagelimit='".$_REQUEST['mylimit']."',screenlist='".$elemek."' where user_id='".$_SESSION['planetsys']['user_id']."' and modulname='".$_SESSION['planetsys']['actpage']['modulname']."' and funcname='".$_SESSION['planetsys']['actpage']['funcname']."'");
		$js.="ProductsDatatables.init();";
	}
}

echo eksemel($statuscode,$errmsg,$html,$js);

?>
