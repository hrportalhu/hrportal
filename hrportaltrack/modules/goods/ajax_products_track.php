<?php

function contentsList($myid,$mydate,$tamount,$mylist="",$level=0){
	global $myproducts;
	$startdate=date("Y-m-d H:i:s",strtotime($mydate)-86400);
	$enddate=date("Y-m-d H:i:s",strtotime($mydate)+(86400*2));
	$mostl=sprintf('%1$03d',date('z',strtotime($mydate))+1);
    if($level>30)return;

	$agt=db_all("SELECT a.agt_id as agt_id, b.name as name,a.amount as amount FROM pt_x_agt a LEFT JOIN alapag_tipus b ON (a.agt_id = b.id) 	WHERE pt_id = '".$myid."' AND amount > 0 AND archdat='0000-00-00 00:00:00'");
	for($a=0;$a<count($agt);$a++){
		$mraid=generateCode();
		if($agt[$a]['amount']!='0.00'){
			$mypt=db_row("select * from product_types where id='".$myid."'");
			$mylist.="<li ><span style=\"float:right;background-color:#fff;\"><button class=\"btn btn-xs btn-info\" onclick=\"javascript:tl('lay_".$mraid."')\"><i class=\"gi gi-zoom_in\"></i></button></span><div style=\"height:24px;color:#fff;background-color:#545454;padding:2px;\">".$agt[$a]['name']." (".round($tamount*$agt[$a]['amount'],2)." kg)</div>";
			$alapag=db_all("select * from alapag_events where agt_id='".$agt[$a]['agt_id']."'  and event='5' and rdate between '".$startdate."' and '".$enddate."' group by ag_id");
			
			//echo "select * from alapag_events where agt_id='".$agt[$a]['agt_id']."' and  post_id='".$mypt['gwp_id']."' and event='5' and rdate between '".$startdate."' and '".$enddate."' group by ag_id";
			if(count($alapag)>0){
				$mylist.="<ul class=\"alapagul\" style=\"display:none;\" id=\"lay_".$mraid."\">";
			    for($b=0;$b<count($alapag);$b++){
					$am=db_row("select * from alapanyag where id='".$alapag[$b]['ag_id']."'  ");	
					$mylist.="<li style=\"margin:3px;padding:0;\">";
					$mylist.="Név: <a href=\"/modules/goods/materials/".$agt[$a]['agt_id']."\">".$agt[$a]['name']."</a><br />";
					$mylist.="LOT: <a href=\"/modules/goods/alapag/".$am['id']."\">".$am['lot']."<br />MMI: ".$am['mmi']."</a></li>";
			    }
			    $mylist.="</ul>";
			}
			else{
				$mylist.="<ul class=\"alapagul\" style=\"display:none;\" id=\"lay_".$mraid."\">";
				$am=db_row("select * from alapanyag where agt_id='".$agt[$a]['agt_id']."' and consumed='0000-00-00 00:00:00' order by id asc");	
				//$mylist.="Nem található kiadott alapanyag.";
				$mylist.="<li style=\"margin:3px;padding:0;\">";
				$mylist.="Név: <a href=\"/modules/goods/materials/".$agt[$a]['agt_id']."\">".$agt[$a]['name']."</a><br />";
				$mylist.="LOT: <a href=\"/modules/goods/alapag/".$am['id']."\">".$am['lot']."<br />MMI: ".$am['mmi']."</a></li>";
				$mylist.="</ul>";
			}
			$mylist.="</li>";
		}
	}
	
	$pt=db_all("SELECT a.pt2_id,a.amount,b.name FROM pt_x_pt a LEFT JOIN product_types b ON (a.pt2_id = b.id) WHERE pt1_id = '".$myid."' AND a.amount > 0 AND archdat='0000-00-00 00:00:00' ORDER BY a.amount DESC");
	//echo "SELECT a.pt2_id,a.amount,b.name FROM pt_x_pt a LEFT JOIN product_types b ON (a.pt2_id = b.id) WHERE pt1_id = '".$myid."' AND a.amount > 0 AND archdat='0000-00-00 00:00:00' ORDER BY a.amount DESC<br />";
	$mmismall=db_one("select mmi from product_types where id='".$myid."'");
	
	foreach($pt as $pk=>$pv){
		if($pv['amount']!='0.00'){
		// Megvan a termek
			$mmibig=db_one("select mmi from product_types where id='".$pv['pt2_id']."'");
			
			if($mmibig==$mmismall){
				$mmidif=$mmibig;	
			}
			elseif($mmbig>$mmsmall){
				$mmidif=$mmibig-$mmismall;	
			}
			
			if($mmidif>1){
				$mmidif=1;	
			}
			$mmidif=1;	
			$mytime=strtotime($mydate);
			$mindate=date("Y-m-d",$mytime-(86400*$mmidif));
			
			
		// Volt-e aznap lebego termek ebbol a tipusbol? Egyelore eleg ennyi
			$floatingproducts=db_all("SELECT id,name,code,amount,created FROM ".$tableprefix."products WHERE pt_id = '".$pv['pt2_id']."' AND created between '".$mindate."' and '".$mydate."'  ORDER BY id");
			$myprodsum=db_one("SELECT sum(amount) FROM ".$tableprefix."products WHERE pt_id = '".$pv['pt2_id']."' AND created = '".$mydate."'  ORDER BY id");
			$gunitw=db_row("select * from pt_units where pt_id='".$pv['pt2_id']."' and units_type='2' and unit2_id='1'" );
			$mraid2=generateCode();
			$mylist.="<li class=\"termekli\"><span style=\"float:right;background-color:#fff;\"><button class=\"btn btn-xs btn-info\" onclick=\"javascript:tl('lay_".$mraid2."')\"><i class=\"gi gi-zoom_in\"></i></button></span><div style=\"height:24px;color:#fff;background-color:#ababab;padding:2px;font-weight:bold;\">".$pv['name']." Teljes gyártott mennyiség: ".($myprodsum*$gunitw['mesure2'])." Kg (Ebbe a termékbe felhasználva belőle: ".$pv['amount']*$tamount." kg)</div>";		
			$mylist.="<div id=\"lay_".$mraid2."\" style=\"display:none;\">";
			if(count($floatingproducts)>0){
				foreach($floatingproducts as $fpk=>$fpv){
					
					$mylist.="Termék: <a href=\"/modules/goods/producttype/".$pv['pt2_id']."\">".$fpv['name']."</a><br />";
					if($pv['amount']>$fpv['amount']){ // Tobbet hasznaltunk el, mint amennyi papiron rendelkezesre allt a felkesz termekbol
						$mylist.="<span style='color:red;'>Gyártott mennyiség: ".$fpv['amount']." ".idchange("mertekegyseg","name",$gunitw['unit1_id'])."<br /><a href=\"/modules/goods/products/".$fpv['id']."\">Kód:".$fpv['code']."</a></span><br />";
					}else{
						$mylist.="Mennyiség: ".$fpv['amount']." ".idchange("mertekegyseg","name",$gunitw['unit1_id'])."<br /><a href=\"/modules/goods/products/".$fpv['id']."\">Kód:".$fpv['code']."</a><br />";
					}
					$tapado=db_all("select * from pt_darabaru where pt_id='".$pv['pt2_id']."'");
					if(count($tapado)>0){
									$ag_id=db_one("select ag_id from alapag_events where agt_id='".$tapado[$a]['agt_id']."'  and event='5' and rdate between '2014-01-01 00:00:00' and '".$T['created']."' order by rdate DESC");
									$agi=db_row("select * from alapanyag where id='".$ag_id."'");
									$agidate=strtotime($agi['created'])+86400;
									$nap=date("z",$agidate);
						$mylist.="Tapadó és csomagoló darabáruk:<br />";
						for($a=0;$a<count($tapado);$a++){
							if(round($tapado[$a]['darab'],4)==0){
								$tapado[$a]['darab']=1;	
							}
							$mylist.=idchange("alapag_tipus","name",$tapado[$a]['agt_id']).": ".round($tapado[$a]['darab'],4)." db (LOT:".$mostl.")<br />";
						}	
					}
				}
			}else{
				$mylist.="Nem található ".substr($mindate,0,10)." és ".substr($mydate,0,10)." között legyártott termék.";
			}
		}
		$mylist.="<ul>";
		$mylist=contentsList($pv['pt2_id'],$mydate,($fpv['amount']),$mylist,++$level);
		$tapado=db_all("select * from pt_darabaru where pt_id='".$pv['pt2_id']."'");
		if(count($tapado)>0){
			$mylist.="Tapadó és csomagoló darabáruk:<br />";
			for($a=0;$a<count($tapado);$a++){
				if(round($tapado[$a]['darab'],4)==0){
					$tapado[$a]['darab']=1;	
				}
			$ag_id=db_one("select ag_id from alapag_events where agt_id='".$tapado[$a]['agt_id']."'  and event='5' and rdate between '2014-01-01 00:00:00' and '".$T['created']."' order by rdate DESC");
			$agi=db_row("select * from alapanyag where id='".$ag_id."'");
			$agidate=strtotime($agi['created'])+86400;
			$nap=date("z",$agidate);
				
				$mylist.=idchange("alapag_tipus","name",$tapado[$a]['agt_id']).": ".round($tapado[$a]['darab'],4)." db (LOT/NNSZ:".$nap.")<br />";
			}	
		}
		$mylist.="</ul>";
		$mylist.="</div>";
		$mylist.="</li>";

	}
	return $mylist;
}

	$T=db_row("SELECT * FROM ".$tableprefix."products WHERE id = '".$myproducts['id']."'");
	$gunit=db_row("select * from pt_units where pt_id='".$T['pt_id']."' and units_type='2' and unit2_id='1'" );
	$szorzat=$gunit['mesure2']*$T['amount'];
	$html.="<div class='shtrack'>
				<h2 style=\"font-size:14px;font-weight:bold;\">Termék követés - ".$T['name'].", ".$T['code']." (".substr($T['created'],0,10).")<br >Rendelt mennyiség: ".round($T['total_weight'],3)." Kg - Gyártott mennyiség: ".$szorzat." Kg azaz ".round($T['amount'])." ".idchange("mertekegyseg","name",$gunit['unit1_id'])." </h2>
				
				<div class='trackdiv'>
					<ul>".contentsList($T['pt_id'],$T['created'],$T['amount'])."</ul>
				</div>
			</div>";
	$tapado=db_all("select * from pt_darabaru where pt_id='".$T['pt_id']."'");
	$mostl=sprintf('%1$03d',date('z',strtotime($T['created']))+1);
	if(count($tapado)>0){
		$html.="Tapadó és csomagoló darabáruk:<br />";
		for($a=0;$a<count($tapado);$a++){
			if(round($tapado[$a]['darab'],4)==0){
				$tapado[$a]['darab']=1;	
			}
			$getdarab=db_one("select termek_darab from orders_elements where termek_id='".$T['pt_id']."' and szall_date='".substr($T['created'],0,10)."'");
			//
			
			$ag_id=db_one("select ag_id from alapag_events where agt_id='".$tapado[$a]['agt_id']."'  and event='5' and rdate between '2014-01-01 00:00:00' and '".$T['created']."' order by rdate DESC");
			$agi=db_row("select * from alapanyag where id='".$ag_id."'");
			//print_r($agi);
			$agidate=strtotime($agi['created'])+86400;
			
			$nap=date("z",$agidate);
			//echo "----".$nap;
			$html.="<a href=\"/modules/goods/alapag/".$ag_id."\">".idchange("alapag_tipus","name",$tapado[$a]['agt_id']).": ".round($getdarab,4)." db (LOT/NNSZ/:".$nap.")</a><br />";
		}	
	}
echo $html;

?>
