<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors','Off');
header("Cache-control: private");
header('Content-Type: text/plain;charset=utf-8');
include_once dirname(__FILE__).("/../../inc/sys.conf");
include_once dirname(__FILE__).("/../../inc/_functions.php");

$screearray=explode(",",$_SESSION['planetsys']['actpage']['screenlist']);
$cusfilter="";
$catfilter="";

if(isset($_REQUEST['cussum'])){
	$cuselement=explode("-",$_REQUEST['cussum']);
	for($z=0;$z<count($cuselement);$z++){
		if($_REQUEST['cus_'.$cuselement[$z]]==1){ 
			if($cusfilter==""){$cusfilter.="and ("; }
			$cusfilter.=" ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".customer_id='".$cuselement[$z]."' or";
		}
	}
	if($cusfilter!=""){$cusfilter=substr($cusfilter,0,-3).") ";}
}

if(isset($_REQUEST['catsum'])){
	$catelement=explode("-",$_REQUEST['catsum']);
	for($z=0;$z<count($catelement);$z++){
		if(isset($_REQUEST['cat_'.$catelement[$z]]) && $_REQUEST['cat_'.$catelement[$z]]==1){ 
			if($catfilter==""){$catfilter.="and ("; }
			$catfilter.=" ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".gwp_id='".$catelement[$z]."' or";	
		}	
	}
	if($catfilter!=""){$catfilter=substr($catfilter,0,-3).") ";	}
}
//Tábla, mező olvasási definíciók
	$selee="";
	$joiele=" from ".$_SESSION['planetsys']['actpage']['scrtable']." ".$_SESSION['planetsys']['actpage']['scrtable_shortname']." ";
	for($i=0;$i<count($screearray);$i++){
		//if(!isset($_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['con_table_fields'][0])){}
		if($_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['connected']=="1"){
			$selee.=" ".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['con_table_shortname'].".".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['con_table_fields'][1]." as ".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['name'].",";
			$joiele.=" LEFT JOIN ".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['con_table']." ".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['con_table_shortname']." on (".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['name']."=".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['con_table_shortname'].".".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['con_table_fields'][0]."  )";
		}
		else{
			if($_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['name']!=""){
				$selee.=" ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['name']." as ".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['name'].",";		
			}
		}
	}
	$selee=substr($selee,0,-1);

//Limit definíció
	$sLimit = "";
	if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){$sLimit = "LIMIT ".mysql_real_escape_string( $_GET['iDisplayStart'] ).", ".mysql_real_escape_string( $_GET['iDisplayLength'] );}

//Rendezés
	$sOrder = "";
	if ( isset( $_GET['iSortCol_0'] ) ){
		$sOrder = "ORDER BY  ";
		if($_SESSION['planetsys']['actpage']['elements'][$screearray[$_GET['iSortCol_0']]]['connected']=="1"){
			$sOrder.=" ".$_SESSION['planetsys']['actpage']['elements'][$screearray[$_GET['iSortCol_0']]]['con_table_shortname'].".".$_SESSION['planetsys']['actpage']['elements'][$screearray[$_GET['iSortCol_0']]]['con_table_fields'][1]."";	
		}
		else{
			$sOrder.=" ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".".$screearray[$_GET['iSortCol_0']]."";	
		}
		if($_GET['sSortDir_0']=="asc"){$sOrder.=" asc ";}else{$sOrder.=" desc ";}
	}
//keresés
	$sWhere =  " ";
	if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
		$sWhere.= " and ( ";
		for($i=0;$i<count($screearray);$i++){
			if($_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['connected']=="1"){
				$sWhere.=" ".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['con_table_shortname'].".".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['con_table_fields'][1]." LIKE  '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
			}
			else{
				if($screearray[$i]!=""){
					$sWhere.=" ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".".$screearray[$i]." LIKE  '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
				}
			}
		}
		$sWhere=substr($sWhere,0,-4);
		$sWhere.= ")";
	}
	$sWhere2="";
#	if($_SESSION['planetsys']['user']['superadmin']!='1'){
#		$sWhere2= " and ((".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".private = 1 AND ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".owner = '".$_SESSION['planet']['user_id'].
#		"') OR (".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".private = 0))";
#	}

	$sWhere4="";
	if($_REQUEST['active']==1){
		$sWhere4.=" and ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".status='1' ";
	}
	else{
		if($_REQUEST['active']==2){
			$sWhere4.=" and ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".status='0' ";
		}
	}
	$_SESSION['planetsys']['producttype']['filter']['active']=$_REQUEST['active'];
	$sWhere5="";
	if($_REQUEST['efproduct']==1){
		$sWhere5.=" and ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".endproduct='1' ";
	}
	else{
		if($_REQUEST['efproduct']==2){
			$sWhere5.=" and ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".halfproduct='1' ";
		}
	}
	$_SESSION['planetsys']['producttype']['filter']['efproduct']=$_REQUEST['efproduct'];
	
	$elems=db_all("SELECT ".$selee." ".$joiele." where ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".id>0 ".$sWhere2." ".$sWhere5." ".$sWhere4." ".$sWhere." ".$cusfilter."  ".$catfilter."  ".$sOrder." ".$sLimit."");
	$elemdb=db_one("SELECT count(id) from ".$_SESSION['planetsys']['actpage']['scrtable']." ".$_SESSION['planetsys']['actpage']['scrtable_shortname']." where ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".id>0   ".$sOrder." ");
	$iFilteredTotal =  count($elems);
	$iTotal = db_one("SELECT COUNT(id) FROM  ".$_SESSION['planetsys']['actpage']['scrtable']." WHERE id>0 ");
	$output = array("sEcho" => intval($_GET['sEcho']),"iTotalRecords" => $iTotal,"iTotalDisplayRecords" =>  $elemdb,"aaData" => array());

	for($k=0;$k<count($elems);$k++){
		$row = array();
		//$row[] = $elems[$k]['id'];
		
		for($z=0;$z<count($screearray);$z++){
			if($screearray[$z]=="status"){if($elems[$k][$screearray[$z]]==1){$elems[$k][$screearray[$z]]="Aktív";}else{$elems[$k][$screearray[$z]]="Inaktív";}}
			if($screearray[$z]=="visszaru"){if($elems[$k][$screearray[$z]]==1){$elems[$k][$screearray[$z]]="van";}else{$elems[$k][$screearray[$z]]="Nem";}}
			if($screearray[$z]=="lezarva"){if($elems[$k][$screearray[$z]]==1){$elems[$k][$screearray[$z]]="Lezárva";}else{$elems[$k][$screearray[$z]]="Nincs Lezárva";}}
			if($screearray[$z]=="megjegyzes"){if($elems[$k][$screearray[$z]]!=""){$elems[$k][$screearray[$z]]=substr($elems[$k][$screearray[$z]],0,40);	}}
			if($screearray[$z]=="name"){if($elems[$k][$screearray[$z]]!=""){$elems[$k][$screearray[$z]]=iconv("utf-8","utf-8",$elems[$k][$screearray[$z]]);	}}
			if($screearray[$z]=="amount"){$elems[$k][$screearray[$z]]=round($elems[$k][$screearray[$z]],3);}
			if($screearray[$z]=="energiakj"){$elems[$k][$screearray[$z]]="<div style=\"text-align:center;\">".round($elems[$k][$screearray[$z]],3)." kJ</div>";}
			if($screearray[$z]=="energiakc"){$elems[$k][$screearray[$z]]="<div style=\"text-align:center;\">".round($elems[$k][$screearray[$z]],3)." kcal</div>";}
			if($screearray[$z]=="feherje"){$elems[$k][$screearray[$z]]="<div style=\"text-align:center;\">".round($elems[$k][$screearray[$z]],3)." g</div>";}
			if($screearray[$z]=="zsir"){$elems[$k][$screearray[$z]]="<div style=\"text-align:center;\">".round($elems[$k][$screearray[$z]],3)." g</div>";}
			if($screearray[$z]=="szenhidrat"){$elems[$k][$screearray[$z]]="<div style=\"text-align:center;\">".round($elems[$k][$screearray[$z]],3)." g</div>";}
			if($screearray[$z]=="cukor"){$elems[$k][$screearray[$z]]="<div style=\"text-align:center;\">".round($elems[$k][$screearray[$z]],3)." g</div>";}
			if($screearray[$z]=="rost"){$elems[$k][$screearray[$z]]="<div style=\"text-align:center;\">".round($elems[$k][$screearray[$z]],3)." g</div>";}
			if($screearray[$z]=="koleszterin"){$elems[$k][$screearray[$z]]="<div style=\"text-align:center;\">".round($elems[$k][$screearray[$z]],3)." g</div>";}
			if($screearray[$z]=="so"){$elems[$k][$screearray[$z]]="<div style=\"text-align:center;\">".round($elems[$k][$screearray[$z]],3)." g</div>";}
			if(isset($elems[$k][$screearray[$z]])){
				$row[] = $elems[$k][$screearray[$z]];
			}
			else{
				$row[] = "";
			}
		}
		$output['aaData'][] = $row;
	}
$_SESSION['planetsys']['exportdata'] = $output ;	
echo json_encode( $output );
mysql_close($connid);	
?>
