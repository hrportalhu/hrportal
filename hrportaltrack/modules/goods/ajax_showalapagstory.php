<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors','Off');
header("Cache-control: private");
header('Content-Type: text/plain;charset=utf-8');
include_once dirname(__FILE__).("/../../inc/sys.conf");
include_once dirname(__FILE__).("/../../inc/_functions.php");

if($_REQUEST['event_id']==0){
	$qt=" and (event=1 or event=3 or event=7 or event=6) ";	
}
else{
	$qt=" and event='".$_REQUEST['event_id']."' ";	
}
if($_REQUEST['limit']==0){
	$ql=" ";	
}
else{
	$ql=" limit ".$_REQUEST['limit'];	
}

$dat="";
if($_REQUEST['matstdate']!="" && $_REQUEST['matendate']!=""){
	$dat=" and rdate between '".$_REQUEST['matstdate']." 00:00:00' and '".$_REQUEST['matendate']." 23:59:59' ";	
}


$b=db_all("select * from alapag_events where agt_id='".$_REQUEST['elemid']."' ".$qt." ".$dat."  order by id desc ".$ql." ");

?>
<div >

	<form  id="asztalform" id="asztalform" class="form-horizontal form-bordered" onsubmit="return false;">
		<table class="table table-vcenter table-striped">
			<thead>
				<tr>
					<th>ID</th>
					<th>Dátum</th>
					<th>Esemény</th>
					<th>Mennyiség</th>
					<th>Tudnivalók</th>
					<?
						if($_REQUEST['event_id']=="1" && priv("materials","baseprice")){
							echo"<th>Beszállító</th>";	
							echo"<th>Besz ár</th>";	
						}
					?>
				</tr>
			</thead>
			<tbody>
				<?php
				$output=array();
				$da=0;
					for($i=0;$i<count($b);$i++){
						$row = array();
						$estxt="";
						$tutext="";
						$ag=db_row("select * from alapanyag where id='".$b[$i]['ag_id']."'");
						$agf=db_row("select * from alapanyag_fejlec where id='".$ag['agf_id']."'");
						$alap=db_row("select * from alapag_tipus where id='".$ag['agt_id']."'");
						$mertek=idchange("mertekegyseg","name",$alap['measure']);
						
						if($b[$i]['event']==1){
							$estxt="Alapanyag bevételezes külső beszállítótol, vagy másik raktárból";
							$tutext.="Bevételi szám: <a href=\"/modules/goods/bevetelezes/".$agf['id']."\">".$agf['bev_szlaszam']."</a> LOT:<a href=\"/modules/goods/alapag/".$ag['id']."\">".$ag['lot']." - (".$ag['mmi'].")</a>";
						}
						elseif($b[$i]['event']==2){
							$estxt="Alapanyag bevételezési törlés, valamilyen hiba miatt!";
							$tutext.="Bevételi szám: <a href=\"/modules/goods/bevetelezes/".$agf['id']."\">".$agf['bev_szlaszam']."</a> LOT:<a href=\"/modules/goods/alapag/".$ag['id']."\">".$ag['lot']." - (".$ag['mmi'].")</a>";
						}
						elseif($b[$i]['event']==3){
							$estxt="Raktár kiadás posztra vagy másik raktárba";
							$tutext.="Kiadó raktár: <a href=\"/modules/goods/stores/".$b[$i]['store_id']."\" >".idchange("stores","name",$b[$i]['store_id'])."</a> LOT:<a href=\"/modules/goods/alapag/".$ag['id']."\">".$ag['lot']." - (".$ag['mmi'].")</a>";
						}
						elseif($b[$i]['event']==5){
							$estxt="Poszt bevételezés raktárból";
							$tutext.="Bevételező poszt: <a href=\"/modules/goodworkers/posts/".$b[$i]['post_id']."\" >".idchange("gw_posts","name",$b[$i]['post_id'])."</a> LOT:<a href=\"/modules/goods/alapag/".$ag['id']."\">".$ag['lot']." - (".$ag['mmi'].")</a>";
						}
						elseif($b[$i]['event']==7){
							$estxt="Raktár visszavét posztról";
							$tutext.="
							Viasszaadó poszt: <a href=\"/modules/goodworkers/posts/".$b[$i]['post_id']."\" >".idchange("gw_posts","name",$b[$i]['post_id'])."</a> 
							Fogadó raktár : <a href=\"/modules/goods/stores/".$b[$i]['store_id']."\" >".idchange("stores","name",$b[$i]['stores_id'])."</a> 
							LOT:<a href=\"/modules/goods/alapag/".$ag['id']."\">".$ag['lot']."</a>";
						}
						elseif($b[$i]['event']==8){
							$estxt="Poszt visszadás raktárnak";
							$tutext.="
							Viasszaadó poszt: <a href=\"/modules/goodworkers/posts/".$b[$i]['post_id']."\" >".idchange("gw_posts","name",$b[$i]['post_id'])."</a> 
							Fogadó raktár : <a href=\"/modules/goods/stores/".$b[$i]['store_id']."\" >".idchange("stores","name",$b[$i]['stores_id'])."</a> 
							LOT:<a href=\"/modules/goods/alapag/".$ag['id']."\">".$ag['lot']."</a>";
						}
						elseif($b[$i]['event']==6){
							$estxt="Raktárkorrekció leltárral ".limit_text($b[$i]['params'],100);
							$fullm=$b[$i]['params'];
							$tutext.="Leltározott raktár: <a href=\"/modules/goods/stores/".$b[$i]['store_id']."\" >".idchange("stores","name",$b[$i]['store_id'])."</a> LOT:<a href=\"/modules/goods/alapag/".$ag['id']."\">".$ag['lot']." - (".$ag['mmi'].")</a>";
						}
						elseif($b[$i]['event']==10){
							$estxt="Felhasználás a poszton";
							$tutext.="Felhasználó poszt: <a href=\"/modules/goodworkers/posts/".$b[$i]['post_id']."\" >".idchange("gw_posts","name",$b[$i]['post_id'])."</a> ";
						}
						
						echo"<tr>";
						
						echo"<td>".$b[$i]['id']."</td>";
						echo"<td>".$b[$i]['rdate']."</td>";
						echo"<td data-toggle=\"popover\" data-content=\"".$fullm."\" data-placement=\"top\" title=\"Információk\" >".$estxt."</td>";
						echo"<td>".round($b[$i]['amount'],2)." ".$mertek."</td>";
						echo"<td>".$tutext."</td>";
						if($_REQUEST['event_id']=="1" && priv("materials","baseprice")){
							echo"<td>".idchange("partners","name",$agf['partner_id'])."</td>";
							echo"<td>".$ag['price']." Ft/Kg</td>";
						}
						echo"</tr>";
						$row[]=$b[$i]['id'];
						$row[]=$b[$i]['rdate'];
						$row[]=$estxt;
						$row[]=round($b[$i]['amount'],2)." ".$mertek;
						$row[]=$tutext;
						$output['aaData'][] = $row;
						$da=$da+round($b[$i]['amount'],2);
					}
					$row = array();
						$row[]="Összesen:";
						$row[]="";
						$row[]="";
						$row[]=$da." ".$mertek;
						$row[]="* A mennyiség sok összetevőtől függ, emiatt tájékoztató adatként szerepel!";
						$output['aaData'][] = $row;

					echo"<tr>";
						
						echo"<td colspan=\"3\">összesen:</td>";
						echo"<td>".$da." ".$mertek."</td>";
						echo"<td>* A mennyiség sok összetevőtől függ, emiatt tájékoztató adatként szerepel!</td>";
						echo"</tr>";
				?>
			</tbody>
		</table>
	</form>	

	</div>
	<div style="clear:both;height:10px;"></div>
<?
$_SESSION['planetsys']['exportdata'] = $output ;	
mysql_close($connid);
?>
