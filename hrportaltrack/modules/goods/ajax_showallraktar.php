<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors','Off');
header("Cache-control: private");
header('Content-Type: text/plain;charset=utf-8');
include_once dirname(__FILE__).("/../../inc/sys.conf");
include_once dirname(__FILE__).("/../../inc/_functions.php");

$stoelement=explode("-",$_REQUEST['stosum']);
$stofilter="";
$stos=array();
for($z=0;$z<count($stoelement);$z++){
	if($_REQUEST['sto_'.$stoelement[$z]]==1){ 
		if($stofilter==""){$stofilter.=" ("; }
		$stofilter.=" ae.store_id='".$stoelement[$z]."' or";
		$stos[]=$stoelement[$z];
	}
}
if($stofilter!=""){$stofilter=substr($stofilter,0,-3).") ";}
$output['aaData']=array();
?>
<table class="table table-vcenter table-striped">
	<thead>
		<tr>
			
			<th>Alapanyag név</th>
			<?
			$row=array();
				$row[]="Alapanyag sorszám";
				$row[]="Alapanyag név";
				for($a=0;$a<count($stos);$a++){
					echo"<th>".idchange("stores","name",$stos[$a])."</th>";
					$row[]=idchange("stores","name",$stos[$a]);
				}
				$row[]="Jelenlegi össz mennyiség";
				$row[]="Beszár";
				$output['aaData'][] = $row;
				
			?>
			<th>Jelenlegi össz mennyiség</th>
			<th>Beszár</th>
		</tr>
	</thead>
	<tbody>
		<?php
			if($_POST['teteles']==1){
				$myalapgs=db_all("select ae.agt_id as agt_id,ae.ag_id as ag_id, at.name as name  
					from 
						alapag_events ae 
							left join alapag_tipus at on (ae.agt_id=at.id) 
							left join alapanyag a on (ae.ag_id=a.id) 
						where ".$stofilter." and  (ae.event=1 or ae.event=3 or ae.event=7  or ae.event=6) and consumed='0000-00-00 00:00:00' and ae.rdate<'".$_POST['enddate']." 23:59:59' and ae.ag_id>0 group by ae.ag_id order by at.name");
								$sumar=0;
				$artomb=array();
				for($a=0;$a<count($myalapgs);$a++){
					$row = array();
					$pt=db_one("select price from alapanyag where agt_id='".$myalapgs[$a]['agt_id']."'");
					$al=db_row("select * from alapanyag where id='".$myalapgs[$a]['ag_id']."'");
					$elemdb=0;
					echo"<tr>";
					echo"<td><a href=\"/modules/goods/materials/".$myalapgs[$a]['agt_id']."\">".$myalapgs[$a]['name']." (".$al['lot'].")</a></td>";
					$row[]=$myalapgs[$a]['agt_id'];
					$row[]=$myalapgs[$a]['name'];
					for($b=0;$b<count($stos);$b++){
						$mennyi=gettetel($myalapgs[$a]['ag_id'],$stos[$b],$_POST['startdate'],$_POST['enddate']);
						$elemdb=$elemdb+$mennyi;
						$sumar=$sumar+(round($pt)*round($mennyi));
						echo"<td>".round($mennyi,2)." Kg</td>";
						$row[]=round($mennyi,2);


					}
					
					$row[]=round($elemdb,2);
					$row[]=huf(round($pt)*round($elemdb))." Ft";
					$output['aaData'][] = $row;
					echo"<td class=\"text-right\">".round($elemdb,2)." Kg</td>";
					echo"<td class=\"text-right\">".huf(round($pt)*round($elemdb))." Ft</td>";
					echo"</tr>";
					
				}
			}
			else{
				$myalapgs=db_all("select ae.agt_id as agt_id, at.name as name  from alapag_events ae left join alapag_tipus at on (ae.agt_id=at.id) where ".$stofilter." and  (ae.event=1 or ae.event=3 or ae.event=7  or ae.event=6) and ae.agt_id>0 group by ae.agt_id order by at.name");
				//echo"select ae.agt_id agt_id,at.name as name  from alapag_events ae left join alapag_tipus at on (ae.agt_id=at.id) where ".$stofilter." and  (ae.event=1 or ae.event=3 or ae.event=7  or ae.event=6) group by ae.agt_id order by at.name";
				
				//$instore=db_all("select ae.agt_id as agt_id,at.name as name  from alapag_events ae left join alapag_tipus at on ae.agt_id=at.id  where store_id!='".$mystores['id']."' group by agt_id order by at.name asc ");
				$sumar=0;
				$artomb=array();
				for($a=0;$a<count($myalapgs);$a++){
					$row = array();
					$pt=db_one("select price from alapanyag where agt_id='".$myalapgs[$a]['agt_id']."'");
					$elemdb=0;
					for($b=0;$b<count($stos);$b++){
						$mennyi=getalap($myalapgs[$a]['agt_id'],$stos[$b],$_POST['startdate']." 00:00:00",$_POST['enddate']." 23:59:59");
						$elemdb=$elemdb+$mennyi;
					}
					
					if($elemdb!=0){						
						echo"<tr>";
						echo"<td><a href=\"/modules/goods/materials/".$myalapgs[$a]['agt_id']."\">".$myalapgs[$a]['name']."</a></td>";
						$row[]=$myalapgs[$a]['agt_id'];
						$row[]=$myalapgs[$a]['name'];
						for($b=0;$b<count($stos);$b++){
							$mennyi=getalap($myalapgs[$a]['agt_id'],$stos[$b],$_POST['startdate']." 00:00:00",$_POST['enddate']." 23:59:59");
							$elemdb=$elemdb+$mennyi;
							$sumar=$sumar+(round($pt)*round($mennyi));
							echo"<td>".round($mennyi,2)." Kg</td>";
							$row[]=round($mennyi,2);
						}
						$row[]=round($elemdb,2);
						$row[]=huf(round($pt)*round($elemdb))." Ft";
						$output['aaData'][] = $row;
						echo"<td class=\"text-right\">".round($elemdb,2)." Kg</td>";
						echo"<td class=\"text-right\">".huf(round($pt)*round($elemdb))." Ft</td>";
						echo"</tr>";
					}
					
				}
			}
				echo"<tr>";
				echo"<td colspan=\"2\">Összesen:</td>";
				echo"<td class=\"text-right\">".huf($sumar)." Ft</td>";
				
				echo"</tr>";
		?>
	</tbody>
</table>
<?
$_SESSION['planetsys']['exportdata'] = $output ;
mysql_close($connid);
?>
