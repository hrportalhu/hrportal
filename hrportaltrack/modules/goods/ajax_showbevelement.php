<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors','Off');
header("Cache-control: private");
header('Content-Type: text/plain;charset=utf-8');
include_once dirname(__FILE__).("/../../inc/sys.conf");
include_once dirname(__FILE__).("/../../inc/_functions.php");


$b=db_all("select * from alapanyag where agf_id='".$_REQUEST['elemid']."' and status=1 order by id asc");
$stores=db_all("select id, name from stores where status=1 order by name");
$lezart=db_one("select lezarva from alapanyag_fejlec where id='".$_REQUEST['elemid']."'");
?>

<div class="table-responsive">
		<table class="table table-vcenter table-striped">
			<thead>
				<tr>
					
					<th>Termék név</th>
					<th>Bevételezett mennyiség</th>
					<th>Aktuális mennyiség</th>
					<th>Egységár</th>
					<th>Elhelyezés</th>
					<th>Egyéb infók</th>
					
					<th style="width: 150px;" class="text-center">Művelet</th>
				</tr>
			</thead>
			<tbody>
				<?php
					//$orders=db_all("select * from orders where status='1' order by order_date desc");
					for($i=0;$i<count($b);$i++){
						$alap=db_row("select * from alapag_tipus where id='".$b[$i]['agt_id']."'");
						$bev=db_row("select * from alapag_events where ag_id='".$b[$i]['id']."' and event=1 order by id asc");
						$rakt=db_row("select name from stores where id='".$b[$i]['store_id']."'");
						$mertek=idchange("mertekegyseg","name",$alap['measure']);
						echo"<tr>";
						echo"<td><a href=\"/modules/goods/materials/".$b[$i]['agt_id']."\">".$alap['name']."</a></td>";
						echo"<td>".round($bev['amount'],3)." ".$mertek."</td>";
						echo"<td>".round($b[$i]['units'],3)." ".$mertek."</td>";
						echo"<td>".round($b[$i]['price'],2)." ft</td>";
						echo"<td><a href=\"/modules/goods/stores/".$b[$i]['store_id']."\">".$rakt['name']."</a></td>";
						echo"<td>MMI:".$b[$i]['mmi'].", LOT: ".$b[$i]['lot'].", Hőmérséklet: ".$b[$i]['temperature'].", </td>";
						echo"<td class=\"text-center\">";
						if($lezart==0){
							echo"<div class=\"btn-group btn-group-xs\"><a href=\"javascript:delbevetelemet('".$b[$i]['id']."','".$_REQUEST['elemid']."');\" data-toggle=\"tooltip\" title=\"Delete\" class=\"btn btn-danger\"><i class=\"fa fa-times\"></i></a></div>";
							echo"<div class=\"btn-group btn-group-xs\"><a href=\"#modal-regular_".$b[$i]['id']."\" class=\"btn btn-info\" data-toggle=\"modal\"><i class=\"fa fa-floppy-o\"></i></a><br></div>";
							?>
							 <!-- Regular Modal -->
									<div id="modal-regular_<?=$b[$i]['id'];?>" class="modal" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
													<h3 class="modal-title"><?=$alap['name'];?></h3>
												</div>
												<div class="modal-body">
										<div class="row block-content" style="padding:10px;">
										<div class="col-sm-12">
											<div class="block">
												
												<div class="form-group" >
													<label class="col-md-6 control-label" for="mennyiseg_<?=$b[$i]['id'];?>">Mennyiség</label>
													<div class="col-md-6">
														<input type="text" id="mennyiseg_<?=$b[$i]['id'];?>" name="mennyiseg_<?=$b[$i]['id'];?>" class="form-control"  value="<?=$bev['amount'];?>" />
													</div>
												</div>
												<div class="form-group" >
													<label class="col-md-6 control-label" for="nettoar_<?=$b[$i]['id'];?>">Nettó Ár</label>
													<div class="col-md-6">
														<input type="text" id="nettoar_<?=$b[$i]['id'];?>" name="nettoar" class="form-control" value="<?=$b[$i]['price'];?>" />
													</div>
												</div>
												<div class="form-group" >
													<label class="col-md-6 control-label" for="homerseklet_<?=$b[$i]['id'];?>">Hő</label>
													<div class="col-md-6">
														<input type="text" id="homerseklet_<?=$b[$i]['id'];?>" name="homerseklet_<?=$b[$i]['id'];?>" class="form-control" value="<?=$b[$i]['amount'];?>" />
													</div>
												</div>
											
												<div class="form-group" >
													<label class="col-md-6 control-label" for="lot_<?=$b[$i]['id'];?>">LOT Kód</label>
													<div class="col-md-6">
														<input type="text" id="lot_<?=$b[$i]['id'];?>" name="lot_<?=$b[$i]['id'];?>" class="form-control" value="<?=$b[$i]['lot'];?>" />
													</div>
												</div>
												<div class="form-group" >
													<label class="col-md-6 control-label" for="mmi">MMI</label>
													<div class="col-md-6">
														<input type="text" id="mmi_<?=$b[$i]['id'];?>" name="mmi_<?=$b[$i]['id'];?>" class="form-control input-datepicker" value="<?=$b[$i]['mmi'];?>" />
													</div>
												</div>
											
												<div class="form-group" >
													<label class="col-md-6 control-label" for="nnsz">NNSZ</label>
													<div class="col-md-6">
														<input type="text" id="nnsz_<?=$b[$i]['id'];?>" name="nnsz_<?=$b[$i]['id'];?>" class="form-control" value="<?=$b[$i]['nnsz'];?>" />
													</div>
												</div>
												<div class="form-group" >
													<label class="col-md-6 control-label" for="mystore_<?=$b[$i]['id'];?>">Raktár</label>
													<div class="col-md-6">
													<select id="mystore_<?=$b[$i]['id'];?>" name="mystore_<?=$b[$i]['id'];?>" class="form-control" >
															<option value="0">Kérlek válassz</option>
															<?
																for($a=0;$a<count($stores);$a++){
																	$sel="";if($bev['store_id']==$stores[$a]['id']){$sel=" selected ";}
																	echo"<option value=\"".$stores[$a]['id']."\" ".$sel.">".$stores[$a]['name']."</option>";	
																}
															?>
														</select>
													</div>
												</div>
												<div style="clear:both;"></div>
											</div>
										</div>
										<div style="clear:both;"></div>	
									</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Bezár</button>
													<?
														echo"<button type=\"button\" class=\"btn btn-sm btn-primary\" onclick=\"savemodbevetelezes('".$b[$i]['id']."');\">Szerkeszt</button>";
													?>
												</div>
											</div>
										</div>
									</div>
									<!-- END Regular Modal -->
							<?
						}
						echo"</td>";
						echo"</tr>";
					}
				?>
			</tbody>
		</table>
	</div>
</div>
<?

mysql_close($connid);
?>
