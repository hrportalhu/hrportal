<?php
    /*
    $nullak=db_all("select * from alapagleltar where found_amount='0.000' group by ag_id");
    for($a=0;$a<count($nullak);$a++){
		echo $nullak[$a]['ag_id']."<br />";
		db_execute("update alapanyag set consumed='2015-02-26 08:15:12' where id='".$nullak[$a]['ag_id']."'");
	}
	*/ 
?>
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Alapanyag leltár
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/goods/alapagleltar">Alapanyag leltár</a></li>
				<li><a href="/new">Új leltár készítése</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új termék felvitele</strong></h2>
				</div>
			<?
			
				$stores=db_all("select id,name from stores where status=1 order by name");
				$posts=db_all("select id,name from gw_posts where status=1 order by name");
				
				echo "<form id=\"newalapagleltar\" name=\"newalapagleltar\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"store_id\">Raktár:</label>
							<div class=\"col-md-9\">
								<select id=\"store_id\" name=\"store_id\" class=\"form-control\" placeholder=\"Tárolási helye\"   >
									<option value=\"0\">Kérlek válassz</option>";
									for($i=0;$i<count($stores);$i++){		
										echo "<option value=\"".$stores[$i]['id']."\" >".$stores[$i]['name']."</option>";	
									}
								echo "</select>
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"post_id\">Munkahely:</label>
							<div class=\"col-md-9\">
								<select id=\"post_id\" name=\"post_id\" class=\"form-control\" placeholder=\"Munkahelyének neve\"   >
									<option value=\"0\">Kérlek válassz</option>";
									for($i=0;$i<count($posts);$i++){		
										echo "<option value=\"".$posts[$i]['id']."\" >".$posts[$i]['name']."</option>";	
									}
								echo "</select>
							</div>
						</div>";
						
						echo "<div class=\"form-group\" >
							<label class=\"col-md-3 control-label\" for=\"status\">Státusz:</label>
							<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span></label>
						</div>";
						
						echo "<div style=\"clear:both;height:10px;\"></div>";
						
						echo "<div class=\"form-group form-actions\">
							<div class=\"col-md-9 col-md-offset-3\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewalapagleltar();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>";
						
				echo "</div>
				</form>";
			?>
			</div>
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/goods/js/inc_alapagleltar.js"></script>
			<?php include 'inc/template_end.php'; ?>
<?
		}
		else{
			
			//echo "select * from functions where id='".$uritags[2]."'";
			$myalapagleltar=db_row("select * from alapagleltar_fejlec where id='".$uritags[3]."'");
			$mystores=db_row("select * from stores where id='".$uritags[3]."'");
			if($myalapagleltar['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="modules/goods/alapagleltar">Alapanyag leltár</a></li>
				<li><a href="/<?=$URI;?>"><?=$$R['store_id'];?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->
					<div class="block-title">
						<h2><strong><?=$mystores['name'];?></strong> leltár oldala</h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
								if(priv("alapagleltar","view")){echo"<li class=\"active\" onclick=\"javascript:getleltar(".$myalapagleltar['id'].",'tab-1');\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							?>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">
						<div class="tab-pane active" id="tab-1"></div>
					</div>
					<!-- END Tabs Content -->
					
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/goods/js/inc_alapagleltar.js"></script>

			</script>
			<script>$(function(){ 
				getleltar('<?=$myalapagleltar['id'];?>','tab-1');
			});</script>	

			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
		//print_r($_SESSION['planetsys'])
?>    
<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Alapanyag leltár</a></li>
			</ul>
			<!-- END Datatables Header -->

			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Alapanyag leltár</strong> lista</h2>
				</div>

				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/goods/js/inc_alapagleltar.js"></script>
		<script>$(function(){ alapagleltarDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>

