<div id="page-content">
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Alapanyag listák
            </h1>
        </div>
    </div>
	<ul class="breadcrumb breadcrumb-top">
		<li><a href="/">Rendszer</a></li>
		<li><a href="/<?=$URI;?>">Alapanyag </a></li>
	</ul>
	<div class="block full">
		<div class="block-title">
			<h2><strong>Elérhető</strong> alapanyag lekérdezések</h2>
			<ul class="nav nav-tabs" data-toggle="tabs">
				<?
					if(priv("producttype","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"Alapanyag készlet és fogyás\" onclick=\"javascript:showakflist();\"><i class=\"gi gi-eye_open\"></i></a></li>";}
					if(priv("producttype","view")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"Fogyóban levő alapanyagok\" onclick=\"javascript:showfalist();\"><i class=\"gi gi-thumbs_down\"></i></a></li>";}			
					if(priv("producttype","view")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"Túlrendelt alapanyagok\"><i class=\"gi gi-thumbs_up\" onclick=\"javascript:showtalist();\"></i></a></li>";}			
					if(priv("producttype","view")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"Közeli lejáratú alapanyagok\" onclick=\"javascript:showalertlist();\"><i class=\"gi gi-skull\"></i></a></li>";}			
					if(priv("materials","story")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"Alapanyag kivezetés\" onclick=\"javascript:shownotconsumed();\"><i class=\"gi gi-send\"></i></a></li>";}			
					if(priv("producttype","view")){echo"<li><a href=\"#tab-6\" data-toggle=\"tooltip\" title=\"Alapanyag minőségi dokumentumok\" onclick=\"javascript:showalapanyagdoclist();\"><i class=\"fa fa-list-ol\"></i></a></li>";}			
					if(priv("producttype","view")){echo"<li><a href=\"#tab-7\" data-toggle=\"tooltip\" title=\"Csomagoló anyagok minőségi dokumentumai\" onclick=\"javascript:showcsomagolodoclist();\"><i class=\"fa fa-list-ul\"></i></a></li>";}			
					if(priv("producttype","view")){echo"<li><a href=\"#tab-8\" data-toggle=\"tooltip\" title=\"Allergénlista\" onclick=\"javascript:showallergenlist();\"><i class=\"fa fa-th-list\"></i></a></li>";}			
					if(priv("producttype","view")){echo"<li><a href=\"#tab-9\" data-toggle=\"tooltip\" title=\"Súlyozott alapanyag lista\" onclick=\"javascript:showweightmaterials();\"><i class=\"fa fa-sort-amount-desc\"></i></a></li>";}			
					if(priv("producttype","view")){echo"<li><a href=\"#tab-10\" data-toggle=\"tooltip\" title=\"Napi készletlekérdezés\" onclick=\"javascript:showdailymaterials();\"><i class=\"fa fa-sun-o\"></i></a></li>";}			
					if(priv("materials","baseprice")){echo"<li><a href=\"#tab-11\" data-toggle=\"tooltip\" title=\"Alapanyag árazás\" onclick=\"javascript:showmaterialprices();\"><i class=\"gi gi-fishes\"></i></a></li>";}			
					if(priv("materials","view")){echo"<li><a href=\"#tab-12\" data-toggle=\"tooltip\" title=\"Alapanyag beszállító lista\" onclick=\"javascript:showmaterialpartnerlist();\"><i class=\"gi gi-cars\"></i></a></li>";}			
				?>	
			</ul>
							
		</div>
		<div class="tab-content">
			<div class="tab-pane active" id="tab-1">
				<div class="table-responsive">
					<table class="table table-vcenter table-striped">
						<thead>
							<tr>
								<th class="text-center">Alapanyag név</th>
								<th class="text-center">Aktuális készlet</th>
								<th class="text-center">Heti fogyás</th>
								<th class="text-center">Havi fogyás</th>
								<th class="text-center">Összeg</th>
								<th class="text-center">Forgási sebesség</th>
							</tr>
						</thead>
						<tbody id="alapanyaglist1"></tbody>
					</table>
				</div>
				<div style="clear:both;height:1px;"></div>
			</div>
			<div class="tab-pane" id="tab-2">
				<div class="table-responsive">
					<table class="table table-vcenter table-striped">
						<thead>
							<tr>
								<th class="text-center">Alapanyag név</th>
								<th class="text-center">Minimum készlet</th>
								<th class="text-center">Aktuális készlet</th>
								<th class="text-center">Heti fogyás</th>
								<th class="text-center">Forgási sebesség</th>
							</tr>
						</thead>
						<tbody id="alapanyaglist2"></tbody>
					</table>
				</div>
				<div style="clear:both;height:1px;"></div>
			</div>
			<div class="tab-pane" id="tab-3">
				<div class="table-responsive">
					<table class="table table-vcenter table-striped">
						<thead>
							<tr>
								<th class="text-center">Alapanyag név</th>
								<th class="text-center">Maximum készlet</th>
								<th class="text-center">Aktuális készlet</th>
								<th class="text-center">Heti fogyás</th>
								<th class="text-center">Forgási sebesség</th>
							</tr>
						</thead>
						<tbody id="alapanyaglist3"></tbody>
					</table>
				</div>
				<div style="clear:both;height:1px;"></div>
			</div>
			<div class="tab-pane" id="tab-4">
				<div class="table-responsive">
					<table class="table table-vcenter table-striped">
						<thead>
							<tr>
								<th class="text-center">Alapanyag név</th>
								<th class="text-center">Alapanyag tétel</th>
								<th class="text-center">MMI</th>
								<th class="text-center">Aktuális mennyiség</th>
								
							</tr>
						</thead>
						<tbody id="alapanyaglist4"></tbody>
					</table>
					
				</div>
				<div style="clear:both;height:1px;"></div>
			</div>
			<div class="tab-pane" id="tab-5">
				<div class="table-responsive">
					<table class="table table-vcenter table-striped">
						<thead>
							<tr>
								<th class="text-center">Alapanyag név</th>
								<th class="text-center">Alapanyag tétel</th>
								<th class="text-center">MMI</th>
								<th class="text-center">Aktuális mennyiség</th>
							</tr>
						</thead>
						<tbody id="alapanyaglist5"></tbody>
					</table>
					<input type='button' id='submitcontent' value='Mindet kivezet' onclick="javascript:donotconsumed();" />
				</div>
				<div style="clear:both;height:1px;"></div>
			</div>
			<div class="tab-pane" id="tab-6">
				<div class="table-responsive">
					<table class="table table-vcenter table-striped">
						<thead>
							<tr>
								<th class="text-center">Alapanyag név</th>
								<th class="text-center">Specifikáció</th>
								<th class="text-center">GMO Nyilatkozat</th>
							</tr>
						</thead>
						<tbody id="alapanyaglist6"></tbody>
					</table>
				</div>
				<div style="clear:both;height:1px;"></div>
			</div>
			<div class="tab-pane" id="tab-7">
				<div class="table-responsive">
					<table class="table table-vcenter table-striped">
						<thead>
							<tr>
								<th class="text-center">Alapanyag név</th>
								<th class="text-center">Specifikáció</th>
								<th class="text-center">1935 számú rendelet...</th>
							</tr>
						</thead>
						<tbody id="alapanyaglist7"></tbody>
					</table>
				</div>
				<div style="clear:both;height:1px;"></div>
			</div>
			<div class="tab-pane" id="tab-8">
				<div class="table-responsive">
					<table class="table table-vcenter table-striped">
						<thead>
							<tr>
								<th class="text-center">Alapanyag név</th>
								<th class="text-center">Glutén</th>
								<th class="text-center">Szója</th>
								<th class="text-center">Tejtermék</th>
								<th class="text-center">Tojás</th>
								<th class="text-center">Kén-dioxid</th>
								<th class="text-center">Diófélék</th>
								<th class="text-center">Mogyorófélék</th>
								<th class="text-center">Szezámmag</th>
							</tr>
						</thead>
						<tbody id="alapanyaglist8"></tbody>
					</table>
				</div>
				<div style="clear:both;height:1px;"></div>
			</div>
			<div class="tab-pane" id="tab-9">
				<div class="table-responsive">
					<table class="table table-vcenter table-striped">
						<thead>
							<tr>
								<th class="text-center">Alapanyag név</th>
								<th class="text-center">Termékek száma</th>
								
							</tr>
						</thead>
						<tbody id="alapanyaglist9"></tbody>
					</table>
				</div>
				<div style="clear:both;height:1px;"></div>
			</div>
			<div class="tab-pane" id="tab-12">
				<div class="table-responsive">
					<table class="table table-vcenter table-striped">
						<thead>
							<tr>
								<th class="text-center">Alapanyag név</th>
								<th class="text-center">Beszállító</th>
								<th class="text-center">Kialkudott ár</th>
								<th class="text-center">Utolsó ár</th>
								
							</tr>
						</thead>
						<tbody id="alapanyaglist12"></tbody>
					</table>
				</div>
				<div style="clear:both;height:1px;"></div>
			</div>
			<div class="tab-pane" id="tab-10">
				<div class="block hidden-lt-ie9">
					<div class="block-title">
						<h2><strong>Szűrő</strong> </h2>
					</div>
					<?
				   echo"<div class=\"form-group\" >
							<div class=\"col-md-4\">
								<select id=\"post_id\" name=\"post_id\" class=\"form-control\"  onchange=\"javascript:productallergen();\" >";
									echo"<option value=\"1\" >Raktár</option>";
									echo"<option value=\"2\" selected>Üzem</option>";
								echo"</select>
							</div>";
					echo"</div>";
					echo"<div class=\"form-group\">
							<div class=\"col-md-4\">
								<input type=\"text\" id=\"myday\" name=\"myday\" class=\"form-control input-datepicker\" value=\"".date("Y-m-d",time())."\" />
							</div>
						</div>";

					echo"<div class=\"col-md-2\">
							<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"javascript:showdailymaterials();\">Szűr</button>
						</div>";
				   
						?>	
						<div style="clear:both;height:10px;"></div>
				</div>	
						

				
				<div class="table-responsive">
					<table class="table table-vcenter table-striped">
						<thead>
							<tr>
								<th class="text-center">Alapanyag név</th>
								<th class="text-center">Tétel</th>
								<th class="text-center">MMI</th>
								<th class="text-center">Mennyiség</th>
								<th class="text-center">Egység</th>
								<th class="text-center">Érték</th>
							</tr>
						</thead>
						<tbody id="alapanyaglist10"></tbody>
					</table>
				</div>
				<div style="clear:both;height:1px;"></div>
			</div>
			
			<div class="tab-pane" id="tab-11">
				<div class="block hidden-lt-ie9">
					<div class="block-title">
						<h2><strong>Szűrő</strong> </h2>
					</div>
					<?
					$alaps=db_all("select id, name from alapag_tipus where status=1 order by name asc");
				   echo"<div class=\"form-group\" >
							<div class=\"col-md-4\">
								<select id=\"myagt_id\" name=\"myagt_id\" class=\"form-control\"  onchange=\"javascript:showmaterialprices();\" >";
								echo"<option value=\"0\" selected>Kérlek válassz</option>";
									for($i=0;$i<count($alaps);$i++){
										echo"<option value=\"".$alaps[$i]['id']."\" >".$alaps[$i]['name']."</option>";
									}
								echo"</select>
							</div>";
					echo"</div>";
					echo"<div class=\"form-group\">
							<div class=\"col-md-4\">
								<input type=\"text\" id=\"myday\" name=\"myday\" class=\"form-control input-datepicker\" value=\"".date("Y-m-d",time())."\" />
							</div>
						</div>";

					echo"<div class=\"col-md-2\">
							<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"javascript:showmaterialprices();\">Szűr</button>
						</div>";
				   
						?>	
						<div style="clear:both;height:10px;"></div>
				</div>	
						

				
				<div class="table-responsive">
					<table class="table table-vcenter table-striped">
						<thead>
							<tr>
								<th class="text-center">Sorszám</th>
								<th class="text-center">Alapanyag név</th>
								<th class="text-center">Beszállító</th>
								<th class="text-center">Beszállítás ideje</th>
								<th class="text-center">Tétel</th>
								<th class="text-center">MMI</th>
								<th class="text-center">Me</th>
								<th class="text-center">Ár</th>
								<th class="text-center">Művelet</th>
							</tr>
						</thead>
						<tbody id="alapanyaglist11"></tbody>
					</table>
				</div>
				<div style="clear:both;height:1px;"></div>
			</div>
			
		</div>		
	</div>					
</div>
<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<script src="/modules/goods/js/inc_alapanyaglists.js"></script>
<script >
	showakflist();			
</script>
<?php include 'inc/template_end.php'; ?>
