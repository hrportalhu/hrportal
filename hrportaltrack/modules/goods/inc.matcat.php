<?php
$most=date('Y-m-d',time());

$screen=array(
	"elements"=>array(
		"id"=>array(
			"name"=>"id",	
			"textname"=>"Sorszám",	
			"type"=>"text50",	
			"type_param"=>"",	
			"help"=>"",	
			"defval"=>"",	
			"connected"=>0,	
			"con_table"=>"",	
			"con_table_shortname"=>"",	
			"con_table_fields"=>"",	
			"width"=>"",	
			"deflist"=>0,	
			"scrdb"=>0,	
			"scrcmd"=>1	
		),
		"name"=>array(
			"name"=>"name",	
			"textname"=>"Alapanyag kategória megnevezése",	
			"type"=>"text50",	
			"type_param"=>"",	
			"help"=>"",	
			"defval"=>"",	
			"connected"=>0,	
			"con_table"=>"",	
			"con_table_shortname"=>"",	
			"con_table_fields"=>"",	
			"width"=>"",	
			"deflist"=>1,	
			"scrdb"=>1,	
			"scrcmd"=>1	
		),
		"shortname"=>array(
			"name"=>"shortname",	
			"textname"=>"Alapanyag címkenév",	
			"type"=>"text50",	
			"type_param"=>"",	
			"help"=>"",	
			"defval"=>"",	
			"connected"=>0,	
			"con_table"=>"",	
			"con_table_shortname"=>"",	
			"con_table_fields"=>"",	
			"width"=>"",	
			"deflist"=>1,	
			"scrdb"=>1,	
			"scrcmd"=>1	
		),
		"status"=>array(
			"name"=>"status",	
			"textname"=>"Státusz",	
			"type"=>"status",	
			"type_param"=>"",	
			"help"=>"",	
			"defval"=>"",	
			"connected"=>0,	
			"con_table"=>"",	
			"con_table_shortname"=>"",	
			"con_table_fields"=>"",	
			"width"=>"",	
			"deflist"=>0,	
			"scrdb"=>1,	
			"scrcmd"=>1	
		)
	),
	"tableprefix"=>"",
	"modulname"=>"goods",
	"funcname"=>"matcat",
	"mylimit"=>"20",
	"mywidth"=>"",
	"scrtable"=>"alapag_kategoria",
	"scrtable_shortname"=>"apgk",
);

getlistoptions($screen);
$screen['screenlist']=$_SESSION['planetsys']['listoption']['screenlist'];
$screen['screendb']=$_SESSION['planetsys']['screendb'];

?>
