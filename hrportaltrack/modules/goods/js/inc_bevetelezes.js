/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */
var bevetelezesDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();
            

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/goods/bevetelezes/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"bDestroy": true,
				"aaSorting": [[ 0, "desc" ]],
				"iDisplayLength": 30,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/goods/ajax_bevetelezes_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					aoData.push({"name": "mpartner_id", "value":$("#mpartner_id").val() });
					
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });

            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveeditbevetelezes(elemid){
	var valid = f_c($('#editbevetelezes'));
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_bevetelezes_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editbevetelezes').serialize(),'');
	}
}

function savemodbevetelezes(elemid){
	
		f_xml(ServerName +'/modules/goods/ajax_bevetelezes_data.php','view=mod&id=' + elemid +'&modsubmit='+elemid+'&me='+$('#mennyiseg_'+elemid).val()+'&lot='+$('#lot_'+elemid).val()+'&mmi='+$('#mmi_'+elemid).val()+'&nnsz='+$('#nnsz_'+elemid).val()+'&nettoar='+$('#nettoar_'+elemid).val() +'&homerseklet='+$('homerseklet_'+elemid).val() ,'');
	
}

function closeeditbevetelezes(elemid){
	
		f_xml(ServerName +'/modules/goods/ajax_bevetelezes_data.php','view=edit&id=' + elemid +'&closesubmit='+elemid,'');
	
}

function savenewbevetelezes(){
	var valid = f_c($('#newbevetelezes'));
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_bevetelezes_data.php','newsubmit=1&' + $('#newbevetelezes').serialize(),'');
	}
}

function savebevcostcont(editid,elemid){
	f_xml(ServerName +'/modules/goods/ajax_bevetelezes_data.php','savebevcostcont=1&editid='+editid+'&elemid='+elemid+'&priceem='+$('#priceem_'+editid).val(),'');
}

function savebevpart(editid,elemid){
	f_xml(ServerName +'/modules/goods/ajax_bevetelezes_data.php','savebevpart=1&editid='+editid+'&elemid='+elemid+'&myspec='+$('#myspec_'+editid).val(),'');
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_bevetelezes_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}

function addbevelement(elemid,targetdiv){
	if($('#agt_id').val()!="" && $('#agt_myid').val()!="" && $('#mennyiseg').val()!="" &&  $('#mystore').val()!="0"){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_addbevelement.php',
		data:'newsubmit=1&elemid='+elemid+'&'+$('#newelementform').serialize(),
		success: function(data){
			if(data=="alert"){
				alert("FIGYELEM! A bevételezett alapanyag nem teljesíti a 2/3 - 1/3 szabályt.");	
			}	
			showbevelement(elemid,targetdiv);
			$('#agt_id').val('');
			$('#mennyiseg').val('');
			$('#nettoar').val('');
			$('#homerseklet').val('');
			$('#lot').val('');
		}});
	}
	else{
		alert("Üres tételt nem lehet betölteni!");
	}
}

function delbevetelemet(elemid,headerid){
	if(confirm("Biztos törlöd ezt a tételt a bevételezésből")){	
		$.ajax({
			type:'POST',
			url: ServerName +'/modules/goods/ajax_delbevetelemet.php',
			data:'delsubmit=1&elemid='+elemid,
			success: function(data){
				document.location='/modules/goods/bevetelezes/'+headerid;
			}});
		}
}

function checkorder(){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_checkorder.php',
		data:'partner_id='+$("#partner_id").val(),
		success: function(data){
			$('#myorderplace').html(data);
		}});
}
function showbevelement(elemid,targetdiv){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_showbevelement.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#'+targetdiv).html(data);
		}});
}

function getspecelem(){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_getspecelem.php',
		data:'elemid='+ $("#agt_id").val(),
		success: function(data){
			$('#myspec').html(data);
		}});	
}

function bevpart(elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_bevpart.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#bevpartdiv').html(data);
		}});	
}



function bevcostcont(elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_bevcostcont.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#bevcostcontdiv').html(data);
		}});	
}




$("#agt_id").autocomplete({
	source: ServerName+"/modules/goods/ajax_alapanyag_term.php",
	minLength: 0,select: 
	function(event, ui) {
		$("#agt_id").val(ui.item.value);
		$("#agt_myid").val(ui.item.id);
	}
});
