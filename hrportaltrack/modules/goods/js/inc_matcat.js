/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */
var MatCatDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/goods/matcat/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"aaSorting": [[ 0, "desc" ]],
				"iDisplayLength": 30,
				"bDestroy": true,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/goods/ajax_matcat_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					aoData.push({"name": "filtergroup_id", "value":$("#filtergroup_id").val() });
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });
            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveeditmatcat(elemid){
	var valid = f_c($('#editfiling'));
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_matcat_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editmatcat').serialize(),'');
	}
}

function savenewmatcat(){
	var valid = f_c($('#newfunc'));
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_matcat_data.php','newsubmit=1&' + $('#newmatcat').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_matcat_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}
