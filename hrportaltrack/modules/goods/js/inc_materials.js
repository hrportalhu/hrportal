/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */	
var MaterialsDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/goods/materials/'+id;
		} );
		var catArray="";
		if($("#catsum").val()!= undefined){
			catArray = $("#catsum").val().split('-'); 
		}
            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"bDestroy": true,
				"aaSorting": [[ 1, "asc" ]],
				"iDisplayLength": 30,
				"bDestroy": true,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/goods/ajax_materials_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					aoData.push({"name": "dealer_id", "value":$("#dealer_id").val() });
					aoData.push({"name": "manufact_id", "value":$("#manufact_id").val() });
					aoData.push({"name": "active", "value":$("#active").val() });
					aoData.push({"name": "catsum", "value": $("#catsum").val()});
					if(catArray.length>0){
						for(i=0;i<catArray.length;i++){
							if($("#cat_"+catArray[i]).prop('checked')){
								aoData.push({"name": $("#cat_"+catArray[i]).val(), "value":"1"});
							}
							else{
								aoData.push({"name": $("#cat_"+catArray[i]).val(), "value":"0"});
							}
						}
					}
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });

            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveeditmaterials(elemid){
	var valid = f_c($('#editmaterials'));
	var osszetetelText = CKEDITOR.instances.osszetetel.getData();
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_materials_data.php','view=edit&osszeteteltxt='+osszetetelText+'&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editmaterials').serialize(),'');
	}
}

function savenewmaterials(){
	var valid = f_c($('#newmaterials'));
	var osszetetelText = CKEDITOR.instances.osszetetel.getData();
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_materials_data.php','newsubmit=1&osszeteteltxt='+osszetetelText+'&' + $('#newmaterials').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_materials_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}

function addbeszelement(elemid,targetdiv){
	if($('#beszallito_id').val()!="0" ){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_addbeszelement.php',
		data:'newsubmit=1&elemid='+elemid+'&'+$('#newelementformbesz').serialize(),
		success: function(data){
			showbeszelement(elemid,targetdiv);
		}});
	}
	else{
		alert("Üres tételt nem lehet betölteni!");
	}
}

function showbeszelement(elemid,targetdiv){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_showbeszelement.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#'+targetdiv).html(data);
		}});
}

function savebaseprice(elemid){
	var valid = f_c($('#editbaseprice'));
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_materials_data.php','editbaseprice='+elemid+'&' + $('#editbaseprice').serialize(),'');
	}
}

function savedealprice(elemid){
	var valid = f_c($('#editdealprice'));
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_materials_data.php','editdealprice='+elemid+'&' + $('#editdealprice').serialize(),'');
	}
}

function saveeditstoreform(elemid){
	var valid = f_c($('#editbaseprice'));
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_materials_data.php','saveeditstoreform='+elemid+'&' + $('#editstoreform').serialize(),'');
	}
}

function saveconsumform(elemid){
	var valid = f_c($('#editbaseprice'));
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_materials_data.php','saveconsumform='+elemid+'&' + $('#editstoreform').serialize(),'');
	}
}
function showalapagstory(elemid,targetdiv){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_showalapagstory.php',
		data:'elemid='+elemid+'&event_id='+$('#event_id').val()+'&limit='+$('#limit').val()+'&matstdate='+$('#matstdate').val()+'&matendate='+$('#matendate').val(),
		success: function(data){
			$('#'+targetdiv).html(data);
		}});
}

function delbeszelement(delid,elemid){
	if(confirm("Biztos törlöd ezt a beszállítót?")){	
		$.ajax({
			type:'POST',
			url: ServerName +'/modules/goods/ajax_delbeszelemet.php',
			data:'delsubmit=1&elemid='+elemid+'&delid='+delid,
			success: function(data){
				document.location='/modules/goods/materials/'+elemid;
			}});
		}
}

function setdefbeszelement(setid,elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_setdefbeszelement.php',
		data:'setdefbesz=1&elemid='+elemid+'&setid='+setid,
		success: function(data){
			document.location='/modules/goods/materials/'+elemid;
		}});
}
var retid;
function retetel(elemid,retid){
	if(confirm("Biztos visszakapcsolod ezt a tételt?")){	
		$.ajax({
			type:'POST',
			url: ServerName +'/modules/goods/ajax_retetel.php',
			data:'retetel=1&retid='+retid,
			success: function(data){
				document.location='/modules/goods/materials/'+elemid;
			}});
		}
}


function setalergen(elemid,retid){
	f_xml(ServerName +'/modules/goods/ajax_materials_data.php','setalergen='+elemid+'&alergen='+retid,'');
}
