/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */

var materialsorderDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/goods/materialsorder/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"aaSorting": [[ 0, "desc" ]],
				"iDisplayLength": 30,
				"bDestroy": true,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/goods/ajax_materialsorder_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					aoData.push({"name": "name", "value":$("#name").val() });
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });
            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveeditmaterialsorder(elemid){
	var valid = f_c($('#editmaterialsorder'));
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_materialsorder_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editmaterialsorder').serialize(),'');
	}
}

function savetavozottmaterialsorder(elemid){
	
		f_xml(ServerName +'/modules/goods/ajax_materialsorder_data.php','view=tav&id=' + elemid +'&tavsubmit='+elemid,'');

}

function neweditorderadat(edit_id,elemid){
	
		f_xml(ServerName +'/modules/goods/ajax_materialsorder_data.php','neweditorderadat=1&edit_id='+edit_id+'&elemid='+elemid+'&me='+$('#me_'+edit_id).val()+'&mee='+$('#mee_'+edit_id).val(),'');
	
}


function testordermail(partner_id,order_id){
	
		f_xml(ServerName +'/modules/goods/ajax_materialsorder_data.php','testordermail=1&partner_id='+partner_id+'&order_id='+order_id,'');
	
}
function sendordermail(partner_id,order_id){
		if(confirm("Biztos kiküldöd a levelet?")){
			f_xml(ServerName +'/modules/goods/ajax_materialsorder_data.php','sendordermail=1&partner_id='+partner_id+'&order_id='+order_id,'');
		}
}

function closematerialsorder(order_id){
	
		f_xml(ServerName +'/modules/goods/ajax_materialsorder_data.php','closematerialsorder=1&order_id='+order_id,'');
	
}
function chorderpartner(elemid,order_id){
	
		f_xml(ServerName +'/modules/goods/ajax_materialsorder_data.php','chorderpartner=1&order_id='+order_id+'&agt_id='+elemid+'&newpartner='+$('#ospec_'+elemid).val(),'');
	
}

function savenewmaterialsorder(){
	var valid = f_c($('#newmaterialsorder'));
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_materialsorder_data.php','newsubmit=1&' + $('#newmaterialsorder').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_materialsorder_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}
function showordertable(elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_showordertable.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#ordercont').html(data);
			
		}
	});	
}
function showorderaprove(elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_showorderaprove.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#orderaprovecont').html(data);
			
		}
	});	
}

function aproveorderadat(edit_id,elemid){
	
		f_xml(ServerName +'/modules/goods/ajax_materialsorder_data.php','aproveorderadat=1&edit_id='+edit_id+'&elemid='+elemid+'&me='+$('#ome_'+edit_id).val()+'&mee='+$('#omee_'+edit_id).val()+'&spec='+$('#ospec_'+edit_id).val(),'');
	
}
function delorderadat(edit_id,elemid){
	
		f_xml(ServerName +'/modules/goods/ajax_materialsorder_data.php','delorderadat=1&edit_id='+edit_id+'&elemid='+elemid,'');
	
}
function aproveallorderadat(partner_id,elemid){
	
		f_xml(ServerName +'/modules/goods/ajax_materialsorder_data.php','aproveallorderadat=1&partner_id='+partner_id+'&elemid='+elemid,'');
	
}

function saveorderdata(elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_saveorderdata.php',
		data:'elemid='+elemid
		+'&agt_id='+$('#agt_id').val()
		+'&agt_name='+$('#agt_name').val()
		+'&me='+$('#me').val()
		+'&mee='+$('#mee').val(),
		success: function(data){
			showordertable(elemid);
			
		}
	});	
}
function saveeditorderdata(edit_id,elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_saveeditorderdata.php',
		data:'elemid='+elemid
		+'&edit_id='+edit_id
		+'&me='+$('#me').val()
		+'&mee='+$('#mee').val(),
		success: function(data){
			showordertable(elemid);
			
		}
	});	
}

function delorderadat_old(edit_id,elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_delorderadat.php',
		data:'elemid='+elemid
		+'&edit_id='+edit_id,
		success: function(data){
			showordertable(elemid);
			
		}
	});	
}
function editorderadat(editid,elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_editorderadat.php',
		data:'elemid='+elemid
			+'&editid='+editid,
		success: function(data){
			$('#orderdataelem').html(data);
		}
	});	
}


$("#agt_name").autocomplete({
	source: ServerName+"/modules/goods/ajax_alapanyag_term.php",
	minLength: 0,select: 
	function(event, ui) {
		$("#agt_name").val(ui.item.value);
		$("#agt_id").val(ui.item.id);
	}
});
