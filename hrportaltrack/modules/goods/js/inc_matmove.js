/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */



$("#newitem").autocomplete({
	 source: ServerName+"/modules/goods/ajax_goods_issue_term.php",
	 minLength: 0,
	 select: function(event, ui) {
			$("#newitem").val(ui.item.value);
			$("#newitem_id").val(ui.item.id);
			getkiadlot(ui.item.id);
		}
});

function getkiadlot(elemid){
		$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_goods_issue_getkiadlot.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#tetel').html(data);
		}});
	
}

function addmatmovelement(){
	if($('#newitem').val()!='' && $('#newitem_id').val()!=''){
		if($('#store2').val()!='0'){
	
			var myni=$('#newitem').val().match(/([0-9]*)\)$/);

			f_xml(ServerName +'/modules/goods/ajax_goods_addmatmovelement.php','view=edit&newsubmit=1&' +
				'&newitem=' + myni[1] +
				'&amount=' + $('#amount').val() +
				'&store2=' + $('#store2').val() +
				'&tetel=' + $('#tetel').val() +
				'&rdate=' + $('#rdate').val(),
				'newitemlist');
		}
		else{
			alert("Tudom, hogy nincs kedved, de ki kell választani, hogy hova mozgatod a tételt!");
		}	
	}
		
		$('#newitem').val('');
		$('#amount').val('');
		$('#tetel').html('<option value="0">Kérlek válassz</option>');
}
