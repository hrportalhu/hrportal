/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */
var ProductTypeDatatables = function() {

    return {
        init: function() {
			//getstx();
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/goods/producttype/'+id;
		} );
		var catArray="";
		if($("#catsum").val()!= undefined){
			catArray = $("#catsum").val().split('-'); 
		}
		
            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"aaSorting": [[ 1, "asc" ]],
				"iDisplayLength": -1,
				"bDestroy": true,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/goods/ajax_producttype_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					aoData.push({"name": "catsum", "value": $("#catsum").val()});
					aoData.push({"name": "active", "value":$("#active").val() });
					aoData.push({"name": "efproduct", "value":$("#efproduct").val() });
					if(catArray.length>0){
						for(i=0;i<catArray.length;i++){
							if($("#cat_"+catArray[i]).prop('checked')){
								aoData.push({"name": $("#cat_"+catArray[i]).val(), "value":"1"});
							}
							else{
								aoData.push({"name": $("#cat_"+catArray[i]).val(), "value":"0"});
							}
						}
					}
					$.getJSON( sSource, aoData, function (json) { fnCallback(json);} );
				}
            });
			$('.dataTables_filter input').focus();


            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
            $('#modal-regular-filter').modal('hide');
            
            
            
        }
       
    };
}();

function saveeditproducttypes(elemid){
	
	var osszetetelText = CKEDITOR.instances.osszetetel.getData();
	var adalekanyagText = CKEDITOR.instances.adalekanyag.getData();
	
	var minosegText = CKEDITOR.instances.minoseg.getData();
	var eleljarasText = CKEDITOR.instances.eleljaras.getData();
	
		f_xml(ServerName +'/modules/goods/ajax_producttype_data.php','view=edit&osszeteteltxt='+osszetetelText+'&adalekanyagtxt='+adalekanyagText+'&minosegtxt='+minosegText+'&eleljarastxt='+eleljarasText+'&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editproducttype').serialize(),'');
	
}

function savenewproducttypes(){
	var valid = f_c($('#newproducttype'));
	var osszetetelText = CKEDITOR.instances.osszetetel.getData();
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_producttype_data.php','newsubmit=1&osszeteteltxt='+osszetetelText+'&' + $('#newproducttype').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_producttype_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}

function showplusrow(){
	$('#contentrow_' + ind++).show();
}

var aclist;
$(function(){  
	
	$('.contentfield').autocomplete({
		source: aclist
	});
	$("#newprodd").focus();
	$("#startdate").datepicker();
	$("#enddate").datepicker();
	
	$.datepicker.regional['hu'] = {
		closeText: 'bezárás',
		prevText: '&laquo;&nbsp;vissza',
		nextText: 'előre&nbsp;&raquo;',
		currentText: 'ma',
		monthNames: ['Január', 'Február', 'Március', 'Április', 'Május', 'Június',
		'Július', 'Augusztus', 'Szeptember', 'Október', 'November', 'December'],
		monthNamesShort: ['Jan', 'Feb', 'Már', 'Ápr', 'Máj', 'Jún',
		'Júl', 'Aug', 'Szep', 'Okt', 'Nov', 'Dec'],
		dayNames: ['Vasárnap', 'Hétfö', 'Kedd', 'Szerda', 'Csütörtök', 'Péntek', 'Szombat'],
		dayNamesShort: ['Vas', 'Hét', 'Ked', 'Sze', 'Csü', 'Pén', 'Szo'],
		dayNamesMin: ['V', 'H', 'K', 'Sze', 'Cs', 'P', 'Szo'],
		weekHeader: 'Hé',
		dateFormat: 'yy-mm-dd',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};								
});


function saveinbounddata(elemid){
	f_xml(ServerName +'/modules/goods/ajax_producttype_contents.php','elemid=' + elemid +'&submit=1&agt_id='+$('#agt_id').val()+'&me='+$('#me').val()+'&post_id='+$('#me').val(),'contentdata');
}


function delelemtrow(elemid,t,id){
		 f_xml(ServerName +'/modules/goods/ajax_producttype_contents.php','elemid=' + elemid + '&del='+t+'_'+id+'','contentdata');	
}
var mrow;
function saveelemrow(mrow,elemid,t,id){
	f_xml(ServerName +'/modules/goods/ajax_producttype_contents.php','elemid=' + elemid + '&saveelemrow='+t+'&id='+id+'&mrow='+mrow+'&post_id='+$('#posta_'+ mrow).val()+'&nemenny='+$('#me_'+mrow).val(),'');	
}





function refproducttypes(){
	ProuctTypeDatatables.init();
}

function savetypedata(elemid){
	f_xml(ServerName +'/modules/goods/ajax_producttype_contents.php','elemid=' + elemid +'&submit=1&' + $('#pttcont').serialize(),'contentdata');
}

function delelemtrow(elemid,t,id){
		 f_xml(ServerName +'/modules/goods/ajax_producttype_contents.php','elemid=' + elemid + '&del='+t+'_'+id+'','contentdata');	
}

function refproducttypes(){
	ProuctTypeDatatables.init();
}

function showpricetable(elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_showpricetable.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#ptcont').html(data);
			
		}
	});	
}
function showdarabaru(elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_showdarabaru.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#dacont').html(data);
			
		}
	});	
}
function showszorzok(elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_showszorzok.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#szcont').html(data);
			
		}
	});	
}

function showmerttable(elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_showmerttable.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#ptmert').html(data);
			
		}
	});	
}
function showinboundcont(elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_showinboundcont.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#inboundcont').html(data);
			
		}
	});	
}

function savenewpricedata(elemid){
	
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_savenewpricedata.php',
		data:'elemid='+elemid
			+'&newprice='+$('#newprice').val()
			+'&akcionewprice='+$('#akcionewprice').val()
			+'&startdate='+$('#startdate').val()
			+'&enddate='+$('#enddate').val()
			+'&afa_vat='+$('#afa_vat').val()
			+'&brutto='+$('#brutto').val()
			+'&bruttoakcios='+$('#bruttoakcios').val()
		,
		success: function(data){
			showpricetable(elemid);
		}
	});	
}

function savenewmertdata(elemid){
	
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_savenewmertdata.php',
		data:'elemid='+elemid
			+'&mesure1='+$('#mesure1').val()
			+'&mesure2='+$('#mesure2').val()
			+'&unit1_id='+$('#unit1_id').val()
			+'&unit2_id='+$('#unit2_id').val()
			+'&units_type='+$('#units_type').val()
			
		,
		success: function(data){
			showmerttable(elemid);
		}
	});	
}
var rowid;
function saveeditelemrow(rowid,elemid){
	
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_saveeditelemrow.php',
		data:'elemid='+rowid
			+'&mesure1='+$('#'+rowid+'_mesure1').val()
			+'&mesure2='+$('#'+rowid+'_mesure2').val()
			+'&unit1_id='+$('#'+rowid+'_unit1_id').val()
			+'&unit2_id='+$('#'+rowid+'_unit2_id').val()
			+'&units_type='+$('#'+rowid+'_units_type').val()
			
		,
		success: function(data){
			showmerttable(elemid);
		}
	});	
}

function savenewdarabdata(elemid){
	
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_savenewdarabdata.php',
		data:'elemid='+elemid
			+'&agt='+$('#agt').val()
			+'&newdarab='+$('#newdarab').val()
		,
		success: function(data){
			showdarabaru(elemid);
		}
	});	
}

function savenewszorzodata(elemid){
	
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_savenewszorzodata.php',
		data:'elemid='+elemid
			+'&szor='+$('#szor').val()
			+'&name='+$('#newszorzoname').val()
			+'&szorzo='+$('#newszorzo').val()
		,
		success: function(data){
			showszorzok(elemid);
		}
	});	
}

function delmertdata(t,elemid){
		$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_delmertdata.php',
		data:'elemid='+elemid
			+'&delid='+t
		,
		success: function(data){
			showmerttable(elemid);
		}
	});	
}

function deledarabarurow(t,elemid){
		$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_deledarabarurow.php',
		data:'elemid='+elemid
			+'&delid='+t
		,
		success: function(data){
			showdarabaru(elemid);
		}
	});	
}

function deleszorzorow(t,elemid){
		$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_deleszorzorow.php',
		data:'elemid='+elemid
			+'&delid='+t
		,
		success: function(data){
			showszorzok(elemid);
		}
	});	
}

function reorg(elemid){
	if(confirm("Biztos újragenerálod az összetételi táblát?")){
		
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_reorg.php',
		data:'elemid='+elemid,
		success: function(data){
			document.location='/modules/goods/producttype/'+elemid+'/tab-2/tab-a5';
		}
	});	
}
}	


function getstx(){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_getstx.php',
		data:'elemid=1',
		success: function(data){
			$('#stx').html(data);
			
		}
	});	
}


$("#newprodd").autocomplete({
	source: ServerName+"/modules/goods/ajax_newprodd_term.php",
	minLength: 0,
	select: function(event, ui) {
		document.location='/modules/goods/producttype/'+ui.item.id;
		//$("#newprodd_id").val(ui.item.value);
	}
});
