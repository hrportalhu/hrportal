/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */
	
var storesDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/goods/stores/'+id;
		} );
		
            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"aaSorting": [[ 0, "desc" ]],
				"iDisplayLength": 10,
				"bDestroy": true,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/goods/ajax_stores_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });



            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveeditstores(elemid){
	var valid = f_c($('#editstores'));
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_stores_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editstores').serialize(),'');
	}
}

function savenewstores(){
	var valid = f_c($('#newstores'));
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_stores_data.php','newsubmit=1&' + $('#newstores').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_stores_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}

function showallraktar(){
	$('#allraktar').html('<center><h1><i class="fa fa-sun-o fa-spin"></i>Betöltés</h1></center>');
	var stoArray="";
	if($("#stosum").val()!= undefined){
		stoArray = $("#stosum").val().split('-'); 
	}
	var myurl="";
	if(stoArray.length>0){
		for(i=0;i<stoArray.length;i++){
			if($("#sto_"+stoArray[i]).prop('checked')){
				
				myurl=myurl+'&'+$("#sto_"+stoArray[i]).val()+'=1';
			}
			else{
				myurl=myurl+'&'+$("#sto_"+stoArray[i]).val()+'=0';
			}
		}
	}
	
	var teteles;
	if($("#teteles").prop('checked')){teteles=1;}else{teteles=0;}
	
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_showallraktar.php',
		data:'elemid=1&stosum='+$("#stosum").val()+'&teteles='+teteles+'&startdate='+$("#startdate").val()+'&enddate='+$("#enddate").val()+myurl,
		success: function(data){
			$('#allraktar').html(data);
		}});
}

