/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */

var TermekleltarozoDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/goods/termekleltarozo/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"aaSorting": [[ 0, "desc" ]],
				"iDisplayLength": 30,
				"bDestroy": true,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/goods/ajax_termekleltarozo_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					aoData.push({"name": "name", "value":$("#name").val() });
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });
            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveedittermekleltarozo(elemid){
	var valid = f_c($('#edittermekleltarozo'));
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_termekleltarozo_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#edittermekleltarozo').serialize(),'');
	}
}

function savenewtermekleltarozo(){
	var valid = f_c($('#newtermekleltarozo'));
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_termekleltarozo_data.php','newsubmit=1&' + $('#newtermekleltarozo').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/goods/ajax_termekleltarozo_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}


function termletarok(elemid,optid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goods/ajax_termletarok.php',
		data:'elemid='+elemid+'&optid='+optid+'&unit='+$('#unit_'+optid).val()+'&found='+$('#found_'+optid).val(),
		success: function(data){
			
			$('#ret_'+optid).html(data);
			$('#let_'+optid).css("background-color", "yellow");
			
		}
	});	
}
