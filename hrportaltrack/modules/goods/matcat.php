<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Alapanyag kategória kezelő
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/goods/matcat">Alapanyag kategória kezelő</a></li>
				<li><a href="/new">Új alapanyag kategória felvitele</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új alapanyag kategória felvitele</strong></h2>
				</div>
			<?
				echo "<form id=\"newmatcat\" name=\"newmatcat\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";

						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"name\">Új alapanyag kategória:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" placeholder=\"Új alapanyag kategória elnevezése\" />																		
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"shortname\">Új alpanyag címkenév:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"shortname\" name=\"shortname\" class=\"form-control\" placeholder=\"Új alpanyag címkenév:\" />																		
							</div>
						</div>";
						
						echo "<div class=\"form-group\" >
							<label class=\"col-md-3 control-label\" for=\"status\">Státusz:</label>
							<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span></label>
						</div>";
						
						echo "<div style=\"clear:both;height:10px;\"></div>";
						
						echo "<div class=\"form-group form-actions\">
							<div class=\"col-md-9 col-md-offset-3\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewmatcat();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>";
						
				echo "</div>
				</form>";
			?>
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/goods/js/inc_matcat.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?	
		}
		else{
			
			//echo "select * from functions where id='".$uritags[2]."'";
			$mymatcat=db_row("select * from alapag_kategoria where id='".$uritags[3]."'");
			if($mymatcat['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="modules/goods/matcat">Alapanyag kategória</a></li>
				<li><a href="/<?=$URI;?>"><?=$mymatcat['name'];?></a></li>
			</ul>
			<div class="block full">
				
					<!-- Block Tabs Title -->
					<div class="block-title">
						<h2><strong><?=$mymatcat['name'];?></strong> alapanyag kategória oldal</h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
							if(priv("matcat","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("matcat","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							if(priv("matcat","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("matcat","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
							if(priv("matcat","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}	
							?>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="col-md-6">
								<dl class="dl-horizontal">
									<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$mymatcat['id']); ?></dd>
									<dt>Név:</dt><dd><? echo $mymatcat['name']; ?></dd>
									<dt>Rövid név:</dt><dd><? echo $mymatcat['comment']; ?></dd>
									<dt>Státusz:</dt><dd><? if($mymatcat['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
								</dl>	
							</div>
							<div class="col-md-6">
								<?php
									$pic=db_one("select filename from files where modul='goods' and type='matcat' and type_id='".$mymatcat['id']."' order by cover desc");
									if(is_file($pic)){
										echo"<div class=\"col-sm-3\"><img src=\"/".$pic."\" alt=\"avatar\" width=\"450px\"></div>";
									}
									else{
										echo"<img src=\"img/placeholders/avatars/avatar9@2x.jpg\" alt=\"avatar\">";
									}
								?>
							</div>
							<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-2">
						<?
							echo "<form id=\"editmatcat\" name=\"editmatcat\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
							echo "<div style=\"margin-top:1px;\">";
							
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"name\">Alapanyag kategória megnevezése:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" value=\"".$mymatcat['name']."\" />																		
									</div>
								</div>";
								
								
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"shortname\">Új alpanyag címkenév:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"shortname\" name=\"shortname\" class=\"form-control\" value=\"".$mymatcat['comment']."\" />																		
							</div>
						</div>";
								echo "<div class=\"form-group\" >
									<label class=\"col-md-3 control-label\" for=\"status\">Státusz:</label>";
									$st="";if($mymatcat	['status']==1){ $st=" checked "; }
									echo "<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\"".$st." ><span></span></label>
								</div>";
								
								echo "<div class=\"form-group form-actions\">
									<div class=\"col-md-9 col-md-offset-3\">
										<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditmatcat(".$mymatcat['id'].")\"><i class=\"fa fa-angle-right\"></i> Ment</button>
									</div>
								</div>";
									
							echo "</div>
							</form>";
						?>
						</div>
						<div class="tab-pane" id="tab-3">
								<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mymatcat['id'],"Dokumentum feltöltés")."</div>";
								?> 
								<div id="filelists"></div>
								<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mymatcat['id'],"Hozzászólások")."</div>";
								?>
						</div>
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>
				
					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
					
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/goods/js/inc_matcat.js"></script>

			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mymatcat['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$mymatcat['id'];?>,'Dokumentum feltöltés','filelists');
			</script>
			<script>$(function(){ MatCatDatatables.init(); });</script>	

			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
		//print_r($_SESSION['planetsys'])
?>    
<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Alapanyag kategória</a></li>
			</ul>
			<!-- END Datatables Header -->

			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Alapanyag kategória</strong> lista</h2>
				</div>
				
				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							//echo"<th>Művelet</th>";
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/goods/js/inc_matcat.js"></script>
		<script>$(function(){ MatCatDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>
