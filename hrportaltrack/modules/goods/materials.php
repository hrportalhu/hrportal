<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Alapanyag kezelő
            </h1>
        </div>
    </div>
<?
//echo getalap(312);
if(isset($uritags[3]) && $uritags[3]!=""){
	if($uritags[3]=="new"){
		?>
		<!-- Ez egy elem  -->		
		<ul class="breadcrumb breadcrumb-top">
			<li><a href="/">Rendszer</a></li>
			<li><a href="/modules/goods/materials">Alapanyag kezelő</a></li>
			<li><a href="/new">Új alapanyag</a></li>
		</ul>
		<div class="block full">
			<div class="block-title">
				<h2><strong>Új alapanyag felvitele</strong></h2>
			</div>
			<?
			$cats=db_all("select id, name from alapag_kategoria where status=1 order by name");
			$mert=db_all("select id, name from mertekegyseg where status=1 order by name");
			$ceg=db_all("select id, name from partners where status=1 order by name");
			
			
			echo"<form id=\"newmaterials\" name=\"newmaterials\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo"<ul class=\"nav nav-tabs\" data-toggle=\"tabs\">";
					echo"<li class=\"active\"><a href=\"#tab-a1\" data-toggle=\"tooltip\" title=\"Alaptulajdonságok\"><i class=\"fa fa-copy\"></i></a></li>";
					//echo"<li><a href=\"#tab-a2\" data-toggle=\"tooltip\" title=\"Allergének\"><i class=\"fa fa-file-o\"></i></a></li>";
				echo"</ul>";
				
				
				echo"<div class=\"tab-content\">";
				echo"<div class=\"tab-pane active\" id=\"tab-a1\">";

			
					echo"<div class=\"form-group\">
					<label class=\"col-md-3 control-label\" for=\"tcode\">Alapanyag kódja:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"tcode\" name=\"tcode\" class=\"form-control\" placeholder=\"Alapanyag kódja\"  />																		
						</div>
					</div>";				
							
					echo"<div class=\"form-group\">
					<label class=\"col-md-3 control-label\" for=\"name\">Alapanyag megnevezése:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" placeholder=\"Alapanyag megnevezése\"  />																		
						</div>
					</div>";
					
					echo"<div class=\"form-group\">
					<label class=\"col-md-3 control-label\" for=\"shortname\">Alapanyag rövid neve:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"shortname\" name=\"shortname\" class=\"form-control\" placeholder=\"Alapanyag rövid neve\"  />																		
						</div>
					</div>";
					
					echo"<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"tcs_id\">Kategória:</label>
						<div class=\"col-md-9\">
							<select id=\"tcs_id\" name=\"tcs_id\" class=\"form-control\" placeholder=\"Kategória\" >
							<option value=\"0\">Kérlek válassz</option>";
							for($i=0;$i<count($cats);$i++){
								echo"<option value=\"".$cats[$i]['id']."\" >".$cats[$i]['name']."</option>";	
							}
							echo"</select>";
						echo"</div>
					</div>";
					
					echo"<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"measure\">Alapmértékegység:</label>
						<div class=\"col-md-9\">
							<select id=\"measure\" name=\"measure\" class=\"form-control\" >
							<option value=\"0\">Kérlek válassz</option>";
							for($i=0;$i<count($mert);$i++){
								echo"<option value=\"".$mert[$i]['id']."\" >".$mert[$i]['name']."</option>";	
							}
							echo"</select>";
						echo"</div>
					</div>";
					
					echo"<div class=\"form-group\">
					<label class=\"col-md-3 control-label\" for=\"kiszereles\">Kiszerelés:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"kiszereles\" name=\"kiszereles\" class=\"form-control\" placeholder=\"Kiszerelés\"  />																		
						</div>
					</div>";
					
					echo"<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"dealer_id\">Forgalmazó:</label>
						<div class=\"col-md-9\">
							<select id=\"dealer_id\" name=\"dealer_id\" class=\"form-control\" >
							<option value=\"0\">Kérlek válassz</option>";
							for($i=0;$i<count($ceg);$i++){
								echo"<option value=\"".$ceg[$i]['id']."\" >".$ceg[$i]['name']."</option>";	
							}
							echo"</select>";
						echo"</div>
					</div>";
					
					echo"<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"manufact_id\">Gyártó:</label>
						<div class=\"col-md-9\">
							<select id=\"manufact_id\" name=\"manufact_id\" class=\"form-control\"  >
							<option value=\"0\">Kérlek válassz</option>";
							for($i=0;$i<count($ceg);$i++){
								echo"<option value=\"".$ceg[$i]['id']."\" >".$ceg[$i]['name']."</option>";	
							}
							echo"</select>";
						echo"</div>
					</div>";
					
					echo"<div class=\"form-group\">
					<label class=\"col-md-3 control-label\" for=\"max_stock\">Maximális készlet:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"max_stock\" name=\"max_stock\" class=\"form-control\" placeholder=\"Maximális készlet\"  />																		
						</div>
					</div>";
					
					echo"<div class=\"form-group\">
					<label class=\"col-md-3 control-label\" for=\"min_stock\">Minimális készlet:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"min_stock\" name=\"min_stock\" class=\"form-control\" placeholder=\"Minimális készlet\"  />																		
						</div>
					</div>";	
					
					echo"<div class=\"form-group\">
					<label class=\"col-md-3 control-label\" for=\"osszetetel\">Összetétel:</label>
						<div class=\"col-md-9\">
							<textarea id=\"osszetetel\" name=\"osszetetel\" class=\"ckeditor\" placeholder=\"Összetétel..\"></textarea>
						</div>
					</div>";	
											
					echo"<!-- <div class=\"form-group\">
					<label class=\"col-md-3 control-label\" for=\"measure\">Heti átlag fogyás:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"measure\" name=\"measure\" class=\"form-control\" placeholder=\"Heti átlag fogyás\"  />																		
						</div>
					</div> -->";
					echo"<div class=\"form-group\">
					<label class=\"col-md-3 control-label\" for=\"tarolasi_ho\">Tárolási hő:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"tarolasi_ho\" name=\"tarolasi_ho\" class=\"form-control\" placeholder=\"Tárolási Hő\"  value=\"\"/>																		
						</div>
					</div>";
					echo"<div class=\"form-group\">
					<label class=\"col-md-3 control-label\" for=\"minosegmegorzes1\"> Normál minőség megőrzési idő:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"minosegmegorzes1\" name=\"minosegmegorzes1\" class=\"form-control\"   value=\"\"/>																		
						</div>
					</div>";
					echo"<div class=\"form-group\">
					<label class=\"col-md-3 control-label\" for=\"minosegmegorzes2\">Másodlagos minőség megőrzési idő</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"minosegmegorzes2\" name=\"minosegmegorzes2\" class=\"form-control\"   value=\"\"/>																		
						</div>
									</div>";
					echo "<div class=\"form-group\" >
						<label class=\"col-md-3 control-label\" for=\"multiplier\">Szorzótényező:</label>
						<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"multiplier\" name=\"multiplier\" checked><span></span></label>
					</div>";
					
					echo"<div class=\"form-group\" >";
						echo"<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"child_warn\" name=\"child_warn\" ><span></span><br />1333/2008/EK számú, az élelmiszer-adalékanyagokról szóló rendelet szerint kell-e figyelmeztető feliratot (E102, E104, E110, E122, E124, E129)</label>
					</div>";
					
					echo "<div class=\"form-group\" >
						<label class=\"col-md-3 control-label\" for=\"status\">Státusz:</label>
						<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span></label>
					</div>";
				
				echo"</div>	";
				
			/*	
				echo"<div class=\"tab-pane\" id=\"tab-a2\">";
					echo"<div class=\"form-group\" >";
						echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_gluten\" name=\"allerg_gluten\"><span></span>Glutén</label>
					</div>";
					echo"<div class=\"form-group\" >";
						echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_szoja\" name=\"allerg_szoja\"><span></span>Szója</label>
					</div>";
					echo"<div class=\"form-group\" >";
						echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_tejtermek\" name=\"allerg_tejtermek\"><span></span>Tejtermék</label>
					</div>";
					echo"<div class=\"form-group\" >";
						echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_tojas\" name=\"allerg_tojas\"><span></span>Tojás</label>
					</div>";
					echo"<div class=\"form-group\" >";
						echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_kendioxid\" name=\"allerg_kendioxid\"><span></span>Kén-dioxid</label>
					</div>";
					echo"<div class=\"form-group\" >";
						echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_diofelek\" name=\"allerg_diofelek\"><span></span>Diófélék</label>
					</div>";
					echo"<div class=\"form-group\" >";
						echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_mogyorofelek\" name=\"allerg_mogyorofelek\"><span></span>Mogyorófélék</label>
					</div>";
					echo"<div class=\"form-group\" >";
						echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_szezammag\" name=\"allerg_szezammag\"><span></span>Szezámmag</label>
					</div>";
					echo"<div class=\"form-group\" >";
						echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_rakfelek\" name=\"allerg_rakfelek\"><span></span>Rákfélék</label>
					</div>";
					echo"<div class=\"form-group\" >";
						echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_halak\" name=\"allerg_halak\"><span></span>Halak</label>
					</div>";
					echo"<div class=\"form-group\" >";
						echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_zeller\" name=\"allerg_zeller\"><span></span>Zeller</label>
					</div>";
					echo"<div class=\"form-group\" >";
						echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_mustar\" name=\"allerg_mustar\"><span></span>Mustár</label>
					</div>";
					echo"<div class=\"form-group\" >";
						echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_puhatestu\" name=\"allerg_puhatestu\"><span></span>Puhatestű</label>
					</div>";
					echo"<div class=\"form-group\" >";
						echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_csillagfurt\" name=\"allerg_csillagfurt\"><span></span>Csillagfürt</label>
					</div>";
				echo"</div>	";
			*/	
				echo"<div style=\"clear:both;height:10px;\"></div>";
				
				echo"<div class=\"form-group form-actions\">
					<div class=\"col-md-9 col-md-offset-3\">
						<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewmaterials();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
					</div>
				</div>";
				
			echo"</div>
			</form>";
		?>	
		</div>	
		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>
		<script src="js/ckeditor/ckeditor.js"></script>
		<script src="/modules/goods/js/inc_materials.js"></script>
		<?php include 'inc/template_end.php'; ?>	
		<?	
	}
	else{
		
		//echo "select * from functions where id='".$uritags[2]."'";
		$mymaterials=db_row("select * from alapag_tipus where id='".$uritags[3]."'");
		
		//$chec=db_all("select * from alapanyag where consumed!='0000-00-00 00:00:00'");
		
		$summennyiseg=db_one("select sum(ag.amount) as amount from alapag_events ag left join alapanyag a on (ag.ag_id=a.id) where a.consumed='0000-00-00 00:00:00' and ag.agt_id='".$mymaterials['id']."' and (ag.event=1 or ag.event=3 or ag.event=7  or ag.event=6)");
		if($mymaterials['id']==''){
			die("Helytelen hozzáférés!");
		}
		else{
?>
		<!-- Ez egy elem  -->		
		<ul class="breadcrumb breadcrumb-top">
			<li><a href="/">Rendszer</a></li>
			<li><a href="modules/goods/materials">Alapanyagok</a></li>
			<li><a href="/<?=$URI;?>"><?=$mymaterials['name'];?></a></li>
		</ul>
		<div class="block full">
						   
				<!-- Block Tabs Title -->

				<div class="block-title">
					<h2>
						<span style="text-align:left;float:left;width:30px;">
							<a href="/modules/goods/materials/<?=($mymaterials['id']-1);?>"><i class="fa fa-arrow-left"></i></a>
						</span>
						<span style="text-align:center;">
							<strong><?=$mymaterials['name'];?></strong> alapanyagtípus oldala
						</span>
						<a href="/modules/goods/materials/<?=($mymaterials['id']+1);?>"><i class="fa fa-arrow-right"></i></a>
					</h2>
					<ul class="nav nav-tabs" data-toggle="tabs">
						<?
						if(priv("materials","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
						if(priv("materials","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
						if(priv("materials","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
						if(priv("materials","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
						if(priv("materials","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}
						if(priv("materials","inside")){echo"<li><a href=\"#tab-6\" data-toggle=\"tooltip\" title=\"inside\"><i class=\"fa fa-th\"></i></a></li>";}
						if(priv("materials","supplier")){echo"<li><a href=\"#tab-7\" data-toggle=\"tooltip\" title=\"supplier\"><i class=\"fa fa-users\"></i></a></li>";}
						if(priv("materials","story")){echo"<li><a href=\"#tab-8\" data-toggle=\"tooltip\" title=\"story\"><i class=\"gi gi-beer\"></i></a></li>";}
						if(priv("materials","baseprice")){echo"<li><a href=\"#tab-9\" data-toggle=\"tooltip\" title=\"Utolsó beszerzési ár\"><i class=\"fa fa-euro\"></i></a></li>";}
						if(priv("materials","baseprice")){echo"<li><a href=\"#tab-11\" data-toggle=\"tooltip\" title=\"Kialkudott ár\"><i class=\"fa fa-yen\"></i></a></li>";}
						if(priv("materials","story")){echo"<li><a href=\"#tab-10\" data-toggle=\"tooltip\" title=\"storemodify\"><i class=\"fa fa-exchange\"></i></a></li>";}
						?>
					</ul>
				</div>
				<!-- END Block Tabs Title -->

				<!-- Tabs Content -->
				<div class="tab-content">

					<div class="tab-pane active" id="tab-1">
						<div class="block hidden-lt-ie9">
							<!-- Switches Title -->
							<?
						  
								//echo"<div class=\"block-title \"><h2 ><strong >Alapanyag információk</strong> </h2></div>";
								$mertek=idchange("mertekegyseg","name",$mymaterials['measure']);
							?>
							<div class="row">
							<div class="col-md-6">
								<div class="block">
									<div class="block-title clearfix">
										<h2 class="pull-right"><strong>Alapanyag</strong> Adatok</h2>
									</div>
								<dl class="dl-horizontal">
									<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$mymaterials['id']); ?></dd>
									<dt>Alapanyag kódja:</dt><dd><? echo $mymaterials['tcode']; ?></dd>
									<dt>Alapanyag megnvezése:</dt><dd><? echo $mymaterials['name']; ?></dd>
									<dt>Alapanyag rövid neve:</dt><dd><? echo $mymaterials['shortname']; ?></dd>
									<dt>Kategória:</dt><dd><? echo idchange("alapag_kategoria","name",$mymaterials['tcs_id']); ?></dd>
									<dt>Alapmértékegység:</dt><dd><? echo $mertek; ?></dd>
									<dt>Kiszerelés:</dt><dd><? echo $mymaterials['kiszereles']; ?></dd>
									<dt>Forgalmazó:</dt><dd><? echo idchange("partners","name",$mymaterials['dealer_id']); ?></dd>
									<dt>Gyártó:</dt><dd><? echo idchange("partners","name",$mymaterials['manufact_id']); ?></dd>
									<dt>Maximális készlet:</dt><dd><? echo $mymaterials['max_stock']; ?> <? echo $mertek; ?></dd>
									<dt>Minimális készlet:</dt><dd><? echo $mymaterials['min_stock']; ?> <? echo $mertek; ?></dd>
									<dt>Fellelhető össz mennyis:</dt><dd><? 
										if(priv("materials","story")){echo round(getalap($mymaterials['id']),3); }
									?> <? echo $mertek; ?></dd>
									<dt>Aktuális beszerzési ár:</dt><dd><? echo round($mymaterials['baseprice'],3); ?> Ft</dd>
									<dt>Szorzótényező:</dt><dd><? if($mymaterials['multiplier']==1){echo"Igen";}else{echo"Nem";} ?></dd>
									<dt>Érvényese:</dt><dd><? if($mymaterials['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
								</dl>	
							</div>
							</div>
							
									<div class="col-md-6">
										<div class="block">
											<div class="block-title">
												<h2><strong>Fotó</strong></h2>
											</div>
											<?php
												$pic=db_one("select filename from files where modul='goods' and type='materials' and type_id='".$mymaterials['id']."' order by cover desc");
												//echo $pic;
												if(is_file($pic)){
													echo"<center><img src=\"/".$pic."\" alt=\"avatar\" style=\"height:180px;\" /></center>";
												}
												else{
													echo"<center><img src=\"img/placeholders/avatars/avatar9@2x.jpg\" alt=\"avatar\" /></center>";
												}
											?>
										</div>		
									
									</div>
							

							</div>
							<div class="col-md-6">
								<div class="block">
									<div class="block-title clearfix">
										<h2 class="pull-right"><strong>Tápérték</strong> Adatok</h2>
									</div>
									<dl class="dl-horizontal">
										<dt>Energia KJ /100g-ban/:</dt><dd><?= $mymaterials['energiakj']; ?> Kj</dd>
										<dt>Energia Kcal /100g-ban/:</dt><dd><?= $mymaterials['energiakc']; ?>KCal</dd>
										<dt>Fehérje /100g-ban/:</dt><dd><?= $mymaterials['feherje']; ?> g</dd>
										<dt>Zsír /100g-ban/:</dt><dd><?= $mymaterials['zsir']; ?> g</dd>
										<dt>Telített zsírsavak /100g-ban/:</dt><dd><?= $mymaterials['zsir_telitett']; ?> g</dd>
										<dt>Egyszeresen telített zsírsavak /100g-ban/::</dt><dd><?= $mymaterials['zsir_egyszeres']; ?> g</dd>
										<dt>Többszörösen telített zsírsavak /100g-ban/:</dt><dd><?= $mymaterials['zsir_tobb']; ?> g</dd>
										<dt>Szénhidrát /100g-ban/:</dt><dd><?= $mymaterials['szenhidrat']; ?> g</dd>
										<dt>Szénhidrát cukortartalma /100g-ban/:</dt><dd><?= $mymaterials['cukor']; ?> g</dd>
										<dt>Szénhidrát maltitol tartalma /100g-ban/:</dt><dd><?= $mymaterials['maltitol']; ?> g</dd>
										<dt>Szénhidrát cukoralkohol /100g-ban/:</dt><dd><?= $mymaterials['cukoralkohol']; ?> g</dd>
										<dt>Szénhidrát poliol tartalma /100g-ban/:</dt><dd><?= $mymaterials['poliol']; ?> g</dd>
										<dt>Rost /100g-ban/:</dt><dd><?= $mymaterials['rost']; ?> g</dd>
										<dt>Koleszterin /100g-ban/:</dt><dd><?= $mymaterials['koleszterin']; ?> g</dd>
										<dt>Só /100g-ban/:</dt><dd><?= $mymaterials['so']; ?> g</dd>
								</dl>	
								</div>
							</div>
														<div class="col-md-6">
								<div class="block">
									<div class="block-title clearfix">
										<h2 class="pull-right"><strong>Allergén</strong> Adatok</h2>
									</div>
									<dl class="dl-horizontal">
										<dt>Glutén:</dt><dd><? if($mymaterials['allerg_gluten']=="1"){echo" tartalmaz";}elseif($mymaterials['allerg_gluten_opcio']=="1"){echo" nyomokban tartalmazhat";}else{echo"nem tartalmaz";} ?></dd>
										<dt>Szója:</dt><dd><? if($mymaterials['allerg_szoja']=="1"){echo" tartalmaz";}elseif($mymaterials['allerg_szoja_opcio']=="1"){echo" nyomokban tartalmazhat";}else{echo"nem tartalmaz";} ?></dd>
										<dt>Tejtermék:</dt><dd><? if($mymaterials['allerg_tejtermek']=="1"){echo" tartalmaz";}elseif($mymaterials['allerg_tejtermek_opcio']=="1"){echo" nyomokban tartalmazhat";}else{echo"nem tartalmaz";} ?></dd>
										<dt>Tojás:</dt><dd><? if($mymaterials['allerg_tojas']=="1"){echo" tartalmaz";}elseif($mymaterials['allerg_tajas_opcio']=="1"){echo" nyomokban tartalmazhat";}else{echo"nem tartalmaz";} ?></dd>
										<dt>Kén dioxid:</dt><dd><? if($mymaterials['allerg_kendioxid']=="1"){echo" tartalmaz";}elseif($mymaterials['allerg_kendioxid_opcio']=="1"){echo" nyomokban tartalmazhat";}else{echo"nem tartalmaz";} ?></dd>
										<dt>Diófélék:</dt><dd><? if($mymaterials['allerg_diofelek']=="1"){echo" tartalmaz";}if($mymaterials['allerg_diofelek_opcio']=="1"){echo" nyomokban tartalmazhat";}else{echo"nem tartalmaz";} ?></dd>
										<dt>Mogyorófélék:</dt><dd><? if($mymaterials['allerg_mogyorofelek']=="1"){echo" tartalmaz";}elseif($mymaterials['allerg_mogyorofelek_opcio']=="1"){echo" nyomokban tartalmazhat";}else{echo"nem tartalmaz";} ?></dd>
										<dt>Szezámmag:</dt><dd><? if($mymaterials['allerg_szezammag']=="1"){echo" tartalmaz";}elseif($mymaterials['allerg_szezammag_opcio']=="1"){echo" nyomokban tartalmazhat";}else{echo"nem tartalmaz";} ?></dd>
										<dt>Rákfélék:</dt><dd><? if($mymaterials['allerg_rakfelek']=="1"){echo" tartalmaz";}elseif($mymaterials['allerg_rakfelek_opcio']=="1"){echo" nyomokban tartalmazhat";}else{echo"nem tartalmaz";} ?></dd>
										<dt>Halak:</dt><dd><? if($mymaterials['allerg_halak']=="1"){echo" tartalmaz";}elseif($mymaterials['allerg_halak_opcio']=="1"){echo" nyomokban tartalmazhat";}else{echo"nem tartalmaz";} ?></dd>
										<dt>Zeller:</dt><dd><? if($mymaterials['allerg_zeller']=="1"){echo" tartalmaz";}elseif($mymaterials['allerg_zeller_opcio']=="1"){echo" nyomokban tartalmazhat";}else{echo"nem tartalmaz";} ?></dd>
										<dt>Mustár:</dt><dd><? if($mymaterials['allerg_mustar']=="1"){echo" tartalmaz";}elseif($mymaterials['allerg_mustar_opcio']=="1"){echo" nyomokban tartalmazhat";}else{echo"nem tartalmaz";} ?></dd>
										<dt>Puhatestű:</dt><dd><? if($mymaterials['allerg_puhatestu']=="1"){echo" tartalmaz";}elseif($mymaterials['allerg_puhatestu_opcio']=="1"){echo" nyomokban tartalmazhat";}else{echo"nem tartalmaz";} ?></dd>
										<dt>Csillagfürt:</dt><dd><? if($mymaterials['allerg_csillagfurt']=="1"){echo" tartalmaz";}elseif($mymaterials['allerg_csillagfurt_opcio']=="1"){echo" nyomokban tartalmazhat";}else{echo"nem tartalmaz";} ?></dd>
									</dl>	
								</div>
							</div>

							
							
							<div style="clear:both;height:1px;"></div>
							
						</div>
						
						<div class="block hidden-lt-ie9">
							<!-- Switches Title -->
							<div class="block-title">
								<h2><strong>Beszállítói lista</strong> </h2>
							</div>
							<!-- END Switches Title -->
							<div id="elementdiv1"></div>
						</div>	
						
					</div>
					<div class="tab-pane" id="tab-2">
						<?
						$cats=db_all("select id, name from alapag_kategoria where status=1 order by name");
						$mert=db_all("select id, name from mertekegyseg where status=1 order by name");
						$ceg=db_all("select id, name from partners where status=1 order by name");
						
						echo "<form id=\"editmaterials\" name=\"editmaterials\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
							echo"<ul class=\"nav nav-tabs\" data-toggle=\"tabs\">";
								echo"<li class=\"active\"><a href=\"#tab-a1\" data-toggle=\"tooltip\" title=\"Alaptulajdonságok\"><i class=\"fa fa-copy\"></i></a></li>";
								echo"<li ><a href=\"#tab-a4\" data-toggle=\"tooltip\" title=\"Tápérték adatok\"><i class=\"gi gi-pizza\"></i></a></li>";
								echo"<li ><a href=\"#tab-a5\" data-toggle=\"tooltip\" title=\"Beszállítói adatok\"><i class=\"fa fa-truck\"></i></a></li>";
								echo"<li><a href=\"#tab-a2\" data-toggle=\"tooltip\" title=\"Allergént tartalmaz\"><i class=\"fa fa-file-o\"></i></a></li>";
								echo"<li><a href=\"#tab-a3\" data-toggle=\"tooltip\" title=\"Allergént tartalmazhat\"><i class=\"fa fa-file\"></i></a></li>";
							echo"</ul>";
							
							
							echo"<div class=\"tab-content\">";
							echo"<div class=\"tab-pane active\" id=\"tab-a1\">";

								echo"<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"tcode\">Alapanyag kódja:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"tcode\" name=\"tcode\" class=\"form-control\" placeholder=\"Alapanyag kódja\"  value=\"".$mymaterials['tcode']."\"/>																		
									</div>
								</div>";	
												
								echo"<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"name\">Alapanyag megnevezése:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" placeholder=\"Alapanyag megnevezése\"  value=\"".$mymaterials['name']."\"/>																		
									</div>
								</div>";
								
								echo"<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"shortname\">Alapanyag rövid neve:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"shortname\" name=\"shortname\" class=\"form-control\" placeholder=\"Alapanyag rövid neve\"  value=\"".$mymaterials['shortname']."\"/>																		
									</div>
								</div>";
								
								echo"<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"tcs_id\">Kategória:</label>
									<div class=\"col-md-9\">
										<select id=\"tcs_id\" name=\"tcs_id\" class=\"form-control\" >
										<option value=\"0\">Kérlek válassz</option>";
										for($i=0;$i<count($cats);$i++){
											$ch1="";if($mymaterials['tcs_id']==$cats[$i]['id']){$ch1=" selected "; }
											echo"<option value=\"".$cats[$i]['id']."\" ".$ch1." >".$cats[$i]['name']."</option>";	
										}
										echo"</select>";
									echo"</div>
								</div>";
								
								echo"<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"measure\">Alapmértékegység:</label>
									<div class=\"col-md-9\">
										<select id=\"measure\" name=\"measure\" class=\"form-control\" >
										<option value=\"0\">Kérlek válassz</option>";
										for($i=0;$i<count($mert);$i++){
											$ch2="";if($mymaterials['measure']==$mert[$i]['id']){$ch2=" selected "; }
											echo"<option value=\"".$mert[$i]['id']."\" ".$ch2." >".$mert[$i]['name']."</option>";	
										}
										echo"</select>";
									echo"</div>
								</div>";
								
								echo"<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"kiszereles\">Kiszerelés:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"kiszereles\" name=\"kiszereles\" class=\"form-control\" placeholder=\"Kiszerelés\"  value=\"".$mymaterials['kiszereles']."\"/>																		
									</div>
								</div>";
								
								
								
								echo"<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"max_stock\">Maximális készlet:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"max_stock\" name=\"max_stock\" class=\"form-control\" placeholder=\"Maximális készlet\"  value=\"".$mymaterials['max_stock']."\"/>																		
									</div>
								</div>";
								
								echo"<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"min_stock\">Minimális készlet:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"min_stock\" name=\"min_stock\" class=\"form-control\" placeholder=\"Minimális készlet\"  value=\"".$mymaterials['min_stock']."\"/>																		
									</div>
								</div>";
								
																
								echo"<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"osszetetel\">Összetétel:</label>
									<div class=\"col-md-9\">
										<textarea id=\"osszetetel\" name=\"osszetetel\" class=\"ckeditor\" value=\"".$mymaterials['osszetetel']."\" >".$mymaterials['osszetetel']."</textarea>
									</div>
								</div>";	
														

								echo"<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"tarolasi_ho\">Tárolási hő:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"tarolasi_ho\" name=\"tarolasi_ho\" class=\"form-control\" placeholder=\"Tárolási Hő\"  value=\"".$mymaterials['tarolasi_ho']."\"/>																		
									</div>
								</div>";
								echo"<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"minosegmegorzes1\"> Normál minőség megőrzési idő:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"minosegmegorzes1\" name=\"minosegmegorzes1\" class=\"form-control\"   value=\"".$mymaterials['minosegmegorzes1']."\"/>																		
									</div>
								</div>";
								echo"<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"minosegmegorzes2\">Másodlagos minőség megőrzési idő</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"minosegmegorzes2\" name=\"minosegmegorzes2\" class=\"form-control\"   value=\"".$mymaterials['minosegmegorzes2']."\"/>																		
									</div>
								</div>";

								
								echo "<div class=\"form-group\" >
									<label class=\"col-md-3 control-label\" for=\"multiplier\">Szorzótényező:</label>";
									$chf="";if($mymaterials['multiplier']==1){ $chf=" checked "; }
									echo "<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"multiplier\" name=\"multiplier\"".$chf." ><span></span></label>
								</div>";
								echo"<div class=\"form-group\" >";
										$chpa="";if($mymaterials['child_warn']==1){ $chpa=" checked "; }
										echo"<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"child_warn\" name=\"child_warn\" ".$chpa."><span></span><br />1333/2008/EK számú, az élelmiszer-adalékanyagokról szóló rendelet szerint kell-e figyelmeztető feliratot (E102, E104, E110, E122, E124, E129)</label>
									</div>";
								echo "<div class=\"form-group\" >
									<label class=\"col-md-3 control-label\" for=\"status\">Státusz:</label>";
									$st="";if($mymaterials['status']==1){ $st=" checked "; }
									echo "<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\"".$st." ><span></span></label>
								</div>";
							echo"</div>	";	
								
								echo"<div class=\"tab-pane\" id=\"tab-a5\">";
									echo"<h3>Beszállítói adatok</h3>";
										echo"<div class=\"form-group\">
												<label class=\"col-md-3 control-label\" for=\"dealer_id\">Forgalmazó:</label>
												<div class=\"col-md-9\">
													<select id=\"dealer_id\" name=\"dealer_id\" class=\"form-control\">
													<option value=\"0\">Kérlek válassz</option>";
													for($i=0;$i<count($ceg);$i++){
														$ch3="";if($mymaterials['dealer_id']==$ceg[$i]['id']){$ch3=" selected "; }
														echo"<option value=\"".$ceg[$i]['id']."\" ".$ch3.">".$ceg[$i]['name']."</option>";	
													}
													echo"</select>";
												echo"</div>
											</div>";
											
											echo"<div class=\"form-group\">
												<label class=\"col-md-3 control-label\" for=\"manufact_id\">Gyártó:</label>
												<div class=\"col-md-9\">
													<select id=\"manufact_id\" name=\"manufact_id\" class=\"form-control\">
													<option value=\"0\">Kérlek válassz</option>";
													for($i=0;$i<count($ceg);$i++){
														$ch4="";if($mymaterials['manufact_id']==$ceg[$i]['id']){$ch4=" selected "; }
														echo"<option value=\"".$ceg[$i]['id']."\" ".$ch4.">".$ceg[$i]['name']."</option>";	
													}
													echo"</select>";
												echo"</div>
											</div>";
								echo"</div>	";
								echo"<div class=\"tab-pane\" id=\"tab-a2\">";
									echo"<h3>Tartalmazó allergének</h3>";
									echo"<div class=\"form-group\" >";
										$st1="";if($mymaterials['allerg_gluten']==1){ $st1=" checked "; }
										echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_gluten\" name=\"allerg_gluten\"".$st1." onclick=\"javascript:setalergen('".$mymaterials['id']."','allerg_gluten');\"><span></span>Glutén</label>
									</div>";
									echo"<div class=\"form-group\" >";
										$st2="";if($mymaterials['allerg_szoja']==1){ $st2=" checked "; }
										echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_szoja\" name=\"allerg_szoja\"".$st2." onclick=\"javascript:setalergen('".$mymaterials['id']."','allerg_szoja');\" ><span></span>Szója</label>
									</div>";
									echo"<div class=\"form-group\" >";
										$st3="";if($mymaterials['allerg_tejtermek']==1){ $st3=" checked "; }
										echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_tejtermek\" name=\"allerg_tejtermek\"".$st3." onclick=\"javascript:setalergen('".$mymaterials['id']."','allerg_tejtermek');\"><span></span>Tejtermék</label>
									</div>";
									echo"<div class=\"form-group\" >";
										$st4="";if($mymaterials['allerg_tojas']==1){ $st4=" checked "; }
										echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_tojas\" name=\"allerg_tojas\"".$st4." onclick=\"javascript:setalergen('".$mymaterials['id']."','allerg_tojas');\"><span></span>Tojás</label>
									</div>";
									echo"<div class=\"form-group\" >";
										$st5="";if($mymaterials['allerg_kendioxid']==1){ $st5=" checked "; }
										echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_kendioxid\" name=\"allerg_kendioxid\"".$st5." onclick=\"javascript:setalergen('".$mymaterials['id']."','allerg_kendioxid');\"><span></span>Kén-dioxid</label>
									</div>";
									echo"<div class=\"form-group\" >";
										$st6="";if($mymaterials['allerg_diofelek']==1){ $st6=" checked "; }
										echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_diofelek\" name=\"allerg_diofelek\"".$st6." onclick=\"javascript:setalergen('".$mymaterials['id']."','allerg_diofelek');\"><span></span>Diófélék</label>
									</div>";
									echo"<div class=\"form-group\" >";
										$st7="";if($mymaterials['allerg_mogyorofelek']==1){ $st7=" checked "; }
										echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_mogyorofelek\" name=\"allerg_mogyorofelek\"".$st7." onclick=\"javascript:setalergen('".$mymaterials['id']."','allerg_mogyorofelek');\"><span></span>Mogyorófélék</label>
									</div>";
									echo"<div class=\"form-group\" >";
										$st8="";if($mymaterials['allerg_szezammag']==1){ $st8=" checked "; }
										echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_szezammag\" name=\"allerg_szezammag\"".$st8." onclick=\"javascript:setalergen('".$mymaterials['id']."','allerg_szezammag');\"><span></span>Szezámmag</label>
									</div>";
									echo"<div class=\"form-group\" >";
										$st9="";if($mymaterials['allerg_rakfelek']==1){ $st9=" checked "; }
										echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_rakfelek\" name=\"allerg_rakfelek\"".$st9." onclick=\"javascript:setalergen('".$mymaterials['id']."','allerg_rakfelek');\"><span></span>Rákfélék</label>
									</div>";
									echo"<div class=\"form-group\" >";
										$st10="";if($mymaterials['allerg_halak']==1){ $st10=" checked "; }
										echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_halak\" name=\"allerg_halak\"".$st10." onclick=\"javascript:setalergen('".$mymaterials['id']."','allerg_halak');\"><span></span>Halak</label>
									</div>";
									echo"<div class=\"form-group\" >";
										$st11="";if($mymaterials['allerg_zeller']==1){ $st11=" checked "; }
										echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_zeller\" name=\"allerg_zeller\"".$st11." onclick=\"javascript:setalergen('".$mymaterials['id']."','allerg_zeller');\"><span></span>Zeller</label>
									</div>";
									echo"<div class=\"form-group\" >";
										$st12="";if($mymaterials['allerg_mustar']==1){ $st12=" checked "; }
										echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_mustar\" name=\"allerg_mustar\"".$st12." onclick=\"javascript:setalergen('".$mymaterials['id']."','allerg_mustar');\"><span></span>Mustár</label>
									</div>";
									echo"<div class=\"form-group\" >";
										$st13="";if($mymaterials['allerg_puhatestu']==1){ $st13=" checked "; }
										echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_puhatestu\" name=\"allerg_puhatestu\"".$st13." onclick=\"javascript:setalergen('".$mymaterials['id']."','allerg_puhatestu');\"><span></span>Puhatestű</label>
									</div>";
									echo"<div class=\"form-group\" >";
										$st14="";if($mymaterials['allerg_csillagfurt']==1){ $st14=" checked "; }
										echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_csillagfurt\" name=\"allerg_csillagfurt\"".$st14." onclick=\"javascript:setalergen('".$mymaterials['id']."','allerg_csillagfurt');\"><span></span>Csillagfürt</label>
									</div>";
								echo"</div>	";
								
								echo"<div class=\"tab-pane\" id=\"tab-a3\">";
									echo"<h3>Opcionális allergének</h3>";
									echo"<div class=\"form-group\" >";
										$st22="";if($mymaterials['allerg_gluten_opcio']==1){ $st22=" checked "; }
										echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_gluten_opcio\" name=\"allerg_gluten_opcio\"".$st22." onclick=\"javascript:setalergen('".$mymaterials['id']."','allerg_gluten_opcio');\"><span></span>Glutén</label>
									</div>";
									echo"<div class=\"form-group\" >";
										$st33="";if($mymaterials['allerg_szoja_opcio']==1){ $st33=" checked "; }
										echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_szoja_opcio\" name=\"allerg_szoja_opcio\"".$st33." onclick=\"javascript:setalergen('".$mymaterials['id']."','allerg_szoja_opcio');\" ><span></span>Szója</label>
									</div>";
									echo"<div class=\"form-group\" >";
										$st44="";if($mymaterials['allerg_tejtermek_opcio']==1){ $st44=" checked "; }
										echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_tejtermek_opcio\" name=\"allerg_tejtermek_opcio\"".$st44." onclick=\"javascript:setalergen('".$mymaterials['id']."','allerg_tejtermek_opcio');\"><span></span>Tejtermék</label>
									</div>";
									echo"<div class=\"form-group\" >";
										$st55="";if($mymaterials['allerg_tojas_opcio']==1){ $st55=" checked "; }
										echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_tojas_opcio\" name=\"allerg_tojas_opcio\"".$st55." onclick=\"javascript:setalergen('".$mymaterials['id']."','allerg_tojas_opcio');\"><span></span>Tojás</label>
									</div>";
									echo"<div class=\"form-group\" >";
										$st66="";if($mymaterials['allerg_kendioxid_opcio']==1){ $st66=" checked "; }
										echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_kendioxid_opcio\" name=\"allerg_kendioxid_opcio\"".$st66." onclick=\"javascript:setalergen('".$mymaterials['id']."','allerg_kendioxid_opcio');\"><span></span>Kén-dioxid</label>
									</div>";
									echo"<div class=\"form-group\" >";
										$st77="";if($mymaterials['allerg_diofelek_opcio']==1){ $st77=" checked "; }
										echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_diofelek_opcio\" name=\"allerg_diofelek_opcio\"".$st77." onclick=\"javascript:setalergen('".$mymaterials['id']."','allerg_diofelek_opcio');\"><span></span>Diófélék</label>
									</div>";
									echo"<div class=\"form-group\" >";
										$st88="";if($mymaterials['allerg_mogyorofelek_opcio']==1){ $st88=" checked "; }
										echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_mogyorofelek_opcio\" name=\"allerg_mogyorofelek_opcio\"".$st88." onclick=\"javascript:setalergen('".$mymaterials['id']."','allerg_mogyorofelek_opcio');\"><span></span>Mogyorófélék</label>
									</div>";
									echo"<div class=\"form-group\" >";
										$st98="";if($mymaterials['allerg_szezammag_opcio']==1){ $st98=" checked "; }
										echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_szezammag_opcio\" name=\"allerg_szezammag_opcio\"".$st98." onclick=\"javascript:setalergen('".$mymaterials['id']."','allerg_szezammag_opcio');\"><span></span>Szezámmag</label>
									</div>";
									echo"<div class=\"form-group\" >";
										$st100="";if($mymaterials['allerg_rakfelek_opcio']==1){ $st100=" checked "; }
										echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_rakfelek_opcio\" name=\"allerg_rakfelek_opcio\"".$st100." onclick=\"javascript:setalergen('".$mymaterials['id']."','allerg_rakfelek_opcio');\"><span></span>Rákfélék</label>
									</div>";
									echo"<div class=\"form-group\" >";
										$st101="";if($mymaterials['allerg_halak_opcio']==1){ $st101=" checked "; }
										echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_halak_opcio\" name=\"allerg_halak_opcio\"".$st101." onclick=\"javascript:setalergen('".$mymaterials['id']."','allerg_halak_opcio');\"><span></span>Halak</label>
									</div>";
									echo"<div class=\"form-group\" >";
										$st111="";if($mymaterials['allerg_zeller_opcio']==1){ $st111=" checked "; }
										echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_zeller_opcio\" name=\"allerg_zeller_opcio\"".$st111." onclick=\"javascript:setalergen('".$mymaterials['id']."','allerg_zeller_opcio');\"><span></span>Zeller</label>
									</div>";
									echo"<div class=\"form-group\" >";
										$st121="";if($mymaterials['allerg_mustar_opcio']==1){ $st121=" checked "; }
										echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_mustar_opcio\" name=\"allerg_mustar_opcio\"".$st121." onclick=\"javascript:setalergen('".$mymaterials['id']."','allerg_mustar_opcio');\"><span></span>Mustár</label>
									</div>";
									echo"<div class=\"form-group\" >";
										$st131="";if($mymaterials['allerg_puhatestu_opcio']==1){ $st131=" checked "; }
										echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_puhatestu_opcio\" name=\"allerg_puhatestu_opcio\"".$st131." onclick=\"javascript:setalergen('".$mymaterials['id']."','allerg_puhatestu_opcio');\"><span></span>Puhatestű</label>
									</div>";
									echo"<div class=\"form-group\" >";
										$st141="";if($mymaterials['allerg_csillagfurt_opcio']==1){ $st141=" checked "; }
										echo"<label class=\"col-md-3 switch switch-primary\" ><input type=\"checkbox\" id=\"allerg_csillagfurt_opcio\" name=\"allerg_csillagfurt_opcio\"".$st141." onclick=\"javascript:setalergen('".$mymaterials['id']."','allerg_csillagfurt_opcio');\"><span></span>Csillagfürt</label>
									</div>";
								echo"</div>	";
								
								echo"<div class=\"tab-pane\" id=\"tab-a4\">";
									echo"<h3>Tápérték adatok</h3>";
									echo"<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"energiakj\">Energia KJ /100g-ban/:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"energiakj\" name=\"energiakj\" class=\"form-control\" value=\"".$mymaterials['energiakj']."\"/>
										</div>
									</div>";
								
									echo"<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"energiakc\">Energia KCal /100g-ban/:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"energiakc\" name=\"energiakc\" class=\"form-control\" value=\"".$mymaterials['energiakc']."\"/>
										</div>
									</div>";
								
									echo"<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"feherje\">Fehérje g /100g-ban/:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"feherje\" name=\"feherje\" class=\"form-control\" value=\"".$mymaterials['feherje']."\"/>
										</div>
									</div>";
								
									echo"<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"zsir\">Zsír g /100g-ban/:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"zsir\" name=\"zsir\" class=\"form-control\" value=\"".$mymaterials['zsir']."\"/>
										</div>
									</div>";
								
									echo"<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"zsir_telitett\">Zsír - Telített zsírsavak g /100g-ban/:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"zsir_telitett\" name=\"zsir_telitett\" class=\"form-control\" value=\"".$mymaterials['zsir_telitett']."\"/>
										</div>
									</div>";
									
										echo"<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"zsir_egyszeres\">Zsír - Egyszeresen telítettlen zsírsavak g /100g-ban/:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"zsir_egyszeres\" name=\"zsir_egyszeres\" class=\"form-control\" value=\"".$mymaterials['zsir_egyszeres']."\"/>
										</div>
									</div>";
																
									echo"<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"zsir_tobb\">Zsír - T9bbszörösen telítettlen zsírsavak g /100g-ban/:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"zsir_tobb\" name=\"zsir_tobb\" class=\"form-control\" value=\"".$mymaterials['zsir_tobb']."\"/>
										</div>
									</div>";
								

								
									echo"<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"szenhidrat\">Szénhidrát g /100g-ban/:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"szenhidrat\" name=\"szenhidrat\" class=\"form-control\" value=\"".$mymaterials['szenhidrat']."\"/>
										</div>
									</div>";
								
									echo"<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"cukor\">Szénhidrát cukortartalma g /100g-ban/:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"cukor\" name=\"cukor\" class=\"form-control\" value=\"".$mymaterials['cukor']."\"/>
										</div>
									</div>";
								
									echo"<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"maltitol\">Szénhidrát maltitol tartalma g /100g-ban/:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"maltitol\" name=\"maltitol\" class=\"form-control\" value=\"".$mymaterials['maltitol']."\"/>
										</div>
									</div>";
								
									echo"<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"cukoralkohol\">Szénhidrát cukoralkohol tartalma g /100g-ban/:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"cukoralkohol\" name=\"cukoralkohol\" class=\"form-control\" value=\"".$mymaterials['cukoralkohol']."\"/>
										</div>
									</div>";
								
									echo"<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"poliol\">Szénhidrát poliol tartalma g /100g-ban/:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"poliol\" name=\"poliol\" class=\"form-control\" value=\"".$mymaterials['poliol']."\"/>
										</div>
									</div>";
								
									echo"<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"koleszterin\">Koleszterin g /100g-ban/:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"koleszterin\" name=\"koleszterin\" class=\"form-control\" value=\"".$mymaterials['koleszterin']."\"/>
										</div>
									</div>";
								
									echo"<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"rost\">Rost g /100g-ban/:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"rost\" name=\"rost\" class=\"form-control\" value=\"".$mymaterials['rost']."\"/>
										</div>
									</div>";
								
									echo"<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"so\">Só g /100g-ban/:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"so\" name=\"so\" class=\"form-control\" value=\"".$mymaterials['so']."\"/>
										</div>
									</div>";
								
								echo"</div>	";
								
								echo"<div style=\"clear:both;height:10px;\"></div>";
								
								echo"<div class=\"form-group form-actions\">
									<div class=\"col-md-9 col-md-offset-3\">
										<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditmaterials('".$mymaterials['id']."')\"><i class=\"fa fa-angle-right\"></i> Ment</button>
									</div>
								</div>";
								
							echo"</div>";
						echo"</form>";
						?>	
					</div>
					<div class="tab-pane" id="tab-9">
						<?
						echo "<form id=\"editbaseprice\" name=\"editbaseprice\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
							echo "<div style=\"margin-top:1px;\">";
								
								echo"<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"baseprice\">Almértékegységre vetített ár - Ft/(".$mertek."):</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"baseprice\" name=\"baseprice\" class=\"form-control\"   value=\"".round($mymaterials['baseprice'],2)."\" />																		
									</div>
								</div>";	
												
										
								echo"<div style=\"clear:both;height:10px;\"></div>";
								
								echo"<div class=\"form-group form-actions\">
									<div class=\"col-md-9 col-md-offset-3\">
										<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"savebasepricebutton\" onclick=\"savebaseprice('".$mymaterials['id']."')\"><i class=\"fa fa-angle-right\"></i> Ment</button>
									</div>
								</div>";
								
							echo"</div>";
						echo"</form>";
						?>	
					</div>
					<div class="tab-pane" id="tab-11">
						<?
						echo "<form id=\"editdealprice\" name=\"editdealprice\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
							echo "<div style=\"margin-top:1px;\">";
								
								echo"<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"dealprice\">Kialkudott ár - Ft/(".$mertek."):</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"dealprice\" name=\"dealprice\" class=\"form-control\"   value=\"".round($mymaterials['dealprice'],2)."\" />																		
									</div>
								</div>";	
												
										
								echo"<div style=\"clear:both;height:10px;\"></div>";
								
								echo"<div class=\"form-group form-actions\">
									<div class=\"col-md-9 col-md-offset-3\">
										<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"savebasepricebutton\" onclick=\"savedealprice('".$mymaterials['id']."')\"><i class=\"fa fa-angle-right\"></i> Ment</button>
									</div>
								</div>";
								
							echo"</div>";
						echo"</form>";
						?>	
					</div>
					<div class="tab-pane" id="tab-10">
						<?
						echo "<form id=\"editstoreform\" name=\"editstoreform\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
							echo "<div style=\"margin-top:1px;\">";
								
								echo"<div class=\"form-group\" style=\"width:300px;float:left;\">
										<label class=\"col-md-3 control-label\" for=\"tetel2\">Tétel</label>
										<div class=\"col-md-9\">
											<select id=\"tetel2\" name=\"tetel2\" class=\"form-control\"  >
												<option value=\"0\">Kérlek válassz</option>";
												
													$melem=db_all("select id,lot,mmi,units from alapanyag where agt_id='".$mymaterials['id']."' and consumed='0000-00-00 00:00:00'");
													for($i=0;$i<count($melem);$i++){
														$mennyi=db_one("select sum(amount) from alapag_events where  agt_id='".$mymaterials['id']."' and ag_id='".$melem[$i]['id']."' and (event=1 or event=3 or event=7 or event=6)");
														echo"<option value=\"".$melem[$i]['id']."\" >".$melem[$i]['lot']." - ".$melem[$i]['mmi']." (".round($mennyi,5).")</option>";
													}
												
											echo"</select>
										</div>
									</div>";
								
								echo"<div class=\"form-group\" style=\"width:300px;float:left;\">
										<label class=\"col-md-3 control-label\" for=\"tetel2\">Raktár</label>
										<div class=\"col-md-9\">
											<select id=\"store\" name=\"store\" class=\"form-control\"  >
												<option value=\"0\">Kérlek válassz</option>";
												
													 $stores=db_all("select * from stores where status=1 order by name asc");
													for($i=0;$i<count($stores);$i++){
														echo"<option value=\"".$stores[$i]['id']."\" >".$stores[$i]['name']."</option>";
													}
												
											echo"</select>
										</div>
									</div>";
								
								
								echo"<div class=\"form-group\" style=\"width:300px;float:left;\">
								<label class=\"col-md-3 control-label\" for=\"mennyiseg\">Készleten:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"mennyiseg\" name=\"mennyiseg\" class=\"form-control\"   placeholder=\"írd be a készleten levő mennyiséget\"  />																		
									</div>
								</div>";	
								
								echo"<div class=\"form-group\" style=\"width:300px;float:left;\">
								<label class=\"col-md-3 control-label\" for=\"megjegyzes\">Megjegyzés:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"megjegyzes\" name=\"megjegyzes\" class=\"form-control\"   placeholder=\"Megjegyzés\"  />																		
									</div>
								</div>";	
									
												
										
								echo"<div style=\"clear:both;height:10px;\"></div>";
								
								echo"<div class=\"form-group form-actions\">
									<div class=\"col-md-9\">
										<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"savebasepricebutton1\" onclick=\"saveeditstoreform('".$mymaterials['id']."')\"><i class=\"fa fa-angle-right\"></i> Ment</button>
										<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"savebasepricebutton2\" onclick=\"saveconsumform('".$mymaterials['id']."')\"><i class=\"fa fa-angle-right\"></i> Kivezetés</button>
									</div>
								</div>";
								$stores=db_all("select * from stores order by name");
								echo "<h1>".$mymaterials['name']."</h1><br />";
									$alapanyagok=db_all("select * from alapanyag where agt_id='".$mymaterials['id']."' order by mmi desc,id desc");
										echo"<table class=\"table table-vcenter table-striped\">";
										echo"<thead>";
												echo"<tr>";
													echo"<th>Alapnyag Tétel</th>";
													echo"<th>Kivezetve</th>";
													for($c=0;$c<count($stores);$c++){
														echo"<th title=\"".$stores[$c]['name']."\">".$stores[$c]['id']."</th>";
													}
													echo"<th>0</th>";
													echo"<th>Összesen</th>";
												echo"</tr>";
											echo"</thead>";
											echo"<tbody >";
									for($b=0;$b<count($alapanyagok);$b++){
										$bco="";
										$ret="";
										$regi=0;
										if($alapanyagok[$b]['consumed']!='0000-00-00 00:00:00'){
											$bco=" style=\"background-color:#B7B7FF;\"";	
											$ret=" style=\"cursor:pointer;\"  onclick=\"javascript:retetel('".$mymaterials['id']."','".$alapanyagok[$b]['id']."');\" ";
										}
										$lastevent=db_row("select * from alapag_events where ag_id='".$alapanyagok[$b]['id']."' order by rdate desc limit 1");
										if($alapanyagok[$b]['consumed']=='0000-00-00 00:00:00'){
											//$lastevent=db_row("select * from alapag_events where ag_id='".$alapanyagok[$b]['id']."' order by rdate desc limit 1");
											if(time()-strtotime($lastevent['rdate'])>(86400*30)){
												$regi=1;
												$stx="";
												if($lastevent['event']==3){
													$stx=" - Raktárkiadás";	
												}
												$bco=" style=\"background-color:#FFB6BB;\"";
												$alapanyagok[$b]['consumed']="Utolsó esemény: ".$lastevent['rdate'].$stx;	
											}
												
										}
										echo"<tr ".$bco.">";
											echo"<td ".$ret."><a href=\"/modules/goods/alapag/".$alapanyagok[$b]['id']."#tab-6\">".$alapanyagok[$b]['id']."--".$alapanyagok[$b]['lot']."--".$alapanyagok[$b]['mmi']."</a></td>";
											echo"<td>".$alapanyagok[$b]['consumed']."</td>";
											$mymennyi=0;
											for($c=0;$c<count($stores);$c++){
												$mennyi=db_one("select sum(amount) from alapag_events where   ag_id='".$alapanyagok[$b]['id']."' and (event=1 or event=3 or event=7 or event=6) and store_id='".$stores[$c]['id']."' " );
												$mymennyi=$mymennyi+round($mennyi,5);
												echo"<td>".round($mennyi,5)."</td>";
											}
											$mennyi2=db_one("select sum(amount) from alapag_events where   ag_id='".$alapanyagok[$b]['id']."' and (event=1 or event=3 or event=7 or event=6) and store_id='0' " );
											echo"<td>".round($mennyi2,3)."</td>";
											$mymennyi=$mymennyi+$mennyi2;
											
											if($mymennyi==0 && $regi==1){
												//$bele="style=\"cursor:pointer;\"  onclick=\"javascript:doconsumed('".$mymaterials['id']."','".$alapanyagok[$b]['id']."');\" ";	
												db_execute("update alapanyag set consumed='".$lastevent['rdate']."' where id='".$alapanyagok[$b]['id']."'");	
											}
											echo"<td>".$mymennyi."</td>";
										echo"</tr>";
									}
									
									echo"</tbody>";
									echo"</table>";
									echo"<div style=\"clear:both;\"></div>";
								
							echo"</div>";
						echo"</form>";
						?>	
					</div>
					<div class="tab-pane" id="tab-3">
							<?
								echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mymaterials['id'],"Dokumentum feltöltés")."</div>";
							?> 
						   <div id="filelists"></div>
						   <div style="clear:both;height:1px;"></div>
					</div>
					<div class="tab-pane" id="tab-4">
							<?
								echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mymaterials['id'],"Hozzászólások")."</div>";
							?>
					</div>
					<div class="tab-pane" id="tab-6">
						<h3>Ezen terméktípusokban van az alapanyagból:</h3>
							<?
								$pts=db_all("select * from pt_x_agt where agt_id='".$mymaterials['id']."' group by pt_id");
								if(count($pts)>0){
									for($i=0;$i<count($pts);$i++){
										$product=db_row("select * from product_types where id='".$pts[$i]['pt_id']."'");
										echo"<div><a href=\"/modules/goods/producttype/".$product['id']."\">".$product['name']."</a></div>";	
									}
								}
								else{
									echo"<div>Jelenleg nincs egy terméktípusban sem összetevőként felhasználva!</div>";	
								}
								$pts=db_all("select * from pt_darabaru where agt_id='".$mymaterials['id']."' group by pt_id");
								if(count($pts)>0){
									for($i=0;$i<count($pts);$i++){
										$product=db_row("select * from product_types where id='".$pts[$i]['pt_id']."'");
										echo"<div><a href=\"/modules/goods/producttype/".$product['id']."\">".$product['name']."</a></div>";	
									}
								}
								else{
									echo"<div>Jelenleg nincs egy terméktípusban darabáruként sem felhasználva!</div>";	
								}
							?>
					</div>
					<div class="tab-pane" id="tab-5">
						<div id="printdiv"></div>
					</div>
					<div class="tab-pane" id="tab-8">
						 <div class="col-sm-6 col-lg-3">
							<!-- Widget -->
							<a href="#" class="widget widget-hover-effect1">
								<div class="widget-simple" style="width:300px;">
									<div class="widget-icon pull-left themed-background animation-fadeIn" style="width:200px;">
										<?=round($summennyiseg,2);?> Kg
									</div>
								</div>
							</a>
							<!-- END Widget -->
						</div>
						<div style="clear:both;height:10px;"></div>
						<div class="block hidden-lt-ie9">
							<!-- Switches Title -->
							<div class="block-title">
								<h2><strong>Szűrő</strong> </h2>
							</div>
							<?
						   echo"<div class=\"form-group\" >
									
									<div class=\"col-md-2\">
										<select id=\"event_id\" name=\"event_id\" class=\"form-control\"  onchange=\"javascript:showalapagstory('".$mymaterials['id']."','elementdiv3');	\" >
											<option value=\"0\" selected>Minden esemény</option>";
											
											echo"<option value=\"1\" >Bevételezés</option>";
											echo"<option value=\"3\" >Raktárkiadás</option>";
											echo"<option value=\"6\" >Raktárkorrekció</option>";
										echo"</select>
									</div>
									
									<div class=\"col-md-2\">
										<select id=\"limit\" name=\"limit\" class=\"form-control\"  onchange=\"javascript:showalapagstory('".$mymaterials['id']."','elementdiv3');	\" >
											<option value=\"10\" selected>utolsó 10 bejegyzés</option>";
											echo"<option value=\"50\" >utolsó 50 bejegyzés</option>";
											echo"<option value=\"100\" >utolsó 100 bejegyzés</option>";
											echo"<option value=\"0\" >Összes bejegyzés</option>";
										echo"</select>
									</div>
									<div class=\"col-md-2\">
										<input type=\"text\" id=\"matstdate\" name=\"matstdate\" class=\"form-control input-datepicker\" placeholder=\"Intervallum eleje\"  />			
									</div>
									
									<div class=\"col-md-2\">
										<input type=\"text\" id=\"matendate\" name=\"matendate\" class=\"form-control input-datepicker\" placeholder=\"Intervallum vége\"  />			
									</div>
									<div class=\"col-md-2\">
										<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"showalapagstory('".$mymaterials['id']."','elementdiv3');\">Szűr</button>
									</div>
								</div>";
						   
								?>	
								<div style="clear:both;height:10px;"></div>
						</div>	
						
						<div style="clear:both;height:10px;"></div>
						<div class="block hidden-lt-ie9">
							<!-- Switches Title -->
							<div class="block-title">
								<h2><strong>Történet</strong> </h2>
							</div>
							
							<!-- END Switches Title -->

							<div id="elementdiv3"></div>
						</div>		
					
					</div>
					
					<div class="tab-pane" id="tab-7">
						<h3>Beszállítói források</h3>
						<?
							$partners=db_all("select * from partners where status=1 order by name asc");
						?>
						
						   <div class="block hidden-lt-ie9 " >
							<!-- Switches Title -->
							<div class="block-title">
								<h2><strong>Új beszállító</strong> </h2>
							</div>
							<!-- END Switches Title -->

							<!-- Switches Content -->
							<form  id="newelementformbesz" id="newelementformbesz" class="form-horizontal form-bordered" onsubmit="return false;">
			
								<div class="form-group" style="width:250px;float:left;">
									<label class="col-md-3 control-label" for="gyarto_id">Gyártó</label>
									<div class="col-md-9">
										<select id="gyarto_id" name="gyarto_id" class="form-control" >
											<option value="0">Kérlek válassz</option>
											<?
												for($i=0;$i<count($partners);$i++){
													echo"<option value=\"".$partners[$i]['id']."\">".$partners[$i]['name']."</option>";	
												}
											?>
										</select>
									</div>
								</div>
								
								<div class="form-group" style="width:250px;float:left;">
									<label class="col-md-3 control-label" for="forgalmazo_id">Forgalmazó</label>
									<div class="col-md-9">
										<select id="forgalmazo_id" name="forgalmazo_id" class="form-control" >
											<option value="0">Kérlek válassz</option>
											<?
												for($i=0;$i<count($partners);$i++){
													echo"<option value=\"".$partners[$i]['id']."\">".$partners[$i]['name']."</option>";	
												}
											?>
										</select>
									</div>
								</div>
								
								<div class="form-group" style="width:250px;float:left;">
									<label class="col-md-3 control-label" for="beszallito_id">Beszállító</label>
									<div class="col-md-9">
										<select id="beszallito_id" name="beszallito_id" class="form-control" >
											<option value="0">Kérlek válassz</option>
											<?
												for($i=0;$i<count($partners);$i++){
													echo"<option value=\"".$partners[$i]['id']."\">".$partners[$i]['name']."</option>";	
												}
											?>
										</select>
									</div>
								</div>

								<div class="form-group" style="width:250px;float:left;">
									<label class="col-md-3 control-label" for="specifikacio">Spec?</label>
									<div class="col-md-9">
										<select id="spec" name="spec" class="form-control" >
											<option value="0">Nem</option>
											<option value="1">igen</option>
										</select>
									</div>
								</div>

								
								<div style="clear:both;"></div>
								<div style="clear:both;height:10px;"></div>
								<div class="col-md-9 col-md-offset-0">
									
								<button type="button" class="btn btn-sm btn-primary" id="savenewbutton" onclick="javascript:addbeszelement(<?=$mymaterials['id'];?>,'elementdiv2');"><i class="fa fa-angle-right"></i> Ment</button>
								</div>
								<div style="clear:both;"></div>
							</form>	
							<div style="clear:both;height:10px;"></div>
				
						</div>
						<div class="block hidden-lt-ie9">
							<!-- Switches Title -->
							<div class="block-title">
								<h2><strong>Beszállítói lista</strong> </h2>
							</div>
							<!-- END Switches Title -->

							<div id="elementdiv2"></div>
						</div>		

					<div style="clear:both;"></div>
					</div>
					
				<!-- END Tabs Content -->
			</div>
			<!-- END Block Tabs -->
				
		</div>
		</div>
		
		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>
		<script src="js/ckeditor/ckeditor.js"></script>
		<script src="/modules/goods/js/inc_materials.js"></script>

		<script >
			showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mymaterials['id'];?>,'Dokumentum feltöltés','filelists');
			showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$mymaterials['id'];?>,'Dokumentum feltöltés','filelists');
			showbeszelement('<?=$mymaterials['id'];?>','elementdiv1');			
			showbeszelement('<?=$mymaterials['id'];?>','elementdiv2');	
			showalapagstory('<?=$mymaterials['id'];?>','elementdiv3');	
		</script>
		<script>
			$(function(){
			// materialsDatatables.init(); 
	
			});
		</script>

		<?php include 'inc/template_end.php'; ?>		
<?
		}
	}
}
else{
	//print_r($_SESSION['planetsys'])
	$alaps=db_all("select * from alapag_tipus where status=1");
	for($a=0;$a<count($alaps);$a++){
		getalap($alaps[$a]['id']);
	}
?>    
<!-- Ez a lista  -->
		<ul class="breadcrumb breadcrumb-top">
			<li><a href="/">Rendszer</a></li>
			<li><a href="/<?=$URI;?>">Alapanyagok</a></li>
		</ul>
		<!-- END Datatables Header -->

		<!-- Datatables Content -->
		<div class="block full">
			<div class="block-title">
				<h2><strong>Alapanyagok</strong> listája</h2>
			</div>

			<div class="table-responsive">
				<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
					<thead>
						<tr>
						<?
						foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
							if($screen['elements'][$f]['textname']!=""){
								echo"<th>".$screen['elements'][$f]['textname']."</th>";
							}
						}
						//echo"<th>Művelet</th>";
						?>
						</tr>
					</thead>
					<tbody ></tbody>
				</table>
			</div>
		</div>
		<!-- END Datatables Content -->
	</div>
	<!-- END Page Content -->

	<?php include 'inc/page_footer.php'; ?>
	<?php include 'inc/template_scripts.php'; ?>

	<!-- Load and execute javascript code used only in this page -->
	<script src="modules/goods/js/inc_materials.js"></script>
	<script>$(function(){ MaterialsDatatables.init(); });</script>

	<?php include 'inc/template_end.php'; ?>
<?
}
?>
