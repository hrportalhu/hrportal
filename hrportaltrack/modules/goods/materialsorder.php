
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Alapanyag rendelés
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/goods/materialsorder">Alapanyag rendelés</a></li>
				<li><a href="/new">Új rendelés</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új rendelés</strong></h2>
				</div>
			<?
				
				
				echo "<form id=\"newmaterialsorder\" name=\"newmaterialsorder\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";
							
						
			
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"order_date\">Megrendelés ideje:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"order_date\" name=\"order_date\" class=\"form-control input-datepicker\" value=\"".date("Y-m-d",time())."\" />
							</div>
						</div>";
			
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"want_date\">Beszállítás ideje:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"want_date\" name=\"want_date\" class=\"form-control input-datepicker\" value=\"".date("Y-m-d",time()+(86400*3))."\" />
							</div>
						</div>";
						
										

						
						echo "<div style=\"clear:both;height:10px;\"></div>";
						
						echo "<div class=\"form-group form-actions\">
							<div class=\"col-md-6 col-md-offset-3\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewmaterialsorder();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>";
						
				echo "</div>
				</form>";
			?>
			</div>	
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/goods/js/inc_materialsorder.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?	
		}
		else{

			//echo "select * from functions where id='".$uritags[2]."'";
			$mymaterialsorder=db_row("select * from materials_orders_header where id='".$uritags[3]."'");
			if($mymaterialsorder['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
				
?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="modules/goods/materialsorder"> Rendelések </a></li>
				<li><a href="/<?=$URI;?>"><?=$mymaterialsorder['name'];?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->

					<div class="block-title">
						<h2><strong><?=$mymaterialsorder['name'];?> </strong> <?=$mymaterialsorder['erkezett'];?> </h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
							if(priv("materialsorder","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("materialsorder","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							if(priv("materialsorder","elements")){echo"<li><a href=\"#tab-6\" data-toggle=\"tooltip\" title=\"Rendelési tételek\"><i class=\"fa fa-lightbulb-o\"></i></a></li>";}
							if(priv("materialsorder","aproved")){echo"<li><a href=\"#tab-7\" data-toggle=\"tooltip\" title=\"Rendelés jóváhagyás\" onclick=\"showorderaprove('".$mymaterialsorder['id']."');\"><i class=\"gi gi-turtle\"></i></a></li>";}
							if(priv("materialsorder","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("materialsorder","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
							//if(priv("materialsorder","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}	
							if(priv("materialsorder","print")){echo"<li><a href=\"#tab-99\" data-toggle=\"tooltip\" title=\"Történet\" onclick=\"javascript:showstory('".$mymaterialsorder['id']."');\"><i class=\"gi gi-history\"></i></a></li>";}			
							if(priv("materialsorder","print")){echo"<li onclick=\"javascript:getprintscreen('".$_SESSION['planetsys']['actpage']['modulname']."','visitorprint','".$mymaterialsorder['id']."');\" ><a href=\"#modal-regular-print\" data-toggle=\"modal\" title=\"Nyomtatás\"><i class=\"fa fa-print\" ></i></a></li>";}
							?>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="col-md-6">
								<dl class="dl-horizontal">
									<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$mymaterialsorder['id']); ?></dd>
									<dt>Rendelés ideje:</dt><dd><? echo $mymaterialsorder['order_date']; ?></dd>
									<dt>Beszállítás ideje:</dt><dd><? echo $mymaterialsorder['want_date']; ?></dd>
									<dt>Partnerek:</dt><dd><? echo $mymaterialsorder['partners']; ?></dd>
									<dt>Rögzítő:</dt><dd><? echo idchange("users","realname",$mymaterialsorder['user_id']); ?></dd>
									<dt>Státusz:</dt><dd><? 
										if($mymaterialsorder['last_ins']=="0000-00-00 00:00:00"){
											echo"Rögzítés alatt";
										}
										else{
											if($mymaterialsorder['aproved_date']=="0000-00-00 00:00:00"){
												echo"Rögzítés lezárva, jóváhagyásra vár";
											}
											else{
												if($mymaterialsorder['send_date']=="0000-00-00 00:00:00"){
													echo"Rögzítés lezárva, jóváhagyva, küldésre vár";
												}
												else{
													echo"Rögzítés lezárva, jóváhagyva, küldve";
												}
											}
										} 
									?></dd>
								</dl>	
							</div>
							
							<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-2">
						<?
							
							
							echo"<form id=\"editmaterialsorder\" name=\"editmaterialsorder\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
							echo "<div style=\"margin-top:1px;\">";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"order_date\">Megrendelés ideje:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"order_date\" name=\"order_date\" class=\"form-control input-datepicker\" value=\"".$mymaterialsorder['order_date']."\" />
							</div>
						</div>";
			
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"want_date\">Beszállítás ideje:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"want_date\" name=\"want_date\" class=\"form-control input-datepicker\" value=\"".$mymaterialsorder['want_date']."\" />
							</div>
						</div>";	
								
							
								echo "<div class=\"form-group form-actions\">
									<div class=\"col-md-6 col-md-offset-3\">
										<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditmaterialsorder(".$mymaterialsorder['id'].")\"><i class=\"fa fa-angle-right\"></i> Ment</button>
									</div>
								</div>";
									
							echo "</div>
							</form>";
						?>
						</div>

						<div class="tab-pane" id="tab-3">
								<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mymaterialsorder['id'],"Dokumentum feltöltés")."</div>";
								?> 
								<div id="filelists"></div>
								<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mymaterialsorder['id'],"Hozzászólások")."</div>";
								?>
						</div>
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>
						<div class="tab-pane" id="tab-99">
							<div id="storydiv"></div>
						</div>
						<div class="tab-pane" id="tab-6">
							<div class="block">
								<div class="block-title">
									<h2><strong>Rendelési tételek</strong></h2>
								</div>
								<div class="table-responsive">
									<table class="table table-vcenter table-striped">
									<thead>
										<tr>
											<th style="width: 100px;" class="text-center"><i class="gi gi-disk_save"></i></th>
											<th class="text-center" style="width:200px;">Alapanyag</th>
											<th class="text-center">Készleten</th>
											<th class="text-center">Rendelés</th>
											<th class="text-center">MEE</th>
											<th class="text-center">Állapot</th>
											<th class="text-center" style="width: 130px;">Művelet</th>
											
										</tr>
									</thead>
									<tbody  id="ordercont"></tbody>
									</table>		
								</div>
								<?
								if($mymaterialsorder['last_ins']=="0000-00-00 00:00:00"){
									echo "<div class=\"form-group form-actions\">
										<div class=\"col-md-6\">
											<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"closematerialsorder(".$mymaterialsorder['id'].")\">Lezár</button>
										</div>
									</div>";
								}
								?>
							</div>
						</div>
						<div class="tab-pane" id="tab-7">
							<div class="block">
								<div class="block-title">
									<h2><strong>Rendelés jóváhagyás</strong></h2>
								</div>
								<div class="table-responsive">
									<table class="table table-vcenter table-striped">
									<thead>
										<tr>
											<th style="width: 100px;" class="text-center"><i class="gi gi-disk_save"></i></th>
											<th class="text-center" style="width:200px;">Alapanyag</th>
											<th class="text-center">Készletinformációk</th>
											<th class="text-center">Rendelés</th>
											<th class="text-center">MEE</th>
											<th class="text-center">Beszállító</th>
											<th class="text-center">Állapot</th>
											<th class="text-center" style="width: 130px;">Művelet</th>
											
										</tr>
									</thead>
									<tbody  id="orderaprovecont"></tbody>
									</table>		
								</div>

							</div>
						</div>
					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
					
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/goods/js/inc_materialsorder.js"></script>

			<script >
				showorderaprove('<?=$mymaterialsorder['id'];?>');
				showordertable('<?=$mymaterialsorder['id'];?>');
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mymaterialsorder['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$mymaterialsorder['id'];?>,'Dokumentum feltöltés','filelists');
			</script>
			<script>$(function(){ materialsorderDatatables.init(); });</script>	
<?
					if(substr($uritags[4],0,4)=="tab-"){
						?>
						<script>
							var activeTab = $('[href=#<?=$uritags[4];?>]');
							activeTab && activeTab.tab('show');
						</script>	
					
						<?
					}
					if(substr($uritags[5],0,4)=="tab-"){
						?>
						<script>
							var activeTab = $('[href=#<?=$uritags[5];?>]');
							activeTab && activeTab.tab('show');
						</script>	
					
						<?
					}
				?>
			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
		//print_r($_SESSION['planetsys'])
?>    
<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Rendelések</a></li>
			</ul>
			<!-- END Datatables Header -->
		
				
				
			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Rendelési</strong> lista</h2>
				</div>
				
				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							//echo"<th>Művelet</th>";
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/goods/js/inc_materialsorder.js"></script>
		<script>$(function(){ materialsorderDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>


