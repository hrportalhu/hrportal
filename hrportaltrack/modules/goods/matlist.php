<?
    $screen=getscreenelems('goods','alapag');
    getlistoptions($screen);
    $screen['screenlist']=$_SESSION['planetsys']['listoption']['screenlist'];
    $screen['screendb']=$_SESSION['planetsys']['screendb'];

    naplo('info','user','Alapanyag listázó  betöltése');
    $_SESSION['planetsys']['actpage']=$screen;

?>
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Alapanyag listázó
            </h1>
        </div>
    </div>
<?

		if(isset($uritags[3]) && $uritags[3]!=""){
			if($uritags[3]=="new"){
				?>
				<!-- Ez egy elem  -->		
				<ul class="breadcrumb breadcrumb-top">
					<li><a href="/">Rendszer</a></li>
					<li><a href="/moduls/goods/matlist">Alapanyag listázó</a></li>
					<li><a href="/new">Új alapanyag felvitele</a></li>
				</ul>
				<div class="block full">
				<?
					echo "<div style=\"margin-top:1px;\">
							<div class='inputelem'>
								<span></span>
								<span style=\"font-size:14px;\">Új alapanyagtípus</span>
							</div>".genFormJ("newalapanyag",$_SESSION['planetsys']['actpage'],"",$_SESSION['planetsys']['screencmd']).	"
							<div class=\"form-group form-actions\">
								<div class=\"col-md-9 col-md-offset-3\">
									<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"savenewbutton\" onclick=\"savenewscreenmatlist();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
								</div>
							</div>
							<div style=\"clear:both;\"></div>";
				?>
					</div>	
				</div>	
				<?php include 'inc/page_footer.php'; ?>
				<?php include 'inc/template_scripts.php'; ?>
				<script src="js/pages/inc_matlist.js"></script>
				<?php include 'inc/template_end.php'; ?>	
				<?	
			}
			else{
				
				//echo "select * from functions where id='".$uritags[2]."'";
				$mymatlist=db_row("select * from alapanyag where id='".$uritags[3]."'");
				$agt=db_row("select * from alapag_tipus where id='".$mymatlist['agt_id']."'");
				if($mymatlist['id']==''){
					die("Helytelen hozzáférés!");
				}
				else{
	?>
				<!-- Ez egy elem  -->		
				<ul class="breadcrumb breadcrumb-top">
					<li><a href="/">Rendszer</a></li>
					<li><a href="moduls/goods/matlist">Alapanyag</a></li>
					<li><a href="/<?=$URI;?>"><?=$mymatlist['name'];?></a></li>
				</ul>
				<div class="block full">
								   
						<!-- Block Tabs Title -->

						<div class="block-title">
							<h2><strong><?=$agt['name'];?> </strong>LOT: <?=$mymatlist['lot'];?>  alapanyag oldala</h2>
							<ul class="nav nav-tabs" data-toggle="tabs">
								<li class="active"><a href="#tab-1" data-toggle="tooltip" title="view"><i class="gi gi-eye_open"></i></a></li>
								<li><a href="#tab-2" data-toggle="tooltip" title="edit"><i class="gi gi-pencil"></i></a></li>
								<li><a href="#tab-3" data-toggle="tooltip" title="upload"><i class="gi gi-floppy_open"></i></a></li>
								<li><a href="#tab-4" data-toggle="tooltip" title="comments"><i class="fa fa-comments-o"></i></a></li>
								<li><a href="#tab-5" data-toggle="tooltip" title="print"><i class="fa fa-print"></i></a></li>
								<li><a href="#tab-6" data-toggle="tooltip" title="story"><i class="fa fa-tasks"></i></a></li>
								<li><a href="#tab-7" data-toggle="tooltip" title="tracking"><i class="fa fa-eye"></i></a></li>
								
								
							</ul>
						</div>
						<!-- END Block Tabs Title -->

						<!-- Tabs Content -->
						<div class="tab-content">
							<div class="tab-pane active" id="tab-1">
								<div class="col-md-6">
									<dl class="dl-horizontal">
										<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$mymatlist['id']); ?></dd>
										<?
											echo showRecord($_SESSION['planetsys']['actpage'],$mymatlist)."<br />";
										?>
									</dl>	
								
								</div>
								<div style="clear:both;height:1px;"></div>
							</div>
							<div class="tab-pane" id="tab-3">
							<?
								echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mymatlist['id'],"Dokumentum feltöltés")."</div>";
							?> 
								<div id="filelists"></div>
								<div style="clear:both;height:1px;"></div>
							</div>
							<div class="tab-pane" id="tab-4">
							<?
								echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mymatlist['id'],"Hozzászólások")."</div>";
							?>
							</div>
							<div class="tab-pane" id="tab-5">
								<div id="printdiv"></div>
							
							</div>
					
							<div class="tab-pane" id="tab-6">
								<?
									include("ajax_alapag_hist.php");
								?>
						
							
							</div>
					
							<div class="tab-pane" id="tab-7">
								<?
									include("ajax_alapag_track.php");
								?>
								
							
							</div>
					
							
								<div class="tab-pane" id="tab-2">
								<?
									echo "<div style=\"margin-top:1px;\">
											<div class='inputelem'>
												<span>Sorszám: </span>
												<span style=\"font-size:14px;\">".sprintf('%1$08d',$mymatlist['id'])."</span>
											</div>";
											echo genFormJ("editmatlist",$_SESSION['planetsys']['actpage'],$mymatlist,$_SESSION['planetsys']['screencmd']);
											echo"<div class=\"form-group form-actions\">
												<div class=\"col-md-9 col-md-offset-3\">
													<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditscreenmatlists(".$mymatlist['id'].")\"><i class=\"fa fa-angle-right\"></i> Ment</button>
												</div>
											</div>
											
											<div style=\"clear:both;\"></div></div>";
								?>
							</div>
							</div>
						</div>
						
						<!-- END Tabs Content -->
					</div>
					<!-- END Block Tabs -->
						
				</div>
				
				<?php include 'inc/page_footer.php'; ?>
				<?php include 'inc/template_scripts.php'; ?>
				<script src="/js/pages/inc_matlist.js"></script>

				<script >
					showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mymatlist['id'];?>,'Dokumentum feltöltés','filelists');
					showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mymatlist['id'];?>,'Dokumentum feltöltés','filelists');
								
			
				</script>
				<script>$(function(){ MatlistDatatables.init(); });</script>	
			

				<?php include 'inc/template_end.php'; ?>		
	<?
				}
			}
		}
		else{
			
?>    
	<!-- Ez a lista  -->
				<ul class="breadcrumb breadcrumb-top">
					<li><a href="/">Rendszer</a></li>
					<li><a href="/<?=$URI;?>">Alapanyag </a></li>
				</ul>
				<!-- END Datatables Header -->

				<!-- Datatables Content -->
				<div class="block full">
					<div class="block-title">
						<h2><strong>Alapanyag </strong> lista</h2>
					</div>
					

					<div class="table-responsive">
						<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
							<thead>
								<tr>
									<?
										foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
											if($screen['elements'][$f]['textname']!=""){
												echo"<th>".$screen['elements'][$f]['textname']."</th>";
											}
										}
										//echo"<th>Művelet</th>";
										?>
								</tr>
							</thead>
							<tbody ></tbody>
						</table>
					</div>
				</div>
				<!-- END Datatables Content -->
			</div>
			<!-- END Page Content -->



			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>

			<!-- Load and execute javascript code used only in this page -->
			<script src="js/pages/inc_matlist.js"></script>
			<script>$(function(){ MatlistDatatables.init(); });</script>

			<?php include 'inc/template_end.php'; ?>
<?
}
?>

