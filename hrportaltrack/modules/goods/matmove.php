<?

    naplo('info','user','Kiadás kezelő  betöltése');
    $_SESSION['planetsys']['actpage']=$screen;
    $munkahelyek=db_all("select id,name from gw_posts where status=1");
    $stores=db_all("select * from stores where status=1 order by name asc");
?>
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Alapanyag mozgatás raktárak között
            </h1>
        </div>
    </div>
<?


			
		?>
		<!-- Ez egy elem  -->		
		<ul class="breadcrumb breadcrumb-top">
			<li><a href="/">Rendszer</a></li>
			<li><a href="/modules/goods/matmove">Alapanyag mozgatás</a></li>
			<li><a href="/new">Új mozgatás</a></li>
		</ul>
		<div class="block full">
			 <form  id="newelementform" id="newelementform" class="form-horizontal form-bordered" onsubmit="return false;">
				<div class="form-group" style="width:300px;float:left;">
					<label class="col-md-3 control-label" for="newitem">Alapanyag</label>
					<div class="col-md-9">
						<input type="text" id="newitem" name="newitem" class="form-control" value="" />
						<input type="hidden" id="newitem_id" name="newitem_id" value="" />
					</div>
				</div>
				<div class="form-group" style="width:300px;float:left;">
					<label class="col-md-3 control-label" for="tetel">Tétel</label>
					<div class="col-md-9">
						<select id="tetel" name="tetel" class="form-control" >
							<option value="0">Kérlek válassz</option>
						</select>
					</div>
				</div>
				<div class="form-group" style="width:300px;float:left;">
					<label class="col-md-3 control-label" for="amount">Mennyiség</label>
					<div class="col-md-9">
						<input type="text" id="amount" name="amount" class="form-control" value="" />
					</div>
				</div>
				<div class="form-group" style="width:300px;float:left;">
					<label class="col-md-3 control-label" for="amount">Dátum</label>
					<div class="col-md-9">
						<input type="text" id="rdate" name="rdate" class="form-control input-datepicker" value="<?=date("Y-m-d",strtotime("NOW"));?>" />
					</div>
				</div>
				
				<div class="form-group" style="width:300px;float:left;">
					<label class="col-md-3 control-label" for="store2">Raktárba</label>
					<div class="col-md-9">
						<select id="store2" name="store2" class="form-control"  >
							<option value="0">Kérlek válassz</option>
							<?
								for($i=0;$i<count($stores);$i++){
									
									echo"<option value=\"".$stores[$i]['id']."\" >".$stores[$i]['name']."</option>";	
								}
							?>
						</select>
					</div>
				</div>
				<div class="col-md-9 col-md-offset-0">					
					<button type="button" class="btn btn-sm btn-primary" id="savenewbutton" onclick="javascript:addmatmovelement();"><i class="fa fa-angle-right"></i> Felvesz</button>
				</div>
				<div style="clear:both;"></div>
			</form>	
			<div style="clear:both;height:10px;"></div>

		<!-- Datatables Content -->
		<div class="block full">
			<div class="block-title">
				<h2><strong>Alapanyagok</strong> listája</h2>
			</div>
			
			 
		</div>

		<div style='clear:both;height:1px;' ></div>

		</div>	
		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>
		<script src="modules/goods/js/inc_matmove.js"></script>

		
		<?php include 'inc/template_end.php'; ?>	

<?

?>
