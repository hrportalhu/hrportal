<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Termék kezelő
            </h1>
        </div>
    </div>
<?

		if(isset($uritags[3]) && $uritags[3]!=""){
			if($uritags[3]=="new"){
				?>
				<!-- Ez egy elem  -->		
				<ul class="breadcrumb breadcrumb-top">
					<li><a href="/">Rendszer</a></li>
					<li><a href="/modules/goods/products">Termék kezelő</a></li>
					<li><a href="/new">Új termék felvitele</a></li>
				</ul>
				<div class="block full">
					<div class="block-title">
						<h2><strong>Új termék felvitele</strong></h2>
					</div>
				<?
				
					$products=db_all("select id,name from product_types where status=1 order by name");
					$stores=db_all("select id,name from stores where status=1 order by name");
					
					echo"<form id=\"newproducts\" name=\"newproducts\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
					echo "<div style=\"margin-top:1px;\">
							
							<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"mypt_id\">Termék:</label>
								<div class=\"col-md-9\">
									<select id=\"mypt_id\" name=\"mypt_id\" class=\"form-control\" placeholder=\"Termék neve\"  onchange=\"javascript:getlotcode();getegyseg();\" >
										<option value=\"0\">Kérlek válassz</option>";
										for($i=0;$i<count($products);$i++){		
											echo"<option value=\"".$products[$i]['id']."\" >".$products[$i]['name']."</option>";	
										}
									echo"</select>";
								echo"</div>
							</div>
							
							<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"code\">LOT kód:</label>
								<div class=\"col-md-9\">
									<input type=\"text\" id=\"code\" name=\"code\" class=\"form-control\" placeholder=\"Késztermék LOT kódja\" />
																										
								</div>
							</div>
							
							<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"amount\">Mennyiség:</label>
								<div class=\"col-md-9\">
									<input type=\"text\" id=\"amount\" name=\"amount\" class=\"form-control\" placeholder=\"Késztermék mennyisége\" />	

									<span  id=\"meename\" class=\"help-block\"></span>																		
								</div>
							</div>
							
							<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"created\">Készítési ideje:</label>
								<div class=\"col-md-9\">
									<input type=\"text\" id=\"created\" name=\"created\" class=\"form-control\" placeholder=\"Elkészítéséhez szükséges idő\" "; ?> value="<?=date("Y-m-d",time());?>" <? echo"/>																		
								</div>
							</div>
							
							<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"store_id\">Raktár:</label>
								<div class=\"col-md-9\">
									<select id=\"store_id\" name=\"store_id\" class=\"form-control\" placeholder=\"Tárolási helye\"   >
										<option value=\"0\">Kérlek válassz</option>";
										for($i=0;$i<count($stores);$i++){		
											echo"<option value=\"".$stores[$i]['id']."\" >".$stores[$i]['name']."</option>";	
										}
									echo"</select>";
								echo"</div>
							</div>
							
							<div class=\"form-group\" >";
								echo"<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span><br />Státusz</label>";
							echo"</div>
							
							<div style=\"clear:both;height:10px;\"></div>
							
							<div class=\"form-group form-actions\">
								<div class=\"col-md-9 col-md-offset-3\">
									<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewproducts();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
								</div>
							</div>
							
						</div>
					</form>";
				?>
				</div>
				<?php include 'inc/page_footer.php'; ?>
				<?php include 'inc/template_scripts.php'; ?>
				<script src="modules/goods/js/inc_products.js"></script>
				<?php include 'inc/template_end.php'; ?>	
				<?	
			}
			else{
				
				//echo "select * from functions where id='".$uritags[2]."'";
				$myproducts=db_row("select * from products where id='".$uritags[3]."'");
				$pt=db_row("select * from product_types where id='".$myproducts['pt_id']."'");
				
				if($myproducts['id']==''){
					die("Helytelen hozzáférés!");
				}
				else{
	?>
				<!-- Ez egy elem  -->		
				<ul class="breadcrumb breadcrumb-top">
					<li><a href="/">Rendszer</a></li>
					<li><a href="modules/goods/products">Termék</a></li>
					<li><a href="/<?=$URI;?>"><?=$myproducts['name'];?></a></li>
				</ul>
				<div class="block full">
								   
						<!-- Block Tabs Title -->

						<div class="block-title">
							<h2><strong><?=$myproducts['name'];?></strong> termék oldal</h2>
							<ul class="nav nav-tabs" data-toggle="tabs">
								<?
								if(priv("products","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
								if(priv("products","comments")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
								if(priv("products","elements")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"elements\"><i class=\"fa fa-tasks\"></i></a></li>";}	
								if(priv("products","tracking")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"tracking\"><i class=\"fa fa-eye\"></i></a></li>";}	
								if(priv("products","tracking")){echo"<li onclick=\"javascript:getprintscreen('".$_SESSION['planetsys']['actpage']['modulname']."','producttrackprint','".$myproducts['id']."');\" ><a href=\"#modal-regular-print\" data-toggle=\"modal\" title=\"tracking print\"><i class=\"gi gi-cars\" ></i></a></li>";}
								?>
							</ul>
						</div>
						<!-- END Block Tabs Title -->

						<!-- Tabs Content -->
						<div class="tab-content">

							<div class="tab-pane active" id="tab-1">
								<div class="col-md-6">
									<dl class="dl-horizontal">
										<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$myproducts['id']); ?></dd>
										<dt>Név:</dt><dd><a href="/modules/goods/producttype/<?=$myproducts['pt_id'];?>" ><? echo idchange("product_types","name",$myproducts['pt_id']); ?></a></dd>
										<dt>LOT kód:</dt><dd><? echo $myproducts['code']; ?></dd>
										<?
										//$d=substr(myproducts['created'],0,10);
										$rendel=db_row("select * from orders_elements where termek_id='".$myproducts['pt_id']."' and szall_date='".substr($myproducts['created'],0,10)."'");
										//echo "select * from orders_element where termek_id='".$myproducts['pt_id']."' and szall_date='".substr($myproducts['created'],0,10)."'";	
										//echo "select * from orders_element where termek_id='".$myproducts['pt_id']."' and szall_date='".."'";
										//echo $myproducts['pt_id'];
										$pta=db_all("select * from pt_units where pt_id='".$myproducts['pt_id']."'");
										$gyartasi=db_row("select * from pt_units where pt_id='".$myproducts['pt_id']."' and units_type=2 and unit2_id=1");
										$gyartkilo=$myproducts['amount']*$gyartasi['mesure2'];
										$rendelesi=db_row("select * from pt_units where pt_id='".$myproducts['pt_id']."' and units_type=1 and unit2_id=1");
										$rendkilo=$rendelesi['mesure2'];
										echo"<dt>Gyártási mennyiség:</dt><dd>".round($myproducts['amount'],2)." ".idchange("mertekegyseg","name",$gyartasi['unit1_id'])."</dd>	";
										if($myproducts['id']=="39"){
											echo"<dt>Rendelési mennyiség:</dt><dd>465 ".idchange("mertekegyseg","name",$rendelesi['unit1_id'])."</dd>	";
										}
										else{
											echo"<dt>Rendelési mennyiség:</dt><dd>".$rendel['termek_darab']." ".idchange("mertekegyseg","name",$rendelesi['unit1_id'])."</dd>	";
										}
										?>
										<dt>Készítési idő:</dt><dd><? echo $myproducts['created']; ?></dd>
										<dt>Raktár:</dt><dd><? echo idchange("stores","name",$myproducts['store_id']); ?></dd>
										<dt>Érvényes?:</dt><dd><? if($myproducts['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
									</dl>	
								</div>
								<div style="clear:both;height:1px;"></div>
							</div>
							<div class="tab-pane" id="tab-2">
									<?
										echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$myproducts['id'],"Hozzászólások")."</div>";
									?>
							</div>
							
							<div class="tab-pane" id="tab-3">
									<?
										include("ajax_products_contents.php");
									?>
								<div style="clear:both;height:10px;"></div>
							
							</div>
							
							<div class="tab-pane" id="tab-4">
								<?
									include("ajax_products_track.php");
								?>
								<div style="clear:both;height:10px;"></div>
							</div>
						</div>
						<!-- END Tabs Content -->
					</div>
					<!-- END Block Tabs -->
						
				</div>
				
				<?php include 'inc/page_footer.php'; ?>
				<?php include 'inc/template_scripts.php'; ?>
				<script src="modules/goods/js/inc_products.js"></script>

				<script >
					showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$myproducts['id'];?>,'Dokumentum feltöltés','filelists');
					showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$myproducts['id'];?>,'Dokumentum feltöltés','filelists');
				</script>
				<script>$(function(){ ProductsDatatables.init(); });</script>	
			

				<?php include 'inc/template_end.php'; ?>		
	<?
				}
			}
		}
		else{
			//print_r($_SESSION['planetsys'])
	?>    
	<!-- Ez a lista  -->
				<ul class="breadcrumb breadcrumb-top">
					<li><a href="/">Rendszer</a></li>
					<li><a href="/<?=$URI;?>">Termék</a></li>
				</ul>
				<!-- END Datatables Header -->

				<!-- Datatables Content -->
				<div class="block full">
					<div class="block-title">
						<h2><strong>Termék</strong> lista</h2>
					</div>
					

					<div class="table-responsive">
						<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
							<thead>
								<tr>
									<?
										foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
											if($screen['elements'][$f]['textname']!=""){
												echo"<th>".$screen['elements'][$f]['textname']."</th>";
											}
										}
										//echo"<th>Művelet</th>";
										?>
								</tr>
							</thead>
							<tbody ></tbody>
						</table>
					</div>
				</div>
				<!-- END Datatables Content -->
			</div>
			<!-- END Page Content -->



			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>

			<!-- Load and execute javascript code used only in this page -->
			<script src="modules/goods/js/inc_products.js"></script>
			<script>$(function(){ ProductsDatatables.init(); });</script>

			<?php include 'inc/template_end.php'; ?>
<?
}
?>

