<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Termék kategória
            </h1>
        </div>
    </div>
<?
		if(isset($uritags[3]) && $uritags[3]!=""){
			if($uritags[3]=="new"){
				?>
				<!-- Ez egy elem  -->		
				<ul class="breadcrumb breadcrumb-top">
					<li><a href="/">Rendszer</a></li>
					<li><a href="/modules/goods/products_kateg">Termék kategória kezelő</a></li>
					<li><a href="/new">Új termék kategória felvitele</a></li>
				</ul>
				<div class="block full">
				<?
				
					$products_kateg=db_all("select id,name from products_kateg where status=1 order by name");
					
					echo"<form id=\"newproducts_kateg\" name=\"newproducts_kateg\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
					echo "<div style=\"margin-top:1px;\">";
							
						echo"<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"parent_id\">Szülő kategória:</label>
							<div class=\"col-md-9\">
								<select id=\"parent_id\" name=\"parent_id\" class=\"form-control\" placeholder=\"Szülő kategória\"   >
									<option value=\"0\">Kérlek válassz</option>";
									for($i=0;$i<count($products_kateg);$i++){		
										echo"<option value=\"".$products_kateg[$i]['id']."\" >".$products_kateg[$i]['name']."</option>";	
									}
								echo"</select>";
							echo"</div>
						</div>";
						
						echo"<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"name\">Elnevezés:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" placeholder=\"Elnevezés\" />																		
							</div>
						</div>";
						
						echo"<div class=\"form-group\" >";
							echo"<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span><br />Érvényes-e</label>";
						echo"</div>";
						
						echo"<div style=\"clear:both;height:10px;\"></div>";
						
						echo"<div class=\"form-group form-actions\">
							<div class=\"col-md-9 col-md-offset-3\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewproducts_kateg();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>";
						
					echo"</div>";
					echo"</form>";
				?>
				</div>
				<?php include 'inc/page_footer.php'; ?>
				<?php include 'inc/template_scripts.php'; ?>
				<script src="modules/goods/js/inc_products_kateg.js"></script>
				<?php include 'inc/template_end.php'; ?>
				<?
			}
			else{
				
				//echo "select * from functions where id='".$uritags[2]."'";
				$myproducts_kateg=db_row("select * from products_kateg where id='".$uritags[3]."'");
				if($myproducts_kateg['id']==''){
					die("Helytelen hozzáférés!");
				}
				else{
	?>
				<!-- Ez egy elem  -->		
				<ul class="breadcrumb breadcrumb-top">
					<li><a href="/">Rendszer</a></li>
					<li><a href="modules/goods/products_kateg">Termék kategóriák</a></li>
					<li><a href="/<?=$URI;?>"><?=$myproducts_kateg['name'];?></a></li>
				</ul>
				<div class="block full">
								   
						<!-- Block Tabs Title -->

						<div class="block-title">
							<h2><strong><?=$myproducts_kateg['name'];?></strong> termék kategória oldala</h2>
							<ul class="nav nav-tabs" data-toggle="tabs">
								<?
								if(priv("products_kateg","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
								if(priv("products_kateg","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
								if(priv("products_kateg","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
								if(priv("products_kateg","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
								if(priv("products_kateg","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}	
								?>
							</ul>
						</div>
						<!-- END Block Tabs Title -->

						<!-- Tabs Content -->
						<div class="tab-content">

							<div class="tab-pane active" id="tab-1">
								<div class="col-md-6">
									<dl class="dl-horizontal">
										<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$myproducts_kateg['id']); ?></dd>
										<dt>Szülő kategória:</dt><dd><? echo idchange("products_kateg","name",$myproducts_kateg['parent_id']); ?></dd>
										<dt>Elnevezés:</dt><dd><? echo $myproducts_kateg['name']; ?></dd>
										<dt>Érvényes?:</dt><dd><? if($myproducts_kateg['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
									</dl>	
								</div>
								<div style="clear:both;height:1px;"></div>

							</div>
							<div class="tab-pane" id="tab-2">
							<?
						
							$products_kateg=db_all("select id,name from products_kateg where status=1 order by name");
							
							echo"<form id=\"editproducts_kateg\" name=\"editproducts_kateg\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
							echo "<div style=\"margin-top:1px;\">";
									
								echo"<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"parent_id\">Szülő kategória:</label>
									<div class=\"col-md-9\">
										<select id=\"parent_id\" name=\"parent_id\" class=\"form-control\" placeholder=\"Szülő kategória\"   >
											<option value=\"0\">Kérlek válassz</option>";
											for($i=0;$i<count($products_kateg);$i++){		
												echo"<option value=\"".$products_kateg[$i]['id']."\" >".$products_kateg[$i]['name']."</option>";	
											}
										echo"</select>";
									echo"</div>
								</div>";
								
								echo"<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"name\">Elnevezés:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" placeholder=\"Elnevezés\" value=\"".$myproducts_kateg['name']."\"/>																		
									</div>
								</div>";
								
								echo"<div class=\"form-group\" >";
								$chc="";if($mybevetelezes['status']==1){ $chc=" checked "; }
									echo"<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" ".$chc."><span></span><br />Státusz</label>";
								echo"</div>";
								
								echo"<div style=\"clear:both;height:10px;\"></div>";
								
								echo"<div class=\"form-group form-actions\">
									<div class=\"col-md-9 col-md-offset-3\">
										<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditproducts_kategs(".$myproducts_kateg['id'].");\"><i class=\"fa fa-angle-right\"></i> Ment</button>
									</div>
								</div>";
								
							echo"</div>";
							echo"</form>";
							?>
							</div>
							<div class="tab-pane" id="tab-3">
									<?
										echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$myproducts_kateg['id'],"Dokumentum feltöltés")."</div>";
									?> 
									<div id="filelists"></div>
									<div style="clear:both;height:1px;"></div>
							</div>
							
							<div class="tab-pane" id="tab-4">
									<?
										echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$myproducts_kateg['id'],"Hozzászólások")."</div>";
									?>
									<div style="clear:both;height:1px;"></div>
							</div>
							
							<div class="tab-pane" id="tab-5">
								<div id="printdiv"></div>
							</div>
					
						</div>
						<!-- END Tabs Content -->
					</div>
					<!-- END Block Tabs -->
						
				</div>
				
				<?php include 'inc/page_footer.php'; ?>
				<?php include 'inc/template_scripts.php'; ?>
				<script src="modules/goods/js/inc_products_kateg.js"></script>

				<script >
					showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$myproducts_kateg['id'];?>,'Dokumentum feltöltés','filelists');
					showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$myproducts_kateg['id'];?>,'Dokumentum feltöltés','filelists');
				</script>
				<script>$(function(){ products_kategDatatables.init(); });</script>	
			

				<?php include 'inc/template_end.php'; ?>		
	<?
				}
			}
		}
		else{
			//print_r($_SESSION['planetsys'])
?>    
	<!-- Ez a lista  -->
				<ul class="breadcrumb breadcrumb-top">
					<li><a href="/">Rendszer</a></li>
					<li><a href="/<?=$URI;?>">Termék kategóriák</a></li>
				</ul>
				<!-- END Datatables Header -->

				<!-- Datatables Content -->
				<div class="block full">
					<div class="block-title">
						<h2><strong>Termék kategória</strong> lista</h2>
					</div>
					

					<div class="table-responsive">
						<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
							<thead>
								<tr>
									<?
									foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
										if($screen['elements'][$f]['textname']!=""){
											echo"<th>".$screen['elements'][$f]['textname']."</th>";
										}
									}
									//echo"<th>Művelet</th>";
									?>
								</tr>
							</thead>
							<tbody ></tbody>
						</table>
					</div>
				</div>
				<!-- END Datatables Content -->
			</div>
			<!-- END Page Content -->

			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>

			<!-- Load and execute javascript code used only in this page -->
			<script src="modules/goods/js/inc_products_kateg.js"></script>
			<script>$(function(){ products_kategDatatables.init(); });</script>

			<?php include 'inc/template_end.php'; ?>
<?
}
?>

