<?
include dirname(__FILE__).("/inc.producttype.php");
naplo('info','producttype','Terméktípuskezelő  betöltése');
$_SESSION['planetsys']['actpage']=$screen;

if(!isset($_SESSION['planetsys']['producttype']['filter'])){
	
	$_SESSION['planetsys']['producttype']['filter']['catsum']="";
	$_SESSION['planetsys']['producttype']['filter']['active']="1";
	$_SESSION['planetsys']['producttype']['filter']['efproduct']="1";
	$_SESSION['planetsys']['producttype']['ste']="Szűrve: aktív, késztermék";
}
$_SESSION['planetsys']['producttype']['ste']="Szűrve: ";
			
if(priv("producttype","setup")){
?>
<li class="dropdown">
	<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
		<i class="gi gi-cogwheels"></i>
	</a>
	<ul class="dropdown-menu dropdown-custom dropdown-options">
		<li class="dropdown-header text-center">Beállítások</li>
		<li>
			<form name="setupform" id="setupform">
			
			<?
			$lq="select * from listoptions where user_id='".$_SESSION['planetsys']['user_id']."' and modulname='".$_SESSION['planetsys']['actpage']['modulname']."' and funcname='".$_SESSION['planetsys']['actpage']['funcname']."'";
			$listoptions=db_row($lq);
			$listelemets=explode(",",$listoptions['screenlist']);
			foreach($_SESSION['planetsys']['actpage']['elements'] as $record){
				if(in_array($record['name'],$listelemets)){	$ch=" checked=\"checked\" ";}else{$ch=" ";}
				echo"<div ><input type=\"checkbox\" id=\"AAA_".$record['name']."\" name=\"AAA_".$record['name']."\" ".$ch." onclick=\"javascript:savesetupscreen();\" />&nbsp;".$record['textname']."</div>";
			}
			?>
			<div class="clear"></div>
			</form>

		</li>
	</ul>
</li>
<?
}
if(priv("producttype","export")){
?>
<li class="dropdown">
	<a href="#modal-regular-export" data-toggle="modal">
		<i class="fa fa-briefcase" title="export"></i>
	</a>
</li>
<?
}
if(priv("producttype","filter")){
?>
<li class="dropdown">
	<a href="#modal-regular-filter" data-toggle="modal">
		<i class="fa fa-filter" title="filter"></i>
	</a>

</li>
<?
}
if(priv("producttype","new")){
?>
<li class="dropdown">
	<a href="/<?=$template['workpage'];?>/new " >
		<i class="gi gi-bomb" title="New"></i>
	</a>	
</li>
<?
}
?>
<form  id="newelementform" id="newelementform" class="form-horizontal form-bordered" onsubmit="return false;">
<?
	echo"<div id=\"modal-regular-filter\" class=\"modal\" tabindex=\"-2\" role=\"dialog\" aria-hidden=\"true\">
			<div class=\"modal-dialog\">
				<div class=\"modal-content\">
					<div class=\"modal-header\">
						<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
						<h3 class=\"modal-title\">Szűrés</h3>
					</div>
					<div class=\"modal-body\">";
					
					?>
					<div class="form-group" >
						<label class="col-md-3 control-label" for="kat_id">Kategória</label>
						<div style="clear:both;height:10px;"></div>
						<div class="col-md-9" style="width:550px;height:200px;overflow:auto;float:left;">
							<?
								$categories=db_all("select * from gw_posts where status=1 order by name asc");
								$catstring="";
								for($i=0;$i<count($categories);$i++){
									echo"<label class=\"switch switch-primary\" style=\"width:180px;float:left;\"><input type=\"checkbox\" id=\"cat_".$categories[$i]['id']."\" name=\"cat_".$categories[$i]['id']."\" value=\"cat_".$categories[$i]['id']."\" /><span></span>".$categories[$i]['name']."</label>";
									//echo"<div style=\"width:20px;float:left;\"><input type=\"checkbox\" name=\"cat_".$categories[$i]['id']."\" id=\"cat_".$categories[$i]['id']."\" value=\"cat_".$categories[$i]['id']."\" /></div><div class=\"leftitemtext\">".$categories[$i]['name']."</div><div style=\"clear:both;height:2px;\"></div>";
									$catstring.=$categories[$i]['id']."-";
								}
								echo"<input type=\"hidden\" id=\"catsum\" value=\"".$catstring."\"/>";
							?>
						</div>
					</div>
					
					<div style="clear:both;height:1px;"></div>
					<div class="form-group" >
						<label class="col-md-3 control-label" for="active">Aktív</label>
						<div class="col-md-9">
							<select id="active" name="active" class="form-control" >
								<?
								$sle1="";if($_SESSION['planetsys']['producttype']['filter']['active']==1){$sel1=" selected ";$_SESSION['planetsys']['producttype']['ste'].="aktív, ";}echo"<option value=\"1\" ".$sel1.">Aktívak</option>";
								$sle2="";if($_SESSION['planetsys']['producttype']['filter']['active']==0){$sel2=" selected ";$_SESSION['planetsys']['producttype']['ste'].="összes, ";}echo"<option value=\"0\" ".$sel2.">Mind</option>";
								$sle3="";if($_SESSION['planetsys']['producttype']['filter']['active']==2){$sel3=" selected ";$_SESSION['planetsys']['producttype']['ste'].="inaktív, ";}echo"<option value=\"2\" ".$sel3.">Inaktívak</option>";
								?>
							</select>
						</div>
					</div>
					<div style="clear:both;height:5px;"></div>
					
					<div style="clear:both;height:1px;"></div>
					<div class="form-group" >
						<label class="col-md-3 control-label" for="efproduct">Termék készültség</label>
						<div class="col-md-9">
							<select id="efproduct" name="efproduct" class="form-control" >
								<?
								$sle21="";if($_SESSION['planetsys']['producttype']['filter']['efproduct']==0){$sel21=" selected ";$_SESSION['planetsys']['producttype']['ste'].="mind";}echo"<option value=\"0\" ".$sel21.">Mind</option>";
								$sle22="";if($_SESSION['planetsys']['producttype']['filter']['efproduct']==1){$sel22=" selected ";$_SESSION['planetsys']['producttype']['ste'].="késztermék";}echo"<option value=\"1\" ".$sel22.">Késztermékek</option>";
								$sle23="";if($_SESSION['planetsys']['producttype']['filter']['efproduct']==2){$sel23=" selected ";$_SESSION['planetsys']['producttype']['ste'].="félkész termék";}echo"<option value=\"2\" ".$sel23.">Félkész termékek</option>";
								?>
							</select>
						</div>
					</div>
					<div style="clear:both;height:5px;"></div>
					<?										
					echo"</div>
					<div class=\"modal-footer\">
						<button type=\"button\" class=\"btn btn-sm btn-warning\" onclick=\"javascript:ProductTypeDatatables.init();\" >Szűr</button>
						<button type=\"button\" class=\"btn btn-sm btn-default\" data-dismiss=\"modal\">Bezár</button>
					</div>
				</div>
			</div>
		</div>";	
?>
</form>
