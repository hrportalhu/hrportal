<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Raktár kezelő
            </h1>
        </div>
    </div>
<?
if(priv("stores","allsum")){
$myalapgs=db_all("select ae.agt_id agt_id,at.name as name  from alapag_events ae left join alapag_tipus at on (ae.agt_id=at.id) where   (ae.event=1 or ae.event=3 or ae.event=7  or ae.event=6) group by ae.agt_id order by at.name");
$sumar=0;
for($a=0;$a<count($myalapgs);$a++){
	$mennyi=db_one("select sum(amount) from alapag_events where  agt_id='".$myalapgs[$a]['agt_id']."' and (event=1 or event=3 or event=7 or event=6)");
	$pt=db_one("select price from alapanyag where agt_id='".$myalapgs[$a]['agt_id']."'");
	$sumar=$sumar+(round($pt)*round($mennyi));
	
}
?>
<!-- Widget -->
<a href="#" class="widget widget-hover-effect1">
	<div class="widget-simple" style="width:500px;">
		<div class="widget-icon pull-left themed-background animation-fadeIn" style="width:450px;">
			Összraktárkészlet <?=huf(round($sumar,2));?> Ft	
		</div>
	</div>
</a>
							<!-- END Widget -->
<?
}
?>
<?



		if(isset($uritags[3]) && $uritags[3]!=""){
			if($uritags[3]=="new"){
				?>
				<!-- Ez egy elem  -->		
				<ul class="breadcrumb breadcrumb-top">
					<li><a href="/">Rendszer</a></li>
					<li><a href="/modules/goods/stores">Raktár kezelő</a></li>
					<li><a href="/new">Új raktár felvitele</a></li>
				</ul>
				<div class="block full">
					<div class="block-title">
						<h2><strong>Új Raktár felvitele</strong></h2>
					</div>
				<?
				
					
					echo"<form id=\"newstores\" name=\"newstores\"  method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
					echo "<div style=\"margin-top:1px;\">
							
						<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"name\">Elnevezés:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" placeholder=\"Raktár elnevezése\"  />																		
							</div>
						</div>	
						<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"address\">Cím:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"address\" name=\"address\" class=\"form-control\" placeholder=\"Raktár címe\"  />																		
							</div>
						</div>	
						<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"city\">Település:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"city\" name=\"city\" class=\"form-control\" placeholder=\"Raktár települése\"  />					
							</div>
						</div>
						<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"zip\">Irányítószám:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"zip\" name=\"zip\" class=\"form-control\" placeholder=\"Raktár irányítószáma\"  />
							</div>
						</div>
						<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"phone\">Telefonszám:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"phone\" name=\"phone\" class=\"form-control\" placeholder=\"Raktár telefonszáma\"  />								
							</div>
						</div>
						<div class=\"form-group\" >";
							echo"<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span><br />Érvényes-e</label>";
						echo"</div>
						<div style=\"clear:both;height:10px;\"></div>
						<div class=\"form-group form-actions\">
							<div class=\"col-md-9 col-md-offset-3\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewstores();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>
					</div>
					</form>";
				?>
				</div>	
				<?php include 'inc/page_footer.php'; ?>
				<?php include 'inc/template_scripts.php'; ?>
				<script src="modules/goods/js/inc_stores.js"></script>
				<?php include 'inc/template_end.php'; ?>	
				<?	
			}
			elseif($uritags[3]=="alllist"){
				echo"<ul class=\"breadcrumb breadcrumb-top\">";
				echo"	<li><a href=\"/\">Rendszer</a></li>";
				echo"	<li><a href=\"modules/goods/stores\">Raktárak</a></li>";
				echo"	<li><a href=\"/".$URI."\">Raktárak lista</a></li>";
				echo"</ul>";
				echo"<form id=\"newstaaores\" name=\"newstaaores\"  method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				$stores=db_all("select * from stores where status=1 order by name");
				$storestring="";
				for($a=0;$a<count($stores);$a++){
					$ch="";if($stores[$a]['id']==1){$ch=" checked ";}
					echo"<label class=\"switch switch-primary\" style=\"margin-right:20px;\"><input type=\"checkbox\" id=\"sto_".$stores[$a]['id']."\" name=\"sto_".$stores[$a]['id']."\" value=\"sto_".$stores[$a]['id']."\" ".$ch." onclick=\"showallraktar();\"  /><span></span>".$stores[$a]['name']."</label>";
					
					$storestring.=$stores[$a]['id']."-";	
				}
				
				
						echo"<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"startdate\">Kezdődátum:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"startdate\" name=\"startdate\"  class=\"form-control input-datepicker\"  value=\"2015-01-01\"/>
								
							</div>
						</div>";
						echo"<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"enddate\">Befejező:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"enddate\" name=\"enddate\" value=\"".date("Y-m-d")."\" class=\"form-control input-datepicker\" />
							</div>
						</div>";
					echo"<label class=\"switch switch-primary\" style=\"margin-right:20px;\"><input type=\"checkbox\" id=\"teteles\" name=\"teteles\" value=\"teteles\"  onclick=\"showallraktar();\" class=\"form-control \" /><span></span>Tételes</label>";
				echo"<input type=\"hidden\" id=\"stosum\" value=\"".$storestring."\"/>";
				echo"<div class=\"form-group form-actions\">
							<div class=\"col-md-9 col-md-offset-3\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"showallraktar();\"><i class=\"fa fa-angle-right\"></i>Frissít</button>
							</div>
						</div>";
				echo"</form>";
				?>
					<div id="allraktar"></div>
					</div>
					</div>
					<?php include 'inc/page_footer.php'; ?>
					<?php include 'inc/template_scripts.php'; ?>
					<script src="modules/goods/js/inc_stores.js"></script>
					
					<script >
						showallraktar();
						
									
				
					</script>
					<?php include 'inc/template_end.php'; ?>	
				<?
			}
			else{
				
				//echo "select * from functions where id='".$uritags[2]."'";
				$mystores=db_row("select * from stores where id='".$uritags[3]."'");
				if($mystores['id']==''){
					die("Helytelen hozzáférés!");
				}
				else{
	?>
				<!-- Ez egy elem  -->		
				<ul class="breadcrumb breadcrumb-top">
					<li><a href="/">Rendszer</a></li>
					<li><a href="modules/goods/stores">Raktárak</a></li>
					<li><a href="/<?=$URI;?>"><?=$mystores['name'];?></a></li>
				</ul>
				<div class="block full">
								   
						<!-- Block Tabs Title -->

						<div class="block-title">
							<h2><strong><?=$mystores['name'];?></strong> raktár oldala</h2>
							<ul class="nav nav-tabs" data-toggle="tabs">
								
								<?
								if(priv("stores","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
								if(priv("stores","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
								if(priv("stores","instore")){echo"<li><a href=\"#tab-6\" data-toggle=\"tooltip\" title=\"instore\"><i class=\"gi gi-cargo\"></i></a></li>";}
								if(priv("stores","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
								if(priv("stores","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
								if(priv("stores","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}
								
								?>

							</ul>
						</div>
						<!-- END Block Tabs Title -->

						<!-- Tabs Content -->
						<div class="tab-content">

							<div class="tab-pane active" id="tab-1">
								<div class="col-md-6">
									<dl class="dl-horizontal">
										<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$mystores['id']); ?></dd>
										<dt>Elnevezés:</dt><dd><? echo $mystores['name']; ?></dd>
										<dt>Cím:</dt><dd><? echo $mystores['address']; ?></dd>
										<dt>Település:</dt><dd><? echo $mystores['city']; ?></dd>
										<dt>Irányítószám:</dt><dd><? echo $mystores['zip']; ?></dd>
										<dt>Telefonszám:</dt><dd><? echo $mystores['phone']; ?></dd>
										<dt>Érvényese:</dt><dd><? if($mystores['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
									</dl>	
								</div>
								<div style="clear:both;height:1px;"></div>

							</div>
							<div class="tab-pane" id="tab-2">
								<?
				
					
					echo"<form id=\"editstores\" name=\"editstores\"  method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
					echo "<div style=\"margin-top:1px;\">
							
						<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"name\">Elnevezés:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" placeholder=\"Raktár elnevezése\" value=\"".$mystores['name']."\" />																		
							</div>
						</div>	
						<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"address\">Cím:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"address\" name=\"address\" class=\"form-control\" placeholder=\"Raktár címe\" value=\"".$mystores['address']."\" />																		
							</div>
						</div>	
						<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"city\">Település:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"city\" name=\"city\" class=\"form-control\" placeholder=\"Raktár települése\" value=\"".$mystores['city']."\" />					
							</div>
						</div>
						<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"zip\">Irányítószám:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"zip\" name=\"zip\" class=\"form-control\" placeholder=\"Raktár irányítószáma\" value=\"".$mystores['zip']."\" />
							</div>
						</div>
						<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"phone\">Telefonszám:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"phone\" name=\"phone\" class=\"form-control\" placeholder=\"Raktár telefonszáma\" value=\"".$mystores['phone']."\" />								
							</div>
						</div>
						<div class=\"form-group\" >";
							echo"<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span><br />Érvényes-e</label>";
						echo"</div>
						<div style=\"clear:both;height:10px;\"></div>
						<div class=\"form-group form-actions\">
							<div class=\"col-md-9 col-md-offset-3\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditstores('".$mystores['id']."');\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>
					</div>
					</form>";
				?>
							</div>
							<div class="tab-pane" id="tab-3">
									<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mystores['id'],"Dokumentum feltöltés")."</div>";
								   ?> 
								   <div id="filelists"></div>
								   <div style="clear:both;height:1px;"></div>
							</div>
							<div class="tab-pane" id="tab-4">
									<?
										echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mystores['id'],"Hozzászólások")."</div>";
									?>
									
							</div>
							<div class="tab-pane" id="tab-6">
									
								<div class="table-responsive">
										<table class="table table-vcenter table-striped">
											<thead>
												<tr>
													
													<th>Alapanyag név</th>
													<th>Jelenlegi mennyiség</th>
													<th>Beszár</th>
												</tr>
											</thead>
											<tbody>
												<?php
													$myalapgs=db_all("select ae.agt_id agt_id,at.name as name  from alapag_events ae left join alapag_tipus at on (ae.agt_id=at.id) where ae.store_id='".$mystores['id']."' and  (ae.event=1 or ae.event=3 or ae.event=7  or ae.event=6) group by ae.agt_id order by at.name");
													//$instore=db_all("select ae.agt_id as agt_id,at.name as name  from alapag_events ae left join alapag_tipus at on ae.agt_id=at.id  where store_id!='".$mystores['id']."' group by agt_id order by at.name asc ");
													$sumar=0;
													for($a=0;$a<count($myalapgs);$a++){
														$mennyi=getalap($myalapgs[$a]['agt_id'],$mystores['id']);
														//$mennyi=db_one("select sum(amount) from alapag_events where store_id='".$mystores['id']."' and agt_id='".$myalapgs[$a]['agt_id']."' and (event=1 or event=3 or event=7 or event=6)");
														$pt=db_one("select price from alapanyag where agt_id='".$myalapgs[$a]['agt_id']."'");
														$sumar=$sumar+(round($pt)*round($mennyi));
														echo"<tr>";
														echo"<td><a href=\"/modules/goods/materials/".$myalapgs[$a]['agt_id']."\">".$myalapgs[$a]['name']."</a></td>";
														echo"<td class=\"text-right\">".round($mennyi,2)." Kg</td>";
														echo"<td class=\"text-right\">".huf(round($pt)*round($mennyi))." Ft</td>";
														
														echo"</tr>";
													}
														echo"<tr>";
														echo"<td colspan=\"2\">Összesen:</td>";
														echo"<td class=\"text-right\">".huf($sumar)." Ft</td>";
														
														echo"</tr>";
												?>
											</tbody>
										</table>
									</div>

									
							</div>
							<div class="tab-pane" id="tab-5">
								<div id="printdiv"></div>
							
							</div>
					
						</div>
						<!-- END Tabs Content -->
					</div>
					<!-- END Block Tabs -->
						
				</div>
				
				<?php include 'inc/page_footer.php'; ?>
				<?php include 'inc/template_scripts.php'; ?>
				<script src="modules/goods/js/inc_stores.js"></script>

				<script >
					showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mystores['id'];?>,'Dokumentum feltöltés','filelists');
					showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mystores['id'];?>,'Dokumentum feltöltés','filelists');
								
			
				</script>
				<script>$(function(){ storesDatatables.init(); });</script>	
			

				<?php include 'inc/template_end.php'; ?>		
	<?
				}
			}
		}
		else{
			//print_r($_SESSION['planetsys'])
?>    
	<!-- Ez a lista  -->
				<ul class="breadcrumb breadcrumb-top">
					<li><a href="/">Rendszer</a></li>
					<li><a href="/<?=$URI;?>">Raktárak</a></li>
				</ul>
				<!-- END Datatables Header -->

				<!-- Datatables Content -->
				<div class="block full">
					<div class="block-title">
						<h2><strong>Raktár</strong> lista</h2>
					</div>
					

					<div class="table-responsive">
						<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
							<thead>
								<tr>
									<?
										foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
											if($screen['elements'][$f]['textname']!=""){
												echo"<th>".$screen['elements'][$f]['textname']."</th>";
											}
										}
										//echo"<th>Művelet</th>";
										?>
								</tr>
							</thead>
							<tbody ></tbody>
						</table>
					</div>
				</div>
				<!-- END Datatables Content -->
			</div>
			<!-- END Page Content -->



			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>

			<!-- Load and execute javascript code used only in this page -->
			<script src="modules/goods/js/inc_stores.js"></script>
			<script>$(function(){ storesDatatables.init(); });</script>

			<?php include 'inc/template_end.php'; ?>
<?
}
?>

