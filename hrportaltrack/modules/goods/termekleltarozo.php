<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Termékleltározó kezelő
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/goods/termekleltarozo">Termékleltározó kezelő</a></li>
				<li><a href="/new">Új termékleltározó felvitele</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új termékleltározó felvitele</strong></h2>
				</div>
			<?
				echo "<form id=\"newtermekleltarozo\" name=\"newtermekleltarozo\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";

						echo"<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"name\">Elnevezés:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" placeholder=\"Termékleltározó elnevezése\" />																		
							</div>
						</div>";
						
						echo"<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"manufact_date\">Idő:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"manufact_date\" name=\"manufact_date\" class=\"form-control input-datepicker\" placeholder=\"Idő\" />																		
							</div>
						</div>";
						
						echo"<div class=\"form-group\" >";
							 echo"<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span><br />Státusz</label>";
						echo"</div>";
						

						echo"<div style=\"clear:both;height:10px;\"></div>";
						echo"<div class=\"form-group form-actions\">
							<div class=\"col-md-9 col-md-offset-3\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewtermekleltarozo();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>";
				echo"</div>
				</form>";
			?>
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/goods/js/inc_termekleltarozo.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?	
		}
		else{

			//echo "select * from functions where id='".$uritags[2]."'";
			$mytermekleltarozo=db_row("select * from product_header where id='".$uritags[3]."'");
			if($mytermekleltarozo['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="modules/goods/termekleltarozo">Termékleltározó</a></li>
				<li><a href="/<?=$URI;?>"><?=$mytermekleltarozo['name'];?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->

					<div class="block-title">
						<h2><strong><?=$mytermekleltarozo['name'];?></strong> termékleltározó oldal</h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
							if(priv("termekleltarozo","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("termekleltarozo","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							if(priv("termekleltarozo","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("termekleltarozo","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
							if(priv("termekleltarozo","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}	
							?>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="col-md-6">
								<dl class="dl-horizontal">
									<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$mytermekleltarozo['id']); ?></dd>
									<dt>Elnevezés:</dt><dd><? echo $mytermekleltarozo['name']; ?></dd>
									<dt>Leltár ideje:</dt><dd><? echo $mytermekleltarozo['manufact_date']; ?></dd>
									<dt>Státusz:</dt><dd><? if($mytermekleltarozo['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
								</dl>	
							</div>
							<div class="col-md-6">
								<?php
									$pic=db_one("select filename from files where modul='goodworkers' and type='termekleltarozo' and type_id='".$mytermekleltarozo['id']."' order by cover desc");
									if(is_file($pic)){
										echo"<div class=\"col-sm-3\"><img src=\"/".$pic."\" alt=\"avatar\" width=\"450px\"></div>";
									}
									else{
										echo"<img src=\"img/placeholders/avatars/avatar9@2x.jpg\" alt=\"avatar\">";
									}
								?>
							</div>
							<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-2">
						<?
							echo"<form id=\"edittermekleltarozo\" name=\"edittermekleltarozo\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
							echo "<div style=\"margin-top:1px;\">";
							
								echo"<div class=\"form-group\">
										<label class=\"col-md-3 control-label\" for=\"name\">Elnevezés:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" placeholder=\"Értékesítési hely elnevezése\" value=\"".$mytermekleltarozo['name']."\" />																		
										</div>
									</div>";
									
									echo"<div class=\"form-group\">
										<label class=\"col-md-3 control-label\" for=\"manufact_date\">Idő:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"manufact_date\" name=\"manufact_date\" class=\"form-control input-datepicker\" placeholder=\"Szállítás ideje\" value=\"".$mytermekleltarozo['manufact_date']."\"  />																		
										</div>
									</div>";
									echo"<div class=\"form-group\" >";
										 echo"<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span><br />Státusz</label>";
									echo"</div>";
									
									echo"<div style=\"clear:both;height:10px;\"></div>
									<div class=\"form-group form-actions\">
										<div class=\"col-md-9 col-md-offset-3\">
											<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveedittermekleltarozo(".$mytermekleltarozo['id'].");\"><i class=\"fa fa-angle-right\"></i> Ment</button>
										</div>
									</div>";
								echo"</div>
								</form>";
								
									$posts=db_all("select post_id from product_elements where ph_id='".$mytermekleltarozo['id']."' and post_id!=0 group by post_id order by post_id ");
								
									echo"<div class=\"block-title\">";
									echo"<h3>Termékek leltározása</h3>";
									echo"<ul class=\"nav nav-tabs\" data-toggle=\"tabs\">";
									for($i=0;$i<count($posts);$i++){
										$acz1="";if($i==0){$acz1=" class=\"active\" ";}
										echo"<li><a href=\"#ptab-".$i."\" ".$acz1." data-toggle=\"tooltip\" title=\"".idchange("gw_posts","name",$posts[$i]['post_id'])."\">".idchange("gw_posts","name",$posts[$i]['post_id'])."</a></li>";
									}
									echo"</ul>";
									echo"</div>";
									echo"<div class=\"tab-content\" style=\"background-color:#ffffff;\">";
									for($i=0;$i<count($posts);$i++){
										$acz="";if($i==0){$acz=" active";}

										echo"<div class=\"tab-pane ".$acz."\" id=\"ptab-".$i."\">";
										echo"<h1>".idchange("gw_posts","name",$posts[$i]['post_id'])." munkahely</h1>";
										$pts=db_all("select * from product_elements where post_id='".$posts[$i]['post_id']."' and ph_id='".$mytermekleltarozo['id']."' group by pt_id order by pt_id ");
										//echo "select * from manufact_element where post_id='".$posts[$i]['post_id']."' and mh_id='".$mymanufact['id']."'  order by pt_id ";
										//echo "select pt_id from manufact_element where post_id='".$posts[$i]['post_id']."' group by pt_id ordr by pt_id ";
										
										echo"<table id=\"general-table\" class=\"table table-striped table-vcenter\">
											<thead>
												<tr>
													<th>Termék elnevezése</th>
													<th>Készítő munkahely</th>
													<th>Talált mennyiség</th>
													<th>Talált egység</th>
													<th>Egalizált mennyiség /Kg/</th>
													<th>Alapmértékegység</th>
													<th>Művelet</th>
												</tr>
											</thead>
											<tbody>";
											$mert=db_all("select * from mertekegyseg where status='1' order by id asc");
										for($a=0;$a<count($pts);$a++){
											//$me=mee($pts[$a]['ph_id']);
											$col="";if($pts[$a]['ok']==1){
												$col="background-color:yellow;";
											}
											echo"<tr>";
											echo"<td><a href=\"modules/goods/producttype/".$pts[$a]['pt_id']."\">".idchange("product_types","name",$pts[$a]['pt_id'])."</a></td>";
											echo"<td>".idchange("gw_posts","name",$posts[$i]['post_id'])."</td>";
											//$val=if($pts[$a]['pt_id'])
											echo"<td><input type=\"text\" name=\"found_".$pts[$a]['id']."\" id=\"found_".$pts[$a]['id']."\" value=\"".$pts[$a]['instock_amount']."\"></td>";
											echo"<td>";
											?>
											<div class="input-group">
												<select name="unit_<?=$pts[$a]['id'];?>" id="unit_<?=$pts[$a]['id'];?>" class="form-control" disabled >
														<option value="1">Kilogramm</option>
													<?
														for($x=0;$x<count($mert);$x++){	
															echo"<option value=\"".$mert[$x]['id']."\">".$mert[$x]['name']."</option>";	
														}	
													?>
													</select>
											</div>
											<?
											echo"</td>";
											echo"<td id=\"ret_".$pts[$a]['id']."\">".$pts[$a]['instock_amount']."</td>";
											echo"<td>Kg</td>";
											echo"<td class=\"text-center\">";
											
												echo"<div class=\"btn-group btn-group-xs\" >";
													echo"<a href=\"javascript:termletarok('".$pts[$a]['ph_id']."','".$pts[$a]['id']."');\" data-toggle=\"tooltip\" title=\"Jóváhagy\"  class=\"btn btn-info\"><i class=\"gi gi-floppy_saved\"></i></a>";
												echo"<div id=\"let_".$pts[$a]['id']."\" style=\"width:5px;float:left;height:22px;".$col."\">&nbsp;</div>";
												echo"</div>";
											
										echo"</td>";
											echo"</tr>";
										}
										  echo"</tbody> </table>";
									echo"</div>";
										
										
									}
									echo"<div style=\"clear:both;height:10px;\"></div>";
							echo"</div>";
				
								
								
								
								
								
						?>
						</div>
						<div class="tab-pane" id="tab-3">
								<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mytermekleltarozo['id'],"Dokumentum feltöltés")."</div>";
								?> 
								<div id="filelists"></div>
								<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mytermekleltarozo['id'],"Hozzászólások")."</div>";
								?>
						</div>
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>
				
					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
					
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/goods/js/inc_termekleltarozo.js"></script>

			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mytermekleltarozo['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$mytermekleltarozo['id'];?>,'Dokumentum feltöltés','filelists');
			</script>
			<script>$(function(){ TermekleltarozoDatatables.init(); });</script>	

			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
		//print_r($_SESSION['planetsys'])
?>    
<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Termékleltározó</a></li>
			</ul>
			<!-- END Datatables Header -->

			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Termékleltározó</strong> lista</h2>
				</div>
				
				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							//echo"<th>Művelet</th>";
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/goods/js/inc_termekleltarozo.js"></script>
		<script>$(function(){ TermekleltarozoDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>
