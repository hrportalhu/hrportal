<div id="page-content">
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Termék listák
            </h1>
        </div>
    </div>
	<ul class="breadcrumb breadcrumb-top">
		<li><a href="/">Rendszer</a></li>
		<li><a href="/<?=$URI;?>">Termékek </a></li>
	</ul>
	<div class="block full">
		<div class="block-title">
			<h2><strong>Elérhető</strong> terméktípus lekérdezések</h2>
			<ul class="nav nav-tabs" data-toggle="tabs">
				<?
					if(priv("producttype","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"Félkész termékek listája\" onclick=\"javascript:halfproductslists();\"><i class=\"gi gi-eye_open\"></i></a></li>";}
					if(priv("producttype","view")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"Végtermék lista\" onclick=\"javascript:productslists();\"><i class=\"gi gi-eye_close\"></i></a></li>";}			
					if(priv("producttype","view")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"Alergén mátrix\" onclick=\"javascript:productallergen();\"><i class=\"gi gi-thumbs_down\"></i></a></li>";}			
					
				?>	
			</ul>
							
		</div>
		<div class="tab-content">
			<div class="tab-pane active" id="tab-1">
				<div class="table-responsive">
					<table class="table table-vcenter table-striped">
						<thead>
							<tr>
								<th class="text-center">Félkész termék neve</th>
								<th class="text-center">MMI</th>
								<th class="text-center">Tárolási hőmérséklet</th>
							</tr>
						</thead>
						<tbody id="termeklist1"></tbody>
					</table>
				</div>
				<div style="clear:both;height:1px;"></div>
			</div>
			<div class="tab-pane active" id="tab-3">
				<div class="table-responsive">
					<table class="table table-vcenter table-striped">
						<thead>
							<tr>
								<th class="text-center">Termék neve</th>
								<th class="text-center">MMI</th>
								<th class="text-center">Tárolási hőmérséklet</th>
							</tr>
						</thead>
						<tbody id="termeklist3"></tbody>
					</table>
				</div>
				<div style="clear:both;height:1px;"></div>
			</div>
			<div class="tab-pane" id="tab-2">
				<div class="block hidden-lt-ie9">
							<!-- Switches Title -->
							<div class="block-title">
								<h2><strong>Szűrő</strong> </h2>
							</div>
							<?
							$posts=db_all("select * from gw_posts where status=1");
						   echo"<div class=\"form-group\" >
									
									<div class=\"col-md-6\">
										<select id=\"post_id\" name=\"post_id\" class=\"form-control\"  onchange=\"javascript:productallergen();\" >
											<option value=\"0\" selected>Minden munkahely</option>";
											for($i=0;$i<count($posts);$i++){
												echo"<option value=\"".$posts[$i]['id']."\" >".$posts[$i]['name']."</option>";
											}
										echo"</select>
									</div>";									
									echo"<div class=\"col-md-2\">
										<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"javascript:productallergen();\">Szűr</button>
									</div>
								</div>";
						   
								?>	
								<div style="clear:both;height:10px;"></div>
						</div>	
						
						<div style="clear:both;height:10px;"></div>
				<div class="table-responsive">
					<table class="table table-vcenter table-striped">
						<thead>
							<tr>
								<th class="text-center">Termék neve</th>
								<th class="text-center">Diófélék</th>
								<th class="text-center">Mogyorófélék</th>
								<th class="text-center">Kén-Dioxid</th>
								<th class="text-center">Tejtermék</th>
								<th class="text-center">Szója</th>
								<th class="text-center">Tojás</th>
								<th class="text-center">Glutén</th>
							</tr>
						</thead>
						<tbody id="termeklist2"></tbody>
					</table>
				</div>
				<div style="clear:both;height:1px;"></div>
			</div>
		</div>		
	</div>					
</div>
<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<script src="/modules/goods/js/inc_termeklists.js"></script>
<script >
	halfproductslists();			
</script>
<?php include 'inc/template_end.php'; ?>
