<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors','Off');
header("Cache-control: private");
header('Content-Type: text/plain;charset=utf-8');
include_once dirname(__FILE__).("/../../inc/sys.conf");
include_once dirname(__FILE__).("/../../inc/_functions.php");

$a_date = $_POST['month']."-01";
$fin= date("Y-m-t", strtotime($a_date));

$dat1=" and workday between '".$_POST['month']."-01' and '".$fin."'  ";
$dat3=" and mdate between '".$_POST['month']."-01' and '".$fin."'  ";
$dat4=" and mydate between '".$_POST['month']."-01' and '".$fin."'  ";
$dat2=" ".$_POST['month']."-01 és ".$fin." között";

$workers=db_all("select * from gw_shift_x_worker where worker_id='".$_REQUEST['elemid']."' ".$dat1." group by workday  order by workday asc ");
echo"<div class=\"block\">
	<div class=\"block-title\">
		<h2><strong>Havi fizetés összegzés ".$dat2."</strong></h2>
	</div>";
	echo"<div>";
	echo"<table class=\"table table-vcenter table-striped\">";
		echo"<thead>";
			echo"<tr>";
				echo"<th style=\"width: 100px;\" class=\"text-center\"><i class=\"gi gi-disk_save\"></i></th>";
				echo"<th class=\"text-center\">Dátum</th>";
				echo"<th class=\"text-center\">Munkaidő</th>";
				echo"<th class=\"text-center\">Összeg</th>";
			echo"</tr>";
		echo"</thead>";
		echo"<tbody >";
		$awt=0;
		$adc=0;
		for($i=0;$i<count($workers);$i++){
			$wt=db_one("select sum(worktime) from gw_shift_x_worker where worker_id='".$_REQUEST['elemid']."' and workday='".$workers[$i]['workday']."' ");
			$dc=db_one("select sum(dailycash) from gw_shift_x_worker where worker_id='".$_REQUEST['elemid']."' and workday='".$workers[$i]['workday']."' ");
			$awt=$awt+$wt;
			$adc=$adc+$dc;
			echo"<tr>";
			echo"<td><a href=\"/modules/goodworkers/timesheet/".$workers[$i]['id']."#tab-2\">".$workers[$i]['id']."</a></td>";	
			echo"<td class=\"text-center\">".$workers[$i]['workday']."</td>";	
			echo"<td class=\"text-right\" title=\"".$workers[$i]['workcomm']."\">".round($wt,2)." óra</td>";	
			echo"<td class=\"text-right\">".$dc." Ft</td>";	
			echo"</tr>";	
		}
		echo"<tr>";
			echo"<td>&nbsp;</td>";	
			echo"<td class=\"text-center\">Összesen:</td>";	
			echo"<td class=\"text-right\">".round($awt,2)." óra</td>";	
			echo"<td class=\"text-right\"><b>".$adc." Ft</b></td>";	
			echo"</tr>";	
		echo"</tbody>";
		echo"</table>";		
	echo"</div>";
echo"</div>";
$sutik=db_all("select * from gw_worker_cookies where worker_id='".$_REQUEST['elemid']."' ".$dat3." and status=1  order by id desc");
if(count($sutik)>0){
echo"<div class=\"block\">
	<div class=\"block-title\">
		<h2><strong>Sütemények ".$dat2."</strong></h2>
	</div>";
	echo"<div>";
	echo"<table class=\"table table-vcenter table-striped\">";
		echo"<thead>";
			echo"<tr>";
				echo"<th style=\"width: 100px;\" class=\"text-center\"><i class=\"gi gi-disk_save\"></i></th>";
				echo"<th class=\"text-center\">Dátum</th>";
				echo"<th class=\"text-center\">Sütemény neve:</th>";
				echo"<th class=\"text-center\">Fizetendő összeg</th>";
				echo"<th class=\"text-center\">Művelet</th>";
			echo"</tr>";
		echo"</thead>";
		echo"<tbody >";
		$sp=0;
		for($i=0;$i<count($sutik);$i++){
			$sp=$sp+$sutik[$i]['price'];
			echo"<tr>";
			echo"<td>".$sutik[$i]['id']."</td>";	
			echo"<td class=\"text-center\">".$sutik[$i]['mdate']."</td>";	
			echo"<td class=\"text-left\">".idchange("product_types","name",$sutik[$i]['pt_id'])."</td>";	
			echo"<td class=\"text-right\">".$sutik[$i]['price']." Ft</td>";	
			echo"<td>";
			echo"
				<div class=\"btn-group btn-group-xs\">";
					if($sutik[$i]['fizetve']=='0000-00-00'){echo"<a href=\"javascript:paycookieadat('".$sutik[$i]['id']."','".$_REQUEST['elemid']."');\" data-toggle=\"tooltip\" title=\"pay\" class=\"btn btn-info\"><i class=\"fa fa-usd\"></i></i></a>";}
					if($sutik[$i]['fizetve']=='0000-00-00'){echo"<a href=\"javascript:delcookieadat('".$sutik[$i]['id']."','".$_REQUEST['elemid']."','".$_REQUEST['targetdiv']."');\" data-toggle=\"tooltip\" title=\"Delete\" class=\"btn btn-danger\"><i class=\"fa fa-times\"></i></a>";}
				echo"</div>";
			echo"</td>";	
			echo"</tr>";	
		}
		echo"<tr>";
			echo"<td>&nbsp;</td>";	
			echo"<td class=\"text-center\" colspan=\"2\">Összesen:</td>";	
			echo"<td class=\"text-right\"><b>".$sp." Ft</b></td>";	
			echo"</tr>";	
		echo"</tbody>";
		echo"</table>";		
	echo"</div>";
echo"</div>";
}

$loans=db_all("select * from gw_worker_loans where worker_id='".$_REQUEST['elemid']."' ");
$elems=db_all("select * from gw_worker_loans where worker_id='".$_REQUEST['elemid']."' and status=1 order by id asc");
$tartozik=db_one("select sum(loan) from gw_worker_loans where worker_id='".$_REQUEST['elemid']."'  and fizetve='0000-00-00'");
$kovetel=db_one("select sum(redemption) from gw_worker_loans where worker_id='".$_REQUEST['elemid']."'  and fizetve='0000-00-00'");
$esedekes=db_one("select sum(redemption) from gw_worker_loans where worker_id='".$_REQUEST['elemid']."'  ".$dat4."   and status=1");
if($esedekes==""){$esedekes=0;}
//echo "select sum(redemption) from gw_worker_loans where ".$dat4." and fizetve!='0000-00-00' and status=1";
//if($tartozik!=$kovetel){
	if(($tartozik-$kovetel)<0){$tart=" tartozás";	}else{$tart=" hitelezés";	}
echo"<div class=\"block\">
	<div class=\"block-title\">
		<h2><strong>Pénzügyi események </strong> - Egyenleg: ".huf($kovetel-$tartozik)." Ft - ".$tart."</h2>
	</div>";
	
	echo"<div>";
	echo"<table class=\"table table-vcenter table-striped\">";
	?>
	<thead>
		<tr>
			<th style="width: 100px;" class="text-center"><i class="gi gi-disk_save"></i></th>
			<th class="text-center">Terhelés típusa</th>
			<th class="text-center">Terhelés összege</th>
			<th class="text-center">Törlesztés</th>
			<th class="text-center">Tervezett Dátum</th>
			<th class="text-center">Megjegyzés</th>
			<th class="text-center">Státusz</th>
			<th class="text-center">Művelet</th>
		</tr>
	</thead>
	<?
	if(count($elems)>0){
		for($i=0;$i<count($elems);$i++){
			$fiz="Nincs fizetve";if($elems[$i]['fizetve']!='0000-00-00'){$fiz="Fizetve: ".$elems[$i]['fizetve'];}
			if($elems[$i]['loan_type']=="hitel"){$lt="Hitel";}
			elseif($elems[$i]['loan_type']=="torlesztes"){$lt="Törlesztés";}
			elseif($elems[$i]['loan_type']=="eloleg"){$lt="Előleg";}
			elseif($elems[$i]['loan_type']=="szankcio"){$lt="Szankció";}
			elseif($elems[$i]['loan_type']=="letiltas"){$lt="Letiltás";}
			echo"<tr > ";
			echo"<td style=\"width: 100px;\" class=\"text-center\">".$elems[$i]['id']."</td>";
			echo"<td class=\"text-center\">".$lt."</td>";
			echo"<td class=\"text-center\">".$elems[$i]['loan']." Ft</td>";
			echo"<td class=\"text-center\">".$elems[$i]['redemption']." Ft</td>";
			echo"<td class=\"text-center\">".$elems[$i]['mydate']."</td>";
			echo"<td class=\"text-center\">".$elems[$i]['comment']."</td>";
			echo"<td class=\"text-center\">".$fiz."</td>";
			echo"<td>";
			echo"
				<div class=\"btn-group btn-group-xs\">";
					if($elems[$i]['fizetve']=='0000-00-00'){echo"<a href=\"javascript:payloanadat('".$elems[$i]['id']."','".$_REQUEST['elemid']."');\" data-toggle=\"tooltip\" title=\"pay\" class=\"btn btn-info\"><i class=\"fa fa-usd\"></i></i></a>";}
					if($elems[$i]['fizetve']=='0000-00-00'){echo"<a href=\"javascript:delloanadat('".$elems[$i]['id']."','".$_REQUEST['elemid']."','".$_REQUEST['targetdiv']."');\" data-toggle=\"tooltip\" title=\"Delete\" class=\"btn btn-danger\"><i class=\"fa fa-times\"></i></a>";}
				echo"</div>";
			echo"</td>";
			echo"</tr>";
		}
	}
		
	echo"</table>";
	echo"</div>";
	echo"<div style=\"clear:both;\"></div>";
echo"</div>";
//}


echo"<div class=\"block\">
	<div class=\"block-title\">
		<h2><strong>Összesen</strong></h2>
	</div>";
	echo"<dl class=\"dl-horizontal\">";
		echo"<dt>Munkabér:</dt><dd>".huf($adc)." Ft</dd>";
		echo"<dt>Sütemény:</dt><dd>".huf($sp)." Ft</dd>";
		echo"<dt>Előleg:</dt><dd>-".huf($esedekes)." Ft</dd>";
		echo"<dt>Összesen:</dt><dd>".huf($adc-$sp-$esedekes)." Ft</dd>";
	echo"</dl>";
	echo"<div style=\"clear:both;\"></div>";
echo"</div>";

	
$check=db_row("select * from gw_payments where worker_id='".$_REQUEST['elemid']."' and payment_month='".$_POST['month']."'");	

if($check==""){
	$kadate=date("Y-m-d",time());		
	$kafiz="";		
	$katxt="";		
}
else{
	$kadate=$check['payment_date'];		
	$kafiz=$check['payment_real'];		
	$katxt="Kiadva";
}	
echo"<div class=\"block\">
	<div class=\"block-title\">
		<h2><strong>Fizetés</strong></h2>
	</div>";
	echo "<form id=\"fizetes\" name=\"fizetes\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";

	echo "<div class=\"form-group\" style=\"width:300px;float:left;\">
		<label class=\"col-md-6 control-label\" for=\"payment_real\">Kiadott fizetés:</label>
		<div class=\"col-md-6\">
			<input type=\"text\" id=\"payment_real\" name=\"payment_real\" class=\"form-control\" value=\"".$kafiz."\" />																		
		</div>
	</div>";

	echo "<div class=\"form-group\" style=\"width:300px;float:left;\">
		<label class=\"col-md-6 control-label\" for=\"payment_date\">Kiadás ideje:</label>
		<div class=\"col-md-6\">
			<input type=\"text\" id=\"payment_date\" name=\"payment_date\" class=\"form-control date-picker\" value=\"".$kadate."\" />																		
		</div>
	</div>";
	echo "<div class=\"form-group form-actions\" style=\"width:100px;float:left;\">
		<div >
			<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savepay('".$_REQUEST['elemid']."','".$_POST['month']."','".($adc-$sp-$esedekes)."');\"><i class=\"fa fa-angle-right\"></i> Ment</button>
		</div>
	</div>";								
	echo "<div id=\"fizflag\" style=\"width:100px;float:left;\">".$katxt."</div>";											
	echo"<div style=\"clear:both;\"></div>";
echo"</div>";
mysql_close($connid);
?>

