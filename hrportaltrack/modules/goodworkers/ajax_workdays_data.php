<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors','Off');
header("Cache-control: private");
header('Content-Type: text/xml;charset=utf-8');
include_once dirname(__FILE__).("/../../inc/sys.conf");
include_once dirname(__FILE__).("/../../inc/_functions.php");

$myname = 'workdays';
$saved = 1;
$errmsg = array();
$statuscode = 0;

if($_POST['newsubmit']=='1'){
	$check=db_one("SELECT id FROM gw_workdays WHERE name = '".$_POST['name']."'");
	if($check==""){
		$maxnap=db_one("select name from gw_workdays order by name desc limit 1");
		$st=0;if(isset($_POST['status']) && $_POST['status']=="on"){$st=1;}
		$insid=db_execute("insert into gw_workdays (name,status) value ('".addslashes($_POST['name'])."','".$st."')");
		$_POST['id']=mysql_insert_id();
		
		if($insid==1){
			$workers=db_all("select * from gw_workers where status=1 order by name asc");
			for($i=0;$i<count($workers);$i++){
				$lastdata=db_one("select id from gw_shift_x_worker where workday='".$_POST['name']."' and worker_id='".$workers[$i]['id']."'");
				if($lastdata==""){
					db_execute("insert into gw_shift_x_worker (workday,worker_id) values('".$_POST['name']."','".$workers[$i]['id']."')");
				}
				//$lastdata=db_row("select * from gw_shift_x_worker where workday='".$maxnap."' and worker_id='".$workers[$i]['id']."'");
				//if($lastdata['id']!=""){
				//	db_execute("insert into gw_shift_x_worker 
				//	(workday,worker_id,post_id,starttime,endtime,worktime,dailycash) values
				//	('".$_POST['name']."','".$workers[$i]['id']."','".$lastdata['post_id']."','".$lastdata['starttime']."','".$lastdata['endtime']."','".$lastdata['worktime']."','".$lastdata['dailycash']."')");
				//}
				//else{
					//db_execute("insert into gw_shift_x_worker (workday,worker_id) values('".$_POST['name']."','".$workers[$i]['id']."')");
				//}
			}
			
			
			$errmsg[]=eeea("msg","Sikeres felvitel.");
			
			$_POST['view']='edit';
			$js="document.location='modules/goodworkers/workdays/".$_POST['id']."'";
		}
		else{
			$errmsg[]=eeea("error","Felvitel sikertelen!".$q.")");
			$saved=0;
			$statuscode = 1;
			$_POST['view']='edit';
		}
	}
	else{
		$errmsg[]=eeea("error","Ez a nap már rögzítve van a rendszerben!");
		$saved=0;
		$statuscode = 1;
		$_POST['new']=1;
	}
}
elseif(is_numeric($_POST['editsubmit'])){
	$saved=0;
	$_POST['edit']=$_POST['editsubmit'];
	$st=0;if(isset($_POST['status']) && $_POST['status']=="on"){$st=1;}
	$updid=db_execute("UPDATE gw_workdays SET name='".$_POST['name']."',status='".$st."' WHERE id = '".$_POST['editsubmit']."'");
	$workers=db_all("select * from gw_workers where status=1 order by name asc");
	for($i=0;$i<count($workers);$i++){
		$lastdata=db_one("select id from gw_shift_x_worker where workday='".$_POST['name']."' and worker_id='".$workers[$i]['id']."'");
		if($lastdata==""){
			db_execute("insert into gw_shift_x_worker (workday,worker_id) values('".$_POST['name']."','".$workers[$i]['id']."')");
		}
	}
	if($updid==1){
		$errmsg[]=eeea("msg","Sikeres módosítás");
	}
	else{
		$errmsg[]=eeea("error","Sikertelen változtatás!");
		$saved=0;
		$statuscode = 1;
	}
}
elseif(is_numeric($_POST['saveworkdaypost'])){
	$saved=0;
	$updid=db_execute("UPDATE gw_shift_x_worker SET post_id='".$_POST['post_id']."' WHERE id = '".$_POST['elemid']."'");
	if($updid==1){
		$errmsg[]=eeea("msg","Sikeres módosítás");
	}
	else{
		$errmsg[]=eeea("error","Sikertelen változtatás!");
		$saved=0;
		$statuscode = 1;
	}
}
elseif(is_numeric($_POST['saveworkdaytime'])){
	$saved=0;
	$st=$_POST['nap']." ".html_entity_decode($_POST['st']).":00";
	$en=$_POST['nap']." ".html_entity_decode($_POST['en']).":00";
	
	$sta=strtotime($st);
	$ste=strtotime($en);
	if($ste<$sta){

		$sta=$sta-86400;
		$st=date("Y-m-d H:i:s",$sta);
	}
	$wt=($ste-$sta)/3600;
	$worker=db_one("select worker_id from gw_shift_x_worker where id='".$_POST['elemid']."'");
	//echo "select worker_id from gw_shift_x_worker where id='".$_POST['elemid']."'";
	$fizu=db_one("select price_base from gw_worker_payments where worker_id='".$worker."' and status=1");
	$updid=db_execute("UPDATE gw_shift_x_worker SET starttime='".$st."',endtime='".$en."',worktime='".$wt."',dailycash='".round($wt*$fizu)."',post_date=NOW() WHERE id = '".$_POST['elemid']."'");
	if($updid==1){
		$errmsg[]=eeea("msg","Sikeres módosítás");
	}
	else{
		$errmsg[]=eeea("error","Sikertelen változtatás!");
		$saved=0;
		$statuscode = 1;
	}
}

if($_REQUEST['command']=="savesetupscreen"){
	if($_REQUEST['viewtype']=="setupscreen"){
		$elemek="";
		foreach($_POST as $nev => $defs){
			if(substr($nev,0,4)=="AAA_"){
				$elemek.=substr($nev,4).",";	
			}
		}
		$elemek=substr($elemek,0,-1);
		db_execute("update listoptions set pagelimit='".$_REQUEST['mylimit']."',screenlist='".$elemek."' where user_id='".$_SESSION['planetsys']['user_id']."' and modulname='".$_SESSION['planetsys']['actpage']['modulname']."' and funcname='".$_SESSION['planetsys']['actpage']['funcname']."'");
		$js.="MuszakDatatables.init();";
	}
}

echo eksemel($statuscode,$errmsg,$html,$js);

?>
