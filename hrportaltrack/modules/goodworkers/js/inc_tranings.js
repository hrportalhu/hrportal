/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */

	
var traningsDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/goodworkers/tranings/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"aaSorting": [[ 1, "asc" ]],
				"iDisplayLength": -1,
				"bDestroy": true,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/goodworkers/ajax_tranings_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });



            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function setteaching(elemid,worker_id){
	
		f_xml(ServerName +'/modules/goodworkers/ajax_tranings_data.php','view=setteaching&worker_id='+worker_id+'&id=' + elemid +'&teachingsubmit='+elemid,'');
	
}

function saveedittranings(elemid){
	var valid = f_c($('#edittranings'));
	if(valid){
		var traning_commentText = CKEDITOR.instances.traning_comment.getData();
		f_xml(ServerName +'/modules/goodworkers/ajax_tranings_data.php','view=edit&treningtxt='+traning_commentText+'&id=' + elemid +'&editsubmit='+elemid+'&' + $('#edittranings').serialize(),'');
	}
}


function savenewtranings(){
	var valid = f_c($('#newtranings'));
	if(valid){
		var traning_commentText = CKEDITOR.instances.traning_comment.getData();
		f_xml(ServerName +'/modules/goodworkers/ajax_tranings_data.php','newsubmit=1&treningtxt='+traning_commentText+'&' + $('#newtranings').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/goodworkers/ajax_tranings_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}

