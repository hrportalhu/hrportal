/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */

var workdaysDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/goodworkers/workdays/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"aaSorting": [[ 1, "desc" ]],
				"iDisplayLength": 30,
				"bDestroy": true,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/goodworkers/ajax_workdays_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					aoData.push({"name": "name", "value":$("#name").val() });
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });
            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveeditworkdays(elemid){
	var valid = f_c($('#editmuszak'));
	if(valid){
		f_xml(ServerName +'/modules/goodworkers/ajax_workdays_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editworkdays').serialize(),'');
	}
}

function savenewworkdays(){
	var valid = f_c($('#newmuszak'));
	if(valid){
		f_xml(ServerName +'/modules/goodworkers/ajax_workdays_data.php','newsubmit=1&' + $('#newworkdays').serialize(),'');
	}
}

function saveworkdaypost(elemid){
	f_xml(ServerName +'/modules/goodworkers/ajax_workdays_data.php','saveworkdaypost=1&elemid=' + elemid + '&post_id='+ $('#post_'+elemid).val(),'');
}
var nap;
function saveworkdaytime(elemid,nap){
	f_xml(ServerName +'/modules/goodworkers/ajax_workdays_data.php','saveworkdaytime=1&elemid=' + elemid + '&st='+ $('#st_'+elemid).val()+ '&en='+ $('#en_'+elemid).val()+ '&nap='+ nap,'');
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/goodworkers/ajax_workdays_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}

function showdailyworktime(elemid,targetdiv){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goodworkers/ajax_showdailyworktime.php',
		data:'elemid='+elemid+'&mypost_id='+$('#mypost_id').val(),
		success: function(data){
			$('#'+targetdiv).html(data);
		}});
}
