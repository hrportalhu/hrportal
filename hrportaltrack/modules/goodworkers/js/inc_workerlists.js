
function showszemuvegeslist(){
	$('#workerlist1').html('<center><i class="fa fa-sun-o fa-spin fa-4x"></i></center>');
	
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goodworkers/ajax_showszemuvegeslist.php',
		data:'elemid=1',
		success: function(data){
			$('#workerlist1').html(data);
			
		}
	});	
}



function showeukonyvlist(){
	$('#workerlist2').html('<center><i class="fa fa-sun-o fa-spin fa-4x"></i></center>');
	
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goodworkers/ajax_showeukonyvlist.php',
		data:'elemid=1',
		success: function(data){
			$('#workerlist2').html(data);
			
		}
	});	
}

function showmunkabersum(){
	$('#workerlist3').html('<center><i class="fa fa-sun-o fa-spin fa-4x"></i></center>');
	
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goodworkers/ajax_showmunkabersum.php',
		data:'elemid=1'+'&month='+$('#startDate').val(),
		success: function(data){
			$('#workerlist3').html(data);
			
		}
	});	
}

function showszabisum(){
	$('#workerlist4').html('<center><i class="fa fa-sun-o fa-spin fa-4x"></i></center>');
	
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goodworkers/ajax_showszabisum.php',
		data:'elemid=1'+'&month='+$('#startDate4').val(),
		success: function(data){
			$('#workerlist4').html(data);
			
		}
	});	
}

$(function() {
    $(".monthpicker").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: "yy-mm",
		showButtonPanel: true,
		currentText: "This Month",
		onChangeMonthYear: function (year, month, inst) {
			$(this).val($.datepicker.formatDate('yy-mm', new Date(year, month - 1, 1)));
		},
		onClose: function(dateText, inst) {
			var month = $(".ui-datepicker-month :selected").val();
			var year = $(".ui-datepicker-year :selected").val();
			$(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
			showmunkabersum();
		}
		}).focus(function () {
			$(".ui-datepicker-calendar").hide();
		}).after(
			$("<a href='javascript: void(0);'>clear</a>").click(function() {
				$(this).prev().val('');
			})
		);
    $(".monthpicker2").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: "yy-mm",
		showButtonPanel: true,
		currentText: "This Month",
		onChangeMonthYear: function (year, month, inst) {
			$(this).val($.datepicker.formatDate('yy-mm', new Date(year, month - 1, 1)));
		},
		onClose: function(dateText, inst) {
			var month = $(".ui-datepicker-month :selected").val();
			var year = $(".ui-datepicker-year :selected").val();
			$(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
			showszabisum();
		}
		}).focus(function () {
			$(".ui-datepicker-calendar").hide();
		}).after(
			$("<a href='javascript: void(0);'>clear</a>").click(function() {
				$(this).prev().val('');
			})
		);
});
