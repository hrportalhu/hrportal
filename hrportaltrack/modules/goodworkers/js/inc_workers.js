/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */

	
var workersDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/goodworkers/workers/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"aaSorting": [[ 2, "asc" ]],
				"iDisplayLength": -1,
				"bDestroy": true,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/goodworkers/ajax_workers_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					aoData.push({"name": "active", "value":$("#active").val() });
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });



            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveeditscreenworkers(elemid){
	var valid = f_c($('#editworkers'));
	if(valid){
		f_xml(ServerName +'/modules/goodworkers/ajax_workers_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editworkers').serialize(),'');
	}
}


function savenewscreenworkers(){
	var valid = f_c($('#newworkers'));
	if(valid){
		f_xml(ServerName +'/modules/goodworkers/ajax_workers_data.php','newsubmit=1&' + $('#newworkers').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/goodworkers/ajax_workers_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}

function showshifttable(elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goodworkers/ajax_showshifttable.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#shiftdiv').html(data);
		}});
}
function showpaymenttable(elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goodworkers/ajax_showpaymenttable.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#paymentcont').html(data);
			
		}
	});	
}

function saveworkerspayrow(elemid){
	//if(confirm("Biztos módosítod ezt a munkaadatot?")){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goodworkers/ajax_saveworkerspayrow.php',
		data:'elemid='+elemid
			+'&post='+$('#post_'+elemid).val()
			+'&st='+$('#st_'+elemid).val()
			+'&en='+$('#en_'+elemid).val()
		,
		success: function(data){
			$('#wsrow_'+elemid).html(data);
			//showpaymenttable(elemid);
		}
	});	
	//}
}
function saveworkersminwork(elemid){
	//if(confirm("Biztos módosítod ezt a munkaadatot?")){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goodworkers/ajax_saveworkersminwork.php',
		data:'elemid='+elemid
			+'&post='+$('#post_'+elemid).val()
			+'&st='+$('#st_'+elemid).val()
			+'&en='+$('#en_'+elemid).val()
		,
		success: function(data){
			$('#wsrow_'+elemid).html(data);
			//showpaymenttable(elemid);
		}
	});	
	//}
}
function showtraningtable(elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goodworkers/ajax_showtraningtable.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#traningcont').html(data);
			
		}
	});	
}
function savepaymentdata(elemid){
	if(confirm("Biztos mented ezt az oktatási bejegyzést?")){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goodworkers/ajax_savepaymentdata.php',
		data:'elemid='+elemid
			+'&newprice='+$('#newprice').val()
			+'&newpricecateg='+$('#newpricecateg').val()
			+'&startdate='+$('#startdate').val()
		,
		success: function(data){
			showpaymenttable(elemid);
		}
	});	
	}
}
function savetraningdata(elemid){
	if(confirm("Biztos mented ezt az oktatási bejegyzést?")){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goodworkers/ajax_savetraningdata.php',
		data:'elemid='+elemid
			+'&newtraning='+$('#newtraning').val()
			+'&startdate='+$('#startdate').val()
		,
		success: function(data){
			showtraningtable(elemid);
		}
	});	
	}
}

function deltreningadat(delid,elemid){
	if(confirm("Biztos törlöd az oktatási bejegyzést?")){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goodworkers/ajax_deltreningadat.php',
		data:'elemid='+elemid
			+'&delid='+delid
		,
		success: function(data){
			showtraningtable(elemid);
		}
	});	
	}
}
function edittreningadat(editid,elemid){
	
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goodworkers/ajax_edittreningadat.php',
		data:'elemid='+elemid
			+'&editid='+editid
		,
		success: function(data){
			$('#treningdataelem').html(data);
		}
	});	

}
function saveedittraningdata(editid,elemid){
	if(confirm("Biztos mented ezt az oktatási bejegyzést?")){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goodworkers/ajax_saveedittraningdata.php',
		data:'elemid='+elemid
			+'&editid='+editid
			+'&newtraning='+$('#newtraning').val()
			+'&startdate='+$('#startdate').val()
		,
		success: function(data){
			showtraningtable(elemid);
		}
	});	
	}
}

function showcookiestable(elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goodworkers/ajax_showcookiestable.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#cookiescont').html(data);
			
		}
	});	
}

function showcookiestable2(elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goodworkers/ajax_showcookiestable2.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#cookiescont2').html(data);
			
		}
	});	
}
function calksutiprice(){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goodworkers/ajax_calksutiprice.php',
		data:'newpt='+$('#newpt').val()
			+'&newsutime='+$('#newsutime').val(),
		success: function(data){
			$('#newsutipenz').val(data);
			
		}
	});	
}
function calksutiprice2(){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goodworkers/ajax_calksutiprice.php',
		data:'newpt='+$('#newpt2').val()
			+'&newsutime='+$('#newsutime2').val(),
		success: function(data){
			$('#newsutipenz2').val(data);
			
		}
	});	
}
function savecookiesdata(elemid,cucc){
	if(confirm("Biztos rögzíted a bejegyzést?")){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goodworkers/ajax_savecookiesdata.php',
		data:'elemid='+elemid
			+'&newpt='+$('#newpt').val()
			+'&newsutime='+$('#newsutime').val()
			+'&newsutipenz='+$('#newsutipenz').val()
			+'&startsutidate='+$('#startsutidate').val()
		,
		success: function(data){
				showcookiestable(elemid);
		}
	});	
	}
}
function savecookiesdata2(elemid,cucc){
	if(confirm("Biztos rögzíted a bejegyzést?")){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goodworkers/ajax_savecookiesdata.php',
		data:'elemid='+elemid
			+'&newpt='+$('#newpt2').val()
			+'&newsutime='+$('#newsutime2').val()
			+'&newsutipenz='+$('#newsutipenz2').val()
			+'&startsutidate='+$('#startsutidate2').val()
		,
		success: function(data){
				showcookiestable2(elemid);
		}
	});	
	}
}
function saveeditcookiesdata(editid,elemid){
	if(confirm("Biztos módosítod  a bejegyzést?")){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goodworkers/ajax_saveeditcookiesdata.php',
		data:'elemid='+elemid
			+'&editid='+editid
			+'&newpt='+$('#newpt').val()
			+'&newsutime='+$('#newsutime').val()
			+'&newsutipenz='+$('#newsutipenz').val()
			+'&startsutidate='+$('#startsutidate').val()
		,
		success: function(data){
			showcookiestable(elemid);
		}
	});	
	}
}
function editcookieadat(editid,elemid){
	
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goodworkers/ajax_editcookieadat.php',
		data:'elemid='+elemid
			+'&editid='+editid
		,
		success: function(data){
			$('#cookiesdataelem').html(data);
		}
	});	

}
function paycookieadat(editid,elemid){
	if(confirm("Biztos kifizették a sütikét?")){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goodworkers/ajax_paycookieadat.php',
		data:'elemid='+elemid
			+'&editid='+editid
		,
		success: function(data){
			showcookiestable(elemid);
		}
	});	
	}
}
function delcookieadat(editid,elemid){
	if(confirm("Biztos törlöd a bejegyzést?")){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goodworkers/ajax_delcookieadat.php',
		data:'elemid='+elemid
			+'&editid='+editid
		,
		success: function(data){
			showcookiestable(elemid);
		}
	});	
	}
}

function showmonthsum(){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goodworkers/ajax_showmonthsum.php',
		data:'elemid='+$('#worker_id').val()+'&month='+$('#startDate').val(),
		success: function(data){
			$('#monthsum').html(data);
		}
	});	
}
var month,fizu;
function savepay(elemid,month,fizu){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goodworkers/ajax_savepay.php',
		data:'worker_id='+elemid+'&payment_real='+$('#payment_real').val()+'&payment_date='+$('#payment_date').val()+'&month='+month+'&fizu='+fizu,
		success: function(data){
			$('#fizflag').html(data);
		}
	});	
}

function showloantable(elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goodworkers/ajax_showloantable.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#loancont').html(data);
			$('#mydate').datepicker({
		closeText: 'bezárás',
		prevText: '&laquo;&nbsp;vissza',
		nextText: 'előre&nbsp;&raquo;',
		currentText: 'ma',
		monthNames: ['Január', 'Február', 'Március', 'Április', 'Május', 'Június',
		'Július', 'Augusztus', 'Szeptember', 'Október', 'November', 'December'],
		monthNamesShort: ['Jan', 'Feb', 'Már', 'Ápr', 'Máj', 'Jún',
		'Júl', 'Aug', 'Szep', 'Okt', 'Nov', 'Dec'],
		dayNames: ['Vasárnap', 'Hétfö', 'Kedd', 'Szerda', 'Csütörtök', 'Péntek', 'Szombat'],
		dayNamesShort: ['Vas', 'Hét', 'Ked', 'Sze', 'Csü', 'Pén', 'Szo'],
		dayNamesMin: ['V', 'H', 'K', 'Sze', 'Cs', 'P', 'Szo'],
		weekHeader: 'Hé',
		dateFormat: 'yy-mm-dd',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''});
		}
	});	
}
function saveloandata(elemid){
	if(confirm("Biztos rögzíted a bejegyzést?")){
		var a=0;
		if($('#loan_type').val()=="torlesztes" && $('#loan').val()!=''){
			a="A törlesztés nem lehet kiadás!";
		}
		if(a==0){
			$.ajax({
				type:'POST',
				url: ServerName +'/modules/goodworkers/ajax_saveloandata.php',
				data:'elemid='+elemid
					+'&loan_type='+$('#loan_type').val()
					+'&loan='+$('#loan').val()
					+'&redemption='+$('#redemption').val()
					+'&mydate='+$('#mydate').val()
					+'&comment='+$('#comment').val()
				,
				success: function(data){
					showloantable(elemid);
				}
			});	
			}
			else{
				alert(a);
			}	
		}
}

function editloanadat(editid,elemid){
	
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goodworkers/ajax_editloanadat.php',
		data:'elemid='+elemid
			+'&editid='+editid
		,
		success: function(data){
			$('#loandataelem').html(data);
		}
	});	

}
function saveeditloandata(editid,elemid){
	if(confirm("Biztos módosítod  a bejegyzést?")){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goodworkers/ajax_saveeditloandata.php',
		data:'elemid='+elemid
			+'&editid='+editid
			+'&loan_type='+$('#loan_type').val()
			+'&loan='+$('#loan').val()
			+'&redemption='+$('#redemption').val()
			+'&mydate='+$('#mydate').val()
			+'&comment='+$('#comment').val()
		,
		success: function(data){
			showloantable(elemid);
		}
	});	
	}
}
function delloanadat(editid,elemid){
	if(confirm("Biztos törlöd a bejegyzést?")){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goodworkers/ajax_delloanadat.php',
		data:'elemid='+elemid
			+'&editid='+editid
		,
		success: function(data){
			showloantable(elemid);
		}
	});	
	}
}
function payloanadat(editid,elemid){
	if(confirm("Biztos kiegyenlíted bejegyzést?")){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/goodworkers/ajax_payloanadat.php',
		data:'elemid='+elemid
			+'&editid='+editid
		,
		success: function(data){
			showloantable(elemid);
		}
	});	
	}
}
$(function() {
    $(".monthpicker").datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: "yy-mm",
    showButtonPanel: true,
    currentText: "This Month",
    onChangeMonthYear: function (year, month, inst) {
        $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month - 1, 1)));
    },
    onClose: function(dateText, inst) {
        var month = $(".ui-datepicker-month :selected").val();
        var year = $(".ui-datepicker-year :selected").val();
        $(this).val($.datepicker.formatDate('yy-mm', new Date(year, month, 1)));
        showmonthsum();
    }
}).focus(function () {
    $(".ui-datepicker-calendar").hide();
}).after(
    $("<a href='javascript: void(0);'>clear</a>").click(function() {
        $(this).prev().val('');
    })
);
});
