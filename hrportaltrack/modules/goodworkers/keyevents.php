
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Kulcs nyílvántartó
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/goodworkers/keyevents">Kulcs nyílvántartó</a></li>
				<li><a href="/new">Új kulcskiadás /visszavétel/</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új kulcskiadás /visszavétel/</strong></h2>
				</div>
			<?
			
				$dolgozo=db_all("select id, name from gw_workers where status=1 order by name");
				$kulcs=db_all("select id, name from gw_keys where status=1 order by name");
				$kulcshely=db_all("select id, name from gw_keyplace where status=1 order by name");
				
				echo "<form id=\"newkeyevents\" name=\"newkeyevents\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";
				
			
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"worker_id\">Dolgozó:</label>
							<div class=\"col-md-9\">
								<select id=\"worker_id\" name=\"worker_id\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($dolgozo);$i++){
									
									echo"<option value=\"".$dolgozo[$i]['id']."\" >".$dolgozo[$i]['name']."</option>";	
								}
								echo"</select>
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"place_id\">Kulcs elhelyezés:</label>
							<div class=\"col-md-9\">
								<select id=\"place_id\" name=\"place_id\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($kulcshely);$i++){
									
									echo"<option value=\"".$kulcshely[$i]['id']."\" >".$kulcshely[$i]['name']."</option>";	
								}
								echo"</select>
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"key_id\">Kulcs:</label>
							<div class=\"col-md-9\">
								<select id=\"key_id\" name=\"key_id\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($kulcs);$i++){
									
									echo"<option value=\"".$kulcs[$i]['id']."\" >".$kulcs[$i]['name']."</option>";	
								}
								echo"</select>
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"megjegyzes\">Megjegyzés:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"megjegyzes\" name=\"megjegyzes\" class=\"form-control\" placeholder=\"Megjegyzés\" />																		
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"trans_date\">Kiadás ideje:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"trans_date\" name=\"trans_date\" class=\"form-control input-datepicker\" value=\"".date("Y-m-d",time())."\" />																		
							</div>
						</div>";
						
						echo "<div class=\"form-group\" >
							<label class=\"col-md-3 control-label\" for=\"status\">Érvényes-e:</label>
							<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span></label>
						</div>";
						
						echo "<div style=\"clear:both;height:10px;\"></div>";
						
						echo "<div class=\"form-group form-actions\">
							<div class=\"col-md-9 col-md-offset-3\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewkeyevents();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>";
						
				echo "</div>
				</form>";
			?>
			</div>	
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/goodworkers/js/inc_keyevents.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?	
		}
		else{

			//echo "select * from functions where id='".$uritags[2]."'";
			$mykeyevents=db_row("select * from gw_keyevents where id='".$uritags[3]."'");
			if($mykeyevents['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
				$dolgozo=db_all("select id, name from gw_workers where status=1 order by name");
				$kulcs=db_all("select id, name from gw_keys where status=1 order by name");
				$kulcshely=db_all("select id, name from gw_keyplace where status=1 order by name");

?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="modules/goodworkers/keyevents"> Kulcs</a></li>
				<li><a href="/<?=$URI;?>"><?=$mykeyevents['id'];?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->

					<div class="block-title">
						<h2><strong><?=$mykeyevents['id'];?></strong> kulcsesemény oldal</h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
							if(priv("keyevents","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("keyevents","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							if(priv("keyevents","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("keyevents","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
							if(priv("keyevents","print")){echo"<li onclick=\"javascript:getprintscreen('".$_SESSION['planetsys']['actpage']['modulname']."','keyeventsprint','".$mykeyevents['id']."');\" ><a href=\"#modal-regular-print\" data-toggle=\"modal\" title=\"osszetevok print\"><i class=\"fa fa-print\" ></i></a></li>";}
							?>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="col-md-6">
								<dl class="dl-horizontal">
									<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$mykeyevents['id']); ?></dd>
									<dt>Dolgozó:</dt><dd><? echo idchange("gw_workers","name",$mykeyevents['worker_id']); ?></dd>
									<dt>Kulcshely:</dt><dd><? echo idchange("gw_keyplace","name",$mykeyevents['place_id']); ?></dd>
									<dt>Kulcs:</dt><dd><? echo idchange("gw_keys","name",$mykeyevents['key_id']); ?></dd>
									<dt>Kiadó felhasználó:</dt><dd><? echo idchange("users","realname",$mykeyevents['user_id']); ?></dd>
									<dt>Megjegyzés:</dt><dd><? echo $mykeyevents['megjegyzes']; ?></dd>
									<dt>Esemény dátuma:</dt><dd><? echo $mykeyevents['trans_date']; ?></dd>
									<dt>Státusz:</dt><dd><? if($mykeyevents['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
								</dl>	
							</div>
							<div class="col-md-6"></div>
							<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-2">
						<?
						
							
							echo"<form id=\"editkeyevents\" name=\"editkeyevents\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
							echo "<div style=\"margin-top:1px;\">";
							
									echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"workers_id\">Dolgozó:</label>
							<div class=\"col-md-9\">
								<select id=\"workers_id\" name=\"workers_id\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($dolgozo);$i++){
									$sel="";if($dolgozo[$i]['id']==$mykeyevents['worker_id']){$sel=" selected ";}
									echo"<option value=\"".$dolgozo[$i]['id']."\" >".$dolgozo[$i]['name']."</option>";	
								}
								echo"</select>
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"place_id\">Kulcs elhelyezés:</label>
							<div class=\"col-md-9\">
								<select id=\"place_id\" name=\"place_id\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($kulcshely);$i++){
									$sel="";if($kulcshely[$i]['id']==$mykeyevents['place_id']){$sel=" selected ";}
									echo"<option value=\"".$kulcshely[$i]['id']."\" >".$kulcshely[$i]['name']."</option>";	
								}
								echo"</select>
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"key_id\">Kulcs:</label>
							<div class=\"col-md-9\">
								<select id=\"key_id\" name=\"key_id\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($kulcs);$i++){
									$sel="";if($kulcs[$i]['id']==$mykeyevents['key_id']){$sel=" selected ";}
									echo"<option value=\"".$kulcs[$i]['id']."\" >".$kulcs[$i]['name']."</option>";	
								}
								echo"</select>
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"megjegyzes\">Megjegyzés:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"megjegyzes\" name=\"megjegyzes\" class=\"form-control\" values=\"".$mykeyevents['megjegyzes']."\" />	
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"trans_date\">Kiadás ideje:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"trans_date\" name=\"trans_date\" class=\"form-control input-datepicker\" value=\"".$mykeyevents['trans_date']."\" />																		
							</div>
						</div>";
						
								echo "<div class=\"form-group\" >
									<label class=\"col-md-3 control-label\" for=\"status\">Érvényes-e:</label>";
									$st="";if($mykeyevents['status']==1){ $st=" checked "; }
									echo "<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\"".$st." ><span></span></label>
								</div>";
								
								echo "<div class=\"form-group form-actions\">
									<div class=\"col-md-9 col-md-offset-3\">
										<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditkeyevents(".$mykeyevents['id'].")\"><i class=\"fa fa-angle-right\"></i> Ment</button>
									</div>
								</div>";
									
							echo "</div>
							</form>";
						?>
						</div>
						<div class="tab-pane" id="tab-3">
								<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mykeyevents['id'],"Dokumentum feltöltés")."</div>";
								?> 
								<div id="filelists"></div>
								<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mykeyevents['id'],"Hozzászólások")."</div>";
								?>
						</div>
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>
				
					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
					
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/goodworkers/js/inc_keyevents.js"></script>

			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mykeyevents['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$mykeyevents['id'];?>,'Dokumentum feltöltés','filelists');
			</script>
			<script>$(function(){ keyeventsDatatables.init(); });</script>	

			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
		//print_r($_SESSION['planetsys'])
?>    
<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Kulcs</a></li>
			</ul>
			<!-- END Datatables Header -->

			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Kulcs</strong> lista</h2>
				</div>
				
				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							//echo"<th>Művelet</th>";
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/goodworkers/js/inc_keyevents.js"></script>
		<script>$(function(){ keyeventsDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>
