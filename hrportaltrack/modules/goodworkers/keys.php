
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Kulcs kezelő
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/goodworkers/keys">Kulcs kezelő</a></li>
				<li><a href="/new">Új Kulcs felvitele</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új Kulcs felvitele</strong></h2>
				</div>
			<?
			
				
				
				echo "<form id=\"newkeys\" name=\"newkeys\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";
				
			
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"name\">Új Kulcs:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" placeholder=\"Új Kulcs neve\" />																		
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"code\">Új Kulcs kódja:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"code\" name=\"code\" class=\"form-control\" placeholder=\"Új Kulcs kódja\" />																		
							</div>
						</div>";
						
						echo "<div class=\"form-group\" >
							<label class=\"col-md-3 control-label\" for=\"status\">Érvényes-e:</label>
							<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span></label>
						</div>";
						
						echo "<div style=\"clear:both;height:10px;\"></div>";
						
						echo "<div class=\"form-group form-actions\">
							<div class=\"col-md-9 col-md-offset-3\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewkeys();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>";
						
				echo "</div>
				</form>";
			?>
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/goodworkers/js/inc_keys.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?	
		}
		else{

			//echo "select * from functions where id='".$uritags[2]."'";
			$mykeys=db_row("select * from gw_keys where id='".$uritags[3]."'");
			if($mykeys['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="modules/goodworkers/keys"> Kulcs</a></li>
				<li><a href="/<?=$URI;?>"><?=$mykeys['name'];?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->

					<div class="block-title">
						<h2><strong><?=$mykeys['name'];?></strong> Kulcs oldal</h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
							if(priv("keys","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("keys","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							if(priv("keys","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("keys","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
							if(priv("keys","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}	
							?>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="col-md-6">
								<dl class="dl-horizontal">
									<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$mykeys['id']); ?></dd>
									<dt>Név:</dt><dd><? echo $mykeys['name']; ?></dd>
									<dt>Státusz:</dt><dd><? if($mykeys['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
								</dl>	
							</div>
							<div class="col-md-6">
								<?php
									$pic=db_one("select filename from files where modul='goodworkers' and type='keys' and type_id='".$mykeys['id']."' order by cover desc");
									if(is_file($pic)){
										echo"<div class=\"col-sm-3\"><img src=\"/".$pic."\" alt=\"avatar\" width=\"450px\"></div>";
									}
									else{
										echo"<img src=\"img/placeholders/avatars/avatar9@2x.jpg\" alt=\"avatar\">";
									}
								?>
							</div>
							<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-2">
						<?
						
							
							echo"<form id=\"editkeys\" name=\"editkeys\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
							echo "<div style=\"margin-top:1px;\">";
							
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"name\">Kulcs:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" value=\"".$mykeys['name']."\" />																		
									</div>
								</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"code\">Kulcs kódja:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"code\" name=\"code\" class=\"form-control\" value=\"".$mykeys['code']."\" />																		
									</div>
								</div>";
								
								echo "<div class=\"form-group\" >
									<label class=\"col-md-3 control-label\" for=\"status\">Érvényes-e:</label>";
									$st="";if($mykeys['status']==1){ $st=" checked "; }
									echo "<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\"".$st." ><span></span></label>
								</div>";
								
								echo "<div class=\"form-group form-actions\">
									<div class=\"col-md-9 col-md-offset-3\">
										<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditkeys(".$mykeys['id'].")\"><i class=\"fa fa-angle-right\"></i> Ment</button>
									</div>
								</div>";
									
							echo "</div>
							</form>";
						?>
						</div>
						<div class="tab-pane" id="tab-3">
								<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mykeys['id'],"Dokumentum feltöltés")."</div>";
								?> 
								<div id="filelists"></div>
								<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mykeys['id'],"Hozzászólások")."</div>";
								?>
						</div>
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>
				
					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
					
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/goodworkers/js/inc_keys.js"></script>

			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mykeys['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$mykeys['id'];?>,'Dokumentum feltöltés','filelists');
			</script>
			<script>$(function(){ keysDatatables.init(); });</script>	

			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
		//print_r($_SESSION['planetsys'])
?>    
<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Kulcs</a></li>
			</ul>
			<!-- END Datatables Header -->

			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Kulcs</strong> lista</h2>
				</div>
				
				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							//echo"<th>Művelet</th>";
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/goodworkers/js/inc_keys.js"></script>
		<script>$(function(){ keysDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>
