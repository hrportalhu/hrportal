
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Eszközök kezelő
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/goodworkers/munkahelyieszkozok">Eszközök kezelő</a></li>
				<li><a href="/new">Új eszközök felvitele</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új eszközök felvitele</strong></h2>
				</div>
			<?
			
				$szervezetiegyseg=db_all("select id, name from szervezeti_egysegek where status=1 order by name");
				$eszkoztipus=db_all("select id, name from eszkoztipus where status=1 order by name");
				$munkahely=db_all("select id, name from gw_posts where status=1 order by name");
				$felelos=db_all("select id, username from users where status=1 order by username");
			
				echo "<form id=\"newmunkahelyieszkozok\" name=\"newmunkahelyieszkozok\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";

						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"name\">Új eszköz:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" placeholder=\"Új eszközök neve\" />																		
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"sorozatszam\">Sorozatszám:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"sorozatszam\" name=\"sorozatszam\" class=\"form-control\" placeholder=\" Sorozatszám\" />																		
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"user_id\">Felelős:</label>
							<div class=\"col-md-9\">
								<select id=\"user_id\" name=\"user_id\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($felelos);$i++){
									echo"<option value=\"".$felelos[$i]['id']."\" >".$felelos[$i]['username']."</option>";	
								}
								echo"</select>
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"szervezeti_id\">Szervezeti egység:</label>
							<div class=\"col-md-9\">
								<select id=\"szervezeti_id\" name=\"szervezeti_id\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($szervezetiegyseg);$i++){
									echo"<option value=\"".$szervezetiegyseg[$i]['id']."\" >".$szervezetiegyseg[$i]['name']."</option>";	
								}
								echo"</select>
							</div>
						</div>";
					
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"munkahely_id\">Munkahely:</label>
							<div class=\"col-md-9\">
								<select id=\"munkahely_id\" name=\"munkahely_id\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($munkahely);$i++){
									echo"<option value=\"".$munkahely[$i]['id']."\" >".$munkahely[$i]['name']."</option>";	
								}
								echo"</select>
							</div>
						</div>";
					
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"eszkoztipus_id\">Eszköztípus:</label>
							<div class=\"col-md-9\">
								<select id=\"eszkoztipus_id\" name=\"eszkoztipus_id\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($eszkoztipus);$i++){
									echo"<option value=\"".$eszkoztipus[$i]['id']."\" >".$eszkoztipus[$i]['name']."</option>";	
								}
								echo"</select>
							</div>
						</div>";
						
						echo "<div class=\"form-group\" >
							<label class=\"col-md-3 control-label\" for=\"gyartosor\">Gyártosor-e?:</label>
							<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"gyartosor\" name=\"gyartosor\"><span></span></label>
						</div>";
						
						echo "<div class=\"form-group\" >
							<label class=\"col-md-3 control-label\" for=\"status\">Érvényes-e:</label>
							<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span></label>
						</div>";
						
						echo "<div style=\"clear:both;height:10px;\"></div>";
						
						echo "<div class=\"form-group form-actions\">
							<div class=\"col-md-9 col-md-offset-3\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewmunkahelyieszkozok();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>";
						
				echo "</div>
				</form>";
			?>
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/goodworkers/js/inc_munkahelyieszkozok.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?	
		}
		else{

			//echo "select * from functions where id='".$uritags[2]."'";
			$myeszkozok=db_row("select * from eszkozok where id='".$uritags[3]."'");
			if($myeszkozok['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="modules/goodworkers/munkahelyieszkozok">Eszköz</a></li>
				<li><a href="/<?=$URI;?>"><?=$myeszkozok['name'];?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->

					<div class="block-title">
						<h2><strong><?=$myeszkozok['name'];?></strong> eszköz oldal</h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
							if(priv("munkahelyieszkozok","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("munkahelyieszkozok","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							if(priv("munkahelyieszkozok","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("munkahelyieszkozok","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
							if(priv("munkahelyieszkozok","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}	
							?>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="col-md-6">
								<dl class="dl-horizontal">
									<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$myeszkozok['id']); ?></dd>
									<dt>Név:</dt><dd><? echo $myeszkozok['name']; ?></dd>
									<dt>Sorozatszám:</dt><dd><? echo $myeszkozok['sorozatszam']; ?></dd>
									<dt>Felelős:</dt><dd><? echo idchange("users","username",$myeszkozok['user_id']); ?></dd>
									<dt>Szervezeti egység:</dt><dd><? echo idchange("szervezeti_egysegek","name",$myeszkozok['szervezeti_id']); ?></dd>
									<dt>Munkahely:</dt><dd><? echo idchange("gw_posts","name",$myeszkozok['munkahely_id']); ?></dd>
									<dt>Eszköztípus:</dt><dd><? echo idchange("eszkoztipus","name",$myeszkozok['eszkoztipus_id']); ?></dd>
									<dt>Státusz:</dt><dd><? if($myeszkozok['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
								</dl>	
							</div>
							<div class="col-md-6">
								<?php
									$pic=db_one("select filename from files where modul='development' and type='eszkozok' and type_id='".$myeszkozok['id']."' order by cover desc");
									if(is_file($pic)){
										echo"<div class=\"col-sm-3\"><img src=\"/".$pic."\" alt=\"avatar\" width=\"450px\"></div>";
									}
									else{
										echo"<img src=\"img/placeholders/avatars/avatar9@2x.jpg\" alt=\"avatar\">";
									}
								?>
							</div>
							<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-2">
						<?
						
								$szervezetiegyseg=db_all("select id, name from szervezeti_egysegek where status=1 order by name");
								$eszkoztipus=db_all("select id, name from eszkoztipus where status=1 order by name");
								$munkahely=db_all("select id, name from gw_posts where status=1 order by name");
								$felelos=db_all("select id, username from users where status=1 order by username");
							
							echo"<form id=\"editmunkahelyieszkozok\" name=\"editmunkahelyieszkozok\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
							echo "<div style=\"margin-top:1px;\">";
							
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"name\">Új eszköz :</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" value=\"".$myeszkozok['name']."\" />																		
									</div>
								</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"sorozatszam\"> Sorozatszám:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"sorozatszam\" name=\"sorozatszam\" class=\"form-control\" value=\"".$myeszkozok['sorozatszam']."\" />																		
									</div>
								</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"user_id\">Felelős:</label>
									<div class=\"col-md-9\">
										<select id=\"user_id\" name=\"user_id\" class=\"form-control\" >
										<option value=\"0\">Kérlek válassz</option>";
										for($i=0;$i<count($felelos);$i++){
											$sel="";if($felelos[$i]['id']==$myeszkozok['user_id']){$sel=" selected ";}
											echo"<option value=\"".$felelos[$i]['id']."\" ".$sel.">".$felelos[$i]['username']."</option>";
										}
										echo"</select>
									</div>
								</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"szervezeti_id\">Szervezeti egység:</label>
									<div class=\"col-md-9\">
										<select id=\"szervezeti_id\" name=\"szervezeti_id\" class=\"form-control\" >
										<option value=\"0\">Kérlek válassz</option>";
										for($i=0;$i<count($szervezetiegyseg);$i++){
											$sel="";if($szervezetiegyseg[$i]['id']==$myeszkozok['szervezeti_id']){$sel=" selected ";}
											echo"<option value=\"".$szervezetiegyseg[$i]['id']."\" ".$sel.">".$szervezetiegyseg[$i]['name']."</option>";
										}
										echo"</select>
									</div>
								</div>";
							
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"munkahely_id\">Munkahely:</label>
									<div class=\"col-md-9\">
										<select id=\"munkahely_id\" name=\"munkahely_id\" class=\"form-control\" >
										<option value=\"0\">Kérlek válassz</option>";
										for($i=0;$i<count($munkahely);$i++){
											$sel="";if($munkahely[$i]['id']==$myeszkozok['munkahely_id']){$sel=" selected ";}
											echo"<option value=\"".$munkahely[$i]['id']."\" ".$sel.">".$munkahely[$i]['name']."</option>";
										}
										echo"</select>
									</div>
								</div>";
							
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"eszkoztipus_id\">Eszköztípus:</label>
									<div class=\"col-md-9\">
										<select id=\"eszkoztipus_id\" name=\"eszkoztipus_id\" class=\"form-control\" >
										<option value=\"0\">Kérlek válassz</option>";
										for($i=0;$i<count($eszkoztipus);$i++){
											$sel="";if($eszkoztipus[$i]['id']==$myeszkozok['eszkoztipus_id']){$sel=" selected ";}
											echo"<option value=\"".$eszkoztipus[$i]['id']."\" ".$sel.">".$eszkoztipus[$i]['name']."</option>";
										}
										echo"</select>
									</div>
								</div>";
								
								echo "<div class=\"form-group\" >
									<label class=\"col-md-3 control-label\" for=\"gyartosor\">Gyártósor-e?:</label>";
									$st1="";if($myeszkozok['gyartosor']==1){ $st1=" checked "; }
									echo "<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"gyartosor\" name=\"gyartosor\"".$st1." ><span></span></label>
								</div>";
								
								echo "<div class=\"form-group\" >
									<label class=\"col-md-3 control-label\" for=\"status\">Érvényes-e?:</label>";
									$st="";if($myeszkozok['status']==1){ $st=" checked "; }
									echo "<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\"".$st." ><span></span></label>
								</div>";
								
								echo "<div class=\"form-group form-actions\">
									<div class=\"col-md-9 col-md-offset-3\">
										<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditmunkahelyieszkozok(".$myeszkozok['id'].")\"><i class=\"fa fa-angle-right\"></i> Ment</button>
									</div>
								</div>";
									
							echo "</div>
							</form>";
						?>
						</div>
						<div class="tab-pane" id="tab-3">
								<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$myeszkozok['id'],"Dokumentum feltöltés")."</div>";
								?> 
								<div id="filelists"></div>
								<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$myeszkozok['id'],"Hozzászólások")."</div>";
								?>
						</div>
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>
				
					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
					
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/goodworkers/js/inc_munkahelyieszkozok.js"></script>

			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$myeszkozok['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$myeszkozok['id'];?>,'Dokumentum feltöltés','filelists');
			</script>
			<script>$(function(){ MunkahelyieszkozokDatatables.init(); });</script>	

			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
		//print_r($_SESSION['planetsys'])
?>    
<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Eszközök</a></li>
			</ul>
			<!-- END Datatables Header -->

			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Eszközök</strong> lista</h2>
				</div>
				
				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							//echo"<th>Művelet</th>";
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/goodworkers/js/inc_munkahelyieszkozok.js"></script>
		<script>$(function(){ MunkahelyieszkozokDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>
