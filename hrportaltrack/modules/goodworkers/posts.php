
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Helyszín kezelő
            </h1>
        </div>
    </div>
<?

		if(isset($uritags[3]) && $uritags[3]!=""){
			if($uritags[3]=="new"){
				?>
				<!-- Ez egy elem  -->		
				<ul class="breadcrumb breadcrumb-top">
					<li><a href="/">Rendszer</a></li>
					<li><a href="/modules/goodworkerss/posts">Helyszín kezelő</a></li>
					<li><a href="/new">Új helyszín felvitele</a></li>
				</ul>
				<div class="block full">
				<?
				
					echo"<form id=\"newposts\" name=\"newposts\"  method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
					echo "<div style=\"margin-top:1px;\">";
							
						echo"<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"name\">Munkahely:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" placeholder=\"Munkahely elnevezése\"  />																		
							</div>
						</div>";
						echo"<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"icon\">Munkahely:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"icon\" name=\"icon\" class=\"form-control\" placeholder=\"Ikon\"  />																		
							</div>
						</div>";
						echo"<div class=\"form-group\" >";
							echo"<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span><br />Érvényes-e</label>";
						echo"</div>";
						echo"<div style=\"clear:both;height:10px;\"></div>";
						echo"<div class=\"form-group form-actions\">
							<div class=\"col-md-9 col-md-offset-3\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewposts();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>";	
					echo"</div>";
					echo"</form>";
				?>
				</div>	
				<?php include 'inc/page_footer.php'; ?>
				<?php include 'inc/template_scripts.php'; ?>
				<script src="modules/goodworkers/js/inc_posts.js"></script>
				<?php include 'inc/template_end.php'; ?>	
				<?	
			}
			else{
				
				//echo "select * from functions where id='".$uritags[2]."'";
				$myposts=db_row("select * from gw_posts where id='".$uritags[3]."'");
				if($myposts['id']==''){
					die("Helytelen hozzáférés!");
				}
				else{
	?>
				<!-- Ez egy elem  -->		
				<ul class="breadcrumb breadcrumb-top">
					<li><a href="/">Rendszer</a></li>
					<li><a href="modules/goodworkers/posts">Helyszínek</a></li>
					<li><a href="/<?=$URI;?>"><?=$myposts['name'];?></a></li>
				</ul>
				<div class="block full">
								   
						<!-- Block Tabs Title -->

						<div class="block-title">
							<h2><strong><?=$myposts['name'];?></strong> helyszín oldala</h2>
							<ul class="nav nav-tabs" data-toggle="tabs">
								<?
								
								if(priv("posts","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
								if(priv("posts","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
								if(priv("posts","instore")){echo"<li><a href=\"#tab-6\" data-toggle=\"tooltip\" title=\"instore\"><i class=\"gi gi-cargo\"></i></a></li>";}
								if(priv("posts","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
								if(priv("posts","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
								if(priv("posts","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}
								
								?>
							</ul>
						</div>
						<!-- END Block Tabs Title -->

						<!-- Tabs Content -->
						<div class="tab-content">

							<div class="tab-pane active" id="tab-1">
								<div class="col-md-6">
									<dl class="dl-horizontal">
										<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$myposts['id']); ?></dd>
										<dt>Helyszín:</dt><dd><? echo $myposts['name']; ?></dd>
										<dt>Ikon:</dt><dd><? echo $myposts['icon']; ?></dd>
										<dt>Érvényese:</dt><dd><? if($myposts['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
									</dl>	
								</div>
								<div style="clear:both;height:1px;"></div>

							</div>
							<div class="tab-pane" id="tab-2">
								<?
				
								echo"<form id=\"editposts\" name=\"editposts\"  method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
								echo "<div style=\"margin-top:1px;\">";
							
									echo"<div class=\"form-group\">
										<label class=\"col-md-3 control-label\" for=\"name\">Munkahely:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\"  value=\"".$myposts['name']."\"/>																		
										</div>
									</div>";
									echo"<div class=\"form-group\">
										<label class=\"col-md-3 control-label\" for=\"icon\">ikon:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"icon\" name=\"icon\" class=\"form-control\"  value=\"".$myposts['icon']."\"/>																		
										</div>
									</div>";
									echo"<div class=\"form-group\" >";
									$chz="";if($myposts['status']==1){ $chz=" checked "; }
										echo"<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" ".$chz."><span></span><br />Érvényes-e</label>";
									echo"</div>
									<div style=\"clear:both;height:10px;\"></div>
									<div class=\"form-group form-actions\">
										<div class=\"col-md-9 col-md-offset-3\">
											<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditposts('".$myposts['id']."');\"><i class=\"fa fa-angle-right\"></i> Ment</button>
										</div>
									</div>";	
								echo"</div>";
								echo"</form>";
								?>
							</div>
							<div class="tab-pane" id="tab-3">
									<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$myposts['id'],"Dokumentum feltöltés")."</div>";
								   ?> 
								   <div id="filelists"></div>
								   <div style="clear:both;height:1px;"></div>
							</div>
							<div class="tab-pane" id="tab-4">
									<?
										echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$myposts['id'],"Hozzászólások")."</div>";
									?>
									
							</div>
							<div class="tab-pane" id="tab-6">
									
								<div class="table-responsive">
									<table class="table table-vcenter table-striped">
										<thead>
											<tr>
												
												<th>Alapanyag név</th>
												<th>Jelenlegi mennyiség</th>
												<th>Egyéb infók</th>
											</tr>
										</thead>
										<tbody>
											<?php
												$instore=db_all("select ae.agt_id as agt_id,at.name as name  from alapag_events ae left join alapag_tipus at on ae.agt_id=at.id  where ae.post_id='".$myposts['id']."'  group by agt_id order by at.name asc ");
												for($i=0;$i<count($instore);$i++){
													$mennyi=db_one("select sum(amount) from alapag_events where post_id='".$myposts['id']."' and agt_id='".$instore[$i]['agt_id']."' and (event=5 or event=8 or event=10)");
													
													echo"<tr>";
													echo"<td><a href=\"/modules/goods/materials/".$instore[$i]['agt_id']."\">".$instore[$i]['name']."</a></td>";
													echo"<td class=\"text-right\">".round($mennyi,2)." Kg</td>";
													echo"<td>&nbsp;</td>";
													echo"</tr>";
												}
											?>
										</tbody>
									</table>
								</div>

							</div>
							<div class="tab-pane" id="tab-5">
								<div id="printdiv"></div>
							</div>
					
						</div>
						<!-- END Tabs Content -->
					</div>
					<!-- END Block Tabs -->
						
				</div>
				
				<?php include 'inc/page_footer.php'; ?>
				<?php include 'inc/template_scripts.php'; ?>
				<script src="modules/goodworkers/js/inc_posts.js"></script>

				<script >
					showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$myposts['id'];?>,'Dokumentum feltöltés','filelists');
					showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$myposts['id'];?>,'Dokumentum feltöltés','filelists');
								
			
				</script>
				<script>$(function(){ postsDatatables.init(); });</script>	
			

				<?php include 'inc/template_end.php'; ?>		
	<?
				}
			}
		}
		else{
			
?>    
	<!-- Ez a lista  -->
				<ul class="breadcrumb breadcrumb-top">
					<li><a href="/">Munkaügy</a></li>
					<li><a href="/<?=$URI;?>">Helyszínek</a></li>
				</ul>
				<!-- END Datatables Header -->

				<!-- Datatables Content -->
				<div class="block full">
					<div class="block-title">
						<h2><strong>Helyszín</strong> lista</h2>
					</div>
					

					<div class="table-responsive">
						<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
							<thead>
								<tr>
									<?
										foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
											if($screen['elements'][$f]['textname']!=""){
												echo"<th>".$screen['elements'][$f]['textname']."</th>";
											}
										}
										//echo"<th>Művelet</th>";
										?>
								</tr>
							</thead>
							<tbody ></tbody>
						</table>
					</div>
				</div>
				<!-- END Datatables Content -->
			</div>
			<!-- END Page Content -->

			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>

			<!-- Load and execute javascript code used only in this page -->
			<script src="modules/goodworkers/js/inc_posts.js"></script>
			<script>$(function(){ postsDatatables.init(); });</script>

			<?php include 'inc/template_end.php'; ?>
<?
}
?>

