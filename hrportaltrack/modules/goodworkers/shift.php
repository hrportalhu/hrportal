
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Műszak kezelő
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/goodworkers/shift">Műszak kezelő</a></li>
				<li><a href="/new">Új műszak felvitele</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új műszak felvitele</strong></h2>
				</div>
			<?
				echo "<form id=\"newmuszak\" name=\"newmuszak\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";

					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"name\">Új műszak neve:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" placeholder=\"Új műszak neve\" />																		
						</div>
					</div>";
					
					echo"<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"starttime\"> Műszak kezdete:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"starttime\" name=\"starttime\" class=\"form-control\" placeholder=\"kezdés\"/>																		
						</div>
					</div>";
					
					echo"<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"endtime\"> Műszak vége:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"endtime\" name=\"endtime\" class=\"form-control\" placeholder=\"végzés\"/>																		
						</div>
					</div>";
			
					echo "<div class=\"form-group\" >
						<label class=\"col-md-3 control-label\" for=\"status\">Érvényes-e?:</label>
						<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span></label>
					</div>";
					
					echo "<div style=\"clear:both;height:10px;\"></div>";
					
					echo "<div class=\"form-group form-actions\">
						<div class=\"col-md-9 col-md-offset-3\">
							<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewmuszak();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
						</div>
					</div>";
					
			echo "</div>
			</form>";
			?>
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/goodworkers/js/inc_shift.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?	
		}
		else{

			//echo "select * from functions where id='".$uritags[2]."'";
			$mymuszak=db_row("select * from gw_shift where id='".$uritags[3]."'");
			if($mymuszak['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="modules/goodworkers/shift">Műszak</a></li>
				<li><a href="/<?=$URI;?>"><?=$mymuszak['name'];?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->

					<div class="block-title">
						<h2><strong><?=$mymuszak['name'];?></strong> műszak oldal</h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
							if(priv("muszak","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("muszak","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							if(priv("muszak","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("muszak","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
							if(priv("muszak","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}	
							?>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="col-md-6">
								<dl class="dl-horizontal">
									<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$mymuszak['id']); ?></dd>
									<dt>Név:</dt><dd><? echo $mymuszak['name']; ?></dd>
									<dt>Kezdő idő:</dt><dd><? echo $mymuszak['starttime']; ?></dd>
									<dt>Végzés:</dt><dd><? echo $mymuszak['endtime']; ?></dd>
									<dt>Óra összesen:</dt><dd><? echo $mymuszak['endtime']-$mymuszak['starttime']?></dd>
									<dt>Státusz:</dt><dd><? if($mymuszak['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
								</dl>	
							</div>
							<div class="col-md-6">
								<?php
									$pic=db_one("select filename from files where modul='goodworkers' and type='shift' and type_id='".$mymuszak['id']."' order by cover desc");
									if(is_file($pic)){
										echo"<div class=\"col-sm-3\"><img src=\"/".$pic."\" alt=\"avatar\" width=\"450px\"></div>";
									}
									else{
										echo"<img src=\"img/placeholders/avatars/avatar9@2x.jpg\" alt=\"avatar\">";
									}
								?>
							</div>
							<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-2">
						<?
						echo "<form id=\"editmuszak\" name=\"editmuszak\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
							echo "<div style=\"margin-top:1px;\">";

								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"name\">Új műszak neve:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" value=\"".$mymuszak['name']."\" />																		
									</div>
								</div>";
								
								echo"<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"starttime\"> Műszak kezdete:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"starttime\" name=\"starttime\" class=\"form-control\" value=\"".$mymuszak['starttime']."\"/>																		
									</div>
								</div>";
								
								echo"<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"endtime\"> Műszak vége:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"endtime\" name=\"endtime\" class=\"form-control\" value=\"".$mymuszak['endtime']."\"/>																		
									</div>
								</div>";
						
								echo"<div class=\"form-group\" >";
								$chz="";if($mymuszak['status']==1){ $chz=" checked "; }
									echo"<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" ".$chz."><span></span><br />Érvényes-e</label>";
								echo"</div>";
								
								echo "<div style=\"clear:both;height:10px;\"></div>";
								
								echo "<div class=\"form-group form-actions\">
									<div class=\"col-md-9 col-md-offset-3\">
										<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditmuszak('".$mymuszak['id']."')\"><i class=\"fa fa-angle-right\"></i> Ment</button>
									</div>
								</div>";
								
							echo "</div>
									</form>";
						?>
						</div>
						<div class="tab-pane" id="tab-3">
								<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mymuszak['id'],"Dokumentum feltöltés")."</div>";
								?> 
								<div id="filelists"></div>
								<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mymuszak['id'],"Hozzászólások")."</div>";
								?>
						</div>
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>
				
					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
					
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/goodworkers/js/inc_shift.js"></script>

			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mymuszak['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$mymuszak['id'];?>,'Dokumentum feltöltés','filelists');
			</script>
			<script>$(function(){ MuszakDatatables.init(); });</script>	

			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
		//print_r($_SESSION['planetsys'])
?>    
<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Műszak</a></li>
			</ul>
			<!-- END Datatables Header -->

			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Műszak</strong> lista</h2>
				</div>
				
				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							//echo"<th>Művelet</th>";
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/goodworkers/js/inc_shift.js"></script>
		<script>$(function(){ MuszakDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>
