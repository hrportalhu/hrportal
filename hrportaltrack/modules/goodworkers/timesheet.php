
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Munkaidő kezelő
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/goodworkers/timesheet">Munkaidő kezelő</a></li>
				<li><a href="/new">Új munkaidő felvitele</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új munkaidő felvitele</strong></h2>
				</div>
			<?
			
				$dolgozo=db_all("select id, name from gw_workers where status=1 order by name");
				$hol=db_all("select id, name from gw_posts where status=1 order by name");
			
				echo "<form id=\"newtimesheet\" name=\"newtimesheet\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"workers_id\">Dolgozó:</label>
							<div class=\"col-md-9\">
								<select id=\"workers_id\" name=\"workers_id\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($dolgozo);$i++){
									$sel="";if($_SESSION['gw_worker']['id']==$dolgozo[$i]['id']){$sel=" selected ";}
									echo"<option value=\"".$dolgozo[$i]['id']."\" ".$sel.">".$dolgozo[$i]['name']."</option>";	
								}
								echo"</select>
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"post_id\">Hol dolgozik:</label>
							<div class=\"col-md-9\">
								<select id=\"post_id\" name=\"post_id\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($hol);$i++){
									$sel="";if($_SESSION['gw_worker']['post_id']==$hol[$i]['id']){$sel=" selected ";}
									echo"<option value=\"".$hol[$i]['id']."\" ".$sel.">".$hol[$i]['name']."</option>";	
								}
								echo"<option value=\"100000\"  ".($_SESSION['gw_worker']['post_id']=="100000"?"selected":"").">Szabadságon</option>";
								echo"<option value=\"200000\" ".($_SESSION['gw_worker']['post_id']=="200000"?"selected":"").">Beteg</option>";
								echo"<option value=\"300000\" ".($_SESSION['gw_worker']['post_id']=="300000"?"selected":"").">Egyéb</option>";
								echo"</select>
							</div>
						</div>";

						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"datem\">Dátum:</label>
							<div class=\"col-md-9\">";
								$datem=date("Y-m-d",time());if(isset($_SESSION['gw_worker']['workday'])){$datem=$_SESSION['gw_worker']['workday'];}
								echo"<input type=\"text\" id=\"datem\" name=\"datem\" class=\"form-control input-datepicker\" value=\"".$datem."\" />																		
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"st\">Munkakezdés:</label>
							<div class=\"col-md-9 input-group bootstrap-timepicker\">";
								$st="6";if(isset($_SESSION['gw_worker']['st'])){$st=$_SESSION['gw_worker']['st'];}
								echo"<input type=\"text\" id=\"st\" name=\"st\" class=\"form-control input-timepicker24\" value=\"".$st."\" />																		
								<span class=\"input-group-btn\">
									<a href=\"javascript:void(0)\" class=\"btn btn-primary\"><i class=\"fa fa-clock-o\"></i></a>
								</span>
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"en\">Befejezés:</label>
							<div class=\"col-md-9 input-group bootstrap-timepicker\">";
							$en="14";if(isset($_SESSION['gw_worker']['en'])){$en=$_SESSION['gw_worker']['en'];}
								echo"<input type=\"text\" id=\"en\" name=\"en\" class=\"form-control input-timepicker24\" value=\"".$en."\" />																		
								<span class=\"input-group-btn\">
									<a href=\"javascript:void(0)\" class=\"btn btn-primary\"><i class=\"fa fa-clock-o\"></i></a>
								</span>
							</div>
						</div>";

						
						echo "<div style=\"clear:both;height:10px;\"></div>";
						
						echo "<div class=\"form-group form-actions\">
							<div class=\"col-md-9 col-md-offset-3\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewtimesheet();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>";
						
				echo "</div>
				</form>";
			?>
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/goodworkers/js/inc_timesheet.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?	
		}
		else{

			//echo "select * from functions where id='".$uritags[2]."'";
			$mytimesheet=db_row("select * from gw_shift_x_worker where id='".$uritags[3]."'");
			if($mytimesheet['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="modules/goodworkers/timesheet">Munkaidő</a></li>
				<li><a href="/<?=$URI;?>"><?=$mytimesheet['id'];?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->

					<div class="block-title">
						<h2><strong><?=$mytimesheet['workday'];?>-ei <?=idchange("gw_workers","name",$mytimesheet['worker_id']);?></strong> munkaidejének oldala</h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
							if(priv("timesheet","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("timesheet","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							if(priv("timesheet","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("timesheet","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
							if(priv("timesheet","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}	
							?>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="col-md-6">
								<dl class="dl-horizontal">
									<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$mytimesheet['id']); ?></dd>
									<dt>Dolgozó:</dt><dd><? echo idchange("gw_workers","name",$mytimesheet['worker_id']); ?></dd>
									<dt>Munkahely:</dt><dd><? echo idchange("gw_posts","name",$mytimesheet['post_id']); ?></dd>
									<dt>Dátum:</dt><dd><? echo $mytimesheet['workday']; ?></dd>
									<dt>Munkakezdés:</dt><dd><? echo substr($mytimesheet['starttime'],11,5); ?></dd>
									<dt>Befejezés:</dt><dd><? echo substr($mytimesheet['endtime'],11,5); ?></dd>
									<dt>Munkaidő:</dt><dd><? echo $mytimesheet['worktime']; ?> óra</dd>
									
								</dl>	
							</div>
									<div class="col-md-6">
										<div class="block">
											<div class="block-title">
												<h2><strong>Fotó</strong></h2>
											</div>
											<?php
												$pic1='workpageimage/'.$mytimesheet['img1'];
												$pic2='workpageimage/'.$mytimesheet['img2'];
												echo $pic1;
												if(is_file($pic1)){
													echo"<center><img src=\"/".$pic1."\" alt=\"avatar\" style=\"height:180px;\" /></center>";
												}
												if(is_file($pic2)){
													echo"<center><img src=\"/".$pic2."\" alt=\"avatar\" style=\"height:180px;\" /></center>";
												}
												else{
													echo"<center><img src=\"img/placeholders/avatars/avatar9@2x.jpg\" alt=\"avatar\" /></center>";
												}
											?>
										</div>		
									
									</div>
							

							<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-2">
						<?
						
								$dolgozo=db_all("select id, name from gw_workers where status=1 order by name");
								$hol=db_all("select id, name from gw_posts where status=1 order by name");
							
							echo"<form id=\"edittimesheet\" name=\"edittimesheet\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
							echo "<div style=\"margin-top:1px;\">";
								
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"workers_id\">Dolgozó:</label>
									<div class=\"col-md-9\">
										<select id=\"workers_id\" name=\"workers_id\" class=\"form-control\" >
										<option value=\"0\">Kérlek válassz</option>";
										for($i=0;$i<count($dolgozo);$i++){
											$sel="";if($dolgozo[$i]['id']==$mytimesheet['worker_id']){$sel=" selected ";}
											echo"<option value=\"".$dolgozo[$i]['id']."\" ".$sel.">".$dolgozo[$i]['name']."</option>";
										}
										echo"</select>
									</div>
								</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"post_id\">Hol dolgozik:</label>
									<div class=\"col-md-9\">
										<select id=\"post_id\" name=\"post_id\" class=\"form-control\" >
										<option value=\"0\">Kérlek válassz</option>";
										for($i=0;$i<count($hol);$i++){
											$sel="";if($hol[$i]['id']==$mytimesheet['post_id']){$sel=" selected ";}
											echo"<option value=\"".$hol[$i]['id']."\" ".$sel.">".$hol[$i]['name']."</option>";
										}
										$se2="";if($mytimesheet[$i]['post_id']=="100000"){$se2=" selected ";}
										echo"<option value=\"100000\" ".$se2.">Szabadságon</option>";
										$se3="";if($mytimesheet[$i]['post_id']=="200000"){$se3=" selected ";}
										echo"<option value=\"200000\" ".$se3.">Beteg</option>";
										$se4="";if($mytimesheet[$i]['post_id']=="300000"){$se4=" selected ";}
										echo"<option value=\"300000\" ".$se4.">Egyéb</option>";
										echo"</select>
									</div>
								</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"datem\">Dátum:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"datem\" name=\"datem\" class=\"form-control input-datepicker\" value=\"".$mytimesheet['workday']."\" />																		
									</div>
								</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"st\">Munkakezdés:</label>
									<div class=\"col-md-9 input-group bootstrap-timepicker\">
										<input type=\"text\" id=\"st\" name=\"st\" class=\"form-control input-timepicker24\" value=\"".substr($mytimesheet['starttime'],11,5)."\" />																		
										<span class=\"input-group-btn\">
											<a href=\"javascript:void(0)\" class=\"btn btn-primary\"><i class=\"fa fa-clock-o\"></i></a>
										</span>
									</div>
								</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"en\">Befejezés:</label>
									<div class=\"col-md-9 input-group bootstrap-timepicker\">
										<input type=\"text\" id=\"en\" name=\"en\" class=\"form-control input-timepicker24\" value=\"".substr($mytimesheet['endtime'],11,5)."\" />																		
										<span class=\"input-group-btn\">
											<a href=\"javascript:void(0)\" class=\"btn btn-primary\"><i class=\"fa fa-clock-o\"></i></a>
										</span>
									</div>
								</div>";
								

								
								echo "<div class=\"form-group form-actions\">
									<div class=\"col-md-9 col-md-offset-3\">
										<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveedittimesheet(".$mytimesheet['id'].")\"><i class=\"fa fa-angle-right\"></i> Ment</button>
									</div>
								</div>";
									
							echo "</div>
							</form>";
						?>
						</div>
						<div class="tab-pane" id="tab-3">
								<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mytimesheet['id'],"Dokumentum feltöltés")."</div>";
								?> 
								<div id="filelists"></div>
								<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mytimesheet['id'],"Hozzászólások")."</div>";
								?>
						</div>
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>
				
					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
					
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/goodworkers/js/inc_timesheet.js"></script>

			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mytimesheet['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$mytimesheet['id'];?>,'Dokumentum feltöltés','filelists');
			</script>
			<script>$(function(){ timesheetDatatables.init(); });</script>	

			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
		//print_r($_SESSION['planetsys'])
?>    
<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Munkaidő</a></li>
			</ul>
			<!-- END Datatables Header -->

			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Munkaidő</strong> lista</h2>
				</div>
				
				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							//echo"<th>Művelet</th>";
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/goodworkers/js/inc_timesheet.js"></script>
		<script>$(function(){ timesheetDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>
