
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Oktatás kezelő
            </h1>
        </div>
    </div>
<?

		if(isset($uritags[3]) && $uritags[3]!=""){
			if($uritags[3]=="new"){
				?>
				<!-- Ez egy elem  -->		
				<ul class="breadcrumb breadcrumb-top">
					<li><a href="/">Rendszer</a></li>
					<li><a href="/modules/goodworkers/tranings">Oktatás kezelő</a></li>
					<li><a href="/new">Új Oktatás felvitele</a></li>
				</ul>
				<div class="block full">
				<?
				
					echo"<form id=\"newtranings\" name=\"newtranings\"  method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
					echo "<div style=\"margin-top:1px;\">";
							
							
						echo"<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"traning_date\">Oktatás ideje:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"traning_date\" name=\"traning_date\" class=\"form-control input-datepicker\" value=\"".date("Y-m-d",time())."\" />
							</div>
						</div>";
							
						echo"<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"traning_time\">Oktatás időtartama:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"traning_time\" name=\"traning_time\" class=\"form-control\" value=\"\" />
							</div>
						</div>";
						echo"<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"name\">Oktatás leírása:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" value=\"\" />
								
							</div>
						</div>";
						echo"<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"traning_comment\">Oktatási tematika:</label>
							<div class=\"col-md-9\">
								<textarea id=\"traning_comment\" name=\"traning_comment\" rows=\"6\" class=\"ckeditor\" ></textarea>
							</div>
						</div>";
						
						echo"<div style=\"clear:both;height:10px;\"></div>";
						echo"<div class=\"form-group form-actions\">
							<div class=\"col-md-9 \">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewtranings();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>";	
					echo"</div>";
					echo"</form>";
				?>
				</div>
				</div>
				<?php include 'inc/page_footer.php'; ?>
				<?php include 'inc/template_scripts.php'; ?>
				<script src="js/ckeditor/ckeditor.js"></script>
				<script src="modules/goodworkers/js/inc_tranings.js"></script>
				<?php include 'inc/template_end.php'; ?>	
				<?	
			}
			else{
				
				//echo "select * from functions where id='".$uritags[2]."'";
				$mytranings=db_row("select * from gw_tranings where id='".$uritags[3]."'");
				if($mytranings['id']==''){
					die("Helytelen hozzáférés!");
				}
				else{
				
	?>
				<!-- Ez egy elem  -->		
				<ul class="breadcrumb breadcrumb-top">
					<li><a href="/">Rendszer</a></li>
					<li><a href="modules/goodworkers/tranings">Helyszínek</a></li>
					<li><a href="/<?=$URI;?>"><?=$mytranings['name'];?></a></li>
				</ul>
				<div class="block full">
								   
						<!-- Block Tabs Title -->

						<div class="block-title">
							<h2><strong><?=$mytranings['name'];?></strong> helyszín oldala</h2>
							<ul class="nav nav-tabs" data-toggle="tabs">
								<?
								
								if(priv("tranings","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
								if(priv("tranings","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
								if(priv("tranings","edit")){echo"<li><a href=\"#tab-6\" data-toggle=\"tooltip\" title=\"workers\"><i class=\"gi gi-parents\"></i></a></li>";}
								if(priv("tranings","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
								if(priv("tranings","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
								if(priv("tranings","print")){echo"<li onclick=\"javascript:getprintscreen('".$_SESSION['planetsys']['actpage']['modulname']."','traningsprint','".$mytranings['id']."');\" ><a href=\"#modal-regular-print\" data-toggle=\"modal\" title=\"osszetevok print\"><i class=\"fa fa-print\" ></i></a></li>";}
								
								?>
							</ul>
						</div>
						<!-- END Block Tabs Title -->

						<!-- Tabs Content -->
						<div class="tab-content">

							<div class="tab-pane active" id="tab-1">
								<div class="col-md-6">
									<dl class="dl-horizontal">
										<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$mytranings['id']); ?></dd>
										<dt>Oktatás leírása:</dt><dd><? echo nl2br($mytranings['name']); ?></dd>
										<dt>Oktatás ideje:</dt><dd><? echo $mytranings['traning_date']; ?></dd>
										<dt>Oktatás időtartama:</dt><dd><? echo $mytranings['traning_time']; ?> perc</dd>
										<dt>Oktatási tematika:</dt><dd><? echo html_entity_decode(stripslashes($mytranings['traning_comment'])); ?></dd>
										<dt>Oktatást végezte:</dt><dd><? echo idchange("users","realname",$mytranings['user_id']); ?></dd>
										<dt>Oktatáson részt vett:</dt><dd>&nbsp;</dd>
										<?
											$b=db_all("select * from gw_tranings_workers where status=1 and traning_id='".$mytranings['id']."'");
											//echo"select * from gw_tranings_workers where status=1 and traning_id='".$mytranings['id']."'";
											if(count($b)>0){
												for($i=0;$i<count($b);$i++){
													echo"<dt>&nbsp;</dt><dd>".idchange("gw_workers","name",$b[$i]['worker_id'])."</dd>";
												}
											}
										?>
									</dl>	
								</div>
								<div style="clear:both;height:1px;"></div>

							</div>
							<div class="tab-pane" id="tab-2">
								<?
				
								echo"<form id=\"edittranings\" name=\"edittranings\"  method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
								echo "<div style=\"margin-top:1px;\">";
							
								
							
						echo"<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"traning_date\">Oktatás ideje:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"traning_date\" name=\"traning_date\" class=\"form-control input-datepicker\" value=\"".$mytranings['traning_date']."\" />
							</div>
						</div>";
							
						echo"<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"traning_time\">Oktatás időtartama:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"traning_time\" name=\"traning_time\" class=\"form-control\" value=\"".$mytranings['traning_date']."\" />
							</div>
						</div>";
						
						
						
						echo"<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"name\">Oktatás leírása:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" value=\"".$mytranings['name']."\" />
							</div>
						</div>";
						echo"<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"traning_comment\">Oktatási tematika:</label>
							<div class=\"col-md-9\">
								<textarea id=\"traning_comment\" name=\"traning_comment\" rows=\"6\" class=\"ckeditor\" >".$mytranings['traning_comment']."</textarea>
							</div>
						</div>";
						
									echo"<div style=\"clear:both;height:10px;\"></div>
									<div class=\"form-group form-actions\">
										<div class=\"col-md-9 col-md-offset-3\">
											<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveedittranings('".$mytranings['id']."');\"><i class=\"fa fa-angle-right\"></i> Ment</button>
										</div>
									</div>";	
								echo"</div>";
								echo"</form>";
								?>
							</div>
							<div class="tab-pane" id="tab-6">
									<?
										
										
									?>	
										<div class="table-responsive">
										<table class="table table-vcenter table-striped">
											<thead>
												<tr>
													<th>Dolgozó neve</th>
													<th>Oktatást kap e?</th>
												</tr>
											</thead>
											<tbody>
												<?php
													$workers=db_all("select * from gw_workers where status=1 order by name");
													for($i=0;$i<count($workers);$i++){
														$mc="";
														$ch=db_one("select status from gw_tranings_workers where worker_id='".$workers[$i]['id']."' and traning_id='".$mytranings['id']."'");
														if($ch==1){
															$mc=" checked ";	
														}
														echo"<tr>";
														echo"<td><a href=\"\">".$workers[$i]['name']."</a></td>";
														echo"<td class=\"text-left\">";
														echo"<div class=\"checkbox\">";
															echo"<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" ".$mc." onclick=\"setteaching(".$mytranings['id'].",".$workers[$i]['id'].");\" ><span></span></label>";
														echo"</div>";
														echo"</td>";
														echo"</tr>";
													}
												?>
											</tbody>
										</table>
									</div>
								   <div style="clear:both;height:1px;"></div>
							</div>
							<div class="tab-pane" id="tab-3">
									<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mytranings['id'],"Dokumentum feltöltés")."</div>";
								   ?> 
								   <div id="filelists"></div>
								   <div style="clear:both;height:1px;"></div>
							</div>
							<div class="tab-pane" id="tab-4">
									<?
										echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mytranings['id'],"Hozzászólások")."</div>";
									?>
									
							</div>
							
							<div class="tab-pane" id="tab-5">
								<div id="printdiv"></div>
							</div>
					
						</div>
						<!-- END Tabs Content -->
					</div>
					<!-- END Block Tabs -->
						
				</div>
				
				<?php include 'inc/page_footer.php'; ?>
				<?php include 'inc/template_scripts.php'; ?>
				<script src="js/ckeditor/ckeditor.js"></script>
				<script src="modules/goodworkers/js/inc_tranings.js"></script>

				<script >
					showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mytranings['id'];?>,'Dokumentum feltöltés','filelists');
					showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mytranings['id'];?>,'Dokumentum feltöltés','filelists');
								
			
				</script>
				<script>$(function(){ traningsDatatables.init(); });</script>	
			

				<?php include 'inc/template_end.php'; ?>		
	<?
				}
			}
		}
		else{
			
?>    
	<!-- Ez a lista  -->
				<ul class="breadcrumb breadcrumb-top">
					<li><a href="/">Munkaügy</a></li>
					<li><a href="/<?=$URI;?>">Oktatási lista</a></li>
				</ul>
				<!-- END Datatables Header -->

				<!-- Datatables Content -->
				<div class="block full">
					<div class="block-title">
						<h2><strong>Oktatási</strong> lista</h2>
					</div>
					

					<div class="table-responsive">
						<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
							<thead>
								<tr>
									<?
										foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
											if($screen['elements'][$f]['textname']!=""){
												echo"<th>".$screen['elements'][$f]['textname']."</th>";
											}
										}
										//echo"<th>Művelet</th>";
										?>
								</tr>
							</thead>
							<tbody ></tbody>
						</table>
					</div>
				</div>
				<!-- END Datatables Content -->
			</div>
			<!-- END Page Content -->

			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>

			<!-- Load and execute javascript code used only in this page -->
			<script src="modules/goodworkers/js/inc_tranings.js"></script>
			<script>$(function(){ traningsDatatables.init(); });</script>

			<?php include 'inc/template_end.php'; ?>
<?
}
?>

,

