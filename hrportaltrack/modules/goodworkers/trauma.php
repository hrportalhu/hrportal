
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Baleset kezelő
            </h1>
        </div>
    </div>
<?

		if(isset($uritags[3]) && $uritags[3]!=""){
			if($uritags[3]=="new"){
				?>
				<!-- Ez egy elem  -->		
				<ul class="breadcrumb breadcrumb-top">
					<li><a href="/">Rendszer</a></li>
					<li><a href="/modules/goodworkers/trauma">Baleset kezelő</a></li>
					<li><a href="/new">Új baleset felvitele</a></li>
				</ul>
				<div class="block full">
				<?
				$dolgozo=db_all("select id, name from gw_workers where status=1 order by name");
					echo"<form id=\"newtrauma\" name=\"newtrauma\"  method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
					echo "<div style=\"margin-top:1px;\">";
							
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"worker_id\">Dolgozó:</label>
							<div class=\"col-md-9\">
								<select id=\"worker_id\" name=\"worker_id\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($dolgozo);$i++){
									echo"<option value=\"".$dolgozo[$i]['id']."\" >".$dolgozo[$i]['name']."</option>";	
								}
								echo"</select>
							</div>
						</div>";
							
							
						echo"<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"trauma_date\">Baleset ideje:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"trauma_date\" name=\"trauma_date\" class=\"form-control input-datepicker\" value=\"".date("Y-m-d",time())."\" />
							</div>
						</div>";
						echo"<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"name\">Baleset leírása:</label>
							<div class=\"col-md-9\">
								<textarea id=\"name\" name=\"name\" rows=\"6\" class=\"form-control\" ></textarea>
							</div>
						</div>";
						echo"<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"trauma_device\">Felhasznált elsősegély eszközök:</label>
							<div class=\"col-md-9\">
								<textarea id=\"trauma_device\" name=\"trauma_device\" rows=\"6\" class=\"form-control\" ></textarea>
							</div>
						</div>";
						
						echo"<div style=\"clear:both;height:10px;\"></div>";
						echo"<div class=\"form-group form-actions\">
							<div class=\"col-md-9 \">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewtrauma();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>";	
					echo"</div>";
					echo"</form>";
				?>
				</div>
				</div>
				<?php include 'inc/page_footer.php'; ?>
				<?php include 'inc/template_scripts.php'; ?>
				<script src="modules/goodworkers/js/inc_trauma.js"></script>
				<?php include 'inc/template_end.php'; ?>	
				<?	
			}
			else{
				
				//echo "select * from functions where id='".$uritags[2]."'";
				$mytrauma=db_row("select * from gw_trauma where id='".$uritags[3]."'");
				if($mytrauma['id']==''){
					die("Helytelen hozzáférés!");
				}
				else{
					$dolgozo=db_all("select id, name from gw_workers where status=1 order by name");
	?>
				<!-- Ez egy elem  -->		
				<ul class="breadcrumb breadcrumb-top">
					<li><a href="/">Rendszer</a></li>
					<li><a href="modules/goodworkers/trauma">Balesetek</a></li>
					<li><a href="/<?=$URI;?>"><?=$mytrauma['name'];?></a></li>
				</ul>
				<div class="block full">
								   
						<!-- Block Tabs Title -->

						<div class="block-title">
							<h2><strong><?=$mytrauma['name'];?></strong> baleset oldala</h2>
							<ul class="nav nav-tabs" data-toggle="tabs">
								<?
								
								if(priv("trauma","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
								if(priv("trauma","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
								
								if(priv("trauma","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
								if(priv("trauma","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
								if(priv("trauma","print")){echo"<li onclick=\"javascript:getprintscreen('".$_SESSION['planetsys']['actpage']['modulname']."','traumaprint','".$mytrauma['id']."');\" ><a href=\"#modal-regular-print\" data-toggle=\"modal\" title=\"osszetevok print\"><i class=\"fa fa-print\" ></i></a></li>";}
								
								?>
							</ul>
						</div>
						<!-- END Block Tabs Title -->

						<!-- Tabs Content -->
						<div class="tab-content">

							<div class="tab-pane active" id="tab-1">
								<div class="col-md-6">
									<dl class="dl-horizontal">
										<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$mytrauma['id']); ?></dd>
										<dt>Dolgozó:</dt><dd><? echo idchange("gw_workers","name",$mytrauma['worker_id']); ?></dd>
										<dt>Baleset leírása:</dt><dd><? echo nl2br($mytrauma['name']); ?></dd>
										<dt>Baleset ideje:</dt><dd><? echo $mytrauma['trauma_date']; ?></dd>
										<dt>Felhasznált eszközök:</dt><dd><? echo nl2br($mytrauma['trauma_device']); ?></dd>
										<dt>Elsősegélyt végezte:</dt><dd><? echo idchange("users","realname",$mytrauma['user_id']); ?></dd>
										
									</dl>	
								</div>
								<div style="clear:both;height:1px;"></div>

							</div>
							<div class="tab-pane" id="tab-2">
								<?
				
								echo"<form id=\"edittrauma\" name=\"edittrauma\"  method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
								echo "<div style=\"margin-top:1px;\">";
							
									echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"worker_id\">Dolgozó:</label>
							<div class=\"col-md-9\">
								<select id=\"worker_id\" name=\"worker_id\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($dolgozo);$i++){
									$sel="";if($mytrauma['worker_id']==$dolgozo[$i]['id']){$sel=" selected ";}
									echo"<option value=\"".$dolgozo[$i]['id']."\" ".$sel.">".$dolgozo[$i]['name']."</option>";	
								}
								echo"</select>
							</div>
						</div>";
							
							
						echo"<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"trauma_date\">Baleset ideje:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"trauma_date\" name=\"trauma_date\" class=\"form-control input-datepicker\" value=\"".$mytrauma['trauma_date']."\" />
							</div>
						</div>";
						echo"<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"name\">Baleset leírása:</label>
							<div class=\"col-md-9\">
								<textarea id=\"name\" name=\"name\" rows=\"6\" class=\"form-control\" >".$mytrauma['name']."</textarea>
							</div>
						</div>";
						echo"<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"trauma_device\">Felhasznált elsősegély eszközök:</label>
							<div class=\"col-md-9\">
								<textarea id=\"trauma_device\" name=\"trauma_device\" rows=\"6\" class=\"form-control\" >".$mytrauma['trauma_device']."</textarea>
							</div>
						</div>";
						
									echo"<div style=\"clear:both;height:10px;\"></div>
									<div class=\"form-group form-actions\">
										<div class=\"col-md-9 col-md-offset-3\">
											<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveedittrauma('".$mytrauma['id']."');\"><i class=\"fa fa-angle-right\"></i> Ment</button>
										</div>
									</div>";	
								echo"</div>";
								echo"</form>";
								?>
							</div>
							<div class="tab-pane" id="tab-3">
									<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mytrauma['id'],"Dokumentum feltöltés")."</div>";
								   ?> 
								   <div id="filelists"></div>
								   <div style="clear:both;height:1px;"></div>
							</div>
							<div class="tab-pane" id="tab-4">
									<?
										echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mytrauma['id'],"Hozzászólások")."</div>";
									?>
									
							</div>
							<div class="tab-pane" id="tab-6">
									
								<div class="table-responsive">
									<table class="table table-vcenter table-striped">
										<thead>
											<tr>
												
												<th>Alapanyag név</th>
												<th>Jelenlegi mennyiség</th>
												<th>Egyéb infók</th>
											</tr>
										</thead>
										<tbody>
											<?php
												$instore=db_all("select ae.agt_id as agt_id,at.name as name  from alapag_events ae left join alapag_tipus at on ae.agt_id=at.id  where ae.post_id='".$mytrauma['id']."'  group by agt_id order by at.name asc ");
												for($i=0;$i<count($instore);$i++){
													$mennyi=db_one("select sum(amount) from alapag_events where post_id='".$mytrauma['id']."' and agt_id='".$instore[$i]['agt_id']."' and (event=5 or event=8 or event=10)");
													
													echo"<tr>";
													echo"<td><a href=\"/modules/goods/materials/".$instore[$i]['agt_id']."\">".$instore[$i]['name']."</a></td>";
													echo"<td class=\"text-right\">".round($mennyi,2)." Kg</td>";
													echo"<td>&nbsp;</td>";
													echo"</tr>";
												}
											?>
										</tbody>
									</table>
								</div>

							</div>
							<div class="tab-pane" id="tab-5">
								<div id="printdiv"></div>
							</div>
					
						</div>
						<!-- END Tabs Content -->
					</div>
					<!-- END Block Tabs -->
						
				</div>
				
				<?php include 'inc/page_footer.php'; ?>
				<?php include 'inc/template_scripts.php'; ?>
				<script src="modules/goodworkers/js/inc_trauma.js"></script>

				<script >
					showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mytrauma['id'];?>,'Dokumentum feltöltés','filelists');
					showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mytrauma['id'];?>,'Dokumentum feltöltés','filelists');
								
			
				</script>
				<script>$(function(){ traumaDatatables.init(); });</script>	
			

				<?php include 'inc/template_end.php'; ?>		
	<?
				}
			}
		}
		else{
			
?>    
	<!-- Ez a lista  -->
				<ul class="breadcrumb breadcrumb-top">
					<li><a href="/">Munkaügy</a></li>
					<li><a href="/<?=$URI;?>">Balesetek</a></li>
				</ul>
				<!-- END Datatables Header -->

				<!-- Datatables Content -->
				<div class="block full">
					<div class="block-title">
						<h2><strong>Baleset</strong> lista</h2>
					</div>
					

					<div class="table-responsive">
						<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
							<thead>
								<tr>
									<?
										foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
											if($screen['elements'][$f]['textname']!=""){
												echo"<th>".$screen['elements'][$f]['textname']."</th>";
											}
										}
										//echo"<th>Művelet</th>";
										?>
								</tr>
							</thead>
							<tbody ></tbody>
						</table>
					</div>
				</div>
				<!-- END Datatables Content -->
			</div>
			<!-- END Page Content -->

			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>

			<!-- Load and execute javascript code used only in this page -->
			<script src="modules/goodworkers/js/inc_trauma.js"></script>
			<script>$(function(){ traumaDatatables.init(); });</script>

			<?php include 'inc/template_end.php'; ?>
<?
}
?>

