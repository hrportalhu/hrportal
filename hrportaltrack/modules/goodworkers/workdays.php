
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Munkanap kezelő
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/goodworkers/workdays">Munkanap kezelő</a></li>
				<li><a href="/new">Új Munkanap felvitele</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új Munkanap felvitele</strong></h2>
				</div>
			<?
			
				
				
				echo "<form id=\"newworkdays\" name=\"newworkdays\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";
				

						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"name\">Új Munkanap:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control input-datepicker\" placeholder=\"Új Munkanap neve\" />																		
							</div>
						</div>";
						
						echo "<div class=\"form-group\" >
							<label class=\"col-md-3 control-label\" for=\"status\">Érvényes-e:</label>
							<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span></label>
						</div>";
						
						echo "<div style=\"clear:both;height:10px;\"></div>";
						
						echo "<div class=\"form-group form-actions\">
							<div class=\"col-md-9 col-md-offset-3\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewworkdays();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>";
						
				echo "</div>
				</form>";
			?>
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/goodworkers/js/inc_workdays.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?	
		}
		else{

			//echo "select * from functions where id='".$uritags[2]."'";
			$myworkdays=db_row("select * from gw_workdays where id='".$uritags[3]."'");
			if($myworkdays['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="modules/goodworkers/workdays"> Munkanap</a></li>
				<li><a href="/<?=$URI;?>"><?=$myworkdays['name'];?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->

					<div class="block-title">
						<h2><strong><?=$myworkdays['name'];?></strong> Munkanap oldal</h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
							if(priv("workdays","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("workdays","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							if(priv("workdays","worktimes")){echo"<li><a href=\"#tab-6\" data-toggle=\"tooltip\" title=\"worktimes\"><i class=\"fa fa-list-alt\"></i></a></li>";}
							if(priv("workdays","worktimes")){echo"<li><a href=\"#tab-8\" data-toggle=\"tooltip\" title=\"worktimeslist\"><i class=\"gi gi-show_lines\"></i></a></li>";}
							if(priv("workdays","workplaces")){echo"<li><a href=\"#tab-7\" data-toggle=\"tooltip\" title=\"workplaces\"><i class=\"fa fa-futbol-o\"></i></a></li>";}
							if(priv("workdays","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("workdays","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
							if(priv("workdays","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}	
							?>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="col-md-6">
								<dl class="dl-horizontal">
									<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$myworkdays['id']); ?></dd>
									<dt>Munkanap:</dt><dd><? echo $myworkdays['name']; ?></dd>
									<dt>Státusz:</dt><dd><? if($myworkdays['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
								</dl>	
							</div>
							<div class="col-md-6">
								<dl class="dl-horizontal">
								<?php
									$dolgozok=db_all("select w.name as workename,w.worktime_hiv as worktime_hiv, gw.* from gw_shift_x_worker gw left join gw_workers w on (gw.worker_id=w.id) where gw.workday='".$myworkdays['name']."' group by gw.worker_id order by w.name, gw.id desc  ");	
									for($i=0;$i<count($dolgozok);$i++){
										
										if($dolgozok[$i]['endtime']=="0000-00-00 00:00:00"){
											
											$betxt="Jelenleg bent van! Érkezett:".date("H",(time()-7200)).":00-kor";
										}
										else{
											$erkeztett=db_one("select starttime from gw_shift_x_worker where workday='".$myworkdays['name']."' and worker_id='".$dolgozok[$i]['worker_id']."' order by starttime asc");
											$newdate=strtotime($erkeztett)+60*60*$dolgozok[$i]['worktime_hiv'];
											$betxt=substr($erkeztett,11,5)."-tól ".date("H:i",$newdate)."-ig --- ".$dolgozok[$i]['worktime_hiv']." óra" ;
										}
										echo"<dt>".($i+1).". <a href=\"/modules/goodworkers/workers/".$dolgozok[$i]['worker_id']."#tab-6\">".$dolgozok[$i]['workename']."</a></dt><dd>".$betxt."</dd>";
									}
								?>
								</dl>
							</div>
							<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-2">
						<?
						
							echo"<form id=\"editworkdays\" name=\"editworkdays\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
							echo "<div style=\"margin-top:1px;\">";
															
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"name\">Új Munkanap:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" value=\"".$myworkdays['name']."\" />																		
									</div>
								</div>";
								
								echo "<div class=\"form-group\" >
									<label class=\"col-md-3 control-label\" for=\"status\">Érvényes-e:</label>";
									$st="";if($myworkdays['status']==1){ $st=" checked "; }
									echo "<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\"".$st." ><span></span></label>
								</div>";
								
								echo "<div class=\"form-group form-actions\">
									<div class=\"col-md-9 col-md-offset-3\">
										<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditworkdays(".$myworkdays['id'].")\"><i class=\"fa fa-angle-right\"></i> Ment</button>
									</div>
								</div>";
									
							echo "</div>
							</form>";
						?>
						</div>
						<div class="tab-pane" id="tab-3">
								<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$myworkdays['id'],"Dokumentum feltöltés")."</div>";
								?> 
								<div id="filelists"></div>
								<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$myworkdays['id'],"Hozzászólások")."</div>";
								?>
						</div>
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>
						<div class="tab-pane" id="tab-6">
							
							<div class="col-md-12">
							<div class="block hidden-lt-ie9">
							<!-- Switches Title -->
							<div class="block-title">
								<h2><strong>Szűrő</strong> </h2>
							</div>
							<?
							$munkahelyek=db_all("select id,name from gw_posts where status=1 order by name asc");
						   echo"<div class=\"form-group\" >
									
									<div class=\"col-md-12\">
										<select id=\"mypost_id\" name=\"mypost_id\" class=\"form-control\"  onchange=\"javascript:showdailyworktime('".$myworkdays['id']."','elementdiv3');	\" >
											<option value=\"0\" selected>Minden munkahely</option>";
											for($i=0;$i<count($munkahelyek);$i++){
												echo"<option value=\"".$munkahelyek[$i]['id']."\">".$munkahelyek[$i]['name']."</option>";	
											}
										echo"</select>
									</div>
								</div>";
						   
								?>	
								<div style="clear:both;height:10px;"></div>
								</div>	
							
									<div class="block">
										<div class="block-title">
											<h2><strong>Munkaidő</strong></h2>
										</div>
						
									<div style="clear:both;height:10px;"></div>
										<div class="table-responsive">
											<table class="table table-vcenter table-striped">
												<thead>
													<tr>
														<th style="width: 100px;" class="text-center"><i class="gi gi-disk_save"></i></th>
														<th>Dolgozó</th>
														<th>Munkahely</th>
														<th>Munkakezdés</th>
														<th>Kép 1</th>
														<th>Befejezés</th>
														<th>Kép 2</th>
														<th>Infók</th>
														<th>Művelet</th>
													</tr>
												</thead>
												<tbody id="elementdiv3">
													
													
												</tbody>
											</table>		
										</div>
									</div>		
								</div>
								<div style="clear:both;height:1px;"></div>
					
						</div>
						<div class="tab-pane" id="tab-7">
							<div class="col-md-12">
								<div class="block">
									<div class="block-title">
										<h2><strong>Műszakrend</strong></h2>
									</div>
									<div class="table-responsive">
										<table class="table table-vcenter table-striped">
											<thead>
												<tr>
													<th style="width: 100px;" class="text-center"><i class="gi gi-disk_save"></i></th>
													<th>Dolgozó</th>
													<th>Munkahelye</th>
												</tr>
											</thead>
											<tbody>
												<?
												$workers=db_all("select * from gw_shift_x_worker where workday='".$myworkdays['name']."'");
												$posts=db_all("select id,name from gw_posts where status='1' order by name");
												for($i=0;$i<count($workers);$i++){
													$chp=db_one("select post_id from gw_shift_x_worker where worker_id='".$workers[$i]['worker_id']."' and workday='".$myworkdays['name']."'");
													echo"<tr>";
														echo"<td>".$workers[$i]['id']."</td>";
														echo"<td>".idchange("gw_workers","name",$workers[$i]['worker_id'])."</td>";
														echo"<td>";
															echo"<select id=\"post_".$workers[$i]['id']."\" name=\"post_".$workers[$i]['id']."\" class=\"form-control\" onchange=\"saveworkdaypost(".$workers[$i]['id'].");\">
																	<option value=\"0\">Kérlek válassz</option>";
																	for($a=0;$a<count($posts);$a++){
																		$sel="";if($workers[$i]['post_id']==$posts[$a]['id']){$sel=" selected ";}
																		echo"<option value=\"".$posts[$a]['id']."\" ".$sel.">".$posts[$a]['name']."</option>";	
																	}
																	$se2="";if($workers[$i]['post_id']=="100000"){$se2=" selected ";}
																	echo"<option value=\"100000\" ".$se2.">Szabadságon</option>";
																	$se3="";if($workers[$i]['post_id']=="200000"){$se3=" selected ";}
																	echo"<option value=\"200000\" ".$se3.">Beteg</option>";
																	$se4="";if($workers[$i]['post_id']=="300000"){$se4=" selected ";}
																	echo"<option value=\"300000\" ".$se4.">Egyéb</option>";
																	echo"</select>";
														echo"</td>";
													echo"</tr>";
												}
												?>
											</tbody>
										</table>		
									</div>
								</div>		
							</div>
							<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-8">
							<div class="col-md-12">
								<div class="block">
									<div class="block-title">
										<h2><strong>Gyorslista</strong></h2>
									</div>
									<div class="table-responsive">
										<table class="table table-vcenter table-striped">
											<thead>
												<tr>
													<th style="width: 100px;" class="text-center"><i class="gi gi-disk_save"></i></th>
													<th>Dolgozó</th>
													<th>Ledolgozott munkaidő</th>
													<th>Munkabér</th>
												</tr>
											</thead>
											<tbody>
												<?
												$workers=db_all("select gsw.* from gw_shift_x_worker gsw left join gw_workers gw on (gsw.worker_id=gw.id) where gsw.workday='".$myworkdays['name']."' group by gsw.worker_id order by gw.name asc ");
												$pi=0;
												$mu=0;
												for($i=0;$i<count($workers);$i++){
													$chp=db_one("select sum(worktime) as wt from gw_shift_x_worker where worker_id='".$workers[$i]['worker_id']."' and workday='".$myworkdays['name']."'");
													$cdal=db_one("select sum(dailycash) as wt from gw_shift_x_worker where worker_id='".$workers[$i]['worker_id']."' and workday='".$myworkdays['name']."'");
													//echo "select sum(dailycash) as wt from gw_shift_x_worker where worker_id='".$workers[$i]['worker_id']."' and workday='".$myworkdays['name']."'";
													echo"<tr>";
														echo"<td>".$workers[$i]['id']."</td>";
														echo"<td><a href=\"/modules/goodworkers/workers/".$workers[$i]['worker_id']."#tab-6\">".idchange("gw_workers","name",$workers[$i]['worker_id'])."</a></td>";
														echo"<td>".round($chp,2)." óra</td>";
														echo"<td>".round($cdal,2)." ft</td>";
														$pi=$pi+round($cdal,2);
														$mu=$mu+round($chp,2);
													echo"</tr>";
												}
													echo"<tr>";
														echo"<td>&nbsp;</td>";
														echo"<td>Összesen;</td>";
														echo"<td>".$mu." óra</td>";
														echo"<td>".$pi." ft</td>";
													echo"</tr>";
												
												?>
											</tbody>
										</table>		
									</div>
								</div>		
							</div>
							<div style="clear:both;height:1px;"></div>
						</div>
				
					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
					
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/goodworkers/js/inc_workdays.js"></script>

			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$myworkdays['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$myworkdays['id'];?>,'Dokumentum feltöltés','filelists');
				showdailyworktime('<?=$myworkdays['id'];?>','elementdiv3');
			</script>
			<script>$(function(){ workdaysDatatables.init(); });</script>	

			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
		//print_r($_SESSION['planetsys'])
?>    
<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Munkanap</a></li>
			</ul>
			<!-- END Datatables Header -->

			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Munkanap</strong> lista</h2>
				</div>
				
				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							//echo"<th>Művelet</th>";
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/goodworkers/js/inc_workdays.js"></script>
		<script>$(function(){ workdaysDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>
