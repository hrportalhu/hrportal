<div id="page-content">
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Dolgozói listák
            </h1>
        </div>
    </div>
	<ul class="breadcrumb breadcrumb-top">
		<li><a href="/">Rendszer</a></li>
		<li><a href="/<?=$URI;?>">Dolgozók </a></li>
	</ul>
	<div class="block full">
		<div class="block-title">
			<h2><strong>Elérhető</strong> munkavállalói lekérdezések</h2>
			<ul class="nav nav-tabs" data-toggle="tabs">
				<?
					if(priv("workers","edit")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"Szeműveges dolgozók lekérdezése\" onclick=\"javascript:showszemuvegeslist();\"><i class=\"gi gi-eye_open\"></i></a></li>";}
					if(priv("workers","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"Egészségügyi kiskönyv lekérdezés\" onclick=\"javascript:showeukonyvlist();\"><i class=\"fa fa-medkit\"></i></a></li>";}
					if(priv("workers","payment")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"Munkabér összegzés\" onclick=\"javascript:showmunkabersum();\"><i class=\"fa fa-usd\"></i></a></li>";}
					if(priv("workers","edit")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"Szabadság összegző\" onclick=\"javascript:showszabisum();\"><i class=\"gi gi-beer\"></i></a></li>";}
					if(priv("workers","print")){echo"<li onclick=\"javascript:getprintscreen('".$_SESSION['planetsys']['actpage']['modulname']."','workerprintlist','');\" ><a href=\"#modal-regular-print\" data-toggle=\"modal\" title=\"Névsor nyomtatása\"><i class=\"fa fa-print\" ></i></a></li>";}
					//if(priv("producttype","view")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"Túlrendelt alapanyagok\"><i class=\"gi gi-thumbs_up\" onclick=\"javascript:showtalist();\"></i></a></li>";}			
					//if(priv("producttype","view")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"Közeli lejáratú alapanyagok\" onclick=\"javascript:showalertlist();\"><i class=\"gi gi-skull\"></i></a></li>";}			
					//if(priv("materials","story")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"Alapanyag kivezetés\" onclick=\"javascript:shownotconsumed();\"><i class=\"gi gi-send\"></i></a></li>";}			
					//if(priv("producttype","view")){echo"<li><a href=\"#tab-6\" data-toggle=\"tooltip\" title=\"Alapanyag minőségi dokumentumok\" onclick=\"javascript:showalapanyagdoclist();\"><i class=\"fa fa-list-ol\"></i></a></li>";}			
					//if(priv("producttype","view")){echo"<li><a href=\"#tab-7\" data-toggle=\"tooltip\" title=\"Csomagoló anyagok minőségi dokumentumai\" onclick=\"javascript:showcsomagolodoclist();\"><i class=\"fa fa-list-ul\"></i></a></li>";}			
					//if(priv("producttype","view")){echo"<li><a href=\"#tab-8\" data-toggle=\"tooltip\" title=\"Allergénlista\" onclick=\"javascript:showallergenlist();\"><i class=\"fa fa-th-list\"></i></a></li>";}			
					//if(priv("producttype","view")){echo"<li><a href=\"#tab-9\" data-toggle=\"tooltip\" title=\"Súlyozott alapanyag lista\" onclick=\"javascript:showweightmaterials();\"><i class=\"fa fa-sort-amount-desc\"></i></a></li>";}			
				?>	
			</ul>
							
		</div>
		<div class="tab-content">
			<div class="tab-pane active" id="tab-1">
				<div class="table-responsive">
					<table class="table table-vcenter table-striped">
						<thead>
							<tr>
								<th class="text-center">Dolgozó neve</th>
								<th class="text-center">Szemüveges</th>
								<th class="text-center">Kontaktlencsés</th>
							</tr>
						</thead>
						<tbody id="workerlist1"></tbody>
					</table>
				</div>
				<div style="clear:both;height:1px;"></div>
			</div>
			<div class="tab-pane" id="tab-2">
				<div class="table-responsive">
					<table class="table table-vcenter table-striped">
						<thead>
							<tr>
								<th class="text-center">Dolgozó neve</th>
								<th class="text-center">EÜ könyv van</th>
								<th class="text-center">EÜ könyv érvényesség</th>
							</tr>
						</thead>
						<tbody id="workerlist2"></tbody>
					</table>
				</div>
				<div style="clear:both;height:1px;"></div>
			</div>
			
			<div class="tab-pane" id="tab-3">
				<div class="block">
					<div class="block-title">
						<h2><strong>Hónapválasztó</strong></h2>
					</div>
					<input name="startDate" id="startDate" class="monthpicker form-control" value="<?=date("Y-m",time());?>" />
					
		
					<div class="table-responsive">
						<table class="table table-vcenter table-striped">
							<thead>
								<tr>
									<th class="text-center">Dolgozó neve</th>
									<th class="text-center">Sütike</th>
									<th class="text-center">Egyéb</th>
									<th class="text-center">Fizetés</th>
									<th class="text-center">Összesen</th>
								</tr>
							</thead>
							<tbody id="workerlist3"></tbody>
						</table>
					</div>
					<div style="clear:both;height:1px;"></div>
				</div>
			</div>
			
			<div class="tab-pane" id="tab-4">
				<div class="block">
					<div class="block-title">
						<h2><strong>Hónapválasztó</strong></h2>
					</div>
					<input name="startDate4" id="startDate4" class="monthpicker2 form-control" value="" />
					
		
					<div class="table-responsive">
						<table class="table table-vcenter table-striped">
							<thead>
								<tr>
									<th class="text-center">Dolgozó neve</th>
									<th class="text-center">Munkanap</th>
									<th class="text-center">Szabadság</th>
									<th class="text-center">Beteg</th>
									<th class="text-center">Egyéb</th>
									<th class="text-center">Összesen</th>
								</tr>
							</thead>
							<tbody id="workerlist4"></tbody>
						</table>
					</div>
					<div style="clear:both;height:1px;"></div>
				</div>
			</div>
			
		</div>		
	</div>					
</div>
<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<script src="/modules/goodworkers/js/inc_workerlists.js"></script>
<script >
	showszemuvegeslist();			
</script>
<?php include 'inc/template_end.php'; ?>
