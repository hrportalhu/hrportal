<?php
$fizuk=db_all("SELECT * FROM gw_worker_payments where status=1 and price_base='0'");

for($i=0;$i<count($fizuk);$i++){
		if($fizuk[$i]['price_type']=="hourly"){
			$pr=round($fizuk[$i]['price']/1);
			db_execute("update gw_worker_payments set price_base='".$pr."' where id='".$fizuk[$i]['id']."'");
				
		}
		elseif($fizuk[$i]['price_type']=="daily"){
			$pr=round($fizuk[$i]['price']/10);
			db_execute("update gw_worker_payments set price_base='".$pr."' where id='".$fizuk[$i]['id']."'");
		}
		elseif($fizuk[$i]['price_type']=="weekly"){
			$pr=round($fizuk[$i]['price']/50);
			db_execute("update gw_worker_payments set price_base='".$pr."' where id='".$fizuk[$i]['id']."'");
		}
		elseif($fizuk[$i]['price_type']=="monthly"){
			$pr=round($fizuk[$i]['price']/220);
			db_execute("update gw_worker_payments set price_base='".$pr."' where id='".$fizuk[$i]['id']."'");
		}	
}
?>
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Dolgozó kezelő
            </h1>
        </div>
    </div>
<?

		if(isset($uritags[3]) && $uritags[3]!=""){
			if($uritags[3]=="new"){
				?>
				<!-- Ez egy elem  -->		
				<ul class="breadcrumb breadcrumb-top">
					<li><a href="/">Rendszer</a></li>
					<li><a href="/modules/goodworkers/workers">Dolgozó kezelő</a></li>
					<li><a href="/new">Új dolgozó felvitele</a></li>
				</ul>
				<div class="block full">
					<div class="block-title">
						<h2><strong>Új dolgozó felvitele</strong></h2>
					</div>
				<?
					echo "<form id=\"newworkers\" name=\"newworkers\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
					echo"<ul class=\"nav nav-tabs\" data-toggle=\"tabs\">";
						echo"<li class=\"active\"><a href=\"#tab-a1\" data-toggle=\"tooltip\" title=\"Alapadatok\"><i class=\"fa fa-copy\"></i></a></li>";
						echo"<li><a href=\"#tab-a2\" data-toggle=\"tooltip\" title=\"Kontaktadatok\"><i class=\"fa fa-file-text-o\"></i></a></li>";
						echo"<li><a href=\"#tab-a3\" data-toggle=\"tooltip\" title=\"Kitöltendő anyagok\"><i class=\"fa fa-file-text-o\"></i></a></li>";
					echo"</ul>";
					echo"<div class=\"tab-content\">";
					
						echo"<div class=\"tab-pane active\" id=\"tab-a1\">";

							echo "<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"code\">Dolgozókód:</label>
								<div class=\"col-md-9\">
									<input type=\"text\" id=\"code\" name=\"code\" class=\"form-control\" placeholder=\"Dolgozókód\" />																		
								</div>
							</div>";

							
							echo "<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"name\">Név:</label>
								<div class=\"col-md-9\">
									<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" placeholder=\" Név\" />																		
								</div>
							</div>";
							
							echo "<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"birthname\">Születési név:</label>
								<div class=\"col-md-9\">
									<input type=\"text\" id=\"birthname\" name=\"birthname\" class=\"form-control\" placeholder=\" Születési név\" />																		
								</div>
							</div>";
							
							echo "<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"birthday\">Születési dátum:</label>
								<div class=\"col-md-9\">
									<input type=\"text\" id=\"birthday\" name=\"birthday\" class=\"form-control\" placeholder=\" Születési dátum\" />																		
								</div>
							</div>";
							
							echo "<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"birthplace\">Születési hely:</label>
								<div class=\"col-md-9\">
									<input type=\"text\" id=\"birthplace\" name=\"birthplace\" class=\"form-control\" placeholder=\" Születési dátum\" />																		
								</div>
							</div>";
							
							echo "<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"mothersname\">Anyja neve:</label>
								<div class=\"col-md-9\">
									<input type=\"text\" id=\"mothersname\" name=\"mothersname\" class=\"form-control\" placeholder=\" Anyja neve\" />																		
								</div>
							</div>";
							
							echo "<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"keynumber\">Szekrényszám:</label>
								<div class=\"col-md-9\">
									<input type=\"text\" id=\"keynumber\" name=\"keynumber\" class=\"form-control\" placeholder=\"Dolgozókód\" />																		
								</div>
							</div>";
							echo "<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"szigszam\">Szem. ig. szám:</label>
								<div class=\"col-md-9\">
									<input type=\"text\" id=\"szigszam\" name=\"szigszam\" class=\"form-control\" placeholder=\" Személyi szám\" />																		
								</div>
							</div>";
							
							echo "<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"tbszam\">TAJ szám:</label>
								<div class=\"col-md-9\">
									<input type=\"text\" id=\"tbszam\" name=\"tbszam\" class=\"form-control\" placeholder=\" TB szám\" />																		
								</div>
							</div>";
							
							echo "<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"adoszam\">Adóazonosító jele:</label>
								<div class=\"col-md-9\">
									<input type=\"text\" id=\"adoszam\" name=\"adoszam\" class=\"form-control\" placeholder=\" Adó szám\" />																		
								</div>
							</div>";
							
						echo "</div>";
				
						echo"<div class=\"tab-pane\" id=\"tab-a2\">";
						
							echo "<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"telefon\">Telefon:</label>
								<div class=\"col-md-9\">
									<input type=\"text\" id=\"telefon\" name=\"telefon\" class=\"form-control\" placeholder=\" Telefon\" />																		
								</div>
							</div>";
							
							echo "<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"email\">E-mail:</label>
								<div class=\"col-md-9\">
									<input type=\"text\" id=\"email\" name=\"email\" class=\"form-control\" placeholder=\" E-mail\" />																		
								</div>
							</div>";
							
							echo "<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"zip\">Irányítószám:</label>
								<div class=\"col-md-9\">
									<input type=\"text\" id=\"zip\" name=\"zip\" class=\"form-control\" placeholder=\" Irányítószám\" />																		
								</div>
							</div>";

							echo "<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"city\">Város:</label>
								<div class=\"col-md-9\">
									<input type=\"text\" id=\"city\" name=\"city\" class=\"form-control\" placeholder=\" Város\" />																		
								</div>
							</div>";
							
							echo "<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"address\">Lakcím:</label>
								<div class=\"col-md-9\">
									<input type=\"text\" id=\"address\" name=\"address\" class=\"form-control\" placeholder=\" Lakcím\" />																		
								</div>
							</div>";
							
							
							echo "<div class=\"form-group\" >
								<label class=\"col-md-3 control-label\" for=\"status\">Érvényes-e:</label>
								<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span></label>
							</div>";
							
						echo "</div>";	
						echo"<div class=\"tab-pane\" id=\"tab-a3\">";
						echo"<h1>Ide jönnek a letöltendő doksik</h1>";
						echo"</div>";
					echo "</div>";
					echo "<div style=\"margin-top:1px;\">";
							
						echo "<div style=\"clear:both;height:10px;\"></div>";
							
							echo "<div class=\"form-group form-actions\">
								<div class=\"col-md-9 col-md-offset-3\">
									<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewscreenworkers();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
								</div>
							</div>";
							
					echo "</div>
					</form>";
				?>
					</div>	
				</div>	
				<?php include 'inc/page_footer.php'; ?>
				<?php include 'inc/template_scripts.php'; ?>
				<script src="modules/goodworkers/js/inc_workers.js"></script>
				<?php include 'inc/template_end.php'; ?>	
				<?	
			}
			else{
				
				//echo "select * from functions where id='".$uritags[2]."'";
				$myworkers=db_row("select * from gw_workers where id='".$uritags[3]."'");
				if($myworkers['id']==''){
					die("Helytelen hozzáférés!");
				}
				else{
	?>
				<!-- Ez egy elem  -->		
				<ul class="breadcrumb breadcrumb-top">
					<li><a href="/">Rendszer</a></li>
					<li><a href="modules/goodworkers/workers">Dolgozók</a></li>
					<li><a href="/<?=$URI;?>"><?=$myworkers['name'];?></a></li>
				</ul>
				<div class="block full">
								   
						<!-- Block Tabs Title -->

						<div class="block-title">
							<h2><strong><?=$myworkers['name'];?></strong> dolgozók oldala</h2>
							<ul class="nav nav-tabs" data-toggle="tabs">
								<?
							if(priv("workers","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("workers","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							if(priv("workers","payment")){echo"<li><a href=\"#tab-7\" data-toggle=\"tooltip\" title=\"priceing\"><i class=\"fa fa-usd\"></i></a></li>";}
							if(priv("workers","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("workers","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
							if(priv("workers","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}
							if(priv("workers","shift")){echo "<li><a href=\"#tab-6\" data-toggle=\"tooltip\" title=\"shift\"><i class=\"gi gi-skull\"></i></a></li>";}	
							if(priv("workers","cookies")){echo "<li><a href=\"#tab-8\" data-toggle=\"tooltip\" title=\"cookies\"><i class=\"gi gi-cake\"></i></a></li>";}	
							if(priv("workers","cookies-sec")){echo "<li><a href=\"#tab-81\" data-toggle=\"tooltip\" title=\"cookies-sec\"><i class=\"gi gi-cake\"></i></a></li>";}	
							if(priv("workers","payment")){echo "<li><a href=\"#tab-9\" data-toggle=\"tooltip\" title=\"loan\"><i class=\"gi gi-money\"></i></a></li>";}	
							if(priv("workers","payment")){echo "<li><a href=\"#tab-10\" data-toggle=\"tooltip\" title=\"traning\"><i class=\"fa fa-graduation-cap\"></i></a></li>";}	
							if(priv("workers","payment")){echo "<li><a href=\"#tab-11\" data-toggle=\"tooltip\" title=\"monthly balance\"><i class=\"gi gi-coins\"></i></a></li>";}	
							?>	
							</ul>
						</div>
						<!-- END Block Tabs Title -->

						<!-- Tabs Content -->
						<div class="tab-content">

							<div class="tab-pane active" id="tab-1">
								<div class="row">
									<div class="col-md-6">
										<div class="block">
											
											<div class="block-title">
												<h2><strong>Alapadatok</strong></h2>
											</div>
											<dl class="dl-horizontal">
												<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$myworkers['id']); ?></dd>
												<dt>Dolgozókód:</dt><dd><? echo $myworkers['code']; ?></dd>
												<dt>Név:</dt><dd><? echo $myworkers['name']; ?></dd>
												<dt>Szekrényszám:</dt><dd><? echo $myworkers['keynumber']; ?></dd>
												<dt>Születési név:</dt><dd><? echo $myworkers['birthname']; ?></dd>
												<dt>Születési dátum:</dt><dd><? echo $myworkers['birthday']; ?></dd>
												<dt>Születési hely:</dt><dd><? echo $myworkers['birthplace']; ?></dd>
												<dt>Anyja neve:</dt><dd><? echo $myworkers['mothersname']; ?></dd>
												<dt>Személyi szám:</dt><dd><? echo $myworkers['szigszam']; ?></dd>
												<dt>TB szám:</dt><dd><? echo $myworkers['tbszam']; ?></dd>
												<dt>Adó szám:</dt><dd><? echo $myworkers['adoszam']; ?></dd>
												<dt>Státusz:</dt><dd><? if($myworkers['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
											</dl>
										</div>		
									</div>
									<div class="col-md-6">
										<div class="block">
											<div class="block-title">
												<h2><strong>Fotó</strong></h2>
											</div>
											<?php
												$pic=db_one("select filename from files where modul='goodworkers' and type='workers' and type_id='".$myworkers['id']."' order by cover desc");
												if(is_file($pic)){
													echo"<center><img src=\"/".$pic."\" alt=\"avatar\" style=\"height:180px;\" /></center>";
												}
												else{
													echo"<center><img src=\"img/placeholders/avatars/avatar9@2x.jpg\" alt=\"avatar\" /></center>";
												}
											?>
										</div>		
									
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="block">
											<div class="block-title">
												<h2><strong>Kontaktadatok</strong></h2>
											</div>
											<dl class="dl-horizontal">
												<dt>Telefon:</dt><dd><? echo $myworkers['telefon']; ?></dd>
												<dt>E-mail:</dt><dd><? echo $myworkers['email']; ?></dd>
												<dt>Irányítószám:</dt><dd><? echo $myworkers['zip']; ?></dd>
												<dt>Város:</dt><dd><? echo $myworkers['city']; ?></dd>
												<dt>Lakcím:</dt><dd><? echo $myworkers['address']; ?></dd>
											</dl>
										</div>		
									</div>
								</div>
								<div style="clear:both;height:1px;"></div>
							</div>
							<div class="tab-pane" id="tab-2">
								<?
									echo "<form id=\"editworkers\" name=\"editworkers\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
									echo"<ul class=\"nav nav-tabs\" data-toggle=\"tabs\">";
										echo"<li class=\"active\"><a href=\"#tab-a1\" data-toggle=\"tooltip\" title=\"Alapadatok\"><i class=\"fa fa-copy\"></i></a></li>";
										echo"<li><a href=\"#tab-a2\" data-toggle=\"tooltip\" title=\"Kontaktadatok\"><i class=\"fa fa-file-text-o\"></i></a></li>";
										if(priv("workers","edit-prio")){echo"<li><a href=\"#tab-a3\" data-toggle=\"tooltip\" title=\"Egyéb adatok\"><i class=\"fa fa-heart\"></i></a></li>";}
									echo"</ul>";
									echo"<div class=\"tab-content\">";
									
									echo"<div class=\"tab-pane active\" id=\"tab-a1\">";

											echo "<div class=\"form-group\">
												<label class=\"col-md-3 control-label\" for=\"code\">Dolgozókód:</label>
												<div class=\"col-md-9\">
													<input type=\"text\" id=\"code\" name=\"code\" class=\"form-control\" value=\"".$myworkers['code']."\" />																		
												</div>
											</div>";
											
											echo "<div class=\"form-group\">
												<label class=\"col-md-3 control-label\" for=\"name\">Név:</label>
												<div class=\"col-md-9\">
													<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" value=\"".$myworkers['name']."\" />																		
												</div>
											</div>";
											
											echo "<div class=\"form-group\">
												<label class=\"col-md-3 control-label\" for=\"birthname\">Születési név:</label>
												<div class=\"col-md-9\">
													<input type=\"text\" id=\"birthname\" name=\"birthname\" class=\"form-control\" value=\"".$myworkers['birthname']."\" />																		
												</div>
											</div>";
											

											
											echo "<div class=\"form-group\">
												<label class=\"col-md-3 control-label\" for=\"birthday\">Születési dátum:</label>
												<div class=\"col-md-9\">
													<input type=\"text\" id=\"birthday\" name=\"birthday\" class=\"form-control input-datepicker\" value=\"".$myworkers['birthday']."\" />																		
												</div>
											</div>";
											
																		
												echo "<div class=\"form-group\">
													<label class=\"col-md-3 control-label\" for=\"birthplace\">Születési hely:</label>
													<div class=\"col-md-9\">
														<input type=\"text\" id=\"birthplace\" name=\"birthplace\" class=\"form-control\" value=\"".$myworkers['birthplace']."\" />																		
													</div>
												</div>";
												
												echo "<div class=\"form-group\">
													<label class=\"col-md-3 control-label\" for=\"mothersname\">Anyja neve:</label>
													<div class=\"col-md-9\">
														<input type=\"text\" id=\"mothersname\" name=\"mothersname\" class=\"form-control\" value=\"".$myworkers['mothersname']."\" />																		
													</div>
												</div>";
											echo "<div class=\"form-group\">
												<label class=\"col-md-3 control-label\" for=\"keynumber\">Szekrényszám:</label>
												<div class=\"col-md-9\">
													<input type=\"text\" id=\"keynumber\" name=\"keynumber\" class=\"form-control\" value=\"".$myworkers['keynumber']."\" />																		
												</div>
											</div>";
											
											echo "<div class=\"form-group\">
												<label class=\"col-md-3 control-label\" for=\"startwork\">Belépés ideje:</label>
												<div class=\"col-md-9\">
													<input type=\"text\" id=\"startwork\" name=\"startwork\" class=\"form-control input-datepicker\" value=\"".$myworkers['startwork']."\" />																		
												</div>
											</div>";

											echo "<div class=\"form-group\">
												<label class=\"col-md-3 control-label\" for=\"endwork\">Kilépés ideje:</label>
												<div class=\"col-md-9\">
													<input type=\"text\" id=\"endwork\" name=\"endwork\" class=\"form-control input-datepicker\" value=\"".$myworkers['endwork']."\" />																		
												</div>
											</div>";
											
											echo "<div class=\"form-group\">
												<label class=\"col-md-3 control-label\" for=\"szigszam\">Szem ig. szám:</label>
												<div class=\"col-md-9\">
													<input type=\"text\" id=\"szigszam\" name=\"szigszam\" class=\"form-control\" value=\"".$myworkers['szigszam']."\" />																		
												</div>
											</div>";
											
											echo "<div class=\"form-group\">
												<label class=\"col-md-3 control-label\" for=\"tbszam\">TAJ szám:</label>
												<div class=\"col-md-9\">
													<input type=\"text\" id=\"tbszam\" name=\"tbszam\" class=\"form-control\" value=\"".$myworkers['tbszam']."\" />																		
												</div>
											</div>";
											
											echo "<div class=\"form-group\">
												<label class=\"col-md-3 control-label\" for=\"adoszam\">Adóazonosító jele:</label>
												<div class=\"col-md-9\">
													<input type=\"text\" id=\"adoszam\" name=\"adoszam\" class=\"form-control\" value=\"".$myworkers['adoszam']."\" />																		
												</div>
											</div>";
										echo "</div>";
				
										echo"<div class=\"tab-pane\" id=\"tab-a2\">";	
											
											echo "<div class=\"form-group\">
												<label class=\"col-md-3 control-label\" for=\"telefon\">Telefon:</label>
												<div class=\"col-md-9\">
													<input type=\"text\" id=\"telefon\" name=\"telefon\" class=\"form-control\" value=\"".$myworkers['telefon']."\" />																		
												</div>
											</div>";
											
											echo "<div class=\"form-group\">
												<label class=\"col-md-3 control-label\" for=\"email\">E-mail:</label>
												<div class=\"col-md-9\">
													<input type=\"text\" id=\"email\" name=\"email\" class=\"form-control\" value=\"".$myworkers['email']."\" />																		
												</div>
											</div>";
											
											echo "<div class=\"form-group\">
												<label class=\"col-md-3 control-label\" for=\"zip\">Irányítószám:</label>
												<div class=\"col-md-9\">
													<input type=\"text\" id=\"zip\" name=\"zip\" class=\"form-control\" value=\"".$myworkers['zip']."\" />																		
												</div>
											</div>";

											echo "<div class=\"form-group\">
												<label class=\"col-md-3 control-label\" for=\"city\">Város:</label>
												<div class=\"col-md-9\">
													<input type=\"text\" id=\"city\" name=\"city\" class=\"form-control\" value=\"".$myworkers['city']."\" />																		
												</div>
											</div>";
											
											echo "<div class=\"form-group\">
												<label class=\"col-md-3 control-label\" for=\"address\">Lakcím:</label>
												<div class=\"col-md-9\">
													<input type=\"text\" id=\"address\" name=\"address\" class=\"form-control\" value=\"".$myworkers['address']."\" />																		
												</div>
											</div>";
											
											
											
										echo "</div>";	
						
										echo"<div class=\"tab-pane\" id=\"tab-a3\">";	
											
											echo "<div class=\"form-group\">
												<label class=\"col-md-3 control-label\" for=\"iskolavegzettseg\">Iskolai végzettség:</label>
												<div class=\"col-md-9\">
													<input type=\"text\" id=\"iskolavegzettseg\" name=\"iskolavegzettseg\" class=\"form-control\" value=\"".$myworkers['iskolavegzettseg']."\" />																		
												</div>
											</div>";
											
											echo "<div class=\"form-group\">
												<label class=\"col-md-3 control-label\" for=\"worktime_hiv\">Bejelentett foglalkoztatási idő:</label>
												<div class=\"col-md-9\">
													<input type=\"text\" id=\"worktime_hiv\" name=\"worktime_hiv\" class=\"form-control\" value=\"".$myworkers['worktime_hiv']."\" />																		
												</div>
											</div>";
											
											echo "<div class=\"form-group\">
												<label class=\"col-md-3 control-label\" for=\"eukonyv_status\">Eü könyv érvényessége:</label>
												<div class=\"col-md-9\">
													<input type=\"text\" id=\"eukonyv_status\" name=\"eukonyv_status\" class=\"form-control input-datepicker\" value=\"".$myworkers['eukonyv_status']."\" />																		
												</div>
											</div>";
											
											echo"<div class=\"form-group\" >";
											$che="";if($myworkers['szemuveg']==1){ $che=" checked "; }
												echo"<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"szemuveg\" name=\"szemuveg\" ".$che."><span></span><br />Szemüveges</label>";
											echo"</div>";
											
		
											echo"<div class=\"form-group\" >";
											$che="";if($myworkers['kontaktlencse']==1){ $che=" checked "; }
												echo"<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"kontaktlencse\" name=\"kontaktlencse\" ".$che."><span></span><br />Kontaktlencsés</label>";
											echo"</div>";
											
											echo"<div class=\"form-group\" >";
											$che="";if($myworkers['eukonyv_ok']==1){ $che=" checked "; }
												echo"<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"eukonyv_ok\" name=\"eukonyv_ok\" ".$che."><span></span><br />Érvényes EÜ könyve van e?</label>";
											echo"</div>";
											
											echo"<div class=\"form-group\" >";
											$che="";if($myworkers['bizonyitvany']==1){ $che=" checked "; }
												echo"<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"bizonyitvany\" name=\"bizonyitvany\" ".$che."><span></span><br />Bizonyítvány?</label>";
											echo"</div>";
											
											echo"<div class=\"form-group\" >";
											$che="";if($myworkers['oktatas']==1){ $che=" checked "; }
												echo"<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"oktatas\" name=\"oktatas\" ".$che."><span></span><br />Oktatás?</label>";
											echo"</div>";
											
											echo"<div class=\"form-group\" >";
											$che="";if($myworkers['munkakori']==1){ $che=" checked "; }
												echo"<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"munkakori\" name=\"munkakori\" ".$che."><span></span><br />Munkaköri lerás?</label>";
											echo"</div>";
											
											echo"<div class=\"form-group\" >";
											$che="";if($myworkers['nyilatkozatok']==1){ $che=" checked "; }
												echo"<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"nyilatkozatok\" name=\"nyilatkozatok\" ".$che."><span></span><br />Nyilatkozatok?</label>";
											echo"</div>";
											
											echo"<div class=\"form-group\" >";
											$che="";if($myworkers['kilepopapirok']==1){ $che=" checked "; }
												echo"<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"kilepopapirok\" name=\"kilepopapirok\" ".$che."><span></span><br />Kilépő papírok</label>";
											echo"</div>";
											
											echo"<div class=\"form-group\" >";
											$che="";if($myworkers['monthlypay']==1){ $che=" checked "; }
												echo"<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"monthlypay\" name=\"monthlypay\" ".$che."><span></span><br />Havi elszámolású</label>";
											echo"</div>";
											
											
											
											echo"<div class=\"form-group\" >";
											$chz="";if($myworkers['status']==1){ $chz=" checked "; }
												echo"<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" ".$chz."><span></span><br />Aktív dolgozó?</label>";
											echo"</div>";
											
										echo "</div>";	
						
									echo "</div>";
									echo "<div style=\"margin-top:1px;\">";
											
										echo "<div style=\"clear:both;height:10px;\"></div>";
											
											echo "<div class=\"form-group form-actions\">
												<div class=\"col-md-9 col-md-offset-3\">
													<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditscreenworkers('".$myworkers['id']."')\"><i class=\"fa fa-angle-right\"></i> Ment</button>
												</div>
											</div>";
											
									echo "</div>
									</form>";
								?>
							</div>
							<div class="tab-pane" id="tab-3">
								<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$myworkers['id'],"Dokumentum feltöltés")."</div>";
								?> 
								<div id="filelists"></div>
								<div style="clear:both;height:1px;"></div>
						</div>
							<div class="tab-pane" id="tab-4">
									<?
										echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$myworkers['id'],"Hozzászólások")."</div>";
									?>
									
							</div>
							
							<div class="tab-pane" id="tab-5">
								<div id="printdiv"></div>
							
							</div>
							<div class="tab-pane" id="tab-6">
								<div id="shiftdiv">
									<?
								$munkanaps=db_all("select * from gw_shift_x_worker where worker_id='".$myworkers['id']."' order by workday desc, id desc");
?>
						<table class="table table-vcenter table-striped">
							<thead>
								<tr>
									<th style="width: 100px;" class="text-center"><i class="gi gi-disk_save"></i></th>
									<th class="text-center">Munkanap</th>
									<th class="text-center">Munkahely</th>
									<th class="text-center" style="width:120px;">-tól</th>
									<th class="text-center" style="width:120px;">-ig</th>
									<th class="text-center">Munkaidő</th>
									<th class="text-center">Munkabér</th>
									<th class="text-center">Státusz</th>
								</tr>
							</thead>
							<tbody >
							<?
							$posts=db_all("select id,name from gw_posts where status='1' order by name");
								for($i=0;$i<count($munkanaps);$i++){
									echo"<tr id=\"wsrow_".$munkanaps[$i]['id']."\">";
										echo"<td style=\"width: 100px;\" class=\"text-center\"><a href=\"/modules/goodworkers/timesheet/".$munkanaps[$i]['id']."#tab-2\">".$munkanaps[$i]['id']."</a></td>";
										echo"<td style=\"width: 100px;\" class=\"text-center\">".$munkanaps[$i]['workday']."</td>";
										echo"<td style=\"width: 100px;\" class=\"text-center\">";
										echo"<select id=\"post_".$munkanaps[$i]['id']."\" name=\"post_".$munkanaps[$i]['id']."\" class=\"form-control\" onchange=\"saveworkdaypost(".$munkanaps[$i]['id'].");\">
											<option value=\"0\">Kérlek válassz</option>";
											for($a=0;$a<count($posts);$a++){
												$sel="";if($munkanaps[$i]['post_id']==$posts[$a]['id']){$sel=" selected ";}
												echo"<option value=\"".$posts[$a]['id']."\" ".$sel.">".$posts[$a]['name']."</option>";	
											}
											$se2="";if($munkanaps[$i]['post_id']=="100000"){$se2=" selected ";}
											echo"<option value=\"100000\" ".$se2.">Szabadságon</option>";
											$se3="";if($munkanaps[$i]['post_id']=="200000"){$se3=" selected ";}
											echo"<option value=\"200000\" ".$se3.">Beteg</option>";
											$se4="";if($munkanaps[$i]['post_id']=="300000"){$se4=" selected ";}
											echo"<option value=\"300000\" ".$se4.">Egyéb</option>";
												

											echo"</select>";
										echo"</td>";
										echo"<td style=\"width: 100px;\" class=\"text-center\">";
										echo"<div class=\"input-group bootstrap-timepicker\">
												<input type=\"text\" id=\"st_".$munkanaps[$i]['id']."\" name=\"st_".$munkanaps[$i]['id']."\" class=\"form-control input-timepicker24\" value=\"".substr($munkanaps[$i]['starttime'],11,5)."\">
												<span class=\"input-group-btn\">
													<a href=\"javascript:void(0)\" class=\"btn btn-primary\"><i class=\"fa fa-clock-o\"></i></a>
												</span>
											</div>";
										echo"</td>";
										echo"<td style=\"width: 100px;\" class=\"text-center\">";
										echo"<div class=\"input-group bootstrap-timepicker\">
												<input type=\"text\" id=\"en_".$munkanaps[$i]['id']."\" name=\"en_".$munkanaps[$i]['id']."\" class=\"form-control input-timepicker24\" value=\"".substr($munkanaps[$i]['endtime'],11,5)."\">
												<span class=\"input-group-btn\">
													<a href=\"javascript:void(0)\" class=\"btn btn-primary\"><i class=\"fa fa-clock-o\"></i></a>
												</span>
											</div>";
										echo"</td>";
										echo"<td style=\"width: 100px;\" class=\"text-center\">".$munkanaps[$i]['worktime']." óra</td>";
										echo"<td style=\"width: 100px;\" class=\"text-center\">".huf($munkanaps[$i]['dailycash'])." Ft</td>";
										echo"<td>";
										if(priv("workers","shift-edit")){
											echo"<div class=\"input-group\">
												<span class=\"input-group-btn\">
													<a href=\"javascript:saveworkerspayrow(".$munkanaps[$i]['id'].");\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></a>&nbsp;&nbsp;
													<a href=\"javascript:saveworkersminwork(".$munkanaps[$i]['id'].");\" class=\"btn btn-primary\"><i class=\"fa fa-bullhorn\"></i></a>
												</span>";
											echo"</div>";
										}	
										echo"</td>";
									echo"</tr>";
								}
							?>
							</tbody>
						</table>
								
								</div>
							
							</div>
							<div class="tab-pane" id="tab-7">
								<div>Órabére</div>
								<div class="table-responsive">
									<table class="table table-vcenter table-striped">
									<thead>
										<tr>
											<th style="width: 100px;" class="text-center"><i class="gi gi-disk_save"></i></th>
											<th class="text-center">Munkabér</th>
											<th class="text-center">Óradíj</th>
											<th class="text-center">Munkabér számítás alapja</th>
											<th class="text-center">-tól</th>
											<th class="text-center">Státusz</th>
											
										</tr>
									</thead>
									<tbody  id="paymentcont"></tbody>
									</table>		
								</div>
							</div>
							
							<div class="tab-pane" id="tab-8">
								<div class="block">
									<div class="block-title">
										<h2><strong>Sütemény</strong></h2>
									</div>
									<div class="table-responsive">
										<table class="table table-vcenter table-striped">
										<thead>
											<tr>
												<th style="width: 100px;" class="text-center"><i class="gi gi-disk_save"></i></th>
												<th class="text-center">Sütemény neve</th>
												<th class="text-center">Mennyiség</th>
												<th class="text-center">Ár</th>
												<th class="text-center">Dátum</th>
												<th class="text-center">Állapot</th>
												<th class="text-center">Művelet</th>
												
											</tr>
										</thead>
										<tbody  id="cookiescont"></tbody>
										</table>		
									</div>
								</div>
							</div>
							
							<div class="tab-pane" id="tab-81">
								<div class="block">
									<div class="block-title">
										<h2><strong>Sütemény</strong></h2>
									</div>
									<div class="table-responsive">
										<table class="table table-vcenter table-striped">
										<thead>
											<tr>
												<th style="width: 100px;" class="text-center"><i class="gi gi-disk_save"></i></th>
												<th class="text-center">Sütemény neve</th>
												<th class="text-center">Mennyiség</th>
												<th class="text-center">Dátum</th>
												<th class="text-center">Állapot</th>
												<th class="text-center">Művelet</th>
												
											</tr>
										</thead>
										<tbody  id="cookiescont2"></tbody>
										</table>		
									</div>
								</div>
							</div>
						
							<div class="tab-pane" id="tab-9">
								<div class="block">
									<div class="block-title">
										<h2><strong>Pénzügyek</strong></h2>
									</div>
									<div class="table-responsive">
										<table class="table table-vcenter table-striped">
										<thead>
											<tr>
												<th style="width: 100px;" class="text-center"><i class="gi gi-disk_save"></i></th>
												<th class="text-center">Terhelés típusa</th>
												<th class="text-center">Terhelés összege</th>
												<th class="text-center">Törlesztés</th>
												<th class="text-center">Tervezett Dátum</th>
												<th class="text-center">Megjegyzés</th>
												<th class="text-center">Státusz</th>
												<th class="text-center">Művelet</th>
											</tr>
										</thead>
										<tbody  id="loancont"></tbody>
										</table>		
									</div>
								</div>
							</div>
							<div class="tab-pane" id="tab-10">
								<div class="block">
									<div class="block-title">
										<h2><strong>Oktatások</strong></h2>
									</div>
									<div class="table-responsive">
										<table class="table table-vcenter table-striped">
										<thead>
											<tr>
												<th style="width: 100px;" class="text-center"><i class="gi gi-disk_save"></i></th>
												<th class="text-center">Dátum</th>
												<th class="text-center">Elnevezés</th>
												<th class="text-center">Művelet</th>
												
											</tr>
										</thead>
										<tbody  id="traningcont"></tbody>
										</table>		
									</div>
								</div>
							</div>
						
							<div class="tab-pane" id="tab-11">
								<div class="block">
									<div class="block-title">
										<h2><strong>Hónapválasztó</strong></h2>
									</div>
									<input name="startDate" id="startDate" class="monthpicker form-control" value="<?=date("Y-m",time());?>" />
									<input name="worker_id" id="worker_id" type="hidden" value="<?=$myworkers['id'];?>" />
								</div>
								<div id="monthsum"></div>
								
							</div>
						
						
						</div>
						<!-- END Tabs Content -->
					</div>
					<!-- END Block Tabs -->
						
				</div>
				
				<?php include 'inc/page_footer.php'; ?>
				<?php include 'inc/template_scripts.php'; ?>
				<script src="modules/goodworkers/js/inc_workers.js"></script>

				<script >
					showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$myworkers['id'];?>,'Dokumentum feltöltés','filelists');
					//showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$myworkers['id'];?>,'Dokumentum feltöltés','filelists');
					showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$myworkers['id'];?>,'Dokumentum feltöltés','filelists');
					showpaymenttable('<?=$myworkers['id'];?>');			
					showtraningtable('<?=$myworkers['id'];?>');			
					showcookiestable('<?=$myworkers['id'];?>');			
					showcookiestable2('<?=$myworkers['id'];?>');			
					showloantable('<?=$myworkers['id'];?>');			
					showmonthsum();			
			
				</script>
				<script>$(function(){ workersDatatables.init(); });</script>	
							<?
					if(substr($uritags[4],0,4)=="tab-"){
						?>
						<script>
							var activeTab = $('[href=#<?=$uritags[4];?>]');
							activeTab && activeTab.tab('show');
						</script>	
					
						<?
					}
					if(substr($uritags[5],0,4)=="tab-"){
						?>
						<script>
							var activeTab = $('[href=#<?=$uritags[5];?>]');
							activeTab && activeTab.tab('show');
						</script>	
					
						<?
					}
				?>

				<?php include 'inc/template_end.php'; ?>		
	<?
				}
			}
		}
		else{
			//print_r($_SESSION['planetsys'])
?>    
	<!-- Ez a lista  -->
				<ul class="breadcrumb breadcrumb-top">
					<li><a href="/">Rendszer</a></li>
					<li><a href="/<?=$URI;?>">Dolgozók</a></li>
				</ul>
				<!-- END Datatables Header -->

				<!-- Datatables Content -->
				<div class="block full">
					<div class="block-title">
						<h2><strong>Dolgozó</strong> lista</h2>
					</div>
					

					<div class="table-responsive">
						<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
							<thead>
								<tr>
									<?
										foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
											if($screen['elements'][$f]['textname']!=""){
												echo"<th>".$screen['elements'][$f]['textname']."</th>";
											}
										}
										//echo"<th>Művelet</th>";
										?>
								</tr>
							</thead>
							<tbody ></tbody>
						</table>
					</div>
				</div>
				<!-- END Datatables Content -->
			</div>
			<!-- END Page Content -->



			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>

			<!-- Load and execute javascript code used only in this page -->
			<script src="modules/goodworkers/js/inc_workers.js"></script>
			<script>$(function(){ workersDatatables.init(); });</script>

			<?php include 'inc/template_end.php'; ?>
<?
}
?>

