<?php
$most=date('Y-m-d',time());


//$_SESSION['planetsys']['docusys']['kateg']


$screen['elements']['id']['name']="id";
$screen['elements']['id']['textname']="Sorszám";
$screen['elements']['id']['type']="text50";
$screen['elements']['id']['type_param']="";
$screen['elements']['id']['help']="";
$screen['elements']['id']['defval']="";
$screen['elements']['id']['conected']=0;
$screen['elements']['id']['con_table']="";
$screen['elements']['id']['con_table_shortname']="";
$screen['elements']['id']['con_table_fileds']="";
$screen['elements']['id']['width']="";
$screen['elements']['id']['deflist']=0;
$screen['elements']['id']['scrdb']=0;
$screen['elements']['id']['scrcmd']=1;

$screen['elements']['name']['name']="name";
$screen['elements']['name']['textname']="Számlaszám";
$screen['elements']['name']['type']="text50";
$screen['elements']['name']['type_param']="";
$screen['elements']['name']['help']="Tartalom elnevezése";
$screen['elements']['name']['defval']="";
$screen['elements']['name']['conected']=0;
$screen['elements']['name']['con_table']="";
$screen['elements']['name']['con_table_shortname']="";
$screen['elements']['name']['con_table_fileds']="";
$screen['elements']['name']['width']="";
$screen['elements']['name']['deflist']=1;
$screen['elements']['name']['scrdb']=1;
$screen['elements']['name']['scrcmd']=1;



$screen['elements']['partner_id']['name']="partner_id";
$screen['elements']['partner_id']['textname']="Partner:";
$screen['elements']['partner_id']['type']="selectsql";
$screen['elements']['partner_id']['type_param']="SELECT id,name FROM partners WHERE status = 1 order by name";
$screen['elements']['partner_id']['help']="Tartalom elnevezése";
$screen['elements']['partner_id']['defval']="";
$screen['elements']['partner_id']['conected']=1;
$screen['elements']['partner_id']['con_table']="partners";
$screen['elements']['partner_id']['con_table_shortpartner_id']="p";
$screen['elements']['categories_id']['con_table_fileds'][0]="id";
$screen['elements']['categories_id']['con_table_fileds'][1]="name";
$screen['elements']['partner_id']['width']="";
$screen['elements']['partner_id']['deflist']=1;
$screen['elements']['partner_id']['scrdb']=1;
$screen['elements']['partner_id']['scrcmd']=1;

$screen['elements']['kelt']['name']="kelt";
$screen['elements']['kelt']['textname']="Kelt:";
$screen['elements']['kelt']['type']="date";
$screen['elements']['kelt']['type_param']="";
$screen['elements']['kelt']['help']="Tartalom elnevezése";
$screen['elements']['kelt']['defval']="";
$screen['elements']['kelt']['conected']=0;
$screen['elements']['kelt']['con_table']="";
$screen['elements']['kelt']['con_table_shortkelt']="";
$screen['elements']['kelt']['con_table_fileds']="";
$screen['elements']['kelt']['width']="";
$screen['elements']['kelt']['deflist']=1;
$screen['elements']['kelt']['scrdb']=1;
$screen['elements']['kelt']['scrcmd']=1;


$screen['elements']['telj']['name']="telj";
$screen['elements']['telj']['textname']="Teljesítés ideje";
$screen['elements']['telj']['type']="date";
$screen['elements']['telj']['type_param']="";
$screen['elements']['telj']['help']="Tartalom elnevezése";
$screen['elements']['telj']['defval']="";
$screen['elements']['telj']['conected']=0;
$screen['elements']['telj']['con_table']="";
$screen['elements']['telj']['con_table_shorttelj']="";
$screen['elements']['telj']['con_table_fileds']="";
$screen['elements']['telj']['width']="";
$screen['elements']['telj']['deflist']=1;
$screen['elements']['telj']['scrdb']=1;
$screen['elements']['telj']['scrcmd']=1;


$screen['elements']['hatido']['name']="hatido";
$screen['elements']['hatido']['textname']="Fizetés határideje";
$screen['elements']['hatido']['type']="date";
$screen['elements']['hatido']['type_param']="";
$screen['elements']['hatido']['help']="Tartalom elnevezése";
$screen['elements']['hatido']['defval']="";
$screen['elements']['hatido']['conected']=0;
$screen['elements']['hatido']['con_table']="";
$screen['elements']['hatido']['con_table_shorthatido']="";
$screen['elements']['hatido']['con_table_fileds']="";
$screen['elements']['hatido']['width']="";
$screen['elements']['hatido']['deflist']=1;
$screen['elements']['hatido']['scrdb']=1;
$screen['elements']['hatido']['scrcmd']=1;

$screen['elements']['status']['name']="status";
$screen['elements']['status']['textname']="Státusz";
$screen['elements']['status']['type']="status";
$screen['elements']['status']['type_param']="";
$screen['elements']['status']['help']="";
$screen['elements']['status']['defval']=1;
$screen['elements']['status']['conected']=0;
$screen['elements']['status']['con_table']="";
$screen['elements']['status']['con_table_shortname']="";
$screen['elements']['status']['con_table_fileds']="";
$screen['elements']['status']['width']="";
$screen['elements']['status']['deflist']=0;
$screen['elements']['status']['scrdb']=1;
$screen['elements']['status']['scrcmd']=1;

$screen['tableprefix']="";
$screen['modulname']="invicesys";
$screen['funcname']="invoice";
$screen['mylimit']=20;
$screen['mywidth']="";
$screen['scrtable']=$screen['tableprefix']."invoces_header";
$screen['scrtable_shortname']=$screen['tableprefix']."ih";
getlistoptions($screen);
$screen['screenlist']=$_SESSION['planetsys']['listoption']['screenlist'];
$screen['screendb']=$_SESSION['planetsys']['screendb'];
$statuscode = 0;

?>
