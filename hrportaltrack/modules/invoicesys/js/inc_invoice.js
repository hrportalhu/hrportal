/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */

	
var invoiceDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/invoicesys/invoice/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"aaSorting": [[ 0, "desc" ]],
				"iDisplayLength": 10,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/invoicesys/ajax_invoice_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });



            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveeditscreeninvoices(elemid){
	var valid = f_c($('#editinvoice'));
	if(valid){
		f_xml(ServerName +'/modules/invoicesys/ajax_invoice_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editinvoice').serialize(),'');
	}
}


function savenewscreeninvoice(){
	var valid = f_c($('#newinvoice'));
	if(valid){
		f_xml(ServerName +'/modules/invoicesys/ajax_invoice_data.php','newsubmit=1&' + $('#newinvoice').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/invoicesys/ajax_invoice_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}

function addszlaelement(elemid,targetdiv){
	if($('#pt_id').val()!="0" && $('#mennyiseg').val()!="" ){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/invoicesys/ajax_addszlaelement.php',
		data:'newsubmit=1&elemid='+elemid+'&'+$('#newelementformszamla').serialize(),
		success: function(data){
			
			showszlaelement(elemid,targetdiv);
			$('#mennyiseg').val('');

		}});
	}
	else{
		alert("Üres tételt nem lehet betölteni!");
	}
}

function showszlaelement(elemid,targetdiv){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/invoicesys/ajax_showszlaelement.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#'+targetdiv).html(data);
		}});
}
