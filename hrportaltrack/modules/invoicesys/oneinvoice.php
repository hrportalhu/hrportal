<!-- Page content -->

	<div id="page-content">
		<!-- Invoice Header -->
		<div class="content-header">
			<div class="header-section">
				<h1>
					<i class="gi gi-usd"></i>Számlázás<br><small></small>
				</h1>
			</div>
		</div>
		<ul class="breadcrumb breadcrumb-top">
			<li>Oldalak</li>
			<li><a href="">Számla</a></li>
		</ul>
		<!-- END Invoice Header -->

		<!-- Invoice Block -->
		<div class="block full" id="printdiv">
			<!-- Invoice Title -->
			<div class="block-title">
				<div class="block-options pull-right">
					<a href="javascript:void(0)" class="btn btn-sm btn-alt btn-default" data-toggle="tooltip" title="Új számla"><i class="fa fa-plus"></i></a>
					<a href="javascript:void(0)" class="btn btn-sm btn-alt btn-default" data-toggle="tooltip" title="Számla törlés"><i class="fa fa-times"></i></a>
				</div>
				<h2><strong>Számlaszám</strong> #PLA0002</h2>
			</div>
			<!-- END Invoice Title -->

			<!-- Invoice Content -->
			<!-- 2 Column grid -->
			<div class="row block-section">
				<!-- Company Info -->
				<div class="col-sm-6">
					
					<hr>
					<h2><strong>Planet Star Kft</strong></h2>
					<address>
						Szeged<br>
						Francia utca 24<br>
						6724<br><br>
						<i class="fa fa-phone"></i> (36) 30 4364459<br>
						<i class="fa fa-envelope-o"></i> <a href="javascript:void(0)">planetstar@planetstar.hu</a>
					</address>
				</div>
				<!-- END Company Info -->

				<!-- Client Info -->
				<div class="col-sm-6 text-right">
					
					<hr>
					<h2><strong>SZEPLAST Zrt.</strong></h2>
					<address>
						Szeged<br>
						Külterület 4.<br>
						6728<br><br>
						+36 (62) 555-800 <i class="fa fa-phone"></i><br>
						<a href="javascript:void(0)">office@szeplast.hu</a> <i class="fa fa-envelope-o"></i>
					</address>
				</div>
				<!-- END Client Info -->
			</div>
			<!-- END 2 Column grid -->

			<hr>

			<!-- Table -->
			<div class="table-responsive">
				<table class="table table-vcenter">
					<thead>
						<tr>
							<th></th>
							<th style="width: 60%;">Termék</th>
							<th class="text-center">Menyiség</th>
							<th class="text-center">Egységár</th>
							<th class="text-right">Összesen</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="text-center">1</td>
							<td>
								<h4>Specifikáció</h4>
								<span class="label label-info"><i class="fa fa-clock-o"></i> 30 nap</span>
								<span class="label label-info">4 revizó készült</span>
							</td>
							<td class="text-center"><strong>x <span class="badge">1</span></strong></td>
							<td class="text-center"><strong> 1 000 EUR</strong></td>
							<td class="text-right"><span class="label label-primary"> 1 000 EUR</span></td>
						</tr>
						<tr>
							<td class="text-center">2</td>
							<td>
								<h4>Kiszállási díj</h4>
								<span class="label label-info"><i class="fa fa-clock-o"></i> 3 alkalom</span>
								<span class="label label-info">2 telephelyre</span>
							</td>
							<td class="text-center"><strong>x <span class="badge">3</span></strong></td>
							<td class="text-center"><strong> 200 EUR</strong></td>
							<td class="text-right"><span class="label label-primary"> 600 EUR</span></td>
						</tr>
						
						<tr class="active">
							<td colspan="4" class="text-right"><span class="h4">Nettó érték</span></td>
							<td class="text-right"><span class="h4"> 1 600</span></td>
						</tr>
						<tr class="active">
							<td colspan="4" class="text-right"><span class="h4">Áfa</span></td>
							<td class="text-right"><span class="h4">27%</span></td>
						</tr>
						<tr class="active">
							<td colspan="4" class="text-right"><span class="h4">Áfa Összege</span></td>
							<td class="text-right"><span class="h4">432 EUR</span></td>
						</tr>
						<tr class="active">
							<td colspan="4" class="text-right"><span class="h3"><strong>Végösszeg</strong></span></td>
							<td class="text-right"><span class="h3"><strong>2 032 EUR</strong></span></td>
						</tr>
					</tbody>
				</table>
			</div>
			<!-- END Table -->

			<div class="clearfix">
				<div class="btn-group pull-right">
					<a href="javascript:void(0)" class="btn btn-default"><i class="fa fa-print"></i> Ment és nyomtat</a>
					<a href="javascript:void(0)" class="btn btn-primary"><i class="fa fa-angle-right"></i> Számla küldése</a>
				</div>
			</div>
			<!-- END Invoice Content -->
		</div>
		<!-- END Invoice Block -->
	</div>
	<!-- END Page Content -->

			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>

			<!-- Load and execute javascript code used only in this page -->

			<?php include 'inc/template_end.php'; ?>
