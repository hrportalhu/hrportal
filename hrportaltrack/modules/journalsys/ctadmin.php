
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Saját CT kezelő
            </h1>
        </div>
    </div>
<?
$ceg=db_all("select id, name from partners where status=1 order by name");
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/journalsys/ctadmin">Saját CT kezelő</a></li>
				<li><a href="/new">Új rendelés</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új rendelés</strong></h2>
				</div>
			<?
			
				
				echo "<form id=\"newctadmin\" name=\"newctadmin\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";
							
							echo"<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"partner_id\">Megrendelő:</label>
							<div class=\"col-md-8\">
								<select id=\"partner_id\" name=\"partner_id\" class=\"form-control\" placeholder=\"Megrendelő\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($ceg);$i++){
									echo"<option value=\"".$ceg[$i]['id']."\" >".$ceg[$i]['name']."</option>";	
								}
								echo"</select>
							</div>
						</div>";
						
			
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"name\">Elnevezés:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" placeholder=\"Elnevezés\" />
							</div>
						</div>";
						
					echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"startdate\">Publikálás kezdete:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"startdate\" name=\"startdate\" class=\"form-control input-datepicker\" placeholder=\"Publikálás kezdete\" />
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"enddate\">Publikálás vége:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"enddate\" name=\"enddate\" class=\"form-control input-datepicker\" placeholder=\"Publikálás vége\" />
							</div>
						</div>";
							echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"target_link\">Partner CT link:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"target_link\" name=\"target_link\" class=\"form-control\" placeholder=\"Partner CT link - http és https nélkül\" />
							</div>
						</div>";
					

						echo "<div style=\"clear:both;height:10px;\"></div>";
						
						echo "<div class=\"form-group form-actions\">
							<div class=\"col-md-6\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewctadmin();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>";
						
				echo "</div>
				</form>";
			?>
			</div>	
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/journalsys/js/inc_ctadmin.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?	
		}
		else{

			//echo "select * from functions where id='".$uritags[2]."'";
			$myctadmin=db_row("select * from tag_urls where id='".$uritags[3]."'");
			
			if($myctadmin['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
				$ct1=db_one("select count(id) from tag_ads_counter where ta_id='".$myctadmin['id']."' ");
				$ct2=db_one("select sum(counterem) from tag_ads_counter_sum where ta_id='".$myctadmin['id']."' and type='ct'");
				$count=$ct1+$ct2;
				db_execute("update tag_urls set counter='".$count."' where id='".$myctadmin['id']."'");

			naplo("info","user","Szavazás betöltése","journalsys","ctadmin",$uritags[3],'');	
?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="modules/journalsys/ctadmin"> Rendelési lista</a></li>
				<li><a href="/<?=$URI;?>"><?=$myctadmin['name'];?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->

					<div class="block-title">
						<h2><strong><?=$myctadmin['name'];?> </strong></h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
							if(priv("ctadmin","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("ctadmin","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							if(priv("ctadmin","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("ctadmin","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
							if(priv("ctadmin","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}	
							?>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="col-md-12">
								<dl class="dl-horizontal">
									<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$myctadmin['id']); ?></dd>
									<dt>Megrendelő:</dt><dd><? echo idchange("partners","name",$myctadmin['partner_id']); ?></dd>
									<dt>Elnevezés:</dt><dd><? echo $myctadmin['name']; ?></dd>
									<dt>Publikálás kezdete:</dt><dd><? echo $myctadmin['startdate']; ?></dd>
									<dt>Publikálás vége:</dt><dd><? echo $myctadmin['enddate']; ?></dd>
									<dt>Partner CT Link:</dt><dd><? echo $myctadmin['target_link']; ?></dd>
									<dt>Generált CT Link:</dt><dd><? echo $myctadmin['mylink']; ?></dd>
									<dt>Számláló:</dt><dd>
									<? 
										echo $count; 
									?>
									</dd>
									<dt>Rögzítő:</dt><dd><? echo idchange("users","realname",$myctadmin['user_id']); ?></dd>
									<dt>Státusz:</dt><dd><? if($myctadmin['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
								</dl>	
							</div>

							
							<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-2">
						<?
						
							
							echo"<form id=\"editctadmin\" name=\"editctadmin\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
							echo "<div style=\"margin-top:1px;\">";
							echo"<div class=\"form-group\">
								<label class=\"col-md-4 control-label\" for=\"partner_id\">Megrendelő:</label>
								<div class=\"col-md-8\">
									<select id=\"partner_id\" name=\"partner_id\" class=\"form-control\" placeholder=\"Megrendelő\" >
									<option value=\"0\">Kérlek válassz</option>";
									for($i=0;$i<count($ceg);$i++){
										$sel="";if($ceg[$i]['id']==$myctadmin['partner_id']){$sel=" selected ";}
										echo"<option value=\"".$ceg[$i]['id']."\" ".$sel.">".$ceg[$i]['name']."</option>";	
									}
									echo"</select>
								</div>
							</div>";
								echo "<div class=\"form-group\">
									<label class=\"col-md-4 control-label\" for=\"name\">Elnevezés:</label>
									<div class=\"col-md-8\">
										<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" value=\"".$myctadmin['name']."\" />
									</div>
								</div>";
								
									echo "<div class=\"form-group\">
										<label class=\"col-md-4 control-label\" for=\"startdate\">Publikálás kezdete:</label>
										<div class=\"col-md-8\">
											<input type=\"text\" id=\"startdate\" name=\"startdate\" class=\"form-control input-datepicker\"  value=\"".$myctadmin['startdate']."\"/>
										</div>
									</div>";
									
									echo "<div class=\"form-group\">
										<label class=\"col-md-4 control-label\" for=\"enddate\">Publikálás vége:</label>
										<div class=\"col-md-8\">
											<input type=\"text\" id=\"enddate\" name=\"enddate\" class=\"form-control input-datepicker\"  value=\"".$myctadmin['enddate']."\"/>
										</div>
									</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-4 control-label\" for=\"target_link\">Partner CT link:</label>
									<div class=\"col-md-8\">
										<input type=\"text\" id=\"target_link\" name=\"target_link\" class=\"form-control\" value=\"".$myctadmin['target_link']."\" />
									</div>
								</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-4 control-label\" for=\"mylink\">Saját CT link:</label>
									<div class=\"col-md-8\">
										<input type=\"text\" id=\"mylink\" name=\"mylink\" class=\"form-control\" value=\"".$myctadmin['mylink']."\" />
									</div>
								</div>";
									
								echo "<div class=\"form-group\" >
									<label class=\"col-md-4 control-label\" for=\"status\">Érvényes-e:</label>";
									$st="";if($myctadmin['status']==1){ $st=" checked "; }
									echo "<label class=\"col-md-8 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\"".$st." ><span></span></label>
								</div>";
								
								echo "<div class=\"form-group form-actions\">
									<div class=\"col-md-6 \">
										<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditctadmin(".$myctadmin['id'].")\"><i class=\"fa fa-angle-right\"></i> Ment</button>
									</div>
								</div>";
									
							echo "</div>
							</form>";
						?>
						</div>

						<div class="tab-pane" id="tab-3">
								<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$myctadmin['id'],"Dokumentum feltöltés")."</div>";
								?> 
								<div id="filelists"></div>
								<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$myctadmin['id'],"Hozzászólások")."</div>";
								?>
						</div>
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>
						
						
					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
					
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/journalsys/js/inc_ctadmin.js"></script>

			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$myctadmin['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$myctadmin['id'];?>,'Dokumentum feltöltés','filelists');
				
			</script>
			<script>$(function(){ ctadminDatatables.init(); });</script>	

			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
		//print_r($_SESSION['planetsys'])
?>    
<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Rendelések</a></li>
			</ul>
			<!-- END Datatables Header -->
		
				
				
			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Rendelési</strong> lista</h2>
				</div>
				
				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							//echo"<th>Művelet</th>";
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/journalsys/js/inc_ctadmin.js"></script>
		<script>$(function(){ ctadminDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>

