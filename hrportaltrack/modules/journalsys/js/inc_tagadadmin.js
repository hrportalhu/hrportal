/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */

var tagadadminDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/journalsys/tagadadmin/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"aaSorting": [[ 0, "desc" ]],
				"iDisplayLength": 30,
				"bDestroy": true,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/journalsys/ajax_tagadadmin_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					aoData.push({"name": "name", "value":$("#name").val() });
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });
            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveedittagadadmin(elemid){
	var valid = f_c($('#edittagadadmin'));
	if(valid){
		f_xml(ServerName +'/modules/journalsys/ajax_tagadadmin_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#edittagadadmin').serialize(),'');
	}
}

function savenewtagadadmin(){
	var valid = f_c($('#newtagadadmin'));
	if(valid){
		f_xml(ServerName +'/modules/journalsys/ajax_tagadadmin_data.php','newsubmit=1&' + $('#newtagadadmin').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/journalsys/ajax_tagadadmin_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}

function showanswers(elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/journalsys/ajax_showanswers.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#answers').html(data);
			
		}
	});	
}
function savenewanswerdata(elemid){
	
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/journalsys/ajax_savenewanswerdata.php',
		data:'elemid='+elemid
			+'&newanswer='+$('#newanswer').val()
		,
		success: function(data){
			showanswers(elemid);
		}
	});	
}

function saveeditelemrow(rowid,elemid){
	
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/journalsys/ajax_saveeditelemrow.php',
		data:'rowid='+rowid+'&elemid='+elemid+'&newanswer='+$('#'+rowid+'_answer').val()
		,
		success: function(data){
			showanswers(elemid);
		}
	});	
}
function delanswerdata(rowid,elemid){
	
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/journalsys/ajax_delanswerdata.php',
		data:'rowid='+rowid+'&elemid='+elemid+'&newanswer='+$('#'+rowid+'_answer').val()
		,
		success: function(data){
			showanswers(elemid);
		}
	});	
}
