/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */

var voteadminDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/journalsys/voteadmin/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"aaSorting": [[ 0, "desc" ]],
				"iDisplayLength": 30,
				"bDestroy": true,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/journalsys/ajax_voteadmin_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					aoData.push({"name": "name", "value":$("#name").val() });
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });
            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveeditvoteadmin(elemid){
	var valid = f_c($('#editvoteadmin'));
	if(valid){
		f_xml(ServerName +'/modules/journalsys/ajax_voteadmin_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editvoteadmin').serialize(),'');
	}
}

function savenewvoteadmin(){
	var valid = f_c($('#newvoteadmin'));
	if(valid){
		f_xml(ServerName +'/modules/journalsys/ajax_voteadmin_data.php','newsubmit=1&' + $('#newvoteadmin').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/journalsys/ajax_voteadmin_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}

function showanswers(elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/journalsys/ajax_showanswers.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#answers').html(data);
			
		}
	});	
}
function savenewanswerdata(elemid){
	
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/journalsys/ajax_savenewanswerdata.php',
		data:'elemid='+elemid
			+'&newanswer='+$('#newanswer').val()
		,
		success: function(data){
			showanswers(elemid);
		}
	});	
}

function saveeditelemrow(rowid,elemid){
	
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/journalsys/ajax_saveeditelemrow.php',
		data:'rowid='+rowid+'&elemid='+elemid+'&newanswer='+$('#'+rowid+'_answer').val()
		,
		success: function(data){
			showanswers(elemid);
		}
	});	
}
function delanswerdata(rowid,elemid){
	
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/journalsys/ajax_delanswerdata.php',
		data:'rowid='+rowid+'&elemid='+elemid+'&newanswer='+$('#'+rowid+'_answer').val()
		,
		success: function(data){
			showanswers(elemid);
		}
	});	
}
