
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Címke hírdetés kezelő
            </h1>
        </div>
    </div>
<?
$ceg=db_all("select id, name from partners where status=1 order by name");
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/journalsys/tagadadmin">Címke hírdetés kezelő</a></li>
				<li><a href="/new">Új rendelés</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új rendelés</strong></h2>
				</div>
			<?
			
				
				echo "<form id=\"newtagadadmin\" name=\"newtagadadmin\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";
							
						echo"<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"partner_id\">Megrendelő:</label>
							<div class=\"col-md-8\">
								<select id=\"partner_id\" name=\"partner_id\" class=\"form-control\" placeholder=\"Megrendelő\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($ceg);$i++){
									echo"<option value=\"".$ceg[$i]['id']."\" >".$ceg[$i]['name']."</option>";	
								}
								echo"</select>
							</div>
						</div>";
						
			
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"name\">Kulcsszó:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" placeholder=\"Elnevezés\" />
							</div>
						</div>";
						
				
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"finish\">Publikálás vége:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"finish\" name=\"finish\" class=\"form-control input-datepicker\" placeholder=\"Publikálás vége\" />
							</div>
						</div>";
						
						
									
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"code\">Rendelési gyüjtőkód:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"code\" name=\"code\" class=\"form-control\" placeholder=\"Rendelési gyüjtőkód\" />
							</div>
						</div>";
						
						echo"<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"boxcolor\">Doboz háttérszíne:</label>
							<div class=\"col-md-8\">
								<select id=\"boxcolor\" name=\"boxcolor\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								
								print"<option value=\"5\" >Zöld doboz</option>";
								print"<option value=\"1\" >fehér doboz</option>";
								print"<option value=\"3\" >kék doboz</option>";
								
								echo"</select>
							</div>
						</div>";
						
						
							echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"targeturl\">Partner CT link:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"targeturl\" name=\"targeturl\" class=\"form-control\" placeholder=\"Partner CT link - http és https nélkül\" />
							</div>
						</div>";
						
							echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"adstext\">A kreatív szövege:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"adstext\" name=\"adstext\" class=\"form-control\" placeholder=\"A kreatív szövege\" />
							</div>
						</div>";
					

						echo "<div style=\"clear:both;height:10px;\"></div>";
						
						echo "<div class=\"form-group form-actions\">
							<div class=\"col-md-6\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewtagadadmin();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>";
						
				echo "</div>
				</form>";
			?>
			</div>	
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/journalsys/js/inc_tagadadmin.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?	
		}
		else{

			//echo "select * from functions where id='".$uritags[2]."'";
			$mytagadadmin=db_row("select * from tag_ads where id='".$uritags[3]."'");
			
			if($mytagadadmin['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
			
				$ct1=db_one("select count(id) from tag_ads_counter where ta_id='".$mytagadadmin['id']."' and url!=''");
				$av1=db_one("select count(id) from tag_ads_counter where ta_id='".$mytagadadmin['id']."' and url=''");
				$ct2=db_one("select sum(counterem) from tag_ads_counter_sum where ta_id='".$mytagadadmin['id']."' and type='ct'");
				$av2=db_one("select sum(counterem) from tag_ads_counter_sum where ta_id='".$mytagadadmin['id']."' and type='av'");
				$last=db_one("select visit_date from tag_ads_counter where ta_id='".$mytagadadmin['id']."' order by id desc limit 1");
				$first=db_one("select visit_date from tag_ads_counter where ta_id='".$mytagadadmin['id']."' order by id asc limit 1");
				$ct=$ct1+$ct2;
				$av=$av1+av2;	
				
				

				db_execute("update tag_ads set counter='".$ct."',counter_av='".$av."' where id='".$mytagadadmin['id']."'");
			
				naplo("info","user","Cimke hírdetéskezelő betöltése","journalsys","tagadadmin",$uritags[3],'');	
?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="modules/journalsys/tagadadmin"> Rendelési lista</a></li>
				<li><a href="/<?=$URI;?>"><?=$mytagadadmin['name'];?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->

					<div class="block-title">
						<h2><strong><?=$mytagadadmin['name'];?> </strong></h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
							if(priv("tagadadmin","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("tagadadmin","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							if(priv("tagadadmin","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("tagadadmin","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
							if(priv("tagadadmin","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}	
							?>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="col-md-12">
								<dl class="dl-horizontal">
									<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$mytagadadmin['id']); ?></dd>
									<dt>Megrendelő:</dt><dd><? echo idchange("partners","name",$mytagadadmin['partner_id']); ?></dd>
									<dt>Kulcsszó:</dt><dd><? echo $mytagadadmin['name']; ?></dd>
									<dt>Publikálás kezdete:</dt><dd><? echo $first; ?></dd>
									<dt>Publikálás vége:</dt><dd><? echo $mytagadadmin['finish']; ?></dd>
									<dt>Partner CT Link:</dt><dd><? echo $mytagadadmin['targeturl']; ?></dd>
									<dt>CT:</dt><dd><? echo $ct; ?></dd>
									<dt>AV:</dt><dd><? echo $av; ?></dd>
									<dt>Kreatív</dt><dd>
										<? 

										$belekeret="<div style=\"margin-right:4px;\">[b".$mytagadadmin['boxcolor']."]".$mytagadadmin['body']."[b9]<br />";
										echo borderedparse($belekeret); 
										
										?>
									</dd>
									
									<dt>Rögzítő:</dt><dd><? echo idchange("users","realname",$mytagadadmin['user_id']); ?></dd>
									<dt>Státusz:</dt><dd><? if($mytagadadmin['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
								</dl>	
							</div>

							
							<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-2">
						<?
						
							
							echo"<form id=\"edittagadadmin\" name=\"edittagadadmin\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
							echo "<div style=\"margin-top:1px;\">";
							echo"<div class=\"form-group\">
								<label class=\"col-md-4 control-label\" for=\"partner_id\">Megrendelő:</label>
								<div class=\"col-md-8\">
									<select id=\"partner_id\" name=\"partner_id\" class=\"form-control\" placeholder=\"Megrendelő\" >
									<option value=\"0\">Kérlek válassz</option>";
									for($i=0;$i<count($ceg);$i++){
										$sel="";if($ceg[$i]['id']==$mytagadadmin['partner_id']){$sel=" selected ";}
										echo"<option value=\"".$ceg[$i]['id']."\" ".$sel.">".$ceg[$i]['name']."</option>";	
									}
									echo"</select>
								</div>
							</div>";
								echo "<div class=\"form-group\">
									<label class=\"col-md-4 control-label\" for=\"name\">Kulcsszó:</label>
									<div class=\"col-md-8\">
										<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" value=\"".$mytagadadmin['name']."\" />
									</div>
								</div>";
								

									
									echo "<div class=\"form-group\">
										<label class=\"col-md-4 control-label\" for=\"finish\">Publikálás vége:</label>
										<div class=\"col-md-8\">
											<input type=\"text\" id=\"finish\" name=\"finish\" class=\"form-control input-datepicker\"  value=\"".$mytagadadmin['finish']."\"/>
										</div>
									</div>";
									echo "<div class=\"form-group\">
										<label class=\"col-md-4 control-label\" for=\"code\">Rendelési gyüjtőkód:</label>
										<div class=\"col-md-8\">
											<input type=\"text\" id=\"code\" name=\"code\" class=\"form-control\" value=\"".$mytagadadmin['code']."\" />
										</div>
									</div>";
										echo"<div class=\"form-group\">
										<label class=\"col-md-4 control-label\" for=\"boxcolor\">Doboz háttérszíne:</label>
										<div class=\"col-md-8\">
											<select id=\"boxcolor\" name=\"boxcolor\" class=\"form-control\" >
											<option value=\"0\">Kérlek válassz</option>";
											
											$sel="";if($mytagadadmin['boxcolor']=="5"){$sel=" selected ";}print"<option value=\"5\" ".$sel.">Zöld doboz</option>";
											$sel="";if($mytagadadmin['boxcolor']=="1"){$sel=" selected ";}print"<option value=\"1\" ".$sel.">fehér doboz</option>";
											$sel="";if($mytagadadmin['boxcolor']=="3"){$sel=" selected ";}print"<option value=\"3\" ".$sel.">kék doboz</option>";
											
											echo"</select>
										</div>
									</div>";
								echo "<div class=\"form-group\">
									<label class=\"col-md-4 control-label\" for=\"targeturl\">Partner CT link:</label>
									<div class=\"col-md-8\">
										<input type=\"text\" id=\"targeturl\" name=\"targeturl\" class=\"form-control\" value=\"".$mytagadadmin['targeturl']."\" />
									</div>
								</div>";
								
							echo "<div class=\"form-group\">
								<label class=\"col-md-4 control-label\" for=\"adstext\">A kreatív szövege:</label>
								<div class=\"col-md-8\">
									<input type=\"text\" id=\"adstext\" name=\"adstext\" class=\"form-control\" value=\"".$mytagadadmin['adstext']."\" />
								</div>
							</div>";
									
								echo "<div class=\"form-group\" >
									<label class=\"col-md-4 control-label\" for=\"status\">Érvényes-e:</label>";
									$st="";if($mytagadadmin['status']==1){ $st=" checked "; }
									echo "<label class=\"col-md-8 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\"".$st." ><span></span></label>
								</div>";
								
								echo "<div class=\"form-group form-actions\">
									<div class=\"col-md-6 \">
										<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveedittagadadmin(".$mytagadadmin['id'].")\"><i class=\"fa fa-angle-right\"></i> Ment</button>
									</div>
								</div>";
									
							echo "</div>
							</form>";
						?>
						</div>

						<div class="tab-pane" id="tab-3">
								<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mytagadadmin['id'],"Dokumentum feltöltés")."</div>";
								?> 
								<div id="filelists"></div>
								<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mytagadadmin['id'],"Hozzászólások")."</div>";
								?>
						</div>
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>
						
						
					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
					
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/journalsys/js/inc_tagadadmin.js"></script>

			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mytagadadmin['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$mytagadadmin['id'];?>,'Dokumentum feltöltés','filelists');
				
			</script>
			<script>$(function(){ tagadadminDatatables.init(); });</script>	

			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
		//print_r($_SESSION['planetsys'])
?>    
<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Rendelések</a></li>
			</ul>
			<!-- END Datatables Header -->
		
				
				
			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Rendelési</strong> lista</h2>
				</div>
				
				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							//echo"<th>Művelet</th>";
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/journalsys/js/inc_tagadadmin.js"></script>
		<script>$(function(){ tagadadminDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>

