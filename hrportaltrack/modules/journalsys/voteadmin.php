
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Szavazások
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/journalsys/voteadmin">Szavazás kezelő</a></li>
				<li><a href="/new">Új szavazás</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új szavazás</strong></h2>
				</div>
			<?
			
				
				echo "<form id=\"newvoteadmin\" name=\"newvoteadmin\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";
							
						
			
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"name\">Kérdés:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" placeholder=\"Dokumentum elnevezése\" />
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"startdate\">Publikálás kezdete:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"startdate\" name=\"startdate\" class=\"form-control input-datepicker\" placeholder=\"Publikálás kezdete\" />
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"enddate\">Publikálás vége:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"enddate\" name=\"enddate\" class=\"form-control input-datepicker\" placeholder=\"Publikálás vége\" />
							</div>
						</div>";
	
						echo "<div class=\"form-group\" >
							<label class=\"col-md-4 control-label\" for=\"sidebar\">Oldalsávban megjelenik?:</label>
							<label class=\"col-md-8 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"sidebar\" checked><span></span></label>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"sidestartdate\">Oldalsáv kezdete:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"sidestartdate\" name=\"sidestartdate\" class=\"form-control input-datepicker\" placeholder=\"Oldalsáv kezdete\" />
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"sideenddate\">Oldalsáv vége:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"sideenddate\" name=\"sideenddate\" class=\"form-control input-datepicker\" placeholder=\"Oldalsáv vége\" />
							</div>
						</div>";
								

						echo "<div style=\"clear:both;height:10px;\"></div>";
						
						echo "<div class=\"form-group form-actions\">
							<div class=\"col-md-6\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewvoteadmin();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>";
						
				echo "</div>
				</form>";
			?>
			</div>	
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/journalsys/js/inc_voteadmin.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?	
		}
		else{

			//echo "select * from functions where id='".$uritags[2]."'";
			$myvoteadmin=db_row("select * from voteadmin where id='".$uritags[3]."'");
			
			if($myvoteadmin['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
			naplo("info","user","Szavazás betöltése","journalsys","voteadmin",$uritags[3],'');	
?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="modules/journalsys/voteadmin"> Szavazás</a></li>
				<li><a href="/<?=$URI;?>"><?=$myvoteadmin['name'];?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->

					<div class="block-title">
						<h2><strong><?=$myvoteadmin['name'];?> </strong></h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
							if(priv("voteadmin","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("voteadmin","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							if(priv("voteadmin","options")){echo"<li><a href=\"#tab-6\" data-toggle=\"tooltip\" title=\"válaszok\"><i class=\"fa fa-list\"></i></a></li>";}
							if(priv("voteadmin","print")){echo"<li><a href=\"#tab-99\" data-toggle=\"tooltip\" title=\"Történet\" onclick=\"javascript:showstory('".$myvoteadmin['id']."');\"><i class=\"gi gi-history\"></i></a></li>";}			
							if(priv("voteadmin","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("voteadmin","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
							if(priv("voteadmin","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}	
							?>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="col-md-12">
								<dl class="dl-horizontal">
									<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$myvoteadmin['id']); ?></dd>
									<dt>Kérdés:</dt><dd><? echo $myvoteadmin['name']; ?></dd>
									<dt>Publikálás kezdete:</dt><dd><? echo $myvoteadmin['startdate']; ?></dd>
									<dt>Publikálás vége:</dt><dd><? echo $myvoteadmin['enddate']; ?></dd>
									<dt>Oldalsávban megjelenik?:</dt><dd><? if($myvoteadmin['sidebar']==1){echo"igen";}else{echo"nem";} ?></dd>
									<dt>Oldal megjelenés kezdete:</dt><dd><? echo $myvoteadmin['sidestartdate']; ?></dd>
									<dt>Oldal megjelenés vége:</dt><dd><? echo $myvoteadmin['sideenddate']; ?></dd>
									<dt>Rögzítő:</dt><dd><? echo idchange("users","realname",$myvoteadmin['user_id']); ?></dd>
									<dt>Rögzítés ideje:</dt><dd><? echo $myvoteadmin['rdate']; ?></dd>

									<dt>Státusz:</dt><dd><? if($myvoteadmin['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
								</dl>	
							</div>
							<div class="col-md-12">
									<div class="block hidden-lt-ie9">
										<div class="block-title">
											<h2><strong>Megjelenítve</strong> </h2>
										</div>
										<div >
									<div style="clear:both;height:1px;"></div>
									<iframe src="http://admin2.hrportal.hu/hrvote.php?width=300&voteid=<?=$myvoteadmin['id'];?>" style="width:300px;height:500px;border:0;overflow:hidden;" scrolling="no"></iframe>	
									<div style="clear:both;height:1px;"></div>
										</div>
									</div>
									
							</div>
							<div class="col-md-12">
								<div class="block hidden-lt-ie9">
									<div class="block-title">
										<h2><strong>Beágyazási kód</strong> </h2>
									</div>
									<div >
									<?
										$str="<div style=\"clear:both;height:1px;\"></div><iframe src=\"http://admin2.hrportal.hu/hrvote.php?width=300&voteid=".$myvoteadmin['id']."\" style=\"width:300px;height:500px;border:0;overflow:hidden;\" scrolling=\"no\"></iframe><div style=\"clear:both;height:1px;\"></div>";
										echo highlight_string($str);		
									?>
									</div>
								</div>
							</div>
							
							<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-2">
						<?
						
							
							echo"<form id=\"editvoteadmin\" name=\"editvoteadmin\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
							echo "<div style=\"margin-top:1px;\">";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-4 control-label\" for=\"name\">Kérdés:</label>
									<div class=\"col-md-8\">
										<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" value=\"".$myvoteadmin['name']."\" />
									</div>
								</div>";
								
									echo "<div class=\"form-group\">
										<label class=\"col-md-4 control-label\" for=\"startdate\">Publikálás kezdete:</label>
										<div class=\"col-md-8\">
											<input type=\"text\" id=\"startdate\" name=\"startdate\" class=\"form-control input-datepicker\"  value=\"".$myvoteadmin['startdate']."\"/>
										</div>
									</div>";
									
									echo "<div class=\"form-group\">
										<label class=\"col-md-4 control-label\" for=\"enddate\">Publikálás vége:</label>
										<div class=\"col-md-8\">
											<input type=\"text\" id=\"enddate\" name=\"enddate\" class=\"form-control input-datepicker\"  value=\"".$myvoteadmin['enddate']."\"/>
										</div>
									</div>";
									$st2="";if($myvoteadmin['sidebar']==1){ $st2=" checked "; }
									echo "<div class=\"form-group\" >
										<label class=\"col-md-4 control-label\" for=\"sidebar\">Oldalsávban megjelenik?:</label>
										<label class=\"col-md-8 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"sidebar\"".$st2."><span></span></label>
									</div>";
									
									echo "<div class=\"form-group\">
										<label class=\"col-md-4 control-label\" for=\"sidestartdate\">Oldalsáv kezdete:</label>
										<div class=\"col-md-8\">
											<input type=\"text\" id=\"sidestartdate\" name=\"sidestartdate\" class=\"form-control input-datepicker\"  value=\"".$myvoteadmin['sidestartdate']."\"/>
										</div>
									</div>";
									
									echo "<div class=\"form-group\">
										<label class=\"col-md-4 control-label\" for=\"sideenddate\">Oldalsáv vége:</label>
										<div class=\"col-md-8\">
											<input type=\"text\" id=\"sideenddate\" name=\"sideenddate\" class=\"form-control input-datepicker\"  value=\"".$myvoteadmin['sideenddate']."\"/>
										</div>
									</div>";
						
								echo "<div class=\"form-group\" >
									<label class=\"col-md-4 control-label\" for=\"status\">Érvényes-e:</label>";
									$st="";if($myvoteadmin['status']==1){ $st=" checked "; }
									echo "<label class=\"col-md-8 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\"".$st." ><span></span></label>
								</div>";
								
								echo "<div class=\"form-group form-actions\">
									<div class=\"col-md-6 \">
										<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditvoteadmin(".$myvoteadmin['id'].")\"><i class=\"fa fa-angle-right\"></i> Ment</button>
									</div>
								</div>";
									
							echo "</div>
							</form>";
						?>
						</div>

						<div class="tab-pane" id="tab-3">
								<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$myvoteadmin['id'],"Dokumentum feltöltés")."</div>";
								?> 
								<div id="filelists"></div>
								<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$myvoteadmin['id'],"Hozzászólások")."</div>";
								?>
						</div>
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>
						
							<div class="tab-pane" id="tab-6">
								
													
								<div class="table-responsive">
									<table class="table table-vcenter table-striped">
										<thead>
											<tr>
												<th style="width: 15%;" class="text-center"><i class="gi gi-disk_save"></i></th>
												<th style="width: 50%;">Válasz</th>
												<th style="width: 15%;">Válaszok száma</th>
												<th style="width: 15%;">Státusz</th>
												
											</tr>
										</thead>
										<tbody  id="answers"></tbody>
									</table>		
								</div>
							</div>
						
						
						<div class="tab-pane" id="tab-99">
							<div id="storydiv"></div>
						</div>
					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
					
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/journalsys/js/inc_voteadmin.js"></script>

			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$myvoteadmin['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$myvoteadmin['id'];?>,'Dokumentum feltöltés','filelists');
				showanswers('<?=$myvoteadmin['id'];?>');
			</script>
			<script>$(function(){ voteadminDatatables.init(); });</script>	

			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
		//print_r($_SESSION['planetsys'])
?>    
<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Szavazások</a></li>
			</ul>
			<!-- END Datatables Header -->
		
				
				
			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Szavazás</strong> lista</h2>
				</div>
				
				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							//echo"<th>Művelet</th>";
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/journalsys/js/inc_voteadmin.js"></script>
		<script>$(function(){ voteadminDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>

