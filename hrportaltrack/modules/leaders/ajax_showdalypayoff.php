<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors','Off');
header("Cache-control: private");
header('Content-Type: text/plain;charset=utf-8');
include_once dirname(__FILE__).("/../../inc/sys.conf");
include_once dirname(__FILE__).("/../../inc/_functions.php");
$scrch=0;
if($_REQUEST['matstdate']==""){
	$checkdate=db_one("select max(szall_date) from orders_elements limit 1");
	$tt=db_one("select max(szall_date) from orders_elements limit 1");
	$cq=" and szall_date='".$checkdate."' ";
	$wq=" and workday='".$checkdate."' ";
	$aq=" and name='".$checkdate."' ";
}
else{
	if($_REQUEST['matendate']==""){
		$checkdate=$_REQUEST['matstdate'];
		$tt=$_REQUEST['matstdate'];
		$cq=" and szall_date='".$checkdate."' ";
		$wq=" and workday='".$checkdate."' ";
		$aq=" and name='".$checkdate."' ";
	}	
	else{
		$checkdate=$_REQUEST['matstdate'];
		$tt=$_REQUEST['matstdate']." - ".$_REQUEST['matendate'];
		$cq=" and szall_date between '".$_REQUEST['matstdate']."' and '".$_REQUEST['matendate']."'";
		$wq=" and workday between '".$_REQUEST['matstdate']."' and '".$_REQUEST['matendate']."'";
		$aq=" and name between '".$_REQUEST['matstdate']."' and '".$_REQUEST['matendate']."'";

	}
}
$pq=" and post_id!=0 ";
if($_REQUEST['post_id']!=0){
	$pq=" and post_id='".$_REQUEST['post_id']."' ";
	$scrch=1;
}



echo"<div class=\"block-title\">";
	echo"<h2><strong>".$tt."</strong> </h2>";
echo"</div>";

if($scrch==0){
	if($_REQUEST['qty']==1){
		echo"<div class=\"col-sm-6\" ><table class=\"table table-vcenter table-striped\">";
			echo"<thead>";
				echo"<tr>";
					echo"<th class=\"text-center\">Poszt</th>";
					echo"<th class=\"text-center\">Termelt érték</th>";
					echo"<th class=\"text-center\">Keletkezett munkabér</th>";
					echo"<th class=\"text-center\">Munkabér százalék</th>";
				echo"</tr>";
			echo"</thead>";
			echo"<tbody >";
			//$posts=db_all("select post_id from orders_elements where id>0 ".$cq."  ".$pq." group by post_id");
			$posts=db_all("select * from gw_posts where  status=1 order by name");
			$sue=0;
			$sup=0;
			for($i=0;$i<count($posts);$i++){
				$sumertek=db_one("select sum(price_all) from orders_elements where id>0 ".$cq." and post_id='".$posts[$i]['id']."' ");
				$sumpenz=db_one("select sum(dailycash) from gw_shift_x_worker where id>0 ".$wq." and post_id='".$posts[$i]['id']."' ");
				//$fajlagos=
				echo"<tr>";
					echo"<td>".$posts[$i]['name']."</td>";
					echo"<td class=\"text-right\">".huf($sumertek)." Ft</td>";
					echo"<td class=\"text-right\">".huf($sumpenz)." Ft</td>";
					echo"<td class=\"text-right\">".round($sumpenz*100/$sumertek,2)." %</td>";	
				echo"</tr>";
				$sue=$sue+$sumertek;
				$sup=$sup+$sumpenz;
			}
			echo"<tr>";
				echo"<td>Összesen</td>";
				echo"<td class=\"text-right\">".huf($sue)." Ft</td>";
				echo"<td class=\"text-right\">".huf($sup)." Ft</td>";
				echo"<td class=\"text-right\">".round($sup*100/$sue,2)." %</td>";	
			echo"</tr>";
			echo"</tbody>";
		echo"</table></div>";	
		echo"<div class=\"col-sm-6\" >
				<div class=\"block full\" style=\"overflow:auto;\">
					<div id=\"chartom\" class=\"chart\" ></div>
				</div>
			</div>
		<div style=\"clear:both;\"></div>	
		";
	}
	elseif($_REQUEST['qty']==2){
		echo"<div class=\"col-sm-12\" ><table class=\"table table-vcenter table-striped\">";
			echo"<thead>";
				echo"<tr>";
					echo"<th class=\"text-center\">Poszt</th>";
					echo"<th class=\"text-center\">Termelt darab</th>";
					echo"<th class=\"text-center\">Termelt nettó érték</th>";
					echo"<th class=\"text-center\">Áfa érték</th>";
					echo"<th class=\"text-center\">Termelt bruttó érték</th>";
					echo"<th class=\"text-center\">Munkabér Összesen</th>";
					echo"<th class=\"text-center\">Munkabér százalék nettó</th>";
					echo"<th class=\"text-center\">Munkabér százalék bruttó</th>";
				echo"</tr>";
			echo"</thead>";
			echo"<tbody >";
			$posts=db_all("select post_id from orders_elements where id>0 ".$cq."  ".$pq." group by post_id");
			
			$sue=0;
			$sup=0;
			$sudb=0;
			$suaf=0;
			$subr=0;
			$mposts=array();
			for($i=0;$i<count($posts);$i++){
				$mposts[]=$posts[$i]['post_id'];
				$sumertek=db_one("select sum(price_all) from orders_elements where id>0 ".$cq." and post_id='".$posts[$i]['post_id']."' ");
				$sumafa=db_one("select sum(price_afa) from orders_elements where id>0 ".$cq." and post_id='".$posts[$i]['post_id']."' ");
				$sumbrutto=db_one("select sum(price_brutto) from orders_elements where id>0 ".$cq." and post_id='".$posts[$i]['post_id']."' ");
				$sumtermek=db_one("select sum(gyartas_darab) from orders_elements where id>0 ".$cq." and post_id='".$posts[$i]['post_id']."' ");
				$sumpenz=db_one("select sum(dailycash) from gw_shift_x_worker where id>0 ".$wq." and post_id='".$posts[$i]['post_id']."' ");
				
				$oth1=db_all("select * from gw_posts where id!='".$posts[$i]['post_id']."' and p1='".$posts[$i]['post_id']."'");
				$fiza=0;
				for($z=0;$z<count($oth1);$z++){
					$mposts[]=$oth1[$z]['id'];
					$sumpe=db_one("select sum(dailycash) from gw_shift_x_worker where id>0 ".$wq." and post_id='".$oth1[$z]['id']."' ");
					$fiza=$fiza+($sumpe*($oth1[$z]['bev1']/100));
				}	

				echo"<tr>";
				$oth2=db_all("select * from gw_posts where id!='".$posts[$i]['post_id']."' and p2='".$posts[$i]['post_id']."'");
				$fiza2=0;
				for($z=0;$z<count($oth2);$z++){
					$mposts[]=$oth2[$z]['id'];
					$sumpe=db_one("select sum(dailycash) from gw_shift_x_worker where id>0 ".$wq." and post_id='".$oth2[$z]['id']."' ");
					$fiza2=$fiza2+($sumpe*($oth2[$z]['bev2']/100));
				}	
				
				$oth3=db_all("select * from gw_posts where id!='".$posts[$i]['post_id']."' and p3='".$posts[$i]['post_id']."'");
				$fiza3=0;
				for($z=0;$z<count($oth3);$z++){
					$mposts[]=$oth3[$z]['id'];
					$sumpe=db_one("select sum(dailycash) from gw_shift_x_worker where id>0 ".$wq." and post_id='".$oth3[$z]['id']."' ");
					$fiza3=$fiza3+($sumpe*($oth3[$z]['bev3']/100));
				}	
				$osszesp=round($sumpenz+$fiza+$fiza2+$fiza3);
				
				echo"<tr>";
					echo"<td>".idchange("gw_posts","name",$posts[$i]['post_id'])."</td>";
					echo"<td class=\"text-right\">".round($sumtermek)." db</td>";
					echo"<td class=\"text-right\">".huf($sumertek)." Ft</td>";
					echo"<td class=\"text-right\">".huf(round($sumafa))." Ft</td>";
					echo"<td class=\"text-right\">".huf(round($sumbrutto))." Ft</td>";
					echo"<td class=\"text-right\">D: ".huf($sumpenz)." Ft<br />S: ".huf(round($fiza+$fiza2+$fiza3))." Ft<br />Ö: ".huf($osszesp)." Ft</td>";
					echo"<td class=\"text-right\">".round($osszesp*100/$sumertek,2)." %</td>";	
					echo"<td class=\"text-right\">".round($osszesp*100/$sumbrutto,2)." %</td>";	
				echo"</tr>";
				$sue=$sue+$sumertek;
				$sup=$sup+$osszesp;
				$sudb=$sudb+$sumtermek;
				$suaf=$suaf+$sumafa;
				$subr=$subr+$sumbrutto;
			}
			$posta=db_all("select * from gw_posts where status=1 and id!='9' and id!='10' and id!='11' and id!='13'");
			$plusszos=0;
			$txts="";
			for($l=0;$l<count($posta);$l++){
				if(!in_array($posta[$l]['id'],$mposts)){
					$sumpenz=db_one("select sum(dailycash) from gw_shift_x_worker where id>0 ".$wq." and post_id='".$posta[$l]['id']."' ");
					if($sumpenz!=0){
						$txts.=idchange("gw_posts","name",$posta[$l]['id']).", ";
						//echo "select sum(dailycash) from gw_shift_x_worker where id>0 ".$wq." and post_id='".$posta[$l]['id']."' <br />";
						$plusszos+=$sumpenz;
					}
				}
				//$sumpenz=db_one("select sum(dailycash) from gw_shift_x_worker where id>0 ".$wq." and post_id='".$posts[$i]['post_id']."' ");
			}
			
			echo"<tr>";
					echo"<td colspan=\"5\">Egyéb adott napon nem értékesítő:<br />".$txts."</td>";

					echo"<td class=\"text-right\">D: ".huf($plusszos)." Ft</td>";
					echo"<td class=\"text-right\">&nbsp;</td>";	
					echo"<td class=\"text-right\">&nbsp;</td>";	
				echo"</tr>";
			echo"<tr>";
			$sup+=$plusszos;
			
			$sumtakpenz=db_one("select sum(dailycash) from gw_shift_x_worker where id>0 ".$wq." and post_id='9' ");
			echo"<tr>";
					echo"<td>Takarítás</td>";
					echo"<td class=\"text-right\">&nbsp;</td>";
					echo"<td class=\"text-right\">&nbsp;</td>";
					echo"<td class=\"text-right\">&nbsp;</td>";
					echo"<td class=\"text-right\">&nbsp;</td>";
					echo"<td class=\"text-right\">D: ".huf($sumtakpenz)." Ft</td>";
					echo"<td class=\"text-right\">&nbsp;</td>";	
					echo"<td class=\"text-right\">&nbsp;</td>";	
				echo"</tr>";
			echo"<tr>";
			$sup+=$sumtakpenz;
			
			$b=db_all("select * from gw_workers where status=1 and monthlypay=1");
			$sumirodapenz=0;
			for($u=0;$u<count($b);$u++){
				$fizu=db_one("select price from gw_worker_payments where worker_id='".$b[$u]['id']."' and price_type='monthly' and status=1");
				$sumirodapenz+=($fizu/24);
			}
				$snapok=db_one("select count(id) from gw_workdays where id>0 ".$aq."  ");
			echo"<tr>";
					echo"<td>Iroda, karbantartás</td>";
					echo"<td class=\"text-right\">&nbsp;</td>";
					echo"<td class=\"text-right\">&nbsp;</td>";
					echo"<td class=\"text-right\">&nbsp;</td>";
					echo"<td class=\"text-right\">&nbsp;</td>";
					echo"<td class=\"text-right\">D: ".huf($sumirodapenz*$snapok)." Ft</td>";
					echo"<td class=\"text-right\">&nbsp;</td>";	
					echo"<td class=\"text-right\">&nbsp;</td>";	
				echo"</tr>";
			echo"<tr>";
			$sup+=($sumirodapenz*$snapok);
				echo"<td>Összesen</td>";
				echo"<td class=\"text-right\">".huf(round($sudb))." db</td>";
				echo"<td class=\"text-right\">".huf($sue)." Ft</td>";
				echo"<td class=\"text-right\">".huf(round($suaf))." Ft</td>";
				echo"<td class=\"text-right\">".huf(round($subr))." Ft</td>";
				echo"<td class=\"text-right\">".huf($sup)." Ft</td>";
				echo"<td class=\"text-right\">".round($sup*100/$sue,2)." %</td>";	
				echo"<td class=\"text-right\">".round($sup*100/$subr,2)." %</td>";	
			echo"</tr>";
			echo"</tbody>";
		echo"</table></div>";	
		echo"<div class=\"col-sm-12\" >
				<div class=\"block full\" style=\"overflow:auto;\">
					<div id=\"chartom\" class=\"chart\" ></div>
				</div>
			</div>
		<div style=\"clear:both;\"></div>	
		";
	}
	elseif($_REQUEST['qty']==3){
				echo"<div class=\"col-sm-12\" ><table class=\"table table-vcenter table-striped\">";
			echo"<thead>";
				echo"<tr>";
					echo"<th class=\"text-center\">Poszt</th>";
					echo"<th class=\"text-center\">Termelt darab</th>";
					echo"<th class=\"text-center\">Termelt nettó érték</th>";
					echo"<th class=\"text-center\">Áfa érték</th>";
					echo"<th class=\"text-center\">Termelt bruttó érték</th>";
					echo"<th class=\"text-center\">Munkabér Összesen</th>";
					echo"<th class=\"text-center\">Munkabér százalék nettó</th>";
					echo"<th class=\"text-center\">Munkabér százalék bruttó</th>";
				echo"</tr>";
			echo"</thead>";
			echo"<tbody >";
			
			$mnapok=db_all("select * from gw_workdays where id>0 ".$aq."  ");
			
			for($q=0;$q<count($mnapok);$q++){
				$sup=0;
				$cq=" and szall_date between '".$mnapok[$q]['name']."' and '".$mnapok[$q]['name']."'";
				$wq=" and workday = '".$mnapok[$q]['name']."' ";
		
			
				$posts=db_all("select post_id from orders_elements where id>0 ".$cq."  ".$pq." group by post_id");
				
				$sue=0;
				$sup=0;
				$sudb=0;
				$suaf=0;
				$subr=0;
				$mposts=array();
				for($i=0;$i<count($posts);$i++){
					$mposts[]=$posts[$i]['post_id'];
					$sumertek=db_one("select sum(price_all) from orders_elements where id>0 ".$cq." and post_id='".$posts[$i]['post_id']."' ");
					$sumafa=db_one("select sum(price_afa) from orders_elements where id>0 ".$cq." and post_id='".$posts[$i]['post_id']."' ");
					$sumbrutto=db_one("select sum(price_brutto) from orders_elements where id>0 ".$cq." and post_id='".$posts[$i]['post_id']."' ");
					$sumtermek=db_one("select sum(gyartas_darab) from orders_elements where id>0 ".$cq." and post_id='".$posts[$i]['post_id']."' ");
					$sumpenz=db_one("select sum(dailycash) from gw_shift_x_worker where id>0 ".$wq." and post_id='".$posts[$i]['post_id']."' ");
					
					$oth1=db_all("select * from gw_posts where id!='".$posts[$i]['post_id']."' and p1='".$posts[$i]['post_id']."'");
					$fiza=0;
					for($z=0;$z<count($oth1);$z++){
						$mposts[]=$oth1[$z]['id'];
						$sumpe=db_one("select sum(dailycash) from gw_shift_x_worker where id>0 ".$wq." and post_id='".$oth1[$z]['id']."' ");
						$fiza=$fiza+($sumpe*($oth1[$z]['bev1']/100));
					}	

					echo"<tr>";
					$oth2=db_all("select * from gw_posts where id!='".$posts[$i]['post_id']."' and p2='".$posts[$i]['post_id']."'");
					$fiza2=0;
					for($z=0;$z<count($oth2);$z++){
						$mposts[]=$oth2[$z]['id'];
						$sumpe=db_one("select sum(dailycash) from gw_shift_x_worker where id>0 ".$wq." and post_id='".$oth2[$z]['id']."' ");
						$fiza2=$fiza2+($sumpe*($oth2[$z]['bev2']/100));
					}	
					
					$oth3=db_all("select * from gw_posts where id!='".$posts[$i]['post_id']."' and p3='".$posts[$i]['post_id']."'");
					$fiza3=0;
					for($z=0;$z<count($oth3);$z++){
						$mposts[]=$oth3[$z]['id'];
						$sumpe=db_one("select sum(dailycash) from gw_shift_x_worker where id>0 ".$wq." and post_id='".$oth3[$z]['id']."' ");
						$fiza3=$fiza3+($sumpe*($oth3[$z]['bev3']/100));
					}	
					$osszesp=round($sumpenz+$fiza+$fiza2+$fiza3);
					$sue=$sue+$sumertek;
					$sup=$sup+$osszesp;
					$sudb=$sudb+$sumtermek;
					$suaf=$suaf+$sumafa;
					$subr=$subr+$sumbrutto;
				}
				$posta=db_all("select * from gw_posts where status=1 and id!='9' and id!='10' and id!='11' and id!='13'");
				$plusszos=0;
				$txts="";
				for($l=0;$l<count($posta);$l++){
					if(!in_array($posta[$l]['id'],$mposts)){
						$sumpenz=db_one("select sum(dailycash) from gw_shift_x_worker where id>0 ".$wq." and post_id='".$posta[$l]['id']."' ");
						if($sumpenz!=0){
							$txts.=idchange("gw_posts","name",$posta[$l]['id']).", ";
							$plusszos+=$sumpenz;
						}
					}
				}
				$sup+=$plusszos;
				$sumtakpenz=db_one("select sum(dailycash) from gw_shift_x_worker where id>0 ".$wq." and post_id='9' ");
				$sup+=$sumtakpenz;

				$b=db_all("select * from gw_workers where status=1 and monthlypay=1");
				$sumirodapenz=0;
				for($u=0;$u<count($b);$u++){
					$fizu=db_one("select price from gw_worker_payments where worker_id='".$b[$u]['id']."' and price_type='monthly' and status=1");
					$sumirodapenz+=($fizu/24);
				}
				//$snapok=db_one("select count(id) from gw_workdays where id>0 ".$aq."  ");
				$sup+=$sumirodapenz;
					echo"<td>".$mnapok[$q]['name']."</td>";
					echo"<td class=\"text-right\">".huf(round($sudb))." db</td>";
					echo"<td class=\"text-right\">".huf($sue)." Ft</td>";
					echo"<td class=\"text-right\">".huf(round($suaf))." Ft</td>";
					echo"<td class=\"text-right\">".huf(round($subr))." Ft</td>";
					echo"<td class=\"text-right\">".huf($sup)." Ft</td>";
					echo"<td class=\"text-right\">".round($sup*100/$sue,2)." %</td>";	
					echo"<td class=\"text-right\">".round($sup*100/$subr,2)." %</td>";	
				echo"</tr>";
			
		}	
			echo"</tbody>";
		echo"</table></div>";	
		
		echo"<div class=\"col-sm-12\" >
				<div class=\"block full\" style=\"overflow:auto;\">
					<div id=\"chartom\" class=\"chart\" ></div>
				</div>
			</div>
		<div style=\"clear:both;\"></div>	
		";
	}

}
else{
	echo"<table class=\"table table-vcenter table-striped\">";
		echo"<thead>";
			echo"<tr>";
				echo"<th class=\"text-center\">Dátum</th>";
				echo"<th class=\"text-center\">Termelt érték</th>";
				echo"<th class=\"text-center\">Keletkezett munkabér</th>";
				echo"<th class=\"text-center\">Munkabér százalék</th>";
			echo"</tr>";
		echo"</thead>";
		echo"<tbody >";
		$posts=db_all("select szall_date from orders_elements where id>0 ".$cq."  ".$pq." group by szall_date");
		$sue=0;
		$sup=0;
		for($i=0;$i<count($posts);$i++){
			$sumertek=db_one("select sum(price_all) from orders_elements where id>0 ".$pq." and szall_date='".$posts[$i]['szall_date']."' ");
			$sumpenz=db_one("select sum(dailycash) from gw_shift_x_worker where id>0 ".$pq." and workday='".$posts[$i]['szall_date']."' ");
			//$fajlagos=
			echo"<tr>";
				echo"<td>".$posts[$i]['szall_date']."</td>";
				echo"<td class=\"text-right\">".huf($sumertek)." Ft</td>";
				echo"<td class=\"text-right\">".huf($sumpenz)." Ft</td>";
				echo"<td class=\"text-right\">".round($sumpenz*100/$sumertek,2)." %</td>";	
			echo"</tr>";
			$sue=$sue+$sumertek;
			$sup=$sup+$sumpenz;
		}
		echo"<tr>";
			echo"<td>Összesen</td>";
			echo"<td class=\"text-right\">".huf($sue)." Ft</td>";
			echo"<td class=\"text-right\">".huf($sup)." Ft</td>";
			echo"<td class=\"text-right\">".round($sup*100/$sue,2)." %</td>";	
		echo"</tr>";
		echo"</tbody>";
	echo"</table>";
	echo"<div class=\"row\" style=\"overflow:auto;\">
		<div class=\"col-sm-12\" style=\"overflow:auto;\">
			<div class=\"block full\" style=\"overflow:auto;\">
				<div class=\"block-title\">
					<h2 id=\"charttit\"><strong>".$tt."</strong></h2>
				</div>
				<div id=\"chartom\" class=\"chart\" ></div>
			</div>
		</div>
	</div>";
		
}




mysql_close($connid);
?>
