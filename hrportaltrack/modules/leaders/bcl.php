
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>BCL
            </h1>
        </div>
    </div>
<?
				$koltseghely=db_all("select * from cost_category where status=1 order by name asc");
				$workers=db_all("select * from gw_workers where status=1 order by name asc");

	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/leaders/bcl">BCL</a></li>
				<li><a href="/new">Új BCLe</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új BCLe</strong></h2>
				</div>
			<?
				$bclname=date("Ymd",time())."-".generateCode(6);
				
				echo "<form id=\"newbcl\" name=\"newbcl\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";
			
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"name\">BCLe neve:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" value=\"".$bclname."\" />
							</div>
						</div>";					
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"category_id\">Költséghely:</label>
							<div class=\"col-md-8\">
								<select id=\"category_id\" name=\"category_id\" class=\"form-control\" onchange=\"setjogcim();\">
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($koltseghely);$i++){
									echo"<option value=\"".$koltseghely[$i]['id']."\" >".$koltseghely[$i]['name']."</option>";	
								}
								echo"</select>
							</div>
						</div>";					
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"worker_id\">Dolgozó:</label>
							<div class=\"col-md-8\">
								<select id=\"worker_id\" name=\"worker_id\" class=\"form-control\" onchange=\"setperson();\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($workers);$i++){
									echo"<option value=\"".$workers[$i]['id']."\" >".$workers[$i]['name']."</option>";	
								}
								echo"</select>
							</div>
						</div>";					
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"otherperson\">Átevő személy:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"otherperson\" name=\"otherperson\" class=\"form-control\" value=\"\" />
							</div>
						</div>";	
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"costtext\">Jogcím:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"costtext\" name=\"costtext\" class=\"form-control\"  />
							</div>
						</div>";	
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"cost\">összeg:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"cost\" name=\"cost\" class=\"form-control\"  />
							</div>
						</div>";	
						
						echo"<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"kiad_date\">Kiadás ideje:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"kiad_date\" name=\"kiad_date\" class=\"form-control input-datepicker\" value=\"".date("Y-m-d",time())."\" />
							</div>
						</div>";
						
						echo "<div style=\"clear:both;height:10px;\"></div>";
						
						echo "<div class=\"form-group form-actions\">
							<div class=\"col-md-6 \">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewbcl();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>";
						
				echo "</div>
				</form>";
			?>
			</div>	
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/leaders/js/inc_bcl.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?	
		}
		else{

			//echo "select * from functions where id='".$uritags[2]."'";
			$mybcl=db_row("select * from cost_bcl where id='".$uritags[3]."'");
			if($mybcl['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
				
?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="modules/leaders/bcl"> BCLe</a></li>
				<li><a href="/<?=$URI;?>"><?=$mybcl['name'];?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->

					<div class="block-title">
						<h2><strong><?=$mybcl['name'];?> </strong></h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
							if(priv("bcl","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("bcl","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							if(priv("bcl","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("bcl","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
							if(priv("bcl","print")){echo"<li onclick=\"javascript:getprintscreen('".$_SESSION['planetsys']['actpage']['modulname']."','bclprint','".$mybcl['id']."');\" ><a href=\"#modal-regular-print\" data-toggle=\"modal\" title=\"Bizonylat nyomtatása\"><i class=\"fa fa-print\" ></i></a></li>";}	
													
							?>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="col-md-6">
								<dl class="dl-horizontal">
									<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$mybcl['id']); ?></dd>
									<dt>Kiadási bizonylat száma:</dt><dd><? echo $mybcl['name']; ?></dd>
									<dt>Költséghely:</dt><dd><? echo idchange("cost_category","name",$mybcl['category_id']); ?></dd>
									<dt>Átevő személy:</dt><dd><? echo $mybcl['otherperson']; ?></dd>
									<dt>Jogcím:</dt><dd><? echo $mybcl['costtext']; ?></dd>
									<dt>Összeg:</dt><dd><? echo $mybcl['cost']; ?></dd>
									<dt>Dátum:</dt><dd><? echo $mybcl['name']; ?></dd>

								</dl>	
							</div>
							<div class="col-md-6"></div>
							<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-2">
						<?
						
							
							echo"<form id=\"editbcl\" name=\"editbcl\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
							echo "<div style=\"margin-top:1px;\">";
								
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"name\">BCLe neve:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" value=\"".$mybcl['name']."\" />
							</div>
						</div>";					
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"category_id\">Költséghely:</label>
							<div class=\"col-md-8\">
								<select id=\"category_id\" name=\"category_id\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($koltseghely);$i++){
									$sel="";if($mybcl['category_id']==$koltseghely[$i]['id']){$sel=" selected ";}
									echo"<option value=\"".$koltseghely[$i]['id']."\" ".$sel.">".$koltseghely[$i]['name']."</option>";	
								}
								echo"</select>
							</div>
						</div>";					
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"worker_id\">Dolgozó:</label>
							<div class=\"col-md-8\">
								<select id=\"worker_id\" name=\"worker_id\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($workers);$i++){
									$sel="";if($mybcl['worker_id']==$workers[$i]['id']){$sel=" selected ";}
									echo"<option value=\"".$workers[$i]['id']."\" ".$sel.">".$workers[$i]['name']."</option>";	
								}
								echo"</select>
							</div>
						</div>";					
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"otherperson\">Átevő személy:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"otherperson\" name=\"otherperson\" class=\"form-control\" value=\"".$mybcl['otherperson']."\" />
							</div>
						</div>";	
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"costtext\">Jogcím:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"costtext\" name=\"costtext\" class=\"form-control\" value=\"".$mybcl['costtext']."\" />
							</div>
						</div>";	
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"cost\">összeg:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"cost\" name=\"cost\" class=\"form-control\" value=\"".$mybcl['cost']."\" />
							</div>
						</div>";	
						
						echo"<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"kiad_date\">Kiadás ideje:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"kiad_date\" name=\"kiad_date\" class=\"form-control input-datepicker\" value=\"".$mybcl['kiad_date']."\" />
							</div>
						</div>";
						
								
								echo "<div class=\"form-group form-actions\">
									<div class=\"col-md-6 \">
										<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditbcl(".$mybcl['id'].")\"><i class=\"fa fa-angle-right\"></i> Ment</button>
									</div>
								</div>";
									
							echo "</div>
							</form>";
						?>
						</div>

						<div class="tab-pane" id="tab-3">
								<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mybcl['id'],"Dokumentum feltöltés")."</div>";
								?> 
								<div id="filelists"></div>
								<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mybcl['id'],"Hozzászólások")."</div>";
								?>
						</div>
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>
				
					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
					
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/leaders/js/inc_bcl.js"></script>

			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mybcl['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$mybcl['id'];?>,'Dokumentum feltöltés','filelists');
			</script>
			<script>$(function(){ bclDatatables.init(); });</script>	

			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
		//print_r($_SESSION['planetsys'])
?>    
<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">BCL</a></li>
			</ul>
			<!-- END Datatables Header -->
		
				
				
			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>BCLe</strong> lista</h2>
				</div>
				
				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							//echo"<th>Művelet</th>";
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/leaders/js/inc_bcl.js"></script>
		<script>$(function(){ bclDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>

