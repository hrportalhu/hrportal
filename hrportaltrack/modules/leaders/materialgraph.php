<div id="page-content">
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Alapanyagfogyás statisztika
            </h1>
        </div>
    </div>
	<ul class="breadcrumb breadcrumb-top">
		<li><a href="/">Rendszer</a></li>
		<li><a href="/<?=$URI;?>">Alapanyagfogyás statisztika</a></li>
	</ul>
<div class="row" >
	<div class="col-sm-12" >
		<div class="block full" >
			<div class="block-title">
				<h2 ><strong>Szűrő</strong></h2>
			</div>
			<div >
			<?
			$als=db_all("select id,name from alapag_tipus where status=1 order by name asc");
		   echo"<div class=\"form-group\" >
					<div class=\"col-md-4\">
						<select id=\"post_id\" name=\"post_id\" class=\"form-control\"  onchange=\"javascript:showmaterialgraph();	\" >
							<option value=\"0\" selected>Válassz alapanyagot</option>";
							for($i=0;$i<count($als);$i++){
								echo"<option value=\"".$als[$i]['id']."\" >".$als[$i]['name']."</option>";
							}
						echo"</select>
					</div>
					<div class=\"col-md-3\">
						<input type=\"text\" id=\"matstdate\" name=\"matstdate\" class=\"form-control input-datepicker\" placeholder=\"Kezdő dátum\"  />			
					</div>
					<div class=\"col-md-3\">
						<input type=\"text\" id=\"matendate\" name=\"matendate\" class=\"form-control input-datepicker\" placeholder=\"befejező dátum\"  />			
					</div>
					<div class=\"col-md-2\">
						<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"showmaterialgraph();\">Szűr</button>
					</div>
				</div>";
		   
				?>	
				<div style="clear:both;height:10px;"></div>
			</div>
		</div>
	</div>
</div>
<div class="row" style="overflow:auto;">
	<div class="col-sm-12" style="overflow:auto;">
		<div class="block full" style="overflow:auto;">
			<div class="block-title">
				<h2 id="charttit"><strong>Készlet </strong> felhasználás</h2>
			</div>
			<div id="chartom" class="chart" ></div>
		</div>
	</div>
</div>
				
</div>
<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<script src="/modules/leaders/js/inc_materialgraph.js"></script>
<script >
	//getgraph('1','classic','goods','materials','3','0','2015-01-01','2015-12-32','monthly');
</script>
<?php include 'inc/template_end.php'; ?>
