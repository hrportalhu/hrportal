<?
//$posts=db_all("select post_id from orders_elements where  post_id!=0 group by post_id");
$posts=db_all("select * from gw_posts where  status=1 order by name");
?>
<div id="page-content">
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Éves összehasonlító forgalmi termelési statisztika
            </h1>
        </div>
    </div>
	<ul class="breadcrumb breadcrumb-top">
		<li><a href="/">Rendszer</a></li>
		<li><a href="/<?=$URI;?>">Éves összehasonlító forgalmi termelési statisztika </a></li>
	</ul>
	<div class="block full">
		<div class="block-title">
			<h2><strong>Elérhető</strong> forgalmi lekérdezések</h2>
		</div>
		<div style="clear:both;height:10px;"></div>
			<div class="block hidden-lt-ie9">
				<!-- Switches Title -->
				<div class="block-title">
					<h2><strong>Szűrő</strong> </h2>
				</div>
				<?
			   echo"<div class=\"form-group\" >
						<div class=\"col-md-4\">
							<select id=\"post_id\" name=\"post_id\" class=\"form-control\"  onchange=\"javascript:showyeardifferent();	\" >
								<option value=\"0\" selected>Minden munkahely</option>";
								for($i=0;$i<count($posts);$i++){
									echo"<option value=\"".$posts[$i]['id']."\" >".$posts[$i]['name']."</option>";
								}
							echo"</select>
									</div>
						<div class=\"col-md-2\">
							<input type=\"text\" id=\"matstdate\" name=\"matstdate\" class=\"form-control input-datepicker\" placeholder=\"Kezdő dátum\"  />			
						</div>
						<div class=\"col-md-2\">
							<input type=\"text\" id=\"matendate\" name=\"matendate\" class=\"form-control input-datepicker\" placeholder=\"befejező dátum\"  />			
						</div>
						
						<div class=\"col-md-2\">
							<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"javascript:showyeardifferent();\">Szűr</button>
						</div>
					</div>";
			   
					?>	
					<div style="clear:both;height:10px;"></div>
				</div>	
					
				
				<div class="block hidden-lt-ie9">
					<div id="yearsum"></div>
				</div>	
					


				<div style="clear:both;height:1px;"></div>
		
	</div>					
</div>
<?php include 'inc/page_footer.php'; ?>
<?php include 'inc/template_scripts.php'; ?>
<script src="/modules/leaders/js/inc_yeardifferent.js"></script>
<script >
	showyeardifferent();
	//getgraph('1','classic','goods','materials','3','0','2015-01-01','2015-12-32','monthly');
</script>
<?php include 'inc/template_end.php'; ?>
