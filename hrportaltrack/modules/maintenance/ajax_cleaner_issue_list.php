<?php
session_start();
header("Cache-control: private");
header('Content-Type: text/plain;charset=utf-8');
include_once dirname(__FILE__).("/../../inc/sys.conf");
include_once dirname(__FILE__).("/../../inc/_functions.php");

//Tábla, mező olvasási definíciók
	$qri="select 
					ce.id as id, 
					ce.ct_id as ct_id,
					ce.cc_id as cc_id, 
					ce.amount as amount, 
					ce.transaction_code as transcode, 
					ce.store_id as store_id,
					ce.post_id as post_id,
					ce.event as event,
					ce.rdate as rdate,
					ct.name as name,
					c.lot as lot,
					c.mmi as mmi,
					c.created as created,
					gwp.name as postname,
					st.name as storename
						from cleaner_events ce 
						left join cleaner c on(ce.cc_id=c.id)
						left join cleaner_type ct on (ce.ct_id=ct.id)
						left join gw_posts gwp on (ce.post_id=gwp.id)
						left join stores st on (ce.store_id=st.id)
					where 
					(ce.event='3' or ce.event='7')  ";
					

	$qri2="select 
					count(*) 
						from cleaner_events ce 
						left join cleaner a on(ce.cc_id=a.id)
						left join cleaner_type ct on (ce.ct_id=ct.id)
						left join gw_posts gwp on (ce.post_id=gwp.id)
						left join stores st on (ce.store_id=st.id)
					where 
					(ce.event='3' or ce.event='7') ";
					


//Limit definíció
	$sLimit = "";
	if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){$sLimit = "LIMIT ".mysql_real_escape_string( $_GET['iDisplayStart'] ).", ".mysql_real_escape_string( $_GET['iDisplayLength'] );}

//Rendezés
	$sOrder = "";
	if ( isset( $_GET['iSortCol_0'] ) ){
		$sOrder = "ORDER BY ";
		if($_GET['iSortCol_0']=="0"){$sOrder.=" ce.id ";}
		if($_GET['iSortCol_0']=="1"){$sOrder.=" ct.name ";}
		if($_GET['iSortCol_0']=="2"){$sOrder.=" amount ";}
		if($_GET['iSortCol_0']=="3"){$sOrder.=" c.lot ";}
		if($_GET['iSortCol_0']=="4"){$sOrder.=" c.mmi ";}
		if($_GET['iSortCol_0']=="5"){$sOrder.=" c.created ";}
		if($_GET['iSortCol_0']=="6"){$sOrder.=" gwp.name ";}
		if($_GET['sSortDir_0']=="asc"){$sOrder.=" asc ";}else{$sOrder.=" desc ";}
	}
//keresés
	$sWhere =  " ";
	if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
		$sWhere.= " and (
						ce.ct_id LIKE  '%".mysql_real_escape_string( $_GET['sSearch'] )."%' or 
						cc_id LIKE  '%".mysql_real_escape_string( $_GET['sSearch'] )."%' or 
						amount LIKE  '%".mysql_real_escape_string( $_GET['sSearch'] )."%' or 
						ce.ct_id LIKE  '%".mysql_real_escape_string( $_GET['sSearch'] )."%' or 
						ct.name LIKE  '%".mysql_real_escape_string( $_GET['sSearch'] )."%' or 
						c.lot LIKE  '%".mysql_real_escape_string( $_GET['sSearch'] )."%' or 
						gwp.name LIKE  '%".mysql_real_escape_string( $_GET['sSearch'] )."%' or 
						st.name LIKE  '%".mysql_real_escape_string( $_GET['sSearch'] )."%'  
						) ";
	}
	$sWhere2="";

	$elems=db_all($qri." ".$sWhere."  ".$sOrder." ".$sLimit."");
	$elemdb=db_one($qri2." ".$sWhere);
	$iFilteredTotal =  count($elems);
	$iTotal = db_one($qri2);
	$output = array("sEcho" => intval($_GET['sEcho']),"iTotalRecords" => $iTotal,"iTotalDisplayRecords" =>  $elemdb,"aaData" => array());

	for($k=0;$k<count($elems);$k++){
		$row = array();
		$row[] = $elems[$k]['id'];
		$row[] = "<a href=\"/modules/maintenance/cleaner_issue/".$elems[$k]['ct_id']."\">".$elems[$k]['name']."</a>";
		$row[] = round($elems[$k]['amount'],3);
		$row[] = $elems[$k]['lot'];
		$row[] = $elems[$k]['mmi'];
		$row[] = $elems[$k]['rdate'];
		$row[] = $elems[$k]['postname'];
		$row[] = "<a href=\"/modules/goods/stores/".$elems[$k]['store_id']."\">".$elems[$k]['storename']."</a>";
		if($elems[$k]['event']=="3"){
			$event="Kiadás raktárból";	
		}
		else{
			$event="Visszavétel raktárba";	
		}
		$row[] = $event;
		$output['aaData'][] = $row;
	}
$_SESSION['planetsys']['exportdata'] = $output ;
echo json_encode( $output );
mysql_close($connid);	
?>
