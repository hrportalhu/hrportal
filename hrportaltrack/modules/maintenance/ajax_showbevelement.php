<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors','Off');
header("Cache-control: private");
header('Content-Type: text/plain;charset=utf-8');
include_once dirname(__FILE__).("/../../inc/sys.conf");
include_once dirname(__FILE__).("/../../inc/_functions.php");

$b=db_all("select * from cleaner where cheader_id='".$_REQUEST['elemid']."' and status=1 order by id asc");
$lezart=db_one("select lezarva from cleaner_header where id='".$_REQUEST['elemid']."'");
echo "select lezarva from cleaner_header where id='".$_REQUEST['elemid']."'";
?>

<div class="table-responsive">
		<table class="table table-vcenter table-striped">
			<thead>
				<tr>
					
					<th>Termék név</th>
					<th>Bevételezett mennyiség</th>
					<th>Aktuális mennyiség</th>
					<th>Egységár</th>
					<th>Elhelyezés</th>
					<th>Egyéb infók</th>
					
					<th style="width: 150px;" class="text-center">Művelet</th>
				</tr>
			</thead>
			<tbody>
				<?php
					//$orders=db_all("select * from orders where status='1' order by order_date desc");
					for($i=0;$i<count($b);$i++){
						$alap=db_row("select * from cleaner_type where id='".$b[$i]['ct_id']."'");
						$bev=db_row("select * from cleaner_events where ct_id='".$b[$i]['id']."' and event=1 order by id asc");
						$rakt=db_row("select name from stores where id='".$b[$i]['store_id']."'");
						$mertek=idchange("mertekegyseg","name",$alap['measure']);
						echo"<tr>";
						echo"<td><a href=\"/modules/maintenance/cleaner_type/".$b[$i]['ct_id']."\">".$alap['name']."</a></td>";
						echo"<td>".round($bev['amount'],3)." ".$mertek."</td>";
						echo"<td>".round($b[$i]['units'],3)." ".$mertek."</td>";
						echo"<td>".round($b[$i]['price'],2)." ft</td>";
						echo"<td><a href=\"/modules/goods/stores/".$b[$i]['store_id']."\">".$rakt['name']."</a></td>";
						echo"<td>NNSZ:".$b[$i]['nnsz'].", MMI:".$b[$i]['mmi'].", LOT: ".$b[$i]['lot'].",  </td>";
						echo"<td class=\"text-center\">";
						if($lezart==0){
							echo"<div class=\"btn-group btn-group-xs\"><a href=\"javascript:delbevetelemet('".$b[$i]['id']."','".$_REQUEST['elemid']."');\" data-toggle=\"tooltip\" title=\"Delete\" class=\"btn btn-danger\"><i class=\"fa fa-times\"></i></a></div>";
						}
						echo"</td>";
						echo"</tr>";
					}
				?>
			</tbody>
		</table>
	</div>
</div>
<?

mysql_close($connid);
?>
