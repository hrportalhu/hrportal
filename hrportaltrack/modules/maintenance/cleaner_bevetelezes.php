<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Tisztítószer bevételezés kezelő
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/maintenance/cleaner_bevetelezes">Tisztítószer bevételezés kezelő</a></li>
				<li><a href="/new">Új bevételezés</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új bevételezés felvitele</strong></h2>
				</div>
			<?
			$beszall=db_all("select id, name from partners where status=1 order by name");
					
				echo "<form id=\"newcleaner_bevetelezes\" name=\"newcleaner_bevetelezes\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";
				
					echo"<div class=\"form-group\">
					<label class=\"col-md-3 control-label\" for=\"bev_ido\">Bevételezés ideje:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"bev_ido\" name=\"bev_ido\" class=\"form-control input-datepicker\" placeholder=\"Bevételezés ideje\"  />																		
						</div>
					</div>";
					
					echo"<div class=\"form-group\">
					<label class=\"col-md-3 control-label\" for=\"szall_ido\">Szállítás ideje:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"szall_ido\" name=\"szall_ido\" class=\"form-control input-datepicker\" placeholder=\"Szállítás ideje\"  />																		
						</div>
					</div>";
					
					echo"<div class=\"form-group\">
					<label class=\"col-md-3 control-label\" for=\"bev_szlaszam\">Bizonylat száma:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"bev_szlaszam\" name=\"bev_szlaszam\" class=\"form-control\" placeholder=\"Bizonylat száma\"  />																		
						</div>
					</div>";
					
					echo"<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"partner_id\">Beszállító:</label>
						<div class=\"col-md-9\">
							<select id=\"partner_id\" name=\"partner_id\" class=\"form-control\" placeholder=\"Beszállító\" >
							<option value=\"0\">Kérlek válassz</option>";
							for($i=0;$i<count($beszall);$i++){
								echo"<option value=\"".$beszall[$i]['id']."\" >".$beszall[$i]['name']."</option>";	
							}
							echo"</select>
						</div>
					</div>";
					
					echo"<div class=\"form-group\">
					<label class=\"col-md-3 control-label\" for=\"driversname\">Sofőr neve:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"driversname\" name=\"driversname\" class=\"form-control\" placeholder=\"Sofőr neve\"  />																		
						</div>
					</div>";
					
					echo"<div class=\"form-group\">
					<label class=\"col-md-3 control-label\" for=\"numberplate\">Rendszám:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"numberplate\" name=\"numberplate\" class=\"form-control\" placeholder=\"Rendszám\"  />																		
						</div>
					</div>";
					
					echo"<div class=\"form-group\">
					<label class=\"col-md-3 control-label\" for=\"vehicletemperature\">Raktér hőmérséklet:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"vehicletemperature\" name=\"vehicletemperature\" class=\"form-control\" placeholder=\"Raktér hőmérséklet\"  />																		
						</div>
					</div>";
					
					echo"<div class=\"form-group\">
					<label class=\"col-md-3 control-label\" for=\"ekaer\">EKAER:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"ekaer\" name=\"ekaer\" class=\"form-control\" placeholder=\"EKAER\"  />																		
						</div>
					</div>";						
					
					echo "<div class=\"form-group\" >
						<label class=\"col-md-3 control-label\" for=\"gongyoleg_ok\">Göngyöleg rendben:</label>
						<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"gongyoleg_ok\" name=\"gongyoleg_ok\" checked><span></span></label>
					
						<label class=\"col-md-3 control-label\" for=\"gongyoleg_tiszta\">Göngyöleg tiszta:</label>
						<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"gongyoleg_tiszta\" name=\"gongyoleg_tiszta\" checked><span></span></label>
					
						<label class=\"col-md-3 control-label\" for=\"jarmu_tiszta\">Jármű tiszta:</label>
						<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"jarmu_tiszta\" name=\"jarmu_tiszta\" checked><span></span></label>
					
						<label class=\"col-md-3 control-label\" for=\"visszaru\">Visszaárú:</label>
						<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"visszaru\" name=\"visszaru\" ><span></span></label>
					
						<label class=\"col-md-3 control-label\" for=\"status\">Státusz:</label>
						<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span></label>
					</div>";
					
					echo"<div style=\"clear:both;height:10px;\"></div>";
					
					echo"<div class=\"form-group form-actions\">
						<div class=\"col-md-9 col-md-offset-3\">
							<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewcleaner_bevetelezes();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
						</div>
					</div>";
					
				echo"</div>";
				echo"</form>";
			?>	
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/maintenance/js/inc_cleaner_bevetelezes.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?	
		}
		else{
			
			//echo "select * from functions where id='".$uritags[2]."'";
			$mycleaner_bevetelezes=db_row("select * from cleaner_header where id='".$uritags[3]."'");
			if($mycleaner_bevetelezes['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="modules/maintenance/cleaner_bevetelezes">Tisztítószer Bevételezések</a></li>
				<li><a href="/<?=$URI;?>"><?=$mycleaner_bevetelezes['name'];?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->

					<div class="block-title">
						<h2><strong><?=$mycleaner_bevetelezes['id'];?>.</strong> bevétel oldala</h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<li class="active"><a href="#tab-1" data-toggle="tooltip" title="view"><i class="gi gi-eye_open"></i></a></li>
							<li><a href="#tab-2" data-toggle="tooltip" title="edit"><i class="gi gi-pencil"></i></a></li>
							<li><a href="#tab-6" data-toggle="tooltip" title="inbound"><i class="gi gi-magic"></i></a></li>
							<li><a href="#tab-7" data-toggle="tooltip" title="price"><i class="fa fa-eur"></i></a></li>
							<li><a href="#tab-3" data-toggle="tooltip" title="upload"><i class="gi gi-floppy_open"></i></a></li>
							<li><a href="#tab-4" data-toggle="tooltip" title="comments"><i class="fa fa-comments-o"></i></a></li>
						<!--	<li><a href="#tab-5" data-toggle="tooltip" title="print"><i class="fa fa-print"></i></a></li>-->
							<li onclick="javascript:getprintscreen('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','cleaner_bevetelezesprint','<?=$mycleaner_bevetelezes['id'];?>');" ><a href="#modal-regular-print" data-toggle="modal" title="Névsor nyomtatása"><i class="fa fa-print" ></i></a></li>						
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="block hidden-lt-ie9">
								<!-- Switches Title -->
								<?
								if($mycleaner_bevetelezes['lezarva']!="1"){
									echo"<div class=\"block-title text-danger\"><h2 ><strong >Bevételi információk - NINCS LEZÁRVA!</strong> </h2></div>";
								}
								else{
									echo"<div class=\"block-title \"><h2 ><strong >Bevételi információk</strong> </h2></div>";
								}
								?>
								<div class="col-md-6">
									<dl class="dl-horizontal">
										<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$mycleaner_bevetelezes['id']); ?></dd>
										<dt>Bevételezés ideje:</dt><dd><? echo $mycleaner_bevetelezes['bev_ido']; ?></dd>
										<dt>Szálltás ideje:</dt><dd><? echo $mycleaner_bevetelezes['szall_ido']; ?></dd>
										<dt>Bizonyat száma:</dt><dd><? echo $mycleaner_bevetelezes['bev_szlaszam']; ?></dd>
										<dt>Beszállító:</dt><dd><? echo idchange("partners","name",$mycleaner_bevetelezes['partner_id']); ?></dd>
										<dt>Söfőr neve:</dt><dd><? echo $mycleaner_bevetelezes['driversname']; ?></dd>
										<dt>Rendszám:</dt><dd><? echo $mycleaner_bevetelezes['numberplate']; ?></dd>
										<dt>Raktér hömérséklet:</dt><dd><? echo round($mycleaner_bevetelezes['vehicletemperature'],2); ?> Celsius</dd>
										<dt>EKAER:</dt><dd><? echo $mycleaner_bevetelezes['ekaer']; ?></dd>
										<dt>Göngyöleg rendben:</dt><dd><? if($mycleaner_bevetelezes['gongyoleg_ok']==1){echo"Igen";}else{echo"Nem";} ?></dd>
										<dt>Göngyöleg tiszta:</dt><dd><? if($mycleaner_bevetelezes['gongyoleg_tiszta']==1){echo"Igen";}else{echo"Nem";} ?></dd>
										<dt>Jármű tiszta:</dt><dd><? if($mycleaner_bevetelezes['jarmu_tiszta']==1){echo"Igen";}else{echo"Nem";} ?></dd>
										<dt>Visszárú:</dt><dd><? if($mycleaner_bevetelezes['visszaru']==1){echo"Van";}else{echo"Nincs";} ?></dd>
										<dt>Státusz:</dt><dd><? if($mycleaner_bevetelezes['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
										<br>
										<dt>Lezárva:</dt><dd><? if($mycleaner_bevetelezes['lezarva']==1){echo"Igen";}else{echo"Nem";} ?></dd>
										<?
											//echo showRecord($_SESSION['planetsys']['actpage'],$mycleaner_bevetelezes,"1")."<br />";
										?>
									</dl>	
								</div>
								<div class="col-md-6">
									<?php
										$pic=db_one("select filename from files where modul='goods' and type='cleaner_bevetelezes' and type_id='".$mycleaner_bevetelezes['id']."' order by cover desc");
										if(is_file($pic)){
											echo"<div class=\"col-sm-3\"><img src=\"/".$pic."\" alt=\"avatar\" width=\"450px\"></div>";
										}
										else{
											echo"<img src=\"img/placeholders/avatars/avatar9@2x.jpg\" alt=\"avatar\">";
										}
									?>
								</div>
								<div style="clear:both;height:1px;"></div>
							</div>
							
							<div style="clear:both;height:1px;"></div>
								<div class="block hidden-lt-ie9">
								<!-- Switches Title -->
								<div class="block-title">
									<h2><strong>Bevételezett elemek</strong> </h2>
								</div>
								<!-- END Switches Title -->

								<div id="elementdiv1"></div>
							</div>		
							<div style="clear:both;height:10px;"></div>
						</div>
						<div class="tab-pane" id="tab-2">
						<?
							$beszall=db_all("select id, name from partners where status=1 order by name");
					
							echo "<form id=\"editcleaner_bevetelezes\" name=\"editcleaner_bevetelezes\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
							echo "<div style=\"margin-top:1px;\">";
							
								echo"<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"bev_ido\">Bevételezés ideje:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"bev_ido\" name=\"bev_ido\" class=\"form-control input-datepicker\" value=\"".$mycleaner_bevetelezes['bev_ido']."\" />																		
									</div>
								</div>";
								
								echo"<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"szall_ido\">Szállítás ideje:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"szall_ido\" name=\"szall_ido\" class=\"form-control input-datepicker\" value=\"".$mycleaner_bevetelezes['szall_ido']."\"/>																		
									</div>
								</div>";
								
								echo"<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"bev_szlaszam\">Bizonylat száma:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"bev_szlaszam\" name=\"bev_szlaszam\" class=\"form-control\" value=\"".$mycleaner_bevetelezes['bev_szlaszam']."\"/>																		
									</div>
								</div>";
								
								echo"<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"partner_id\">Beszállító:</label>
									<div class=\"col-md-9\">
										<select id=\"partner_id\" name=\"partner_id\" class=\"form-control\" >
										<option value=\"0\">Kérlek válassz</option>";
										for($i=0;$i<count($beszall);$i++){
											$ch1="";if($mycleaner_bevetelezes['partner_id']==$beszall[$i]['id']){$ch1=" selected "; }
											echo"<option value=\"".$beszall[$i]['id']."\" ".$ch1.">".$beszall[$i]['name']."</option>";	
										}
										echo"</select>
									</div>
								</div>";
								
								echo"<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"driversname\">Sofőr neve:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"driversname\" name=\"driversname\" class=\"form-control\" value=\"".$mycleaner_bevetelezes['driversname']."\"/>																		
									</div>
								</div>";
								
								echo"<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"numberplate\">Rendszám:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"numberplate\" name=\"numberplate\" class=\"form-control\" value=\"".$mycleaner_bevetelezes['numberplate']."\"/>																		
									</div>
								</div>";
								
								echo"<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"vehicletemperature\">Raktér hőmérséklet:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"vehicletemperature\" name=\"vehicletemperature\" class=\"form-control\" value=\"".$mycleaner_bevetelezes['vehicletemperature']."\"/>																		
									</div>
								</div>";
								
								echo"<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"ekaer\">EKAER:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"ekaer\" name=\"ekaer\" class=\"form-control\" value=\"".$mycleaner_bevetelezes['ekaer']."\"/>																		
									</div>
								</div>";
								
								echo "<div class=\"form-group\" >

								<label class=\"col-md-3 control-label\" for=\"gongyoleg_ok\">Göngyöleg rendben:</label>";
								$ch1="";if($mycleaner_bevetelezes['gongyoleg_ok']==1){ $ch1=" checked "; }
								echo "<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"gongyoleg_ok\" name=\"gongyoleg_ok\"".$ch1." ><span></span></label>

								<label class=\"col-md-3 control-label\" for=\"gongyoleg_tiszta\">Göngyöleg tiszta:</label>";
								$ch2="";if($mycleaner_bevetelezes['gongyoleg_tiszta']==1){ $ch2=" checked "; }
								echo "<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"gongyoleg_tiszta\" name=\"gongyoleg_tiszta\"".$ch2." ><span></span></label>

								<label class=\"col-md-3 control-label\" for=\"jarmu_tiszta\">Jármű tiszta:</label>";
								$ch3="";if($mycleaner_bevetelezes['jarmu_tiszta']==1){ $ch3=" checked "; }
								echo "<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"jarmu_tiszta\" name=\"jarmu_tiszta\"".$ch3." ><span></span></label>

								<label class=\"col-md-3 control-label\" for=\"visszaru\">Visszaárú:</label>";
								$ch4="";if($mycleaner_bevetelezes['visszaru']==1){ $ch4=" checked "; }
								echo "<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"visszaru\" name=\"visszaru\"".$ch4." ><span></span></label>

								<label class=\"col-md-3 control-label\" for=\"status\">Státusz:</label>";
								$st="";if($mycleaner_bevetelezes['status']==1){ $st=" checked "; }
								echo "<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\"".$st." ><span></span></label>
								
								</div>";
								
								echo"<div style=\"clear:both;height:10px;\"></div>";
								
								echo"<div class=\"form-group form-actions\">
									<div class=\"col-md-9 col-md-offset-3\">
										<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditcleaner_bevetelezes('".$mycleaner_bevetelezes['id']."');\"><i class=\"fa fa-angle-right\"></i> Ment</button>
									</div>
								</div>";
									
							echo"</div>";
							echo"</form>";
						?>
						</div>
						<div class="tab-pane" id="tab-3">
								<?
								echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mycleaner_bevetelezes['id'],"Dokumentum feltöltés")."</div>";
							   ?> 
							   <div id="filelists"></div>
							   <div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mycleaner_bevetelezes['id'],"Hozzászólások")."</div>";
								?>
						</div>
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						
						</div>
						<div class="tab-pane" id="tab-7">
							<div>Árazás</div>
								<div class="table-responsive">
									<table class="table table-vcenter table-striped">
									<thead>
										<tr>
											<th style="width: 100px;" class="text-center"><i class="gi gi-disk_save"></i></th>
											<th class="text-center">Terméknév</th>
											<th class="text-center">Egységár</th>
											<th class="text-center">Művelet</th>
											
										</tr>
									</thead>
									<tbody  id="bevcostcontdiv"></tbody>
									</table>		
								</div>
						
						</div>
						<div class="tab-pane" id="tab-6">
							<?
								$stores=db_all("select id, name from stores where status=1 order by name");
								$adc="";if($mycleaner_bevetelezes['lezarva']=="1"){ $adc=" hide ";}
							?>
							
							   <div class="block" >

	
								<div class="block-title">
										<div class="block-options pull-right">
											<a href="javascript:void(0)" class="btn btn-alt btn-sm btn-primary" data-toggle="block-toggle-content"><i class="fa fa-arrows-v"></i></a>
										</div>
									<h2><strong>Új bevételezési tétel</strong> </h2>
								</div>
								<!-- END Switches Title -->

								<!-- Switches Content -->
								<form  id="newelementform" id="newelementform" class="form-horizontal form-bordered" onsubmit="return false;">
									
									<div class="row block-content" style="padding:10px;">
										<div class="col-sm-4">
											<div class="block">
												<div class="form-group" >
													<label class="col-md-6 control-label" for="agt_id">Elnevezés</label>
													<div class="col-md-6">
														<input type="text" id="agt_id" name="agt_id" class="form-control" value="" />
														<input type="hidden" id="agt_myid" name="agt_myid" value="" />
													</div>
												</div>
												<div class="form-group" >
													<label class="col-md-6 control-label" for="store">Specifikált elem</label>
													<div class="col-md-6">
														<select id="myspec" name="myspec" class="form-control" onfocus="javascript:getspecelem();" >
															<option value="0">Kérlek válassz</option>
														</select>
													</div>
												</div>
												<div class="form-group" >
													<label class="col-md-6 control-label" for="mennyiseg">Mennyiség</label>
													<div class="col-md-6">
														<input type="text" id="mennyiseg" name="mennyiseg" class="form-control"  value="" />
													</div>
												</div>
												<div class="form-group" >
													<label class="col-md-6 control-label" for="nettoar">Nettó Ár</label>
													<div class="col-md-6">
														<input type="text" id="nettoar" name="nettoar" class="form-control" value="" />
													</div>
												</div>
											</div>
										
										</div>
										
										<div class="col-sm-4">
											
											<div class="block">
												<div class="form-group" >
													<label class="col-md-3 control-label" for="homerseklet">Hő</label>
													<div class="col-md-9">
														<input type="text" id="homerseklet" name="homerseklet" class="form-control" value="" />
													</div>
												</div>
											
												<div class="form-group" >
													<label class="col-md-3 control-label" for="lot">LOT Kód</label>
													<div class="col-md-9">
														<input type="text" id="lot" name="lot" class="form-control" value="" />
													</div>
												</div>
												<div class="form-group" >
													<label class="col-md-3 control-label" for="mmi">MMI</label>
													<div class="col-md-9">
														<input type="text" id="mmi" name="mmi" class="form-control input-datepicker" value="" />
													</div>
												</div>
											
												<div class="form-group" >
													<label class="col-md-3 control-label" for="nnsz">NNSZ</label>
													<div class="col-md-9">
														<input type="text" id="nnsz" name="nnsz" class="form-control" value="" />
													</div>
												</div>
												<div class="form-group" >
													<label class="col-md-3 control-label" for="store">Raktár</label>
													<div class="col-md-9">
														<select id="mystore" name="mystore" class="form-control" >
															<option value="0">Kérlek válassz</option>
															<?
																for($i=0;$i<count($stores);$i++){
																	echo"<option value=\"".$stores[$i]['id']."\">".$stores[$i]['name']."</option>";	
																}
															?>
														</select>
													</div>
												</div>
													<div style="clear:both;"></div>
											</div>
										
										</div>
									
									
									
										
									<div style="clear:both;"></div>
									<div class="col-md-9 col-md-offset-0">
										<button type="button" class="btn btn-sm btn-primary" id="savenewbutton" onclick="javascript:addbevelement(<?=$mycleaner_bevetelezes['id'];?>,'elementdiv2');"><i class="fa fa-angle-right"></i> Ment</button>
									</div>
									<div style="clear:both;"></div>	
									</div>
								</form>	
					
						</div>
						  <div class="block hidden-lt-ie9">
								<!-- Switches Title -->
								<div class="block-title">
									<h2><strong>Bevételezett elemek</strong> </h2>
								</div>
								<!-- END Switches Title -->

								<div id="elementdiv2"></div>
								
							</div>		
								<div style="clear:both;"></div>
								<?
								$eelems=db_one("select count(id) from alapanyag where agf_id='".$mycleaner_bevetelezes['id']."' and price=0 ");
								
								if($mycleaner_bevetelezes['lezarva']==0 && $eelems==0){
									echo"<div style=\"clear:both;\"></div>";
									echo"<div class=\"form-group form-actions\">
											<div class=\"col-md-9\">
												<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"closeeditcleaner_bevetelezes('".$mycleaner_bevetelezes['id']."');\"><i class=\"fa fa-angle-right\"></i> Lezár</button>
											</div>
									<div style=\"clear:both;\"></div>
								</div>";
								}
								?>
					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
					
			</div>
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="/modules/maintenance/js/inc_cleaner_bevetelezes.js"></script>

			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mycleaner_bevetelezes['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$mycleaner_bevetelezes['id'];?>,'Dokumentum feltöltés','filelists');
				showbevelement('<?=$mycleaner_bevetelezes['id'];?>','elementdiv1');			
				showbevelement('<?=$mycleaner_bevetelezes['id'];?>','elementdiv2');			
				bevcostcont('<?=$mycleaner_bevetelezes['id'];?>','bevcostcontdiv');			
			</script>
			<script>
				$(function(){
				 cleaner_bevetelezesDatatables.init(); 
		
				});
				</script>	

			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
		//print_r($_SESSION['planetsys'])
?>    
<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Tisztítószer Bevételek</a></li>
			</ul>
			<!-- END Datatables Header -->

			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Bevételezések</strong> listája</h2>
				</div>
				

				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							//echo"<th>Művelet</th>";
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="/modules/maintenance/js/inc_cleaner_bevetelezes.js"></script>
		<script>$(function(){ cleaner_bevetelezesDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>
