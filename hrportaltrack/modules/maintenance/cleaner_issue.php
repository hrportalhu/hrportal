<?
$munkahelyek=db_all("select id,name from gw_posts where status=1");
$stores=db_all("select * from stores where status=1 order by name asc");
?>
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Kiadás kezelő
            </h1>
        </div>
    </div>
<?
if(isset($uritags[3]) && $uritags[3]!=""){
		//echo "select * from functions where id='".$uritags[2]."'";
		$myissue=db_row("select * from cleaner_events where id='".$uritags[3]."'");
		$melem=db_all("select id,lot,mmi,units from cleaner where ct_id='".$myissue['ct_id']."' and units!='0'");
		$pt=db_row("select * from cleaner_type where id='".$myissue['ct_id']."'");
		
		if($myissue['id']==''){
			die("Helytelen hozzáférés!");
		}
		else{
?>
		<!-- Ez egy elem  -->
		<ul class="breadcrumb breadcrumb-top">
			<li><a href="/">Rendszer</a></li>
			<li><a href="/modules/maintenance/cleaner_issue">Kiadás kezelő</a></li>
			<li><a href="/<?=$myissue['id'];?>"><?=$myissue['id'];?> sorszámú kiadás</a></li>
		</ul>
		<div class="block full">
			
				<!-- Block Tabs Title -->
				<div class="block-title">
					<h2><strong><?=$myissue['id'];?>.</strong> sorszámú kiadás</h2>
					<ul class="nav nav-tabs" data-toggle="tabs">
						<li><a href="#tab-1" data-toggle="tooltip" title="edit"><i class="gi gi-pencil"></i></a></li>
						<?
							if($myissue['event']==3){	
						?>
						<li><a href="#tab-2" data-toggle="tooltip" title="getback"><i class="hi hi-fast-backward"></i></a></li>
						<?
							}
						?>
						
					</ul>
				</div>
				<!-- END Block Tabs Title -->

				<!-- Tabs Content -->
				<div class="tab-content">

					<div class="tab-pane active" id="tab-1">
						<div class="block hidden-lt-ie9">
							<div class="block-title "><h2 ><strong >Szerkeszt</strong> </h2></div>
							<div class="col-md-12">
								<form  id="editelementform" id="editelementform" class="form-horizontal form-bordered" onsubmit="return false;">
									<input type="hidden" id="aid" name="aid" value="<?=$myissue['id'];?>" />  
									<div class="form-group" style="width:300px;float:left;">
										<label class="col-md-3 control-label" for="newitem">Alapanyag</label>
										<div class="col-md-9">
											<input type="text" id="newitem" name="newitem" class="form-control" value="<?=$pt['name'];?>" disabled />
										</div>
									</div>
									<div class="form-group" style="width:300px;float:left;">
										<label class="col-md-3 control-label" for="tetel">Tétel</label>
										<div class="col-md-9">
											<select id="tetel" name="tetel" class="form-control" >
												<option value="0">Kérlek válassz</option>
												<?
													for($i=0;$i<count($melem);$i++){
														$sel="";if($myissue['ct_id']==$melem[$i]['id']){$sel=" selected";}
														echo"<option value=\"".$melem[$i]['id']."\" ".$sel.">".$melem[$i]['lot']." - ".$melem[$i]['mmi']." (".round($melem[$i]['units'],3).")</option>";
													}
												?>
											</select>
										</div>
									</div>
									<div class="form-group" style="width:300px;float:left;">
										<label class="col-md-3 control-label" for="amount">Mennyiség</label>
										<div class="col-md-9">
											<input type="text" id="amount" name="amount" class="form-control" value="<?=abs($myissue['amount']);?>" />
										</div>
									</div>
									<div class="form-group" style="width:300px;float:left;">
										<label class="col-md-3 control-label" for="amount">Dátum</label>
										<div class="col-md-9">
											<input type="text" id="rdate" name="rdate" class="form-control input-datepicker" value="<?=substr($myissue['rdate'],0,10);?>" />
										</div>
									</div>
									
									<div class="form-group" style="width:300px;float:left;">
										<label class="col-md-3 control-label" for="store">Poszt</label>
										<div class="col-md-9">
											<select id="munkahely" name="munkahely" class="form-control" >
												<option value="0">Kérlek válassz</option>
												<?
													for($i=0;$i<count($munkahelyek);$i++){
														$sel="";if($myissue['post_id']==$munkahelyek[$i]['id']){$sel=" selected";}
														echo"<option value=\"".$munkahelyek[$i]['id']."\" ".$sel.">".$munkahelyek[$i]['name']."</option>";	
													}
												?>
											</select>
										</div>
									</div>
									<div style="clear:both;height:10px;"></div>
									<div class="col-md-9 col-md-offset-0">					
										<button type="button" class="btn btn-sm btn-primary" id="savenewbutton" onclick="javascript:editissuelement();"><i class="fa fa-angle-right"></i> Ment</button>
									</div>
									<div style="clear:both;"></div>
								</form>	
								<div style="clear:both;height:10px;"></div>
							</div>
							<div style="clear:both;height:1px;"></div>
						</div>
					</div>
					<?
					if($myissue['event']==3){
						
						$maxmennyi=db_one("select sum(amount) from cleaner_events where post_id='".$myissue['post_id']."' and ag_id='".$myissue['ag_id']."' and (event=7 or event=3) ");
						
						if(abs($maxmennyi)>0){
						
					?>
					<div class="tab-pane" id="tab-2">
						<div class="block hidden-lt-ie9">
							<div class="block-title "><h2 ><strong >Visszavesz</strong> </h2></div>
							<div class="col-md-12">
								<form  id="editelementform" id="editelementform" class="form-horizontal form-bordered" onsubmit="return false;">
									<input type="hidden" id="aid2" name="aid2" value="<?=$myissue['id'];?>" />  
									<input type="hidden" id="maxmennyi" name="maxmennyi" value="<?=abs($maxmennyi);?>" />  
									
									<div class="form-group" style="width:300px;float:left;">
										<label class="col-md-3 control-label" for="newitem2">Alapanyag</label>
										<div class="col-md-9">
											<input type="text" id="newitem2" name="newitem2" class="form-control" value="<?=$pt['name'];?>" disabled />
										</div>
									</div>
									
									<div class="form-group" style="width:300px;float:left;">
										<label class="col-md-3 control-label" for="tetel2">Tétel</label>
										<div class="col-md-9">
											<?
												//echo("select id,lot,mmi,units from alapanyag where id='".$myissue['ag_id']."' and consumed='0000-00-00 00:00:00'");
											?>
											<select id="tetel2" name="tetel2" class="form-control" disabled >
												
												<?
												$melem=db_all("select id,lot,mmi,units from alapanyag where id='".$myissue['ag_id']."' and consumed='0000-00-00 00:00:00'");
													for($i=0;$i<count($melem);$i++){
														$sel="";if($myissue['ag_id']==$melem[$i]['id']){$sel=" selected";}
														echo"<option value=\"".$melem[$i]['id']."\" ".$sel.">".$melem[$i]['lot']." - ".$melem[$i]['mmi']." (".round($melem[$i]['units'],3).")</option>";
													}
												?>
											</select>
										</div>
									</div>
									
									<div class="form-group" style="width:300px;float:left;">
										<label class="col-md-3 control-label" for="amount">Mennyiség</label>
										<div class="col-md-9">
											<input type="text" id="amount2" name="amount2" class="form-control" value="" placeholder="max: <?=abs($maxmennyi);?>" />
										</div>
									</div>
									
									<div class="form-group" style="width:300px;float:left;">
										<label class="col-md-3 control-label" for="amount">Dátum</label>
										<div class="col-md-9">
											<input type="text" id="rdate2" name="rdate2" class="form-control input-datepicker" value="<?=date("Y-m-d",time());?>" />
										</div>
									</div>
									
									<div class="form-group" style="width:300px;float:left;">
										<label class="col-md-3 control-label" for="munkahely2">Posztról</label>
										<div class="col-md-9">
											<select id="munkahely2" name="munkahely2" class="form-control" disabled >
												<option value="0">Kérlek válassz</option>
												<?
													for($i=0;$i<count($munkahelyek);$i++){
														$sel="";if($myissue['post_id']==$munkahelyek[$i]['id']){$sel=" selected";}
														echo"<option value=\"".$munkahelyek[$i]['id']."\" ".$sel.">".$munkahelyek[$i]['name']."</option>";	
													}
												?>
											</select>
										</div>
									</div>
									
									<div class="form-group" style="width:300px;float:left;">
										<label class="col-md-3 control-label" for="store2">Raktárba</label>
										<div class="col-md-9">
											<select id="store2" name="store2" class="form-control"  >
												<option value="0">Kérlek válassz</option>
												<?
													for($i=0;$i<count($stores);$i++){
														
														echo"<option value=\"".$stores[$i]['id']."\" >".$stores[$i]['name']."</option>";	
													}
												?>
											</select>
										</div>
									</div>
									
									<div style="clear:both;height:10px;"></div>
									
									<div class="col-md-9 col-md-offset-0">					
										<button type="button" class="btn btn-sm btn-primary" id="savenewbutton" onclick="javascript:backissuelement();"><i class="fa fa-angle-right"></i> Visszavesz</button>
									</div>
									
									<div style="clear:both;"></div>
								</form>	
								<div style="clear:both;height:10px;"></div>
							</div>
							<div style="clear:both;height:1px;"></div>
						</div>
						<?
							}
						}
						?>
					</div>

				<!-- END Tabs Content -->
			</div>
			<!-- END Block Tabs -->
				
		</div>
		
		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>
		
		<?php include 'inc/template_end.php'; ?>		
<?
		}
}
else{
		if($_POST['newitem']==''){$_POST=$_SESSION['planetsys']['ag_issue'];}
			
		?>
		<!-- Ez egy elem  -->		
		<ul class="breadcrumb breadcrumb-top">
			<li><a href="/">Rendszer</a></li>
			<li><a href="/modules/goods/issue">Kiadás kezelő</a></li>
			<li><a href="/new">Új Kiadás</a></li>
		</ul>
		<div class="block full">
			 <form  id="newelementform" id="newelementform" class="form-horizontal form-bordered" onsubmit="return false;">
				<div class="form-group" style="width:300px;float:left;">
					<label class="col-md-3 control-label" for="newissueitem">Tisztítószer</label>
					<div class="col-md-9">
						<input type="text" id="newissueitem" name="newissueitem" class="form-control" value="" />
					</div>
				</div>
				<div class="form-group" style="width:300px;float:left;">
					<label class="col-md-3 control-label" for="tetelissue">Tétel</label>
					<div class="col-md-9">
						<select id="tetelissue" name="tetelissue" class="form-control" >
							<option value="0">Kérlek válassz</option>
						</select>
					</div>
				</div>
				<div class="form-group" style="width:300px;float:left;">
					<label class="col-md-3 control-label" for="amount">Mennyiség</label>
					<div class="col-md-9">
						<input type="text" id="amount" name="amount" class="form-control" value="" />
					</div>
				</div>
				<div class="form-group" style="width:300px;float:left;">
					<label class="col-md-3 control-label" for="amount">Dátum</label>
					<div class="col-md-9">
						<input type="text" id="rdate" name="rdate" class="form-control input-datepicker" value="<?=date("Y-m-d",strtotime("NOW"));?>" />
					</div>
				</div>
				
				<div class="form-group" style="width:300px;float:left;">
					<label class="col-md-3 control-label" for="store">Poszt</label>
					<div class="col-md-9">
						<select id="munkahely" name="munkahely" class="form-control" >
							<option value="0">Kérlek válassz</option>
							<?
								for($i=0;$i<count($munkahelyek);$i++){
									echo"<option value=\"".$munkahelyek[$i]['id']."\">".$munkahelyek[$i]['name']."</option>";	
								}
							?>
						</select>
					</div>
				</div>
				<div class="col-md-9 col-md-offset-0">					
					<button type="button" class="btn btn-sm btn-primary" id="savenewbutton" onclick="javascript:addissuelement();"><i class="fa fa-angle-right"></i> Felvesz</button>
				</div>
				<div style="clear:both;"></div>
			</form>	
			<div style="clear:both;height:10px;"></div>

		<!-- Datatables Content -->
		<div class="block full">
			<div class="block-title">
				<h2><strong>Alapanyagok</strong> listája</h2>
			</div>
			
			<div >
				<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
					<thead>
						<tr>
						<th>Kiadási sorszám</th>
						<th>Termék név</th>
						<th>Mennyiség</th>
						<th>LOT</th>
						<th>MMI</th>
						<th>Dátum</th>
						<th>Munkahely</th>
						<th>Raktár</th>
						<th>Típus</th>
						</tr>
					</thead>
					<tbody ></tbody>
				</table>
			</div>
		</div>

		<div style='clear:both;height:1px;' ></div>

		</div>	
		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>
		<script src="/modules/maintenance/js/inc_cleaner_issue.js"></script>

		<script>$(function(){ cleaner_issueDatatables.init(); });</script>
		<?php include 'inc/template_end.php'; ?>	

<?
}
?>
