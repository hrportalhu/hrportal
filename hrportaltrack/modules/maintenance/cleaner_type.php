
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Tisztítószer típusok
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/maintenance/cleaner_type">Tisztítószer típus kezelő</a></li>
				<li><a href="/new">Új típusok</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új típusok</strong></h2>
				</div>
			<?
				$kats=db_all("select * from cleaner_categories where status=1 order by name asc");
				
				echo "<form id=\"newcleaner_type\" name=\"newcleaner_type\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";
							
						
			
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"name\">Tisztítószer elnevezése:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" placeholder=\"tisztítószer elnevezése\" />
							</div>
						</div>";
			
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"tcode\">Tisztítószer kódja:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"tcode\" name=\"tcode\" class=\"form-control\" placeholder=\"Tisztítószer kódja\" />
							</div>
						</div>";
						echo "<div class=\"form-group\">
								<label class=\"col-md-6 control-label\" for=\"cc_id\">Csoport:</label>
								<div class=\"col-md-6\">
									<select id=\"cc_id\" name=\"cc_id\" class=\"form-control\" >
									<option value=\"0\">Kérlek válassz</option>";
									for($i=0;$i<count($kats);$i++){
										
										echo"<option value=\"".$kats[$i]['id']."\" ".$sel.">".$kats[$i]['name']."</option>";	
									}
									echo"</select>
								</div>
							</div>";
					echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"kiszereles\">Kiszerelés:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"kiszereles\" name=\"kiszereles\" class=\"form-control\" placeholder=\"Kiszerelés\" />
							</div>
						</div>";
								echo "<div class=\"form-group\">
									<label class=\"col-md-6 control-label\" for=\"max_stock\">Maximális készlet:</label>
									<div class=\"col-md-6\">
										<input type=\"text\" id=\"max_stock\" name=\"max_stock\" class=\"form-control\" value=\"\" />
									</div>
								</div>";	
																
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-6 control-label\" for=\"min_stock\">Minimális készlet:</label>
									<div class=\"col-md-6\">
										<input type=\"text\" id=\"min_stock\" name=\"min_stock\" class=\"form-control\" value=\"\" />
									</div>
								</div>";

						echo "<div class=\"form-group\" >
							<label class=\"col-md-6 control-label\" for=\"status\">Érvényes-e:</label>
							<label class=\"col-md-6 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span></label>
						</div>";
						
						echo "<div style=\"clear:both;height:10px;\"></div>";
						
						echo "<div class=\"form-group form-actions\">
							<div class=\"col-md-6 col-md-offset-3\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewcleaner_type();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>";
						
				echo "</div>
				</form>";
			?>
			</div>	
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/maintenance/js/inc_cleaner_type.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?	
		}
		else{

			//echo "select * from functions where id='".$uritags[2]."'";
			$mycleaner_type=db_row("select * from cleaner_type where id='".$uritags[3]."'");
			if($mycleaner_type['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
				
?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="modules/maintenance/cleaner_type"> Tisztítószer </a></li>
				<li><a href="/<?=$URI;?>"><?=$mycleaner_type['name'];?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->

					<div class="block-title">
						<h2><strong><?=$mycleaner_type['name'];?> </strong></h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
							if(priv("cleaner_type","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("cleaner_type","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							if(priv("cleaner_type","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("cleaner_type","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
							if(priv("cleaner_type","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}	
							?>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="col-md-6">
								<dl class="dl-horizontal">
									<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$mycleaner_type['id']); ?></dd>
									<dt>Elnevezés:</dt><dd><? echo $mycleaner_type['name']; ?></dd>
									<dt>KÓD:</dt><dd><? echo $mycleaner_type['tcode']; ?></dd>
									<dt>Kategória:</dt><dd><? echo idchange("cleaner_categories","name",$mycleaner_type['cc_id']); ?></dd>
									<dt>Kiszerelés:</dt><dd><? echo $mycleaner_type['kiszereles']; ?></dd>
									<dt>Maximális készlet:</dt><dd><? echo $mycleaner_type['max_stock']; ?></dd>
									<dt>Minimális készlet:</dt><dd><? echo $mycleaner_type['min_stock']; ?></dd>
									<dt>Heti fogyás:</dt><dd><? echo $mycleaner_type['hetifogyas']; ?></dd>
									<dt>Aktuális készlet:</dt><dd><? echo $mycleaner_type['units']; ?></dd>
								
									<dt>Státusz:</dt><dd><? if($mycleaner_type['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
								</dl>	
							</div>
							<div class="col-md-6"></div>
							<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-2">
						<?
							$kats=db_all("select * from cleaner_categories where status=1 order by name asc");
							
							echo"<form id=\"editcleaner_type\" name=\"editcleaner_type\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
							echo "<div style=\"margin-top:1px;\">";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-6 control-label\" for=\"name\">Tisztítószer elnevezése:</label>
									<div class=\"col-md-6\">
										<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" value=\"".$mycleaner_type['name']."\" />
									</div>
								</div>";
								
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-6 control-label\" for=\"tcode\">Tisztítószer Kódja:</label>
									<div class=\"col-md-6\">
										<input type=\"text\" id=\"tcode\" name=\"tcode\" class=\"form-control\" value=\"".$mycleaner_type['tcode']."\" />
									</div>
								</div>";
								
							echo "<div class=\"form-group\">
								<label class=\"col-md-6 control-label\" for=\"cc_id\">Csoport:</label>
								<div class=\"col-md-6\">
									<select id=\"cc_id\" name=\"cc_id\" class=\"form-control\" >
									<option value=\"0\">Kérlek válassz</option>";
									for($i=0;$i<count($kats);$i++){
										$sel="";if($mycleaner_type['cc_id']==$kats[$i]['id']){$sel=" selected ";}
										echo"<option value=\"".$kats[$i]['id']."\" ".$sel.">".$kats[$i]['name']."</option>";	
									}
									echo"</select>
								</div>
							</div>";

								echo "<div class=\"form-group\">
									<label class=\"col-md-6 control-label\" for=\"kiszereles\">Kiszerelés:</label>
									<div class=\"col-md-6\">
										<input type=\"text\" id=\"kiszereles\" name=\"kiszereles\" class=\"form-control\" value=\"".$mycleaner_type['kiszereles']."\" />
									</div>
								</div>";	
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-6 control-label\" for=\"max_stock\">Maximális készlet:</label>
									<div class=\"col-md-6\">
										<input type=\"text\" id=\"max_stock\" name=\"max_stock\" class=\"form-control\" value=\"".$mycleaner_type['max_stock']."\" />
									</div>
								</div>";	
																
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-6 control-label\" for=\"min_stock\">Minimális készlet:</label>
									<div class=\"col-md-6\">
										<input type=\"text\" id=\"min_stock\" name=\"min_stock\" class=\"form-control\" value=\"".$mycleaner_type['min_stock']."\" />
									</div>
								</div>";	
																
								echo "<div class=\"form-group\" >
									<label class=\"col-md-6 control-label\" for=\"status\">Érvényes-e:</label>";
									$st="";if($mycleaner_type['status']==1){ $st=" checked "; }
									echo "<label class=\"col-md-6 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\"".$st." ><span></span></label>
								</div>";
								
								echo "<div class=\"form-group form-actions\">
									<div class=\"col-md-6 col-md-offset-3\">
										<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditcleaner_type(".$mycleaner_type['id'].")\"><i class=\"fa fa-angle-right\"></i> Ment</button>
									</div>
								</div>";
									
							echo "</div>
							</form>";
						?>
						</div>

						<div class="tab-pane" id="tab-3">
								<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mycleaner_type['id'],"Dokumentum feltöltés")."</div>";
								?> 
								<div id="filelists"></div>
								<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mycleaner_type['id'],"Hozzászólások")."</div>";
								?>
						</div>
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>
				
					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
					
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/maintenance/js/inc_cleaner_type.js"></script>

			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mycleaner_type['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$mycleaner_type['id'];?>,'Dokumentum feltöltés','filelists');
			</script>
			<script>$(function(){ cleaner_typeDatatables.init(); });</script>	

			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
		//print_r($_SESSION['planetsys'])
?>    
<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Tisztítószer típusok</a></li>
			</ul>
			<!-- END Datatables Header -->
		
				
				
			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Tisztítószer típus</strong> lista</h2>
				</div>
				
				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							//echo"<th>Művelet</th>";
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/maintenance/js/inc_cleaner_type.js"></script>
		<script>$(function(){ cleaner_typeDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>

