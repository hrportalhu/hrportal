/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */
var cleaner_bevetelezesDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();
            

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/maintenance/cleaner_bevetelezes/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"bDestroy": true,
				"aaSorting": [[ 0, "desc" ]],
				"iDisplayLength": 30,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/maintenance/ajax_cleaner_bevetelezes_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					aoData.push({"name": "mpartner_id", "value":$("#mpartner_id").val() });
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });

            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveeditcleaner_bevetelezes(elemid){
	var valid = f_c($('#editcleaner_bevetelezes'));
	if(valid){
		f_xml(ServerName +'/modules/maintenance/ajax_cleaner_bevetelezes_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editcleaner_bevetelezes').serialize(),'');
	}
}

function closeeditcleaner_bevetelezes(elemid){
	
		f_xml(ServerName +'/modules/maintenance/ajax_cleaner_bevetelezes_data.php','view=edit&id=' + elemid +'&closesubmit='+elemid,'');
	
}

function savenewcleaner_bevetelezes(){
	var valid = f_c($('#newcleaner_bevetelezes'));
	if(valid){
		f_xml(ServerName +'/modules/maintenance/ajax_cleaner_bevetelezes_data.php','newsubmit=1&' + $('#newcleaner_bevetelezes').serialize(),'');
	}
}

function savebevcostcont(editid,elemid){
	f_xml(ServerName +'/modules/maintenance/ajax_cleaner_bevetelezes_data.php','savebevcostcont=1&editid='+editid+'&elemid='+elemid+'&priceem='+$('#priceem_'+editid).val(),'');
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/maintenance/ajax_cleaner_bevetelezes_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}

function addbevelement(elemid,targetdiv){
	if($('#agt_id').val()!="" && $('#agt_myid').val()!="" && $('#mennyiseg').val()!="" &&  $('#mystore').val()!="0"){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/maintenance/ajax_addbevelement.php',
		data:'newsubmit=1&elemid='+elemid+'&'+$('#newelementform').serialize(),
		success: function(data){
			
			showbevelement(elemid,targetdiv);
			$('#agt_id').val('');
			$('#mennyiseg').val('');
			$('#nettoar').val('');
			$('#homerseklet').val('');
			$('#lot').val('');
		}});
	}
	else{
		alert("Üres tételt nem lehet betölteni!");
	}
}

function delbevetelemet(elemid,headerid){
	if(confirm("Biztos törlöd ezt a tételt a bevételezésből")){	
		$.ajax({
			type:'POST',
			url: ServerName +'/modules/maintenance/ajax_delbevetelemet.php',
			data:'delsubmit=1&elemid='+elemid,
			success: function(data){
				document.location='/modules/maintenance/cleaner_bevetelezes/'+headerid;
			}});
		}
}

function showbevelement(elemid,targetdiv){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/maintenance/ajax_showbevelement.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#'+targetdiv).html(data);
		}});
}

function getspecelem(){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/maintenance/ajax_getspecelem.php',
		data:'elemid='+ $("#agt_id").val(),
		success: function(data){
			$('#myspec').html(data);
		}});	
}

function bevcostcont(elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/maintenance/ajax_bevcostcont.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#bevcostcontdiv').html(data);
		}});	
}




$("#agt_id").autocomplete({
	source: ServerName+"/modules/maintenance/ajax_alapanyag_term.php",
	minLength: 0,select: 
	function(event, ui) {
		$("#agt_id").val(ui.item.value);
		$("#agt_myid").val(ui.item.id);
	}
});
