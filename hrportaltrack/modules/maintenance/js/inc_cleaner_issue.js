/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */
var cleaner_issueDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/maintenance/cleaner_issue/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"bDestroy": true,
				"aaSorting": [[ 0, "desc" ]],
				"iDisplayLength": 50,
                "aLengthMenu": [[10, 20, 50, -1], [10, 20, 50, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/maintenance/ajax_cleaner_issue_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });



            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveeditscreenworkerss(elemid){
	var valid = f_c($('#editworkers'));
	if(valid){
		f_xml(ServerName +'/modules/maintenance/ajax_cleaner_issue_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editworkers').serialize(),'');
	}
}

function savenewscreenworkers(){
	var valid = f_c($('#newworkers'));
	if(valid){
		f_xml(ServerName +'/modules/maintenance/ajax_cleaner_issue_data.php','newsubmit=1&' + $('#newworkers').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/maintenance/ajax_cleaner_issue_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}

function addissuelement() {
		if($('#newissueitem').val()!=''){
			
		var myni=$('#newissueitem').val();
		
		f_xml(ServerName +'/modules/maintenance/ajax_addissuelement.php','view=edit&newsubmit=1&' +
			'&newitem=' + myni +
			'&amount=' + $('#amount').val() +
			'&munkahely=' + $('#munkahely').val() +
			'&tetel=' + $('#tetelissue').val() +
			'&rdate=' + $('#rdate').val(),
			'newitemlist');
		}
		$('#newissueitem').val('');
		$('#amount').val('');
		$('#tetelissue').html('<option value="0">Kérlek válassz</option>');
}

function editcleaner_issuelement(){
		f_xml(ServerName +'/modules/maintenance/ajax_goods_cleaner_issue.php','view=edit&editsubmit=1' +
			'&amount=' + $('#amount').val() +
			'&munkahely=' + $('#munkahely').val() +
			'&tetel=' + $('#tetel').val() +
			'&aid=' + $('#aid').val() +
			'&rdate=' + $('#rdate').val(),
			'newitemlist');
		
		$('#newitem').val('');
		$('#amount').val('');
		$('#tetel').html('<option value="0">Kérlek válassz</option>');
}

function backcleaner_issuelement(){
	alert($('#maxmennyi').val()+'>'+$('#amount2').val());
		if(parseFloat(ds($('#maxmennyi').val())) > parseFloat(ds($('#amount2').val()))){
			if(confirm("Biztos vissza szeretnéd venni?")){
				f_xml(ServerName +'/modules/maintenance/ajax_goods_cleaner_issue.php','view=edit&backsubmit=1' +
					'&amount=' + $('#amount2').val() +
					'&munkahely=' + $('#munkahely2').val() +
					'&store=' + $('#store2').val() +
					'&tetel=' + $('#tetel2').val() +
					'&aid=' + $('#aid2').val() +
					'&rdate=' + $('#rdate2').val(),
					'newitemlist');
				
				$('#newitem').val('');
				$('#amount').val('');
				$('#tetel').html('<option value="0">Kérlek válassz</option>');
			}
		}
		
}

function delcleaner_issuelement(elemid){
	f_xml(ServerName +'/modules/maintenance/ajax_goods_cleaner_issue.php','del='+elemid,'newitemlist');
}

$("#newissueitem").autocomplete({
	 source: ServerName+"/modules/maintenance/ajax_alapanyag_term.php",
	 minLength: 0,
	 select: function(event, ui) {
			$("#newissueitem").val(ui.item.value);
			getkiadlotissue(ui.item.id);
		}
});

function getkiadlotissue(elemid){
		$.ajax({
		type:'POST',
		url: ServerName +'/modules/maintenance/ajax_getkiadlotissue.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#tetelissue').html(data);
		}});
	
}

function savecleaner_issue(){
	$('#newitem').val('');
		$('#amount').val('');
		if(confirm('Biztosan rögzíti?')){
			f_xml(ServerName +'/modules/maintenance/ajax_goods_cleaner_issue.php','finalsubmit=1','newitemlist');
		}
}

function showcleaner_issueelement(targetdiv){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/maintenance/ajax_goods_cleaner_issue_show.php',
		data:'elemid=1',
		success: function(data){
			$('#'+targetdiv).html(data);
		}});
}
