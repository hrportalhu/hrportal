
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Igazolások
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/medical/certificate">Igazolások</a></li>
				<li><a href="/new">Új igazolás rögzítése</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új Igazolás rögzítése</strong></h2>
				</div>
			<?
				$vt=db_all("select id, name from med_certificate_types where status=1 order by name");
				$pat=db_all("select id, name, birthdate,tajnum from med_patients where status=1 order by name");
				
				echo "<form id=\"newcertificate\" name=\"newcertificate\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";
				
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"patient_id\">Vizsgált páciens:</label>
						<div class=\"col-md-9\">
							<select id=\"patient_id\" name=\"patient_id\" class=\"form-control\" >
							<option value=\"0\">Kérlek válassz</option>";
							for($i=0;$i<count($pat);$i++){
								$sel="";if($uritags[4]==$pat[$i]['id']){$sel=" selected ";}
								echo"<option value=\"".$pat[$i]['id']."\" ".$sel.">".$pat[$i]['name']." (".$pat[$i]['birthdate']." - TAJ: ".$pat[$i]['tajnum'].") </option>";	
								
							}
							echo"</select>
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"certificate_date\">Igazolási dátum:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"certificate_date\" name=\"certificate_date\" class=\"form-control input-datepicker\" value=\"".date("Y-m-d",time())."\" />
						</div>
					</div>";
					
					
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"certtype_id\">Igazolás típusa:</label>
						<div class=\"col-md-9\">
							<select id=\"certtype_id\" name=\"certtype_id\" class=\"form-control\" onchange=\"javascript:getcertificatetext();\">
							<option value=\"0\">Kérlek válassz</option>";
							for($i=0;$i<count($vt);$i++){
								echo"<option value=\"".$vt[$i]['id']."\" >".$vt[$i]['name']."</option>";	
							}
							echo"</select>
						</div>
					</div>";
						echo" <div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"name\">Igazolás tartalma</label>
						<div class=\"col-md-9\">
							<textarea id=\"name\" name=\"name\" rows=\"6\" class=\"form-control\" placeholder=\"Igazolás tartalma\"></textarea>
						</div>
					</div>";	
						echo "<div class=\"form-group form-actions\">
								<div class=\"col-md-9\">
									<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewcertificate();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
								</div>
							</div>";
				echo "</div>
				</form>";
			?>
			</div>	
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/medical/js/inc_certificate.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?
		}
		else{
			
			//echo "select * from functions where id='".$uritags[2]."'";
			$mycertificate=db_row("select * from med_certificate where id='".$uritags[3]."'");
			if($mycertificate['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
				$insdata=db_row("select * from med_certificate where ih_id='".$mycertificate['id']."'");
				$vt=db_all("select id, name from med_certificate_types where status=1 order by name");
				$pat=db_all("select id, name, birthdate,tajnum from med_patients where status=1 order by name");
				$mp=db_row("select * from med_patients where id='".$mycertificate['patient_id']."'");
				$diag=db_row("select * from med_diagnosis where id='".$insdata['diag_id']."'");
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="modules/medical/certificate">Igazolás</a></li>
				<li><a href="/<?=$URI;?>"><?=$mycertificate['certificate_date'];?> - <?= idchange("med_certificate_types","name",$mycertificate['certificate_type']);?> Igazolás</a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->
					<div class="block-title">
						<h2><?=$mycertificate['certificate_date'];?> - <?= idchange("med_certificate_types","name",$mycertificate['certificate_type']);?> Igazolás</h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
							if(priv("certificate","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("certificate","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							if(priv("certificate","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("certificate","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
							//if(priv("certificate","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}	
							if(priv("certificate","print")){echo"<li onclick=\"javascript:getprintscreen('".$_SESSION['planetsys']['actpage']['modulname']."','certificateprint','".$mycertificate['id']."');\" ><a href=\"#modal-regular-print\" data-toggle=\"modal\" title=\"Spec print\"><i class=\"gi gi-print\" ></i></a></li>";}
							?>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="col-md-6">
								<dl class="dl-horizontal">
									<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$mycertificate['id']); ?></dd>
									<dt>Név:</dt><dd><? echo $mp['name']; ?></dd>
									<dt>Születési neve:</dt><dd><? echo $mp['birthname']; ?></dd>
									<dt>Születési dátum:</dt><dd><? echo $mp['birthdate'];; ?></dd>
									<dt>Édesanyja neve:</dt><dd><? echo $mp['mothername']; ?></dd>
									<dt>TAJ szám:</dt><dd><? echo $mp['tajnum']; ?></dd>
									<dt>Telefonszáma:</dt><dd><? echo $mp['phone']; ?></dd>
									<dt>Címe:</dt><dd><? echo $mp['address']; ?></dd>
									
								</dl>	
	
	
							</div>
							<div class="col-md-6">
								<h3>Igazolás tartalma:</h3>
								<p><?=nl2br($mycertificate['name'])?></p>
							</div>
							
							<div style="clear:both;height:1px;"></div>


						</div>
						<div class="tab-pane" id="tab-2">
						<?
							echo"<form id=\"editcertificate\" name=\"editcertificate\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
							echo "<div style=\"margin-top:1px;\">";
				
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"patient_id\">Vizsgált páciens:</label>
						<div class=\"col-md-9\">
							<select id=\"patient_id\" name=\"patient_id\" class=\"form-control\" >
							<option value=\"0\">Kérlek válassz</option>";
							for($i=0;$i<count($pat);$i++){
								$sel="";if($mycertificate['patient_id']==$pat[$i]['id']){$sel=" selected ";}
								echo"<option value=\"".$pat[$i]['id']."\" ".$sel.">".$pat[$i]['name']." (".$pat[$i]['birthdate']." - TAJ: ".$pat[$i]['tajnum'].") </option>";	
							}
							echo"</select>
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"certificate_date\">Igazolási dátum:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"certificate_date\" name=\"certificate_date\" class=\"form-control input-datepicker\" value=\"".$mycertificate['certificate_date']."\" />
						</div>
					</div>";
					
					
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"certtype_id\">Igazolás típusa:</label>
						<div class=\"col-md-9\">
							<select id=\"certtype_id\" name=\"certtype_id\" class=\"form-control\" >
							<option value=\"0\">Kérlek válassz</option>";
							for($i=0;$i<count($vt);$i++){
								$sel="";if($mycertificate['certtype_id']==$vt[$i]['id']){$sel=" selected ";}
								echo"<option value=\"".$vt[$i]['id']."\" ".$sel." >".$vt[$i]['name']."</option>";	
							}
							echo"</select>
						</div>
					</div>";
					echo" <div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"name\">Igazolás tartalma</label>
						<div class=\"col-md-9\">
							<textarea id=\"name\" name=\"name\" rows=\"6\" class=\"form-control\" >".$mycertificate['name']."</textarea>
						</div>
					</div>";	
						echo "<div class=\"form-group form-actions\">
								<div class=\"col-md-9\">
									<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditcertificate('".$mycertificate['id']."');\"><i class=\"fa fa-angle-right\"></i> Ment</button>
								</div>
							</div>";
				echo "</div>
							</form>";
						?>
						</div>
						<div class="tab-pane" id="tab-3">
								<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mycertificate['id'],"Dokumentum feltöltés")."</div>";
								?> 
								<div id="filelists"></div>
								<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mycertificate['id'],"Hozzászólások")."</div>";
								?>
						</div>
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>
						
						
				
					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
					
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/medical/js/inc_certificate.js"></script>

			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mycertificate['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$mycertificate['id'];?>,'Dokumentum feltöltés','filelists');
			
			</script>
			<script>$(function(){ certificateDatatables.init(); });</script>	
			
			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
		//print_r($_SESSION['planetsys'])
?>    
	<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Igazolás lista</a></li>
			</ul>
			<!-- END Datatables Header -->

			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Igazolás</strong> lista</h2>
				</div>

				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							//echo"<th>Művelet</th>";
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/medical/js/inc_certificate.js"></script>
		<script>$(function(){ certificateDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>
