<?
	echo "<div class=\"form-group\">
		<label class=\"col-md-3 control-label\" for=\"diag_id\">Kimenő diagnózis:</label>
		<div class=\"col-md-9\">
			<input type=\"text\" id=\"diag_id\" name=\"diag_id\" class=\"form-control\" placeholder=\"Kimenő diagnózis\" />																		
			<input type=\"hidden\" id=\"diag_idid\" name=\"diag_idid\"  />																		
		</div>
	</div>";

	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"utmensz\">Utolsó menzesz:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"utmensz\" name=\"utmensz\" class=\"form-control input-datepicker\" value=\"".date("Y-m-d",time())."\" />
			</div>
		</div>";

	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"et1\">ET:</label>
			<div class=\"col-md-6\">
				<input type=\"text\" id=\"et1\" name=\"et1\" class=\"form-control\" value=\"\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">X</span>
				<input type=\"text\" id=\"et2\" name=\"et2\" class=\"form-control\" value=\"\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">mm</span>
			</div>
		</div>";

	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"pet1\">Petezsák nagysága:</label>
			<div class=\"col-md-6\">
				<input type=\"text\" id=\"pet1\" name=\"pet1\" class=\"form-control\" value=\"\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">X</span>
				<input type=\"text\" id=\"pet2\" name=\"pet2\" class=\"form-control\" value=\"\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">mm</span>
			</div>
		</div>";

	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"ys1\">YS:</label>
			<div class=\"col-md-6\">
				<input type=\"text\" id=\"ys1\" name=\"ys1\" class=\"form-control\" value=\"\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">X</span>
				<input type=\"text\" id=\"ys2\" name=\"ys2\" class=\"form-control\" value=\"\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">mm</span>
			</div>
		</div>";
	
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"crl\">CRL /mm/ :</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"crl\" name=\"crl\" class=\"form-control\" value=\"\" />
			</div>
		</div>";
	
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"bpd\">BPD /mm/ :</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"bpd\" name=\"bpd\" class=\"form-control\" value=\"\" />
			</div>
		</div>";
	
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"fronto\">Fronto /mm/ :</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"fronto\" name=\"fronto\" class=\"form-control\" value=\"\" />
			</div>
		</div>";
	
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"thorax\">Thorax /mm/ :</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"thorax\" name=\"thorax\" class=\"form-control\" value=\"\" />
			</div>
		</div>";
	
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"femur\">Femur /mm/:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"femur\" name=\"femur\" class=\"form-control\" value=\"\" />
			</div>
		</div>";

		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"szivmukodes\">Szívműködés:</label>
			<div class=\"col-md-9\">
				<select id=\"szivmukodes\" name=\"szivmukodes\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Látható</option>";
					echo"<option value=\"2\">Nem látható</option>";
				echo"</select>
			</div>
		</div>";

	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"fhr\">FHR /min/:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"fhr\" name=\"fhr\" class=\"form-control\" value=\"\" />
			</div>
		</div>";


		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"inact\">Intakt:</label>
			<div class=\"col-md-9\">
				<select id=\"inact\" name=\"inact\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Igen</option>";
					echo"<option value=\"2\">Nem</option>";
				echo"</select>
			</div>
		</div>";
	
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"terhido_het\">Terhességi idő:</label>
			<div class=\"col-md-6\">
				<input type=\"text\" id=\"terhido_het\" name=\"terhido_het\" class=\"form-control\" value=\"\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:30px;\">Hét</span>
				<input type=\"text\" id=\"terhido_nap\" name=\"terhido_nap\" class=\"form-control\" value=\"\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">nap</span>
			</div>
		</div>";
	
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"nuchaszures\">Nuchaszűrés:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"nuchaszures\" name=\"nuchaszures\" class=\"form-control\" value=\"\" />
			</div>
		</div>";
	
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"jobbovarium1\">Jobb Ovárium:</label>
			<div class=\"col-md-6\">
				<input type=\"text\" id=\"jobbovarium1\" name=\"jobbovarium1\" class=\"form-control\" value=\"\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">X</span>
				<input type=\"text\" id=\"jobbovarium2\" name=\"jobbovarium2\" class=\"form-control\" value=\"\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">mm</span>
			</div>
		</div>";
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"balovarium1\">Bal Ovárium:</label>
			<div class=\"col-md-6\">
				<input type=\"text\" id=\"balovarium1\" name=\"balovarium1\" class=\"form-control\" value=\"\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">X</span>
				<input type=\"text\" id=\"balovarium2\" name=\"balovarium2\" class=\"form-control\" value=\"\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">mm</span>
			</div>
		</div>";
	
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"hasurfolyadek\">Szabad hasüri folyadék:</label>
			<div class=\"col-md-9\">
				<select id=\"hasurfolyadek\" name=\"hasurfolyadek\" class=\"form-control\" style=\"width:200px;float:left;\">";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">látható</option>";
					echo"<option value=\"2\">nem látható</option>";
					echo"<option value=\"3\">kevés</option>";
					echo"<option value=\"4\">sok</option>";
				echo"</select>
				<input type=\"text\" id=\"hasurfolyadek_mm\" name=\"hasurfolyadek_mm\" class=\"form-control\" value=\"\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">mm</span>
			</div>
		</div>";
		
	echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"panaszok\">Panaszok</label>
				<div class=\"col-md-9\">
					<textarea id=\"panaszok\" name=\"panaszok\" rows=\"6\" class=\"form-control\" placeholder=\"Panaszok..\"></textarea>
				</div>
			</div>";	
		
	echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"velemeny\">Vélemény</label>
				<div class=\"col-md-9\">
					<textarea id=\"velemeny\" name=\"velemeny\" rows=\"6\" class=\"form-control\" placeholder=\"Vélemény..\"></textarea>
				</div>
			</div>";	
		
	echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"javaslat\">Javaslat</label>
				<div class=\"col-md-9\">
					<textarea id=\"javaslat\" name=\"javaslat\" rows=\"6\" class=\"form-control\" placeholder=\"Javaslat..\">Folsav, Omega 3 szedése</textarea>
				</div>
			</div>";	
		
	echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"megjegyzes\">Megjegyzés</label>
				<div class=\"col-md-9\">
					<textarea id=\"megjegyzes\" name=\"megjegyzes\" rows=\"6\" class=\"form-control\" placeholder=\"Megjegyzés..\">L1:\r\nH1:\r\nB1:\r\nL2:\r\nH2\r\nB2:</textarea>
				</div>
			</div>";	
	
	echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"egyeb\">Egyéb</label>
				<div class=\"col-md-9\">
					<textarea id=\"egyeb\" name=\"egyeb\" rows=\"6\" class=\"form-control\" >FMF Kód: 61527</textarea>
				</div>
			</div>";	
	
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"tappenz\">Táppénz:</label>
			<div class=\"col-md-9\">
				<select id=\"tappenz\" name=\"tappenz\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Tisztelt Kolléga! Kérem szíves táppénzes állományba vételét. Köszönettel:</option>";
					echo"<option value=\"2\">Tisztelt Kolléga! Kérem szíves további táppénzen tartását. Köszönettel:</option>";
				echo"</select>
			</div>
		</div>";
	
	echo "<div class=\"form-group form-actions\">
		<div class=\"col-md-9\">
			<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewinspection();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
		</div>
	</div>";
?>
