<?
	$el=db_row("select * from med_inspection_1 where ih_id='".$_REQUEST['elemid']."'");
	$diagdata=db_row("select * from med_diagnosis where id='".$el['diag_id']."'");
	
	echo "<div class=\"form-group\">
		<label class=\"col-md-3 control-label\" for=\"diag_id\">Kimenő diagnózis:</label>
		<div class=\"col-md-9\">
			<input type=\"text\" id=\"diag_id\" name=\"diag_id\" class=\"form-control\" value=\"".$diagdata['name']." - ".$diagdata['diagcode']."\" />	
			<input type=\"hidden\" id=\"diag_idid\" name=\"diag_idid\" value=\"".$el['diag_id']."\" />																		
		</div>
	</div>";

	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"utmensz\">Utolsó menzesz:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"utmensz\" name=\"utmensz\" class=\"form-control input-datepicker\" value=\"".$el['utmensz']."\" />
			</div>
		</div>";

	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"et1\">ET:</label>
			<div class=\"col-md-6\">
				<input type=\"text\" id=\"et1\" name=\"et1\" class=\"form-control\" value=\"".$el['et1']."\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">X</span>
				<input type=\"text\" id=\"et2\" name=\"et2\" class=\"form-control\" value=\"".$el['et2']."\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">mm</span>
			</div>
		</div>";

	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"pet1\">Petezsák nagysága:</label>
			<div class=\"col-md-6\">
				<input type=\"text\" id=\"pet1\" name=\"pet1\" class=\"form-control\" value=\"".$el['pet1']."\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">X</span>
				<input type=\"text\" id=\"pet2\" name=\"pet2\" class=\"form-control\" value=\"".$el['pet2']."\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">mm</span>
			</div>
		</div>";

	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"ys1\">YS:</label>
			<div class=\"col-md-6\">
				<input type=\"text\" id=\"ys1\" name=\"ys1\" class=\"form-control\" value=\"".$el['ys1']."\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">X</span>
				<input type=\"text\" id=\"ys2\" name=\"ys2\" class=\"form-control\" value=\"".$el['ys2']."\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">mm</span>
			</div>
		</div>";
	
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"crl\">CRL /mm/ :</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"crl\" name=\"crl\" class=\"form-control\" value=\"".$el['crl']."\" />
			</div>
		</div>";
	
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"bpd\">BPD /mm/ :</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"bpd\" name=\"bpd\" class=\"form-control\" value=\"".$el['bpd']."\" />
			</div>
		</div>";
	
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"fronto\">Fronto /mm/ :</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"fronto\" name=\"fronto\" class=\"form-control\" value=\"".$el['fronto']."\" />
			</div>
		</div>";
	
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"thorax\">Thorax /mm/ :</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"thorax\" name=\"thorax\" class=\"form-control\" value=\"".$el['thorax']."\" />
			</div>
		</div>";
	


		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"szivmukodes\">Szívműködés:</label>
			<div class=\"col-md-9\">
				<select id=\"szivmukodes\" name=\"szivmukodes\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\" ".($el['szivmukodes']=="1"?"selected":"").">Látható</option>";
					echo"<option value=\"2\" ".($el['szivmukodes']=="2"?"selected":"").">Nem látható</option>";
				echo"</select>
			</div>
		</div>";

	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"fhr\">FHR /min/:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"fhr\" name=\"fhr\" class=\"form-control\" value=\"".$el['thorax']."\" />
			</div>
		</div>";


		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"inact\">Intakt:</label>
			<div class=\"col-md-9\">
				<select id=\"inact\" name=\"inact\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\" ".($el['inact']=="1"?"selected":"").">Igen</option>";
					echo"<option value=\"2\" ".($el['inact']=="2"?"selected":"").">Nem</option>";
				echo"</select>
			</div>
		</div>";
	
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"terhido_het\">Terhességi idő:</label>
			<div class=\"col-md-6\">
				<input type=\"text\" id=\"terhido_het\" name=\"terhido_het\" class=\"form-control\" value=\"".$el['terhido_het']."\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:30px;\">Hét</span>
				<input type=\"text\" id=\"terhido_nap\" name=\"terhido_nap\" class=\"form-control\" value=\"".$el['terhido_nap']."\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">nap</span>
			</div>
		</div>";
	
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"nuchaszures\">Nuchaszürés:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"nuchaszures\" name=\"nuchaszures\" class=\"form-control\" value=\"".$el['nuchaszures']."\" />
			</div>
		</div>";
	
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"jobbovarium1\">Jobb Ovárium:</label>
			<div class=\"col-md-6\">
				<input type=\"text\" id=\"jobbovarium1\" name=\"jobbovarium1\" class=\"form-control\" value=\"".$el['jobbovarium1']."\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">X</span>
				<input type=\"text\" id=\"jobbovarium2\" name=\"jobbovarium2\" class=\"form-control\" value=\"".$el['jobbovarium2']."\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">mm</span>
			</div>
		</div>";
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"balovarium1\">Bal Ovárium:</label>
			<div class=\"col-md-6\">
				<input type=\"text\" id=\"balovarium1\" name=\"balovarium1\" class=\"form-control\" value=\"".$el['balovarium1']."\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">X</span>
				<input type=\"text\" id=\"balovarium2\" name=\"balovarium2\" class=\"form-control\" value=\"".$el['balovarium1']."\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">mm</span>
			</div>
		</div>";
	
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"hasurfolyadek\">Szabad hasüri folyadék:</label>
			<div class=\"col-md-9\">
				<select id=\"hasurfolyadek\" name=\"hasurfolyadek\" class=\"form-control\" style=\"width:200px;float:left;\">";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\" ".($el['hasurfolyadek']=="1"?"selected":"").">látható</option>";
					echo"<option value=\"2\" ".($el['hasurfolyadek']=="2"?"selected":"").">nem látható</option>";
					echo"<option value=\"3\" ".($el['hasurfolyadek']=="3"?"selected":"").">kevés</option>";
					echo"<option value=\"4\" ".($el['hasurfolyadek']=="4"?"selected":"").">sok</option>";
				echo"</select>
				<input type=\"text\" id=\"hasurfolyadek_mm\" name=\"hasurfolyadek_mm\" class=\"form-control\" value=\"\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">mm</span>
			</div>
		</div>";
		
	echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"panaszok\">Panaszok</label>
				<div class=\"col-md-9\">
					<textarea id=\"panaszok\" name=\"panaszok\" rows=\"6\" class=\"form-control\" >".$el['panaszok']."</textarea>
				</div>
			</div>";	
		
	echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"velemeny\">Vélemény</label>
				<div class=\"col-md-9\">
					<textarea id=\"velemeny\" name=\"velemeny\" rows=\"6\" class=\"form-control\" >".$el['velemeny']."</textarea>
				</div>
			</div>";	
		
	echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"javaslat\">Javaslat</label>
				<div class=\"col-md-9\">
					<textarea id=\"javaslat\" name=\"javaslat\" rows=\"6\" class=\"form-control\">".$el['javaslat']."</textarea>
				</div>
			</div>";	
		
	echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"megjegyzes\">Megjegyzés</label>
				<div class=\"col-md-9\">
					<textarea id=\"megjegyzes\" name=\"megjegyzes\" rows=\"6\" class=\"form-control\">".$el['megjegyzes']."</textarea>
				</div>
			</div>";	
	
	echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"egyeb\">Egyéb</label>
				<div class=\"col-md-9\">
					<textarea id=\"egyeb\" name=\"egyeb\" rows=\"6\" class=\"form-control\" >".$el['egyeb']."</textarea>
				</div>
			</div>";	
	
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"tappenz\">Táppénz:</label>
			<div class=\"col-md-9\">
				<select id=\"tappenz\" name=\"tappenz\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\" ".($el['tappenz']=="1"?"selected":"").">Tisztelt Kolléga! Kérem szíves táppénzes állományba vételét. Köszönettel:</option>";
					echo"<option value=\"2\" ".($el['tappenz']=="2"?"selected":"").">Tisztelt Kolléga! Kérem szíves további táppénzen tartását. Köszönettel:</option>";
				echo"</select>
			</div>
		</div>";
	
	echo "<div class=\"form-group form-actions\">
		<div class=\"col-md-9\">
			<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditinspection('".$_REQUEST['elemid']."');\"><i class=\"fa fa-angle-right\"></i> Ment</button>
		</div>
	</div>";
?>
