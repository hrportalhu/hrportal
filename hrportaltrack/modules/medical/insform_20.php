<?
echo "<div class=\"form-group\">
		<label class=\"col-md-3 control-label\" for=\"diag_id\">Kimenő diagnózis:</label>
		<div class=\"col-md-9\">
			<input type=\"text\" id=\"diag_id\" name=\"diag_id\" class=\"form-control\" placeholder=\"Kimenő diagnózis\" />	
			<input type=\"hidden\" id=\"diag_idid\" name=\"diag_idid\"  />																		
		</div>
	</div>";
	
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"uterus_1\">Uterus:</label>
			<div class=\"col-md-6\">
				<input type=\"text\" id=\"et1\" name=\"uterus_1\" class=\"form-control\" value=\"\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">X</span>
				<input type=\"text\" id=\"et2\" name=\"uterus_2\" class=\"form-control\" value=\"\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">X</span>
				<input type=\"text\" id=\"et3\" name=\"uterus_3\" class=\"form-control\" value=\"\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">mm</span>
			</div>
		</div>";
	echo "<div class=\"form-group\">
		<label class=\"col-md-3 control-label\" for=\"uterus_comment\">Uterus megjegyzés:</label>
		<div class=\"col-md-9\">
			<input type=\"text\" id=\"uterus_comment\" name=\"uterus_comment\" class=\"form-control\" placeholder=\"Uterus megjegyzés\" />																		
		</div>
	</div>";
	echo "<div class=\"form-group\">
		<label class=\"col-md-3 control-label\" for=\"cervix\">Cervix /mm/ :</label>
		<div class=\"col-md-9\">
			<input type=\"text\" id=\"cervix\" name=\"cervix\" class=\"form-control\" placeholder=\"\" />																		
		</div>
	</div>";
	echo "<div class=\"form-group\">
		<label class=\"col-md-3 control-label\" for=\"cervix_comment\">Cervix megjegyzés :</label>
		<div class=\"col-md-9\">
			<input type=\"text\" id=\"cervix_comment\" name=\"cervix_comment\" class=\"form-control\" placeholder=\"Cervix megjegyzés\" />																		
		</div>
	</div>";
	echo "<div class=\"form-group\">
		<label class=\"col-md-3 control-label\" for=\"endemetrium\">Endemetrium /mm/ :</label>
		<div class=\"col-md-9\">
			<input type=\"text\" id=\"endemetrium\" name=\"endemetrium\" class=\"form-control\" placeholder=\"\" />																		
		</div>
	</div>";
	echo "<div class=\"form-group\">
		<label class=\"col-md-3 control-label\" for=\"endemetrium_comment\">Endemetrium megjegyzés :</label>
		<div class=\"col-md-9\">
			<input type=\"text\" id=\"endemetrium_comment\" name=\"endemetrium_comment\" class=\"form-control\" placeholder=\"Endemetrium megjegyzés\" />																		
		</div>
	</div>";
	
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"jobbovarium1\">Jobb Ovárium:</label>
			<div class=\"col-md-6\">
				<input type=\"text\" id=\"jobbovarium1\" name=\"jobbovarium1\" class=\"form-control\" value=\"\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">X</span>
				<input type=\"text\" id=\"jobbovarium2\" name=\"jobbovarium2\" class=\"form-control\" value=\"\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">mm</span>
			</div>
		</div>";
		echo "<div class=\"form-group\">
		<label class=\"col-md-3 control-label\" for=\"jobbovarium_comment\">Jobb ovárium megjegyzés :</label>
		<div class=\"col-md-9\">
			<input type=\"text\" id=\"jobbovarium_comment\" name=\"jobbovarium_comment\" class=\"form-control\" placeholder=\"Jobb ovárium megjegyzés\" />																		
		</div>
	</div>";
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"balovarium1\">Bal Ovárium:</label>
			<div class=\"col-md-6\">
				<input type=\"text\" id=\"balovarium1\" name=\"balovarium1\" class=\"form-control\" value=\"\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">X</span>
				<input type=\"text\" id=\"balovarium2\" name=\"balovarium2\" class=\"form-control\" value=\"\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">mm</span>
			</div>
		</div>";
		
		echo "<div class=\"form-group\">
		<label class=\"col-md-3 control-label\" for=\"balovarium_comment\">Bal ovárium megjegyzés :</label>
		<div class=\"col-md-9\">
			<input type=\"text\" id=\"balovarium_comment\" name=\"balovarium_comment\" class=\"form-control\" placeholder=\"Bal ovárium megjegyzés\" />																		
		</div>
	</div>";
		
	
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"hasurfolyadek\">Szabad hasüri folyadék:</label>
			<div class=\"col-md-9\">
				<select id=\"hasurfolyadek\" name=\"hasurfolyadek\" class=\"form-control\" style=\"width:200px;float:left;\">";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Nem látható</option>";
					echo"<option value=\"2\">Kevés</option>";
					echo"<option value=\"3\">Sok</option>";
				echo"</select>
				<input type=\"text\" id=\"hasurfolyadek_mm\" name=\"hasurfolyadek_mm\" class=\"form-control\" value=\"\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">mm</span>
			</div>
		</div>";
			
			echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"kismedence\">Kismedencei lelet</label>
				<div class=\"col-md-9\">
					<textarea id=\"kismedence\" name=\"kismedence\" rows=\"6\" class=\"form-control\" placeholder=\"Kismedencei lelet..\"></textarea>
				</div>
			</div>";	
			
			echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"panaszok\">Panaszok</label>
				<div class=\"col-md-9\">
					<textarea id=\"panaszok\" name=\"panaszok\" rows=\"6\" class=\"form-control\" placeholder=\"Panaszok..\"></textarea>
				</div>
			</div>";	
		
	echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"velemeny\">Vélemény</label>
				<div class=\"col-md-9\">
					<textarea id=\"velemeny\" name=\"velemeny\" rows=\"6\" class=\"form-control\" placeholder=\"Vélemény..\"></textarea>
				</div>
			</div>";	
		
	echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"javaslat\">Javaslat</label>
				<div class=\"col-md-9\">
					<textarea id=\"javaslat\" name=\"javaslat\" rows=\"6\" class=\"form-control\" placeholder=\"Javaslat..\"></textarea>
				</div>
			</div>";	
		
	echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"megjegyzes\">Megjegyzés</label>
				<div class=\"col-md-9\">
					<textarea id=\"megjegyzes\" name=\"megjegyzes\" rows=\"6\" class=\"form-control\" placeholder=\"Megjegyzés..\"></textarea>
				</div>
			</div>";	
	
	echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"egyeb\">Egyéb</label>
				<div class=\"col-md-9\">
					<textarea id=\"egyeb\" name=\"egyeb\" rows=\"6\" class=\"form-control\" ></textarea>
				</div>
			</div>";		
			
	echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"holyagnyak\">Hólyagnyak-symphozis távolság mérés </label>
				<div class=\"col-md-9\">
					<textarea id=\"holyagnyak\" name=\"holyagnyak\" rows=\"6\" class=\"form-control\" >L1:\r\nH1:\r\nB1:\r\nL2:\r\nH2\r\nB2:\r\ntestsúly/kg.:</textarea>
				</div>
			</div>";		
	echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"hsg\">HSG:</label>
				<div class=\"col-md-9\">
					<textarea id=\"hsg\" name=\"hsg\" rows=\"6\" class=\"form-control\" >Kellő előkészületek után hüvelyi feltárásban a méhszályon keresztül katétert applikálunk az uterusba, majd balon felfújásával rögzítjük azt. Ezt követően ultrahang ellenőrzése mellett...... ml fiziológiás sóoldatot injectálunk az utarus ürébe, közben a kontrasztayag útját ultrahanggal követjük.\r\n Eredmény:  	</textarea>
				</div>
			</div>";		
			
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"tappenz\">Táppénz:</label>
			<div class=\"col-md-9\">
				<select id=\"tappenz\" name=\"tappenz\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Tisztelt Kolléga! Kérem szíves táppénzes állományba vételét. Köszönettel:</option>";
					echo"<option value=\"2\">Tisztelt Kolléga! Kérem szíves további táppénzen tartását. Köszönettel:</option>";
				echo"</select>
			</div>
		</div>";
		echo "<div class=\"form-group form-actions\">
		<div class=\"col-md-9\">
			<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewinspection();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
		</div>
	</div>";
	?>
