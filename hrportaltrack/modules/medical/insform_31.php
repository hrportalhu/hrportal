<?
	$el=db_row("select * from med_inspection_3 where ih_id='".$_REQUEST['elemid']."'");
	$diagdata=db_row("select * from med_diagnosis where id='".$el['diag_id']."'");
echo "<div class=\"form-group\">
		<label class=\"col-md-3 control-label\" for=\"diag_id\">Kimenő diagnózis:</label>
		<div class=\"col-md-9\">
			<input type=\"text\" id=\"diag_id\" name=\"diag_id\" class=\"form-control\" value=\"".$diagdata['name']." - ".$diagdata['diagcode']."\" />	
			<input type=\"hidden\" id=\"diag_idid\" name=\"diag_idid\" value=\"".$el['diag_id']."\" />																																		
		</div>
	</div>";

	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"utmensz\">Utolsó menzesz:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"utmensz\" name=\"utmensz\" class=\"form-control input-datepicker\" value=\"".$el['utmensz']."\" />
			</div>
		</div>";

	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"magzatszam\">Magzatszám:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"magzatszam\" name=\"magzatszam\" class=\"form-control\" value=\"".$el['magzatszam']."\" />
			</div>
		</div>";
	
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"mvizme\">Magzatvíz mennyisége:</label>
			<div class=\"col-md-9\">
				<select id=\"mvizme\" name=\"mvizme\" class=\"form-control\" >";
				echo"<option value=\"0\" ".($el['mvizme']=="0"?"selected":"")." >Kérlek válassz</option>";
					echo"<option value=\"1\" ".($el['mvizme']=="1"?"selected":"").">Normál</option>";
					echo"<option value=\"2\" ".($el['mvizme']=="2"?"selected":"").">Átlagosnál kevesebb</option>";
					echo"<option value=\"3\" ".($el['mvizme']=="3"?"selected":"").">Átlagosnál több</option>";
					echo"<option value=\"4\" ".($el['mvizme']=="4"?"selected":"").">Oligóhydramnion</option>";
					echo"<option value=\"5\" ".($el['mvizme']=="5"?"selected":"").">Polyhydramnion</option>";
				echo"</select>
			</div>
		</div>";
	
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"mhelyz\">Magzat helyzete:</label>
			<div class=\"col-md-9\">
				<select id=\"mhelyz\" name=\"mhelyz\" class=\"form-control\" >";
				echo"<option value=\"0\" ".($el['mhelyz']=="0"?"selected":"").">Kérlek válassz</option>";
					echo"<option value=\"1\" ".($el['mhelyz']=="1"?"selected":"").">koponyavégű</option>";
					echo"<option value=\"2\" ".($el['mhelyz']=="2"?"selected":"").">medencevégű</option>";
					echo"<option value=\"3\" ".($el['mhelyz']=="3"?"selected":"").">harántfekvés</option>";
					echo"<option value=\"4\" ".($el['mhelyz']=="4"?"selected":"").">ferdefekvés</option>";
				echo"</select>
			</div>
		</div>";
		
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"plac_tap\">Placenta tapadása:</label>
			<div class=\"col-md-9\">
				<select id=\"plac_tap\" name=\"plac_tap\" class=\"form-control\" >";
				echo"<option value=\"0\" ".($el['plac_tap']=="0"?"selected":"")." >Kérlek válassz</option>";
					echo"<option value=\"1\" ".($el['plac_tap']=="1"?"selected":"").">Melső</option>";
					echo"<option value=\"2\" ".($el['plac_tap']=="2"?"selected":"").">Hátsó</option>";
					echo"<option value=\"3\" ".($el['plac_tap']=="3"?"selected":"").">Jobb oldalon</option>";
					echo"<option value=\"4\" ".($el['plac_tap']=="4"?"selected":"").">Bal oldalon</option>";
					echo"<option value=\"5\" ".($el['plac_tap']=="5"?"selected":"").">Előlfekvő - Mélyentapadó</option>";
					echo"<option value=\"6\" ".($el['plac_tap']=="6"?"selected":"").">Előlfekvő - Placenta praevia centralis </option>";
					echo"<option value=\"7\" ".($el['plac_tap']=="7"?"selected":"").">Előlfekvő - Placenta praevia lateralis </option>";
					echo"<option value=\"8\" ".($el['plac_tap']=="8"?"selected":"").">Előlfekvő - Placenta praevia marginalis </option>";
				echo"</select>
			</div>
		</div>";	
		
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"plac_eret\">Placenta érettség:</label>
			<div class=\"col-md-9\">
				<select id=\"plac_eret\" name=\"plac_eret\" class=\"form-control\" >";
				echo"<option value=\"0\" ".($el['plac_eret']=="0"?"selected":"").">Kérlek válassz</option>";
					echo"<option value=\"1\" ".($el['plac_eret']=="1"?"selected":"").">G I.</option>";
					echo"<option value=\"2\" ".($el['plac_eret']=="2"?"selected":"").">G I.-II.</option>";
					echo"<option value=\"3\" ".($el['plac_eret']=="3"?"selected":"").">G II.</option>";
					echo"<option value=\"4\" ".($el['plac_eret']=="4"?"selected":"").">G II.-III.</option>";
					echo"<option value=\"5\" ".($el['plac_eret']=="5"?"selected":"").">G III.</option>";
				echo"</select>
			</div>
		</div>";	
	
			echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"sziv\">Szív:</label>
			<div class=\"col-md-9\">
				<select id=\"sziv\" name=\"sziv\" class=\"form-control\" >";
				echo"<option value=\"0\" ".($el['sziv']=="0"?"selected":"").">Kérlek válassz</option>";
					echo"<option value=\"1\" ".($el['sziv']=="1"?"selected":"").">Épnek látszik</option>";
					echo"<option value=\"2\" ".($el['sziv']=="2"?"selected":"").">4 üregű</option>";
					echo"<option value=\"3\" ".($el['sziv']=="3"?"selected":"").">Kontroll</option>";
				echo"</select>
			</div>
		</div>";
	
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"szivmukodes\">Szívműködés:</label>
			<div class=\"col-md-9\">
				<select id=\"szivmukodes\" name=\"szivmukodes\" class=\"form-control\" >";
				echo"<option value=\"0\" ".($el['szivmukodes']=="0"?"selected":"").">Kérlek válassz</option>";
					echo"<option value=\"1\" ".($el['szivmukodes']=="1"?"selected":"").">Szabályos</option>";
					echo"<option value=\"2\" ".($el['szivmukodes']=="2"?"selected":"").">szabálytalan</option>";
				echo"</select>
			</div>
		</div>";
	
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"szivhely\">Szív elhelyzkedése:</label>
			<div class=\"col-md-9\">
				<select id=\"szivhely\" name=\"szivhely\" class=\"form-control\" >";
				echo"<option value=\"0\" ".($el['szivhely']=="0"?"selected":"").">Kérlek válassz</option>";
					echo"<option value=\"1\" ".($el['szivhely']=="1"?"selected":"").">Baloldal</option>";
					echo"<option value=\"2\"".($el['szivhely']=="2"?"selected":"").">Jobboldal</option>";
					echo"<option value=\"2\" ".($el['szivhely']=="3"?"selected":"").">Dislocal</option>";
				echo"</select>
			</div>
		</div>";
		
		
	
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"fhr\">FHR /min/:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"fhr\" name=\"fhr\" class=\"form-control\" value=\"".$el['fhr']."\" />
			</div>
		</div>";
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"mozgas\">Mozgás:</label>
			<div class=\"col-md-9\">
				<select id=\"mozgas\" name=\"mozgas\" class=\"form-control\" >";
				echo"<option value=\"0\" ".($el['mozgas']=="0"?"selected":"").">Kérlek válassz</option>";
					echo"<option value=\"1\" ".($el['mozgas']=="1"?"selected":"").">Élénk</option>";
					echo"<option value=\"2\" ".($el['mozgas']=="2"?"selected":"").">Renyhe</option>";
					echo"<option value=\"3\" ".($el['mozgas']=="3"?"selected":"").">Nem látható</option>";
				echo"</select>
			</div>
		</div>";
	echo "<div class=\"form-group\">
		<label class=\"col-md-3 control-label\" for=\"cervix\">Cervix /mm/ :</label>
		<div class=\"col-md-9\">
			<input type=\"text\" id=\"cervix\" name=\"cervix\" class=\"form-control\" value=\"".$el['cervix']."\" />																		
		</div>
	</div>";
	
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"bpd\">BPD /mm/:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"bpd\" name=\"bpd\" class=\"form-control\" value=\"".$el['bpd']."\" />
			</div>
		</div>";
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"fronto\">FRONTO /mm/:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"fronto\" name=\"fronto\" class=\"form-control\" value=\"".$el['fronto']."\" />
			</div>
		</div>";
	
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"hc\">HC /mm/:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"hc\" name=\"hc\" class=\"form-control\" value=\"".$el['hc']."\" />
			</div>
		</div>";
	
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"cerebellum\">Cerebellum /mm/:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"cerebellum\" name=\"cerebellum\" class=\"form-control\" value=\"".$el['cerebellum']."\" />
			</div>
		</div>";
echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"thorax\">Thorax /mm/:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"thorax\" name=\"thorax\" class=\"form-control\" value=\"".$el['thorax']."\" />
			</div>
		</div>";
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"ac\">AC /mm/:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"ac\" name=\"ac\" class=\"form-control\" value=\"".$el['ac']."\" />
			</div>
		</div>";
			
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"femur\">Femur /mm/:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"femur\" name=\"femur\" class=\"form-control\" value=\"".$el['femur']."\" />
			</div>
		</div>";
			
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"talp\">Talp /mm/:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"talp\" name=\"talp\" class=\"form-control\" value=\"".$el['talp']."\" />
			</div>
		</div>";
		
			echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"humerus\">Humerus /mm/:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"humerus\" name=\"humerus\" class=\"form-control\" value=\"".$el['humerus']."\" />
			</div>
		</div>";
	
			
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"aortaiv\">Aortaív:</label>
			<div class=\"col-md-9\">
				<select id=\"aortaiv\" name=\"aortaiv\" class=\"form-control\" >";
				echo"<option value=\"0\" ".($el['aortaiv']=="0"?"selected":"").">Kérlek válassz</option>";
					echo"<option value=\"1\" ".($el['aortaiv']=="1"?"selected":"").">Épnek látszik</option>";
					echo"<option value=\"2\" ".($el['aortaiv']=="2"?"selected":"").">Nem látszik épnek</option>";
				echo"</select>
			</div>
		</div>";
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"erek\">Erek:</label>
			<div class=\"col-md-9\">
				<select id=\"erek\" name=\"erek\" class=\"form-control\" >";
				echo"<option value=\"0\" ".($el['erek']=="0"?"selected":"").">Kérlek válassz</option>";
					echo"<option value=\"1\" ".($el['erek']=="1"?"selected":"").">Szabályosnak látszó 3 ér kép</option>";
					echo"<option value=\"2\" ".($el['erek']=="2"?"selected":"").">kontroll javasolt</option>";
				echo"</select>
			</div>
		</div>";
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"ductus\">Ductus:</label>
			<div class=\"col-md-9\">
				<select id=\"ductus\" name=\"ductus\" class=\"form-control\" >";
				echo"<option value=\"0\" ".($el['ductus']=="0"?"selected":"").">Kérlek válassz</option>";
					echo"<option value=\"1\" ".($el['ductus']=="1"?"selected":"").">Épnek látszik</option>";
					echo"<option value=\"2\" ".($el['ductus']=="2"?"selected":"").">Nem látszik épnek</option>";
				echo"</select>
			</div>
		</div>";
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"ductus_iv\">Ductus ív:</label>
			<div class=\"col-md-9\">
				<select id=\"ductus_iv\" name=\"ductus_iv\" class=\"form-control\" >";
				echo"<option value=\"0\" s".($el['ductus_iv']=="0"?"selected":"")." >Kérlek válassz</option>";
					echo"<option value=\"1\" ".($el['ductus_iv']=="1"?"selected":"").">Épnek látszik</option>";
					echo"<option value=\"2\" ".($el['ductus_iv']=="2"?"selected":"").">Nem látszik épnek</option>";
				echo"</select>
			</div>
		</div>";	
					echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"gyomor\">Gyomor:</label>
			<div class=\"col-md-9\">
				<select id=\"gyomor\" name=\"gyomor\" class=\"form-control\" >";
				echo"<option value=\"0\" ".($el['gyomor']=="0"?"selected":"").">Kérlek válassz</option>";
					echo"<option value=\"1\" ".($el['gyomor']=="1"?"selected":"").">Telődés látható</option>";
					echo"<option value=\"2\" ".($el['gyomor']=="2"?"selected":"").">telődés nem látható</option>";
				echo"</select>
			</div>
		</div>";	
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"holyag\">Hólyag:</label>
			<div class=\"col-md-9\">
				<select id=\"holyag\" name=\"holyag\" class=\"form-control\" >";
				echo"<option value=\"0\" ".($el['holyag']=="0"?"selected":"").">Kérlek válassz</option>";
					echo"<option value=\"1\" ".($el['holyag']=="1"?"selected":"").">Telődés látható</option>";
					echo"<option value=\"2\" ".($el['holyag']=="2"?"selected":"").">telődés nem látható</option>";
				echo"</select>
			</div>
		</div>";	
		
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"ausd\">Arteria umbilicalis S/D:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"ausd\" name=\"ausd\" class=\"form-control\" value=\"".$el['ausd']."\" />
			</div>
		</div>";	
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"ausdsel\">Arteria umbilicalis S/D:</label>
			<div class=\"col-md-9\">
				<select id=\"ausdsel\" name=\"ausdsel\" class=\"form-control\" >";
				echo"<option value=\"0\" ".($el['ausdsel']=="0"?"selected":"").">Kérlek válassz</option>";
					echo"<option value=\"1\" ".($el['ausdsel']=="1"?"selected":"").">normál</option>";
					echo"<option value=\"2\" ".($el['ausdsel']=="2"?"selected":"").">fokozott</option>";
					echo"<option value=\"3\" ".($el['ausdsel']=="3"?"selected":"").">kóros</option>";
					echo"<option value=\"4\" ".($el['ausdsel']=="4"?"selected":"").">dyastoles stop</option>";
					echo"<option value=\"5\" ".($el['ausdsel']=="5"?"selected":"").">reverse flow</option>";
				echo"</select>
			</div>
		</div>";		
		
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"aupi\">Arteria umbilicalis PI:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"aupi\" name=\"aupi\" class=\"form-control\" value=\"".$el['aupi']."\" />
			</div>
		</div>";	
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"aupisel\">Arteria umbilicalis PI:</label>
			<div class=\"col-md-9\">
				<select id=\"aupisel\" name=\"aupisel\" class=\"form-control\" >";
				echo"<option value=\"0\" ".($el['aupisel']=="0"?"selected":"").">Kérlek válassz</option>";
					echo"<option value=\"1\" ".($el['aupisel']=="1"?"selected":"").">normál</option>";
					echo"<option value=\"2\" ".($el['aupisel']=="2"?"selected":"").">fokozott</option>";
					echo"<option value=\"3\" ".($el['aupisel']=="3"?"selected":"").">kóros</option>";
				echo"</select>
			</div>
		</div>";		
		
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"acm\">ACM:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"acm\" name=\"acm\" class=\"form-control\" value=\"".$el['acm']."\" />
			</div>
		</div>";	
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"acmsel\">ACM:</label>
			<div class=\"col-md-9\">
				<select id=\"acmsel\" name=\"acmsel\" class=\"form-control\" >";
				echo"<option value=\"0\" ".($el['acmsel']=="0"?"selected":"").">Kérlek válassz</option>";
					echo"<option value=\"1\" ".($el['acmsel']=="1"?"selected":"").">normál</option>";
					echo"<option value=\"2\" ".($el['acmsel']=="2"?"selected":"").">fokozott</option>";
					echo"<option value=\"3\" ".($el['acmsel']=="3"?"selected":"").">kóros</option>";
				echo"</select>
			</div>
		</div>";		
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"neme\">Neme:</label>
			<div class=\"col-md-9\">
				<select id=\"neme\" name=\"neme\" class=\"form-control\" >";
				echo"<option value=\"0\" ".($el['neme']=="0"?"selected":"").">Kérlek válassz</option>";
					echo"<option value=\"1\" ".($el['neme']=="1"?"selected":"").">Kisfiú</option>";
					echo"<option value=\"2\" ".($el['neme']=="2"?"selected":"").">Kislány</option>";
					echo"<option value=\"3\" ".($el['neme']=="3"?"selected":"").">Kisfiú (?)</option>";
					echo"<option value=\"4\" ".($el['neme']=="4"?"selected":"").">Kislány (?) </option>";
					echo"<option value=\"5\" ".($el['neme']=="5"?"selected":"").">Nem állapítható meg </option>";
				echo"</select>
			</div>
		</div>";
		
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"terhido_het\">Terhességi idő:</label>
			<div class=\"col-md-6\">
				<input type=\"text\" id=\"terhido_het\" name=\"terhido_het\" class=\"form-control\" value=\"".$el['terhido_het']."\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:30px;\">Hét</span>
				<input type=\"text\" id=\"terhido_nap\" name=\"terhido_nap\" class=\"form-control\" value=\"".$el['terhido_nap']."\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">nap</span>
			</div>
		</div>";
echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"koldokzsinor\">Köldökzsinór:</label>
			<div class=\"col-md-9\">
				<select id=\"koldokzsinor\" name=\"koldokzsinor\" class=\"form-control\" style=\"width:200px;float:left;\">";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\" ".($el['koldokzsinor']=="1"?"selected":"").">SUA</option>";
					echo"<option value=\"2\" ".($el['koldokzsinor']=="2"?"selected":"").">Három Ér</option>";
				echo"</select>
				<input type=\"text\" id=\"koldokzsinor_mm\" name=\"koldokzsinor_mm\" class=\"form-control\" value=\"".$el['koldokzsinor_mm']."\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">mm</span>
			</div>
		</div>";			
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"becsultsuly\">Becsült súly /g/:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"becsultsuly\" name=\"becsultsuly\" class=\"form-control\" value=\"".$el['becsultsuly']."\" />
			</div>
		</div>";
		
echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"panaszok\">Panaszok</label>
				<div class=\"col-md-9\">
					<textarea id=\"panaszok\" name=\"panaszok\" rows=\"6\" class=\"form-control\" placeholder=\"Panaszok..\">".$el['panaszok']."</textarea>
				</div>
			</div>";	
		
	echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"velemeny\">Vélemény</label>
				<div class=\"col-md-9\">
					<textarea id=\"velemeny\" name=\"velemeny\" rows=\"6\" class=\"form-control\" placeholder=\"Vélemény..\">".$el['velemeny']."</textarea>
				</div>
			</div>";	
		
	echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"javaslat\">Javaslat</label>
				<div class=\"col-md-9\">
					<textarea id=\"javaslat\" name=\"javaslat\" rows=\"6\" class=\"form-control\" placeholder=\"Javaslat..\">".$el['javaslat']."</textarea>
				</div>
			</div>";	
		
	echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"megjegyzes\">Megjegyzés</label>
				<div class=\"col-md-9\">
					<textarea id=\"megjegyzes\" name=\"megjegyzes\" rows=\"6\" class=\"form-control\" placeholder=\"Megjegyzés..\">".$el['megjegyzes']."</textarea>
				</div>
			</div>";	
	
		
	echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"egyeb\">Egyéb</label>
				<div class=\"col-md-9\">
					<textarea id=\"egyeb\" name=\"egyeb\" rows=\"6\" class=\"form-control\" >".$el['egyeb']."</textarea>
				</div>
			</div>";	
	
	echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"holyagnyak\">Hólyagnyak Symphisis távolság mérés</label>
				<div class=\"col-md-9\">
					<textarea id=\"holyagnyak\" name=\"holyagnyak\" rows=\"6\" class=\"form-control\" >".$el['holyagnyak']."</textarea>
				</div>
			</div>";	

	
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"tappenz\">Táppénz:</label>
			<div class=\"col-md-9\">
				<select id=\"tappenz\" name=\"tappenz\" class=\"form-control\" >";
				echo"<option value=\"0\" ".($el['tappenz']=="0"?"selected":"").">Kérlek válassz</option>";
					echo"<option value=\"1\" ".($el['tappenz']=="1"?"selected":"").">Tisztelt Kolléga! Kérem szíves táppénzes állományba vételét. Köszönettel:</option>";
					echo"<option value=\"2\" ".($el['tappenz']=="2"?"selected":"").">Tisztelt Kolléga! Kérem szíves további táppénzen tartását. Köszönettel:</option>";
				echo"</select>
			</div>
		</div>";
	
	echo "<div class=\"form-group form-actions\">
		<div class=\"col-md-9\">
			<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditinspection('".$_REQUEST['elemid']."');\"><i class=\"fa fa-angle-right\"></i> Ment</button>
		</div>
	</div>";
?>
