<?
echo "<div class=\"form-group\">
		<label class=\"col-md-3 control-label\" for=\"diag_id\">Kimenő diagnózis:</label>
		<div class=\"col-md-9\">
			<input type=\"text\" id=\"diag_id\" name=\"diag_id\" class=\"form-control\" placeholder=\"Kimenő diagnózis\" />	
			<input type=\"hidden\" id=\"diag_idid\" name=\"diag_idid\"  />																		
		</div>
	</div>";

	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"utmensz\">Utolsó menzesz:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"utmensz\" name=\"utmensz\" class=\"form-control input-datepicker\" value=\"".date("Y-m-d",time())."\" />
			</div>
		</div>";

	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"magzatszam\">Magzatszám:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"magzatszam\" name=\"magzatszam\" class=\"form-control\" value=\"\" />
			</div>
		</div>";
	
echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"mvizme\">Magzatvíz mennyisége:</label>
			<div class=\"col-md-9\">
				<select id=\"mvizme\" name=\"mvizme\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Normál</option>";
					echo"<option value=\"2\">Átlagosnál kevesebb</option>";
					echo"<option value=\"3\">Átlagosnál kevesebb</option>";
					echo"<option value=\"4\">Oligóhydramnion</option>";
					echo"<option value=\"5\">Polyhydramnion</option>";
				echo"</select>
			</div>
		</div>";
	
echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"plac_tap\">Placenta tapadása:</label>
			<div class=\"col-md-9\">
				<select id=\"plac_tap\" name=\"plac_tap\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Melső</option>";
					echo"<option value=\"2\">Hátsó</option>";
					echo"<option value=\"3\">Jobb oldalon</option>";
					echo"<option value=\"4\">Bal oldalon</option>";
					echo"<option value=\"5\">Előlfekvő - Mélyentapadó</option>";
					echo"<option value=\"6\">Előlfekvő - Placenta praevia centralis </option>";
					echo"<option value=\"7\">Előlfekvő - Placenta praevia lateralis </option>";
					echo"<option value=\"8\">Előlfekvő - Placenta praevia marginalis </option>";
				echo"</select>
			</div>
		</div>";
		echo"<input type=\"hidden\" id=\"plac_hely\" value=\"0\" />";

		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"szivmukodes\">Szívműködés:</label>
			<div class=\"col-md-9\">
				<select id=\"szivmukodes\" name=\"szivmukodes\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Szabályos</option>";
					echo"<option value=\"2\">szabálytalan</option>";
				echo"</select>
			</div>
		</div>";

	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"fhr\">FHR /min/:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"fhr\" name=\"fhr\" class=\"form-control\" value=\"\" />
			</div>
		</div>";
	
			echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"mozgas\">Mozgás:</label>
			<div class=\"col-md-9\">
				<select id=\"mozgas\" name=\"mozgas\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Élénk</option>";
					echo"<option value=\"2\">Renyhe</option>";
					echo"<option value=\"3\">Nem látható</option>";
				echo"</select>
			</div>
		</div>";
	
	echo "<div class=\"form-group\">
		<label class=\"col-md-3 control-label\" for=\"cervix\">Cervix /mm/ :</label>
		<div class=\"col-md-9\">
			<input type=\"text\" id=\"cervix\" name=\"cervix\" class=\"form-control\" placeholder=\"\" />																		
		</div>
	</div>";
	
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"bpd\">BPD /mm/:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"bpd\" name=\"bpd\" class=\"form-control\" value=\"\" />
			</div>
		</div>";
	
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"fronto\">FRONTO /mm/:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"fronto\" name=\"fronto\" class=\"form-control\" value=\"\" />
			</div>
		</div>";
	
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"hc\">HC /mm/:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"hc\" name=\"hc\" class=\"form-control\" value=\"\" />
			</div>
		</div>";
	
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"cerebellum\">Cerebellum /mm/:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"cerebellum\" name=\"cerebellum\" class=\"form-control\" value=\"\" />
			</div>
		</div>";

	
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"cisterna\">Cisterna /mm/:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"cisterna\" name=\"cisterna\" class=\"form-control\" value=\"\" />
			</div>
		</div>";
	
		
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"psterior\">Posterior szarv /mm/:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"psterior\" name=\"psterior\" class=\"form-control\" value=\"\" />
			</div>
		</div>";

	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"arckoponya\">Arckoponya:</label>
			<div class=\"col-md-9\">
				<select id=\"arckoponya\" name=\"arckoponya\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Épnek látszó</option>";
					echo"<option value=\"2\">Nem látszik épnek</option>";
					echo"<option value=\"3\">kontrol</option>";
				echo"</select>
			</div>
		</div>";
		echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"arckoponya_comment\">Arckoponya megjegyzés</label>
				<div class=\"col-md-9\">
					<textarea id=\"arckoponya_comment\" name=\"arckoponya_comment\" rows=\"6\" class=\"form-control\" placeholder=\"Panaszok..\"></textarea>
				</div>
			</div>";	
			
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"orcsont\">Orrcsont:</label>
			<div class=\"col-md-9\">
				<select id=\"orcsont\" name=\"orcsont\" class=\"form-control\" style=\"width:200px;float:left;\">";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Épnek látszó</option>";
					echo"<option value=\"2\">Nem látszik épnek</option>";
					echo"<option value=\"3\">kontrol</option>";
				echo"</select>
				<input type=\"text\" id=\"orcsont_mm\" name=\"orcsont_mm\" class=\"form-control\" value=\"\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">mm</span>
			</div>
		</div>";	
				
			
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"pt\">PT /mm/:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"pt\" name=\"pt\" class=\"form-control\" value=\"\" />
			</div>
		</div>";
			
			echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"agykoponya\">Agykoponya:</label>
			<div class=\"col-md-9\">
				<select id=\"agykoponya\" name=\"agykoponya\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Épnek látszó</option>";
					echo"<option value=\"2\">Ventriculomegalia</option>";
					echo"<option value=\"3\">kontrol</option>";
				echo"</select>
			</div>
		</div>";
		echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"agykoponya_comment\">Agykoponya megjegyzés</label>
				<div class=\"col-md-9\">
					<textarea id=\"agykoponya_comment\" name=\"agykoponya_comment\" rows=\"6\" class=\"form-control\" placeholder=\"Panaszok..\"></textarea>
				</div>
			</div>";	
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"flax\">Falx:</label>
			<div class=\"col-md-9\">
				<select id=\"flax\" name=\"flax\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Középen</option>";
					echo"<option value=\"2\">Diszlokált</option>";
				echo"</select>
			</div>
		</div>";
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"septum\">Septum pelucidi:</label>
			<div class=\"col-md-9\">
				<select id=\"septum\" name=\"septum\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Látható</option>";
					echo"<option value=\"2\">nem látható</option>";
				echo"</select>
			</div>
		</div>";
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"4kamra\">4. kamra:</label>
			<div class=\"col-md-9\">
				<select id=\"4kamra\" name=\"4kamra\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Látható</option>";
					echo"<option value=\"2\">nem látható</option>";
				echo"</select>
			</div>
		</div>";
		
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"corpus\">Corpus:</label>
			<div class=\"col-md-9\">
				<select id=\"corpus\" name=\"corpus\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Épnek látszó</option>";
					echo"<option value=\"2\">Nem látható</option>";
					echo"<option value=\"3\">Nem vizsgálható</option>";
				echo"</select>
			</div>
		</div>";
		
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"thalamus\">Thalamus:</label>
			<div class=\"col-md-9\">
				<select id=\"thalamus\" name=\"thalamus\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Épnek látszik</option>";
					echo"<option value=\"2\">Nem látszik épnek</option>";
				echo"</select>
			</div>
		</div>";
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"agyfeltekek\">Agyféltekék:</label>
			<div class=\"col-md-9\">
				<select id=\"agyfeltekek\" name=\"agyfeltekek\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Szimetrikus</option>";
					echo"<option value=\"2\">Asszimetrikus</option>";
				echo"</select>
			</div>
		</div>";
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"thorax\">Thorax /mm/:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"thorax\" name=\"thorax\" class=\"form-control\" value=\"\" />
			</div>
		</div>";
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"ac\">AC /mm/:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"ac\" name=\"ac\" class=\"form-control\" value=\"\" />
			</div>
		</div>";
			
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"femur\">Femur /mm/:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"femur\" name=\"femur\" class=\"form-control\" value=\"\" />
			</div>
		</div>";
			
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"talp\">Talp /mm/:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"talp\" name=\"talp\" class=\"form-control\" value=\"\" />
			</div>
		</div>";
			
			
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"humerus\">Humerus /mm/:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"humerus\" name=\"humerus\" class=\"form-control\" value=\"\" />
			</div>
		</div>";
		
				echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"felsovegtag\">Felső végtagok:</label>
			<div class=\"col-md-9\">
				<select id=\"felsovegtag\" name=\"felsovegtag\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Épnek látszik</option>";
					echo"<option value=\"2\">Nem látszik épnek</option>";
				echo"</select>
			</div>
		</div>";
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"alsovegtag\">Alsó végtagok:</label>
			<div class=\"col-md-9\">
				<select id=\"alsovegtag\" name=\"alsovegtag\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Épnek látszik</option>";
					echo"<option value=\"2\">Nem látszik épnek</option>";
				echo"</select>
			</div>
		</div>";
		
					echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"gerinc\">Gerinc:</label>
			<div class=\"col-md-9\">
				<select id=\"gerinc\" name=\"gerinc\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Épnek látszik</option>";
					echo"<option value=\"2\">Spina bifida</option>";
				echo"</select>
			</div>
		</div>";
		echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"gerinc_comment\">Gerinc megjegyzés</label>
				<div class=\"col-md-9\">
					<textarea id=\"gerinc_comment\" name=\"gerinc_comment\" rows=\"6\" class=\"form-control\" placeholder=\"Panaszok..\"></textarea>
				</div>
			</div>";
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"sziv\">Szív:</label>
			<div class=\"col-md-9\">
				<select id=\"sziv\" name=\"sziv\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Épnek látszik</option>";
					echo"<option value=\"2\">Nem látszik épnek</option>";
				echo"</select>
			</div>
		</div>";
		echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"sziv_comment\">Szív megjegyzés</label>
				<div class=\"col-md-9\">
					<textarea id=\"sziv_comment\" name=\"sziv_comment\" rows=\"6\" class=\"form-control\" placeholder=\"Panaszok..\"></textarea>
				</div>
			</div>";
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"szivhely\">Szív elhelyzkedése:</label>
			<div class=\"col-md-9\">
				<select id=\"szivhely\" name=\"szivhely\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Baloldal</option>";
					echo"<option value=\"2\">Jobboldal</option>";
					echo"<option value=\"2\">Dislocal</option>";
				echo"</select>
			</div>
		</div>";
		
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"szivhely\">Szív 4 üreg:</label>
			<div class=\"col-md-9\">
				<select id=\"szivhely\" name=\"szivhely\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Épnek látszik</option>";
					echo"<option value=\"2\">Nem látszik épnek</option>";
				echo"</select>
			</div>
		</div>";
		
		
		
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"aortaiv\">Aortaív:</label>
			<div class=\"col-md-9\">
				<select id=\"aortaiv\" name=\"aortaiv\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Épnek látszik</option>";
					echo"<option value=\"2\">Nem látszik épnek</option>";
				echo"</select>
			</div>
		</div>";
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"erek\">Erek:</label>
			<div class=\"col-md-9\">
				<select id=\"erek\" name=\"erek\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Szabályosnak látszó 3 ér kép</option>";
					echo"<option value=\"2\">kontroll javasolt</option>";
				echo"</select>
			</div>
		</div>";
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"ductus\">Ductus:</label>
			<div class=\"col-md-9\">
				<select id=\"ductus\" name=\"ductus\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Épnek látszik</option>";
					echo"<option value=\"2\">Nem látszik épnek</option>";
				echo"</select>
			</div>
		</div>";
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"ductus_iv\">Ductus ív:</label>
			<div class=\"col-md-9\">
				<select id=\"ductus_iv\" name=\"ductus_iv\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Épnek látszik</option>";
					echo"<option value=\"2\">Nem látszik épnek</option>";
				echo"</select>
			</div>
		</div>";
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"lvot\">LVOT:</label>
			<div class=\"col-md-9\">
				<select id=\"lvot\" name=\"lvot\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Épnek látszik</option>";
					echo"<option value=\"2\">Nem látszik épnek</option>";
				echo"</select>
			</div>
		</div>";
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"rvot\">RVOT:</label>
			<div class=\"col-md-9\">
				<select id=\"rvot\" name=\"rvot\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Épnek látszik</option>";
					echo"<option value=\"2\">Nem látszik épnek</option>";
				echo"</select>
			</div>
		</div>";
		
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"tudovenak\">Tüdővénák:</label>
			<div class=\"col-md-9\">
				<select id=\"tudovenak\" name=\"tudovenak\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Épnek látszik</option>";
					echo"<option value=\"2\">Nem látszik épnek</option>";
				echo"</select>
			</div>
		</div>";
		
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"hasfal\">Hasfal:</label>
			<div class=\"col-md-9\">
				<select id=\"hasfal\" name=\"hasfal\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Zártnak látszik</option>";
					echo"<option value=\"2\">omphalocele</option>";
					echo"<option value=\"3\">gastrocitis</option>";
				echo"</select>
			</div>
		</div>";
		echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"hasfal_comment\">Hasfal megjegyzés</label>
				<div class=\"col-md-9\">
					<textarea id=\"hasfal_comment\" name=\"hasfal_comment\" rows=\"6\" class=\"form-control\" placeholder=\"Panaszok..\"></textarea>
				</div>
			</div>";
		
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"mellkas\">Mellkas:</label>
			<div class=\"col-md-9\">
				<select id=\"mellkas\" name=\"mellkas\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Zártnak látszik</option>";
					echo"<option value=\"2\">Nyitottnak látszik</option>";
				echo"</select>
			</div>
		</div>";
		echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"mellkas_comment\">Mellkas megjegyzés</label>
				<div class=\"col-md-9\">
					<textarea id=\"mellkas_comment\" name=\"mellkas_comment\" rows=\"6\" class=\"form-control\" placeholder=\"Panaszok..\"></textarea>
				</div>
			</div>";
			
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"hasiecho\">Hasi echo:</label>
			<div class=\"col-md-9\">
				<select id=\"hasiecho\" name=\"hasiecho\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Normális</option>";
					echo"<option value=\"2\">Hyperechogen</option>";
				echo"</select>
			</div>
		</div>";	
			
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"rekesz\">Rekesz:</label>
			<div class=\"col-md-9\">
				<select id=\"rekesz\" name=\"rekesz\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Épnek látszik</option>";
					echo"<option value=\"2\">Nem látszik épnek</option>";
				echo"</select>
			</div>
		</div>";	
			
			
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"gyomor\">Gyomor:</label>
			<div class=\"col-md-9\">
				<select id=\"gyomor\" name=\"gyomor\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Telődés látható</option>";
					echo"<option value=\"2\">telődés nem látható</option>";
				echo"</select>
			</div>
		</div>";	
			
			
			
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"holyag\">Hólyag:</label>
			<div class=\"col-md-9\">
				<select id=\"holyag\" name=\"holyag\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Telődés látható</option>";
					echo"<option value=\"2\">telődés nem látható</option>";
				echo"</select>
			</div>
		</div>";	
			
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"jobbvese\">Jobbvese:</label>
			<div class=\"col-md-9\">
				<select id=\"jobbvese\" name=\"jobbvese\" class=\"form-control\" style=\"width:200px;float:left;\">";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Épnek látszik</option>";
					echo"<option value=\"2\">pyleon tágulat</option>";
				echo"</select>
				<input type=\"text\" id=\"jobbvese_mm\" name=\"jobbvese_mm\" class=\"form-control\" value=\"\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">mm</span>
			</div>
		</div>";	
			
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"balvese\">Bal vese:</label>
			<div class=\"col-md-9\">
				<select id=\"balvese\" name=\"balvese\" class=\"form-control\" style=\"width:200px;float:left;\">";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Épnek látszik</option>";
					echo"<option value=\"2\">pyleon tágulat</option>";
				echo"</select>
				<input type=\"text\" id=\"balvese_mm\" name=\"balvese_mm\" class=\"form-control\" value=\"\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">mm</span>
			</div>
		</div>";	
			
			
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"koldokzsinor\">Köldökzsinór:</label>
			<div class=\"col-md-9\">
				<select id=\"koldokzsinor\" name=\"koldokzsinor\" class=\"form-control\" style=\"width:200px;float:left;\">";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">SUA</option>";
					echo"<option value=\"2\">Három Ér</option>";
				echo"</select>
				<input type=\"text\" id=\"koldokzsinor_mm\" name=\"koldokzsinor_mm\" class=\"form-control\" value=\"\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">mm</span>
			</div>
		</div>";	
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"neme\">Neme:</label>
			<div class=\"col-md-9\">
				<select id=\"neme\" name=\"neme\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Kisfiú</option>";
					echo"<option value=\"2\">Kislány</option>";
					echo"<option value=\"3\">Kisfiú (?)</option>";
					echo"<option value=\"4\">Kislány (?) </option>";
					echo"<option value=\"5\">Nem állapítható meg </option>";
				echo"</select>
			</div>
		</div>";
			
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"terhido_het\">Terhességi idő:</label>
			<div class=\"col-md-6\">
				<input type=\"text\" id=\"terhido_het\" name=\"terhido_het\" class=\"form-control\" value=\"\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:30px;\">Hét</span>
				<input type=\"text\" id=\"terhido_nap\" name=\"terhido_nap\" class=\"form-control\" value=\"\" style=\"width:100px;float:left;\"/><span style=\"width:20px;float:left;line-height:20px;\">nap</span>
			</div>
		</div>";
		
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"becsultsuly\">Becsült súly /g/:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"becsultsuly\" name=\"becsultsuly\" class=\"form-control\" value=\"\" />
			</div>
		</div>";
		
	echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"panaszok\">Panaszok</label>
				<div class=\"col-md-9\">
					<textarea id=\"panaszok\" name=\"panaszok\" rows=\"6\" class=\"form-control\" placeholder=\"Panaszok..\"></textarea>
				</div>
			</div>";	
		
	echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"velemeny\">Vélemény</label>
				<div class=\"col-md-9\">
					<textarea id=\"velemeny\" name=\"velemeny\" rows=\"6\" class=\"form-control\" placeholder=\"Vélemény..\"></textarea>
				</div>
			</div>";	
		
	echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"javaslat\">Javaslat</label>
				<div class=\"col-md-9\">
					<textarea id=\"javaslat\" name=\"javaslat\" rows=\"6\" class=\"form-control\" placeholder=\"Javaslat..\">Genetikai konzílium javsolt: ...\r\nDr Horváth Emese \r\nTel:+3662544951\r\nCím: Szeged, Somogyi Béla u 4\r\n Jelen lelet nem minősülOEP finanszírozott beutalónak.</textarea>
				</div>
			</div>";	
		
	echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"megjegyzes\">Megjegyzés</label>
				<div class=\"col-md-9\">
					<textarea id=\"megjegyzes\" name=\"megjegyzes\" rows=\"6\" class=\"form-control\" placeholder=\"Megjegyzés..\">Mai vizsgálatom során tájékoztatást kaptam a genetikai tanácsadás fontosságáról, az invazív prenatlis bevatkozások kockázatáról, az ultrahangos szűrővizsgálat, hatékonyságáról, valamit az anyai vérből végezhető bikémiai és DNS alapú szűrővizsgálatok (NIFTY és PRENA teszt) hatékonyságáról.\r\nA fenti tájékoztatást tudomásul vettem: </textarea>
				</div>
			</div>";	
	
		
	echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"egyeb\">Egyéb</label>
				<div class=\"col-md-9\">
					<textarea id=\"egyeb\" name=\"egyeb\" rows=\"6\" class=\"form-control\" >FMF Kód: 61527\r\nTisztelt Hölgyem!\r\nTájékoztatom, hogy a kiadott lelet az ön tulajdona. A következő vizsgálatkor kérem hozza magával.</textarea>
				</div>
			</div>";	
	
	echo" <div class=\"form-group\">
				<label class=\"col-md-3 control-label\" for=\"holyagnyak\">Hólyagnyak Symphisis távolság mérés</label>
				<div class=\"col-md-9\">
					<textarea id=\"holyagnyak\" name=\"holyagnyak\" rows=\"6\" class=\"form-control\" >L1:\r\nH1:\r\nB1:\r\nC1:\r\nL2:\r\nH2:\r\nB2:\r\nC2:\r\ntestsúly/kg.:</textarea>
				</div>
			</div>";	
echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"kontroll\">Kontroll:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"kontroll\" name=\"kontroll\" class=\"form-control\" value=\"\" />
			</div>
		</div>";
	
		echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"tappenz\">Táppénz:</label>
			<div class=\"col-md-9\">
				<select id=\"tappenz\" name=\"tappenz\" class=\"form-control\" >";
				echo"<option value=\"0\" selected >Kérlek válassz</option>";
					echo"<option value=\"1\">Tisztelt Kolléga! Kérem szíves táppénzes állományba vételét. Köszönettel:</option>";
					echo"<option value=\"2\">Tisztelt Kolléga! Kérem szíves további táppénzen tartását. Köszönettel:</option>";
				echo"</select>
			</div>
		</div>";
	
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"afp_mom\">AFP /MOM/:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"afp_mom\" name=\"afp_mom\" class=\"form-control\" value=\"\" />
			</div>
		</div>";
	
	echo "<div class=\"form-group\">
			<label class=\"col-md-3 control-label\" for=\"afp_ug\">AFP /ug/l/:</label>
			<div class=\"col-md-9\">
				<input type=\"text\" id=\"afp_ug\" name=\"afp_ug\" class=\"form-control\" value=\"\" />
			</div>
		</div>";
			
	echo "<div class=\"form-group form-actions\">
		<div class=\"col-md-9\">
			<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewinspection();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
		</div>
	</div>";
	?>
