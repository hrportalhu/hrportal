
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Vizsgálatok
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/medical/inspection">Vizsgálatok</a></li>
				<li><a href="/new">Új vizsgálat rögzítése</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új vizsgálat rögzítése</strong></h2>
				</div>
			<?
				$vt=db_all("select id, name from med_inspection_types where status=1 order by name");
				$pat=db_all("select id, name, birthdate,tajnum from med_patients where status=1 order by name");
				
				echo "<form id=\"newinspection\" name=\"newinspection\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";
				
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"patient_id\">Vizsgált páciens:</label>
						<div class=\"col-md-9\">
							<select id=\"patient_id\" name=\"patient_id\" class=\"form-control\" >
							<option value=\"0\">Kérlek válassz</option>";
							for($i=0;$i<count($pat);$i++){
								$sel="";if($uritags[4]==$pat[$i]['id']){$sel=" selected ";}
								echo"<option value=\"".$pat[$i]['id']."\" ".$sel.">".$pat[$i]['name']." (".$pat[$i]['birthdate']." - TAJ: ".$pat[$i]['tajnum'].") </option>";	
								
							}
							echo"</select>
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"inspection_date\">Vizsgálati dátum:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"inspection_date\" name=\"inspection_date\" class=\"form-control input-datepicker\" value=\"".date("Y-m-d",time())."\" />
						</div>
					</div>";
					
					
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"inspection_type\">Vizsgálat típusa:</label>
						<div class=\"col-md-9\">
							<select id=\"inspection_type\" name=\"inspection_type\" class=\"form-control\" onchange=\"javascript:getvizsform('0');\">
							<option value=\"0\">Kérlek válassz</option>";
							for($i=0;$i<count($vt);$i++){
								echo"<option value=\"".$vt[$i]['id']."\" >".$vt[$i]['name']."</option>";	
							}
							echo"</select>
						</div>
					</div>";
					echo"<div id=\"vizsform\"></div>";
				echo "</div>
				</form>";
			?>
			</div>	
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/medical/js/inc_inspection.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?
		}
		else{
			
			//echo "select * from functions where id='".$uritags[2]."'";
			$myinspection=db_row("select * from med_inspection_header where id='".$uritags[3]."'");
			if($myinspection['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
				$insdata=db_row("select * from med_inspection_".$myinspection['inspection_type']." where ih_id='".$myinspection['id']."'");
				$vt=db_all("select id, name from med_inspection_types where status=1 order by name");
				$pat=db_all("select id, name, birthdate,tajnum from med_patients where status=1 order by name");
				$mp=db_row("select * from med_patients where id='".$myinspection['patient_id']."'");
				$diag=db_row("select * from med_diagnosis where id='".$insdata['diag_id']."'");
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="modules/medical/inspection">Vizsgálat</a></li>
				<li><a href="/<?=$URI;?>"><?=$myinspection['inspection_date'];?> - <?= idchange("med_inspection_types","name",$myinspection['inspection_type']);?> vizsgálat</a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->
					<div class="block-title">
						<h2><?=$myinspection['inspection_date'];?> - <?= idchange("med_inspection_types","name",$myinspection['inspection_type']);?> vizsgálat</h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
							if(priv("inspection","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("inspection","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							if(priv("inspection","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("inspection","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
							//if(priv("inspection","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}	
							if(priv("inspection","print")){echo"<li onclick=\"javascript:getprintscreen('".$_SESSION['planetsys']['actpage']['modulname']."','inspectionprint','".$myinspection['id']."');\" ><a href=\"#modal-regular-print\" data-toggle=\"modal\" title=\"Spec print\"><i class=\"gi gi-print\" ></i></a></li>";}
							?>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="col-md-6">
								<dl class="dl-horizontal">
									<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$myinspection['id']); ?></dd>
									<dt>Név:</dt><dd><? echo $mp['name']; ?></dd>
									<dt>Születési neve:</dt><dd><? echo $mp['birthname']; ?></dd>
									<dt>Születési dátum:</dt><dd><? echo $mp['birthdate'];; ?></dd>
									<dt>Édesanyja neve:</dt><dd><? echo $mp['mothername']; ?></dd>
									<dt>TAJ szám:</dt><dd><? echo $mp['tajnum']; ?></dd>
									<dt>Telefonszáma:</dt><dd><? echo $mp['phone']; ?></dd>
									<dt>Címe:</dt><dd><? echo $mp['address']; ?></dd>
									<dt>Státusz:</dt><dd><? if($myinspection['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
								</dl>	
								<?
								if($myinspection['inspection_type']==1){
									echo"<table style=\"font-size:15px;width:670px;\">";
									echo"<tr><td style=\"width:250px;\">Kimenő diagnózis:</td><td><b>".$diag['name']." ".$diag['diagcode']."</b></td></tr>";
									if($insdata['utmensz']!="0000-00-00"){echo"<tr><td style=\"width:250px;\">Utolsó menzesz:</td><td><b>".$insdata['utmensz']."</b></td></tr>";}
									if($insdata['et1']!=""){echo"<tr><td style=\"width:250px;\">ET:</td><td><b>".$insdata['et1']."mm X ".$insdata['et2']."mm</b></td></tr>";}
									if($insdata['pet1']!=""){echo"<tr><td style=\"width:250px;\">Petezsák nagysága:</td><td><b>".$insdata['pet1']."mm X ".$insdata['pet2']."mm</b></td></tr>";}
									if($insdata['ys1']!=""){echo"<tr><td style=\"width:250px;\">YS:</td><td><b>".$insdata['ys1']."mm X ".$insdata['ys2']."mm</b></td></tr>";}
									if($insdata['crl']!=""){echo"<tr><td style=\"width:250px;\">CRL:</td><td><b>".$insdata['crl']."mm</b></td></tr>";}
									if($insdata['bpd']!=""){echo"<tr><td style=\"width:250px;\">BPD:</td><td><b>".$insdata['bpd']."mm</b></td></tr>";}
									if($insdata['fronto']!=""){echo"<tr><td style=\"width:250px;\">Fronto:</td><td><b>".$insdata['fronto']."mm</b></td></tr>";}
									if($insdata['thorax']!=""){echo"<tr><td style=\"width:250px;\">Thorax:</td><td><b>".$insdata['thorax']."mm</b></td></tr>";}
									if($insdata['femur']!=""){echo"<tr><td style=\"width:250px;\">Femur:</td><td><b>".$insdata['femur']."mm</b></td></tr>";}
									if($insdata['szivmukodes']!="0"){
										echo"<tr><td style=\"width:250px;\">Szívműködés:</td><td><b>";
										if($insdata['szivmukodes']==1){echo"Látható";}
										elseif($insdata['szivmukodes']==2){echo"Nem látható";}
										echo"</b></td></tr>";
									}
									if($insdata['fhr']!=""){echo"<tr><td style=\"width:250px;\">FHR:</td><td><b>".$insdata['fhr']."/Min</b></td></tr>";}
									if($insdata['inact']!="0"){
										echo"<tr><td style=\"width:250px;\">Intact:</td><td><b>";
										if($insdata['inact']==1){echo"Igen";}
										elseif($insdata['inact']==2){echo"Nem";}
										echo"</b></td></tr>";
									}
									if($insdata['terhido_het']!="0"){echo"<tr><td style=\"width:250px;\">Terhességi idő:</td><td><b>".$insdata['terhido_het']." hét, ".$insdata['terhido_nap']." nap</b></td></tr>";}
									if($insdata['nuchaszures']!=""){echo"<tr><td style=\"width:250px;\">Nuchaszűrés időpontja:</td><td><b>".$insdata['nuchaszures']."</b></td></tr>";}
									if($insdata['jobbovarium1']!=""){echo"<tr><td style=\"width:250px;\">Jobb ovárium:</td><td><b>".$insdata['jobbovarium1']."mm X ".$insdata['jobbovarium2']."mm</b></td></tr>";}
									
									if($insdata['balovarium1']!=""){echo"<tr><td style=\"width:250px;\">Bal ovárium:</td><td><b>".$insdata['balovarium1']."mm X ".$insdata['balovarium2']."mm</b></td></tr>";}
			
									if($insdata['hasurfolyadek']!="0"){
										echo"<tr><td style=\"width:250px;\">Szabad hasűri folyadék:</td><td><b>";
										if($insdata['hasurfolyadek']==1){echo"látható";}
										elseif($insdata['hasurfolyadek']==2){echo"nem látható";}
										elseif($insdata['hasurfolyadek']==3){echo"kevés";}
										elseif($insdata['hasurfolyadek']==4){echo"sok";}
										echo"</b></td></tr>";
									}
									if($insdata['panaszok']!=""){echo"<tr><td style=\"width:250px;\">Panaszok:</td><td><b>".nl2br($insdata['panaszok'])."</b></td></tr>";}
									if($insdata['velemeny']!=""){echo"<tr><td style=\"width:250px;\">Vélemény:</td><td><b>".nl2br($insdata['velemeny'])."</b></td></tr>";}
									if($insdata['javaslat']!=""){echo"<tr><td style=\"width:250px;\">Javaslat:</td><td><b>".nl2br($insdata['javaslat'])."</b></td></tr>";}
									if($insdata['megjegyzes']!=""){echo"<tr><td style=\"width:250px;\">Megjegyzés:</td><td><b>".nl2br($insdata['megjegyzes'])."</b></td></tr>";}
									if($insdata['egyeb']!=""){echo"<tr><td style=\"width:250px;\">Egyéb:</td><td><b>".nl2br($insdata['egyeb'])."</b></td></tr>";}
									if($insdata['tappenz']!=""){
										echo"<tr><td style=\"width:250px;\">Táppénz:</td><td><b>";
											if($insdata['tappenz']==1){echo"Tisztelt Kolléga! Kérem szíves táppénzes állományba vételét. Köszönettel";}
											elseif($insdata['tappenz']==2){echo"Tisztelt Kolléga! Kérem szíves további táppénzen tartását. Köszönettel";}
										echo"</b></td></tr>";
									}
									echo"</table>";
								}	
								elseif($myinspection['inspection_type']==2){
											echo"<table style=\"font-size:15px;width:670px;\">";
											echo"<tr><td style=\"width:250px;\">Kimenő diagnózis:</td><td><b>".$diag['name']." ".$diag['diagcode']."</b></td></tr>";
											//if($insdata['utmensz']!="0000-00-00"){echo"<tr><td style=\"width:250px;\">Utolsó menzesz:</td><td><b>".$insdata['utmensz']."</b></td></tr>";}
											if($insdata['uterus_1']!=""){echo"<tr><td style=\"width:250px;\">Uterus:</td><td><b>".$insdata['uterus_1']."mm X ".$insdata['uterus_2']."mm X ".$insdata['uterus_3']."mm</b></td></tr>";}
											if($insdata['uterus_comment']!=""){echo"<tr><td style=\"width:250px;\">Uterus megjegyzés:</td><td><b>".$insdata['uterus_comment']."</b></td></tr>";}
											if($insdata['cervix']!=""){echo"<tr><td style=\"width:250px;\">cervix:</td><td><b>".$insdata['cervix']."mm</b></td></tr>";}
											if($insdata['cervix_comment']!=""){echo"<tr><td style=\"width:250px;\">Cervix megjegyzés:</td><td><b>".$insdata['cervix_comment']."</b></td></tr>";}
											if($insdata['endemetrium']!=""){echo"<tr><td style=\"width:250px;\">Endemetrium:</td><td><b>".$insdata['endemetrium']."mm</b></td></tr>";}
											if($insdata['endemetrium_comment']!=""){echo"<tr><td style=\"width:250px;\">Endemetrium megjegyzés:</td><td><b>".$insdata['endemetrium_comment']."</b></td></tr>";}
											if($insdata['jobbovarium1']!=""){echo"<tr><td style=\"width:250px;\">Jobb ovárium:</td><td><b>".$insdata['jobbovarium1']."mm X ".$insdata['jobbovarium2']."mm</b></td></tr>";}
											if($insdata['jobbovarium_comment']!=""){echo"<tr><td style=\"width:250px;\">Jobb ovárium megjegyzes:</td><td><b>".nl2br($insdata['jobbovarium_comment'])."</b></td></tr>";}
											if($insdata['balovarium1']!=""){echo"<tr><td style=\"width:250px;\">Bal ovárium:</td><td><b>".$insdata['balovarium1']."mm X ".$insdata['balovarium2']."mm</b></td></tr>";}
											if($insdata['balovarium_comment']!=""){echo"<tr><td style=\"width:250px;\">Bal ovárium megjegyzes:</td><td><b>".nl2br($insdata['balovarium_comment'])."</b></td></tr>";}
											if($insdata['hasurfolyadek']!="0"){
												echo"<tr><td style=\"width:250px;\">Szabad hasűri folyadék:</td><td><b>";
												if($insdata['hasurfolyadek']==1){echo"látható";}
												elseif($insdata['hasurfolyadek']==2){echo"nem látható";}
												elseif($insdata['hasurfolyadek']==3){echo"kevés";}
												elseif($insdata['hasurfolyadek']==4){echo"sok";}
												echo"</b></td></tr>";
											}

											if($insdata['kismedence']!=""){echo"<tr><td style=\"width:250px;\">Kismedence:</td><td><b>".nl2br($insdata['kismedence'])."</b></td></tr>";}
											if($insdata['panaszok']!=""){echo"<tr><td style=\"width:250px;\">Panaszok:</td><td><b>".nl2br($insdata['panaszok'])."</b></td></tr>";}
											if($insdata['velemeny']!=""){echo"<tr><td style=\"width:250px;\">Vélemény:</td><td><b>".nl2br($insdata['velemeny'])."</b></td></tr>";}
											if($insdata['javaslat']!=""){echo"<tr><td style=\"width:250px;\">Javaslat:</td><td><b>".nl2br($insdata['javaslat'])."</b></td></tr>";}
											if($insdata['megjegyzes']!=""){echo"<tr><td style=\"width:250px;\">Megjegyzés:</td><td><b>".nl2br($insdata['megjegyzes'])."</b></td></tr>";}
											if($insdata['egyeb']!=""){echo"<tr><td style=\"width:250px;\">Egyéb:</td><td><b>".nl2br($insdata['egyeb'])."</b></td></tr>";}
											if($insdata['holyagnyak']!=""){echo"<tr><td style=\"width:250px;\">Hólyagnyak-Symphozis távolság mérés:</td><td><b>".nl2br($insdata['holyagnyak'])."</b></td></tr>";}
											if($insdata['hsg']!=""){echo"<tr><td style=\"width:250px;\">HSG:</td><td><b>".nl2br($insdata['hsg'])."</b></td></tr>";}
											if($insdata['tappenz']!=""){
												echo"<tr><td style=\"width:250px;\">Táppénz:</td><td><b>";
													if($insdata['tappenz']==1){echo"Tisztelt Kolléga! Kérem szíves táppénzes állományba vételét. Köszönettel";}
													elseif($insdata['tappenz']==2){echo"Tisztelt Kolléga! Kérem szíves további táppénzen tartását. Köszönettel";}
												echo"</b></td></tr>";
											}
											echo"</table>";

								}
								elseif($myinspection['inspection_type']==3){
										echo"<table style=\"font-size:15px;width:670px;\">";
										echo"<tr><td style=\"width:250px;\">Kimenő diagnózis:</td><td><b>".$diag['name']." ".$diag['diagcode']."</b></td></tr>";
										if($insdata['utmensz']!="0000-00-00"){echo"<tr><td style=\"width:250px;\">Utolsó menzesz:</td><td><b>".$insdata['utmensz']."</b></td></tr>";}
										if($insdata['magzatszam']!=""){echo"<tr><td style=\"width:250px;\">Magzatszám:</td><td><b>".$insdata['magzatszam']."</b></td></tr>";}
										if($insdata['mvizme']!="0"){
											echo"<tr><td style=\"width:250px;\">Magzatvíz:</td><td><b>";
											if($insdata['mvizme']==1){echo"Normál";}
											elseif($insdata['mvizme']==2){echo"Átlagosnál kevesebb";}
											elseif($insdata['mvizme']==3){echo"Átlagosnál több";}
											elseif($insdata['mvizme']==4){echo"Oligóhydramnion";}
											elseif($insdata['mvizme']==5){echo"Polyhydramnion";}
											echo"</b></td></tr>";
										}
										if($insdata['mvizme']!="0"){
											echo"<tr><td style=\"width:250px;\"Magzat helyzete:</td><td><b>";
											if($insdata['mhelyz']==1){echo"koponyavégű";}
											elseif($insdata['mhelyz']==2){echo"medencevégű";}
											elseif($insdata['mhelyz']==3){echo"harántfekvés";}
											elseif($insdata['mhelyz']==4){echo"ferdefekvés";}
											echo"</b></td></tr>";
										}
										if($insdata['plac_tap']!="0"){
											echo"<tr><td style=\"width:250px;\">Chorion / Placenta tapadás:</td><td><b>";
											if($insdata['plac_tap']==1){echo"Melső";}
											elseif($insdata['plac_tap']==2){echo"Hátsó";}
											elseif($insdata['plac_tap']==3){echo"Jobb oldalon";}
											elseif($insdata['plac_tap']==4){echo"Bal oldalon";}
											elseif($insdata['plac_tap']==5){echo"Előlfekvő - Mélyentapadó";}
											elseif($insdata['plac_tap']==6){echo"Előlfekvő - Placenta praevia centralis";}
											elseif($insdata['plac_tap']==7){echo"Előlfekvő - Placenta praevia lateralis ";}
											elseif($insdata['plac_tap']==8){echo"Előlfekvő - Placenta praevia marginalisó";}
											echo"</b></td></tr>";
										}
										if($insdata['plac_eret']!="0"){
											echo"<tr><td style=\"width:250px;\">Placenta érettség:</td><td><b>";
											if($insdata['plac_eret']==1){echo"G I.";}
											elseif($insdata['plac_eret']==2){echo"G I.-II.";}
											elseif($insdata['plac_eret']==3){echo"G II.";}
											elseif($insdata['plac_eret']==4){echo"G II.-III.";}
											elseif($insdata['plac_eret']==5){echo"G III.";}
											echo"</b></td></tr>";
										}
										if($insdata['sziv']!="0"){
											echo"<tr><td style=\"width:250px;\">Szív:</td><td><b>";
											if($insdata['sziv']==1){echo"Épnek látszik";}
											elseif($insdata['sziv']==2){echo"4 üregű";}
											elseif($insdata['sziv']==3){echo"Kontroll";}
											echo"</b></td></tr>";
										}
										if($insdata['szivmukodes']!="0"){
											echo"<tr><td style=\"width:250px;\">Szívműködés:</td><td><b>";
											if($insdata['szivmukodes']==1){echo"Szabályos";}
											elseif($insdata['szivmukodes']==2){echo"Szabálytalan";}
											echo"</b></td></tr>";
										}
										if($insdata['szivhely']!="0"){
											echo"<tr><td style=\"width:250px;\">Szív elhelyzkedése:</td><td><b>";
											if($insdata['szivhely']==1){echo"Baloldal";}
											elseif($insdata['szivhely']==2){echo"Jobboldal";}
											elseif($insdata['szivhely']==3){echo"Dislocal";}
											echo"</b></td></tr>";
										}
										if($insdata['fhr']!=""){echo"<tr><td style=\"width:250px;\">FHR:</td><td><b>".$insdata['fhr']."/min </b></td></tr>";}
										if($insdata['cervix']!=""){echo"<tr><td style=\"width:250px;\">cervix:</td><td><b>".$insdata['cervix']."mm</b></td></tr>";}
										if($insdata['mozgas']!="0"){
											echo"<tr><td style=\"width:250px;\">Mozgás:</td><td><b>";
											if($insdata['mozgas']==1){echo"Élénk";}
											elseif($insdata['szivmukodes']==2){echo"Renyhe";}
											elseif($insdata['szivmukodes']==3){echo"Nem látható";}
											echo"</b></td></tr>";
										}
										if($insdata['bpd']!=""){echo"<tr><td style=\"width:250px;\">BPD:</td><td><b>".$insdata['bpd']."mm </b></td></tr>";}
										if($insdata['fronto']!=""){echo"<tr><td style=\"width:250px;\">FRONTO:</td><td><b>".$insdata['fronto']."mm </b></td></tr>";}
										if($insdata['hc']!=""){echo"<tr><td style=\"width:250px;\">HC:</td><td><b>".$insdata['hc']."mm </b></td></tr>";}
										if($insdata['cerebellum']!=""){echo"<tr><td style=\"width:250px;\">Cerebellum:</td><td><b>".$insdata['cerebellum']."mm </b></td></tr>";}
										if($insdata['thorax']!=""){echo"<tr><td style=\"width:250px;\">Thorax:</td><td><b>".$insdata['thorax']."mm </b></td></tr>";}
										if($insdata['ac']!=""){echo"<tr><td style=\"width:250px;\">AC:</td><td><b>".$insdata['ac']."mm </b></td></tr>";}
										if($insdata['femur']!=""){echo"<tr><td style=\"width:250px;\">Femur:</td><td><b>".$insdata['femur']."mm </b></td></tr>";}
										if($insdata['talp']!=""){echo"<tr><td style=\"width:250px;\">Talp:</td><td><b>".$insdata['talp']."mm </b></td></tr>";}
										if($insdata['humerus']!=""){echo"<tr><td style=\"width:250px;\">Humerus:</td><td><b>".$insdata['humerus']."mm </b></td></tr>";}
										if($insdata['aortaiv']!="0"){
											echo"<tr><td style=\"width:250px;\">Aorta ív:</td><td><b>";
											if($insdata['aortaiv']==1){echo"Épnek látszik";}
											elseif($insdata['aortaiv']==2){echo"Nem látszik éppnek";}
											echo"</b></td></tr>";
										}

										if($insdata['erek']!="0"){
											echo"<tr><td style=\"width:250px;\">Erek:</td><td><b>";
											if($insdata['erek']==1){echo"Szabályosnak látszó 3 ér kép";}
											elseif($insdata['erek']==2){echo"kontroll javasolt";}
											echo"</b></td></tr>";
										}
										if($insdata['ductus']!="0"){
											echo"<tr><td style=\"width:250px;\">Ductus:</td><td><b>";
											if($insdata['ductus']==1){echo"Épnek látszik";}
											elseif($insdata['ductus']==2){echo"Nem látszik éppnek";}
											echo"</b></td></tr>";
										}
										if($insdata['ductus_iv']!="0"){
											echo"<tr><td style=\"width:250px;\">Ductus:</td><td><b>";
											if($insdata['ductus_iv']==1){echo"Épnek látszik";}
											elseif($insdata['ductus_iv']==2){echo"Nem látszik éppnek";}
											echo"</b></td></tr>";
										}
										if($insdata['gyomor']!="0"){
											echo"<tr><td style=\"width:250px;\">Gyomor:</td><td><b>";
											if($insdata['gyomor']==1){echo"Telítődés látható";}
											elseif($insdata['gyomor']==2){echo"Telítődés nem látható";}
											echo"</b></td></tr>";
										}
										if($insdata['holyag']!="0"){
											echo"<tr><td style=\"width:250px;\">Hólyag:</td><td><b>";
											if($insdata['holyag']==1){echo"Telítődés látható";}
											elseif($insdata['holyag']==2){echo"Telítődés nem látható";}
											echo"</b></td></tr>";
										}
										if($insdata['ausd']!=""){echo"<tr><td style=\"width:250px;\">Arteria umbilicalis S/D:</td><td><b>".$insdata['ausd']."</b></td></tr>";}
										if($insdata['ausdsel']!="0"){
											echo"<tr><td style=\"width:250px;\">Arteria umbilicalis S/D:</td><td><b>";
											if($insdata['ausdsel']==1){echo"normál";}
											elseif($insdata['ausdsel']==2){echo"fokozott";}
											elseif($insdata['ausdsel']==3){echo"kóros";}
											elseif($insdata['ausdsel']==4){echo"dyastoles stop";}
											elseif($insdata['ausdsel']==5){echo"reverse flow";}
											echo"</b></td></tr>";
										}
										if($insdata['aupi']!=""){echo"<tr><td style=\"width:250px;\">Arteria umbilicalis PI:</td><td><b>".$insdata['aupi']."</b></td></tr>";}
										if($insdata['aupisel']!="0"){
											echo"<tr><td style=\"width:250px;\">Arteria umbilicalis PI:</td><td><b>";
											if($insdata['aupisel']==1){echo"normál";}
											elseif($insdata['aupisel']==2){echo"fokozott";}
											elseif($insdata['aupisel']==3){echo"kóros";}
											echo"</b></td></tr>";
										}
										if($insdata['acm']!=""){echo"<tr><td style=\"width:250px;\">ACM:</td><td><b>".$insdata['acm']."</b></td></tr>";}
										if($insdata['acmsel']!="0"){
											echo"<tr><td style=\"width:250px;\">ACM:</td><td><b>";
											if($insdata['acmsel']==1){echo"normál";}
											elseif($insdata['acmsel']==2){echo"fokozott";}
											elseif($insdata['acmsel']==3){echo"kóros";}
											echo"</b></td></tr>";
										}
										
										if($insdata['neme']!="0"){
											echo"<tr><td style=\"width:250px;\">Neme:</td><td><b>";
											if($insdata['neme']==1){echo"Kisfiú";}
											elseif($insdata['neme']==2){echo"Kislány";}
											elseif($insdata['neme']==3){echo"Kisfiú (?)";}
											elseif($insdata['neme']==4){echo"Kislány (?) ";}
											elseif($insdata['neme']==5){echo"Nem állapítható meg ";}
											echo"</b></td></tr>";
										}
										if($insdata['terhido_het']!=""){echo"<tr><td style=\"width:250px;\">Terhességi idő:</td><td><b>".$insdata['terhido_het']."hét ".$insdata['terhido_nap']." </b></td></tr>";}
										if($insdata['koldokzsinor']!="0"){
											echo"<tr><td style=\"width:250px;\">Köldökzsínór:</td><td><b>";
											if($insdata['koldokzsinor']==1){echo"SUA";}
											elseif($insdata['koldokzsinor']==2){echo"3 ér";}
											echo"</b></td></tr>";
										}
										if($insdata['koldokzsinor_mm']!=""){echo"<tr><td style=\"width:250px;\">Köldökzsinó hossza:</td><td><b>".$insdata['koldokzsinor_mm']."mm </b></td></tr>";}


										if($insdata['becsultsuly']!=""){echo"<tr><td style=\"width:250px;\">Becsült súly:</td><td><b>".$insdata['becsultsuly']."</b></td></tr>";}
										if($insdata['panaszok']!=""){echo"<tr><td style=\"width:250px;\">Panaszok:</td><td><b>".nl2br($insdata['panaszok'])."</b></td></tr>";}
										if($insdata['velemeny']!=""){echo"<tr><td style=\"width:250px;\">Vélemény:</td><td><b>".nl2br($insdata['velemeny'])."</b></td></tr>";}
										if($insdata['javaslat']!=""){echo"<tr><td style=\"width:250px;\">Javaslat:</td><td><b>".nl2br($insdata['javaslat'])."</b></td></tr>";}
										if($insdata['megjegyzes']!=""){echo"<tr><td style=\"width:250px;\">Megjegyzés:</td><td><b>".nl2br($insdata['megjegyzes'])."</b></td></tr>";}
										if($insdata['egyeb']!=""){echo"<tr><td style=\"width:250px;\">Egyéb:</td><td><b>".nl2br($insdata['egyeb'])."</b></td></tr>";}
										if($insdata['holyagnyak']!=""){echo"<tr><td style=\"width:250px;\">Hólyagnyak-Symphozis távolság mérés:</td><td><b>".nl2br($insdata['holyagnyak'])."</b></td></tr>";}
										if($insdata['tappenz']!=""){
											echo"<tr><td style=\"width:250px;\">Táppénz:</td><td><b>";
												if($insdata['tappenz']==1){echo"Tisztelt Kolléga! Kérem szíves táppénzes állományba vételét. Köszönettel";}
												elseif($insdata['tappenz']==2){echo"Tisztelt Kolléga! Kérem szíves további táppénzen tartását. Köszönettel";}
											echo"</b></td></tr>";
										}
										echo"</table>";

								}
								elseif($myinspection['inspection_type']==4){if($insdata['cervix']!=""){echo"<tr><td style=\"width:250px;\">cervix:</td><td><b>".$insdata['cervix']."mm</b></td></tr>";}
												echo"<table style=\"font-size:15px;width:670px;\">";
												echo"<tr><td style=\"width:250px;\">Kimenő diagnózis:</td><td><b>".$diag['name']." ".$diag['diagcode']."</b></td></tr>";
												if($insdata['utmensz']!="0000-00-00"){echo"<tr><td style=\"width:250px;\">Utolsó menzesz:</td><td><b>".$insdata['utmensz']."</b></td></tr>";}
												if($insdata['crl']!=""){echo"<tr><td style=\"width:250px;\">CRL:</td><td><b>".$insdata['crl']."mm </b></td></tr>";}
												if($insdata['bpd']!=""){echo"<tr><td style=\"width:250px;\">BPD:</td><td><b>".$insdata['bpd']."mm </b></td></tr>";}
												if($insdata['fronto']!=""){echo"<tr><td style=\"width:250px;\">FRONTO:</td><td><b>".$insdata['fronto']."mm </b></td></tr>";}
												if($insdata['hc']!=""){echo"<tr><td style=\"width:250px;\">HC:</td><td><b>".$insdata['hc']."mm </b></td></tr>";}

												if($insdata['arckoponya']!="0"){
													echo"<tr><td style=\"width:250px;\">Arckoponya:</td><td><b>";
													if($insdata['arckoponya']==1){echo"Épnek látszó";}
													elseif($insdata['arckoponya']==2){echo"nem látszik épnek";}
													echo"</b></td></tr>";
												}

												if($insdata['orcsont']!="0"){
													echo"<tr><td style=\"width:250px;\">Orrcsont:</td><td><b>";
													if($insdata['orcsont']==1){echo"Épnek látszó";}
													elseif($insdata['orcsont']==2){echo"nem látszik épnek";}
													elseif($insdata['orcsont']==3){echo"kontroll";}
													echo"</b></td></tr>";
												}
												if($insdata['orcsont_mm']!=""){echo"<tr><td style=\"width:250px;\">Orrcsont hossza:</td><td><b>".$insdata['orcsont_mm']."mm </b></td></tr>";}
												
												
												if($insdata['agykoponya']!="0"){
													echo"<tr><td style=\"width:250px;\">Agykoponya:</td><td><b>";
													if($insdata['agykoponya']==1){echo"Épnek látszó";}
													elseif($insdata['agykoponya']==2){echo"nem látszik épnek";}
													elseif($insdata['agykoponya']==3){echo"kontroll";}
													echo"</b></td></tr>";
												}

												
												if($insdata['dv']!="0"){
													echo"<tr><td style=\"width:250px;\">DV:</td><td><b>";
													if($insdata['dv']==1){echo"Normális";}
													elseif($insdata['dv']==2){echo"A-hullám";}
													elseif($insdata['dv']==3){echo"reverse flow";}
													echo"</b></td></tr>";
												}
												if($insdata['dvpi']!=""){echo"<tr><td style=\"width:250px;\">DV PI:</td><td><b>".$insdata['dvpi']." min </b></td></tr>";}
												

												if($insdata['3kamra']!="0"){
													echo"<tr><td style=\"width:250px;\">3. kamra:</td><td><b>";
													if($insdata['3kamra']==1){echo"Látható";}
													elseif($insdata['3kamra']==2){echo"Nem látható";}
													echo"</b></td></tr>";
												}
												if($insdata['4kamra']!="0"){
													echo"<tr><td style=\"width:250px;\">4. kamra:</td><td><b>";
													if($insdata['4kamra']==1){echo"Látható";}
													elseif($insdata['4kamra']==2){echo"Nem látható";}
													echo"</b></td></tr>";
												}
												if($insdata['mese']!="0"){
													echo"<tr><td style=\"width:250px;\">Mesencephalom:</td><td><b>";
													if($insdata['mese']==1){echo"Látható";}
													elseif($insdata['mese']==2){echo"Nem látható";}
													echo"</b></td></tr>";
												}
												if($insdata['nucha']!=""){echo"<tr><td style=\"width:250px;\">Nucha:</td><td><b>".$insdata['nucha']."mm </b></td></tr>";}
												if($insdata['thorax']!=""){echo"<tr><td style=\"width:250px;\">Thorax:</td><td><b>".$insdata['thorax']."mm </b></td></tr>";}
												if($insdata['ac']!=""){echo"<tr><td style=\"width:250px;\">AC:</td><td><b>".$insdata['ac']."mm </b></td></tr>";}

												if($insdata['gyomor']!="0"){
													echo"<tr><td style=\"width:250px;\">Gyomor:</td><td><b>";
													if($insdata['gyomor']==1){echo"Telítődés látható";}
													elseif($insdata['gyomor']==2){echo"Telítődés nem látható";}
													echo"</b></td></tr>";
												}
												if($insdata['holyag']!="0"){
													echo"<tr><td style=\"width:250px;\">Hólyag:</td><td><b>";
													if($insdata['holyag']==1){echo"Telítődés látható";}
													elseif($insdata['holyag']==2){echo"Telítődés nem látható";}
													echo"</b></td></tr>";
												}
												if($insdata['humerus']!=""){echo"<tr><td style=\"width:250px;\">Humerus:</td><td><b>".$insdata['humerus']."mm </b></td></tr>";}
												if($insdata['femur']!=""){echo"<tr><td style=\"width:250px;\">Femur:</td><td><b>".$insdata['femur']."mm </b></td></tr>";}
												if($insdata['talp']!=""){echo"<tr><td style=\"width:250px;\">Talp:</td><td><b>".$insdata['talp']."mm </b></td></tr>";}
												if($insdata['felsovegtag']!="0"){
													echo"<tr><td style=\"width:250px;\">Felső végtagok:</td><td><b>";
													if($insdata['felsovegtag']==1){echo"Éppnek látszó";}
													elseif($insdata['felsovegtag']==2){echo"Nem látszik éppnek";}
													echo"</b></td></tr>";
												}

												if($insdata['alsovegtag']!="0"){
													echo"<tr><td style=\"width:250px;\">Alsó végtagok:</td><td><b>";
													if($insdata['alsovegtag']==1){echo"Éppnek látszó";}
													elseif($insdata['alsovegtag']==2){echo"Nem látszik éppnek";}
													echo"</b></td></tr>";
												}
												if($insdata['gerinc']!="0"){
													echo"<tr><td style=\"width:250px;\">Gerinc:</td><td><b>";
													if($insdata['gerinc']==1){echo"Éppnek látszó";}
													elseif($insdata['gerinc']==2){echo"Spina bifida";}
													echo"</b></td></tr>";
												}

												if($insdata['sziv']!="0"){
													echo"<tr><td style=\"width:250px;\">Szív:</td><td><b>";
													if($insdata['sziv']==1){echo"Éppnek látszó";}
													elseif($insdata['sziv']==2){echo"Nem látszik éppnek";}
													echo"</b></td></tr>";
												}

												if($insdata['hasfal']!="0"){
													echo"<tr><td style=\"width:250px;\">Hasfal:</td><td><b>";
													if($insdata['hasfal']==1){echo"Zártnak látszik";}
													elseif($insdata['hasfal']==2){echo"omphalocele";}
													elseif($insdata['hasfal']==3){echo"gastrocitis";}
													echo"</b></td></tr>";
												}

												if($insdata['hasi_echo']!="0"){
													echo"<tr><td style=\"width:250px;\">Hasi echo:</td><td><b>";
													if($insdata['hasi_echo']==1){echo"Normál echogenitas";}
													elseif($insdata['hasi_echo']==2){echo"Nem normál echogenitas";}
													echo"</b></td></tr>";
												}

												if($insdata['jobbvese']!="0"){
													echo"<tr><td style=\"width:250px;\">Jobb vese:</td><td><b>";
													if($insdata['jobbvese']==1){echo"Éppnek látszó";}
													elseif($insdata['jobbvese']==2){echo"pyleon tágulat";}
													echo"</b></td></tr>";
												}

												if($insdata['balvese']!="0"){
													echo"<tr><td style=\"width:250px;\">Bal vese:</td><td><b>";
													if($insdata['balvese']==1){echo"Éppnek látszó";}
													elseif($insdata['balvese']==2){echo"pyleon tágulat";}
													echo"</b></td></tr>";
												}

												if($insdata['koldokzsinor']!="0"){
													echo"<tr><td style=\"width:250px;\">Köldökzsinór:</td><td><b>";
													if($insdata['koldokzsinor']==1){echo"SUA";}
													elseif($insdata['koldokzsinor']==2){echo"3 ér";}
													echo"</b></td></tr>";
												}
												if($insdata['koldokzsinor_mm']!=""){echo"<tr><td style=\"width:250px;\">Köldökzsinó hossza:</td><td><b>".$insdata['koldokzsinor_mm']."mm </b></td></tr>";}
												if($insdata['cptapadas']!="0"){
													echo"<tr><td style=\"width:250px;\">Chorion / Placenta tapadás:</td><td><b>";
													if($insdata['cptapadas']==1){echo"Melső";}
													elseif($insdata['cptapadas']==2){echo"Hátsó";}
													elseif($insdata['cptapadas']==3){echo"Jobb oldalon";}
													elseif($insdata['cptapadas']==4){echo"Bal oldalon";}
													elseif($insdata['cptapadas']==5){echo"Előlfekvő - Mélyentapadó";}
													elseif($insdata['cptapadas']==6){echo"Előlfekvő - Placenta praevia centralis";}
													elseif($insdata['cptapadas']==7){echo"Előlfekvő - Placenta praevia lateralis ";}
													elseif($insdata['cptapadas']==8){echo"Előlfekvő - Placenta praevia marginalisó";}
													echo"</b></td></tr>";
												}

												if($insdata['magzatviz']!="0"){
													echo"<tr><td style=\"width:250px;\">Magzatvíz:</td><td><b>";
													if($insdata['magzatviz']==1){echo"Normál";}
													elseif($insdata['magzatviz']==2){echo"Átlagosnál kevesebb";}
													elseif($insdata['magzatviz']==3){echo"Átlagosnál több";}
													elseif($insdata['magzatviz']==4){echo"Oligóhydramnion";}
													elseif($insdata['magzatviz']==5){echo"Polyhydramnion";}
													echo"</b></td></tr>";
												}

												if($insdata['szivmukodes']!="0"){
													echo"<tr><td style=\"width:250px;\">Szívműködés:</td><td><b>";
													if($insdata['szivmukodes']==1){echo"Szabályos";}
													elseif($insdata['szivmukodes']==2){echo"Szabálytalan";}
													echo"</b></td></tr>";
												}
												if($insdata['fhr']!=""){echo"<tr><td style=\"width:250px;\">FHR:</td><td><b>".$insdata['fhr']."/min </b></td></tr>";}
												if($insdata['mozgas']!="0"){
													echo"<tr><td style=\"width:250px;\">Mozgás:</td><td><b>";
													if($insdata['mozgas']==1){echo"Élénk";}
													elseif($insdata['szivmukodes']==2){echo"Renyhe";}
													elseif($insdata['szivmukodes']==3){echo"Nem látható";}
													echo"</b></td></tr>";
												}

												if($insdata['inact']!="0"){
													echo"<tr><td style=\"width:250px;\">Intakt terhesség:</td><td><b>";
													if($insdata['inact']==1){echo"igen";}
													elseif($insdata['inact']==2){echo"nem";}
													echo"</b></td></tr>";
												}

												if($insdata['neme']!="0"){
													echo"<tr><td style=\"width:250px;\">Neme:</td><td><b>";
													if($insdata['neme']==1){echo"Kisfiú";}
													elseif($insdata['neme']==2){echo"Kislány";}
													elseif($insdata['neme']==3){echo"Kisfiú (?)";}
													elseif($insdata['neme']==4){echo"Kislány (?) ";}
													elseif($insdata['neme']==5){echo"Nem állapítható meg ";}
													echo"</b></td></tr>";
												}
												if($insdata['terhido_het']!=""){echo"<tr><td style=\"width:250px;\">Terhességi idő:</td><td><b>".$insdata['terhido_het']."hét ".$insdata['terhido_nap']." </b></td></tr>";}
												
												if($insdata['panaszok']!=""){echo"<tr><td style=\"width:250px;\">Panaszok:</td><td><b>".nl2br($insdata['panaszok'])."</b></td></tr>";}
												if($insdata['velemeny']!=""){echo"<tr><td style=\"width:250px;\">Vélemény:</td><td><b>".nl2br($insdata['velemeny'])."</b></td></tr>";}
												if($insdata['javaslat']!=""){echo"<tr><td style=\"width:250px;\">Javaslat:</td><td><b>".nl2br($insdata['javaslat'])."</b></td></tr>";}
												if($insdata['megjegyzes']!=""){echo"<tr><td style=\"width:250px;\">Megjegyzés:</td><td><b>".nl2br($insdata['megjegyzes'])."</b></td></tr>";}
												if($insdata['egyeb']!=""){echo"<tr><td style=\"width:250px;\">Egyéb:</td><td><b>".nl2br($insdata['egyeb'])."</b></td></tr>";}
												if($insdata['holyagnyak']!=""){echo"<tr><td style=\"width:250px;\">Hólyagnyak-Symphozis távolság mérés:</td><td><b>".nl2br($insdata['holyagnyak'])."</b></td></tr>";}
												if($insdata['hsg']!=""){echo"<tr><td style=\"width:250px;\">HSG:</td><td><b>".nl2br($insdata['hsg'])."</b></td></tr>";}
												if($insdata['tappenz']!=""){
													echo"<tr><td style=\"width:250px;\">Táppénz:</td><td><b>";
														if($insdata['tappenz']==1){echo"Tisztelt Kolléga! Kérem szíves táppénzes állományba vételét. Köszönettel";}
														elseif($insdata['tappenz']==2){echo"Tisztelt Kolléga! Kérem szíves további táppénzen tartását. Köszönettel";}
													echo"</b></td></tr>";
												}
												if($insdata['arteria_s']!=""){echo"<tr><td style=\"width:250px;\">Arteria uterina I.s PI::</td><td><b>".$insdata['arteria_s']."mm </b></td></tr>";}
												if($insdata['arteria_d']!=""){echo"<tr><td style=\"width:250px;\">Arteria uterina I.d PI::</td><td><b>".$insdata['arteria_d']."mm </b></td></tr>";}
												echo"</table>";

								}
								elseif($myinspection['inspection_type']==5){
									//
											echo"<table style=\"font-size:15px;width:670px;\">";
										echo"<tr><td style=\"width:250px;\">Kimenő diagnózis:</td><td><b>".$diag['name']." ".$diag['diagcode']."</b></td></tr>";
										if($insdata['utmensz']!="0000-00-00"){echo"<tr><td style=\"width:250px;\">Utolsó menzesz:</td><td><b>".$insdata['utmensz']."</b></td></tr>";}
										if($insdata['magzatszam']!=""){echo"<tr><td style=\"width:250px;\">Magzatszám:</td><td><b>".$insdata['magzatszam']."</b></td></tr>";}
										if($insdata['mvizme']!="0"){
											echo"<tr><td style=\"width:250px;\">Magzatvíz:</td><td><b>";
											if($insdata['mvizme']==1){echo"Normál";}
											elseif($insdata['mvizme']==2){echo"Átlagosnál kevesebb";}
											elseif($insdata['mvizme']==3){echo"Átlagosnál több";}
											elseif($insdata['mvizme']==4){echo"Oligóhydramnion";}
											elseif($insdata['mvizme']==5){echo"Polyhydramnion";}
											echo"</b></td></tr>";
										}
										if($insdata['plac_tap']!="0"){
											echo"<tr><td style=\"width:250px;\">Chorion / Placenta tapadás:</td><td><b>";
											if($insdata['plac_tap']==1){echo"Melső";}
											elseif($insdata['plac_tap']==2){echo"Hátsó";}
											elseif($insdata['plac_tap']==3){echo"Jobb oldalon";}
											elseif($insdata['plac_tap']==4){echo"Bal oldalon";}
											elseif($insdata['plac_tap']==5){echo"Előlfekvő - Mélyentapadó";}
											elseif($insdata['plac_tap']==6){echo"Előlfekvő - Placenta praevia centralis";}
											elseif($insdata['plac_tap']==7){echo"Előlfekvő - Placenta praevia lateralis ";}
											elseif($insdata['plac_tap']==8){echo"Előlfekvő - Placenta praevia marginalisó";}
											echo"</b></td></tr>";
										}
										if($insdata['szivmukodes']!="0"){
											echo"<tr><td style=\"width:250px;\">Szívműködés:</td><td><b>";
											if($insdata['szivmukodes']==1){echo"Szabályos";}
											elseif($insdata['szivmukodes']==2){echo"Szabálytalan";}
											echo"</b></td></tr>";
										}
										if($insdata['fhr']!=""){echo"<tr><td style=\"width:250px;\">FHR:</td><td><b>".$insdata['fhr']."/min </b></td></tr>";}
										if($insdata['mozgas']!="0"){
											echo"<tr><td style=\"width:250px;\">Mozgás:</td><td><b>";
											if($insdata['mozgas']==1){echo"Élénk";}
											elseif($insdata['szivmukodes']==2){echo"Renyhe";}
											elseif($insdata['szivmukodes']==3){echo"Nem látható";}
											echo"</b></td></tr>";
										}
										if($insdata['cervix']!=""){echo"<tr><td style=\"width:250px;\">cervix:</td><td><b>".$insdata['cervix']."mm</b></td></tr>";}
										if($insdata['bpd']!=""){echo"<tr><td style=\"width:250px;\">BPD:</td><td><b>".$insdata['bpd']."mm </b></td></tr>";}
										if($insdata['fronto']!=""){echo"<tr><td style=\"width:250px;\">FRONTO:</td><td><b>".$insdata['fronto']."mm </b></td></tr>";}
										if($insdata['hc']!=""){echo"<tr><td style=\"width:250px;\">HC:</td><td><b>".$insdata['hc']."mm </b></td></tr>";}
										if($insdata['cerebellum']!=""){echo"<tr><td style=\"width:250px;\">Cerebellum:</td><td><b>".$insdata['cerebellum']."mm </b></td></tr>";}
										if($insdata['cisterna']!=""){echo"<tr><td style=\"width:250px;\">Cisterna:</td><td><b>".$insdata['cisterna']."mm </b></td></tr>";}
										if($insdata['posterior']!=""){echo"<tr><td style=\"width:250px;\">Posterior szarv:</td><td><b>".$insdata['posterior']."mm </b></td></tr>";}
										if($insdata['arckoponya']!="0"){
											echo"<tr><td style=\"width:250px;\">Arckoponya:</td><td><b>";
											if($insdata['arckoponya']==1){echo"Épnek látszó";}
											elseif($insdata['arckoponya']==2){echo"nem látszik épnek";}
											echo"</b></td></tr>";
										}
										if($insdata['arckoponya_comment']!=""){echo"<tr><td style=\"width:250px;\">Arckoponya megjegyzés:</td><td><b>".$insdata['arckoponya_comment']."</b></td></tr>";}
										if($insdata['orcsont']!="0"){
											echo"<tr><td style=\"width:250px;\">Orrcsont:</td><td><b>";
											if($insdata['orcsont']==1){echo"Épnek látszó";}
											elseif($insdata['orcsont']==2){echo"nem látszik épnek";}
											elseif($insdata['orcsont']==3){echo"kontroll";}
											echo"</b></td></tr>";
										}
										if($insdata['orcsont_mm']!=""){echo"<tr><td style=\"width:250px;\">Orrcsont hossza:</td><td><b>".$insdata['orcsont_mm']."mm </b></td></tr>";}
										if($insdata['pt']!=""){echo"<tr><td style=\"width:250px;\">PT:</td><td><b>".$insdata['pt']."mm </b></td></tr>";}
										if($insdata['agykoponya']!="0"){
											echo"<tr><td style=\"width:250px;\">Agykoponya:</td><td><b>";
											if($insdata['agykoponya']==1){echo"Épnek látszó";}
											elseif($insdata['agykoponya']==2){echo"ventriculomegalia";}
											elseif($insdata['agykoponya']==3){echo"kontroll";}
											echo"</b></td></tr>";
										}
										if($insdata['agykoponya_comment']!=""){echo"<tr><td style=\"width:250px;\">Agykoponya megjegyzés:</td><td><b>".$insdata['agykoponya_comment']."</b></td></tr>";}
										if($insdata['flax']!="0"){
											echo"<tr><td style=\"width:250px;\">Falx:</td><td><b>";
											if($insdata['flax']==1){echo"Középen";}
											elseif($insdata['flax']==2){echo"Diszlokált";}
											echo"</b></td></tr>";
										}
										if($insdata['septum']!="0"){
											echo"<tr><td style=\"width:250px;\">Septum pelucidi:</td><td><b>";
											if($insdata['septum']==1){echo"Látható";}
											elseif($insdata['septum']==2){echo"Nem látható";}
											echo"</b></td></tr>";
										}
										if($insdata['4kamra']!="0"){
											echo"<tr><td style=\"width:250px;\">4. kamra:</td><td><b>";
											if($insdata['4kamra']==1){echo"Látható";}
											elseif($insdata['4kamra']==2){echo"Nem látható";}
											echo"</b></td></tr>";
										}
										
										if($insdata['thalamus']!="0"){
											echo"<tr><td style=\"width:250px;\">Thalamus:</td><td><b>";
											if($insdata['thalamus']==1){echo"Éppnek látszik";}
											elseif($insdata['thalamus']==2){echo"Nem látszik éppnek";}
											echo"</b></td></tr>";
										}
										if($insdata['agyfeltekek']!="0"){
											echo"<tr><td style=\"width:250px;\">Agyféltekék:</td><td><b>";
											if($insdata['agyfeltekek']==1){echo"Szimmetrikusa";}
											elseif($insdata['egyfeltekek']==2){echo"Asszimetrikusak";}
											echo"</b></td></tr>";
										}
										if($insdata['thorax']!=""){echo"<tr><td style=\"width:250px;\">Thorax:</td><td><b>".$insdata['thorax']."mm </b></td></tr>";}
										if($insdata['ac']!=""){echo"<tr><td style=\"width:250px;\">AC:</td><td><b>".$insdata['ac']."mm </b></td></tr>";}
										if($insdata['femur']!=""){echo"<tr><td style=\"width:250px;\">Femur:</td><td><b>".$insdata['femur']."mm </b></td></tr>";}
										if($insdata['talp']!=""){echo"<tr><td style=\"width:250px;\">Talp:</td><td><b>".$insdata['talp']."mm </b></td></tr>";}
										if($insdata['humerus']!=""){echo"<tr><td style=\"width:250px;\">Humerus:</td><td><b>".$insdata['humerus']."mm </b></td></tr>";}
										if($insdata['felsovegtag']!="0"){
											echo"<tr><td style=\"width:250px;\">Felső végtagok:</td><td><b>";
											if($insdata['felsovegtag']==1){echo"Éppnek látszó";}
											elseif($insdata['felsovegtag']==2){echo"Nem látszik éppnek";}
											echo"</b></td></tr>";
										}

										if($insdata['alsovegtag']!="0"){
											echo"<tr><td style=\"width:250px;\">Alsó végtagok:</td><td><b>";
											if($insdata['alsovegtag']==1){echo"Éppnek látszó";}
											elseif($insdata['alsovegtag']==2){echo"Nem látszik éppnek";}
											echo"</b></td></tr>";
										}
										if($insdata['gerinc']!="0"){
											echo"<tr><td style=\"width:250px;\">Gerinc:</td><td><b>";
											if($insdata['gerinc']==1){echo"Éppnek látszó";}
											elseif($insdata['gerinc']==2){echo"Spina bifida";}
											echo"</b></td></tr>";
										}
										if($insdata['gerinc_comment']!=""){echo"<tr><td style=\"width:250px;\">Gerinc megjegyzés:</td><td><b>".$insdata['gerinc_comment']."</b></td></tr>";}
										if($insdata['sziv']!="0"){
											echo"<tr><td style=\"width:250px;\">Szív:</td><td><b>";
											if($insdata['sziv']==1){echo"Éppnek látszó";}
											elseif($insdata['sziv']==2){echo"Nem látszik éppnek";}
											echo"</b></td></tr>";
										}
										if($insdata['sziv_comment']!=""){echo"<tr><td style=\"width:250px;\">Szív megjegyzés:</td><td><b>".$insdata['sziv_comment']."</b></td></tr>";}
										if($insdata['szivhely']!="0"){
											echo"<tr><td style=\"width:250px;\">Szív elhelyezkedése:</td><td><b>";
											if($insdata['szivhely']==1){echo"Baloldal";}
											elseif($insdata['szivhely']==2){echo"Jobboldal";}
											elseif($insdata['szivhely']==3){echo"Dislocal";}
											echo"</b></td></tr>";
										}

										if($insdata['aortaiv']!="0"){
											echo"<tr><td style=\"width:250px;\">Aorta ív:</td><td><b>";
											if($insdata['aortaiv']==1){echo"Épnek látszik";}
											elseif($insdata['aortaiv']==2){echo"Nem látszik éppnek";}
											echo"</b></td></tr>";
										}

										if($insdata['erek']!="0"){
											echo"<tr><td style=\"width:250px;\">Erek:</td><td><b>";
											if($insdata['erek']==1){echo"Szabályosnak látszó 3 ér kép";}
											elseif($insdata['erek']==2){echo"kontroll javasolt";}
											echo"</b></td></tr>";
										}
										if($insdata['ductus']!="0"){
											echo"<tr><td style=\"width:250px;\">Ductus:</td><td><b>";
											if($insdata['ductus']==1){echo"Épnek látszik";}
											elseif($insdata['ductus']==2){echo"Nem látszik éppnek";}
											echo"</b></td></tr>";
										}
										if($insdata['ductus_iv']!="0"){
											echo"<tr><td style=\"width:250px;\">Ductus ív:</td><td><b>";
											if($insdata['ductus_iv']==1){echo"Épnek látszik";}
											elseif($insdata['ductus_iv']==2){echo"Nem látszik éppnek";}
											echo"</b></td></tr>";
										}
										if($insdata['lvot']!="0"){
											echo"<tr><td style=\"width:250px;\">LVOT:</td><td><b>";
											if($insdata['lvot']==1){echo"Épnek látszik";}
											elseif($insdata['lvot']==2){echo"Nem látszik éppnek";}
											echo"</b></td></tr>";
										}
										if($insdata['rvot']!="0"){
											echo"<tr><td style=\"width:250px;\">RVOT:</td><td><b>";
											if($insdata['rvot']==1){echo"Épnek látszik";}
											elseif($insdata['rvot']==2){echo"Nem látszik éppnek";}
											echo"</b></td></tr>";
										}

										if($insdata['tudovenak']!="0"){
											echo"<tr><td style=\"width:250px;\">Tüdővénák:</td><td><b>";
											if($insdata['tudovenak']==1){echo"Épnek látszik";}
											elseif($insdata['tudovenak']==2){echo"Nem látszik éppnek";}
											echo"</b></td></tr>";
										}
										if($insdata['hasfal']!="0"){
											echo"<tr><td style=\"width:250px;\">Hasfal:</td><td><b>";
											if($insdata['hasfal']==1){echo"Zártnak látszik";}
											elseif($insdata['hasfal']==2){echo"nyitottnak látszik";}
											echo"</b></td></tr>";
										}
										if($insdata['hasfal_comment']!=""){echo"<tr><td style=\"width:250px;\">Hasfal megjegyzés:</td><td><b>".$insdata['hasfal_comment']."</b></td></tr>";}

										if($insdata['mellkas']!="0"){
											echo"<tr><td style=\"width:250px;\">Mellkas:</td><td><b>";
											if($insdata['mellkas']==1){echo"Zártnak látszik";}
											elseif($insdata['mellkas']==2){echo"nyitottnak látszik";}
											echo"</b></td></tr>";
										}
										if($insdata['mellkas_comment']!=""){echo"<tr><td style=\"width:250px;\">Mellkas megjegyzés:</td><td><b>".$insdata['mellkas_comment']."</b></td></tr>";}
										if($insdata['hasiecho']!="0"){
											echo"<tr><td style=\"width:250px;\">Hasi echo:</td><td><b>";
											if($insdata['hasiecho']==1){echo"Normál echogenitas";}
											elseif($insdata['hasiecho']==2){echo"Nem normál echogenitas";}
											echo"</b></td></tr>";
										}
										if($insdata['rekesz']!="0"){
											echo"<tr><td style=\"width:250px;\">Rekesz:</td><td><b>";
											if($insdata['rekesz']==1){echo"Épnek látszik";}
											elseif($insdata['rekesz']==2){echo"Nem látszik éppnek";}
											echo"</b></td></tr>";
										}
										if($insdata['gyomor']!="0"){
											echo"<tr><td style=\"width:250px;\">Gyomor:</td><td><b>";
											if($insdata['gyomor']==1){echo"Telítődés látható";}
											elseif($insdata['gyomor']==2){echo"Telítődés nem látható";}
											echo"</b></td></tr>";
										}
										if($insdata['holyag']!="0"){
											echo"<tr><td style=\"width:250px;\">Hólyag:</td><td><b>";
											if($insdata['holyag']==1){echo"Telítődés látható";}
											elseif($insdata['holyag']==2){echo"Telítődés nem látható";}
											echo"</b></td></tr>";
										}


										
										if($insdata['jobbvese']!="0"){
											echo"<tr><td style=\"width:250px;\">Jobb vese:</td><td><b>";
											if($insdata['jobbvese']==1){echo"Éppnek látszó";}
											elseif($insdata['jobbvese']==2){echo"pyleon tágulat";}
											echo"</b></td></tr>";
										}
										if($insdata['jobbvese_comment']!=""){echo"<tr><td style=\"width:250px;\">Jobbvese megjegyzés:</td><td><b>".$insdata['jobbvese_comment']."</b></td></tr>";}
										if($insdata['balvese']!="0"){
											echo"<tr><td style=\"width:250px;\">Bal vese:</td><td><b>";
											if($insdata['balvese']==1){echo"Éppnek látszó";}
											elseif($insdata['balvese']==2){echo"pyleon tágulat";}
											echo"</b></td></tr>";
										}
										if($insdata['balvese_comment']!=""){echo"<tr><td style=\"width:250px;\">Balvese megjegyzés:</td><td><b>".$insdata['balvese_comment']."</b></td></tr>";}
										if($insdata['koldokzsinor']!="0"){
											echo"<tr><td style=\"width:250px;\">Köldökzsínór:</td><td><b>";
											if($insdata['koldokzsinor']==1){echo"SUA";}
											elseif($insdata['koldokzsinor']==2){echo"3 ér";}
											echo"</b></td></tr>";
										}
										if($insdata['koldokzsinor_mm']!=""){echo"<tr><td style=\"width:250px;\">Köldökzsinó hossza:</td><td><b>".$insdata['koldokzsinor_mm']."mm </b></td></tr>";}
										if($insdata['terhido_het']!=""){echo"<tr><td style=\"width:250px;\">Terhességi idő:</td><td><b>".$insdata['terhido_het']."hét ".$insdata['terhido_nap']." </b></td></tr>";}
										if($insdata['becsultsuly']!=""){echo"<tr><td style=\"width:250px;\">Becsült súly:</td><td><b>".$insdata['becsultsuly']."g</b></td></tr>";}
										if($insdata['panaszok']!=""){echo"<tr><td style=\"width:250px;\">Panaszok:</td><td><b>".nl2br($insdata['panaszok'])."</b></td></tr>";}
										if($insdata['velemeny']!=""){echo"<tr><td style=\"width:250px;\">Vélemény:</td><td><b>".nl2br($insdata['velemeny'])."</b></td></tr>";}
										if($insdata['javaslat']!=""){echo"<tr><td style=\"width:250px;\">Javaslat:</td><td><b>".nl2br($insdata['javaslat'])."</b></td></tr>";}
										if($insdata['megjegyzes']!=""){echo"<tr><td style=\"width:250px;\">Megjegyzés:</td><td><b>".nl2br($insdata['megjegyzes'])."</b></td></tr>";}
										if($insdata['egyeb']!=""){echo"<tr><td style=\"width:250px;\">Egyéb:</td><td><b>".nl2br($insdata['egyeb'])."</b></td></tr>";}
										if($insdata['holyagnyak']!=""){echo"<tr><td style=\"width:250px;\">Hólyagnyak-Symphozis távolság mérés:</td><td><b>".nl2br($insdata['holyagnyak'])."</b></td></tr>";}
										if($insdata['tappenz']!=""){
											echo"<tr><td style=\"width:250px;\">Táppénz:</td><td><b>";
												if($insdata['tappenz']==1){echo"Tisztelt Kolléga! Kérem szíves táppénzes állományba vételét. Köszönettel";}
												elseif($insdata['tappenz']==2){echo"Tisztelt Kolléga! Kérem szíves további táppénzen tartását. Köszönettel";}
											echo"</b></td></tr>";
										}
										if($insdata['afp_mom']!=""){echo"<tr><td style=\"width:250px;\">AFP /MOM/</td><td><b>".$insdata['afp_mom']." </b></td></tr>";}
										if($insdata['afp_ug']!=""){echo"<tr><td style=\"width:250px;\">AFP /ug/l/</td><td><b>".$insdata['afp_ug']." </b></td></tr>";}
										echo"</table>";

								}
	?>	
							</div>
							<div class="col-md-6">
								<?php
									$pic=db_one("select filename from files where modul='partners' and type='inspection' and type_id='".$myinspection['id']."' order by cover desc");
									if(is_file($pic)){
										echo"<div class=\"col-sm-3\"><img src=\"/".$pic."\" alt=\"avatar\" width=\"450px\"></div>";
									}
									else{
										echo"<img src=\"img/placeholders/avatars/avatar9@2x.jpg\" alt=\"avatar\">";
									}
								?>
							</div>
							
							<div style="clear:both;height:1px;"></div>


						</div>
						<div class="tab-pane" id="tab-2">
						<?
							echo"<form id=\"editinspection\" name=\"editinspection\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
							echo "<div style=\"margin-top:1px;\">";
				
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"patient_id\">Vizsgált páciens:</label>
						<div class=\"col-md-9\">
							<select id=\"patient_id\" name=\"patient_id\" class=\"form-control\" >
							<option value=\"0\">Kérlek válassz</option>";
							for($i=0;$i<count($pat);$i++){
								$sel="";if($myinspection['patient_id']==$pat[$i]['id']){$sel=" selected ";}
								echo"<option value=\"".$pat[$i]['id']."\" ".$sel." disabled>".$pat[$i]['name']." (".$pat[$i]['birthdate']." - TAJ: ".$pat[$i]['tajnum'].") </option>";	
							}
							echo"</select>
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"inspection_date\">Vizsgálati dátum:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"inspection_date\" name=\"inspection_date\" class=\"form-control input-datepicker\" value=\"".$myinspection['inspection_date']."\" />
						</div>
					</div>";
					
					
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"inspection_type\">Vizsgálat típusa:</label>
						<div class=\"col-md-9\">
							<select id=\"inspection_type\" name=\"inspection_type\" class=\"form-control\" onchange=\"javascript:getvizsform('0');\">
							<option value=\"0\">Kérlek válassz</option>";
							for($i=0;$i<count($vt);$i++){
								$sel="";if($myinspection['inspection_type']==$vt[$i]['id']){$sel=" selected ";}
								echo"<option value=\"".$vt[$i]['id']."\" ".$sel." >".$vt[$i]['name']."</option>";	
							}
							echo"</select>
						</div>
					</div>";
					echo"<div id=\"vizsform\"></div>";
				echo "</div>
							</form>";
						?>
						</div>
						<div class="tab-pane" id="tab-3">
								<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$myinspection['id'],"Dokumentum feltöltés")."</div>";
								?> 
								<div id="filelists"></div>
								<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$myinspection['id'],"Hozzászólások")."</div>";
								?>
						</div>
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>
						
						
				
					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
					
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/medical/js/inc_inspection.js"></script>

			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$myinspection['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$myinspection['id'];?>,'Dokumentum feltöltés','filelists');
				getvizsform('<?=$myinspection['id'];?>')
			</script>
			<script>$(function(){ inspectionDatatables.init(); });</script>	
			
			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
		//print_r($_SESSION['planetsys'])
?>    
	<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Vizsgálat lista</a></li>
			</ul>
			<!-- END Datatables Header -->

			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Vizsgálat</strong> lista</h2>
				</div>

				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							//echo"<th>Művelet</th>";
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/medical/js/inc_inspection.js"></script>
		<script>$(function(){ inspectionDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>
