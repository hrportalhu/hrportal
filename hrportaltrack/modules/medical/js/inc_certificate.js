/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */
var certificateDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/medical/certificate/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"aaSorting": [[ 1, "asc" ]],
				"iDisplayLength": 30,
				"bDestroy": true,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/medical/ajax_certificate_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					aoData.push({"name": "categories_id", "value":$("#categories_id").val() });
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });
            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveeditcertificate(elemid){
	var valid = f_c($('#editcertificate'));
	if(valid){
		f_xml(ServerName +'/modules/medical/ajax_certificate_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editcertificate').serialize(),'');
	}
}

function savenewcertificate(){
	var valid = f_c($('#newcertificate'));
	if(valid){
		f_xml(ServerName +'/modules/medical/ajax_certificate_data.php','newsubmit=1&' + $('#newcertificate').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/medical/ajax_certificate_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}

function getcertificatetext(){
	//alert($('#certificate_type').val());
	if($('#certtype_id').val()=="1"){
		$("#name").html("Igazolom, hogy fent nevezett utolsó menzesze első napja ........................... volt, ez alapján a fogantatási időpont ......................... .  A szülés várható ideje ........................... (+/- 2 hét). Ezen igazolást az Apasági elismerő nyilatkozat tételéhez állítottuk ki.");
	}	
	else if($('#certtype_id').val()=="2"){
		$("#name").html("Igazolom, hogy fent nevezett utolsó menzesze ......................... volt, ez alapján terhességének 91. napját ............................. töltötte be. A szülés várható ideje .................. (+/- 2 hét). Ezen igazolást családi adókedvezmény igénybevételéhez állítottuk ki.");
	}	
	else if($('#certtype_id').val()=="3"){
		$("#name").html("Igazolom, hogy fent nevezett utolsó menzesze .......................... volt, ez alapján terhességének 36. hetét ......................... töltötte be. A szülés várható ideje ..................... (+/- 2 hét).  Ezen igazolást GYÁS igényléséhez állítottuk ki.");
	}	
	else if($('#certtype_id').val()=="4"){
		$("#name").html("Igazolom, hogy fent nevezett várandós kismama tornázhat, mozoghat.");
	}	
	else if($('#certtype_id').val()=="5"){
		$("#name").html("Igazolom, hogy fent nevezett várandós kismama repülőgépen utazhat.");
	}	
	else if($('#certtype_id').val()=="6"){
		$("#name").html("");
	}	
	else if($('#certtype_id').val()=="7"){
		$("#name").html("Alulírott tájékoztatást kaptam a beavatkozás lényegéről. Arról, hogy egy katéteren keresztül kontraszt anyagot juttatnak a petevezetőmbe, ill. a méhembe, aminek útját UH-val követik.  A beavatkozás után enyhe hasi fájdalom, diszkomfort érzés jelentkezhet. Láz, súlyos alhasi fájdalom esetén orvosomnál kell jelentkeznem. Ezek ismeretében a vizsgálathoz hozzájárulok.");
	}	
	else if($('#certtype_id').val()=="0"){
		$("#name").html("Kérem válasszon");
	}	
}
