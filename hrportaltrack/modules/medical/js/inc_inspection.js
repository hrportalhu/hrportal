/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */
var inspectionDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/medical/inspection/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"aaSorting": [[ 0, "desc" ]],
				"iDisplayLength": 30,
				"bDestroy": true,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/medical/ajax_inspection_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					aoData.push({"name": "categories_id", "value":$("#categories_id").val() });
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });
            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveeditinspection(elemid){
	var valid = f_c($('#editinspection'));
	if(valid){
		f_xml(ServerName +'/modules/medical/ajax_inspection_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editinspection').serialize(),'');
	}
}

function savenewinspection(){
	if($('#patient_id').val()!=0){
		f_xml(ServerName +'/modules/medical/ajax_inspection_data.php','newsubmit=1&' + $('#newinspection').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/medical/ajax_inspection_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}

function getvizsform(elemid){
	
		$.ajax({
			type:'POST',
			url: ServerName +'/modules/medical/ajax_getvizsform.php',
			data:'elemid='+elemid+'&inspection_type='+$('#inspection_type').val(),
			success: function(data){
				$('#vizsform').html(data);
				$("#diag_id").autocomplete({
					 source: ServerName+"/modules/medical/ajax_diagnosis_term.php",
					 minLength: 0,
					 select: function(event, ui) {
							$("#diag_id").val(ui.item.value);
							$("#diag_idid").val(ui.item.id);
							
						}
				});
				$('#utmensz').datepicker({weekStart: 1,closeText: 'bezárás',prevText: '&laquo;&nbsp;vissza',nextText: 'előre&nbsp;&raquo;',currentText: 'ma',monthNames: ['Január', 'Február', 'Március', 'Április', 'Május', 'Június','Július', 'Augusztus', 'Szeptember', 'Október', 'November', 'December'],	monthNamesShort: ['Jan', 'Feb', 'Már', 'Ápr', 'Máj', 'Jún',	'Júl', 'Aug', 'Szep', 'Okt', 'Nov', 'Dec'],	dayNames: ['Vasárnap', 'Hétfö', 'Kedd', 'Szerda', 'Csütörtök', 'Péntek', 'Szombat'],dayNamesShort: ['Vas', 'Hét', 'Ked', 'Sze', 'Csü', 'Pén', 'Szo'],dayNamesMin: ['V', 'H', 'K', 'Sze', 'Cs', 'P', 'Szo'],weekHeader: 'Hé',changeYear: 1,dateFormat: 'yy-mm-dd',firstDay: 1,isRTL: false,showMonthAfterYear: true,yearSuffix: ''});
				$('#nuchaszures').datepicker({weekStart: 1,closeText: 'bezárás',prevText: '&laquo;&nbsp;vissza',nextText: 'előre&nbsp;&raquo;',currentText: 'ma',monthNames: ['Január', 'Február', 'Március', 'Április', 'Május', 'Június','Július', 'Augusztus', 'Szeptember', 'Október', 'November', 'December'],	monthNamesShort: ['Jan', 'Feb', 'Már', 'Ápr', 'Máj', 'Jún',	'Júl', 'Aug', 'Szep', 'Okt', 'Nov', 'Dec'],	dayNames: ['Vasárnap', 'Hétfö', 'Kedd', 'Szerda', 'Csütörtök', 'Péntek', 'Szombat'],dayNamesShort: ['Vas', 'Hét', 'Ked', 'Sze', 'Csü', 'Pén', 'Szo'],dayNamesMin: ['V', 'H', 'K', 'Sze', 'Cs', 'P', 'Szo'],weekHeader: 'Hé',changeYear: 1,dateFormat: 'yy-mm-dd',firstDay: 1,isRTL: false,showMonthAfterYear: true,yearSuffix: ''});
				$('#kontrol').datepicker({weekStart: 1,closeText: 'bezárás',prevText: '&laquo;&nbsp;vissza',nextText: 'előre&nbsp;&raquo;',currentText: 'ma',monthNames: ['Január', 'Február', 'Március', 'Április', 'Május', 'Június','Július', 'Augusztus', 'Szeptember', 'Október', 'November', 'December'],	monthNamesShort: ['Jan', 'Feb', 'Már', 'Ápr', 'Máj', 'Jún',	'Júl', 'Aug', 'Szep', 'Okt', 'Nov', 'Dec'],	dayNames: ['Vasárnap', 'Hétfö', 'Kedd', 'Szerda', 'Csütörtök', 'Péntek', 'Szombat'],dayNamesShort: ['Vas', 'Hét', 'Ked', 'Sze', 'Csü', 'Pén', 'Szo'],dayNamesMin: ['V', 'H', 'K', 'Sze', 'Cs', 'P', 'Szo'],weekHeader: 'Hé',changeYear: 1,dateFormat: 'yy-mm-dd',firstDay: 1,isRTL: false,showMonthAfterYear: true,yearSuffix: ''});
				$('#kontroll').datepicker({weekStart: 1,closeText: 'bezárás',prevText: '&laquo;&nbsp;vissza',nextText: 'előre&nbsp;&raquo;',currentText: 'ma',monthNames: ['Január', 'Február', 'Március', 'Április', 'Május', 'Június','Július', 'Augusztus', 'Szeptember', 'Október', 'November', 'December'],	monthNamesShort: ['Jan', 'Feb', 'Már', 'Ápr', 'Máj', 'Jún',	'Júl', 'Aug', 'Szep', 'Okt', 'Nov', 'Dec'],	dayNames: ['Vasárnap', 'Hétfö', 'Kedd', 'Szerda', 'Csütörtök', 'Péntek', 'Szombat'],dayNamesShort: ['Vas', 'Hét', 'Ked', 'Sze', 'Csü', 'Pén', 'Szo'],dayNamesMin: ['V', 'H', 'K', 'Sze', 'Cs', 'P', 'Szo'],weekHeader: 'Hé',changeYear: 1,dateFormat: 'yy-mm-dd',firstDay: 1,isRTL: false,showMonthAfterYear: true,yearSuffix: ''});
			}});

}
