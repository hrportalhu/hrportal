/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */
var patientDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/medical/patient/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"aaSorting": [[ 1, "asc" ]],
				"iDisplayLength": 30,
				"bDestroy": true,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/medical/ajax_patient_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					aoData.push({"name": "categories_id", "value":$("#categories_id").val() });
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });
            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveeditpatient(elemid){
	var valid = f_c($('#editpatient'));
	if(valid){
		f_xml(ServerName +'/modules/medical/ajax_patient_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editpatient').serialize(),'');
	}
}

function savenewpatient(){
	var valid = f_c($('#newpatient'));
	if(valid){
		f_xml(ServerName +'/modules/medical/ajax_patient_data.php','newsubmit=1&' + $('#newpatient').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/medical/ajax_patient_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}

