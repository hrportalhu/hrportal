
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Páciensek
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/medical/patient">Páciens kezelő</a></li>
				<li><a href="/new">Új páciens rögzítése</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új páciens rögzítése</strong></h2>
				</div>
			<?
				
				echo "<form id=\"newpatient\" name=\"newpatient\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";
				
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"name\">Név:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" placeholder=\"Páciens neve\" />																		
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"birthname\">Születési neve:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"birthname\" name=\"birthname\" class=\"form-control\" placeholder=\"Születési neve\" />																		
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"mothername\">Anyja neve:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"mothername\" name=\"mothername\" class=\"form-control\" placeholder=\"Anyja neve\" />																		
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"birthdate\">Születési dátum:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"birthdate\" name=\"birthdate\" class=\"form-control input-datepicker\" placeholder=\"YYYY-MM-DD\" />
						</div>
					</div>";

					
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"tajnum\">TAJ szám:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"tajnum\" name=\"tajnum\" class=\"form-control\" placeholder=\"TAJ száma\" />																		
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"phone\">Telefonszám:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"phone\" name=\"phone\" class=\"form-control\" placeholder=\"Telefon száma\" />																		
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"address\">Címe:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"address\" name=\"address\" class=\"form-control\" placeholder=\"Címe\" />																		
						</div>
					</div>";
				
					
					echo "<div class=\"form-group\" >
						<label class=\"col-md-3 control-label\" for=\"status\">Érvényes-e?:</label>
						<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span></label>
					</div>";
					
					echo "<div style=\"clear:both;height:10px;\"></div>";
					
					echo "<div class=\"form-group form-actions\">
						<div class=\"col-md-9 \">
							<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewpatient();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
						</div>
					</div>";
					
				echo "</div>
				</form>";
			?>
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/medical/js/inc_patient.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?
		}
		else{
			
			//echo "select * from functions where id='".$uritags[2]."'";
			$mypatient=db_row("select * from med_patients where id='".$uritags[3]."'");
			if($mypatient['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="modules/medical/patient">Páciensek</a></li>
				<li><a href="/<?=$URI;?>"><?=$mypatient['name'];?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->
					<div class="block-title">
						<h2><strong><?=$mypatient['name'];?></strong> személy oldala</h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
							if(priv("patient","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("patient","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							if(priv("patient","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("patient","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
							if(priv("patient","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}	
							?>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="col-md-6">
								<dl class="dl-horizontal">
									<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$mypatient['id']); ?></dd>
									<dt>Név:</dt><dd><? echo $mypatient['name']; ?></dd>
									<dt>Születési neve:</dt><dd><? echo $mypatient['birthname']; ?></dd>
									<dt>Születési dátum:</dt><dd><? echo $mypatient['birthdate']; ?></dd>
									<dt>Édesanyja neve:</dt><dd><? echo $mypatient['mothername']; ?></dd>
									<dt>TAJ szám:</dt><dd><? echo $mypatient['tajnum']; ?></dd>
									<dt>Telefonszáma:</dt><dd><? echo $mypatient['phone']; ?></dd>
									<dt>Címe:</dt><dd><? echo $mypatient['address']; ?></dd>
									<dt>Státusz:</dt><dd><? if($mypatient['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
								</dl>	
							</div>
							<div class="col-md-6">
								<?
									$vizsg=db_all("select * from med_inspection_header where patient_id='".$mypatient['id']."' order by inspection_date desc");
									echo"<dl class=\"dl-horizontal\">";
									for($i=0;$i<count($vizsg);$i++){
										echo"<dt>".$vizsg[$i]['inspection_date']."</dt><dd><a href=\"/modules/medical/inspection/".$vizsg[$i]['id']."\">".idchange("med_inspection_types","name",$vizsg[$i]['inspection_type'])."</a></dd>";
									}
									echo"<dt><a href=\"/modules/medical/inspection/new/".$mypatient['id']."\">Új vizsgálat</a></dt>";
									echo"</dl>";
								?>
							</div>
							
							<div style="clear:both;height:1px;"></div>


						</div>
						<div class="tab-pane" id="tab-2">
						<?
							echo"<form id=\"editpatient\" name=\"editcontacs\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
							echo "<div style=\"margin-top:1px;\">";
							
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"name\">Név:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" value=\"".$mypatient['name']."\" />																		
									</div>
								</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"birthname\">Születési neve:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"birthname\" name=\"birthname\" class=\"form-control\" value=\"".$mypatient['birthname']."\" />																		
									</div>
								</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"mothername\">Anyja neve:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"mothername\" name=\"mothername\" class=\"form-control\" value=\"".$mypatient['mothername']."\" />																		
									</div>
								</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"birthdate\">Születési dátum:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"birthdate\" name=\"birthdate\" class=\"form-control input-datepicker\" value=\"".$mypatient['birthdate']."\" />
									</div>
								</div>";

								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"tajnum\">TAJ szám:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"tajnum\" name=\"tajnum\" class=\"form-control\" value=\"".$mypatient['tajnum']."\" />																		
									</div>
								</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"phone\">Telefonszám:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"phone\" name=\"phone\" class=\"form-control\" value=\"".$mypatient['phone']."\" />																		
									</div>
								</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"address\">Címe:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"address\" name=\"address\" class=\"form-control\" value=\"".$mypatient['address']."\" />																		
									</div>
								</div>";
								echo "<div style=\"clear:both;height:10px;\"></div>";
								
								echo "<div class=\"form-group form-actions\">
									<div class=\"col-md-9\">
										<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditpatient('".$mypatient['id']."');\"><i class=\"fa fa-angle-right\"></i> Ment</button>
									</div>
								</div>";
							
							echo "</div>
							</form>";
						?>
						</div>
						<div class="tab-pane" id="tab-3">
								<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mypatient['id'],"Dokumentum feltöltés")."</div>";
								?> 
								<div id="filelists"></div>
								<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mypatient['id'],"Hozzászólások")."</div>";
								?>
						</div>
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>
						
						
				
					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
					
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/medical/js/inc_patient.js"></script>

			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mypatient['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$mypatient['id'];?>,'Dokumentum feltöltés','filelists');

			</script>
			<script>$(function(){ patientDatatables.init(); });</script>	
			
			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
		//print_r($_SESSION['planetsys'])
?>    
	<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Páciens lista</a></li>
			</ul>
			<!-- END Datatables Header -->

			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Páciens</strong> lista</h2>
				</div>

				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							//echo"<th>Művelet</th>";
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/medical/js/inc_patient.js"></script>
		<script>$(function(){ patientDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>
