<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors','Off');
header("Cache-control: private");
header('Content-Type: text/plain;charset=utf-8');
include_once dirname(__FILE__).("/../../inc/sys.conf");
include_once dirname(__FILE__).("/../../inc/_functions.php");


?>

	<h4>Árkönyvhöz tétel hozzáadása</h4>
	<div style="clear:both;height:1px;"></div>
	<div class="form-group" style="width:300px;float:left;">
		<label class="col-md-6 control-label" for="newitem">Árkönyv tétel</label>
		<div class="col-md-6">
			<input type="text" id="newpriceitem" name="newpriceitem" class="form-control" value="" />
			<input type="hidden" id="newpriceitem_id" name="newpriceitem_id" class="form-control" value="" />
		</div>
	</div>

	<div class="form-group" style="width:200px;float:left;">
		<label class="col-md-4 control-label" for="price_eur">Ár-EUR</label>
		<div class="col-md-8" >
			<input type="text" id="price_eur" name="price_eur" class="form-control" value="" />
		</div>
	</div>
	
	<div class="form-group" style="width:200px;float:left;">	
		<?
		
		if($_REQUEST['book_id']=="newpdiv"){
			echo"<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"savenewbutton\" onclick=\"javascript:addnewpricelement();\"><i class=\"fa fa-angle-right\"></i> Felvesz</button>";
		}
		else{
			echo"<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"savenewbutton\" onclick=\"javascript:addnewpricelementtodb('".$_REQUEST['book_id']."');\"><i class=\"fa fa-angle-right\"></i> Felvesz</button>";
		}
		?>
	</div>


	<div style="clear:both;height:1px;"></div>

