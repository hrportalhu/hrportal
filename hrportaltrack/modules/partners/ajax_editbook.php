<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors','Off');
header("Cache-control: private");
header('Content-Type: text/plain;charset=utf-8');
include_once dirname(__FILE__).("/../../inc/sys.conf");
include_once dirname(__FILE__).("/../../inc/_functions.php");

$fizmod=db_all("select id, name from bookkeepsystem_payments where status=1 order by name");
$b=db_row("select * from partner_book where id='".$_REQUEST['editid']."' ");
?>	

<input type="hidden" id="parner_id" name="partner_id" value="<?=$b['partner_id'];?>" />
 <div class="row">
	<div class="col-sm-4">
		<div class="block">
			<div class="block-title">
				<h2><strong>Törzs</strong> Adat</h2>
			</div>
			<div class="form-group" >
				<label class="col-md-3 control-label" for="adoszam">Adószám:</label>
				<div class="col-md-9">
					<input type="text" id="adoszam" name="adoszam" class="form-control" value="<?=$b['adoszam'];?>" />
				</div>
			</div>
			
			<div class="form-group" >
				<label class="col-md-3 control-label" for="euadoszam">EU adószám:</label>
				<div class="col-md-9">
					<input type="text" id="euadoszam" name="euadoszam" class="form-control" value="<?=$b['euadoszam'];?>" />
				</div>
			</div>
			
			<div class="form-group" >
				<label class="col-md-3 control-label" for="iban">IBAN:</label>
				<div class="col-md-9">
					<input type="text" id="iban" name="iban" class="form-control" value="<?=$b['iban'];?>" />
				</div>
			</div>
	<div style="clear:both;height:1px;"></div>
		
        </div>       
    </div>       
	<div class="col-sm-4">
		<div class="block">
			<div class="block-title">
				<h2><strong>Bank</strong> Adat</h2>
			</div>
				<div class="form-group" >
					<label class="col-md-3 control-label" for="bname">Bank neve:</label>
					<div class="col-md-9">
						<input type="text" id="bname" name="bname" class="form-control" value="<?=$b['name'];?>" />
					</div>
				</div>
				
				
				<div class="form-group" >
					<label class="col-md-3 control-label" for="szamlak_szama">Számla száma:</label>
					<div class="col-md-9">
						<input type="text" id="szamlak_szama" name="szamlak_szama" class="form-control" value="<?=$b['szamlak_szama'];?>" />
					</div>
				</div>
				
				<div class="form-group" >
					<label class="col-md-3 control-label" for="swift">Swift:</label>
					<div class="col-md-9">
						<input type="text" id="swift" name="swift" class="form-control"  value="<?=$b['swift'];?>"/>
					</div>
				</div>
			<div style="clear:both;height:1px;"></div>
        </div>       
    </div>       
	<div class="col-sm-4">
		<div class="block">
			<div class="block-title">
				<h2><strong>Egyéb</strong> Adat</h2>
			</div>
			<div class="form-group" >
				<label class="col-md-3 control-label" for="fizmod">Fizetési feltétel:</label>
				<div class="col-md-9">
					<select id="fizmod" name="fizmod" class="form-control" >
						<option value="0">Kérlek válassz</option>
						<?
							for($i=0;$i<count($fizmod);$i++){
								$ch="";if($fizmod[$i]['id']==$b['fizmod']){$ch=" selected ";}
								echo"<option value=\"".$fizmod[$i]['id']."\" ".$ch.">".$fizmod[$i]['name']."</option>";	
							}
						?>
					</select>
				</div>
			</div>
			
			<div class="form-group" >
				<label class="col-md-3 control-label" for="adostatus">Alértelmezett cikk adóstátusza:</label>
				<div class="col-md-9">
					<input type="text" id="adostatus" name="adostatus" class="form-control" value="<?=$b['adostatus'];?>" />
				</div>
			</div>
			
			<div class="form-group" >
				<label class="col-md-3 control-label" for="napikamat">Napi kamat:</label>
				<div class="col-md-9">
					<input type="text" id="napikamat" name="napikamat" class="form-control" value="<?=$b['napikamat'];?>" />
				</div>
			</div>
			<div style="clear:both;height:1px;"></div>
        </div>       
    </div>       
</div>       
                

	
	
	

	
	
	<div style="clear:both;height:10px;"></div>
	<div class="col-md-9 col-md-offset-0">
		
		<button type="button" class="btn btn-sm btn-primary" id="savenewbutton" onclick="javascript:saveeditbook(<?=$b['id'];?>,<?=$_REQUEST['elemid'];?>,'elementdiv11');"><i class="fa fa-angle-right"></i> Ment</button>
	</div>
	<div style="clear:both;"></div>

<?

mysql_close($connid);
?>
