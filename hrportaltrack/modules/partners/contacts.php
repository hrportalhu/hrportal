
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Kapcsolat kezelő
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/partners/contacts">Kapcsolat kezelő</a></li>
				<li><a href="/new">Új kapcsolat felvitele</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új kapcsolat felvitele</strong></h2>
				</div>
			<?
				$beoszt=db_all("select id, name from contact_categories where status=1 order by name");
				$ceg=db_all("select id, name from partners where status=1 order by name");
				
				echo "<form id=\"newcontacts\" name=\"newcontacts\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";
				
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"name\">Név:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" placeholder=\"Ön neve\" />																		
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"birthname\">Születési név:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"birthname\" name=\"birthname\" class=\"form-control\" placeholder=\"Születési neve\" />																		
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"birthdate\">Születési dátum:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"birthdate\" name=\"birthdate\" class=\"form-control input-datepicker\" placeholder=\"YYYY-MM-DD\" />
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"mothersname\">Édesanyja neve:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"mothersname\" name=\"mothersname\" class=\"form-control\" placeholder=\"Édesanyja neve\" />																		
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"taxnum\">Adószám:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"taxnum\" name=\"taxnum\" class=\"form-control\" placeholder=\"Adószáma\" />																		
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"tajnum\">TAJ szám:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"tajnum\" name=\"tajnum\" class=\"form-control\" placeholder=\"TAJ száma\" />																		
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"idnum\">Személyi igazolványszám:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"idnum\" name=\"idnum\" class=\"form-control\" placeholder=\"Személyi igazolványszáma\" />																		
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"passportnum\">Útlevélszám:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"passportnum\" name=\"passportnum\" class=\"form-control\" placeholder=\"Útlevélszáma\" />																		
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"partner_id\">Cége:</label>
						<div class=\"col-md-9\">
							<select id=\"partner_id\" name=\"partner_id\" class=\"form-control\" >
							<option value=\"0\">Kérlek válassz</option>";
							for($i=0;$i<count($ceg);$i++){
								echo"<option value=\"".$ceg[$i]['id']."\" >".$ceg[$i]['name']."</option>";	
							}
							echo"</select>
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"categories_id\">Beosztás:</label>
						<div class=\"col-md-9\">
							<select id=\"categories_id\" name=\"categories_id\" class=\"form-control\" placeholder=\"Beosztása\" >
							<option value=\"0\">Kérlek válassz</option>";
							for($i=0;$i<count($beoszt);$i++){
								echo"<option value=\"".$beoszt[$i]['id']."\" >".$beoszt[$i]['name']."</option>";	
							}
							echo"</select>
						</div>
					</div>";
					
					echo "<div class=\"form-group\" >
						<label class=\"col-md-3 control-label\" for=\"status\">Érvényes-e?:</label>
						<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span></label>
					</div>";
					
					echo "<div style=\"clear:both;height:10px;\"></div>";
					
					echo "<div class=\"form-group form-actions\">
						<div class=\"col-md-9 col-md-offset-3\">
							<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewcontacts();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
						</div>
					</div>";
					
				echo "</div>
				</form>";
			?>
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/partners/js/inc_contacts.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?
		}
		else{
			
			//echo "select * from functions where id='".$uritags[2]."'";
			$mycontacts=db_row("select * from contacts where id='".$uritags[3]."'");
			if($mycontacts['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="modules/partners/contacts">Partnerek</a></li>
				<li><a href="/<?=$URI;?>"><?=$mycontacts['name'];?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->
					<div class="block-title">
						<h2><strong><?=$mycontacts['name'];?></strong> személy oldala</h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
							if(priv("contacts","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("contacts","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							if(priv("contacts","address")){echo"<li><a href=\"#tab-6\" data-toggle=\"tooltip\" title=\"address\"><i class=\"fa fa-home\"></i></a></li>";}
							if(priv("contacts","connectpartners")){echo"<li><a href=\"#tab-7\" data-toggle=\"tooltip\" title=\"connectpartners\"><i class=\"fa fa-users\"></i></a></li>";}
							if(priv("contacts","connecttypes")){echo"<li><a href=\"#tab-8\" data-toggle=\"tooltip\" title=\"connecttypes\"><i class=\"fa fa-sitemap\"></i></a></li>";}
							if(priv("contacts","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("contacts","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
							if(priv("contacts","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}	
							?>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="col-md-6">
								<dl class="dl-horizontal">
									<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$mycontacts['id']); ?></dd>
									<dt>Név:</dt><dd><? echo $mycontacts['name']; ?></dd>
									<dt>Születési neve:</dt><dd><? echo $mycontacts['birthname']; ?></dd>
									<dt>Születési dátum:</dt><dd><? echo $mycontacts['birthdate']; ?></dd>
									<dt>Édesanyja neve:</dt><dd><? echo $mycontacts['mothersname']; ?></dd>
									<dt>Adószám:</dt><dd><? echo $mycontacts['taxnum']; ?></dd>
									<dt>TAJ szám:</dt><dd><? echo $mycontacts['tajnum']; ?></dd>
									<dt>Személyigazolvány szám:</dt><dd><? echo $mycontacts['idnum']; ?></dd>
									<dt>Útlevélszám:</dt><dd><? echo $mycontacts['passportnum']; ?></dd>
									<dt>Cége:</dt><dd><? echo idchange("partners","name",$mycontacts['partner_id']); ?></dd>
									<dt>Beosztás:</dt><dd><? echo idchange("contact_categories","name",$mycontacts['categories_id']); ?></dd>
									<dt>Státusz:</dt><dd><? if($mycontacts['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
								</dl>	
							</div>
							<div class="col-md-6">
								<?php
									$pic=db_one("select filename from files where modul='partners' and type='contacts' and type_id='".$mycontacts['id']."' order by cover desc");
									if(is_file($pic)){
										echo"<div class=\"col-sm-3\"><img src=\"/".$pic."\" alt=\"avatar\" width=\"450px\"></div>";
									}
									else{
										echo"<img src=\"img/placeholders/avatars/avatar9@2x.jpg\" alt=\"avatar\">";
									}
								?>
							</div>
							
							<div style="clear:both;height:1px;"></div>
							<div class="block hidden-lt-ie9">
								<div class="block-title">
									<h2><strong>Címek</strong> </h2>
								</div>
								<div id="elementdiv5"></div>
								
							</div>
							<div class="block hidden-lt-ie9">
								<div class="block-title">
									<h2><strong>Elérhetőségek</strong> </h2>
								</div>
								<div id="elementdiv6"></div>
								
							</div>
							<div class="block hidden-lt-ie9">
								<div class="block-title">
									<h2><strong>Kapcsolódó cégek</strong> </h2>
								</div>
								<div id="elementdiv7"></div>
								
							</div>

						</div>
						<div class="tab-pane" id="tab-2">
						<?
							$beoszt=db_all("select id, name from contact_categories where status=1 order by name");
							$ceg=db_all("select id, name from partners where status=1 order by name");
							
							echo"<form id=\"editcontacts\" name=\"editcontacs\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
							echo "<div style=\"margin-top:1px;\">";
							
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"name\">Név:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" value=\"".$mycontacts['name']."\" />																		
									</div>
								</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"birthname\">Születési neve:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"birthname\" name=\"birthname\" class=\"form-control\" value=\"".$mycontacts['birthname']."\" />																		
									</div>
								</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"birthdate\">Születési dátum:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"birthdate\" name=\"birthdate\" class=\"form-control input-datepicker\" value=\"".$mycontacts['birthdate']."\" />																		
									</div>
								</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"mothersname\">Édesanyja neve:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"mothersname\" name=\"mothersname\" class=\"form-control\" value=\"".$mycontacts['mothersname']."\" />																		
									</div>
								</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"taxnum\">Adószám:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"taxnum\" name=\"taxnum\" class=\"form-control\" value=\"".$mycontacts['taxnum']."\" />																		
									</div>
								</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"tajnum\">TAJ szám:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"tajnum\" name=\"tajnum\" class=\"form-control\" value=\"".$mycontacts['tajnum']."\" />																		
									</div>
								</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"idnum\">Személyi igazolványszám:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"idnum\" name=\"idnum\" class=\"form-control\" value=\"".$mycontacts['idnum']."\" />																		
									</div>
								</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"passportnum\">Útlevélszám:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"passportnum\" name=\"passportnum\" class=\"form-control\" value=\"".$mycontacts['passportnum']."\" />																		
									</div>
								</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"partner_id\">Cége:</label>
									<div class=\"col-md-9\">
										<select id=\"partner_id\" name=\"partner_id\" class=\"form-control\" >
										<option value=\"0\">Kérlek válassz</option>";
										for($i=0;$i<count($ceg);$i++){
											$sel="";if($ceg[$i]['id']==$mycontacts['partner_id']){$sel=" selected ";}
											echo"<option value=\"".$ceg[$i]['id']."\" ".$sel.">".$ceg[$i]['name']."</option>";
										}
										echo"</select>
									</div>
								</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"categories_id\">Beosztás:</label>
									<div class=\"col-md-9\">
										<select id=\"categories_id\" name=\"categories_id\" class=\"form-control\" >
										<option value=\"0\">Kérlek válassz</option>";
										for($i=0;$i<count($beoszt);$i++){
											$se2="";if($beoszt[$i]['id']==$mycontacts['categories_id']){$se2=" selected ";}
											echo"<option value=\"".$beoszt[$i]['id']."\" ".$se2.">".$beoszt[$i]['name']."</option>";
										}
										echo"</select>
									</div>
								</div>";
								
								echo "<div class=\"form-group\" >
									<label class=\"col-md-3 control-label\" for=\"status\">Érvényes-e?:</label>";
									$st="";if($mycontacts['status']==1){ $st=" checked "; }
									echo "<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\"".$st." ><span></span></label>
								</div>";
								
								echo "<div style=\"clear:both;height:10px;\"></div>";
								
								echo "<div class=\"form-group form-actions\">
									<div class=\"col-md-9 col-md-offset-3\">
										<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditcontacts('".$mycontacts['id']."');\"><i class=\"fa fa-angle-right\"></i> Ment</button>
									</div>
								</div>";
							
							echo "</div>
							</form>";
						?>
						</div>
						<div class="tab-pane" id="tab-3">
								<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mycontacts['id'],"Dokumentum feltöltés")."</div>";
								?> 
								<div id="filelists"></div>
								<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mycontacts['id'],"Hozzászólások")."</div>";
								?>
						</div>
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>
						
						<div class="tab-pane" id="tab-8">
						<?
							$addtype=db_all("select id, name from partner_connect_categories where status=1 order by name");
						?>
							<div class="block hidden-lt-ie9 <?=$adc;?>" >
								<div class="block-title">
									<h2><strong>Kapcsolat űrlap</strong> </h2>
								</div>
								<form  id="newelementconnpform" id="newelementconnpform" class="form-horizontal form-bordered" onsubmit="return false;">
									<div class="form-group" style="width:300px;float:left;">
										<label class="col-md-3 control-label" for="category_id">Típusok:</label>
										<div class="col-md-9">
											<select id="category_id" name="category_id" class="form-control" >
												<option value="0">Kérlek válassz</option>
												<?
													for($i=0;$i<count($addtype);$i++){
														echo"<option value=\"".$addtype[$i]['id']."\">".$addtype[$i]['name']."</option>";	
													}
												?>
											</select>
										</div>
									</div>
									<div class="form-group" style="width:400px;float:left;">
										<label class="col-md-3 control-label" for="category_val">érték:</label>
										<div class="col-md-9">
											<input type="text" id="category_val" name="category_val" class="form-control" placeholder="kapcsolat tartalma" />
										</div>
									</div>
									<div style="clear:both;height:10px;"></div>
									<div class="col-md-9 col-md-offset-0">
										<button type="button" class="btn btn-sm btn-primary" id="savenewbutton" onclick="javascript:addcontactconnect(<?=$mycontacts['id'];?>,'elementdiv3');"><i class="fa fa-angle-right"></i> Ment</button>
									</div>
									<div style="clear:both;"></div>
								</form>	
								<div style="clear:both;height:10px;"></div>
							</div>
							<div class="block hidden-lt-ie9">
								<div class="block-title">
									<h2><strong>Elérhetőségek</strong></h2>
								</div>
								<div id="elementdiv3"></div>
							</div>
						</div>
						
						<div class="tab-pane" id="tab-7">
						
							<div class="block hidden-lt-ie9 <?=$adc;?>" >
								<div class="block-title">
									<h2><strong>Partner ürlap</strong> </h2>
								</div>
								<form  id="newelementconnpform" id="newelementconnpform" class="form-horizontal form-bordered" onsubmit="return false;">
									<div class="form-group" style="width:400px;float:left;">
										<label class="col-md-3 control-label" for="partnercompl">Kapcsolódó cégek:</label>
										<div class="col-md-9">
											<input type="text" id="partnercompl" name="partnercompl" class="form-control"  />
											<input type="hidden" id="partnercompl_id" name="partnercompl_id"   />
										</div>
									</div>
									<div style="clear:both;height:10px;"></div>
									<div class="col-md-9 col-md-offset-0">
										<button type="button" class="btn btn-sm btn-primary" id="savenewbutton" onclick="javascript:addconxconn(<?=$mycontacts['id'];?>,'elementdiv4');"><i class="fa fa-angle-right"></i> Ment</button>
									</div>
									<div style="clear:both;"></div>
								</form>	
								<div style="clear:both;height:10px;"></div>
							</div>
							<div class="block hidden-lt-ie9">
								<div class="block-title">
									<h2><strong>Kapcsolódó cégek</strong></h2>
								</div>
								<div id="elementdiv4"></div>
							</div>
						</div>
						
						<div class="tab-pane" id="tab-6">

							<?
								$addtype=db_all("select id, name from partner_address_type where status=1 order by name");
							?>
							
							   <div class="block hidden-lt-ie9 <?=$adc;?>" >
								<!-- Switches Title -->
								<div class="block-title">
									<h2><strong>Cím űrlap</strong> </h2>
								</div>
								<!-- END Switches Title -->

								<!-- Switches Content -->
								<form  id="newelementaddpform" id="newelementaddpform" class="form-horizontal form-bordered" onsubmit="return false;">
									
									<div class="form-group" style="width:400px;float:left;">
										<label class="col-md-3 control-label" for="name">Cím elnevezése</label>
										<div class="col-md-9">
											<input type="text" id="name" name="name" class="form-control" value="" placeholder="a cím elnevezése"/>
										</div>
									</div>

									<div class="form-group" style="width:300px;float:left;">
										<label class="col-md-3 control-label" for="zip">Irányító:</label>
										<div class="col-md-9">
											<input type="text" id="zip" name="zip" class="form-control" value="" />
										</div>
									</div>

									<div class="form-group" style="width:300px;float:left;">
										<label class="col-md-3 control-label" for="city">Város:</label>
										<div class="col-md-9">
											<input type="text" id="city" name="city" class="form-control" value="" />
										</div>
									</div>

									<div class="form-group" style="width:400px;float:left;">
										<label class="col-md-3 control-label" for="address">További címadat:</label>
										<div class="col-md-9">
											<input type="text" id="address" name="address" class="form-control" value="" />
										</div>
									</div>
									
								
									<div class="form-group" style="width:300px;float:left;">
										<label class="col-md-3 control-label" for="country">Ország:</label>
										<div class="col-md-9">
											<input type="text" id="country" name="country" class="form-control" value="" />
										</div>
									</div>
									
									<div class="form-group" style="width:300px;float:left;">
										<label class="col-md-3 control-label" for="address_type">Típusok:</label>
										<div class="col-md-9">
											<select id="address_type" name="address_type" class="form-control" >
												<option value="0">Kérlek válassz</option>
												<?
													for($i=0;$i<count($addtype);$i++){
														echo"<option value=\"".$addtype[$i]['id']."\">".$addtype[$i]['name']."</option>";	
													}
												?>
											</select>
										</div>
									</div>
									
									<div style="clear:both;height:10px;"></div>
									<div class="col-md-9 col-md-offset-0">
										
										<button type="button" class="btn btn-sm btn-primary" id="savenewbutton" onclick="javascript:addccim(<?=$mycontacts['id'];?>,'elementdiv2');"><i class="fa fa-angle-right"></i> Ment</button>
									</div>
									<div style="clear:both;"></div>
								</form>	
								<div style="clear:both;height:10px;"></div>
					
						</div>
							<div class="block hidden-lt-ie9">
								<!-- Switches Title -->
								<div class="block-title">
									<h2><strong>Címek</strong> </h2>
								</div>
								<!-- END Switches Title -->

								<div id="elementdiv2"></div>
								
							</div>
						
						</div>
				
				
					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
					
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/partners/js/inc_contacts.js"></script>

			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mycontacts['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$mycontacts['id'];?>,'Dokumentum feltöltés','filelists');
				showccim('<?=$mycontacts['id'];?>','elementdiv2');
				showcontactconnect('<?=$mycontacts['id'];?>','elementdiv3');
				showconxconn('<?=$mycontacts['id'];?>','elementdiv4');
				showccim('<?=$mycontacts['id'];?>','elementdiv5');
				showcontactconnect('<?=$mycontacts['id'];?>','elementdiv6');
				showconxconn('<?=$mycontacts['id'];?>','elementdiv7');
			</script>
			<script>$(function(){ contactsDatatables.init(); });</script>	
			
			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
		//print_r($_SESSION['planetsys'])
?>    
	<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Személyek</a></li>
			</ul>
			<!-- END Datatables Header -->

			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Személy</strong> lista</h2>
				</div>

				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							//echo"<th>Művelet</th>";
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/partners/js/inc_contacts.js"></script>
		<script>$(function(){ contactsDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>
