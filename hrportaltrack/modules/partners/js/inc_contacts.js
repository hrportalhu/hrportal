/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */
var contactsDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/partners/contacts/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"aaSorting": [[ 0, "desc" ]],
				"iDisplayLength": 30,
				"bDestroy": true,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/partners/ajax_contacts_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					aoData.push({"name": "categories_id", "value":$("#categories_id").val() });
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });
            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveeditcontacts(elemid){
	var valid = f_c($('#editcontacts'));
	if(valid){
		f_xml(ServerName +'/modules/partners/ajax_contacts_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editcontacts').serialize(),'');
	}
}

function savenewcontacts(){
	var valid = f_c($('#newcontacts'));
	if(valid){
		f_xml(ServerName +'/modules/partners/ajax_contacts_data.php','newsubmit=1&' + $('#newcontacts').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/partners/ajax_contacts_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}
function addccim(elemid,targetdiv){
	if($('#name').val()!=""){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_addccim.php',
		data:'newsubmit=1&elemid='+elemid+'&'+$('#newelementaddpform').serialize(),
		success: function(data){
			
			showccim(elemid,targetdiv);
			$('#name').val('');
			$('#address').val('');
			$('#zip').val('');
			$('#city').val('');
			$('#country').val('')
			$('#address_type').val('');
		}});
	}
	else{
		alert("Üres címet nem lehet felvenni!");
	}
}
function delccimadat(delid,elemid,targetdiv){
	if(confirm("Biztos törlöd a címet?")){
		$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_delccimadat.php',
		data:'newsubmit=1&elemid='+elemid+'&delid='+delid,
		success: function(data){
			
			showccim(elemid,targetdiv);
			
		}});
	}
}
function showccim(elemid,targetdiv){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_showccim.php',
		data:'elemid='+elemid+'&targetdiv='+targetdiv,
		success: function(data){
			$('#'+targetdiv).html(data);
		}});
}
function editccimadat(editid,elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_editccimadat.php',
		data:'elemid='+elemid+'&editid='+editid,
		success: function(data){
			$('#newelementaddpform').html(data);
		}});
}


function saveeditccim(editid,elemid,targetdiv){
	if(confirm("Biztos megváltoztatod a címet?")){
		$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_saveeditccim.php',
		data:'editsubmit=1&elemid='+elemid+'&editid='+editid+'&'+$('#newelementaddpform').serialize(),
		success: function(data){
			
			showccim(elemid,targetdiv);
			$('#name').val('');
			$('#namea').val('');
			$('#address').val('');
			$('#zip').val('');
			$('#city').val('');
			$('#country').val('')
			$('#address_type').val('');
		}});
	}
}

function addcontactconnect(elemid,targetdiv){
	if($('#category_val').val()!=""){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_addcontactconnect.php',
		data:'newsubmit=1&elemid='+elemid+'&'+$('#newelementconnpform').serialize(),
		success: function(data){
			showcontactconnect(elemid,targetdiv);
			$('#category_val').val('');
		}});
	}
	else{
		alert("Üres kapcsolati adatot lehet felvenni!");
	}
}
function showcontactconnect(elemid,targetdiv){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_showcontactconnect.php',
		data:'elemid='+elemid+'&targetdiv='+targetdiv,
		success: function(data){
			$('#'+targetdiv).html(data);
		}});
}

function delcontactconnect(delid,elemid,targetdiv){
	if(confirm("Biztos törlöd a kapcsolatot?")){
		$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_delcontactconnect.php',
		data:'newsubmit=1&elemid='+elemid+'&delid='+delid,
		success: function(data){
			showcontactconnect(elemid,targetdiv);
		}});
	}
}

function editcontactconnect(editid,elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_editcontactconnect.php',
		data:'elemid='+elemid+'&editid='+editid,
		success: function(data){
			$('#newelementconnpform').html(data);
		}});
}

function addsavecontactconnect(editid,elemid,targetdiv){
	if(confirm("Biztos megváltoztatod a kapcsolati elemet?")){
		$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_addsavecontactconnect.php',
		data:'newsubmit=1&elemid='+elemid+'&editid='+editid+'&'+$('#newelementconnpform').serialize(),
		success: function(data){
			showcontactconnect(elemid,targetdiv);
			$('#category_val').val('');
		}});
	}
	
}

function addconxconn(elemid,targetdiv){
	if($('#partnercompl_id').val()!=""){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_addconxconn.php',
		data:'newsubmit=1&elemid='+elemid+'&partnercompl_id='+$("#partnercompl_id").val(),
		success: function(data){
			$('#partnercompl_id').val('');
			$('#partnercompl').val('');
			showconxconn(elemid,targetdiv);
			
		}});
	}
	else{
		alert("Üres kapcsolati adatot lehet felvenni!");
	}
}

function showconxconn(elemid,targetdiv){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_showconxconn.php',
		data:'elemid='+elemid+'&targetdiv='+targetdiv,
		success: function(data){
			$('#'+targetdiv).html(data);
		}});
}

function delcontactxpartners(delid,elemid,targetdiv){
	if(confirm("Biztos törlöd a kapcsolatot?")){
		$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_delcontactxpartners.php',
		data:'newsubmit=1&elemid='+elemid+'&delid='+delid,
		success: function(data){
			showconxconn(elemid,targetdiv);
		}});
	}
}

$("#partnercompl").autocomplete({
	 source: ServerName+"/modules/partners/ajax_partnercompl_term.php",
	 minLength: 0,
	 select: function(event, ui) {
			$("#partnercompl").val(ui.item.value);
			$("#partnercompl_id").val(ui.item.id);
			
		}
});
