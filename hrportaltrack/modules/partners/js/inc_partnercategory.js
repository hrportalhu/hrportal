/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */
var PartnerCategoryDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/partners/partnercategory/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"aaSorting": [[ 0, "desc" ]],
				"iDisplayLength": 30,
				"bDestroy": true,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/partners/ajax_partnercategory_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					aoData.push({"name": "name", "value":$("#name").val() });
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });
            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveeditpartnercategory(elemid){
	var valid = f_c($('#editpartnercategory'));
	if(valid){
		f_xml(ServerName +'/modules/partners/ajax_partnercategory_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editpartnercategory').serialize(),'');
	}
}

function savenewpartnercategory(){
	var valid = f_c($('#newpartnercategory'));
	if(valid){
		f_xml(ServerName +'/modules/partners/ajax_partnercategory_data.php','newsubmit=1&' + $('#newpartnercategory').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/partners/ajax_partnercategory_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}
