/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */
var partnersDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/partners/partners/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"aaSorting": [[ 1, "asc" ]],
				"iDisplayLength": -1,
				"bDestroy": true,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/partners/ajax_partners_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					aoData.push({"name": "category_id", "value":$("#category_id").val() });
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });
            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

/* Edit formhoz a js függvény*/
function saveeditpartners(elemid){
	var valid = f_c($('#editpartners'));
	if(valid){
		f_xml(ServerName +'/modules/partners/ajax_partners_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editpartners').serialize(),'');
	}
}

/* New formhoz a js függvény*/
function savenewpartners(){
	var valid = f_c($('#newpartners'));
	if(valid){
		f_xml(ServerName +'/modules/partners/ajax_partners_data.php','newsubmit=1&' + $('#newpartners').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/partners/ajax_partners_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}

/* Az címadatok alfunkcióhoz az új elemek adatbázisba történő írására szolgáló js függvény*/
function addcim(elemid,targetdiv){
	if($('#name').val()!=""){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_addcim.php',
		data:'newsubmit=1&elemid='+elemid+'&'+$('#newelementaddrform').serialize(),
		success: function(data){
			
			showcim(elemid,targetdiv);
			$('#cname').val('');
			$('#address').val('');
			$('#zip').val('');
			$('#city').val('');
			$('#country').val('');
			$('#szelkoord').val('');
			$('#hosszkoord').val('');
			$('#zoom').val('');
			$('#address_type').val('');
			$('#defaultszall').val('');
			$('#defaultszam').val('');
		}});
	}
	else{
		alert("Üres címet nem lehet felvenni!");
	}
}

/* Az címadatok alfunkcióhoz a már felvett elemek törlésére szolgáló js függvény*/
function delcimadat(delid,elemid,targetdiv){
	if(confirm("Biztos törlöd a címet?")){
		$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_delcimadat.php',
		data:'newsubmit=1&elemid='+elemid+'&delid='+delid,
		success: function(data){
			
			showcim(elemid,targetdiv);
			
		}});
	}
}

/* Az címadatok alfunkcióhoz a már felvett elemek megjelenítésére szolgáló js függvény*/
function showcim(elemid,targetdiv){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_showcim.php',
		data:'elemid='+elemid+'&targetdiv='+targetdiv,
		success: function(data){
			$('#'+targetdiv).html(data);
		}});
}

/* Az címadatok alfunkcióhoz a már felvett elemek módosítására szolgáló js függvény*/
function editcimadat(editid,elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_editcimadat.php',
		data:'elemid='+elemid+'&editid='+editid,
		success: function(data){
			$('#newelementaddrform').html(data);
		}});
}

/* Az címadatok alfunkcióhoz a már felvett elemek módosításának adatbázisba történő írására szolgáló js függvény*/
function saveeditcim(editid,elemid,targetdiv){
	if(confirm("Biztos megváltoztatod a címet?")){
		$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_saveeditcim.php',
		data:'editsubmit=1&elemid='+elemid+'&editid='+editid+'&'+$('#newelementaddrform').serialize(),
		success: function(data){
			
			showcim(elemid,targetdiv);
			$('#cname').val('');
			$('#namea').val('');
			$('#address').val('');
			$('#zip').val('');
			$('#city').val('');
			$('#country').val('');
			$('#szelkoord').val('');
			$('#hosszkoord').val('');
			$('#zoom').val('');
			$('#address_type').val('');
			$('#defaultszall').val('');
			$('#defaultszam').val('');
		}});
	}
}

/* Az pénzügyi adatok alfunkcióhoz az új elemek adatbázisba történő írására szolgáló js függvény*/
function addbook(elemid,targetdiv){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_addbook.php',
		data:'newsubmit=1&elemid='+elemid+'&'+$('#newelementaddbookform').serialize(),
		success: function(data){
			
			showbook(elemid,targetdiv);
			$('#bname').val('');
			$('#adoszam').val('');
			$('#euadoszam').val('');
			$('#iban').val('');
			$('#szamlak_szama').val('');
			$('#swift').val('');
			$('#fizmod').val('');
			$('#adostatus').val('');
			$('#napikamat').val('');
		}});
}

/* Az pénzügyi adatok alfunkcióhoz a már felvett elemek megjelenítésére szolgáló js függvény*/
function showbook(elemid,targetdiv){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_showbook.php',
		data:'elemid='+elemid+'&targetdiv='+targetdiv,
		success: function(data){
			$('#'+targetdiv).html(data);
		}});
}

/* Az pénzügyi adatok alfunkcióhoz a már felvett elemek törlésére szolgáló js függvény*/
function delbook(delid,elemid,targetdiv){
	if(confirm("Biztos törlöd a címet?")){
		$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_delbook.php',
		data:'newsubmit=1&elemid='+elemid+'&delid='+delid,
		success: function(data){
			
			showbook(elemid,targetdiv);
			
		}});
	}
}

/* Az pénzügyi adatok alfunkcióhoz a már felvett elemek módosítására szolgáló js függvény*/
function editbook(editid,elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_editbook.php',
		data:'elemid='+elemid+'&editid='+editid,
		success: function(data){
			$('#newelementaddbookform').html(data);
		}});
}

/* Az pénzügyi adatok alfunkcióhoz a már felvett elemek módosításának adatbázisba történő írására szolgáló js függvény*/
function saveeditbook(editid,elemid,targetdiv){
	if(confirm("Biztos megváltoztatod az adatokat?")){
		$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_saveeditbook.php',
		data:'editsubmit=1&elemid='+elemid+'&editid='+editid+'&'+$('#newelementaddbookform').serialize(),
		success: function(data){
			
			showbook(elemid,targetdiv);
			$('#bname').val('');
			$('#adoszam').val('');
			$('#euadoszam').val('');
			$('#iban').val('');
			$('#szamlak_szama').val('');
			$('#swift').val('');
			$('#fizmod').val('');
			$('#adostatus').val('');
			$('#napikamat').val('');
		}});
	}
}


function addpartnerconnect(elemid,targetdiv){
	if($('#category_val').val()!=""){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_addpartnerconnect.php',
		data:'newsubmit=1&elemid='+elemid+'&'+$('#newelementconnform').serialize(),
		success: function(data){
			showpartnerconnect(elemid,targetdiv);
			$('#category_val').val('');
		}});
	}
	else{
		alert("Üres kapcsolati adatot nem lehet felvenni!");
	}
}

function showpartnerconnect(elemid,targetdiv){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_showpartnerconnect.php',
		data:'elemid='+elemid+'&targetdiv='+targetdiv,
		success: function(data){
			$('#'+targetdiv).html(data);
		}});
}

function delpartnerconnect(delid,elemid,targetdiv){
	if(confirm("Biztos törlöd a kapcsolatot?")){
		$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_delpartnerconnect.php',
		data:'newsubmit=1&elemid='+elemid+'&delid='+delid,
		success: function(data){
			showpartnerconnect(elemid,targetdiv);
		}});
	}
}

function editpartnerconnect(editid,elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_editpartnerconnect.php',
		data:'elemid='+elemid+'&editid='+editid,
		success: function(data){
			$('#newelementconnform').html(data);
		}});
}

function addsavepartnerconnect(editid,elemid,targetdiv){
	if(confirm("Biztos megváltoztatod a kapcsolati elemet?")){
		$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_addsavepartnerconnect.php',
		data:'newsubmit=1&elemid='+elemid+'&editid='+editid+'&'+$('#newelementconnform').serialize(),
		success: function(data){
			showpartnerconnect(elemid,targetdiv);
			$('#category_val').val('');
		}});
	}
	
}

function addparxconn(elemid,targetdiv){
	if($('#contactcompl_id').val()!=""){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_addparxconn.php',
		data:'newsubmit=1&elemid='+elemid+'&contactcompl_id='+$("#contactcompl_id").val(),
		success: function(data){
			$('#contactcompl_id').val('');
			$('#contactcompl').val('');
			showparxconn(elemid,targetdiv);
			
		}});
	}
	else{
		alert("Üres kapcsolati adatot lehet felvenni!");
	}
}

function showparxconn(elemid,targetdiv){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_showparxconn.php',
		data:'elemid='+elemid+'&targetdiv='+targetdiv,
		success: function(data){
			$('#'+targetdiv).html(data);
		}});
}

function delpartnerxcontacts(delid,elemid,targetdiv){
	if(confirm("Biztos törlöd a kapcsolatot?")){
		$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_delpartnerxcontacts.php',
		data:'newsubmit=1&elemid='+elemid+'&delid='+delid,
		success: function(data){
			showparxconn(elemid,targetdiv);
		}});
	}
}

function getnewbook(targetdiv){
	//alert(targetdiv);
		$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_addpp.php',
		data:'book_id='+targetdiv,
		success: function(data){
			$('#'+targetdiv).html(data);
			$("#newpriceitem").autocomplete({
				 source: ServerName+"/modules/partners/ajax_newpriceitem_term.php",
				 minLength: 0,
				 select: function(event, ui) {
						$("#newpriceitem").val(ui.item.value);
						$("#newpriceitem_id").val(ui.item.id);
						
					}
			});
		}});
}

function addnewpricelement(){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_addnewpricelement.php',
		data:'newpriceitem_id='+$("#newpriceitem_id").val()+'&price_eur='+$("#price_eur").val()+'&book_type='+$("#book_type").val()+'&aname='+$("#aname").val(),
		success: function(data){
			$('#pricebookelement').html(data);
			
		}});
}

function addnewpricelementtodb(elemid){
	
	f_xml(ServerName +'/modules/partners/ajax_addnewpricelementtodb.php','newpriceitem_id='+$("#newpriceitem_id").val()+'&price_eur='+$("#price_eur").val()+'&book_id='+elemid,'');
}

function savenewpricebook(elemid){
	f_xml(ServerName +'/modules/partners/ajax_savenewpricebook.php','partner_id='+elemid,'');
}

function delpricebookdata(elemid){
	f_xml(ServerName +'/modules/partners/ajax_delpricebookdata.php','delid='+elemid,'');
}

/* A partner bank adatainak alfunkciohoz az új elemek adatbázisba történő írására szolgáló js függvény*/
function addbank(elemid,targetdiv){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_addbank.php',
		data:'newsubmit=1&elemid='+elemid+'&'+$('#newelementaddbankform').serialize(),
		success: function(data){
			
			showbank(elemid,targetdiv);
			$('#bank_id').val('');
			$('#accountnumber').val('');
			$('#defaulta').val('');
		}});
}

/* Az partner bank adatainak alfunkcióhoz a már felvett elemek törlésére szolgáló js függvény*/
function delbank(delid,elemid,targetdiv){
	if(confirm("Biztos törlöd a bank adatait?")){
		$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_delbank.php',
		data:'newsubmit=1&elemid='+elemid+'&delid='+delid,
		success: function(data){
			
			showbank(elemid,targetdiv);
			
		}});
	}
}

/* Az partner bank adatainak alfunkcióhoz a már felvett elemek megjelenítésére szolgáló js függvény*/
function showbank(elemid,targetdiv){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_showbank.php',
		data:'elemid='+elemid+'&targetdiv='+targetdiv,
		success: function(data){
			$('#'+targetdiv).html(data);
		}});
}

/* Az partner bank adatainak alfunkcióhoz a már felvett elemek módosítására szolgáló js függvény*/
function editbank(editid,elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_editbank.php',
		data:'elemid='+elemid+'&editid='+editid,
		success: function(data){
			$('#newelementaddbankform').html(data);
		}});
}

/* Az partner bank adatainak alfunkcióhoz a már felvett elemek módosításának adatbázisba történő írására szolgáló js függvény*/
function saveeditbank(editid,elemid,targetdiv){
	if(confirm("Biztos megváltoztatod a bank adatait?")){
		$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_saveeditbank.php',
		data:'editsubmit=1&elemid='+elemid+'&editid='+editid+'&'+$('#newelementaddbankform').serialize(),
		success: function(data){
			
			showbank(elemid,targetdiv);
			$('#bank_id').val('');
			$('#accountnumber').val('');
			$('#defaulta').val('');
		}});
	}
}

function savenewhitelkeret1(elemid){
	
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_savenewhitelkeret1.php',
		data:'elemid='+elemid
			+'&hitel='+$('#hitel').val()
			+'&moddate='+$('#moddate').val()
		,
		success: function(data){
			showhitelkeret1(elemid);
		}
	});	
}

function savenewhitelkeret2(elemid){
	
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_savenewhitelkeret2.php',
		data:'elemid='+elemid
			+'&hitel='+$('#hitel2').val()
			+'&moddate='+$('#moddate2').val()
		,
		success: function(data){
			showhitelkeret2(elemid);
		}
	});	
}

function showhitelkeret1(elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_showhitelkeret1.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#tab-ah1').html(data);
			
		}
	});	
}

function showhitelkeret2(elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_showhitelkeret2.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#tab-ah2').html(data);
			
		}
	});	
}


function showorders(elemid,targetdiv){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/partners/ajax_showorders.php',
		data:'elemid='+elemid+'&targetdiv='+targetdiv,
		success: function(data){
			$('#'+targetdiv).html(data);
		}});
}




$("#contactcompl").autocomplete({
	 source: ServerName+"/modules/partners/ajax_contactcompl_term.php",
	 minLength: 0,
	 select: function(event, ui) {
			$("#contactcompl").val(ui.item.value);
			$("#contactcompl_id").val(ui.item.id);
			
		}
});

$("#newpriceitem").autocomplete({
	 source: ServerName+"/modules/partners/ajax_newpriceitem_term.php",
	 minLength: 0,
	 select: function(event, ui) {
			$("#newpriceitem").val(ui.item.value);
			$("#newpriceitem_id").val(ui.item.id);
			
		}
});
