<?php
    unset($_SESSION['planetsys']['actpricebook']);
?>
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Partner kezelő
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/partners/partners">Partner kezelő</a></li>
				<li><a href="/new">Új partner felvitele</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új partnercég felvitele</strong></h2>
				</div>
			<?
			
				//Adatbázis lekérdezések a new formhoz, amik a lenyíló mezőhöz kellenek
				$kategoriak=db_all("select id, name from partner_categories where status=1 order by name");
				$uzletkoto=db_all("select id, realname from users where status=1 order by realname");
				$country=db_all("select id, name from countrys where status=1 order by name");
				$citys=db_all("select id, name from citys where status=1 order by name");
				
				echo "<form id=\"newpartners\" name=\"newpartners\"  method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";
						
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"name\">Elnevezés:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" placeholder=\"Partnercég elnevezése\" />																		
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"shortname\">Rövid név:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"shortname\" name=\"shortname\" class=\"form-control\" placeholder=\"Rövid név\" />																		
						</div>
					</div>";
						
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"category_id\">Kategóriák:</label>
						<div class=\"col-md-9\">
							<select id=\"category_id\" name=\"category_id\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($kategoriak);$i++){				
									echo"<option value=\"".$kategoriak[$i]['id']."\" >".$kategoriak[$i]['name']."</option>";	
								}
							echo"</select>
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"taxnum\">Adószám:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"taxnum\" name=\"taxnum\" class=\"form-control\" placeholder=\"Partnercég adószáma\" />					
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"phone\">Telefonszám:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"phone\" name=\"phone\" class=\"form-control\" placeholder=\"Partnercég telefonszáma\" />
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"fax\">Faxszám:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"fax\" name=\"fax\" class=\"form-control\" placeholder=\"Partnercég faxszáma\" />								
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"email\">E-mail:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"email\" name=\"email\" class=\"form-control\" placeholder=\"Partnercég email címe\" />								
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"url\">URL:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"url\" name=\"url\" class=\"form-control\" placeholder=\"Partnercég webcíme\" />								
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"bankacc\">Bankszámla Száma:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"bankacc\" name=\"bankacc\" class=\"form-control\" placeholder=\"Bankszámla száma\" />								
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"country_id\">Ország:</label>
						<div class=\"col-md-9\">
							<select id=\"country_id\" name=\"country_id\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($country);$i++){				
									echo"<option value=\"".$country[$i]['id']."\" >".$country[$i]['name']."</option>";	
								}
							echo"</select>
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"citys_id\">Város:</label>
						<div class=\"col-md-9\">
							<select id=\"citys_id\" name=\"citys_id\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($citys);$i++){				
									echo"<option value=\"".$citys[$i]['id']."\" >".$citys[$i]['name']."</option>";	
								}
							echo"</select>
						</div>
					</div>";
					
					echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"user_id\">Kapcsolt üzletkötő:</label>
							<div class=\"col-md-9\">
								<select id=\"user_id\" name=\"user_id\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($uzletkoto);$i++){
									echo"<option value=\"".$uzletkoto[$i]['id']."\" >".$uzletkoto[$i]['realname']."</option>";	
								}
								echo"</select>
							</div>
						</div>";
						
					echo "<div class=\"form-group\">
						<label class=\"col-md-3 control-label\" for=\"partnerold\">Régi partnerkezelő azonosítója:</label>
						<div class=\"col-md-9\">
							<input type=\"text\" id=\"partnerold\" name=\"partnerold\" class=\"form-control\" placeholder=\"Régi partnercég azonosítója\" />								
						</div>
					</div>";	
					
					echo "<div class=\"form-group\" >
						<label class=\"col-md-3 control-label\" for=\"status\">Érvényes-e?:</label>
						<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span></label>
					</div>";
					
					echo "<div style=\"clear:both;height:10px;\"></div>";
					
					echo "<div class=\"form-group form-actions\">
						<div class=\"col-md-9 col-md-offset-3\">
							<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewpartners();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
						</div>
					</div>";
					
				echo "</div>
				</form>";
			?>
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/partners/js/inc_partners.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?	
		}
		else{
			
			//echo "select * from functions where id='".$uritags[2]."'";
			$mypartners=db_row("select * from partners where id='".$uritags[3]."'");
			if($mypartners['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="modules/partners/partners">Partnerek</a></li>
				<li><a href="/<?=$URI;?>"><?=$mypartners['name'];?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->

					<div class="block-title">
						<h2><strong><?=$mypartners['name'];?></strong> partner oldala</h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
							
							//Jogosultságok kezelése az alfunkciókra és azoknak megjelenítési sorrendje
							if(priv("partners","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("partners","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							//if(priv("partners","edit")){echo"<li><a href=\"#tab-9\" data-toggle=\"tooltip\" title=\"pricebooks\"><i class=\"gi gi-money\"></i></a></li>";}
							//if(priv("partners","address")){echo"<li><a href=\"#tab-6\" data-toggle=\"tooltip\" title=\"address\"><i class=\"fa fa-home\"></i></a></li>";}
							//if(priv("partners","print")){echo"<li><a href=\"#tab-11\" data-toggle=\"tooltip\" title=\"paymentinfos\"><i class=\"fa fa-usd\"></i></a></li>";}
							//if(priv("partners","print")){echo"<li><a href=\"#tab-12\" data-toggle=\"tooltip\" title=\"creditlimit\"><i class=\"fa fa-euro\"></i></a></li>";}
							//if(priv("partners","connectpersons")){echo"<li><a href=\"#tab-7\" data-toggle=\"tooltip\" title=\"connectpersons\"><i class=\"fa fa-users\"></i></a></li>";}
							if(priv("partners","connecttypes")){echo"<li><a href=\"#tab-8\" data-toggle=\"tooltip\" title=\"Rovatok\"><i class=\"fa fa-sitemap\"></i></a></li>";}
							//if(priv("partners","bankdata")){echo"<li><a href=\"#tab-10\" data-toggle=\"tooltip\" title=\"bankdata\"><i class=\"fa fa-cc-mastercard\"></i></a></li>";}
							if(priv("partners","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("partners","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
							if(priv("partners","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}
							//if(priv("partners","partnerorders")){echo"<li><a href=\"#tab-13\" data-toggle=\"tooltip\" title=\"partnerorders\"><i class=\"fa fa-cart-plus\"></i></a></li>";}
							
							?>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">
						
						<!-- Megjelenítés menüpont kezdete-->
						<div class="tab-pane active" id="tab-1">
							<div class="col-md-6">
								<h4 class="sub-header"><? echo $mypartners['name']; ?></h4>
								<div style="clear:both;height:1px;"></div>
								<dl class="dl-horizontal">
									<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$mypartners['id']); ?></dd>
									<dt>Elnevezés:</dt><dd><? echo $mypartners['name']; ?></dd>
									<dt>Rövid név:</dt><dd><? echo $mypartners['shortname']; ?></dd>
									<dt>Típus:</dt><dd><? echo idchange("partner_categories","name",$mypartners['category_id']); ?></dd>
									<dt>Adószám:</dt><dd><? echo $mypartners['taxnum']; ?></dd>
									<dt>Telefonszám:</dt><dd><? echo $mypartners['phone']; ?></dd>
									<dt>Faxszám:</dt><dd><? echo $mypartners['fax']; ?></dd>
									<dt>E-mail:</dt><dd><? echo $mypartners['email']; ?></dd>
									<dt>Bankszámla száma:</dt><dd><? echo $mypartners['bankacc']; ?></dd>
									<dt>Kapcsolt üzletkötő:</dt><dd><? echo idchange("users","realname",$mypartners['user_id']); ?></dd>
									<dt>Régi partner azonosítója:</dt><dd><? echo $mypartners['partnerold']; ?></dd>
									<dt>Státusz:</dt><dd><? if($mypartners['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
								</dl>	
							</div>
							<div class="col-md-6">
								<?php
									$pic=db_one("select filename from files where modul='partners' and type='partners' and type_id='".$mypartners['id']."' order by cover desc");
									if(is_file($pic)){
										echo"<div class=\"col-sm-3\"><img src=\"/".$pic."\" alt=\"avatar\" width=\"350px\"></div>";
									}
									else{
										echo"<img src=\"img/placeholders/avatars/avatar9@2x.jpg\" alt=\"avatar\">";
									}
								?>
							</div>
							<div style="clear:both;height:1px;"></div>

							<div class="block hidden-lt-ie9">
								<div class="block-title">
									<h2><strong>Kapcsolt rovatok</strong> </h2>
								</div>
								<div id="elementdiv6"></div>
								
							</div>

						</div>
						<!-- Megjelenítés menüpont vége-->
						
						<!-- Szerkesztés menüpont kezdete-->
						<div class="tab-pane" id="tab-2">
						<?
							$kategoriak=db_all("select id, name from partner_categories where status = 1 order by name");
							$uzletkoto=db_all("select id, realname from users where status=1 order by realname");
							$country=db_all("select id, name from countrys where status=1 order by name");
							$citys=db_all("select id, name from citys where status=1 order by name");
							
							echo"<form id=\"editpartners\" name=\"editpartners\"  method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
								echo "<div style=\"margin-top:1px;\">";
					
									echo "<div class=\"form-group\">
										<label class=\"col-md-3 control-label\" for=\"name\">Elnevezés:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" value=\"".$mypartners['name']."\" />																		
										</div>
									</div>";
									
									echo "<div class=\"form-group\">
										<label class=\"col-md-3 control-label\" for=\"shortname\">Rövid név:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"shortname\" name=\"shortname\" class=\"form-control\" value=\"".$mypartners['shortname']."\" />																		
										</div>
									</div>";
									
									echo "<div class=\"form-group\">
										<label class=\"col-md-3 control-label\" for=\"category_id\">Kategóriák:</label>
										<div class=\"col-md-9\">
											<select id=\"category_id\" name=\"category_id\" class=\"form-control\" >
											<option value=\"0\">Kérlek válassz</option>";
											for($i=0;$i<count($kategoriak);$i++){
												$ch1="";if($kategoriak[$i]['id']==$mypartners['category_id']){$ch1=" selected ";}
												echo"<option value=\"".$kategoriak[$i]['id']."\" ".$ch1.">".$kategoriak[$i]['name']."</option>";
											}
											echo"</select>
										</div>
									</div>";
									
									echo "<div class=\"form-group\">
										<label class=\"col-md-3 control-label\" for=\"taxnum\">Adószám:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"taxnum\" name=\"taxnum\" class=\"form-control\" value=\"".$mypartners['taxnum']."\" />
										</div>
									</div>";
									
									echo "<div class=\"form-group\">
										<label class=\"col-md-3 control-label\" for=\"phone\">Telefonszám:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"phone\" name=\"phone\" class=\"form-control\" value=\"".$mypartners['phone']."\" />
										</div>
									</div>";
									
									echo "<div class=\"form-group\">
										<label class=\"col-md-3 control-label\" for=\"fax\">Faxszám:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"fax\" name=\"fax\" class=\"form-control\" value=\"".$mypartners['fax']."\" />
										</div>
									</div>";
									
									echo "<div class=\"form-group\">
										<label class=\"col-md-3 control-label\" for=\"email\">E-mail:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"email\" name=\"email\" class=\"form-control\" value=\"".$mypartners['email']."\" />
										</div>
									</div>";
									
									
									echo "<div class=\"form-group\">
										<label class=\"col-md-3 control-label\" for=\"url\">URL:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"url\" name=\"url\" class=\"form-control\" value=\"".$mypartners['url']."\" />
										</div>
									</div>";
									
									
									echo "<div class=\"form-group\">
										<label class=\"col-md-3 control-label\" for=\"bankacc\">Bankszámla száma:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"bankacc\" name=\"bankacc\" class=\"form-control\" value=\"".$mypartners['bankacc']."\" />
										</div>
									</div>";
									
									
									echo "<div class=\"form-group\">
										<label class=\"col-md-3 control-label\" for=\"country_id\">Ország:</label>
										<div class=\"col-md-9\">
											<select id=\"country_id\" name=\"country_id\" class=\"form-control\" >
											<option value=\"0\">Kérlek válassz</option>";
											for($i=0;$i<count($country);$i++){
												$ch1="";if($country[$i]['id']==$mypartners['country_id']){$ch1=" selected ";}
												echo"<option value=\"".$country[$i]['id']."\" ".$ch1.">".$country[$i]['name']."</option>";
											}
											echo"</select>
										</div>
									</div>";
									
								
									echo "<div class=\"form-group\">
										<label class=\"col-md-3 control-label\" for=\"city_id\">Város:</label>
										<div class=\"col-md-9\">
											<select id=\"city_id\" name=\"city_id\" class=\"form-control\" >
											<option value=\"0\">Kérlek válassz</option>";
											for($i=0;$i<count($citys);$i++){
												$ch1="";if($citys[$i]['id']==$mypartners['city_id']){$ch1=" selected ";}
												echo"<option value=\"".$citys[$i]['id']."\" ".$ch1.">".$citys[$i]['name']."</option>";
											}
											echo"</select>
										</div>
									</div>";
									
									echo "<div class=\"form-group\">
										<label class=\"col-md-3 control-label\" for=\"user_id\">Kapcsolt üzletkötő:</label>
										<div class=\"col-md-9\">
											<select id=\"user_id\" name=\"user_id\" class=\"form-control\" >
											<option value=\"0\">Kérlek válassz</option>";
											for($i=0;$i<count($uzletkoto);$i++){
												$ch1="";if($uzletkoto[$i]['id']==$mypartners['user_id']){$ch1=" selected ";}
												echo"<option value=\"".$uzletkoto[$i]['id']."\" ".$ch1.">".$uzletkoto[$i]['realname']."</option>";
											}
											echo"</select>
										</div>
									</div>";
									
									echo "<div class=\"form-group\">
										<label class=\"col-md-3 control-label\" for=\"partnerold\">Régi partnerkezelő azonosítója:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"partnerold\" name=\"partnerold\" class=\"form-control\" value=\"".$mypartners['partnerold']."\" />
										</div>
									</div>";
									
									echo "<div class=\"form-group\" >
										<label class=\"col-md-3 control-label\" for=\"status\">Státusz:</label>";
										$st="";if($mypartners['status']==1){ $st=" checked "; }
										echo "<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\"".$st." ><span></span></label>
									</div>";
										
									echo "<div style=\"clear:both;height:10px;\"></div>";
									
									echo "<div class=\"form-group form-actions\">
										<div class=\"col-md-9 col-md-offset-3\">
											<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditpartners('".$mypartners['id']."');\"><i class=\"fa fa-angle-right\"></i> Ment</button>
										</div>
									</div>";
									
								echo "</div>
								</form>";
							?>
						</div>
						<!--Szerkesztés menüpont vége-->
						
						<!--Kép és doksi feltöltés menüpont kezdete-->
						<div class="tab-pane" id="tab-3">
								<?
									echo "<div >".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mypartners['id'],"Dokumentum feltöltés")."</div>";
								?> 
							   <div id="filelists"></div>
							   <div style="clear:both;height:1px;"></div>
						</div>
						<!--Kép és doksi feltöltés menüpont vége-->
						
						<!--Hozzászólások menüpont kezdete-->
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mypartners['id'],"Hozzászólások")."</div>";
								?>
						</div>
						<!--Hozzászólások menüpont vége-->
						
						<!--Nyomtatás menüpont kezdete-->
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>
						<!--Nyomtatás menüpont vége-->
						
						<!--Kapcsolati típusok menüpont kezdete-->
						<div class="tab-pane" id="tab-8">
						<?
							$addtype=db_all("select id, wname,name from categories where  pname!='' order by wname");
						?>
							<div class="block hidden-lt-ie9 <?=$adc;?>" >
								<div class="block-title">
									<h2><strong>Rovat ürlap</strong> </h2>
								</div>
								<form  id="newelementconnform" id="newelementconnform" class="form-horizontal form-bordered" onsubmit="return false;">
									<div class="form-group" style="width:300px;float:left;">
										<label class="col-md-3 control-label" for="category_id">Rovatok:</label>
										<div class="col-md-9">
											<select id="category_id" name="category_id" class="form-control" >
												<option value="0">Kérlek válassz</option>
												<?
													for($i=0;$i<count($addtype);$i++){
														echo"<option value=\"".$addtype[$i]['id']."\">".$addtype[$i]['wname']." (".$addtype[$i]['wname'].")</option>";	
													}
												?>
											</select>
										</div>
									</div>
									<div class="form-group" style="width:400px;float:left;">
										<label class="col-md-3 control-label" for="category_val">Cél URL:</label>
										<div class="col-md-9">
											<input type="text" id="category_val" name="category_val" class="form-control" value="<?=$mypartners['url'];?>" />
										</div>
									</div>
									<div style="clear:both;height:10px;"></div>
									<div class="col-md-9 col-md-offset-0">
										<button type="button" class="btn btn-sm btn-primary" id="savenewbutton" onclick="javascript:addpartnerconnect(<?=$mypartners['id'];?>,'elementdiv3');"><i class="fa fa-angle-right"></i> Ment</button>
									</div>
									<div style="clear:both;"></div>
								</form>	
								<div style="clear:both;height:10px;"></div>
							</div>
							
							<!--Kiíratás-->
							<div class="block hidden-lt-ie9">
								<div class="block-title">
									<h2><strong>Szponzorált rovatok</strong></h2>
								</div>
								<div id="elementdiv3"></div>
							</div>
						</div>
						<!--Kapcsolati típusok menüpont vége-->
						
						<!--Kapcsolati személyek menüpont kezdete-->
						<div class="tab-pane" id="tab-7">
						
							<div class="block hidden-lt-ie9 <?=$adc;?>" >
								<div class="block-title">
									<h2><strong>Személy ürlap</strong> </h2>
								</div>
								<form  id="newelementconnform" id="newelementconnform" class="form-horizontal form-bordered" onsubmit="return false;">
									<div class="form-group" style="width:400px;float:left;">
										<label class="col-md-3 control-label" for="contactcompl">Kapcsolódó személy:</label>
										<div class="col-md-9">
											<input type="text" id="contactcompl" name="contactcompl" class="form-control"  />
											<input type="hidden" id="contactcompl_id" name="contactcompl_id"   />
										</div>
									</div>
									<div style="clear:both;height:10px;"></div>
									<div class="col-md-9 col-md-offset-0">
										<button type="button" class="btn btn-sm btn-primary" id="savenewbutton" onclick="javascript:addparxconn(<?=$mypartners['id'];?>,'elementdiv4');"><i class="fa fa-angle-right"></i> Ment</button>
									</div>
									<div style="clear:both;"></div>
								</form>	
								<div style="clear:both;height:10px;"></div>
							</div>
							
							<!--Kiíratás-->
							<div class="block hidden-lt-ie9">
								<div class="block-title">
									<h2><strong>Kapcsolódó személyek</strong></h2>
								</div>
								<div id="elementdiv4"></div>
							</div>
						</div>
						<!--Kapcsolati személyek menüpont vége-->
						
						<!--Árkönyv menüpont kezdete-->
						<div class="tab-pane" id="tab-9">
						
							<div class="block hidden-lt-ie9 <?=$adc;?>" >
								<div class="block-title">
									<h2><strong>Árkönyvek</strong> </h2>
									<?
										$pricebooks=db_all("select * from pricebooks where partner_id='".$mypartners['id']."' group by book_id");	
									?>
									
									<div class="block" >
										<?
										 //include dirname(__FILE__).("/partners_addpelement.php");
										?> 
									</div>	
									
									<ul class="nav nav-tabs" data-toggle="tabs">
										<?
											if(count($pricebooks)>0){
												for($i=0;$i<count($pricebooks);$i++){
													$cl="";if($i==0){$cl=" class=\"active\" ";}
													echo"<li ".$cl."><a href=\"#tab-a".$pricebooks[$i]['book_id']."\" data-toggle=\"tooltip\" title=\"".$pricebooks[$i]['book_id']."\">".$pricebooks[$i]['name']."</a></li>";
												}
												echo"<li ><a href=\"#tab-a0\" data-toggle=\"tooltip\" title=\"Newpricebook\"><i class=\"fa fa-plus\"></i></a></li>";	
											}
											else{
												echo"<li class=\"active\"><a href=\"#tab-a0\" data-toggle=\"tooltip\" title=\"Newpricebook\"><i class=\"fa fa-plus\"></i></a></li>";	
											}
										?>
									</ul>
								</div>	
								<div class="tab-content">
									<?
									
									
									if(count($pricebooks)>0){
										for($i=0;$i<count($pricebooks);$i++){
											$cl="";if($i==0){$cl=" active ";}
											echo"<div class=\"tab-pane ".$cl."\" id=\"tab-a".$pricebooks[$i]['book_id']."\">";
												$pbdata=db_all("select * from pricebooks where book_id='".$pricebooks[$i]['book_id']."' and partner_id='".$mypartners['id']."' and status=1");
												include dirname(__FILE__).("/partners_pricebooktable.php");

											echo"</div>";
											
										}
										echo"<div class=\"tab-pane\" id=\"tab-a0\">";
										  include dirname(__FILE__).("/partners_priceboookform.php");
										echo"</div>";

									}
									else{
										echo"<div class=\"tab-pane active\" id=\"tab-a0\">";
										  include dirname(__FILE__).("/partners_priceboookform.php");
										echo"</div>";
				
									}
									?>
									
									
								</div>
										
							
							</div>
						</div>
						<!--Árkönyv menüpont vége-->
						
						<!--Cím menüpont kezdete-->
						<div class="tab-pane" id="tab-6">

							<?
								$addtype=db_all("select id, name from partner_address_type where status=1 order by name");
							?>
							
							   <div class="block hidden-lt-ie9 <?=$adc;?>" >
								<!-- Switches Title -->
								<div class="block-title">
									<h2><strong>Cím űrlap</strong> </h2>
								</div>
								<!-- END Switches Title -->

								<!-- Switches Content -->
								<form  id="newelementaddrform" id="newelementaddrform" class="form-horizontal form-bordered" onsubmit="return false;">

									<div class="form-group" style="width:400px;float:left;">
										<label class="col-md-3 control-label" for="cname">Cím elnevezése</label>
										<div class="col-md-9">
											<input type="text" id="cname" name="cname" class="form-control" placeholder="a cím elnevezése"/>
										</div>
									</div>

									<div class="form-group" style="width:300px;float:left;">
										<label class="col-md-3 control-label" for="zip">Irányító:</label>
										<div class="col-md-9">
											<input type="text" id="zip" name="zip" class="form-control" value="" />
										</div>
									</div>

									<div class="form-group" style="width:300px;float:left;">
										<label class="col-md-3 control-label" for="city">Város:</label>
										<div class="col-md-9">
											<input type="text" id="city" name="city" class="form-control" value="" />
										</div>
									</div>

									<div class="form-group" style="width:400px;float:left;">
										<label class="col-md-3 control-label" for="address">További címadat:</label>
										<div class="col-md-9">
											<input type="text" id="address" name="address" class="form-control" value="" />
										</div>
									</div>
									
								
									<div class="form-group" style="width:300px;float:left;">
										<label class="col-md-3 control-label" for="country">Ország:</label>
										<div class="col-md-9">
											<input type="text" id="country" name="country" class="form-control" value="" />
										</div>
									</div>
									
									<div class="form-group" style="width:300px;float:left;">
										<label class="col-md-3 control-label" for="szelkoord">Szélességi koordináta:</label>
										<div class="col-md-9">
											<input type="text" id="szelkoord" name="szelkoord" class="form-control" value="" />
										</div>
									</div>
									
									<div class="form-group" style="width:300px;float:left;">
										<label class="col-md-3 control-label" for="hosszkoord">Hosszúsági koordináta:</label>
										<div class="col-md-9">
											<input type="text" id="hosszkoord" name="hosszkoord" class="form-control" value="" />
										</div>
									</div>
									
									<div class="form-group" style="width:300px;float:left;">
										<label class="col-md-3 control-label" for="zoom">Zoom:</label>
										<div class="col-md-9">
											<input type="text" id="zoom" name="zoom" class="form-control" value="" />
										</div>
									</div>
									
									<div class="form-group" style="width:300px;float:left;">
										<label class="col-md-3 control-label" for="address_type">Típusok:</label>
										<div class="col-md-9">
											<select id="address_type" name="address_type" class="form-control" >
												<option value="0">Kérlek válassz</option>
												<?
													for($i=0;$i<count($addtype);$i++){
														echo"<option value=\"".$addtype[$i]['id']."\">".$addtype[$i]['name']."</option>";	
													}
												?>
											</select>
										</div>
									</div>
									
									<div class="form-group" style="width:500px;float:left;">
										<label class="col-md-3 control-label" for="defaultszall">Alapértelmezett szállítási cím?</label>
										<label class="col-md-9 switch switch-primary" ><input type="checkbox" id="defaultszall" name="defaultszall" checked><span></span></label>
									</div>
									
									<div class="form-group" style="width:500px;float:left;">
										<label class="col-md-3 control-label" for="defaultszam">Alapértelmezett számlázási cím?</label>
										<label class="col-md-9 switch switch-primary" ><input type="checkbox" id="defaultszam" name="defaultszam" checked><span></span></label>
									</div>
									
									<div style="clear:both;height:10px;"></div>
									<div class="col-md-9 col-md-offset-0">
										
										<button type="button" class="btn btn-sm btn-primary" id="savenewbutton" onclick="javascript:addcim(<?=$mypartners['id'];?>,'elementdiv2');"><i class="fa fa-angle-right"></i> Ment</button>
									</div>
									<div style="clear:both;"></div>
								</form>	
								<div style="clear:both;height:10px;"></div>
					
						</div>
						<div class="block hidden-lt-ie9">
								<!-- Switches Title -->
								<div class="block-title">
									<h2><strong>Címek</strong> </h2>
								</div>
								<!-- END Switches Title -->

								<div id="elementdiv2"></div>
								
							</div>
					</div>					
					<!--Cím menüpont vége-->
				

					<!--Bank adatainak megadása a partnerhez menüpont kezdete-->
					<div class="tab-pane" id="tab-10">
						<?
							$bankdata=db_all("select id, name from bank where status=1 order by name");
						?>							
						   <div class="block hidden-lt-ie9 <?=$adc;?>" >
							<!-- Switches Title -->
							<div class="block-title">
								<h2><strong>Bank űrlap</strong> </h2>
							</div>
							<!-- END Switches Title -->

							<!-- Switches Content -->
							<form  id="newelementaddbankform" id="newelementaddbankform" class="form-horizontal form-bordered" onsubmit="return false;">

								<div class="form-group" style="width:300px;float:left;">
									<label class="col-md-3 control-label" for="bank_id">Bank neve:</label>
									<div class="col-md-9">
										<select id="bank_id" name="bank_id" class="form-control" >
											<option value="0">Kérlek válassz</option>
											<?
												for($i=0;$i<count($bankdata);$i++){
													echo"<option value=\"".$bankdata[$i]['id']."\">".$bankdata[$i]['name']."</option>";	
												}
											?>
										</select>
									</div>
								</div>

								<div class="form-group" style="width:350px;float:left;">
									<label class="col-md-3 control-label" for="accountnumber">Számlaszám:</label>
									<div class="col-md-9">
										<input type="text" id="accountnumber" name="accountnumber" class="form-control" />
									</div>
								</div>	
								
								<div class="form-group" style="width:500px;float:left;">
									<label class="col-md-3 control-label" for="defaulta">Alapértelmezett?</label>
									<label class="col-md-9 switch switch-primary" ><input type="checkbox" id="defaulta" name="defaulta" checked><span></span></label>
								</div>							
								
								<div style="clear:both;height:10px;"></div>
								<div class="col-md-9 col-md-offset-0">
									
									<button type="button" class="btn btn-sm btn-primary" id="savenewbutton" onclick="javascript:addbank(<?=$mypartners['id'];?>,'elementdiv8');"><i class="fa fa-angle-right"></i> Ment</button>
								</div>
								<div style="clear:both;"></div>
							</form>	
							<div style="clear:both;height:10px;"></div>
				
						</div>
						<!--Kiíratás-->
						<div class="block hidden-lt-ie9">
							<!-- Switches Title -->
							<div class="block-title">
								<h2><strong>Bank adatai</strong> </h2>
							</div>
							<!-- END Switches Title -->

							<div id="elementdiv8"></div>
							
						</div>
					
					</div>
					<!--Bank adatainak megadása a partnerhez menüpont kezdete-->
						
					<!--Partner rendeléseinek kezelése menüpont kezdete-->
					<div class="tab-pane" id="tab-12">
						
						<?
							echo"<ul class=\"nav nav-tabs\" data-toggle=\"tabs\">";
								echo"<li class=\"active\"><a href=\"#tab-ah1\" data-toggle=\"tooltip\" title=\"Hitelkeret\">Pénzintézeti Hitelkeret</a></li>";
								echo"<li><a href=\"#tab-ah2\" data-toggle=\"tooltip\" title=\"Atmeneti\">Üzemi Hitelkeret</a></li>";
							echo"</ul>";
							echo"<div class=\"tab-content\">";
								?>
								<div class="tab-pane active" id="tab-ah1"></div>
								<div class="tab-pane" id="tab-ah2"></div>
							</div>

					</div>
									
					<div class="tab-pane" id="tab-13">

						<div class="block hidden-lt-ie9">
						<!-- Switches Title -->
							<div class="block-title">
								<h2><strong>Partner rendelései</strong> </h2>
							</div>
							<!-- END Switches Title -->

							<div id="elementdiv10"></div>
						
						</div>
					
					</div>
					<!--Partner rendeléseinek kezelése menüpont vége-->	
						
					<!--Partner pénzügyi adatainek kezelése menüpont kezdete-->	
					<div class="tab-pane" id="tab-11">

							<?
								$fizmod=db_all("select id, name from bookkeepsystem_payments where status=1 order by name");
							?>
							
							   <div class="block hidden-lt-ie9 <?=$adc;?>" >
								<!-- Switches Title -->
								<div class="block-title">
									<h2><strong>Pénzügyi űrlap</strong> </h2>
								</div>
								<!-- END Switches Title -->

								<!-- Switches Content -->
								<form  id="newelementaddbookform" id="newelementaddbookform" class="form-horizontal form-bordered" onsubmit="return false;">

    <div class="row">
        <div class="col-sm-4">
            <div class="block">
                <div class="block-title">
                    <h2><strong>Törzs</strong> Adat</h2>
                </div>
                <div class="form-group" >
					<label class="col-md-3 control-label" for="adoszam">Adószám:</label>
					<div class="col-md-9">
						<input type="text" id="adoszam" name="adoszam" class="form-control"/>
					</div>
				</div>

				<div class="form-group" >
					<label class="col-md-3 control-label" for="euadoszam">EU adószám:</label>
					<div class="col-md-9">
						<input type="text" id="euadoszam" name="euadoszam" class="form-control"  />
					</div>
				</div>

				<div class="form-group" >
					<label class="col-md-3 control-label" for="iban">IBAN:</label>
					<div class="col-md-9">
						<input type="text" id="iban" name="iban" class="form-control"  />
					</div>
				</div>
				<div style="clear:both;height:1px;"></div>
           </div>

        </div>
    
        <div class="col-sm-4">
            <div class="block">
                <div class="block-title">
                    <h2><strong>Bank</strong> Adat</h2>
                </div>
                				<div class="form-group" >
				<label class="col-md-3 control-label" for="bname">Bank megnevezése</label>
				<div class="col-md-9">
					<input type="text" id="bname" name="bname" class="form-control" />
				</div>
			</div>
			<div class="form-group" >
				<label class="col-md-3 control-label" for="szamlak_szama">Számla száma:</label>
				<div class="col-md-9">
					<input type="text" id="szamlak_szama" name="szamlak_szama" class="form-control"  />
				</div>
			</div>
			
			<div class="form-group" >
				<label class="col-md-3 control-label" for="swift">Swift:</label>
				<div class="col-md-9">
					<input type="text" id="swift" name="swift" class="form-control"  />
				</div>
			</div>
			<div style="clear:both;height:1px;"></div>
           </div>

        </div>
    
        <div class="col-sm-4">
            <div class="block">
                <div class="block-title">
                    <h2><strong>Egyéb</strong> Adat</h2>
                </div>
                <div class="form-group" >
				<label class="col-md-3 control-label" for="fizmod">Fizetési mód:</label>
				<div class="col-md-9">
					<select id="fizmod" name="fizmod" class="form-control" >
						<option value="0">Kérlek válassz</option>
						<?
							for($i=0;$i<count($fizmod);$i++){
								echo"<option value=\"".$fizmod[$i]['id']."\">".$fizmod[$i]['name']."</option>";	
							}
						?>
					</select>
				</div>
			</div>
			
			<div class="form-group" >
				<label class="col-md-3 control-label" for="adostatus">Adó státusza:</label>
				<div class="col-md-9">
					<input type="text" id="adostatus" name="adostatus" class="form-control"  />
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-md-3 control-label" for="napikamat">Napi kamat:</label>
				<div class="col-md-9">
					<input type="text" id="napikamat" name="napikamat" class="form-control"  />
				</div>
			</div>
			 <div style="clear:both;height:1px;"></div>
           </div>
			
        </div>
   
    
    </div>
                
                
                

									
									
									
									
								
									<div style="clear:both;height:10px;"></div>
									<div class="col-md-9 col-md-offset-0">
										
										<button type="button" class="btn btn-sm btn-primary" id="savenewbutton" onclick="javascript:addbook(<?=$mypartners['id'];?>,'elementdiv11');"><i class="fa fa-angle-right"></i> Ment</button>
									</div>
									<div style="clear:both;"></div>
								</form>	
								<div style="clear:both;height:10px;"></div>
					
						</div>
						<!--Kiíratás-->
						<div class="block hidden-lt-ie9">
								<!-- Switches Title -->
								<div class="block-title">
									<h2><strong>Pénzügyi adatok</strong> </h2>
								</div>
								<!-- END Switches Title -->

								<div id="elementdiv11"></div>
								
						</div>
					</div>
					<!--Partner pénzügyi adatainek kezelése menüpont vége-->	
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
				</div>
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/partners/js/inc_partners.js"></script>
			
			<!--Megjelenítési scriptek-->
			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mypartners['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$mypartners['id'];?>,'filelists');
				showcim('<?=$mypartners['id'];?>','elementdiv2');
				showpartnerconnect('<?=$mypartners['id'];?>','elementdiv3');
				showparxconn('<?=$mypartners['id'];?>','elementdiv4');
				showcim('<?=$mypartners['id'];?>','elementdiv5');
				showpartnerconnect('<?=$mypartners['id'];?>','elementdiv6');
				showparxconn('<?=$mypartners['id'];?>','elementdiv7');
				showbank('<?=$mypartners['id'];?>','elementdiv8');
				showbank('<?=$mypartners['id'];?>','elementdiv9');
				showhitelkeret1('<?=$mypartners['id'];?>');
				showhitelkeret2('<?=$mypartners['id'];?>');
				showorders('<?=$mypartners['id'];?>','elementdiv10');
				showbook('<?=$mypartners['id'];?>','elementdiv11');
			</script>
			<script>$(function(){ partnersDatatables.init(); });</script>	
			
			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
		//print_r($_SESSION['planetsys'])
?>    
	<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Partner</a></li>
			</ul>
			<!-- END Datatables Header -->

			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Partner</strong> lista</h2>
				</div>
				
				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							//echo"<th>Művelet</th>";
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/partners/js/inc_partners.js"></script>
		<script>$(function(){ partnersDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>
