
	<h4>Árkönyvhöz tétel hozzáadása</h4>
	<div style="clear:both;height:1px;"></div>
	<div class="form-group" style="width:300px;float:left;">
		<label class="col-md-6 control-label" for="newitem">Árkönyv tétel</label>
		<div class="col-md-6">
			<input type="text" id="newpriceitem" name="newpriceitem" class="form-control" value="" />
			<input type="hidden" id="newpriceitem_id" name="newpriceitem_id" class="form-control" value="" />
		</div>
	</div>

	<div class="form-group" style="width:200px;float:left;">
		<label class="col-md-4 control-label" for="price_eur">Ár-EUR</label>
		<div class="col-md-8" >
			<input type="text" id="price_eur" name="price_eur" class="form-control" value="" />
		</div>
	</div>
	<div class="form-group" style="width:200px;float:left;">
		<label class="col-md-4 control-label" for="arfolyam">Árfolyam</label>
		<div class="col-md-8">
			<input type="text" id="arfolyam" name="arfolyam" class="form-control" value="" />
		</div>
	</div>

	<div class="form-group" style="width:200px;float:left;">
		<label class="col-md-4 control-label" for="arfolyam_type">Árfolyam típus</label>
		<div class="col-md-8">
			<select id="arfolyam_type" name="arfolyam_type" class="form-control" >
				<option value="0">Kérlek válassz</option>
				<option value="atlag">Átlag árfolyam</option>
				<option value="napi">Napi árfolyam</option>
				
			</select>
		</div>
	</div>
	<div class="form-group" style="width:200px;float:left;">		
				
		<button type="button" class="btn btn-sm btn-primary" id="savenewbutton" onclick="javascript:addnewpricelement('<?=$pricebooks[$i]['book_id'];?>');"><i class="fa fa-angle-right"></i> Felvesz</button>
	</div>


	<div style="clear:both;height:1px;"></div>

