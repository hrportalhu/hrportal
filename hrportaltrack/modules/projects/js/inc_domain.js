/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */

var DomainDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/projects/domain/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"aaSorting": [[ 0, "desc" ]],
				"iDisplayLength": 30,
				"bDestroy": true,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/projects/ajax_domain_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					aoData.push({"name": "name", "value":$("#name").val() });
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });
            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveeditdomain(elemid){
	var valid = f_c($('#editdomain'));
	if(valid){
		f_xml(ServerName +'/modules/projects/ajax_domain_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editdomain').serialize(),'');
	}
}

function savenewdomain(){
	var valid = f_c($('#newdomain'));
	if(valid){
		f_xml(ServerName +'/modules/projects/ajax_domain_data.php','newsubmit=1&' + $('#newdomain').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/projects/ajax_domain_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}

function adddomainacces(elemid,targetdiv){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/projects/ajax_adddomainacces.php',
		data:'newsubmit=1&elemid='+elemid+'&'+$('#newelementadddomainaccesform').serialize(),
		success: function(data){
			
			showdomainacces(elemid,targetdiv);
			$('#hozzaferes_id').val('');
			$('#name').val('');
			$('#password').val('');
		}});
}
function deldomainacces(delid,elemid,targetdiv){
	if(confirm("Biztos törlöd az adatokat?")){
		$.ajax({
		type:'POST',
		url: ServerName +'/modules/projects/ajax_deldomainacces.php',
		data:'newsubmit=1&elemid='+elemid+'&delid='+delid,
		success: function(data){
			
			showdomainacces(elemid,targetdiv);
			$('#hozzaferes_id').val('');
			$('#name').val('');
			$('#password').val('');
		}});
	}
}
function showdomainacces(elemid,targetdiv){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/projects/ajax_showdomainacces.php',
		data:'elemid='+elemid+'&targetdiv='+targetdiv,
		success: function(data){
			$('#'+targetdiv).html(data);
		}});
}
function editdomainacces(editid,elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/projects/ajax_editdomainacces.php',
		data:'elemid='+elemid+'&editid='+editid,
		success: function(data){
			$('#newelementadddomainaccesform').html(data);
		}});
}


function saveeditdomainacces(editid,elemid,targetdiv){
	if(confirm("Biztos megváltoztatod az adatokat?")){
		$.ajax({
		type:'POST',
		url: ServerName +'/modules/projects/ajax_saveeditdomainacces.php',
		data:'editsubmit=1&elemid='+elemid+'&editid='+editid+'&'+$('#newelementadddomainaccesform').serialize(),
		success: function(data){
			
			showdomainacces(elemid,targetdiv);
			$('#hozzaferes_id').val('');
			$('#name').val('');
			$('#password').val('');
		}});
	}
}
