/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */

var PprojectsDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/projects/pprojects/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"aaSorting": [[ 0, "desc" ]],
				"iDisplayLength": 30,
				"bDestroy": true,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/projects/ajax_pprojects_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					aoData.push({"name": "name", "value":$("#name").val() });
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });
            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveeditpprojects(elemid){
	var valid = f_c($('#editpprojects'));
	if(valid){
		f_xml(ServerName +'/modules/projects/ajax_pprojects_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editpprojects').serialize(),'');
	}
}

function savenewpprojects(){
	var valid = f_c($('#newpprojects'));
	if(valid){
		f_xml(ServerName +'/modules/projects/ajax_pprojects_data.php','newsubmit=1&' + $('#newpprojects').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/projects/ajax_pprojects_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}

function addacces(elemid,targetdiv){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/projects/ajax_addacces.php',
		data:'newsubmit=1&elemid='+elemid+'&'+$('#newelementaddaccesform').serialize(),
		success: function(data){
			
			showacces(elemid,targetdiv);
			$('#tipus_id').val('');
			$('#name').val('');
			$('#password').val('');
		}});
}
function delacces(delid,elemid,targetdiv){
	if(confirm("Biztos törlöd a címet?")){
		$.ajax({
		type:'POST',
		url: ServerName +'/modules/projects/ajax_delacces.php',
		data:'newsubmit=1&elemid='+elemid+'&delid='+delid,
		success: function(data){
			
			showacces(elemid,targetdiv);
			
		}});
	}
}
function showacces(elemid,targetdiv){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/projects/ajax_showacces.php',
		data:'elemid='+elemid+'&targetdiv='+targetdiv,
		success: function(data){
			$('#'+targetdiv).html(data);
		}});
}
function editacces(editid,elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/projects/ajax_editacces.php',
		data:'elemid='+elemid+'&editid='+editid,
		success: function(data){
			$('#newelementaddaccesform').html(data);
		}});
}


function saveeditacces(editid,elemid,targetdiv){
	if(confirm("Biztos megváltoztatod a címet?")){
		$.ajax({
		type:'POST',
		url: ServerName +'/modules/projects/ajax_saveeditacces.php',
		data:'editsubmit=1&elemid='+elemid+'&editid='+editid+'&'+$('#newelementaddaccesform').serialize(),
		success: function(data){
			
			showacces(elemid,targetdiv);
			$('#tipus_id').val('');
			$('#name').val('');
			$('#password').val('');
		}});
	}
}


