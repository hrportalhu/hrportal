/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */
var rightsDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/projects/rights/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"aaSorting": [[ 0, "desc" ]],
				"iDisplayLength": 30,
				"bDestroy": true,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/projects/ajax_rights_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					aoData.push({"name": "category_id", "value":$("#category_id").val() });
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });
            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveeditpartners(elemid){
	var valid = f_c($('#editpartners'));
	if(valid){
		f_xml(ServerName +'/modules/partners/ajax_partners_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editpartners').serialize(),'');
	}
}

function savenewpartners(){
	var valid = f_c($('#newpartners'));
	if(valid){
		f_xml(ServerName +'/modules/partners/ajax_partners_data.php','newsubmit=1&' + $('#newpartners').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/partners/ajax_rights_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}

