/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */

var SzerverekDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/projects/szerverek/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"aaSorting": [[ 0, "desc" ]],
				"iDisplayLength": 30,
				"bDestroy": true,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/projects/ajax_szerverek_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					aoData.push({"name": "name", "value":$("#name").val() });
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });
            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveeditszerverek(elemid){
	var valid = f_c($('#editszerverek'));
	if(valid){
		f_xml(ServerName +'/modules/projects/ajax_szerverek_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editszerverek').serialize(),'');
	}
}

function savenewszerverek(){
	var valid = f_c($('#newszerverek'));
	if(valid){
		f_xml(ServerName +'/modules/projects/ajax_szerverek_data.php','newsubmit=1&' + $('#newszerverek').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/projects/ajax_szerverek_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}

function addszerveracces(elemid,targetdiv){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/projects/ajax_addszerveracces.php',
		data:'newsubmit=1&elemid='+elemid+'&'+$('#newelementaddszerveraccesform').serialize(),
		success: function(data){
			
			showszerveracces(elemid,targetdiv);
			$('#hozzaferes_id').val('');
			$('#name').val('');
			$('#password').val('');
		}});
}
function delszerveracces(delid,elemid,targetdiv){
	if(confirm("Biztos törlöd a címet?")){
		$.ajax({
		type:'POST',
		url: ServerName +'/modules/projects/ajax_delszerveracces.php',
		data:'newsubmit=1&elemid='+elemid+'&delid='+delid,
		success: function(data){
			
			showszerveracces(elemid,targetdiv);
			
		}});
	}
}
function showszerveracces(elemid,targetdiv){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/projects/ajax_showszerveracces.php',
		data:'elemid='+elemid+'&targetdiv='+targetdiv,
		success: function(data){
			$('#'+targetdiv).html(data);
		}});
}
function editszerveracces(editid,elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/projects/ajax_editszerveracces.php',
		data:'elemid='+elemid+'&editid='+editid,
		success: function(data){
			$('#newelementaddszerveraccesform').html(data);
		}});
}


function saveeditszerveracces(editid,elemid,targetdiv){
	if(confirm("Biztos megváltoztatod a címet?")){
		$.ajax({
		type:'POST',
		url: ServerName +'/modules/projects/ajax_saveeditszerveracces.php',
		data:'editsubmit=1&elemid='+elemid+'&editid='+editid+'&'+$('#newelementaddszerveraccesform').serialize(),
		success: function(data){
			
			showszerveracces(elemid,targetdiv);
			$('#hozzaferes_id').val('');
			$('#name').val('');
			$('#password').val('');
		}});
	}
}
