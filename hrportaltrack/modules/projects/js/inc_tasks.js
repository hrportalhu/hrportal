/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */

var TasksDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/projects/tasks/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"aaSorting": [[ 0, "desc" ]],
				"iDisplayLength": -1,
				"bDestroy": true,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/projects/ajax_tasks_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					aoData.push({"name": "project_id", "value":$("#projectselect").val() });
					aoData.push({"name": "user_id", "value":$("#userselect").val() });
					aoData.push({"name": "task_status", "value":$("#allapotselect").val() });
					aoData.push({"name": "prio", "value":$("#prioselect").val() });
					aoData.push({"name": "lezarva", "value":$("#lezarva").val() });
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });
            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveedittasks(elemid){
	var taskText = CKEDITOR.instances.task.getData();
	var valid = f_c($('#edittasks'));
	if(valid){
		f_xml(ServerName +'/modules/projects/ajax_tasks_data.php','view=edit&id=' + elemid +'&tasktext='+taskText+'&editsubmit='+elemid+'&' + $('#edittasks').serialize(),'');
	}
}

function savenewtasks(){
	var taskText = CKEDITOR.instances.task.getData();
	var valid = f_c($('#newtasks'));
	if(valid){
		f_xml(ServerName +'/modules/projects/ajax_tasks_data.php','newsubmit=1&tasktext='+taskText+'&' + $('#newtasks').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/projects/ajax_tasks_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}
