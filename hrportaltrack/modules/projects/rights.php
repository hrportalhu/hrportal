 <?include dirname(__FILE__).("/inc.rights.php");
    naplo('info','rights','Jog kezelő  betöltése');
    $_SESSION['planetsys']['actpage']=$screen;?>
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Jog kezelő
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/projects/domain">Jog kezelő</a></li>
				<li><a href="/new">Új domain felvitele</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új domain felvitele</strong></h2>
				</div>
			<?
			
				$megrendelo=db_all("select id, name from contacts where status=1 order by name");
				$ceg=db_all("select id, name from partners where status=1 order by name");
				$tulajdonos=db_all("select id, name from contacts where status=1 order by name");
				$domainek=db_all("select id, dname from domain where status=1 order by dname");
				
				
				echo "<form id=\"newdomain\" name=\"newdomain\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";
						
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"dname\">Domain neve:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"dname\" name=\"dname\" class=\"form-control\" placeholder=\"Domain neve\" />																		
							</div>
						</div>";
						
						echo"<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"domain_id\">Szülő domain:</label>
							<div class=\"col-md-9\">
								<select id=\"domain_id\" name=\"domain_id\" class=\"form-control\" placeholder=\"Szülő kategória\"   >
									<option value=\"0\">Kérlek válassz</option>";
									for($i=0;$i<count($domainek);$i++){		
										echo"<option value=\"".$domainek[$i]['id']."\" >".$domainek[$i]['dname']."</option>";	
									}
								echo"</select>";
							echo"</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"megrendelo_id\">Megrendelő neve:</label>
							<div class=\"col-md-9\">
								<select id=\"megrendelo_id\" name=\"megrendelo_id\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($megrendelo);$i++){
									echo"<option value=\"".$megrendelo[$i]['id']."\" >".$megrendelo[$i]['name']."</option>";	
								}
								echo"</select>
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"partner_id\">Tulajdonos cég:</label>
							<div class=\"col-md-9\">
								<select id=\"partner_id\" name=\"partner_id\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($ceg);$i++){
									echo"<option value=\"".$ceg[$i]['id']."\" >".$ceg[$i]['name']."</option>";	
								}
								echo"</select>
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"contacts_id\">Tulajdonos személy:</label>
							<div class=\"col-md-9\">
								<select id=\"contacts_id\" name=\"contacts_id\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($tulajdonos);$i++){
									echo"<option value=\"".$tulajdonos[$i]['id']."\" >".$tulajdonos[$i]['name']."</option>";	
								}
								echo"</select>
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"dregisztral\">Domain regisztrálási idő:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"dregisztral\" name=\"dregisztral\" class=\"form-control input-datepicker\" placeholder=\"Domain regisztrálási idő\" />																		
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"diranyurl\">Domain irányítási url:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"diranyurl\" name=\"diranyurl\" class=\"form-control\" placeholder=\"Domain irányítási url\" />																		
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"dlejarat\">Domain lejárati idő:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"dlejarat\" name=\"dlejarat\" class=\"form-control input-datepicker\" placeholder=\"Domain lejárati idő\" />																		
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"dkezdetiido\">Domain kezdési idő:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"dkezdetiido\" name=\"dkezdetiido\" class=\"form-control input-datepicker\" placeholder=\"Domain kezdési idő\" />																		
							</div>
						</div>";
				
						
						echo "<div class=\"form-group\" >
							<label class=\"col-md-3 control-label\" for=\"status\">Érvényes-e?:</label>
							<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span></label>
						</div>";
						
				echo "</div>";
				echo "<div style=\"margin-top:1px;\">";
					echo "<div style=\"clear:both;height:10px;\"></div>";
						echo"<div class=\"form-group form-actions\">
							<div class=\"col-md-9 col-md-offset-3\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewdomain();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>";
				echo "</div>";
				echo "</form>";
			?>
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/projects/js/inc_domain.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?	
		}
		else{

			//echo "select * from functions where id='".$uritags[2]."'";
			$mydomain=db_row("select * from domain where id='".$uritags[3]."'");
			if($mydomain['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="modules/projects/domain"> Domain</a></li>
				<li><a href="/<?=$URI;?>"><?=$mydomain['dname'];?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->

					<div class="block-title">
						<h2><strong><?=$mydomain['dname'];?></strong> domain oldal</h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
							if(priv("domain","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("domain","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							if(priv("domain","acces")){echo"<li><a href=\"#tab-6\" data-toggle=\"tooltip\" title=\"acces\"><i class=\"fa fa-user-secret\"></i></a></li>";}	
							if(priv("domain","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("domain","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
							if(priv("domain","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}	
							?>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="row">
								<div class="col-md-6">
									<div class="block">
										<div class="block-title">
											<h2><strong>Domain adatai</strong></h2>
										</div>
										<dl class="dl-horizontal">
											<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$mydomain['id']); ?></dd>
											<dt>Domain neve:</dt><dd><? echo $mydomain['dname']; ?></dd>
											<dt>Megrendelő neve:</dt><dd><? echo  idchange("contacts","name",$mydomain['megrendelo_id']); ?></dd>
											<dt>Tulajdonos cég:</dt><dd><?echo  idchange("partners","name",$mydomain['partner_id']); ?></dd>
											<dt>Tulajdonos személy:</dt><dd><?echo  idchange("contacts","name",$mydomain['contacts_id']); ?></dd>
											<dt>Domain regisztrálási idő:</dt><dd><? echo $mydomain['dregisztral']; ?></dd>
											<dt>Domain irányítási url:</dt><dd><? echo $mydomain['diranyurl']; ?></dd>
											<dt>Domain lejárati idő:</dt><dd><? echo $mydomain['dlejarat']; ?></dd>
											<dt>Domain kezdési idő:</dt><dd><? echo $mydomain['dkezdetiido']; ?></dd>
											<dt>Érvényes-e?:</dt><dd><? if($mydomain['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>									
										</dl>	
									</div>
								</div>	
								<div class="col-md-6">
									<?php
										$pic=db_one("select filename from files where modul='projects' and type='domain' and type_id='".$mydomain['id']."' order by cover desc");
										if(is_file($pic)){
											echo"<div class=\"col-sm-3\"><img src=\"/".$pic."\" alt=\"avatar\" width=\"450px\"></div>";
										}
										else{
											echo"<img src=\"img/placeholders/avatars/avatar9@2x.jpg\" alt=\"avatar\">";
										}
									?>
								</div>
							</div>	
							<div style="clear:both;height:1px;"></div>
							<div class="block hidden-lt-ie9">
								<div class="block-title">
									<h2><strong>Hozzáférések</strong> </h2>
								</div>
								<div id="elementdiv8"></div>
								
							</div>
						</div>
						<div class="tab-pane" id="tab-2">
						<?
							$megrendelo=db_all("select id, name from contacts where status=1 order by name");
							$ceg=db_all("select id, name from partners where status=1 order by name");
							$tulajdonos=db_all("select id, name from contacts where status=1 order by name");
							$domainek=db_all("select id, dname from domain where status=1 order by dname");
							
							echo"<form id=\"editdomain\" name=\"editdomain\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
							echo "<div style=\"margin-top:1px;\">";
							
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"dname\">Domain név:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"dname\" name=\"dname\" class=\"form-control\" value=\"".$mydomain['dname']."\" />																		
									</div>
								</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"domain_id\">Szülő domain:</label>
									<div class=\"col-md-9\">
										<select id=\"domain_id\" name=\"domain_id\" class=\"form-control\" >
										<option value=\"0\">Kérlek válassz</option>";
										for($i=0;$i<count($domainek);$i++){
											$se2="";if($domainek[$i]['id']==$mydomain['domain_id']){$se2=" selected ";}
											echo"<option value=\"".$domainek[$i]['id']."\" ".$se2.">".$domainek[$i]['dname']."</option>";
										}
										echo"</select>
									</div>
								</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"megrendelo_id\">Megrendelő:</label>
									<div class=\"col-md-9\">
										<select id=\"megrendelo_id\" name=\"megrendelo_id\" class=\"form-control\" >
										<option value=\"0\">Kérlek válassz</option>";
										for($i=0;$i<count($megrendelo);$i++){
											$se2="";if($megrendelo[$i]['id']==$mydomain['megrendelo_id']){$se2=" selected ";}
											echo"<option value=\"".$megrendelo[$i]['id']."\" ".$se2.">".$megrendelo[$i]['name']."</option>";
										}
										echo"</select>
									</div>
								</div>";
								
								
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"partner_id\">Tulajdonos cég:</label>
									<div class=\"col-md-9\">
										<select id=\"partner_id\" name=\"partner_id\" class=\"form-control\" >
										<option value=\"0\">Kérlek válassz</option>";
										for($i=0;$i<count($ceg);$i++){
											$sel="";if($ceg[$i]['id']==$mydomain['partner_id']){$sel=" selected ";}
											echo"<option value=\"".$ceg[$i]['id']."\" ".$sel.">".$ceg[$i]['name']."</option>";
										}
										echo"</select>
									</div>
								</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"contacts_id\">Tulajdonos személy:</label>
									<div class=\"col-md-9\">
										<select id=\"contacts_id\" name=\"contacts_id\" class=\"form-control\" >
										<option value=\"0\">Kérlek válassz</option>";
										for($i=0;$i<count($tulajdonos);$i++){
											$sel="";if($tulajdonos[$i]['id']==$mydomain['contacts_id']){$sel=" selected ";}
											echo"<option value=\"".$tulajdonos[$i]['id']."\" ".$sel.">".$tulajdonos[$i]['name']."</option>";
										}
										echo"</select>
									</div>
								</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"dregisztral\">Domain regisztrálási idő:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"dregisztral\" name=\"dregisztral\" class=\"form-control\" value=\"".$mydomain['dregisztral']."\" />																		
									</div>
								</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"diranyurl\">Domain irányítási url:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"diranyurl\" name=\"diranyurl\" class=\"form-control\" value=\"".$mydomain['diranyurl']."\" />																		
									</div>
								</div>";
										
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"dlejarat\">Domain lejárati idő</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"dlejarat\" name=\"dlejarat\" class=\"form-control input-datepicker\" value=\"".$mydomain['dlejarat']."\" />																		
									</div>
								</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"dkezdetiido\">Domain kezdési idő:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"dkezdetiido\" name=\"dkezdetiido\" class=\"form-control input-datepicker\" value=\"".$mydomain['dkezdetiido']."\" />																		
									</div>
								</div>";
								
								
								echo "<div class=\"form-group\" >
									<label class=\"col-md-3 control-label\" for=\"status\">Érvényes-e?:</label>";
									$st="";if($mydomain['status']==1){ $st=" checked "; }
									echo "<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\"".$st." ><span></span></label>
								</div>";
								
							
						echo "</div>";
						echo "<div style=\"margin-top:1px;\">";
							echo "<div style=\"clear:both;height:10px;\"></div>";	
							echo "<div class=\"form-group form-actions\">
								<div class=\"col-md-9 col-md-offset-3\">
									<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditdomain(".$mydomain['id'].")\"><i class=\"fa fa-angle-right\"></i> Ment</button>
								</div>
							</div>";									
						echo "</div>";
						echo "</form>";
						?>
						</div>
						<div class="tab-pane" id="tab-3">
								<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mydomain['id'],"Dokumentum feltöltés")."</div>";
								?> 
								<div id="filelists"></div>
								<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mydomain['id'],"Hozzászólások")."</div>";
								?>
						</div>
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>
						
						<div class="tab-pane" id="tab-6">

							<?
								$tipus=db_all("select id, name from hozzaferestipusok where status=1 order by name");
							?>
							
							   <div class="block hidden-lt-ie9 <?=$adc;?>" >
								<!-- Switches Title -->
									<div class="block-title">
										<h2><strong>Hozzáférések űrlap</strong> </h2>
									</div>
									<!-- END Switches Title -->

									<!-- Switches Content -->
									<form  id="newelementadddomainaccesform" id="newelementadddomainaccesform" class="form-horizontal form-bordered" onsubmit="return false;">

										<div class="form-group" style="width:400px;float:left;">
											<label class="col-md-3 control-label" for="hozzaferes_id">hozzaferestipusok:</label>
											<div class="col-md-9">
												<select id="hozzaferes_id" name="hozzaferes_id" class="form-control" >
													<option value="0">Kérlek válassz</option>
													<?
														for($i=0;$i<count($tipus);$i++){
															echo"<option value=\"".$tipus[$i]['id']."\">".$tipus[$i]['name']."</option>";	
														}
													?>
												</select>
											</div>
										</div>

										<div class="form-group" style="width:420px;float:left;">
											<label class="col-md-3 control-label" for="name">Felhasználónév:</label>
											<div class="col-md-9">
												<input type="text" id="name" name="name" class="form-control" />
											</div>
										</div>	
										
										<div class="form-group" style="width:300px;float:left;">
											<label class="col-md-3 control-label" for="password">Jelszó:</label>
											<div class="col-md-9">
												<input type="text" id="password" name="password" class="form-control" />
											</div>
										</div>								
										
										<div style="clear:both;height:10px;"></div>
										<div class="col-md-9 col-md-offset-0">
											
											<button type="button" class="btn btn-sm btn-primary" id="savenewbutton" onclick="javascript:adddomainacces(<?=$mydomain['id'];?>,'elementdiv7');"><i class="fa fa-angle-right"></i> Ment</button>
										</div>
										<div style="clear:both;"></div>
									</form>	
									<div style="clear:both;height:10px;"></div>
					
								</div>
						
								<div class="block hidden-lt-ie9">
									<!-- Switches Title -->
									<div class="block-title">
										<h2><strong>Hozzáférések</strong> </h2>
									</div>
									<!-- END Switches Title -->

									<div id="elementdiv7"></div>
									
								</div>
						
							</div>
				
					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
					
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/projects/js/inc_rights.js"></script>

			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mydomain['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$mydomain['id'];?>,'Dokumentum feltöltés','filelists');
				showdomainacces('<?=$mydomain['id'];?>','elementdiv7');
				showdomainacces('<?=$mydomain['id'];?>','elementdiv8');	
			</script>
			<script>$(function(){ rightsDatatables.init(); });</script>	

			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
		//print_r($_SESSION['planetsys'])
?>    
<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Jogok</a></li>
			</ul>
			<!-- END Datatables Header -->

			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Jogok listája</strong> lista</h2>
				</div>
				
				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							//echo"<th>Művelet</th>";
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/projects/js/inc_rights.js"></script>
		<script>$(function(){ rightsDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>
