
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Szerverek kezelő
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/projects/szerverek">Szerverek kezelő</a></li>
				<li><a href="/new">Új szerverek felvitele</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új szerverek felvitele</strong></h2>
				</div>
			<?
						
				$partner=db_all("select id, name from partners where status=1 order by name");
			
				echo "<form id=\"newszerverek\" name=\"newszerverek\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";

						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"name\">Új szerverek:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" placeholder=\"Új szerverek neve\" />																		
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"ip_cim\">Ip-cím:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"ip_cim\" name=\"ip_cim\" class=\"form-control\" placeholder=\" Ip-cím\" />																		
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"partner_id\">Partner:</label>
							<div class=\"col-md-9\">
								<select id=\"partner_id\" name=\"partner_id\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($partner);$i++){
									echo"<option value=\"".$partner[$i]['id']."\" >".$partner[$i]['name']."</option>";	
								}
								echo"</select>
							</div>
						</div>";
									
					
						
						echo "<div class=\"form-group\" >
							<label class=\"col-md-3 control-label\" for=\"status\">Érvényes-e:</label>
							<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span></label>
						</div>";
						
						echo "<div style=\"clear:both;height:10px;\"></div>";
						
						echo "<div class=\"form-group form-actions\">
							<div class=\"col-md-9 col-md-offset-3\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewszerverek();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>";
						
				echo "</div>
				</form>";
			?>
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/projects/js/inc_szerverek.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?	
		}
		else{

			//echo "select * from functions where id='".$uritags[2]."'";
			$myszerverek=db_row("select * from szerverek where id='".$uritags[3]."'");
			if($myszerverek['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="modules/projects/szerverek">Szerver</a></li>
				<li><a href="/<?=$URI;?>"><?=$myszerverek['name'];?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->

					<div class="block-title">
						<h2><strong><?=$myszerverek['name'];?></strong> szerver oldal</h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
							if(priv("szerverek","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("szerverek","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							if(priv("szerverek","acces")){echo"<li><a href=\"#tab-6\" data-toggle=\"tooltip\" title=\"acces\"><i class=\"fa fa-user-secret\"></i></a></li>";}	
							if(priv("szerverek","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("szerverek","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
							if(priv("szerverek","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}	
							?>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="col-md-6">
								<dl class="dl-horizontal">
									<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$myszerverek['id']); ?></dd>
									<dt>Név:</dt><dd><? echo $myszerverek['name']; ?></dd>
									<dt>Ip-cim:</dt><dd><? echo $myszerverek['ip_cim']; ?></dd>
									<dt>Partner_id:</dt><dd><? echo idchange("partners","name",$myszerverek['partner_id']); ?></dd>
									<dt>Státusz:</dt><dd><? if($myszerverek['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>  
								</dl>	
							</div>
							<div class="col-md-6">
								<?php
									$pic=db_one("select filename from files where modul='projects' and type='szerverek' and type_id='".$myszerverek['id']."' order by cover desc");
									if(is_file($pic)){
										echo"<div class=\"col-sm-3\"><img src=\"/".$pic."\" alt=\"avatar\" width=\"450px\"></div>";
									}
									else{
										echo"<img src=\"img/placeholders/avatars/avatar9@2x.jpg\" alt=\"avatar\">";
									}
								?>
							</div>
							<div style="clear:both;height:1px;"></div>
							<div class="block hidden-lt-ie9">
								<div class="block-title">
									<h2><strong>Hozzáférések</strong> </h2>
								</div>
								<div id="elementdiv8"></div>
								
							</div>
							
						</div>
						<div class="tab-pane" id="tab-2">
						<?
						
								
							$partner=db_all("select id, name from partners where status=1 order by name");
							
							echo"<form id=\"editszerverek\" name=\"editszerverek\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
							echo "<div style=\"margin-top:1px;\">";
							
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"name\">Új szerver :</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" value=\"".$myszerverek['name']."\" />																		
									</div>
								</div>";
								
							echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"ip_cim\"> Ip-cím</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"ip_cim\" name=\"ip_cim\" class=\"form-control\" value=\"".$myszerverek['ip_cim']."\" />																		
									</div>
								</div>";
								
							echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"partner_id\">Partner:</label>
									<div class=\"col-md-9\">
										<select id=\"user_id\" name=\"partner_id\" class=\"form-control\" >
										<option value=\"0\">Kérlek válassz</option>";
										for($i=0;$i<count($partner);$i++){
											$sel="";if($partner[$i]['id']==$myszerverek['partner_id']){$sel=" selected ";}
											echo"<option value=\"".$partner[$i]['id']."\" ".$sel.">".$partner[$i]['name']."</option>";
										}
										echo"</select>
									</div>
								</div>";
									
								
								
								echo "<div class=\"form-group\" >
									<label class=\"col-md-3 control-label\" for=\"status\">Státusz:</label>";
									$st="";if($myszerverek['status']==1){ $st=" checked "; }
									echo "<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\"".$st." ><span></span></label>
								</div>";
								
								echo "<div class=\"form-group form-actions\">
									<div class=\"col-md-9 col-md-offset-3\">
										<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditszerverek(".$myszerverek['id'].")\"><i class=\"fa fa-angle-right\"></i> Ment</button>
									</div>
								</div>";
									
							echo "</div>
							</form>";
						?>
						</div>
						<div class="tab-pane" id="tab-3">
								<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$myszerverek['id'],"Dokumentum feltöltés")."</div>";
								?> 
								<div id="filelists"></div>
								<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$myszerverek['id'],"Hozzászólások")."</div>";
								?>
						</div>
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>
						
						<div class="tab-pane" id="tab-6">

							<?
								$tipus=db_all("select id, name from hozzaferestipusok where status=1 order by name");
							?>
							
							   <div class="block hidden-lt-ie9 <?=$adc;?>" >
								<!-- Switches Title -->
									<div class="block-title">
										<h2><strong>Hozzáférések űrlap</strong> </h2>
									</div>
									<!-- END Switches Title -->

									<!-- Switches Content -->
									<form  id="newelementaddszerveraccesform" id="newelementaddszerveraccesform" class="form-horizontal form-bordered" onsubmit="return false;">

										<div class="form-group" style="width:400px;float:left;">
											<label class="col-md-3 control-label" for="hozzaferes_id">Rendszertípus:</label>
											<div class="col-md-9">
												<select id="hozzaferes_id" name="hozzaferes_id" class="form-control" >
													<option value="0">Kérlek válassz</option>
													<?
														for($i=0;$i<count($tipus);$i++){
															echo"<option value=\"".$tipus[$i]['id']."\">".$tipus[$i]['name']."</option>";	
														}
													?>
												</select>
											</div>
										</div>

										<div class="form-group" style="width:420px;float:left;">
											<label class="col-md-3 control-label" for="name">Felhasználónév:</label>
											<div class="col-md-9">
												<input type="text" id="name" name="name" class="form-control" />
											</div>
										</div>	
										
										<div class="form-group" style="width:300px;float:left;">
											<label class="col-md-3 control-label" for="password">Jelszó:</label>
											<div class="col-md-9">
												<input type="text" id="password" name="password" class="form-control" />
											</div>
										</div>								
										
										<div style="clear:both;height:10px;"></div>
										<div class="col-md-9 col-md-offset-0">
											
											<button type="button" class="btn btn-sm btn-primary" id="savenewbutton" onclick="javascript:addszerveracces(<?=$myszerverek['id'];?>,'elementdiv7');"><i class="fa fa-angle-right"></i> Ment</button>
										</div>
										<div style="clear:both;"></div>
									</form>	
									<div style="clear:both;height:10px;"></div>
					
								</div>
						
								<div class="block hidden-lt-ie9">
									<!-- Switches Title -->
									<div class="block-title">
										<h2><strong>Hozzáférések</strong> </h2>
									</div>
									<!-- END Switches Title -->

									<div id="elementdiv7"></div>
									
								</div>
						
							</div>
				
				
					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
					
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/projects/js/inc_szerverek.js"></script>

			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$myszerverek['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$myszerverek['id'];?>,'Dokumentum feltöltés','filelists');
				showszerveracces('<?=$myszerverek['id'];?>','elementdiv7');
				showszerveracces('<?=$myszerverek['id'];?>','elementdiv8');			
			</script>
			<script>$(function(){ SzerverekDatatables.init(); });</script>	

			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
		//print_r($_SESSION['planetsys'])
?>    
<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Szerverek</a></li>
			</ul>
			<!-- END Datatables Header -->

			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Szerverek</strong> lista</h2>
				</div>
				
				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							//echo"<th>Művelet</th>";
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/projects/js/inc_szerverek.js"></script>
		<script>$(function(){ SzerverekDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>
