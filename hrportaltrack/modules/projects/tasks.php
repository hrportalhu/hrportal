<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Feladat kezelő
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/projects/tasks">Feladat kezelő</a></li>
				<li><a href="/new">Új feladat felvitele</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új feladat felvitele</strong></h2>
				</div>
			<?
			
				$user=db_all("select id, realname from users where status=1 order by realname");
				$state=db_all("select id, name from states where status=1 order by name");
				$project=db_all("select id, name from pprojects where status=1 order by name");
				
				echo "<form id=\"newtasks\" name=\"newtasks\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";
						
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"project_id\">Project név:</label>
							<div class=\"col-md-9\">
								<select id=\"project_id\" name=\"project_id\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($project);$i++){
									echo"<option value=\"".$project[$i]['id']."\" >".$project[$i]['name']."</option>";	
								}
								echo"</select>
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"modul\">Modul:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"modul\" name=\"modul\" class=\"form-control\" placeholder=\"Modul neve\" />																		
							</div>
						</div>";
						
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"function\">Funkció:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"function\" name=\"function\" class=\"form-control\" placeholder=\"Funkció neve\" />																		
							</div>
						</div>";
/*						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"task\">Feladat leírása:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"task\" name=\"task\" class=\"form-control\" placeholder=\"Feladat leírása\" />																		
							</div>
						</div>";						
*/						
						echo"<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"task\">Feladat leírása:</label>
							<div class=\"col-md-9\">
								<textarea id=\"task\" name=\"task\" class=\"ckeditor\" ></textarea>
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"deadline\">Határidő:</label>
							<div class=\"col-md-9 input-group bootstrap-timepicker\">
								<input type=\"text\" id=\"deadlinedate\" name=\"deadlinedate\" class=\"form-control input-datepicker\" style=\"width:50%;float:left;\"/>																		
								<input type=\"text\" id=\"deadlinetime\" name=\"deadlinetime\" class=\"form-control input-timepicker24\" style=\"width:50%;float:left;\" />																		
								<span class=\"input-group-btn\">
									<a href=\"javascript:void(0)\" class=\"btn btn-primary\"><i class=\"fa fa-clock-o\"></i></a>
								</span>
							</div>
						</div>";
						
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"prio\">Prioritás:</label>
							<div class=\"col-md-9\">
								<select id=\"prio\" name=\"prio\" class=\"form-control\" >
									<option value=\"0\">Kérlek válassz</option>
									<option value=\"1\">1</option>
									<option value=\"2\">2</option>
									<option value=\"3\">3</option>
									<option value=\"4\">4</option>
									<option value=\"5\">5</option>";
								echo"</select>
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"user_id\">Delegálva:</label>
							<div class=\"col-md-9\">
								<select id=\"user_id\" name=\"user_id\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($user);$i++){
									echo"<option value=\"".$user[$i]['id']."\" >".$user[$i]['realname']."</option>";	
								}
								echo"</select>
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"task_status\">Státusz:</label>
							<div class=\"col-md-9\">
								<select id=\"task_status\" name=\"task_status\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($state);$i++){
									echo"<option value=\"".$state[$i]['id']."\" >".$state[$i]['name']."</option>";	
								}
								echo"</select>
							</div>
						</div>";

						echo "<div class=\"form-group\" >
							<label class=\"col-md-3 control-label\" for=\"status\">Érvényes-e?:</label>
							<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span></label>
						</div>";
							
				echo "</div>";
				echo "<div style=\"margin-top:1px;\">";
					echo "<div style=\"clear:both;height:10px;\"></div>";
						echo"<div class=\"form-group form-actions\">
							<div class=\"col-md-9 col-md-offset-3\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewtasks();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>";
				echo "</div>";
				echo "</form>";
			?>
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="js/ckeditor/ckeditor.js"></script>
			<script src="modules/projects/js/inc_tasks.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?	
		}
		else{

			//echo "select * from functions where id='".$uritags[2]."'";
			$mytasks=db_row("select * from tasks where id='".$uritags[3]."'");
			if($mytasks['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="modules/projects/tasks"> Feladat</a></li>
				<li><a href="/<?=$URI;?>"><?=limit_text(strip_tags(html_entity_decode($mytasks['task'])),40);?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->

					<div class="block-title">
						<h2><strong><?=limit_text(strip_tags(html_entity_decode($mytasks['task'])),40);?></strong> feladat oldala</h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
							if(priv("tasks","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("tasks","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							if(priv("tasks","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("tasks","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
							if(priv("tasks","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}	
							?>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="row">
								<div class="col-md-6">
										<dl class="dl-horizontal">
											<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$mytasks['id']); ?></dd>
											<dt>Feladatállapot:</dt><dd><? echo  idchange("pprojects","name",$mytasks['project_id']); ?></dd>
											<dt>Modul:</dt><dd><? echo $mytasks['modul']; ?></dd>
											<dt>Funkció:</dt><dd><? echo $mytasks['function']; ?></dd>
											<dt>Feladat:</dt><dd><? echo html_entity_decode($mytasks['task']); ?></dd>
											<dt>Prioritás:</dt><dd><? if($mytasks['prio']==1){echo"1";}elseif($mytasks['prio']==2){echo"2";}elseif($mytasks['prio']==3){echo"3";}elseif($mytasks['prio']==4){echo"4";}else{echo"5";} ?></dd>
											<dt>Delegálva:</dt><dd><? echo  idchange("users","realname",$mytasks['user_id']); ?></dd>
											<dt>Feladatállapot:</dt><dd><? echo  idchange("states","name",$mytasks['task_status']); ?></dd>
											<dt>Érvényes-e?:</dt><dd><? if($mytasks['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>									
										</dl>	
								</div>	
								<div class="col-md-6">
									<?php
										$pic=db_one("select filename from files where modul='projects' and type='tasks' and type_id='".$mytasks['id']."' order by cover desc");
										if(is_file($pic)){
											echo"<div class=\"col-sm-3\"><img src=\"/".$pic."\" alt=\"avatar\" width=\"450px\"></div>";
										}
										else{
											echo"<img src=\"img/placeholders/avatars/avatar9@2x.jpg\" alt=\"avatar\">";
										}
									?>
								</div>
							</div>	
							<div style="clear:both;height:1px;"></div>
							<div class="row">
							<div class="block hidden-lt-ie9">
								<div class="block-title">
									<h2><strong>Történet</strong> </h2>
								</div>
								<?
								$story=db_all("select * from tasks_events where task_id='".$mytasks['id']."'");
								
								?>	
								<div class="table-responsive">
									<table class="table table-vcenter table-striped">
										<thead>
											<tr>
												
												<th>Sorszám</th>
												<th>Project</th>
												<th>Felhasználó</th>
												<th>Prioritás</th>
												<th>Státusz</th>
												<th>Eltöltött idő</th>
												<th>Dátum</th>
											</tr>
										</thead>
										<tbody>
											<?php
												//$orders=db_all("select * from orders where status='1' order by order_date desc");
												for($i=0;$i<count($story);$i++){
													$idokezd=strtotime($story[$i-1]['rdate']);
													$idoveg=strtotime($story[$i]['rdate']);
													$kulonb=$idoveg-$idokezd;
													if($idokezd==""){
														$bedat="Felvéve:".$story[$i]['rdate'];
													}
													else{
														$bedat=pastorora($kulonb);
													}
													echo"<tr>";
													echo"<td>".$story[$i]['id']."</td>";
													echo"<td>".idchange("pprojects","name",$story[$i]['project_id'])."</td>";
													echo"<td>".idchange("users","realname",$story[$i]['user_id'])."</td>";
													echo"<td>".$story[$i]['prio']."</td>";
													echo"<td>".idchange("states","name",$story[$i]['status_id'])."</td>";
													echo"<td>".$bedat."</td>";
													echo"<td>".$story[$i]['rdate']."</td>";
						
													echo"</tr>";
												}
											?>
										</tbody>
									</table>
								</div>
							</div>
							
							</div>	
							<div class="row">
							<div class="block hidden-lt-ie9">
								<div class="block-title">
									<h2><strong>Hozzászólások</strong> </h2>
								</div>
								<div id="com2"></div>
							</div>
							
							</div>	
						</div>
						<div class="tab-pane" id="tab-2">
						<?
							$user=db_all("select id, realname from users where status=1 order by realname");
							$state=db_all("select id, name from states where status=1 order by name");
							$project=db_all("select id, name from pprojects where status=1 order by name");
							
							echo"<form id=\"edittasks\" name=\"edittasks\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
							echo"<div class=\"tab-content\">";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"project_id\">Project név:</label>
									<div class=\"col-md-9\">
										<select id=\"project_id\" name=\"project_id\" class=\"form-control\" >
										<option value=\"0\">Kérlek válassz</option>";
										for($i=0;$i<count($project);$i++){
											$se2="";if($project[$i]['id']==$mytasks['project_id']){$se2=" selected ";}
											echo"<option value=\"".$project[$i]['id']."\" ".$se2.">".$project[$i]['name']."</option>";
										}
										echo"</select>
									</div>
								</div>";
								
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"modul\">Modul:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"modul\" name=\"modul\" class=\"form-control\" value=\"".$mytasks['modul']."\" />																		
									</div>
								</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"function\">Funkció:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"function\" name=\"function\" class=\"form-control\" value=\"".$mytasks['function']."\" />																		
									</div>
								</div>";
/*								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"task\">Feladat leírás:</label>
									<div class=\"col-md-9\">
										<input type=\"text\" id=\"task\" name=\"task\" class=\"form-control\" value=\"".$mytasks['task']."\" />																		
									</div>
								</div>";
*/								
								echo"<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"task\">Feladat leírása:</label>
									<div class=\"col-md-9\">
										<textarea id=\"task\" name=\"task\" class=\"ckeditor\" >".$mytasks['task']."</textarea>
									</div>
								</div>";
								
														
							echo "<div class=\"form-group\">
								<label class=\"col-md-3 control-label\" for=\"deadline\">Határidő:</label>
								<div class=\"col-md-9 input-group bootstrap-timepicker\">
									<input type=\"text\" id=\"deadlinedate\" name=\"deadlinedate\" class=\"form-control input-datepicker\" style=\"width:50%;float:left;\" value=\"".substr($mytasks['deadline'],0,10)."\"/>																		
									<input type=\"text\" id=\"deadlinetime\" name=\"deadlinetime\" class=\"form-control input-timepicker24\" style=\"width:50%;float:left;\" value=\"".substr($mytasks['deadline'],11,5)."\" />																		
									<span class=\"input-group-btn\">
										<a href=\"javascript:void(0)\" class=\"btn btn-primary\"><i class=\"fa fa-clock-o\"></i></a>
									</span>
								</div>
							</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"prio\">Prioritás:</label>
									<div class=\"col-md-9\">
										<select id=\"prio\" name=\"prio\" class=\"form-control\" >
											<option value=\"0\">Kérlek válassz</option>";
											$ch2="";if($mytasks['prio']== 1){$ch2=" selected ";}
											echo "<option value=\"1\" ".$ch2.">1</option>";
											$ch2="";if($mytasks['prio']== 2){$ch2=" selected ";}
											echo "<option value=\"2\" ".$ch2.">2</option>";
											$ch2="";if($mytasks['prio']== 3){$ch2=" selected ";}
											echo "<option value=\"3\" ".$ch2.">3</option>";
											$ch2="";if($mytasks['prio']== 4){$ch2=" selected ";}
											echo "<option value=\"4\" ".$ch2.">4</option>";
											$ch2="";if($mytasks['prio']== 5){$ch2=" selected ";}
											echo "<option value=\"5\" ".$ch2.">5</option>";
										echo"</select>
									</div>
								</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"user_id\">Delegálva:</label>
									<div class=\"col-md-9\">
										<select id=\"user_id\" name=\"user_id\" class=\"form-control\" >
										<option value=\"0\">Kérlek válassz</option>";
										for($i=0;$i<count($user);$i++){
											$se2="";if($user[$i]['id']==$mytasks['user_id']){$se2=" selected ";}
											echo"<option value=\"".$user[$i]['id']."\" ".$se2.">".$user[$i]['realname']."</option>";
										}
										echo"</select>
									</div>
								</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-3 control-label\" for=\"task_status\">Állapot:</label>
									<div class=\"col-md-9\">
										<select id=\"task_status\" name=\"task_status\" class=\"form-control\" >
										<option value=\"0\">Kérlek válassz</option>";
										for($i=0;$i<count($state);$i++){
											$sel="";if($state[$i]['id']==$mytasks['task_status']){$sel=" selected ";}
											echo"<option value=\"".$state[$i]['id']."\" ".$sel.">".$state[$i]['name']."</option>";
										}
										echo"</select>
									</div>
								</div>";
								
								echo "<div class=\"form-group\" >
									<label class=\"col-md-3 control-label\" for=\"status\">Érvényes-e?:</label>";
									$st="";if($mytasks['status']==1){ $st=" checked "; }
									echo "<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\"".$st." ><span></span></label>
								</div>";
								
								echo "<div class=\"form-group\" >
									<label class=\"col-md-3 control-label\" for=\"lezarva\">Lezárva?:</label>";
									$st="";if($mytasks['lezarva']==1){ $st=" checked "; }
									echo "<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"lezarva\" name=\"lezarva\"".$st." ><span></span></label>
								</div>";
								
							
							echo "</div>";
							echo "<div style=\"margin-top:1px;\">";
								echo "<div style=\"clear:both;height:10px;\"></div>";	
								echo "<div class=\"form-group form-actions\">
									<div class=\"col-md-9 col-md-offset-3\">
										<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveedittasks(".$mytasks['id'].")\"><i class=\"fa fa-angle-right\"></i> Ment</button>
									</div>
								</div>";									
							echo "</div>";
						echo "</form>";
						?>
						</div>
						<div class="tab-pane" id="tab-3">
								<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mytasks['id'],"Dokumentum feltöltés")."</div>";
								?> 
								<div id="filelists"></div>
								<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mytasks['id'],"Hozzászólások")."</div>";
								?>
						</div>
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>
				
					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
					
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="js/ckeditor/ckeditor.js"></script>
			<script src="modules/projects/js/inc_tasks.js"></script>

			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mytasks['id'];?>,'Dokumentum feltöltés','filelists');
				showcommentlist2('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mytasks['id'];?>,'Dokumentum feltöltés','com2');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$mytasks['id'];?>,'Dokumentum feltöltés','filelists');
			</script>
			

			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
		//print_r($_SESSION['planetsys'])
?>    
<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Feladatok</a></li>
			</ul>
			<!-- END Datatables Header -->

			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Feladatok</strong> listája</h2>
				</div>
				
				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							//echo"<th>Művelet</th>";
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/projects/js/inc_tasks.js"></script>
		<script>$(function(){ TasksDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>
