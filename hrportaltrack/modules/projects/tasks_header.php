<?
include dirname(__FILE__).("/inc.tasks.php");
naplo('info','tasks','Feladat kezelő betöltése');
$_SESSION['planetsys']['actpage']=$screen;

if(priv("tasks","setup")){
?>
<li class="dropdown">
	<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
		<i class="gi gi-cogwheels"></i>
	</a>
	<ul class="dropdown-menu dropdown-custom dropdown-options">
		<li class="dropdown-header text-center">Beállítások</li>
		<li>
			<form name="setupform" id="setupform">
			<?
			$lq="select * from listoptions where user_id='".$_SESSION['planetsys']['user_id']."' and modulname='".$_SESSION['planetsys']['actpage']['modulname']."' and funcname='".$_SESSION['planetsys']['actpage']['funcname']."'";
			$listoptions=db_row($lq);
			$listelemets=explode(",",$listoptions['screenlist']);
			foreach($_SESSION['planetsys']['actpage']['elements'] as $record){
				if(in_array($record['name'],$listelemets)){	$ch=" checked=\"checked\" ";}else{$ch=" ";}
				echo"<div ><input type=\"checkbox\" id=\"AAA_".$record['name']."\" name=\"AAA_".$record['name']."\" ".$ch." onclick=\"javascript:savesetupscreen();\" />&nbsp;".$record['textname']."</div>";
			}
			?>
			<div class="clear"></div>
			</form>
		</li>
	</ul>
</li>
<?
}
if(priv("tasks","export")){
?>
<li class="dropdown">
	<a href="#modal-regular-export" data-toggle="modal">
		<i class="fa fa-briefcase" title="export"></i>
	</a>
</li>
<?
}
if(priv("tasks","filter")){
?>
<li class="dropdown">
	<a href="#modal-regular-filter" data-toggle="modal">
		<i class="fa fa-filter" title="filter"></i>
	</a>
</li>
<?
}
if(priv("tasks","new")){
?>
<li class="dropdown">
	<a href="/<?=$template['workpage'];?>/new " >
		<i class="gi gi-bomb" title="New"></i>
	</a>	
</li>
<form  id="newelementform" id="newelementform" class="form-horizontal form-bordered" onsubmit="return false;">
<?
}
	echo"<div id=\"modal-regular-filter\" class=\"modal\" tabindex=\"-2\" role=\"dialog\" aria-hidden=\"true\">
			<div class=\"modal-dialog\">
				<div class=\"modal-content\">
					<div class=\"modal-header\">
						<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
						<h3 class=\"modal-title\">Szűrés</h3>
					</div>
					<div class=\"modal-body\">";
						
					$user=db_all("select id, realname from users where status=1 order by realname");
					$state=db_all("select id, name from states where status=1 order by name");
					$project=db_all("select id, name from pprojects where status=1 order by name");
					?>
					<div class="form-group" >
						<label class="col-md-3 control-label" for="projectselect">Projektek:</label>
						<div class="col-md-9">
							<select id="projectselect" name="projectselect" class="form-control" >
								<option value="0">Kérlek válassz</option>
								<?
									for($i=0;$i<count($project);$i++){
										echo"<option value=\"".$project[$i]['id']."\">".$project[$i]['name']."</option>";	
									}
								?>
							</select>
						</div>
					</div>
					<div class="form-group" >
						<label class="col-md-3 control-label" for="userselect">Delegálva:</label>
						<div class="col-md-9">
							<select id="userselect" name="userselect" class="form-control" >
								<option value="0">Kérlek válassz</option>
								<?
									for($i=0;$i<count($user);$i++){
										echo"<option value=\"".$user[$i]['id']."\">".$user[$i]['realname']."</option>";	
									}
								?>
							</select>
						</div>
					</div>
					<div class="form-group" >
						<label class="col-md-3 control-label" for="allapotselect">Állapot:</label>
						<div class="col-md-9">
							<select id="allapotselect" name="allapotselect" class="form-control" >
								<option value="0">Kérlek válassz</option>
								<?
									for($i=0;$i<count($state);$i++){
										echo"<option value=\"".$state[$i]['id']."\">".$state[$i]['name']."</option>";	
									}
								?>
							</select>
						</div>
					</div>
					<div class="form-group" >
								<label class="col-md-3 control-label" for="prioselect">Prioritás</label>
								<div class="col-md-9">
									<select id="prioselect" name="prioselect" class="form-control"  >
										<option value="0">Kérlek válassz</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
									</select>
								</div>
							</div>
					<div style="clear:both;height:5px;"></div>									
					<div class="form-group" >
								<label class="col-md-3 control-label" for="lezarva">Prioritás</label>
								<div class="col-md-9">
									<select id="lezarva" name="lezarva" class="form-control"  >
										<option value="0">Nem lezárt feladatok</option>
										<option value="1">Lezárt feladatok</option>
									</select>
								</div>
							</div>
					<div style="clear:both;height:5px;"></div>
					<?										
					echo"</div>
					<div class=\"modal-footer\">
						<button type=\"button\" class=\"btn btn-sm btn-warning\" onclick=\"javascript:TasksDatatables.init();\" >Szűr</button>
						<button type=\"button\" class=\"btn btn-sm btn-default\" data-dismiss=\"modal\">Bezár</button>
					</div>
				</div>
			</div>
		</div>";	
?>
</form>
