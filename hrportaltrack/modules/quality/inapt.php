
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Helyesbítő intézkedések
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/quality/inapt">Helyesbítő intézkedések</a></li>
				<li><a href="/new">Új helyesbítő intézkedés</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új helyesbítő intézkedés</strong></h2>
				</div>
			<?
				$categories=db_all("select * from qm_inapt_category where status='1' order by name asc");
				
				echo "<form id=\"newinapt\" name=\"newinapt\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";
			
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"name\">Helyesbítő intézkedés kódja:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" value=\"INAPT-".generateCode(3)."-".date("Ymd",time())."\" />
							</div>
						</div>";					
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"category_id\">Kategória:</label>
							<div class=\"col-md-8\">
								<select id=\"category_id\" name=\"category_id\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($categories);$i++){
									
									echo"<option value=\"".$categories[$i]['id']."\" >".$categories[$i]['name']."</option>";	
								}
								echo"</select>
							</div>
						</div>";
						echo"<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"inapt_date\">Deklaráció ideje:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"inapt_date\" name=\"inapt_date\" class=\"form-control input-datepicker\" value=\"".date("Y-m-d",time())."\" />
							</div>
						</div>";
						echo"<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"inapt\">Nem megfeleősség:</label>
							<div class=\"col-md-8\">
								<textarea id=\"inapt\" name=\"inapt\" rows=\"6\" class=\"ckeditor\" ></textarea>
							</div>
						</div>";
						echo "<div style=\"clear:both;height:10px;\"></div>";
						
						echo "<div class=\"form-group form-actions\">
							<div class=\"col-md-6 col-md-offset-3\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewinapt();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>";
						
				echo "</div>
				</form>";
			?>
			</div>	
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="js/ckeditor/ckeditor.js"></script>
			<script src="modules/quality/js/inc_inapt.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?	
		}
		else{

			//echo "select * from functions where id='".$uritags[2]."'";
			$myinapt=db_row("select * from qm_inapt where id='".$uritags[3]."'");
			if($myinapt['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
				
?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="modules/quality/inapt"> Helyesbítő intézkedés</a></li>
				<li><a href="/<?=$URI;?>"><?=$myinapt['name'];?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->

					<div class="block-title">
						<h2><strong><?=$myinapt['name'];?> </strong> helyesbítő intézkedés</h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
							if(priv("inapt","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("inapt","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							if(priv("inapt","corrective")){echo"<li><a href=\"#tab-6\" data-toggle=\"tooltip\" title=\"Helyesbítő intézkedések\"><i class=\"fa fa-lightbulb-o\"></i></a></li>";}
							if(priv("inapt","print")){echo"<li><a href=\"#tab-99\" data-toggle=\"tooltip\" title=\"Történet\" onclick=\"javascript:showstory('".$myinapt['id']."');\"><i class=\"gi gi-history\"></i></a></li>";}			
							if(priv("inapt","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("inapt","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
							if(priv("inapt","print")){echo"<li onclick=\"javascript:getprintscreen('".$_SESSION['planetsys']['actpage']['modulname']."','inaptprint','".$myinapt['id']."');\" ><a href=\"#modal-regular-print\" data-toggle=\"modal\" title=\"Nyomtatás\"><i class=\"fa fa-print\" ></i></a></li>";}
							?>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="col-md-6">
								<dl class="dl-horizontal">
									<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$myinapt['id']); ?></dd>
									<dt>Kódja:</dt><dd><? echo $myinapt['name']; ?></dd>
									<dt>Kategória:</dt><dd><? echo idchange("qm_inapt_category","name",$myinapt['category_id']); ?></dd>
									<dt>Deklaráció ideje:</dt><dd><? echo $myinapt['inapt_date']; ?></dd>
									<dt>Deklaráló személy:</dt><dd><? echo idchange("users","name",$myinapt['user_id']); ?></dd>
									<dt>Deklaráció:</dt><dd style="width:400px;"><? echo $myinapt['inapt']; ?></dd>
									
								</dl>	
							</div>
							<div class="col-md-6"></div>
							<div style="clear:both;height:1px;"></div>
							<div class="block">
									<div class="block-title">
										<h2><strong>Helyesbítő intézkedések</strong></h2>
									</div>
									<div class="table-responsive">
										<table class="table table-vcenter table-striped">
										<thead>
											<tr>
												<th style="width: 100px;" class="text-center"><i class="gi gi-disk_save"></i></th>
												<th class="text-center" style="width:200px;">Intézkedés</th>
												<th class="text-center">Határidő</th>
												<th class="text-center">Felelős</th>
												<th class="text-center">Ellenőrzés dátuma</th>
												<th class="text-center">Ellenőrzés</th>
												<th class="text-center">Állapot</th>
												
												
											</tr>
										</thead>
										<tbody  >
							<?
							$sutik=db_all("select * from qm_inapt_corrective where inapt_id='".$myinapt['id']."' and status>0 order by id desc");
								if(count($sutik)>0){
									for($i=0;$i<count($sutik);$i++){
										
										if($sutik[$i]['status']==2){$ad="lezárva";}
										elseif($sutik[$i]['status']==1){$ad="aktív";}
										
										echo"<tr > ";
										echo"<td style=\"width: 100px;\" class=\"text-center\">".$sutik[$i]['id']."</td>";
										
										echo"<td class=\"text-left\">".$sutik[$i]['c_name']."</td>";
										echo"<td class=\"text-center\">".$sutik[$i]['c_date']."</td>";
										echo"<td class=\"text-center\">".idchange("users","realname",$sutik[$i]['user_id'])."</td>";
										echo"<td class=\"text-center\">".$sutik[$i]['ve_date']."</td>";
										echo"<td class=\"text-center\">".$sutik[$i]['ve_comment']."</td>";
										echo"<td class=\"text-center\">".$ad."</td>";

										echo"</tr>";
									}
								}
								?>
								</tbody>
								</table>		
									</div>
						
						</div>
						<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-2">
						<?
						$categories=db_all("select * from qm_inapt_category where status='1' order by name asc");
							
							echo"<form id=\"editinapt\" name=\"editinapt\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
							echo "<div style=\"margin-top:1px;\">";
								
							echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"name\">Helyesbítő intézkedés kódja:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" value=\"".$myinapt['name']."\" />
							</div>
						</div>";					
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"category_id\">Kategória:</label>
							<div class=\"col-md-8\">
								<select id=\"category_id\" name=\"category_id\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($categories);$i++){
									$sel="";if($myinapt['category_id']==$categories[$i]['id']){$sel=" selected ";}
									echo"<option value=\"".$categories[$i]['id']."\" ".$sel.">".$categories[$i]['name']."</option>";	
								}
								echo"</select>
							</div>
						</div>";
						echo"<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"inapt_date\">Deklaráció ideje:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"inapt_date\" name=\"inapt_date\" class=\"form-control input-datepicker\" value=\"".$myinapt['inapt_date']."\" />
							</div>
						</div>";
						echo"<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"inapt\">Nem megfelelősség:</label>
							<div class=\"col-md-8\">
								<textarea id=\"inapt\" name=\"inapt\" rows=\"6\" class=\"ckeditor\" >".$myinapt['inapt']."</textarea>
							</div>
						</div>";

								echo "<div class=\"form-group form-actions\">
									<div class=\"col-md-6 col-md-offset-3\">
										<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditinapt(".$myinapt['id'].")\"><i class=\"fa fa-angle-right\"></i> Ment</button>
									</div>
								</div>";
									
							echo "</div>
							</form>";
						?>
						</div>

						<div class="tab-pane" id="tab-3">
								<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$myinapt['id'],"Dokumentum feltöltés")."</div>";
								?> 
								<div id="filelists"></div>
								<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$myinapt['id'],"Hozzászólások")."</div>";
								?>
						</div>
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>
						<div class="tab-pane" id="tab-99">
							<div id="storydiv"></div>
						</div>
						<div class="tab-pane" id="tab-6">
							<div class="block">
								<div class="block-title">
									<h2><strong>Helyesbítő intézkedések</strong></h2>
								</div>
								<div class="table-responsive">
									<table class="table table-vcenter table-striped">
									<thead>
										<tr>
											<th style="width: 100px;" class="text-center"><i class="gi gi-disk_save"></i></th>
											<th class="text-center" style="width:200px;">Intézkedés</th>
											<th class="text-center">Határidő</th>
											<th class="text-center">Felelős</th>
											<th class="text-center">Ellenőrzés dátuma</th>
											<th class="text-center">Ellenőrzés</th>
											<th class="text-center">Állapot</th>
											<th class="text-center" style="width: 130px;">Művelet</th>
											
										</tr>
									</thead>
									<tbody  id="inaptcont"></tbody>
									</table>		
								</div>
							</div>
						</div>
					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
					
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="js/ckeditor/ckeditor.js"></script>
			<script src="modules/quality/js/inc_inapt.js"></script>

			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$myinapt['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$myinapt['id'];?>,'Dokumentum feltöltés','filelists');
				showinapttable('<?=$myinapt['id'];?>');
			</script>
			<script>$(function(){ inaptDatatables.init(); });</script>	

			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
		//print_r($_SESSION['planetsys'])
?>    
<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Helyesbítő intézkedések</a></li>
			</ul>
			<!-- END Datatables Header -->
		
				
				
			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Helyesbítő intézkedések</strong></h2>
				</div>
				
				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							//echo"<th>Művelet</th>";
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/quality/js/inc_inapt.js"></script>
		<script>$(function(){ inaptDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>

