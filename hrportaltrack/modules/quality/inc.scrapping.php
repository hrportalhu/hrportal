<?php
$most=date('Y-m-d',time());

$screen=array(
	"elements"=>array(
		"id"=>array(
			"name"=>"id",	
			"textname"=>"Sorszám",	
			"type"=>"text50",	
			"type_param"=>"",	
			"help"=>"",	
			"defval"=>"",	
			"connected"=>0,	
			"con_table"=>"",	
			"con_table_shortname"=>"",	
			"con_table_fields"=>"",	
			"width"=>"",	
			"deflist"=>0,	
			"scrdb"=>0,	
			"scrcmd"=>1	
		),
		"type"=>array(
			"name"=>"type",	
			"textname"=>"Típus",	
			"type"=>"text50",	
			"type_param"=>"",	
			"help"=>"",	
			"defval"=>"",	
			"connected"=>0,	
			"con_table"=>"",	
			"con_table_shortname"=>"",	
			"con_table_fields"=>"",	
			"width"=>"",	
			"deflist"=>1,	
			"scrdb"=>1,	
			"scrcmd"=>1	
		),
		"elem_name"=>array(
			"name"=>"elem_name",	
			"textname"=>"Elnevezés",	
			"type"=>"text50",	
			"type_param"=>"",	
			"help"=>"",	
			"defval"=>"",	
			"connected"=>0,	
			"con_table"=>"",	
			"con_table_shortname"=>"",	
			"con_table_fields"=>"",	
			"width"=>"",	
			"deflist"=>1,	
			"scrdb"=>1,	
			"scrcmd"=>1	
		),
		"scarpname"=>array(
			"name"=>"scarpname",	
			"textname"=>"Egyébnév",	
			"type"=>"text50",	
			"type_param"=>"",	
			"help"=>"",	
			"defval"=>"",	
			"connected"=>0,	
			"con_table"=>"",	
			"con_table_shortname"=>"",	
			"con_table_fields"=>"",	
			"width"=>"",	
			"deflist"=>1,	
			"scrdb"=>1,	
			"scrcmd"=>1	
		),
		"me"=>array(
			"name"=>"me",	
			"textname"=>"Mennyisége",	
			"type"=>"text50",	
			"type_param"=>"",	
			"help"=>"",	
			"defval"=>"",	
			"connected"=>0,	
			"con_table"=>"",	
			"con_table_shortname"=>"",	
			"con_table_fields"=>"",	
			"width"=>"",	
			"deflist"=>1,	
			"scrdb"=>1,	
			"scrcmd"=>1	
		),
		"price_all"=>array(
			"name"=>"price_all",	
			"textname"=>"Selejt érték",	
			"type"=>"text50",	
			"type_param"=>"",	
			"help"=>"",	
			"defval"=>"",	
			"connected"=>0,	
			"con_table"=>"",	
			"con_table_shortname"=>"",	
			"con_table_fields"=>"",	
			"width"=>"",	
			"deflist"=>1,	
			"scrdb"=>1,	
			"scrcmd"=>1	
		),
		"selejt_date"=>array(
			"name"=>"selejt_date",	
			"textname"=>"Selejtezés dátuma",	
			"type"=>"text50",	
			"type_param"=>"",	
			"help"=>"",	
			"defval"=>"",	
			"connected"=>0,	
			"con_table"=>"",	
			"con_table_shortname"=>"",	
			"con_table_fields"=>"",	
			"width"=>"",	
			"deflist"=>1,	
			"scrdb"=>1,	
			"scrcmd"=>1	
		),
		"post_id"=>array(
			"name"=>"post_id",	
			"textname"=>"Munkahely neve",	
			"type"=>"selectsql",	
			"type_param"=>"SELECT id, name FROM gw_posts WHERE status = 1 order by name",	
			"help"=>"",	
			"defval"=>"",	
			"connected"=>1,	
			"con_table"=>"gw_posts",	
			"con_table_shortname"=>"gp",	
			"con_table_fields"=>array(
				0=>"id",
				1=>"name"
			),	
			"width"=>"",	
			"deflist"=>1,	
			"scrdb"=>1,	
			"scrcmd"=>1	
		),
		"aproved"=>array(
			"name"=>"aproved",	
			"textname"=>"Jóváhagyva",	
			"type"=>"status",	
			"type_param"=>"",	
			"help"=>"",	
			"defval"=>"",	
			"connected"=>0,	
			"con_table"=>"",	
			"con_table_shortname"=>"",	
			"con_table_fields"=>"",	
			"width"=>"",	
			"deflist"=>0,	
			"scrdb"=>1,	
			"scrcmd"=>1	
		),
		"status"=>array(
			"name"=>"status",	
			"textname"=>"Státusz",	
			"type"=>"status",	
			"type_param"=>"",	
			"help"=>"",	
			"defval"=>"",	
			"connected"=>0,	
			"con_table"=>"",	
			"con_table_shortname"=>"",	
			"con_table_fields"=>"",	
			"width"=>"",	
			"deflist"=>0,	
			"scrdb"=>1,	
			"scrcmd"=>1	
		)
	),
	"tableprefix"=>"",
	"modulname"=>"quality",
	"funcname"=>"scrapping",
	"mylimit"=>"20",
	"mywidth"=>"",
	"scrtable"=>"scrapping",
	"scrtable_shortname"=>"s",
);

getlistoptions($screen);
$screen['screenlist']=$_SESSION['planetsys']['listoption']['screenlist'];
$screen['screendb']=$_SESSION['planetsys']['screendb'];

?>
