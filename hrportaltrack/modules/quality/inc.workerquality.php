<?php
$most=date('Y-m-d',time());

$screen=array(
	"elements"=>array(
		"id"=>array(
			"name"=>"id",	
			"textname"=>"Sorszám",	
			"type"=>"text50",	
			"type_param"=>"",	
			"help"=>"",	
			"defval"=>"",	
			"connected"=>0,	
			"con_table"=>"",	
			"con_table_shortname"=>"",	
			"con_table_fields"=>"",	
			"width"=>"",	
			"deflist"=>0,	
			"scrdb"=>0,	
			"scrcmd"=>1	
		),
			"worker_id"=>array(
			"name"=>"worker_id",	
			"textname"=>"Dolgozó neve",	
			"type"=>"selectsql",	
			"type_param"=>"SELECT id, name FROM gw_workers WHERE status = 1 order by name",	
			"help"=>"",	
			"defval"=>"",	
			"connected"=>1,	
			"con_table"=>"gw_workers",	
			"con_table_shortname"=>"gw",	
			"con_table_fields"=>array(
				0=>"id",
				1=>"name"
			),	
			"width"=>"",	
			"deflist"=>1,	
			"scrdb"=>1,	
			"scrcmd"=>1	
		),
		"ert_date"=>array(
			"name"=>"ert_date",	
			"textname"=>"Értékelés dátuma",	
			"type"=>"text50",	
			"type_param"=>"",	
			"help"=>"",	
			"defval"=>"",	
			"connected"=>0,	
			"con_table"=>"",	
			"con_table_shortname"=>"",	
			"con_table_fields"=>"",	
			"width"=>"",	
			"deflist"=>1,	
			"scrdb"=>1,	
			"scrcmd"=>1	
		),
		"szumma"=>array(
			"name"=>"szumma",	
			"textname"=>"Százalék",	
			"type"=>"text50",	
			"type_param"=>"",	
			"help"=>"",	
			"defval"=>"",	
			"connected"=>0,	
			"con_table"=>"",	
			"con_table_shortname"=>"",	
			"con_table_fields"=>"",	
			"width"=>"",	
			"deflist"=>1,	
			"scrdb"=>1,	
			"scrcmd"=>1	
		),
		"min1"=>array(
			"name"=>"min1",	
			"textname"=>"Norma",	
			"type"=>"text50",	
			"type_param"=>"",	
			"help"=>"",	
			"defval"=>"",	
			"connected"=>0,	
			"con_table"=>"",	
			"con_table_shortname"=>"",	
			"con_table_fields"=>"",	
			"width"=>"",	
			"deflist"=>1,	
			"scrdb"=>1,	
			"scrcmd"=>1	
		),
		"min2"=>array(
			"name"=>"min2",	
			"textname"=>"Minőség",	
			"type"=>"text50",	
			"type_param"=>"",	
			"help"=>"",	
			"defval"=>"",	
			"connected"=>0,	
			"con_table"=>"",	
			"con_table_shortname"=>"",	
			"con_table_fields"=>"",	
			"width"=>"",	
			"deflist"=>1,	
			"scrdb"=>1,	
			"scrcmd"=>1	
		),
		"min3"=>array(
			"name"=>"min3",	
			"textname"=>"Lojalitás",	
			"type"=>"text50",	
			"type_param"=>"",	
			"help"=>"",	
			"defval"=>"",	
			"connected"=>0,	
			"con_table"=>"",	
			"con_table_shortname"=>"",	
			"con_table_fields"=>"",	
			"width"=>"",	
			"deflist"=>1,	
			"scrdb"=>1,	
			"scrcmd"=>1	
		),
		"min4"=>array(
			"name"=>"min4",	
			"textname"=>"Higiéna",	
			"type"=>"text50",	
			"type_param"=>"",	
			"help"=>"",	
			"defval"=>"",	
			"connected"=>0,	
			"con_table"=>"",	
			"con_table_shortname"=>"",	
			"con_table_fields"=>"",	
			"width"=>"",	
			"deflist"=>1,	
			"scrdb"=>1,	
			"scrcmd"=>1	
		),
		"min5"=>array(
			"name"=>"min5",	
			"textname"=>"Moralizácó",	
			"type"=>"text50",	
			"type_param"=>"",	
			"help"=>"",	
			"defval"=>"",	
			"connected"=>0,	
			"con_table"=>"",	
			"con_table_shortname"=>"",	
			"con_table_fields"=>"",	
			"width"=>"",	
			"deflist"=>1,	
			"scrdb"=>1,	
			"scrcmd"=>1	
		),
		"status"=>array(
			"name"=>"status",	
			"textname"=>"Státusz",	
			"type"=>"status",	
			"type_param"=>"",	
			"help"=>"",	
			"defval"=>"",	
			"connected"=>0,	
			"con_table"=>"",	
			"con_table_shortname"=>"",	
			"con_table_fields"=>"",	
			"width"=>"",	
			"deflist"=>0,	
			"scrdb"=>1,	
			"scrcmd"=>1	
		)
	),
	"tableprefix"=>"",
	"modulname"=>"quality",
	"funcname"=>"worker_quality",
	"mylimit"=>"20",
	"mywidth"=>"",
	"scrtable"=>"gw_worker_quality",
	"scrtable_shortname"=>"gwq",
);

getlistoptions($screen);
$screen['screenlist']=$_SESSION['planetsys']['listoption']['screenlist'];
$screen['screendb']=$_SESSION['planetsys']['screendb'];

?>
