/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */

var inaptDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/quality/inapt/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"aaSorting": [[ 0, "desc" ]],
				"iDisplayLength": 30,
				"bDestroy": true,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/quality/ajax_inapt_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					aoData.push({"name": "name", "value":$("#name").val() });
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });
            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveeditinapt(elemid){
	var valid = f_c($('#editinapt'));
	if(valid){
		var inaptText = CKEDITOR.instances.inapt.getData();
		f_xml(ServerName +'/modules/quality/ajax_inapt_data.php','view=edit&inapttext='+inaptText+'&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editinapt').serialize(),'');
	}
}

function savenewinapt(){
	var valid = f_c($('#newinapt'));
	if(valid){
		var inaptText = CKEDITOR.instances.inapt.getData();
		f_xml(ServerName +'/modules/quality/ajax_inapt_data.php','newsubmit=1&inapttext='+inaptText+'&' + $('#newinapt').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/quality/ajax_inapt_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}
function showinapttable(elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/quality/ajax_showinapttable.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#inaptcont').html(data);
			
		}
	});	
}

function saveinaptdata(elemid){
	if(confirm("Biztos rögzíted a bejegyzést?")){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/quality/ajax_saveinaptdata.php',
		data:'elemid='+elemid
			+'&c_name='+$('#c_name').val()
			+'&c_date='+$('#c_date').val()
			+'&user_id='+$('#user_id').val()
			+'&ve_date='+$('#ve_date').val()
			+'&ve_comment='+$('#ve_comment').val()
		,
		success: function(data){
			showinapttable(elemid);
		}
	});	
	}
}
function editinaptadat(editid,elemid){
	
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/quality/ajax_editinaptadat.php',
		data:'elemid='+elemid
			+'&editid='+editid
		,
		success: function(data){
			$('#inaptdataelem').html(data);
		}
	});	

}
function delinaptadat(editid,elemid){
	if(confirm("Biztos törlöd a bejegyzést?")){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/quality/ajax_delinaptadat.php',
		data:'elemid='+elemid
			+'&editid='+editid
		,
		success: function(data){
			showinapttable(elemid);
		}
	});	
	}
}
function veinaptadat(editid,elemid){
	
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/quality/ajax_veinaptadat.php',
		data:'elemid='+elemid
			+'&editid='+editid
		,
		success: function(data){
			showinapttable(elemid);
		}
	});	
	
}


function saveeditinaptdata(editid,elemid){
	if(confirm("Biztos módosítod  a bejegyzést?")){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/quality/ajax_saveeditinaptdata.php',
		data:'elemid='+elemid
			+'&editid='+editid
			+'&c_name='+$('#c_name').val()
			+'&c_date='+$('#c_date').val()
			+'&user_id='+$('#user_id').val()
			+'&ve_date='+$('#ve_date').val()
			+'&ve_comment='+$('#ve_comment').val()
		,
		success: function(data){
			showinapttable(elemid);
		}
	});	
	}
}
delinaptadat
