/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */

var scrappingDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/quality/scrapping/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"aaSorting": [[ 0, "desc" ]],
				"iDisplayLength": 30,
				"bDestroy": true,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/quality/ajax_scrapping_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					aoData.push({"name": "name", "value":$("#name").val() });
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });
            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveeditscrapping(elemid){
	if( $("#price_all").val()!=0 ){
		f_xml(ServerName +'/modules/quality/ajax_scrapping_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editscrapping').serialize(),'');
	}
	else{
		alert("Meg kell határozni a selejt értékét a jóváhagyáskor! ");	
	}
}

function savenewscrapping(){
	if($("#scrapname").val()!=""){
		f_xml(ServerName +'/modules/quality/ajax_scrapping_data.php','newsubmit=1&' + $('#newscrapping').serialize(),'');	
	}
	else{
		//if( $("#eetetel_id").val() < $("#me").val() ){
		//	alert($("#eetetel_id").val() +"<"+ $("#me").val());
		//	alert("Nem lehet ennyit selejtezni! ");	
		//}
		//else{
			if($("#tetel").val()!=0){
				if($("#me").val()!=0){
					if($("#selejt_comment").val()!=""){
						f_xml(ServerName +'/modules/quality/ajax_scrapping_data.php','newsubmit=1&' + $('#newscrapping').serialize(),'');	
					}
					else{
						alert("Üres megjegyzés nem lehet a selejtezés mellett!");		
					}
				}
				else{
					alert("Nullás mennyiséget nem lehet selejtezni!");		
				}		
			}
			else{
				alert("Nincs meghatározott mennyiségi egység!");		
			}
		//}
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/quality/ajax_scrapping_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}
function getscappingtype(){
		//$('#elem_id').disabled();
		$( "#elem_id" ).prop( "disabled", true );
		$.ajax({
		type:'POST',
		url: ServerName +'/modules/quality/ajax_getscappingtype.php',
		data:'type='+ $("#type").val(),
		success: function(data){
			$( "#elem_id" ).prop( "disabled", false );
			$('#elem_id').html(data);
		}});	
}
function getscappingtetel(){
		//$('#elem_id').disabled();
		$( "#elem_tetel_id" ).prop( "disabled", true );
		$.ajax({
		type:'POST',
		url: ServerName +'/modules/quality/ajax_getscappingtetel.php',
		data:'type='+ $("#type").val()+'&elemid='+$("#elem_id").val(),
		success: function(data){
			$( "#elem_tetel_id" ).prop( "disabled", false );
			$('#elem_tetel_id').html(data);
		}});	
}
function getscappingme(){
		//$('#elem_id').disabled();
		$( "#me" ).prop( "disabled", true );
		$.ajax({
		type:'POST',
		url: ServerName +'/modules/quality/ajax_getscappingme.php',
		data:'type='+ $("#type").val()+'&elemid='+$("#elem_id").val()+'&elemtetel='+$("#elem_tetel_id").val(),
		success: function(data){
			$( "#me" ).prop( "disabled", false );
			$('#eetetel_id').val(data);
		}});	
}
