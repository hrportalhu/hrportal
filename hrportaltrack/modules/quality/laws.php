
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Jogszabályok
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/quality/laws">Jogszabály kezelő</a></li>
				<li><a href="/new">Új minősítés</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új minősítés</strong></h2>
				</div>
			<?
			
				
				echo "<form id=\"newlaws\" name=\"newlaws\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";
							
						
			
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"name\">Dokumentum elnevezése:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" placeholder=\"Dokumentum elnevezése\" />
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"megjegyzes\">Dokumentum tartalma:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"megjegyzes\" name=\"megjegyzes\" class=\"form-control\" placeholder=\"Dokumentum tartalma\" />																		
							</div>
						</div>";
					
						
						echo "<div class=\"form-group\" >
							<label class=\"col-md-6 control-label\" for=\"status\">Érvényes-e:</label>
							<label class=\"col-md-6 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span></label>
						</div>";
						
						echo "<div style=\"clear:both;height:10px;\"></div>";
						
						echo "<div class=\"form-group form-actions\">
							<div class=\"col-md-6 col-md-offset-3\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewlaws();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>";
						
				echo "</div>
				</form>";
			?>
			</div>	
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/quality/js/inc_laws.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?	
		}
		else{

			//echo "select * from functions where id='".$uritags[2]."'";
			$mylaws=db_row("select * from qm_laws where id='".$uritags[3]."'");
			if($mylaws['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
				
?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="modules/quality/laws"> Törvény, rendelet</a></li>
				<li><a href="/<?=$URI;?>"><?=$mylaws['name'];?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->

					<div class="block-title">
						<h2><strong><?=$mylaws['name'];?> </strong> dokumentum</h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
							if(priv("laws","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("laws","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							if(priv("laws","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("laws","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
							if(priv("laws","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}	
							?>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="col-md-6">
								<dl class="dl-horizontal">
									<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$mylaws['id']); ?></dd>
									<dt>Elnevezés:</dt><dd><? echo $mylaws['name']; ?></dd>
									<dt>Leírás:</dt><dd><? echo $mylaws['megjegyzes']; ?></dd>
									<dt>Státusz:</dt><dd><? if($mylaws['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
								</dl>	
							</div>
							<div class="col-md-6"></div>
							<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-2">
						<?
						
							
							echo"<form id=\"editlaws\" name=\"editlaws\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
							echo "<div style=\"margin-top:1px;\">";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-6 control-label\" for=\"name\">Dokumentum elnevezése:</label>
									<div class=\"col-md-6\">
										<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" value=\"".$mylaws['name']."\" />
									</div>
								</div>";
								
								echo "<div class=\"form-group\">
									<label class=\"col-md-6 control-label\" for=\"megjegyzes\">Dokumentum tartalma:</label>
									<div class=\"col-md-6\">
										<input type=\"text\" id=\"megjegyzes\" name=\"megjegyzes\" class=\"form-control\" value=\"".$mylaws['megjegyzes']."\" />																		
									</div>
								</div>";
							
								
								echo "<div class=\"form-group\" >
									<label class=\"col-md-6 control-label\" for=\"status\">Érvényes-e:</label>";
									$st="";if($mylaws['status']==1){ $st=" checked "; }
									echo "<label class=\"col-md-6 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\"".$st." ><span></span></label>
								</div>";
								
								echo "<div class=\"form-group form-actions\">
									<div class=\"col-md-6 col-md-offset-3\">
										<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditlaws(".$mylaws['id'].")\"><i class=\"fa fa-angle-right\"></i> Ment</button>
									</div>
								</div>";
									
							echo "</div>
							</form>";
						?>
						</div>

						<div class="tab-pane" id="tab-3">
								<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mylaws['id'],"Dokumentum feltöltés")."</div>";
								?> 
								<div id="filelists"></div>
								<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mylaws['id'],"Hozzászólások")."</div>";
								?>
						</div>
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>
				
					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
					
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/quality/js/inc_laws.js"></script>

			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mylaws['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$mylaws['id'];?>,'Dokumentum feltöltés','filelists');
			</script>
			<script>$(function(){ lawsDatatables.init(); });</script>	

			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
		//print_r($_SESSION['planetsys'])
?>    
<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Törvények, rendeletek</a></li>
			</ul>
			<!-- END Datatables Header -->
		
				
				
			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Törvények, rendeletek</strong> lista</h2>
				</div>
				
				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							//echo"<th>Művelet</th>";
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/quality/js/inc_laws.js"></script>
		<script>$(function(){ lawsDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>

