
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Partner minősítés kezelő
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/quality/partnerquality">Partner minősítés kezelő</a></li>
				<li><a href="/new">Új minősítés</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új minősítés</strong></h2>
				</div>
			<?
			
				$partners=db_all("select id, name from partners where status=1 order by name");
				
				echo "<form id=\"newpartnerquality\" name=\"newpartnerquality\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";
							
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"partner_id\">Partner:</label>
							<div class=\"col-md-6\">
								<select id=\"partner_id\" name=\"partner_id\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($partners);$i++){
									
									echo"<option value=\"".$partners[$i]['id']."\" >".$partners[$i]['name']."</option>";	
								}
								echo"</select>
							</div>
						</div>";
			
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"alapanyag_type\">Külső teljesítés jellege, tevékenység:</label>
							<div class=\"col-md-6\">
								<select id=\"alapanyag_type\" name=\"alapanyag_type\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
									echo"<option value=\"1\" >Alapanyag</option>";	
									echo"<option value=\"2\" >Csomagoló anyag</option>";	
									echo"<option value=\"3\" >Tisztítószer, vegyszer</option>";	
								echo"</select>
							</div>
						</div>";
			
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"ert_date\">Értékelés dátuma:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"ert_date\" name=\"ert_date\" class=\"form-control input-datepicker\" value='".date("Y-m-d",time())."' />																		
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min1\">Mennyiségi megfelelőség:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min1\" name=\"min1\" class=\"form-control\" placeholder=\"Mennyiségi megfelelőség\" />																		
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min2\">Csomagolási megfelelőség:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min2\" name=\"min2\" class=\"form-control\" placeholder=\"Csomagolási megfelelőség\" />
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min3\">Minőségi megfelelőség:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min3\" name=\"min3\" class=\"form-control\" placeholder=\"Minőségi megfelelőség\" />																		
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min4\">Száll. határidő:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min4\" name=\"min4\" class=\"form-control\" placeholder=\"Száll. határidő\" />																		
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min5\">Dokumentáció (specifikáció, termékvizsgálati jelentés):</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min5\" name=\"min5\" class=\"form-control\" placeholder=\"Dokumentáció (specifikáció, termékvizsgálati jelentés)\" />																		
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min6\">Árszinvonal:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min6\" name=\"min6\" class=\"form-control\" placeholder=\"Árszinvonal\" />																		
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min7\">Fizetési határidő:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min7\" name=\"min7\" class=\"form-control\" placeholder=\"Fizetési határidő\" />																		
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min8\">Fizetési mód:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min8\" name=\"min8\" class=\"form-control\" placeholder=\"Fizetési mód\" />
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min9\">Tanúsítvány:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min9\" name=\"min9\" class=\"form-control\" placeholder=\"Tanúsítvány\" />																		
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min10\">Nyomonkövetési rendszer:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min10\" name=\"min10\" class=\"form-control\" placeholder=\"Nyomonkövetési rendszer\" />																		
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min11\">Reklamációk száma:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min11\" name=\"min11\" class=\"form-control\" placeholder=\"Reklamációk száma\" />																		
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min12\">Beszállítói múlt:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min12\" name=\"min12\" class=\"form-control\" placeholder=\"Beszállítói múlt\" />																		
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min13\">Szállított alapanyag mennyisége:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min13\" name=\"min13\" class=\"form-control\" placeholder=\"Szállított alapanyag mennyisége\" />																		
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min14\">Összetevőkkel kapcsolatos kockázat:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min14\" name=\"min14\" class=\"form-control\" placeholder=\"Összetevőkkel kapcsolatos kockázat\" />																		
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min15\">Kiszolgálás:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min15\" name=\"min15\" class=\"form-control\" placeholder=\"Kiszolgálás\" />																		
							</div>
						</div>";
												

						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"jovahagy_type\">Jóváhagyás módja:</label>
							<div class=\"col-md-6\">
								<select id=\"jovahagy_type\" name=\"jovahagy_type\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
									echo"<option value=\"1\" >Tanúsítvány elegendő</option>";	
									echo"<option value=\"2\" >Helyszíni audit szükséges LEHET</option>";	
									echo"<option value=\"3\" >Helyszíni audit szükséges</option>";	
								echo"</select>
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"kockazat_vizsg\">Kockázat vizsgálat:</label>
							<div class=\"col-md-6\">
								<select id=\"kockazat_vizsg\" name=\"kockazat_vizsg\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
									echo"<option value=\"1\" >Alacsony kockázatú termék</option>";	
									echo"<option value=\"2\" >Helyszíni audit vizsgálandó</option>";	
									echo"<option value=\"3\" >Szükségtlen</option>";	
								echo"</select>
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"helyszini_audit\">Helyszínni audit szükséges:</label>
							<div class=\"col-md-6\">
								<select id=\"helyszini_audit\" name=\"helyszini_audit\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
									echo"<option value=\"1\" >Igen</option>";	
									echo"<option value=\"2\" >Nem</option>";	
								echo"</select>
							</div>
						</div>";

						
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"felugyelet_modja\">Felügyelet módja:</label>
							<div class=\"col-md-6\">
								<select id=\"felugyelet_modja\" name=\"felugyelet_modja\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
									echo"<option value=\"1\" >Megújító tanúsítvány éves</option>";	
									echo"<option value=\"2\" >Éves felülvizsgálat</option>";	
									echo"<option value=\"3\" >Ismételt helyszíni audit</option>";	
								echo"</select>
							</div>
						</div>";

						
						echo "<div class=\"form-group\" >
							<label class=\"col-md-6 control-label\" for=\"status\">Érvényes-e:</label>
							<label class=\"col-md-6 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span></label>
						</div>";
						
						echo "<div style=\"clear:both;height:10px;\"></div>";
						
						echo "<div class=\"form-group form-actions\">
							<div class=\"col-md-6 col-md-offset-3\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewpartnerquality();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>";
						
				echo "</div>
				</form>";
			?>
			</div>	
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/quality/js/inc_partnerquality.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?	
		}
		else{

			//echo "select * from functions where id='".$uritags[2]."'";
			$mypartnerquality=db_row("select * from partner_quality where id='".$uritags[3]."'");
			if($mypartnerquality['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
				$partners=db_all("select id, name from partners where status=1 order by name");
?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="modules/quality/partnerquality"> Minősítés</a></li>
				<li><a href="/<?=$URI;?>"><?=$mypartnerquality['id'];?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->

					<div class="block-title">
						<h2><strong><?=idchange("partners","name",$mypartnerquality['partner_id']);?> <?=$mypartnerquality['ert_date'];?>-ei</strong> Minősítése</h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
							if(priv("partnerquality","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("partnerquality","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							if(priv("partnerquality","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("partnerquality","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
							if(priv("partnerquality","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}	
							?>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="col-md-6">
								<dl class="dl-horizontal">
									<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$mypartnerquality['id']); ?></dd>
									<dt>Partner:</dt><dd><? echo idchange("partners","name",$mypartnerquality['partner_id']); ?></dd>
									<dt>Értékelés ideje:</dt><dd><? echo $mypartnerquality['ert_date']; ?></dd>
									<dt>Értékelés eredménye:</dt><dd><? echo ($mypartnerquality['szazalek']*100); ?>%</dd>
									<dt>Mennyiségi megfelelőség:</dt><dd><? echo $mypartnerquality['min1']; ?></dd>
									<dt>Csomagolási megfelelőség:</dt><dd><? echo $mypartnerquality['min2']; ?></dd>
									<dt>Minőségi megfelelőség:</dt><dd><? echo $mypartnerquality['min3']; ?></dd>
									<dt>Száll. határidő:</dt><dd><? echo $mypartnerquality['min4']; ?></dd>
									<dt>Dokumentáció (specifikáció, termékvizsgálati jelentés):</dt><dd><? echo $mypartnerquality['min5']; ?></dd>
									<dt>Árszinvonal:</dt><dd><? echo $mypartnerquality['min6']; ?></dd>
									<dt>Fizetési határidő:</dt><dd><? echo $mypartnerquality['min7']; ?></dd>
									<dt>Fizetési mód:</dt><dd><? echo $mypartnerquality['min8']; ?></dd>
									<dt>Tanúsítvány:</dt><dd><? echo $mypartnerquality['min9']; ?></dd>
									<dt>Nyomonkövetési rendszer:</dt><dd><? echo $mypartnerquality['min10']; ?></dd>
									<dt>Reklamációk száma:</dt><dd><? echo $mypartnerquality['min11']; ?></dd>
									<dt>Beszállítói múlt:</dt><dd><? echo $mypartnerquality['min12']; ?></dd>
									<dt>Szállított alapanyag mennyisége:</dt><dd><? echo $mypartnerquality['min13']; ?></dd>
									<dt>Összetevőkkel kapcsolatos kockázat:</dt><dd><? echo $mypartnerquality['min14']; ?></dd>
									<dt>Kiszolgálás:</dt><dd><? echo $mypartnerquality['min15']; ?></dd>
									<?
									echo"<dt>Jóváhagyás módja:</dt>";
									if($mypartnerquality['jovahagy_type']==1){echo"<dd>Tanúsítvány elegendő</dd>";}
									elseif($mypartnerquality['jovahagy_type']==2){echo"<dd>Helyszíni audit szükséges LEHET</dd>";}
									elseif($mypartnerquality['jovahagy_type']==3){echo"<dd>Helyszíni audit szükséges</dd>";}
										
								
									echo"<dt>Kockázat vizsgálat:</dt>";
									if($mypartnerquality['kockazat_vizsg']==1){echo"<dd>Alacsony kockázatú termék</dd>";}
									elseif($mypartnerquality['kockazat_vizsg']==2){echo"<dd>Helyszíni audit vizsgálandó</dd>";}
									elseif($mypartnerquality['kockazat_vizsg']==3){echo"<dd>Nem szükséges</dd>";}
										
								
									echo"<dt>Helyszíni audit szükséges-e:</dt>";
									if($mypartnerquality['helyszini_audit']==1){echo"<dd>igen</dd>";}
									elseif($mypartnerquality['helyszini_audit']==2){echo"<dd>Nem</dd>";}
								
									echo"<dt>Felügyelet módja:</dt>";
									if($mypartnerquality['felugyelet_modja']==1){echo"<dd>Megújító tanúsítvány éves</dd>";}
									elseif($mypartnerquality['felugyelet_modja']==2){echo"<dd>Éves felülvizsgálat</dd>";}
									elseif($mypartnerquality['felugyelet_modja']==3){echo"<dd>Ismételt helyszíni audit</dd>";}
									
									?>
									<dt>Kiértékelő:</dt><dd><? echo idchange("users","realname",$mypartnerquality['user_id']); ?></dd>
									<dt>Státusz:</dt><dd><? if($mypartnerquality['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
								</dl>	
							</div>
							<div class="col-md-6">
								
							</div>
							<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-2">
						<?
						
							
							echo"<form id=\"editpartnerquality\" name=\"editpartnerquality\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
							echo "<div style=\"margin-top:1px;\">";
							
							echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"partner_id\">Partner:</label>
							<div class=\"col-md-6\">
								<select id=\"partner_id\" name=\"partner_id\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($partners);$i++){
									$sel="";if($mypartnerquality['partner_id']==$partners[$i]['id']){$sel=" selected ";}
									echo"<option value=\"".$partners[$i]['id']."\" ".$sel.">".$partners[$i]['name']."</option>";	
								}
								echo"</select>
							</div>
						</div>";
			
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"alapanyag_type\">Külső teljesítés jellege, tevékenység:</label>
							<div class=\"col-md-6\">
								<select id=\"alapanyag_type\" name=\"alapanyag_type\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
									echo"<option value=\"1\" ".($mypartnerquality['alapanyag_type']=="1"?"selected":"")." >Alapanyag</option>";	
									echo"<option value=\"2\" ".($mypartnerquality['alapanyag_type']=="2"?"selected":"").">Csomagoló anyag</option>";	
									echo"<option value=\"3\" ".($mypartnerquality['alapanyag_type']=="3"?"selected":"").">Tisztítószer, vegyszer</option>";	
								echo"</select>
							</div>
						</div>";
			
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"ert_date\">Értékelés dátuma:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"ert_date\" name=\"ert_date\" class=\"form-control input-datepicker\" value='".$mypartnerquality['ert_date']."' />																		
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min1\">Mennyiségi megfelelőség:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min1\" name=\"min1\" class=\"form-control\" value='".$mypartnerquality['min1']."' />																		
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min2\">Csomagolási megfelelőség:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min2\" name=\"min2\" class=\"form-control\" value='".$mypartnerquality['min2']."' />
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min3\">Minőségi megfelelőség:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min3\" name=\"min3\" class=\"form-control\" value='".$mypartnerquality['min3']."' />																		
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min4\">Száll. határidő:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min4\" name=\"min4\" class=\"form-control\" value='".$mypartnerquality['min4']."'/>																		
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min5\">Dokumentáció (specifikáció, termékvizsgálati jelentés):</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min5\" name=\"min5\" class=\"form-control\" value='".$mypartnerquality['min5']."' />																		
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min6\">Árszinvonal:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min6\" name=\"min6\" class=\"form-control\" value='".$mypartnerquality['min6']."' />																		
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min7\">Fizetési határidő:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min7\" name=\"min7\" class=\"form-control\" value='".$mypartnerquality['min7']."' />																		
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min8\">Fizetési mód:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min8\" name=\"min8\" class=\"form-control\" value='".$mypartnerquality['min8']."'  />
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min9\">Tanúsítvány:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min9\" name=\"min9\" class=\"form-control\" value='".$mypartnerquality['min9']."' />																		
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min10\">Nyomonkövetési rendszer:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min10\" name=\"min10\" class=\"form-control\" value='".$mypartnerquality['min10']."'  />																		
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min11\">Reklamációk száma:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min11\" name=\"min11\" class=\"form-control\" value='".$mypartnerquality['min11']."'  />																		
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min12\">Beszállítói múlt:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min12\" name=\"min12\" class=\"form-control\" value='".$mypartnerquality['min12']."'  />																		
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min13\">Szállított alapanyag mennyisége:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min13\" name=\"min13\" class=\"form-control\" value='".$mypartnerquality['min13']."'  />																		
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min14\">Összetevőkkel kapcsolatos kockázat:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min14\" name=\"min14\" class=\"form-control\" value='".$mypartnerquality['min14']."' />																		
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min15\">Kiszolgálás:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min15\" name=\"min15\" class=\"form-control\" value='".$mypartnerquality['min15']."'  />																		
							</div>
						</div>";
												

						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"jovahagy_type\">Jóváhagyás módja:</label>
							<div class=\"col-md-6\">
								<select id=\"jovahagy_type\" name=\"jovahagy_type\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
									echo"<option value=\"1\" ".($mypartnerquality['jovahagy_type']=="1"?"selected":"").">Tanúsítvány elegendő</option>";	
									echo"<option value=\"2\" ".($mypartnerquality['jovahagy_type']=="2"?"selected":"").">Helyszíni audit szükséges LEHET</option>";	
									echo"<option value=\"3\" ".($mypartnerquality['jovahagy_type']=="3"?"selected":"").">Helyszíni audit szükséges</option>";	
								echo"</select>
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"kockazat_vizsg\">Kockázat vizsgálat:</label>
							<div class=\"col-md-6\">
								<select id=\"kockazat_vizsg\" name=\"kockazat_vizsg\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
									echo"<option value=\"1\" ".($mypartnerquality['kockazat_vizsg']=="1"?"selected":"").">Alacsony kockázatú termék</option>";	
									echo"<option value=\"2\" ".($mypartnerquality['kockazat_vizsg']=="2"?"selected":"").">Helyszíni audit vizsgálandó</option>";	
									echo"<option value=\"3\" ".($mypartnerquality['kockazat_vizsg']=="3"?"selected":"").">Szükségtlene</option>";	
								echo"</select>
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"helyszini_audit\">Helyszínni audit szükséges:</label>
							<div class=\"col-md-6\">
								<select id=\"helyszini_audit\" name=\"helyszini_audit\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
									echo"<option value=\"1\" ".($mypartnerquality['helyszini_audit']=="1"?"selected":"").">Igen</option>";	
									echo"<option value=\"2\" ".($mypartnerquality['helyszini_audit']=="2"?"selected":"").">Nem</option>";	
								echo"</select>
							</div>
						</div>";

						
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"felugyelet_modja\">Felügyelet módja:</label>
							<div class=\"col-md-6\">
								<select id=\"felugyelet_modja\" name=\"felugyelet_modja\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
									echo"<option value=\"1\" ".($mypartnerquality['felugyelet_modja']=="1"?"selected":"").">Megújító tanúsítvány éves</option>";	
									echo"<option value=\"2\" ".($mypartnerquality['felugyelet_modja']=="2"?"selected":"").">Éves felülvizsgálat</option>";	
									echo"<option value=\"3\" ".($mypartnerquality['felugyelet_modja']=="3"?"selected":"").">Ismételt helyszíni audit</option>";	
								echo"</select>
							</div>
						</div>";
								
								echo "<div class=\"form-group\" >
									<label class=\"col-md-6 control-label\" for=\"status\">Érvényes-e:</label>";
									$st="";if($mypartnerquality['status']==1){ $st=" checked "; }
									echo "<label class=\"col-md-6 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\"".$st." ><span></span></label>
								</div>";
								
								echo "<div class=\"form-group form-actions\">
									<div class=\"col-md-6 col-md-offset-3\">
										<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditpartnerquality(".$mypartnerquality['id'].")\"><i class=\"fa fa-angle-right\"></i> Ment</button>
									</div>
								</div>";
									
							echo "</div>
							</form>";
						?>
						</div>

						<div class="tab-pane" id="tab-3">
								<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mypartnerquality['id'],"Dokumentum feltöltés")."</div>";
								?> 
								<div id="filelists"></div>
								<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mypartnerquality['id'],"Hozzászólások")."</div>";
								?>
						</div>
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>
				
					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
					
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/quality/js/inc_partnerquality.js"></script>

			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mypartnerquality['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$mypartnerquality['id'];?>,'Dokumentum feltöltés','filelists');
			</script>
			<script>$(function(){ partnerqualityDatatables.init(); });</script>	

			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
		//print_r($_SESSION['planetsys'])
?>    
<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Minősítés</a></li>
			</ul>
			<!-- END Datatables Header -->
		<div class="block full">
				<div class="block-title">
					<h2><strong>Minősítés</strong> meghatározása</h2>
				</div>
				<div>
				<p>A munkalapon minden, a minőség szempontjából fontos beszállító- és szolgáltató céget feltüntetünk, láthatjuk az adott partner kiértékelési szempontjait is és elvégezzük a kiértékelést.       A cégek számától függően szükség lehet a táblázat bővítésére vagy a felesleges sorok törlésére. A kiértékelési szempontokat 1-5-ig osztályozhatjuk. A megjegyzés rovatban tüntetjük fel időrendi sorrendben a beszállító céggel kapcsolatos évközi megjegyzéseinket (minőségi kifogások, késedelmes szállítások, mennyiségi eltérések, bizonylatolások hiányossága, észlelt hibák, beszállító nem - megfelelő vevőszolgálati tevékenysége stb.). Új szállító esetén minimum 75%-ot el kell érni, illetve kitöltött beszállítói nyilatkozat megléte. Minden termékspecifikációnak meg kell lennie. Ha alkalmazható, akkor termékvizsgálati eredményt kérünk. A szállító teljesítmény értékelése 50-74%, akkor helyszíni audittal győződünk meg az alkalmasságáról. 75% felett a szállító minőség irányítási rendszer biztonságát elfogadjuk. A táblázat automatikusan színnel jelöli a kiértékelés eredményét a következők szerint:<br />
					<div style="background-color:#00FF00;width:200px;">Megfelelt - 75-100%</div>
					<div style="background-color:#FFFF00;width:200px;">Feltételesen megfelelt - 50-74%</div>
					<div style="background-color:#FFA500;width:200px;">Nem felelt meg - 0-50%</div>
				</p>
			</div>
		</div>
				
				
			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Minősítés</strong> lista</h2>
				</div>
				
				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							//echo"<th>Művelet</th>";
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/quality/js/inc_partnerquality.js"></script>
		<script>$(function(){ partnerqualityDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>

