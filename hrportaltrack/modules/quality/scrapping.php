
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Selejtezés 
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/quality/scrapping">Selejtezés kezelő</a></li>
				<li><a href="/new">Új selejtezés</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új selejetezés</strong></h2>
				</div>
			<?
			
				$mee=db_all("select id, name from mertekegyseg where status=1 order by name");
				
				echo "<form id=\"newscrapping\" name=\"newscrapping\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";
							
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"type\">Selejt típusa:</label>
							<div class=\"col-md-8\">
								<select id=\"type\" name=\"type\" class=\"form-control\" onchange=\"javascript:getscappingtype();\">";
								echo"<option value=\"0\">Kérlek válassz</option>";
								echo"<option value=\"material\">Alapanyag</option>";
								echo"<option value=\"halfproduct\">Félkész termék</option>";
								echo"<option value=\"endproduct\">Késztermék</option>";
								echo"<option value=\"device\">Eszköz</option>";
								echo"<option value=\"cleaner\">Tisztítószer</option>";
								echo"</select>
							</div>
						</div>";

			
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"selejt_date\">Selejtezés dátuma:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"selejt_date\" name=\"selejt_date\" class=\"form-control input-datepicker\" value='".date("Y-m-d",time())."' />																		
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"elem_id\">Selejtezés tárgya:</label>
								<div class=\"col-md-8\">";
								echo"<select id=\"elem_id\" name=\"elem_id\" class=\"form-control\" onchange=\"javascript:getscappingtetel();\">";
									echo"<option value=\"0\">Kérlek válassz</option>";
								echo"</select>";
							echo"</div>";
						echo"</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"elem_tetel_id\">Selejt tételjelölése:</label>
							<div class=\"col-md-8\">";
								echo"<select id=\"elem_tetel_id\" name=\"elem_tetel_id\" class=\"form-control\" onchange=\"javascript:getscappingme();\">";
									echo"<option value=\"0\">Kérlek válassz</option>";
								echo"</select>";
							echo"<input type=\"hidden\" id=\"eetetel_id\" name=\"eetetel_id\" >";
							echo"</div>";
						echo"</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"scrapname\">Elem neve, ha nincs tétel:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"scrapname\" name=\"scrapname\" class=\"form-control\" placeholder=\"Speciális elemnév\" />																		
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"me\">Mennyisége:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"me\" name=\"me\" class=\"form-control\" placeholder=\"Mennyisége\" />																		
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"mee\">Mennyiségi egység:</label>
							<div class=\"col-md-8\">
								<select id=\"mee\" name=\"mee\" class=\"form-control\" >";
									echo"<option value=\"0\">Kérlek válassz</option>";
											
									for($i=0;$i<count($mee);$i++){
										echo"<option value=\"".$mee[$i]['id']."\">".$mee[$i]['name']."</option>";
									}
											
								echo"</select>
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"selejt_comment\">Selejtezés oka:</label>
							<div class=\"col-md-8\">
								<textarea id=\"selejt_comment\" name=\"selejt_comment\" rows=\"6\" class=\"form-control\" ></textarea>
							</div>
						</div>";

						
						echo "<div style=\"clear:both;height:10px;\"></div>";
						
						echo "<div class=\"form-group form-actions\">
							<div class=\"col-md-6 col-md-offset-3\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewscrapping();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>";
						
				echo "</div>
				</form>";
			?>
			</div>	
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/quality/js/inc_scrapping.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?	
		}
		else{

			//echo "select * from functions where id='".$uritags[2]."'";
			$myscrapping=db_row("select * from scrapping where id='".$uritags[3]."'");
			
			
			$a=db_row("select * from alapanyag where id='".$myscrapping['elem_tetel_id']."'");
			if($myscrapping['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
				naplo("info","user","Selejtezés betöltése","quality","scrapping",$uritags[3],"");	
?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="modules/quality/scrapping"> Selejtezés</a></li>
				<li><a href="/<?=$URI;?>"><?=$myscrapping['id'];?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->

					<div class="block-title">
						<h2><strong><?=idchange("scrapping","elem_name",$myscrapping['id']);?> <?=substr($myscrapping['selejt_date'],0,10);?>-ei</strong> selejtezése</h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
							if(priv("scrapping","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("scrapping","aprove")){echo"<li ><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"ellenőriz\"><i class=\"fa fa-check\"></i></a></li>";}
							//if(priv("scrapping","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							if(priv("scrapping","print")){echo"<li><a href=\"#tab-99\" data-toggle=\"tooltip\" title=\"Történet\" onclick=\"javascript:showstory('".$myscrapping['id']."');\"><i class=\"gi gi-history\"></i></a></li>";}			
							if(priv("scrapping","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("scrapping","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
							if(priv("scrapping","print")){echo"<li onclick=\"javascript:getprintscreen('".$_SESSION['planetsys']['actpage']['modulname']."','scrappingprint','".$myscrapping['id']."');\" ><a href=\"#modal-regular-print\" data-toggle=\"modal\" title=\"Selejtezési bizonylat\"><i class=\"fa fa-print\" ></i></a></li>";}
							if(priv("scrapping","print")){echo"<li onclick=\"javascript:getprintscreen('".$_SESSION['planetsys']['actpage']['modulname']."','fullscrappingprint','".$myscrapping['id']."');\" ><a href=\"#modal-regular-print\" data-toggle=\"modal\" title=\"Selejtezési bizonylat\"><i class=\"hi hi-print\" ></i></a></li>";}
							?>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="col-md-6">
								<dl class="dl-horizontal">
									<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$myscrapping['id']); ?></dd>
									<dt>Selejtezett termék típus:</dt><dd><? echo $myscrapping['elem_name']; ?></dd>
									<dt>Selejtezett tétel:</dt><dd><? echo $a['lot']." - ".$a['mmi'];?></dd>
									<dt>Selejtezett név, ha nincs tétel:</dt><dd><? echo $myscrapping['scarpname'];?></dd>
									<dt>Selejt mennyisége:</dt><dd><? echo huf($myscrapping['me'])." ".idchange("mertekegyseg","name",$myscrapping['mee']); ?></dd>
									<dt>Selejt értéke:</dt><dd><? echo huf($myscrapping['price_all']); ?> Ft</dd>
									<dt>Selejtezés ideje:</dt><dd><? echo substr($myscrapping['selejt_date'],0,10); ?></dd>
									<dt>Selejtezés oka:</dt><dd><? echo $myscrapping['selejt_comment']; ?></dd>
									<dt>Selejt keletkezése:</dt><dd><? echo idchange("gw_workers","name",$myscrapping['post_id']); ?></dd>
									<dt>Selejtezést végezte:</dt><dd><? echo idchange("users","realname",$myscrapping['user_id']); ?></dd>
									
									<? 
									if($myscrapping['aproved']=="0"){
										echo "<dt>Jóváhagyva:</dt><dd>Nincs jóváhagyva</dd>"; 
									}
									else{
										echo "<dt>Jóváhagyta:</dt><dd>".idchange("users","realname",$myscrapping['aproved_user_id'])." ".$myscrapping['aproved_date']."-én</dd>"; 
										if($myscrapping['aproved_comment']!=""){
											echo "<dt>Jóváhagyási megjegyzés:</dt><dd>".$myscrapping['aproved_comment']."</dd>"; 
										}
									}
									echo"<dt>Utó, helyesbítő intézkedés:</dt><dd><a href=\"/modules/quality/inapt/".$myscrapping['inapt_id']."\">".idchange("qm_inapt","name",$myscrapping['inapt_id'])."</a></dd>";	
									//if($myscrapping['utointezkedes']=="0"){
										
									//} 
									/*
									else{
										echo"<dt>Utóintézkedés szükséges e:</dt><dd>utólagos intézkedésre szükség van</dd>";	
										echo"<dt>Utóintézkedés indoka:</dt><dd>".$myscrapping['utointezkedes_comment']."</dd>";	
										echo"<dt>Utóintézkedés felelőse:</dt><dd>".idchange("users","realname",$myscrapping['ut_user_id'])."</dd>";	
										echo"<dt>Utóintézkedés visszaellőrzésének ideje:</dt><dd>".$myscrapping['utcheck_date']."</dd>";	
										if($myscrapping['utcheck']==0){
											echo"<dt>Utóintézkedés visszaellenőrzés:</dt><dd>Még nem történt meg</dd>";	
										}
										else{
											echo"<dt>Utóintézkedés visszaellenőrzés:</dt><dd>Megtörtént</dd>";	
											echo"<dt>Utóintézkedés visszaellenőrzés leírása:</dt><dd>".$myscrapping['utcheck_comment']."</dd>";	
										}	
									
									}
									*/ 
										//echo $myscrapping['utointezkedes']; 
									?>
									</dd>
								</dl>	
							</div>
							<div class="col-md-6">
								
							</div>
							<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-2">
							<?
							$munkahely=db_all("select id, name from gw_posts where status=1 order by name");
								echo "<form id=\"editscrapping\" name=\"editscrapping\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";
							
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"aproved\">Jóváhagy:</label>
							<div class=\"col-md-8\">
								<select id=\"aproved\" name=\"aproved\" class=\"form-control\" >";
								echo"<option value=\"0\">Kérlek válassz</option>";
								echo"<option value=\"1\" ".($myscrapping['aproved']=="1"?"selected":"").">Jóváhagy</option>";
								echo"<option value=\"2\" ".($myscrapping['aproved']=="2"?"selected":"").">Ügyvezetői döntés</option>";
								echo"</select>
							</div>
						</div>";

					echo "<div class=\"form-group\">
									<label class=\"col-md-4 control-label\" for=\"post_id\">Munkahely:</label>
									<div class=\"col-md-8\">
										<select id=\"post_id\" name=\"post_id\" class=\"form-control\" >
										<option value=\"0\">Kérlek válassz</option>";
										for($i=0;$i<count($munkahely);$i++){
											$sel="";if($munkahely[$i]['id']==$myeszkozok['post_id']){$sel=" selected ";}
											echo"<option value=\"".$munkahely[$i]['id']."\" ".$sel.">".$munkahely[$i]['name']."</option>";
										}
										echo"</select>
									</div>
								</div>";

						if($myscrapping['price']==0 && ($myscrapping['type']=="halfproduct" || $myscrapping['type']=="endproduct")){
							//echo "alma";
							$mennyik=db_all("select * from pt_alapanyaghanyad where parent_id='".$myscrapping['elem_id']."' and status=1");
							$elemar=0;
							for($i=0;$i<count($mennyik);$i++){
								$kiloar=db_row("select * from alapag_tipus where id='".$mennyik[$i]['agt_id']."'");
								//echo $kiloar['name']
								$elemar+=round($kiloar['baseprice']*$mennyik[$i]['egalizalt']);	
							}
							$myscrapping['price']=$elemar;
							$myscrapping['price_all']=$elemar*$myscrapping['me'];
						}
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"price\">Egységár:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"price\" name=\"price\" class=\"form-control\" value=\"".$myscrapping['price']."\" />																		
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"price_all\">Összes ár:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"price_all\" name=\"price_all\" class=\"form-control\" value=\"".$myscrapping['price_all']."\" />																		
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"aproved_comment\">Jováhagyási megjegyzés:</label>
							<div class=\"col-md-8\">
								<textarea id=\"aproved_comment\" name=\"aproved_comment\" rows=\"6\" class=\"form-control\" >".$myscrapping['aproved_comment']."</textarea>
							</div>
						</div>";
						/*
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"utointezkedes\">Helyesbítő intézkedésre:</label>
							<div class=\"col-md-8\">
								<select id=\"utointezkedes\" name=\"utointezkedes\" class=\"form-control\" >";
								echo"<option value=\"0\" ".($myscrapping['utointezkedes']=="0"?"selected":"").">Nincs szükség rá</option>";
								echo"<option value=\"1\" ".($myscrapping['utointezkedes']=="1"?"selected":"").">Szükség van</option>";
								echo"</select>
							</div>
						</div>";

						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"ut_user_id\">Helyesbítő intézkedés felelőse:</label>
							<div class=\"col-md-8\">
								<select id=\"ut_user_id\" name=\"ut_user_id\" class=\"form-control\" >";
									echo"<option value=\"0\">Kérlek válassz</option>";
									$users=db_all("select id,realname from users where status=1 order by realname asc");
									for($i=0;$i<count($users);$i++){
										echo"<option value=\"".$users[$i]['id']."\" ".($myscrapping['ut_user_id']==$users[$i]['id'] ? "selected":"").">".$users[$i]['realname']."</option>";
									}
											
								echo"</select>
							</div>
						</div>";


						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"utointezkedes_comment\">Helyesbítő intézkedés leírása:</label>
							<div class=\"col-md-8\">
								<textarea id=\"utointezkedes_comment\" name=\"utointezkedes_comment\" rows=\"6\" class=\"form-control\" >".$myscrapping['utointezkedes_comment']."</textarea>
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"utcheck_date\">Helyesbítő intézkedés ellenőrzése:</label>
							<div class=\"col-md-8\">
								<input type=\"text\" id=\"utcheck_date\" name=\"utcheck_date\" class=\"form-control input-datepicker\" value='".$myscrapping['utcheck_date']."' />
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"utcheck\">Helyesbítő intézkedés:</label>
							<div class=\"col-md-8\">
								<select id=\"utcheck\" name=\"utcheck\" class=\"form-control\" >";
								echo"<option value=\"0\" ".($myscrapping['utcheck']=="0"?"selected":"").">nem készült el</option>";
								echo"<option value=\"1\" ".($myscrapping['utcheck']=="1"?"selected":"").">elkészült</option>";
								echo"</select>
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-4 control-label\" for=\"utcheck_comment\">Helyesbítő ellenőrzése:</label>
							<div class=\"col-md-8\">
								<textarea id=\"utcheck_comment\" name=\"utcheck_comment\" rows=\"6\" class=\"form-control\" >".$myscrapping['utcheck_comment']."</textarea>
							</div>
						</div>";						
						
						echo "<div style=\"clear:both;height:10px;\"></div>";
						*/
						echo "<div class=\"form-group form-actions\">
							<div class=\"col-md-6 col-md-offset-3\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditscrapping(".$myscrapping['id'].");\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>";
						
				echo "</div>
				</form>";
							?>
						</div>

						<div class="tab-pane" id="tab-3">
								<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$myscrapping['id'],"Dokumentum feltöltés")."</div>";
								?> 
								<div id="filelists"></div>
								<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$myscrapping['id'],"Hozzászólások")."</div>";
								?>
						</div>
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>
						<div class="tab-pane" id="tab-99">
							<div id="storydiv"></div>
						</div>
				
					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
					
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/quality/js/inc_scrapping.js"></script>

			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$myscrapping['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$myscrapping['id'];?>,'Dokumentum feltöltés','filelists');
			</script>
			<script>$(function(){ scrappingDatatables.init(); });</script>	

			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
		//print_r($_SESSION['planetsys'])
		naplo("info","user","Selejtező betöltése","quality","scrapping","","");	
?>    
<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Selejtezés</a></li>
			</ul>

				
				
			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Selejtezés</strong> lista</h2>
				</div>
				
				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							//echo"<th>Művelet</th>";
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/quality/js/inc_scrapping.js"></script>
		<script>$(function(){ scrappingDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>

