
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Dolgozói minősítés kezelő
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/quality/workerquality">Dolgozói minősítés kezelő</a></li>
				<li><a href="/new">Új minősítés</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új minősítés</strong></h2>
				</div>
			<?
			
				$workers=db_all("select id, name from gw_workers where status=1 order by name");
				
				echo "<form id=\"newworkerquality\" name=\"newworkerquality\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";
							
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"worker_id\">Dolgozó:</label>
							<div class=\"col-md-6\">
								<select id=\"worker_id\" name=\"worker_id\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($workers);$i++){
									
									echo"<option value=\"".$workers[$i]['id']."\" >".$workers[$i]['name']."</option>";	
								}
								echo"</select>
							</div>
						</div>";

			
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"ert_date\">Értékelés dátuma:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"ert_date\" name=\"ert_date\" class=\"form-control input-datepicker\" value='".date("Y-m-d",time())."' />																		
							</div>
						</div>";
						
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min1\">Norma:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min1\" name=\"min1\" class=\"form-control\" placeholder=\"Norma\" />
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min2\">Minőség:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min2\" name=\"min2\" class=\"form-control\" placeholder=\"Minőség\" />
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min3\">Lojalitás (2x):</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min3\" name=\"min3\" class=\"form-control\" placeholder=\"Lojalitás (2x)\" />																		
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min4\">Higiénia (2x):</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min4\" name=\"min4\" class=\"form-control\" placeholder=\"Higiénia (2x):\" />	
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min5\">Moralizáció:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min5\" name=\"min5\" class=\"form-control\" placeholder=\"Moralizáció:\" />																		
							</div>
						</div>";
						
						echo "<div class=\"form-group\" >
							<label class=\"col-md-6 control-label\" for=\"status\">Érvényes-e:</label>
							<label class=\"col-md-6 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span></label>
						</div>";
						
						echo "<div style=\"clear:both;height:10px;\"></div>";
						
						echo "<div class=\"form-group form-actions\">
							<div class=\"col-md-6 col-md-offset-3\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewworkerquality();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>";
						
				echo "</div>
				</form>";
			?>
			</div>	
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/quality/js/inc_workerquality.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?	
		}
		else{

			//echo "select * from functions where id='".$uritags[2]."'";
			$myworkerquality=db_row("select * from gw_worker_quality where id='".$uritags[3]."'");
			if($myworkerquality['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
				$workers=db_all("select id, name from gw_workers where status=1 order by name");
?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="modules/quality/workerquality"> Minősítés</a></li>
				<li><a href="/<?=$URI;?>"><?=$myworkerquality['id'];?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->

					<div class="block-title">
						<h2><strong><?=idchange("gw_workers","name",$myworkerquality['worker_id']);?> <?=$myworkerquality['ert_date'];?>-ei</strong> Minősítése</h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
							if(priv("workerquality","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("workerquality","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							if(priv("workerquality","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("workerquality","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
							if(priv("workerquality","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}	
							?>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="col-md-6">
								<dl class="dl-horizontal">
									<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$myworkerquality['id']); ?></dd>
									<dt>Dolgozó:</dt><dd><? echo idchange("gw_workers","name",$myworkerquality['worker_id']); ?></dd>
									<dt>Értékelés ideje:</dt><dd><? echo $myworkerquality['ert_date']; ?></dd>
									<dt>Értékelés eredménye:</dt><dd><? echo ($myworkerquality['szumma']); ?></dd>
									<dt>Norma:</dt><dd><? echo $myworkerquality['min1']; ?></dd>
									<dt>Minőség:</dt><dd><? echo $myworkerquality['min2']; ?></dd>
									<dt>Lojalitás:</dt><dd><? echo $myworkerquality['min3']; ?></dd>
									<dt>Higiéna:</dt><dd><? echo $myworkerquality['min4']; ?></dd>
									<dt>Moralizácó:</dt><dd><? echo $myworkerquality['min5']; ?></dd>
									<dt>Kiértékelő:</dt><dd><? echo idchange("users","realname",$myworkerquality['user_id']); ?></dd>
									<dt>Státusz:</dt><dd><? if($myworkerquality['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
								</dl>	
							</div>
							<div class="col-md-6">
								
							</div>
							<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-2">
						<?
						
							
							echo"<form id=\"editworkerquality\" name=\"editworkerquality\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
							echo "<div style=\"margin-top:1px;\">";
							
							echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"worker_id\">Dolgozó:</label>
							<div class=\"col-md-6\">
								<select id=\"worker_id\" name=\"worker_id\" class=\"form-control\" >
								<option value=\"0\">Kérlek válassz</option>";
								for($i=0;$i<count($workers);$i++){
									$sel="";if($myworkerquality['worker_id']==$workers[$i]['id']){$sel=" selected ";}
									echo"<option value=\"".$workers[$i]['id']."\" ".$sel.">".$workers[$i]['name']."</option>";	
								}
								echo"</select>
							</div>
						</div>";
			
					
			
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"ert_date\">Értékelés dátuma:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"ert_date\" name=\"ert_date\" class=\"form-control input-datepicker\" value='".$myworkerquality['ert_date']."' />																		
							</div>
						</div>";
						
					echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min1\">Norma:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min1\" name=\"min1\" class=\"form-control\" value='".$myworkerquality['min1']."' />
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min2\">Minőség:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min2\" name=\"min2\" class=\"form-control\" value='".$myworkerquality['min2']."' />
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min3\">Lojalitás (2x):</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min3\" name=\"min3\" class=\"form-control\" value='".$myworkerquality['min3']."' />																		
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min4\">Higiénia (2x):</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min4\" name=\"min4\" class=\"form-control\" value='".$myworkerquality['min4']."' />	
							</div>
						</div>";
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"min5\">Moralizáció:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"min5\" name=\"min5\" class=\"form-control\" value='".$myworkerquality['min5']."'/>																		
							</div>
						</div>";
								echo "<div class=\"form-group\" >
									<label class=\"col-md-6 control-label\" for=\"status\">Érvényes-e:</label>";
									$st="";if($myworkerquality['status']==1){ $st=" checked "; }
									echo "<label class=\"col-md-6 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\"".$st." ><span></span></label>
								</div>";
								
								echo "<div class=\"form-group form-actions\">
									<div class=\"col-md-6 col-md-offset-3\">
										<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditworkerquality(".$myworkerquality['id'].")\"><i class=\"fa fa-angle-right\"></i> Ment</button>
									</div>
								</div>";
									
							echo "</div>
							</form>";
						?>
						</div>

						<div class="tab-pane" id="tab-3">
								<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$myworkerquality['id'],"Dokumentum feltöltés")."</div>";
								?> 
								<div id="filelists"></div>
								<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$myworkerquality['id'],"Hozzászólások")."</div>";
								?>
						</div>
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>
				
					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
					
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/quality/js/inc_workerquality.js"></script>

			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$myworkerquality['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$myworkerquality['id'];?>,'Dokumentum feltöltés','filelists');
			</script>
			<script>$(function(){ workerqualityDatatables.init(); });</script>	

			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
		//print_r($_SESSION['planetsys'])
?>    
<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Minősítés</a></li>
			</ul>
			<!-- END Datatables Header -->
		<div class="block full">
				<div class="block-title">
					<h2><strong>Minősítés</strong> meghatározása</h2>
				</div>
				<div>
				<p>
					<div style="background-color:#00FF00;padding:3px;">31-35 pont között<br />Béremelés jár, amennyiben minimum 3 hónapig megszakítás nélkül folyamatos<br />Oktatáson részt vett, képzést visszaellenőrző papírja minimum 90%<br />Nincs visszáru és minden maradéktalanul le van szállítva</div>
					<div style="background-color:#FFFF00;padding:3px;">28-30 pont között<br />Teljesítménye jó, kis fejlődés szükséges a még jobbhoz</div>
					<div style="background-color:#FFA500;padding:3px;">24-27 pont között<br />Elbeszélgetés szükséges - döntés, hogy ki mit szeretne a másiktól</div>
				</p>
			</div>
		</div>
				
				
			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Minősítés</strong> lista</h2>
				</div>
				
				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							//echo"<th>Művelet</th>";
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/quality/js/inc_workerquality.js"></script>
		<script>$(function(){ workerqualityDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>

