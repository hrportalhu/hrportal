<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors','Off');
header("Cache-control: private");
header('Content-Type: text/plain;charset=utf-8');
include_once dirname(__FILE__).("/../../inc/sys.conf");
include_once dirname(__FILE__).("/../../inc/_functions.php");

$anyagok=db_all("select * from orders_ag where order_id='".$_REQUEST['elemid']."' and post_id='".$_REQUEST['post_id']."' group by agt_id");	

for($a=0;$a<count($anyagok);$a++){

	$alapanyag=db_row("select * from alapag_tipus where id='".$anyagok[$a]['agt_id']."'");
	$sume=db_one("select sum(agt_sume) from orders_ag where agt_id='".$anyagok[$a]['agt_id']."' and order_id='".$_REQUEST['elemid']."' and post_id='".$_REQUEST['post_id']."'");
	$items=db_all("SELECT id,units,mmi,lot FROM alapanyag WHERE agt_id = '".$anyagok[$a]['agt_id']."' ORDER BY mmi ASC");
	
	if(count($items)==0){
		$items=db_row("SELECT ag.id,ag.units,ag.mmi,ag.lot,agt.name AS tname,stor.name AS sname FROM alapanyag ag LEFT JOIN alapag_tipus agt ON (ag.agt_id = agt.id) LEFT JOIN stores stor ON (ag.store_id = stor.id) WHERE agt_id = '".$anyagok[$a]['agt_id']."' ORDER BY mmi ASC LIMIT 1");
		if($items['sname']!=''){
			$statuscode=1;
			$errmsg[]=eeea('error',$items['tname']." alapanyag nincs ebben a raktárban, de találtam itt: ".$items['sname']);
			$items=NULL;
		}else{
			$statuscode=2;
			$errmsg[]=eeea('error','Nincs ilyen termék egyik raktárban sem!');
		}
	}
	$amount=$sume;
	if($_SESSION['planetsys']['ag_issue']['nr_records']=='')$_SESSION['planetsys']['ag_issue']['nr_records']=0;


	for($i=0;$i<$_SESSION['planetsys']['ag_issue']['nr_records'];$i++){
	    if($_SESSION['planetsys']['ag_issue'][$i]['tid']==$anyagok[$a]['agt_id']){ // fogtunk egy duplikaciot
		$amount+=$_SESSION['planetsys']['ag_issue'][$i]['units']; // hozzaadjuk az uj ertekhez az eddigieket
		$_SESSION['planetsys']['ag_issue'][$i]=NULL; // ...es kilojuk a regit
	    }
	}


	$sum=0;
	foreach($items as $k => $v){
		$sum+=$v['units'];
	}
	if($amount>$sum){
		$statuscode=5;
		$errmsg[]=eeea('error',"A maximum kiadható mennyiség ".$sum." !");
	}else{

		foreach($items as $k => $v){
			if($v['units']>$amount){ // tobb all rendelkezesre, mint amennyi kell
				$_SESSION['planetsys']['ag_issue'][$_SESSION['planetsys']['ag_issue']['nr_records']]['tid']=$anyagok[$a]['agt_id'];
				$_SESSION['planetsys']['ag_issue'][$_SESSION['planetsys']['ag_issue']['nr_records']]['id']=$v['id'];
				$_SESSION['planetsys']['ag_issue'][$_SESSION['planetsys']['ag_issue']['nr_records']]['lotnum']=$v['lot'];
				$_SESSION['planetsys']['ag_issue'][$_SESSION['planetsys']['ag_issue']['nr_records']]['mmi']=$v['mmi'];
				$_SESSION['planetsys']['ag_issue'][$_SESSION['planetsys']['ag_issue']['nr_records']]['units']=$amount;
				$_SESSION['planetsys']['ag_issue'][$_SESSION['planetsys']['ag_issue']['nr_records']]['rdate']=date("Y-m-d H:i:s",time());
				$_SESSION['planetsys']['ag_issue'][$_SESSION['planetsys']['ag_issue']['nr_records']]['munkahely']=$_REQUEST['post_id'];
				$_SESSION['planetsys']['ag_issue']['nr_records']++;
				break;
			}else{ // Nincs eleg unit, ezt majd leszanaljuk es a foreach miatt megyunk a kovetkezore. Nem kene kulon kezelni de igy erthetobb
				$_SESSION['planetsys']['ag_issue'][$_SESSION['planetsys']['ag_issue']['nr_records']]['tid']=$anyagok[$a]['agt_id'];
				$_SESSION['planetsys']['ag_issue'][$_SESSION['planetsys']['ag_issue']['nr_records']]['id']=$v['id'];
				$_SESSION['planetsys']['ag_issue'][$_SESSION['planetsys']['ag_issue']['nr_records']]['lotnum']=$v['lot'];
				$_SESSION['planetsys']['ag_issue'][$_SESSION['planetsys']['ag_issue']['nr_records']]['mmi']=$v['mmi'];
				$_SESSION['planetsys']['ag_issue'][$_SESSION['planetsys']['ag_issue']['nr_records']]['units']=$v['units'];
				$_SESSION['planetsys']['ag_issue'][$_SESSION['planetsys']['ag_issue']['nr_records']]['rdate']=date("Y-m-d H:i:s",time());
				$_SESSION['planetsys']['ag_issue'][$_SESSION['planetsys']['ag_issue']['nr_records']]['munkahely']=$_REQUEST['post_id'];
				$amount-=$v['units'];
				$_SESSION['planetsys']['ag_issue']['nr_records']++;
				if($amount==0){ break; }
			}
		}
	}
}	
?>	
