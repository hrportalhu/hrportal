<?php
session_start();
header("Cache-control: private");
header('Content-Type: text/plain;charset=utf-8');
include_once dirname(__FILE__).("/../../inc/sys.conf");
include_once dirname(__FILE__).("/../../inc/_functions.php");

$screearray=explode(",",$_SESSION['planetsys']['actpage']['screenlist']);
//Tábla, mező olvasási definíciók
	$selee="";
	$joiele=" from ".$_SESSION['planetsys']['actpage']['scrtable']." ".$_SESSION['planetsys']['actpage']['scrtable_shortname']." ";
	for($i=0;$i<count($screearray);$i++){
		if($_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['connected']=="1"){
			$selee.=" ".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['con_table_shortname'].".".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['con_table_fields'][1]." as ".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['name'].",";
			$joiele.=" LEFT JOIN ".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['con_table']." ".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['con_table_shortname']." on (".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['name']."=".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['con_table_shortname'].".".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['con_table_fields'][0]."  )";
		}
		else{
			if($_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['name']!=""){
				$selee.=" ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['name']." as ".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['name'].",";		
			}
		}
	}
	$selee=substr($selee,0,-1);
//Limit definíció
	$sLimit = "";
	if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){$sLimit = "LIMIT ".mysql_real_escape_string( $_GET['iDisplayStart'] ).", ".mysql_real_escape_string( $_GET['iDisplayLength'] );}

//Rendezés
	$sOrder = "";
	if ( isset( $_GET['iSortCol_0'] ) ){
		$sOrder = "ORDER BY  ";
		if($_SESSION['planetsys']['actpage']['elements'][$screearray[$_GET['iSortCol_0']]]['connected']=="1"){
			$sOrder.=" ".$_SESSION['planetsys']['actpage']['elements'][$screearray[$_GET['iSortCol_0']]]['con_table_shortname'].".".$_SESSION['planetsys']['actpage']['elements'][$screearray[$_GET['iSortCol_0']]]['con_table_fields'][0]."";	
		}
		else{
			$sOrder.=" ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".".$screearray[$_GET['iSortCol_0']]."";	
		}
		if($_GET['sSortDir_0']=="asc"){$sOrder.=" asc ";}else{$sOrder.=" desc ";}
	}
//keresés
	$sWhere =  " ";
	if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
		$sWhere.= " and ( ";
		for($i=0;$i<count($screearray);$i++){
			if($_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['connected']=="1"){
				$sWhere.=" ".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['con_table_shortname'].".".$_SESSION['planetsys']['actpage']['elements'][$screearray[$i]]['con_table_fields'][1]." LIKE  '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
			}
			else{
				if($screearray[$i]!=""){
					$sWhere.=" ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".".$screearray[$i]." LIKE  '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";
				}
			}
		}
		$sWhere=substr($sWhere,0,-4);
		$sWhere.= ")";
	}
	$sWhere2="";
	if($_SESSION['planetsys']['user']['superadmin']!='1'){
		$sWhere2= " and (".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".owner='".$_SESSION['planetsys']['user_id']."' or ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".id='".$_SESSION['planetsys']['customer_id']."') ";
	}
	$sWhere3="";
	if($_REQUEST['partner_id']!=0){
		$sWhere3.=" and (".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".partner_id='".$_REQUEST['partner_id']."' ) ";
	}

	$elems=db_all("SELECT ".$selee." ".$joiele." where ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".id>0 ".$sWhere2." ".$sWhere3." ".$sWhere." ".$cusfilter."  ".$sOrder." ".$sLimit."");
	
	//echo "SELECT ".$selee." ".$joiele." where ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".id>0 ".$sWhere2." ".$sWhere3." ".$sWhere." ".$cusfilter."  ".$sOrder." ".$sLimit."";
	
	$elemdb=db_one("SELECT count(id) from ".$_SESSION['planetsys']['actpage']['scrtable']." ".$_SESSION['planetsys']['actpage']['scrtable_shortname']." where ".$_SESSION['planetsys']['actpage']['scrtable_shortname'].".id>0   ".$sOrder." ");
	$iFilteredTotal =  count($elems);
	$iTotal = db_one("SELECT COUNT(id) FROM  ".$_SESSION['planetsys']['actpage']['scrtable']." WHERE id>0 ");
	$output = array("sEcho" => intval($_GET['sEcho']),"iTotalRecords" => $iTotal,"iTotalDisplayRecords" =>  $elemdb,"aaData" => array());

	for($k=0;$k<count($elems);$k++){
		$row = array();
		for($z=0;$z<count($screearray);$z++){
			if($screearray[$z]=="status"){if($elems[$k][$screearray[$z]]==1){$elems[$k][$screearray[$z]]="Aktiv";}else{$elems[$k][$screearray[$z]]="Inaktív";}}
			if($screearray[$z]=="superadmin"){if($elems[$k][$screearray[$z]]==1){$elems[$k][$screearray[$z]]="Igen";}else{$elems[$k][$screearray[$z]]="Nem";}}
			if($screearray[$z]=="kiszolg_szint"){$elems[$k][$screearray[$z]]=round(($elems[$k][$screearray[$z]]*100),2)." %";}
			if($screearray[$z]=="megjegyzes"){if($elems[$k][$screearray[$z]]!=""){$elems[$k][$screearray[$z]]=substr($elems[$k][$screearray[$z]],0,40);	}}
			$row[] = $elems[$k][$screearray[$z]];
		}
		$output['aaData'][] = $row;
	}
	$_SESSION['planetsys']['exportdata'] = $output ;
echo json_encode( $output );
mysql_close($connid);	
?>

