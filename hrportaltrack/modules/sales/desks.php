<?
    //$screen=getscreenelems('sales','desks');
?>
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Értékesítési hely kezelő
            </h1>
        </div>
    </div>
<?

		if(isset($uritags[3]) && $uritags[3]!=""){
			if($uritags[3]=="new"){
				?>
				<!-- Ez egy elem  -->		
				<ul class="breadcrumb breadcrumb-top">
					<li><a href="/">Rendszer</a></li>
					<li><a href="/moduls/sales/desks">Értékesítési hely kezelő</a></li>
					<li><a href="/new">Új értékesítési hely felvitele</a></li>
				</ul>
				<div class="block full">
				<?
				
					echo"<form id=\"newdesks\" name=\"newdesks\"  method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
					echo "<div style=\"margin-top:1px;\">
							
						<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"name\">Elnevezés:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" placeholder=\"Értékesítési hely elnevezése\"  />																		
							</div>
						</div>
						<div class=\"form-group\" >";
							 echo"<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span><br />Státusz</label>";
						echo"</div>
						<div style=\"clear:both;height:10px;\"></div>
						<div class=\"form-group form-actions\">
							<div class=\"col-md-9 col-md-offset-3\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewdesks();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>
					</div>
					</form>";
				?>
					</div>	
				</div>	
				<?php include 'inc/page_footer.php'; ?>
				<?php include 'inc/template_scripts.php'; ?>
				<script src="modules/sales/js/inc_desks.js"></script>
				<?php include 'inc/template_end.php'; ?>	
				<?	
			}
			else{
				
				//echo "select * from functions where id='".$uritags[2]."'";
				$mydesks=db_row("select * from desks where id='".$uritags[3]."'");
				if($mydesks['id']==''){
					die("Helytelen hozzáférés!");
				}
				else{
	?>
				<!-- Ez egy elem  -->		
				<ul class="breadcrumb breadcrumb-top">
					<li><a href="/">Rendszer</a></li>
					<li><a href="moduls/sales/desks">Értékesítési helyek</a></li>
					<li><a href="/<?=$URI;?>"><?=$mydesks['name'];?></a></li>
				</ul>
				<div class="block full">
								   
						<!-- Block Tabs Title -->

						<div class="block-title">
							<h2><strong><?=$mydesks['name'];?></strong> Értékesítési hely oldala</h2>
							<ul class="nav nav-tabs" data-toggle="tabs">
								<?
								if(priv("desks","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
								if(priv("desks","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
								if(priv("desks","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
								if(priv("desks","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
								if(priv("desks","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}	
								?>
							</ul>
						</div>
						<!-- END Block Tabs Title -->

						<!-- Tabs Content -->
						<div class="tab-content">

							<div class="tab-pane active" id="tab-1">
								<div class="col-md-6">
									<dl class="dl-horizontal">
										<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$mydesks['id']); ?></dd>
										<dt>Elnevezés:</dt><dd><? echo $mydesks['name']; ?></dd>
										<dt>Érvényes?:</dt><dd><? if($mydesks['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
									</dl>	
								</div>
								<div style="clear:both;height:1px;"></div>

							</div>
							<div class="tab-pane" id="tab-2">
								<?
				
								echo"<form id=\"editdesks\" name=\"editdesks\"  method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
								echo "<div style=\"margin-top:1px;\">
										
									<div class=\"form-group\">
										<label class=\"col-md-3 control-label\" for=\"name\">Elnevezés:</label>
										<div class=\"col-md-9\">
											<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" placeholder=\"Értékesítési hely elnevezése\" value=\"".$mydesks['name']."\" />																		
										</div>
									</div>
									<div class=\"form-group\" >";
										 echo"<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span><br />Státusz</label>";
									echo"</div>
									<div style=\"clear:both;height:10px;\"></div>
									<div class=\"form-group form-actions\">
										<div class=\"col-md-9 col-md-offset-3\">
											<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditdesks(".$mydesks['id'].");\"><i class=\"fa fa-angle-right\"></i> Ment</button>
										</div>
									</div>
								</div>
								</form>";
								?>
							</div>
							<div class="tab-pane" id="tab-3">
									<?
										echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mydesks['id'],"Dokumentum feltöltés")."</div>";
									?> 
									<div id="filelists"></div>
									<div style="clear:both;height:1px;"></div>
							</div>
							<div class="tab-pane" id="tab-4">
									<?
										echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mydesks['id'],"Hozzászólások")."</div>";
									?>
									
							</div>
							<div class="tab-pane" id="tab-5">
								<div id="printdiv"></div>
							
							</div>
					
						</div>
						<!-- END Tabs Content -->
					</div>
					<!-- END Block Tabs -->
						
				</div>
				
				<?php include 'inc/page_footer.php'; ?>
				<?php include 'inc/template_scripts.php'; ?>
				<script src="modules/sales/js/inc_desks.js"></script>

				<script >
					showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mydesks['id'];?>,'Dokumentum feltöltés','filelists');
					showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mydesks['id'];?>,'Dokumentum feltöltés','filelists');
								
			
				</script>
				<script>$(function(){ desksDatatables.init(); });</script>	
			

				<?php include 'inc/template_end.php'; ?>		
	<?
				}
			}
		}
		else{
			//print_r($_SESSION['planetsys'])
?>    
	<!-- Ez a lista  -->
				<ul class="breadcrumb breadcrumb-top">
					<li><a href="/">Rendszer</a></li>
					<li><a href="/<?=$URI;?>">Értékesítési helyek</a></li>
				</ul>
				<!-- END Datatables Header -->

				<!-- Datatables Content -->
				<div class="block full">
					<div class="block-title">
						<h2><strong>Értékesítési hely</strong> lista</h2>
					</div>
					

					<div class="table-responsive">
						<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
							<thead>
								<tr>
									<?
										foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
											if($screen['elements'][$f]['textname']!=""){
												echo"<th>".$screen['elements'][$f]['textname']."</th>";
											}
										}
										//echo"<th>Művelet</th>";
										?>
								</tr>
							</thead>
							<tbody ></tbody>
						</table>
					</div>
				</div>
				<!-- END Datatables Content -->
			</div>
			<!-- END Page Content -->



			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>

			<!-- Load and execute javascript code used only in this page -->
			<script src="modules/sales/js/inc_desks.js"></script>
			<script>$(function(){ desksDatatables.init(); });</script>

			<?php include 'inc/template_end.php'; ?>
<?
}
?>

