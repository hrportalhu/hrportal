<?php
$most=date('Y-m-d',time());

$screen=array(
	"elements"=>array(
		"id"=>array(
			"name"=>"id",	
			"textname"=>"Sorszám",	
			"type"=>"text50",	
			"type_param"=>"",	
			"help"=>"",	
			"defval"=>"",	
			"connected"=>0,	
			"con_table"=>"",	
			"con_table_shortname"=>"",	
			"con_table_fields"=>"",	
			"width"=>"",	
			"deflist"=>0,	
			"scrdb"=>0,	
			"scrcmd"=>1	
		),
		"name"=>array(
			"name"=>"name",	
			"textname"=>"Megrendelés elnevezése",	
			"type"=>"text50",	
			"type_param"=>"",	
			"help"=>"",	
			"defval"=>"",	
			"connected"=>0,	
			"con_table"=>"",	
			"con_table_shortname"=>"",	
			"con_table_fields"=>"",	
			"width"=>"",	
			"deflist"=>1,	
			"scrdb"=>1,	
			"scrcmd"=>1	
		),
		"szall_date"=>array(
			"name"=>"szall_date",	
			"textname"=>"Szálltás ideje",	
			"type"=>"date",	
			"type_param"=>"",	
			"help"=>"",	
			"defval"=>"",	
			"connected"=>0,	
			"con_table"=>"",	
			"con_table_shortname"=>"",	
			"con_table_fields"=>"",	
			"width"=>"",	
			"deflist"=>1,	
			"scrdb"=>1,	
			"scrcmd"=>1	
		),
		"ordernum"=>array(
			"name"=>"ordernum",	
			"textname"=>"Rendelés szám",	
			"type"=>"text50",	
			"type_param"=>"",	
			"help"=>"",	
			"defval"=>"",	
			"connected"=>0,	
			"con_table"=>"",	
			"con_table_shortname"=>"",	
			"con_table_fields"=>"",	
			"width"=>"",	
			"deflist"=>1,	
			"scrdb"=>1,	
			"scrcmd"=>1	
		),
		"partner_id"=>array(
			"name"=>"partner_id",	
			"textname"=>"Megrendelő",	
			"type"=>"selectsql",	
			"type_param"=>"SELECT id,name FROM partners WHERE status = 1 order by name",	
			"help"=>"",	
			"defval"=>"",	
			"connected"=>1,	
			"con_table"=>"partners",	
			"con_table_shortname"=>"p",	
			"con_table_fields"=>array(
				0=>"id",
				1=>"name"
			),	
			"width"=>"",	
			"deflist"=>1,	
			"scrdb"=>1,	
			"scrcmd"=>1	
		),
		"status"=>array(
			"name"=>"status",	
			"textname"=>"Státusz",	
			"type"=>"status",	
			"type_param"=>"",	
			"help"=>"",	
			"defval"=>"1",	
			"connected"=>0,	
			"con_table"=>"",	
			"con_table_shortname"=>"",	
			"con_table_fields"=>"",	
			"width"=>"",	
			"deflist"=>0,	
			"scrdb"=>1,	
			"scrcmd"=>1	
		),
		
	),
	"tableprefix"=>"",
	"modulname"=>"sales",
	"funcname"=>"sales",
	"mylimit"=>"20",
	"mywidth"=>"",
	"scrtable"=>"orders",
	"scrtable_shortname"=>"o",
	
);


getlistoptions($screen);
$screen['screenlist']=$_SESSION['planetsys']['listoption']['screenlist'];
$screen['screendb']=$_SESSION['planetsys']['screendb'];



?>
