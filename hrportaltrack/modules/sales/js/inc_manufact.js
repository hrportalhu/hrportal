/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */

	
var manufactDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/sales/manufact/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"aaSorting": [[ 0, "desc" ]],
				"iDisplayLength": 10,
				"bDestroy": true,				
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/sales/ajax_manufact_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });



            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveeditmanufact(elemid){
	var valid = f_c($('#editmanufact'));
	if(valid){
		f_xml(ServerName +'/modules/sales/ajax_manufact_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editmanufact').serialize(),'');
	}
}


function savenewmanufact(){
	var valid = f_c($('#newmanufact'));
	if(valid){
		f_xml(ServerName +'/modules/sales/ajax_manufact_data.php','newsubmit=1&' + $('#newmanufact').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/sales/ajax_manufact_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}

function showmanufactlist(){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/sales/ajax_showmanufactlist.php',
		data:'newsubmit=1&elemid=1&szalldate='+$('#manufact_date').val(),
		success: function(data){
			$('#manufactlistid').html(data);
		}});	
}

function manufactkiadok(elemid){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/sales/ajax_manufactkiadok.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#let_'+elemid).css("background-color", "yellow");
			
		}
	});	
}
var datem;
function domanufact(elemid,datem){
	if(confirm("Biztos egyben rögzíted a "+datem+" napi komplett termelést?")){
		$.ajax({
			type:'POST',
			url: ServerName +'/modules/sales/ajax_domanufact.php',
			data:'elemid='+elemid,
			success: function(data){
				document.location='/modules/goods/products';
				
			}
		});	
	}
}
  $(function() {
    $( "#accordion" ).accordion({ heightStyle: "content",collapsible: true, active: false});
  });
