/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */

	
var salesDatatables = function() {
	return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/sales/sales/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"aaSorting": [[ 0, "desc" ]],
				"iDisplayLength": 10,
				"bDestroy": true,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/sales/ajax_orders_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					aoData.push({"name": "partner_id", "value":$("#partner_id").val() });
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });



            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
            
            
        }
    };
}();

function saveeditsales(elemid){
	var valid = f_c($('#editsales'));
	if(valid){
		f_xml(ServerName +'/modules/sales/ajax_sales_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editsales').serialize(),'');
	}
}


function savenewsales(){
	var valid = f_c($('#newsales'));
	if(valid){
		f_xml(ServerName +'/modules/sales/ajax_sales_data.php','newsubmit=1&' + $('#newsales').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/sales/ajax_sales_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}

function savenewsaleselement(elemid,targetdiv){
	if($('#myptid').val()!="" && $('#mennyiseg').val()!="" ){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/sales/ajax_savenewsaleselement.php',
		data:'newsubmit=1&elemid='+elemid+'&'+$('#newelementformord').serialize(),
		success: function(data){
			
			/*showorderement(elemid,targetdiv);
			$('#myptid').val('');
			$('#mennyiseg').val('');
			$('#mertek').val('');*/
			$('#'+targetdiv).html(data);
		}});
	}
	else{
		alert("Üres tételt nem lehet betölteni!");
	}
}
function showraktarkiad(elemid,targetdiv){
	
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/sales/ajax_showraktarkiad.php',
		data:'newsubmit=1&elemid='+elemid,
		success: function(data){
			$('#'+targetdiv).html(data);
		}});

}

function delorderelemet(elemid,headerid){
	if(confirm("Biztos törlöd ezt a tételt a rendelésből")){	
		$.ajax({
			type:'POST',
			url: ServerName +'/modules/goods/ajax_delbevetelemet.php',
			data:'delsubmit=1&elemid='+elemid,
			success: function(data){
				document.location='/modules/sales/sales/'+headerid;
			}});
		}
	
}




function showsalesement(elemid,targetdiv){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/sales/ajax_showsalesement.php',
		data:'elemid='+elemid,
		success: function(data){
			$('#'+targetdiv).html(data);
		}});
}


function kiadgroup(elemid,post_id){
	$.ajax({
		type:'POST',
		url: ServerName +'/modules/sales/ajax_kiadgroup.php',
		data:'elemid='+elemid+'&post_id='+post_id,
		success: function(data){
			document.location='/modules/goods/issue';
		}});
}

function loadmesure(elemid){
		$.ajax({
		type:'POST',
		url: ServerName +'/modules/sales/ajax_loadmesure.php',
		data:'elemid='+elemid,
		success: function(data){
			//document.location='/modules/goods/issue';
		}});
}

 $("#agt_id").autocomplete({
	 source: ServerName+"/modules/sales/ajax_alapanyag_term.php",
	 minLength: 0,select: 
		function(event, ui) {
			$("#agt_id").val(ui.item.value);
		}
	});
