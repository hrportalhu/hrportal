<?
    //$screen=getscreenelems('sales','manufact');
?>
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>GYártás tervező kezelő
            </h1>
        </div>
    </div>
<?

		if(isset($uritags[3]) && $uritags[3]!=""){
			if($uritags[3]=="new"){
				?>
				<!-- Ez egy elem  -->		
				<ul class="breadcrumb breadcrumb-top">
					<li><a href="/">Rendszer</a></li>
					<li><a href="/modules/sales/manufact">GYártás tervező kezelő</a></li>
					<li><a href="/new">Új gyártás felvitele</a></li>
				</ul>
				<div class="block full">
				<?
				
					echo"<form id=\"newmanufact\" name=\"newmanufact\"  method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
					echo "<div style=\"margin-top:1px;\">";
							
						echo"<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"name\">Elnevezés:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" placeholder=\"GYártás elnevezése\"  />																		
							</div>
						</div>";
						echo"<div class=\"form-group\">
							<label class=\"col-md-3 control-label\" for=\"manufact_date\">Szállítás ideje:</label>
							<div class=\"col-md-9\">
								<input type=\"text\" id=\"manufact_date\" name=\"manufact_date\" class=\"form-control input-datepicker\" placeholder=\"Szállítás ideje\"  	/>																		
							</div>
						</div>";
						echo"<div class=\"form-group\" >";
							 echo"<label class=\"col-md-9 switch switch-primary\" ><input type=\"checkbox\" id=\"status\" name=\"status\" checked><span></span><br />Státusz</label>";
						echo"</div>";
						echo"<div style=\"clear:both;height:10px;\"></div>";

						echo"</div>";
						echo"<div style=\"clear:both;height:10px;\"></div>";

						echo"<div style=\"clear:both;height:10px;\"></div>";
						echo"<div class=\"form-group form-actions\">
							<div class=\"col-md-9 col-md-offset-3\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewmanufact();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>";

					echo"</div>
					</form>";
						echo"<div id=\"manufactlistid\">";
						
						
						
$pts=db_all("select op.pt2_id as pt2_id from orders_pt op left join orders o on (op.order_id=o.id) where o.szall_date='2015-03-30 00:00:00' group by pt2_id ");

?>

<table id="general-table" class="table table-striped table-vcenter">
	<thead>
		<tr>
			<th>Termék elnevezése</th>
			<th style="width: 80px;" class="text-center">Késztermék</th>
			<th>Rendelt mennyiség</th>
			<th>Készleten levő mennyiség</th>
			<th>Hiányzó mennyiség /Kg/</th>
			<th>Gyártani kell</th>
		</tr>
	</thead>
	<tbody>
		<?
		for($i=0;$i<count($pts);$i++){
			$mpt=db_row("select * from product_types where id='".$pts[$i]['pt2_id']."'");
			$m=db_one("select name from mertekegyseg where id='".$mpt['egyseg']."'");
			
			$sume=db_one("select sum(pt2_sume) as sum from orders_pt where pt2_id='".$pts[$i]['pt2_id']."'  ");
			$keszlet=round($mpt['amount']);
			$hianyzo=$sume-$keszlet;
			$gy=db_row("select * from pt_units where pt_id='".$pts[$i]['pt2_id']."' and units_type='2' and unit2_id=1 ");
			$m2=db_one("select name from mertekegyseg where id='".$gy['unit1_id']."'");
			$gykell=ceil($hianyzo/$gy['mesure2']);
			//echo 
			
			
			echo "<tr>";
			echo "<td><a href=\"/modules/goods/producttype/".$mpt['id']."\">".$mpt['name']."</a></td>";
			$kesz="";if($mpt['endproduct']==1){$kesz=" checked";}echo "<td class=\"text-center\"><input type=\"checkbox\" id=\"checkbox1-1\" name=\"checkbox1-1\" ".$kesz."></td>";
			echo "<td>".$sume." ".$m."</td>";
			echo "<td>".$keszlet." ".$m."</td>";
			echo "<td >".$hianyzo." ".$m."</td>";
			echo "<td ><input type=\"text\" name=\"gyi_".$mpt['id']."\" value=\"".$gykell."\" style=\"width:30px;\"/> ".$m2." (minimum ".$gykell." ".$m2.")</td>";
			echo "</tr>";
			//echo $mpt['name']."  ".$sume."<br />";
	
		}
		?>	
		</tr>
  </tbody>  
</table>
						
						<?
						
						

				?>
					</div>	
				</div>	
				<?php include 'inc/page_footer.php'; ?>
				<?php include 'inc/template_scripts.php'; ?>
				<script src="modules/sales/js/inc_manufact.js"></script>
				<?php include 'inc/template_end.php'; ?>	
				<?	
			}
			else{
				
				//echo "select * from functions where id='".$uritags[2]."'";
				$mymanufact=db_row("select * from manufact_header where id='".$uritags[3]."'");
				if($mymanufact['id']==''){
					die("Helytelen hozzáférés!");
				}
				else{
	?>
				<!-- Ez egy elem  -->		
				<ul class="breadcrumb breadcrumb-top">
					<li><a href="/">Rendszer</a></li>
					<li><a href="modules/sales/manufact">Gyártás</a></li>
					<li><a href="/<?=$URI;?>"><?=$mymanufact['name'];?></a></li>
				</ul>
				<div class="block full">
								   
						<!-- Block Tabs Title -->

						<div class="block-title">
							<h2><strong><?=$mymanufact['name'];?></strong> Gyártás oldala</h2>
							<ul class="nav nav-tabs" data-toggle="tabs">
								<?
								if(priv("manufact","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
								if(priv("manufact","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
								if(priv("manufact","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
								if(priv("manufact","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
								if(priv("manufact","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}	
								?>
							</ul>
						</div>
						<!-- END Block Tabs Title -->

						<!-- Tabs Content -->
						<div class="tab-content">

							<div class="tab-pane active" id="tab-1">
								<div class="col-md-6">
									<dl class="dl-horizontal">
										<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$mymanufact['id']); ?></dd>
										<dt>Elnevezés:</dt><dd><? echo $mymanufact['name']; ?></dd>
										<dt>Szállítási nap:</dt><dd><? echo $mymanufact['manufact_date']; ?></dd>
										<dt>Érvényes?:</dt><dd><? if($mymanufact['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
									</dl>	
								</div>
								
								<div style="clear:both;height:1px;"></div>
								<?
								
								$posts=db_all("select post_id from manufact_element where mh_id='".$mymanufact['id']."' and post_id!=0 group by post_id order by post_id ");
								//echo"<div class=\"block-title\">";
								echo"<h3>Posztok gyártása és alapanyag igénye</h3>";
									
								echo"<div id=\"accordion\" >";	
									for($i=0;$i<count($posts);$i++){
										$szorb=array();
										$cuk=array();
										echo"<h1>".idchange("gw_posts","name",$posts[$i]['post_id'])."</h1>";
										echo"<div >";
										$pts=db_all("select * from manufact_element where post_id='".$posts[$i]['post_id']."' and mh_id='".$mymanufact['id']."' group by pt_id order by pt_id ");
										
										echo"<table id=\"general-table\" class=\"table table-striped table-vcenter\" >
											<thead>
												<tr>
													<th>Termék elnevezése</th>
													<th>Rendelt mennyiség - rendelése</th>
													<th>Rendelt mennyiség - Kg</th>
													<th>Készleten levő mennyiség</th>
													<th>Hiányzó mennyiség /Kg/</th>
													<th>Gyártani kell</th>
												</tr>
											</thead>
											<tbody>";
										for($a=0;$a<count($pts);$a++){
											$me=mee($pts[$a]['pt_id']);
											$gya=db_one("select sum(manufact_amount) from manufact_element where post_id='".$posts[$i]['post_id']."' and mh_id='".$mymanufact['id']."' and  pt_id='".$pts[$a]['pt_id']."' ");
											echo"<tr>";
											echo"<td><a href=\"modules/goods/producttype/".$pts[$a]['pt_id']."\">".idchange("product_types","name",$pts[$a]['pt_id'])."</a></td>";
											echo"<td>".$pts[$a]['orderbase_amount']."</td>";
											echo"<td>".round($pts[$a]['order_amount'],3)."</td>";
											echo"<td>".$pts[$a]['instock']."</td>";
											echo"<td>".round(($pts[$a]['order_amount']-$pts[$a]['instock']),3)."</td>";
											echo"<td>".$gya." ".$me['gyartas_egyseg']."</td>";
											echo"</tr>";
										}
										  echo"</tbody> </table>";
										echo"<div id=\"pal-".$i."\" >";
										
										echo"<h4 >".idchange("gw_posts","name",$posts[$i]['post_id'])." alapanyag lista ".$mymanufact['manufact_date']." napra</h4>";
										echo"<table id=\"general-table\" class=\"table table-striped table-vcenter\">
											<thead>
												<tr>
													<th>Alapanyag megnevezése</th>
													<th>Kiadandó mennyiség</th>
													<th>&nbsp;</th>
												</tr>
											</thead>
											<tbody>";
											$aids=array();
											$ait="and (";
											for($z=0;$z<count($pts);$z++){
												$ag=db_all("select agt_id from manufact_ag where  pt_id='".$pts[$z]['pt_id']."'  and mf_id='".$mymanufact['id']."' order by agt_name asc ");
												for($u=0;$u<count($ag);$u++){
													$aids[]=$ag[$u]['agt_id'];
													$ait.=" agt_id='".$ag[$u]['agt_id']."' or ";
												}
											}
											//$ait=substr($ait,0,-3).") ";
											//echo $ait."--";
											//echo "select * from manufact_ag where post_id='".$posts[$i]['post_id']."' group by agt_id order by agt_name asc \n";
											$allszorbat=array();
											$allcukor=array();
											$alapanyags=db_all("select * from manufact_ag where post_id='".$posts[$i]['post_id']."' group by agt_id order by agt_name asc ");
											
											for($a=0;$a<count($alapanyags);$a++){
												$col="";if($alapanyags[$a]['kiadva']==1){
													$col="background-color:yellow;";
												}
												$summ=db_one("select sum(agt_sume) from manufact_ag where post_id='".$posts[$i]['post_id']."' and mf_id='".$mymanufact['id']."' and agt_id='".$alapanyags[$a]['agt_id']."'");
												//echo "select sum(agt_sume) from manufact_ag where post_id='".$posts[$i]['post_id']."' ".$ait."  and mf_id='".$mymanufact['id']."'";
												
												if($summ!=""){
													if($alapanyags[$a]['agt_id']=="61"){
														$prods=db_all("select pt_id,pt_name from manufact_ag where post_id='".$posts[$i]['post_id']."' and agt_id='".$alapanyags[$a]['agt_id']."' and mf_id='".$mymanufact['id']."' group by pt_id");
														if(count($prods)>0){
															for($v=0;$v<count($prods);$v++){
																$keveres=db_one("select sum(pt_me) from manufact_ag where post_id='".$posts[$i]['post_id']."' and agt_id='".$alapanyags[$a]['agt_id']."' and mf_id='".$mymanufact['id']."' and pt_id='".$prods[$v]['pt_id']."'");
																$sume=db_one("select sum(agt_sume) from manufact_ag where post_id='".$posts[$i]['post_id']."' and agt_id='".$alapanyags[$a]['agt_id']."' and mf_id='".$mymanufact['id']."' and pt_id='".$prods[$v]['pt_id']."'");	
																$allszorbat[$v]['post']=$posts[$i]['post_id'];
																$allszorbat[$v]['nap']=$mymanufact['name'];
																$allszorbat[$v]['term_id']=$prods[$v]['pt_id'];
																$allszorbat[$v]['term']=$prods[$v]['pt_name'];
																$allszorbat[$v]['prod']=$prods[$v]['pt_id'];
																$allszorbat[$v]['keveres']=$keveres;
																$allszorbat[$v]['sume']=round($sume,4);
																$szorb[]=$allszorbat;
															}
														}
													}
													if($alapanyags[$a]['agt_id']=="15"){
														$prods=db_all("select pt_id,pt_name from manufact_ag where post_id='".$posts[$i]['post_id']."' and agt_id='".$alapanyags[$a]['agt_id']."' and mf_id='".$mymanufact['id']."' group by pt_id");
														if(count($prods)>0){
															for($v=0;$v<count($prods);$v++){
																$keveres=db_one("select sum(pt_me) from manufact_ag where post_id='".$posts[$i]['post_id']."' and agt_id='".$alapanyags[$a]['agt_id']."' and mf_id='".$mymanufact['id']."' and pt_id='".$prods[$v]['pt_id']."'");
																$sume=db_one("select sum(agt_sume) from manufact_ag where post_id='".$posts[$i]['post_id']."' and agt_id='".$alapanyags[$a]['agt_id']."' and mf_id='".$mymanufact['id']."' and pt_id='".$prods[$v]['pt_id']."'");	
																$allcukor[$v]['post']=$posts[$i]['post_id'];
																$allcukor[$v]['nap']=$mymanufact['name'];
																$allcukor[$v]['term_id']=$prods[$v]['pt_id'];
																$allcukor[$v]['term']=$prods[$v]['pt_name'];
																$allcukor[$v]['prod']=$prods[$v]['pt_id'];
																$allcukor[$v]['keveres']=$keveres;
																$allcukor[$v]['sume']=round($sume,4);
																$cuk[]=$allcukor;
															}
														}
													}
												
												echo"<tr >
													
													<td>".$alapanyags[$a]['agt_name']."</td>
													<td>".round($summ,5)." Kg</td>
													<td>";
													echo"<div class=\"btn-group btn-group-xs\" >";
														echo"<a href=\"javascript:manufactkiadok('".$alapanyags[$a]['id']."');\" data-toggle=\"tooltip\" title=\"Kiadva\"  class=\"btn btn-info\"><i class=\"gi gi-floppy_saved\"></i></a>";
														echo"<div id=\"let_".$alapanyags[$a]['id']."\" style=\"width:5px;float:left;height:22px;".$col."\">&nbsp;</div>";
														echo"</div>";
													echo"</td>
													</tr>";
												}	
												
											}

												//print_r($allszorbat);	

											echo"</tbody> 
											</table>";
											echo"</div >";
											//echo"<div style=\"clear:both;height:10px;\"></div>
												echo"<div class=\"form-group form-actions\">
													<div class=\"col-md-9\">";
													echo"<div  onclick=\"javascript:getprintscreen('".$_SESSION['planetsys']['actpage']['modulname']."','manufactalapprint','".$mymanufact['id']."-".$posts[$i]['post_id']."');\" style=\"width:250px;height:20px;border:1px solid #ccc;text-align:center;float:left;\" ><a href=\"#modal-regular-print\" data-toggle=\"modal\" title=\"Spec print\"><i class=\"gi gi-print\" ></i>Alapanyaglista Nyomtatása</a></div>";
													echo"<div  onclick=\"javascript:getprintscreen('".$_SESSION['planetsys']['actpage']['modulname']."','manufactprodprint','".$mymanufact['id']."-".$posts[$i]['post_id']."');\" style=\"width:250px;height:20px;border:1px solid #ccc;text-align:center;float:left;\" ><a href=\"#modal-regular-print\" data-toggle=\"modal\" title=\"Spec print\"><i class=\"gi gi-print\" ></i>Gyártási lista Nyomtatása</a></div>";
													if(count($szorb)>0){echo"<div  onclick=\"javascript:getprintscreen('".$_SESSION['planetsys']['actpage']['modulname']."','manufactsorbatprint','".$mymanufact['id']."-".$posts[$i]['post_id']."');\" style=\"width:250px;height:20px;border:1px solid #ccc;text-align:center;float:left;\" ><a href=\"#modal-regular-print\" data-toggle=\"modal\" title=\"Spec print\"><i class=\"gi gi-print\" ></i>Kálium-szorbát bemérés Nyomtatása</a></div>";}
													if(count($cuk)>0){echo"<div  onclick=\"javascript:getprintscreen('".$_SESSION['planetsys']['actpage']['modulname']."','manufactcukorprint','".$mymanufact['id']."-".$posts[$i]['post_id']."');\" style=\"width:250px;height:20px;border:1px solid #ccc;text-align:center;float:left;\" ><a href=\"#modal-regular-print\" data-toggle=\"modal\" title=\"Spec print\"><i class=\"gi gi-print\" ></i>Porcukor bemérés Nyomtatása</a></div>";}
													echo"</div>
												</div>";
											echo"</div>";
										}

								echo"</div>";	
									
									
								/*
									echo"<ul class=\"nav nav-tabs\" data-toggle=\"tabs\">";
										for($i=0;$i<count($posts);$i++){
											$acz1="";if($i==0){$acz1=" class=\"active\" ";}
											echo"<li><a href=\"#ptab-".$i."\" ".$acz1." data-toggle=\"tooltip\" title=\"".idchange("gw_posts","name",$posts[$i]['post_id'])."\">".idchange("gw_posts","name",$posts[$i]['post_id'])."</a></li>";
										}
									echo"</ul>";
									*/ 
								//echo"</div>";
								echo"<div class=\"tab-content\" style=\"background-color:#ffffff;\">";
									echo"<div style=\"clear:both;height:10px;\"></div>
						
									<div class=\"form-group form-actions\">
										<div class=\"col-md-9 \">
											<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"domanufact(".$mymanufact['id'].",'".$mymanufact['manufact_date']."');\"><i class=\"gi gi-beer\"></i> Gyárt</button>
										</div>
									</div>";
								echo"<div>";
								
							?>
								<div style="clear:both;height:1px;"></div>

							</div>
							<div class="tab-pane" id="tab-2"></div>
							<div class="tab-pane" id="tab-3">
									<?
										echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mymanufact['id'],"Dokumentum feltöltés")."</div>";
									?> 
									<div id="filelists"></div>
									<div style="clear:both;height:1px;"></div>
							</div>
							<div class="tab-pane" id="tab-4">
									<?
										echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$mymanufact['id'],"Hozzászólások")."</div>";
									?>
									
							</div>
							<div class="tab-pane" id="tab-5">
								<div id="printdiv"></div>
							
							</div>
					
						</div>
						<!-- END Tabs Content -->
					</div>
					<!-- END Block Tabs -->
						
					</div>
				
				</div>
				</div>

				
				<?php include 'inc/page_footer.php'; ?>
				<?php include 'inc/template_scripts.php'; ?>
				<script src="modules/sales/js/inc_manufact.js"></script>

				<script >
					showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mymanufact['id'];?>,'Dokumentum feltöltés','filelists');
					showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$mymanufact['id'];?>,'Dokumentum feltöltés','filelists');
								
			
				</script>
				<script>$(function(){ manufactDatatables.init(); });</script>	
			

				<?php include 'inc/template_end.php'; ?>	
	
	<?

				}
			}
		}
		else{			
			//print_r($_SESSION['planetsys'])
?>    
	<!-- Ez a lista  -->
				<ul class="breadcrumb breadcrumb-top">
					<li><a href="/">Rendszer</a></li>
					<li><a href="/<?=$URI;?>">GYártások</a></li>
				</ul>
				<!-- END Datatables Header -->

				<!-- Datatables Content -->
				<div class="block full">
					<div class="block-title">
						<h2><strong>Gyártás</strong> lista</h2>
					</div>
					

					<div class="table-responsive">
						<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
							<thead>
								<tr>
									<?
										foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
											if($screen['elements'][$f]['textname']!=""){
												echo"<th>".$screen['elements'][$f]['textname']."</th>";
											}
										}
										//echo"<th>Művelet</th>";
										?>
								</tr>
							</thead>
							<tbody ></tbody>
						</table>
					</div>
				</div>
				<!-- END Datatables Content -->
			</div>
			<!-- END Page Content -->




			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>

			<!-- Load and execute javascript code used only in this page -->
			<script src="modules/sales/js/inc_manufact.js"></script>
			<script>$(function(){ manufactDatatables.init(); });</script>

			<?php include 'inc/template_end.php'; ?>
<?
}
?>

