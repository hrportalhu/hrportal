<?php include 'inc/page_head.php'; ?>

<?
if($uritags[2]!=''&&$uritags[2]=='visitors'){include_once dirname(__FILE__).("/visitors.php");}
elseif($uritags[2]!=''&&$uritags[2]=='cleaner_type'){include_once dirname(__FILE__).("/cleaner_type.php");}
	else{
?>

<!-- Page content -->
<div id="page-content">
    <!-- Dashboard Header -->
    <!-- For an image header add the class 'content-header-media' and an image as in the following example -->
    <div class="content-header content-header-media">
        <div class="header-section">
            <div class="row">
                <!-- Main Title (hidden on small devices for the statistics to fit) -->
                <div class="col-md-4 col-lg-6 hidden-xs hidden-sm">
                    <h1>Termék kezelő</h1>
                </div>
                <!-- END Main Title -->


            </div>
        </div>
        <!-- For best results use an image with a resolution of 2560x248 pixels (You can also use a blurred image with ratio 10:1 - eg: 1000x100 pixels - it will adjust and look great!) -->
        <img src="img/placeholders/headers/widget1_header.jpg" alt="header image" class="animation-pulseSlow">
    </div>
    <!-- END Dashboard Header -->

    <!-- Mini Top Stats Row -->
    <div class="row">
		<?php 
			
			$topfunctions=explode(',',$_SESSION['planetsys']['user']['quickfunctions']);
			
			
			 echo"<div class=\"col-sm-6 col-lg-3\">";
				echo"<div class=\"widget\">";
					echo"<div class=\"widget-simple\">";
							echo"<a href=\"/moduls/goods/producttype\" class=\"widget-icon pull-left themed-background animation-fadeIn\">";
								echo"<i class=\"fa fa-cutlery\"></i>";
							echo"</a>";
						echo"<h3 class=\"widget-content text-right animation-pullDown\">";
						echo"<strong>Termék típusok</strong><br>";
						echo"<small>torták, sütemények, desszertek</small>";
						echo"</h3>";
					echo"</div>";
				echo"</div>";
			echo"</div>";

		?>
    </div>
    <!-- END Mini Top Stats Row -->

   
</div>
<!-- END Page Content -->

<?php include 'inc/page_footer.php'; ?>

<!-- Remember to include excanvas for IE8 chart support -->
<!--[if IE 8]><script src="js/helpers/excanvas.min.js"></script><![endif]-->

<?php include 'inc/template_scripts.php'; ?>

<!-- Google Maps API + Gmaps Plugin, must be loaded in the page you would like to use maps (Remove 'http:' if you have SSL) -->
<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="js/helpers/gmaps.min.js"></script>

<!-- Load and execute javascript code used only in this page -->
<script src="js/pages/index.js"></script>
<script>$(function(){ Index.init(); });</script>
<?
}
?>
