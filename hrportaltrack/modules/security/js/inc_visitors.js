/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */

var visitorsDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

		$('#example-datatable tbody').on('click', 'tr', function () {
        var id = $('td', this).eq(0).text();
			document.location='/modules/security/visitors/'+id;
		} );

            /* Initialize Datatables */
           $('#example-datatable').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"aaSorting": [[ 0, "desc" ]],
				"iDisplayLength": 30,
				"bDestroy": true,
                "aLengthMenu": [[10, 20, 30, -1], [10, 20, 30, "All"]],
				"oSearch": {"sSearch": $("#stext").val()},
				"sAjaxSource": ServerName + "/modules/security/ajax_visitors_list.php",
				"fnServerData": function(sSource, aoData, fnCallback){
					aoData.push({"name": "name", "value":$("#name").val() });
					$.getJSON( sSource, aoData, function (json) { fnCallback(json)} );
				}
            });
            /* Add Bootstrap classes to select and input elements added by datatables above the table */
            $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Search');
            $('.dataTables_length select').addClass('form-control');
        }
    };
}();

function saveeditvisitors(elemid){
	var valid = f_c($('#editvisitors'));
	if(valid){
		f_xml(ServerName +'/modules/security/ajax_visitors_data.php','view=edit&id=' + elemid +'&editsubmit='+elemid+'&' + $('#editvisitors').serialize(),'');
	}
}

function savetavozottvisitors(elemid){
	
		f_xml(ServerName +'/modules/security/ajax_visitors_data.php','view=tav&id=' + elemid +'&tavsubmit='+elemid,'');

}

function savenewvisitors(){
	var valid = f_c($('#newvisitors'));
	if(valid){
		f_xml(ServerName +'/modules/security/ajax_visitors_data.php','newsubmit=1&' + $('#newvisitors').serialize(),'');
	}
}

function savesetupscreen(elemid){
	var valid = f_c($('#setupform'));
	if(valid){
		f_xml(ServerName +'/modules/security/ajax_visitors_data.php','command=savesetupscreen&viewtype=setupscreen&mod=filing&' + $('#setupform').serialize());
	}
}
