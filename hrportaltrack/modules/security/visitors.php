
<!-- Page content -->
<div id="page-content">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-sampler"></i>Látogatók
            </h1>
        </div>
    </div>
<?
	if(isset($uritags[3]) && $uritags[3]!=""){
		if($uritags[3]=="new"){
			?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/modules/security/visitors">Latógató kezelő</a></li>
				<li><a href="/new">Új látogató</a></li>
			</ul>
			<div class="block full">
				<div class="block-title">
					<h2><strong>Új látogató</strong></h2>
				</div>
			<?
				
				
				echo "<form id=\"newvisitors\" name=\"newvisitors\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
				echo "<div style=\"margin-top:1px;\">";
							
						
			
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"name\">Látogató neve:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" placeholder=\"Látogató neve\" />
							</div>
						</div>";
			
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"szig\">Személyi igazolvány száma:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"szig\" name=\"szig\" class=\"form-control\" placeholder=\"Személyi igazolvány \" />
							</div>
						</div>";
						
					echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"target\">Látogatás célja:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"target\" name=\"target\" class=\"form-control\" placeholder=\"Látogatás célja\" />
							</div>
						</div>";
								echo "<div class=\"form-group\">
									<label class=\"col-md-6 control-label\" for=\"cardcode\">Látogatói kártya száma:</label>
									<div class=\"col-md-6\">
										<input type=\"text\" id=\"cardcode\" name=\"cardcode\" class=\"form-control\" value=\"\" />
									</div>
								</div>";	
																

						
						echo "<div style=\"clear:both;height:10px;\"></div>";
						
						echo "<div class=\"form-group form-actions\">
							<div class=\"col-md-6 col-md-offset-3\">
								<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savenewvisitors();\"><i class=\"fa fa-angle-right\"></i> Ment</button>
							</div>
						</div>";
						
				echo "</div>
				</form>";
			?>
			</div>	
			</div>	
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/security/js/inc_visitors.js"></script>
			<?php include 'inc/template_end.php'; ?>	
			<?	
		}
		else{

			//echo "select * from functions where id='".$uritags[2]."'";
			$myvisitors=db_row("select * from visitors where id='".$uritags[3]."'");
			if($myvisitors['id']==''){
				die("Helytelen hozzáférés!");
			}
			else{
				
?>
			<!-- Ez egy elem  -->		
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="modules/security/visitors"> Látogatók </a></li>
				<li><a href="/<?=$URI;?>"><?=$myvisitors['name'];?></a></li>
			</ul>
			<div class="block full">
							   
					<!-- Block Tabs Title -->

					<div class="block-title">
						<h2><strong><?=$myvisitors['name'];?> </strong> <?=$myvisitors['erkezett'];?> </h2>
						<ul class="nav nav-tabs" data-toggle="tabs">
							<?
							if(priv("visitors","view")){echo"<li class=\"active\"><a href=\"#tab-1\" data-toggle=\"tooltip\" title=\"view\"><i class=\"gi gi-eye_open\"></i></a></li>";}
							if(priv("visitors","edit")){echo"<li><a href=\"#tab-2\" data-toggle=\"tooltip\" title=\"edit\"><i class=\"gi gi-pencil\"></i></a></li>";}
							if(priv("visitors","upload")){echo"<li><a href=\"#tab-3\" data-toggle=\"tooltip\" title=\"upload\"><i class=\"gi gi-floppy_open\"></i></a></li>";}
							if(priv("visitors","comments")){echo"<li><a href=\"#tab-4\" data-toggle=\"tooltip\" title=\"comments\"><i class=\"fa fa-comments-o\"></i></a></li>";}
							//if(priv("visitors","print")){echo"<li><a href=\"#tab-5\" data-toggle=\"tooltip\" title=\"print\"><i class=\"fa fa-print\"></i></a></li>";}	
							if(priv("visitors","print")){echo"<li onclick=\"javascript:getprintscreen('".$_SESSION['planetsys']['actpage']['modulname']."','visitorprint','".$myvisitors['id']."');\" ><a href=\"#modal-regular-print\" data-toggle=\"modal\" title=\"Nyomtatás\"><i class=\"fa fa-print\" ></i></a></li>";}
							?>
						</ul>
					</div>
					<!-- END Block Tabs Title -->

					<!-- Tabs Content -->
					<div class="tab-content">

						<div class="tab-pane active" id="tab-1">
							<div class="col-md-6">
								<dl class="dl-horizontal">
									<dt>Sorszám:</dt><dd><? echo sprintf('%1$08d',$myvisitors['id']); ?></dd>
									<dt>Neve:</dt><dd><? echo $myvisitors['name']; ?></dd>
									<dt>Célja:</dt><dd><? echo $myvisitors['target']; ?></dd>
									<dt>Kártya száma:</dt><dd><? echo $myvisitors['cardcode']; ?></dd>
									<dt>Érkezett:</dt><dd><? echo $myvisitors['erkezett']; ?></dd>
									<dt>Távozott:</dt><dd><? echo $myvisitors['tavozott']; ?></dd>
									<dt>Státusz:</dt><dd><? if($myvisitors['status']==1){echo"Aktív";}else{echo"Inaktív";} ?></dd>
								</dl>	
							</div>
							<div class="col-md-6">
							<?
								if($myvisitors['tavozott']=="0000-00-00 00:00:00"){
									echo "<div class=\"form-group form-actions\">
									<div class=\"col-md-6\">
										<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"savetavozottvisitors(".$myvisitors['id'].")\"><i class=\"fa fa-angle-right\"></i> Távozási időpont rögzítése</button>
									</div>
								</div>";
								}
							?>
							</div>
							<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-2">
						<?
							
							
							echo"<form id=\"editvisitors\" name=\"editvisitors\" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal form-bordered\" onsubmit=\"return false;\">";
							echo "<div style=\"margin-top:1px;\">";
								
								echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"name\">Látogató neve:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" value=\"".$myvisitors['name']."\" />
							</div>
						</div>";
			
						echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"szig\">Személyi igazolvány száma:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"szig\" name=\"szig\" class=\"form-control\" value=\"".$myvisitors['szig']."\"  />
							</div>
						</div>";
						
					echo "<div class=\"form-group\">
							<label class=\"col-md-6 control-label\" for=\"target\">Látogatás célja:</label>
							<div class=\"col-md-6\">
								<input type=\"text\" id=\"target\" name=\"target\" class=\"form-control\" value=\"".$myvisitors['target']."\"  />
							</div>
						</div>";
								echo "<div class=\"form-group\">
									<label class=\"col-md-6 control-label\" for=\"cardcode\">Látogatói kártya száma:</label>
									<div class=\"col-md-6\">
										<input type=\"text\" id=\"cardcode\" name=\"cardcode\" class=\"form-control\" value=\"".$myvisitors['carcode']."\"  />
									</div>
								</div>";	
															
								
								echo "<div class=\"form-group form-actions\">
									<div class=\"col-md-6 col-md-offset-3\">
										<button type=\"button\" class=\"btn btn-sm btn-primary\" id=\"saveeditbutton\" onclick=\"saveeditvisitors(".$myvisitors['id'].")\"><i class=\"fa fa-angle-right\"></i> Ment</button>
									</div>
								</div>";
									
							echo "</div>
							</form>";
						?>
						</div>

						<div class="tab-pane" id="tab-3">
								<?
									echo "<div>".uploadplace_new("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$myvisitors['id'],"Dokumentum feltöltés")."</div>";
								?> 
								<div id="filelists"></div>
								<div style="clear:both;height:1px;"></div>
						</div>
						<div class="tab-pane" id="tab-4">
								<?
									echo"<div>".commentplace("".$_SESSION['planetsys']['actpage']['modulname']."","".$_SESSION['planetsys']['actpage']['funcname']."",$_SESSION['planetsys']['user_id'],$myvisitors['id'],"Hozzászólások")."</div>";
								?>
						</div>
						<div class="tab-pane" id="tab-5">
							<div id="printdiv"></div>
						</div>
				
					</div>
					<!-- END Tabs Content -->
				</div>
				<!-- END Block Tabs -->
					
			</div>
			
			<?php include 'inc/page_footer.php'; ?>
			<?php include 'inc/template_scripts.php'; ?>
			<script src="modules/security/js/inc_visitors.js"></script>

			<script >
				showcommentlist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$_SESSION['planetsys']['user_id'];?>,<?=$myvisitors['id'];?>,'Dokumentum feltöltés','filelists');
				showfilelist('<?=$_SESSION['planetsys']['actpage']['modulname'];?>','<?=$_SESSION['planetsys']['actpage']['funcname'];?>',<?=$myvisitors['id'];?>,'Dokumentum feltöltés','filelists');
			</script>
			<script>$(function(){ visitorsDatatables.init(); });</script>	

			<?php include 'inc/template_end.php'; ?>		
<?
			}
		}
	}
	else{
		//print_r($_SESSION['planetsys'])
?>    
<!-- Ez a lista  -->
			<ul class="breadcrumb breadcrumb-top">
				<li><a href="/">Rendszer</a></li>
				<li><a href="/<?=$URI;?>">Látogatók</a></li>
			</ul>
			<!-- END Datatables Header -->
		
				
				
			<!-- Datatables Content -->
			<div class="block full">
				<div class="block-title">
					<h2><strong>Látogatói lista</strong> lista</h2>
				</div>
				
				<div class="table-responsive">
					<table id="example-datatable" class="table table-vcenter table-striped table-borderless table-hover">
						<thead>
							<tr>
							<?
							foreach(explode(',',$_SESSION['planetsys']['listoption']['screenlist']) as $f){
								if($screen['elements'][$f]['textname']!=""){
									echo"<th>".$screen['elements'][$f]['textname']."</th>";
								}
							}
							//echo"<th>Művelet</th>";
							?>
							</tr>
						</thead>
						<tbody ></tbody>
					</table>
				</div>
			</div>
			<!-- END Datatables Content -->
		</div>
		<!-- END Page Content -->

		<?php include 'inc/page_footer.php'; ?>
		<?php include 'inc/template_scripts.php'; ?>

		<!-- Load and execute javascript code used only in this page -->
		<script src="modules/security/js/inc_visitors.js"></script>
		<script>$(function(){ visitorsDatatables.init(); });</script>

		<?php include 'inc/template_end.php'; ?>
<?
}
?>

