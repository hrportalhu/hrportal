<?PHP
//      Funkcio:        Planet Star alap funkciok
//
//                      Verzió  Dátum        	Módosította     Módosítások
//      Verziokovetes:  1.002	2010.08.14      six             Másolás a lib alól
//						1.100	2011.06.29		six				Kód tisztítás keret2-hoz

if(basename($_SERVER['PHP_SELF']) == '_functions.php'){print "Nem megengedett hozzaferesi mod!";exit;}

$wrong=array('[','\\',"'",'"','$','%',']','@','/');
$right=array('&#91;','&#92;','&apos;','&quot;','&#36;','&#37;','&#93;','&#64;','&#47;');
foreach($_GET as $k=>$v){if(get_magic_quotes_gpc()){$_GET[$k]=stripslashes($v);}$_GET[$k]=str_replace($wrong,$right,$v);}

$arrfrom=array("<",">",'"',"%","‘","’","‚","“","”","„","'","!","|",'/',"[","]","(",")","{","}","*","?","@",":","$");
$arrto=array("&#60;","&#62;","&quot;","&#37;","&#8216;","&#8217;","&#8218;","&#8220;","&#8221;","&#8222;","&#39;","&#33;","&#124;","&#47;","&#91;","&#93;","&#40;","&#41;","&#123;","&#125;","&#42;","&#63;","&#64;","&#58;","&#36;");
foreach($_POST as $k=>$v){if(get_magic_quotes_gpc()){$_POST[$k]=stripslashes($v);}
    $v=preg_replace('/&([a-z]{2,4});/i','[[$1]]',$v);
    $v=preg_replace('/&#([0-9]{2,4});/i','[[$1]]',$v);
    $v=str_replace("&","&amp;",$v);
    $_POST[$k]=str_replace($arrfrom,$arrto,$v);
}


global $connid;

/* MYSQL specific db functions */
if(!$connid = @mysql_pconnect(DB_SERVER, DB_USER, DB_PASSWORD)){
	die ("<p class='error'>Cannot connect to mysql server. Is the server running?</p>");
}

if (!mysql_select_db(DB_NAME, $connid)){
	mysql_query("CREATE DATABASE ".DB_NAME." DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci", $connid);
	@mysql_select_db(DB_NAME,$connid) or die ("<p class='error'>Cannot find database. Please initialize database (".DB_NAME.")!</p>");
}
mysql_query("SET NAMES utf8;");


function db_execute($query,$debug=0){
	if($debug==1){
		echo $query;
	}
	extract($GLOBALS);$qid=mysql_query($query,$connid);
	return $qid;
}

function db_rownumber($qid){
	extract($GLOBALS);$rows=@mysql_num_rows($qid);if($rows=='') $rows=0;
	return $rows;
}

function db_one($query, $i=0){
	extract($GLOBALS);$result=mysql_query($query,$connid);
 	$output=mysql_fetch_array($result);
	return $output[0];
}

function db_row($query, $i = 0){
	extract($GLOBALS);$result=mysql_query($query,$connid);
 	$output=mysql_fetch_array($result);
	return $output;
}

function db_onerow($r,$i=MYSQL_ASSOC){
	extract($GLOBALS);return mysql_fetch_assoc($r);
}
function db_multirow($result, $i = 0){
	extract($GLOBALS);
	$myar = array();
	for($myi=0;$myi<db_rownumber($result);$myi++){
        $output = mysql_fetch_array($result,MYSQL_ASSOC);
        foreach($output as $key => $value){
            if($key != '0'){$myar[$myi][$key] = $value;}
        }
    }
	return $myar;
}

function db_all($query,$debug=0){
	extract($GLOBALS);
	if($debug==1){
		echo $query;	
	}
	$result=mysql_query($query,$connid);
	$myar = array();
	$rows=@mysql_num_rows($result);
	for($myi=0;$myi<$rows;$myi++){
		$output=mysql_fetch_assoc($result);
		foreach($output as $key => $value){
			if($key != '0'){
				$myar[$myi][$key] = $value;
			}
		}
	}
	return $myar;
}

function dangChars($s){
	$arrfrom=array("&#60;","&#62;","&quot;","&#37;","&#8216;","&#8217;","&#8218;","&#8220;","&#8221;","&#8222;","&#39;","&#33;","&#124;","&#47;","&#91;","&#93;","&#40;","&#41;","&#123;","&#125;","&#42;","&#63;","&#64;","&#58;","&#36;");
	$arrto=array("<",">",'"',"%","‘","’","‚","“","”","„","'","!","|",'/',"[","]","(",")","{","}","*","?","@",":","$");
	$s=str_replace($arrfrom,$arrto,$s);
	$s=str_replace("&amp;","&",$s);
	$s=preg_replace('/\[\[([0-9]{2,4})\]\]/i','&#$1;',$s);
	return preg_replace('/\[\[([a-z]{2,4})\]\]/i','&$1;',$s);
}


function parseget($newstr){
	$getparameters = '?';foreach ($_GET as $getkey => $getval){
	if(strpos($newstr, $getkey.'=') !== false){$stpos = strpos($newstr, $getkey.'=');$cutstr = substr($newstr, $stpos, (strpos($newstr, '&', $stpos)));$getparameters .= substr($newstr, $stpos, (strpos($newstr, '&', $stpos))).'&';$newpl=strlen(substr($newstr, $stpos, (strpos($newstr, '&', $stpos))));
	$newstr = str_replace($cutstr,"",$newstr);if($newstr == '&'){$newstr = '';}}else{$getparameters .= $getkey .'='. preg_replace('/[\'";$%]/','',$getval) .'&';}}$getparameters.=$newstr;if(strpos($getparameters,'adm=') === true)
	{$getparameters.='adm=help';}else{$getparameters = substr($getparameters,0,-1);}$getparameters = str_replace('&&','&',$getparameters);
	return $getparameters;
}


function antikiddie($szoveg) {
    $szoveg=preg_replace("`<([\/b]?[abipr])>`im","`!--\\1--`",$szoveg);
    $szoveg=preg_replace("`<(\/?[oule][mli])>`im","`!--\\1--`",$szoveg);
    $szoveg=preg_replace("`<(\/?h[12345])>`im","`!--\\1--`",$szoveg);
    $szoveg=preg_replace("`<(\/?strong)>`im","`!--\\1--`",$szoveg);
    $szoveg=preg_replace("`<(\/?t[dr])>`im","`!--\\1--`",$szoveg);
    $szoveg=preg_replace("`<(\/?table)>`im","`!--\\1--`",$szoveg);
    $szoveg=preg_replace("`<(\/?t[dr]\s+[\w\/\"\'\=\+\\\?\!\.\|\:\;\,\{\}\[\]\-\@\#\$\%\^\&\*\(\)\~\s]+)>`im","`!--\\1--`",$szoveg);
    $szoveg=preg_replace("`<(\/?table\s+[\w\/\"\'\=\+\\\?\!\.\|\:\;\,\{\}\[\]\-\@\#\$\%\^\&\*\(\)\~\s]+)>`im","`!--\\1--`",$szoveg);
    $szoveg=preg_replace("`<(img\s+[\w\/\"\'\=\+\\\?\!\.\|\:\;\,\{\}\[\]\-\@\#\$\%\^\&\*\(\)\~\s]+)>`im","`!--\\1--`",$szoveg);
    $szoveg=preg_replace("`<([ap]\s+[\w\/\"\'\=\+\\\?\!\.\|\:\;\,\{\}\[\]\-\@\#\$\%\^\&\*\(\)\~\s]+)>`im","`!--\\1--`",$szoveg);
    $szoveg=preg_replace("`\<[\w\/\"\'\=\+\\\?\!\.\|\:\;\,\{\}\[\]\-\@\#\$\%\^\&\*\(\)\~\s]*\>`im","",$szoveg);
    $szoveg=preg_replace("`\`!--([\w\/\"\'\=\+\\\?\!\.\|\:\;\,\{\}\[\]\-\@\#\$\%\^\&\*\(\)\~\s]+)--\``im","<\\1>",$szoveg);
    $szoveg=preg_replace("'<(|/)(span|o:p).*?>'si", "", $szoveg);
    $szoveg=preg_replace  ("' style.*?>'si", ">", $szoveg);
    $szoveg=str_replace("#8211;","-",$szoveg);
    $szoveg=str_replace(chr(132),"\"",$szoveg);
    $szoveg=str_replace(chr(148),"\"",$szoveg);
    $szoveg=str_replace(chr(146),"'",$szoveg);
return $szoveg;
}

function sqlize($s){
	return(str_replace("\'","'",str_replace("'","''",str_replace('"',"&quot;",str_replace("\n","<br />",antikiddie($s))))));
}
function resqlize($s){
	return(str_replace("'","\'",str_replace("''","'",str_replace("&quot;",'"',str_replace("<br />","\n",antikiddie($s))))));
}


function szamlista($db,$act,$url,$name){
	$pre="";
	$egysor=5;
	$szelet=ceil($act/$egysor);
	//echo"<a href=\"".$url.$name."=1><< Első</a>&nbsp;&nbsp;";
	
	echo"<nav>";							
		echo"<div class=\"text-center\">";	
		  echo"<ul class=\"pagination\">";
			
		
	

	echo"<li>";
	  echo"<a href=\"".$url.$name."/1\" aria-label=\"Első&nbsp;\">";
		echo"<span aria-hidden=\"true\">&laquo;</span>";
	  echo"</a>";
	echo"</li>";
	
		$pp=$act-1;if(($act-1)==0){$pp=1;}
		echo "<li><a href=\"".$url.$name."/".($pp)."\" aria-label=\"Előző;\">";
		echo "&lt;&nbsp;&nbsp;";
		echo "</a></li> ";
	
	if ($szelet>1){
		echo "<li><a href=\"".$url.$name."/".(($szelet-2)*$egysor+1)."\">";
		echo "...";
		echo "</a> </li>";
	}
	echo "&nbsp;";
	for ($i=($szelet-1)*$egysor+1;($i<=$db)and($i<$szelet*$egysor+1);$i++){
		if ($i!=$act){
			echo "<li><a href=\"".$url.$name."/".$i."\">";
		}else{
			echo "<li><a href=\"".$url.$name."/".$i."\"><b>";
		}
		echo $i;
		
		if ($i!=$act){
			echo "</a></li> ";
		}else{
			echo "</b></a></li> ";
		}
	}
	echo "&nbsp;";
	if ($szelet<ceil($db/$egysor)){
		echo "<li><a href=\"".$url.$name."/".($szelet*$egysor+1)."\">";
		echo "...";
		echo "</a> </li>";
	}
	if(($i-1)!=$db){
		echo "<li><a href=\"".$url.$name."/".($act+1)."\" aria-label=\"Következő\">";
		echo "&nbsp;&gt;";
		echo "</a></li>";
	//echo  ($i-1)."-".$db;
	
	echo"<li>";
			 echo"<a href=\"".$url.$name."/".($db)."\" aria-label=\"Utolsó\">";
				echo"<span aria-hidden=\"true\">&raquo;</span>";
			  echo"</a>";
			echo"</li>";
		}	
			 echo" </ul>";
		echo"</div>";
	echo"</nav>";
}

function naplo($level,$type,$message,$modul='',$func='',$valamiid='',$sqlins=''){
	$uid=$_SESSION['planetsys']['user_id'];
	$cid=$_SESSION['planetsys']['customer']['id'];
	
	if($uid==''){
		$uid=0;
		$username="System";
		$cid=0;
		$cname="System";
	}
	else{
		$username=$_SESSION['planetsys']['realname'];
		$cname=$_SESSION['planetsys']['customer']['name'];
	}
	if((SYS_LOGLEVEL==0&&$level=='crit') ||
	   (SYS_LOGLEVEL==1&&($level=='crit'||$level=='error')) ||
	   (SYS_LOGLEVEL==2&&($level=='crit'||$level=='error'||$level=='warn')) ||
	   (SYS_LOGLEVEL>2)
	  ){
		$insertlog=db_execute("INSERT INTO ".DB_TABLEPREFIX."logs (rdate,user_id,user_name,customer_id,customer_name,loglevel,logtype,message,ip,url,modul,func,function_dataid,sqlins) 
		VALUES(NOW(),'".$uid."','$username','".$cid."','".$cname."','$level','$type','$message','".$_SERVER['REMOTE_ADDR']."','".$_SERVER['REQUEST_URI']."','".$modul."','".$func."','".$valamiid."','".$sqlins."')");
		if($insertlog!=1){
			if (!$fh = @fopen("planetsys_paniclog.txt", "a")){
				echo "Critical error, system logging is not possible!";
				die;
			}
			if (fwrite($fh, date("Y.m.d H:i:s", strtotime("now")).
				"$uid $username $level $type $message\n") === FALSE){
					echo "Critical error, system logging is not possible!";
					die;
			}
			@fclose($fh);
		}
	   }
}


function limit_text($myvalue,$length,$separator=" "){
    $myvalue=dangChars($myvalue); 
    if(strlen(utf8_decode($myvalue))<=$length)return $myvalue;
    $avalue="";
    $av=split(" ",$myvalue);
    foreach($av as $ava){
        if(strlen($avalue." ".$ava)<=$length){
            $avalue.=$ava." ";
        }else{break;}
    }
    $avalue=substr($avalue,0,-1);
    if($avalue==""){$avalue=substr($av[0],0,$length-3)."...";}
    if(strlen($myvalue)>strlen($avalue)){$avalue.="...";}
    return $avalue;
}

function safeNames($check,$ponte=0){

    if($ponte==1){ 
    $newchars=array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","_",".","-","1","2","3","4","5","6","7","8","9","0");
    }elseif($ponte==2){
    $newchars=array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","1","2","3","4","5","6","7","8","9","0");
    $check=str_replace(' ','',strtolower($check));
        }
    else{ 
    $newchars=array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u",
                    "v","w","x","y","z","_","-","1","2","3","4","5","6","7","8","9","0");
    }
    $safename = "";
    for($j=0;$j<strlen($check);$j++){
        if(in_array($check[$j],$newchars)){ $safename .= $check[$j];}
        else{ $safename .= "_";}
    }
    return $safename;
}

function safe_filename($text){
	$oldchars=array("á","é","í","ó","ö","ő","ú","ü","ű","Á","É","Í","Ó","Ö","Ő","Ú","Ű"," ",",");
    $newchars=array("a","e","i","o","o","o","u","u","u","A","E","I","O","O","O","U","U","-","-");
    $filename=strtolower(str_replace($oldchars,$newchars,$text));
    if (preg_match ("/^.*\\\\(.*)$/", $filename, $regs)){$filename = $regs[1];}
	return safeNames($filename);
}

function crypt_password($clear, $salt=''){
	return md5($clear);
}
function validate_password($p,$p2){
	if(!preg_match("/^[-_\.0-9a-zA-Z]*$/", $p))return eee("Nem megengedett karakterek a jelszóban!");
	if($p!=$p2)return eee("A két jelszó nem egyezik!");
	if(PASS_MINLENGTH > 0 && strlen($p)<PASS_MINLENGTH) return eee("A jelszó hossza minimum ".PASS_MINLENGTH." karakter!");
	if(PASS_MAXLENGTH > 0 && strlen($p)>PASS_MAXLENGTH) return eee("A jelszó hossza maximum ".PASS_MAXLENGTH." karakter!");
	if(PASS_NUMBER > 0 && !preg_match("/^[0-9]*$/", $p)) return eee("A jelszónak számokat is kell tartalmaznia!");
	if(PASS_UDCASE > 0 && !(preg_match("/^[A-Z]*$/", $p)&&preg_match("/^[a-z]*$/", $p))) return eee("A jelszónak kis- és nagybetűket is tartalmaznia kell!");
}

function resizeImage($filename, $max_width, $max_height,$size,$crop="",$csize=""){
	
	if($filename==""){return;}
	if(is_file('inc/_thumbnail.inc.php')){include_once('inc/_thumbnail.inc.php');}
	else{include_once('_thumbnail.inc.php');}
	$gettype=substr($filename,-3);
	if($gettype=="jpg" || $gettype=="JPG"){$fileveg=".jpg";}
	if($gettype=="gif" || $gettype=="GIF"){$fileveg=".gif";}
	if($gettype=="png" || $gettype=="PNG"){$fileveg=".png";}
		if(file_exists($filename)){
			$filename2=substr($filename,0,-4)."_thumb_".$size.$fileveg;
			if (!file_exists($filename2)){
				$thumb = new Thumbnail($filename);
				$thumb->resize($max_width,$max_height);
				if($crop=="1"){$thumb->cropFromCenter($csize);}
				$thumb->save($filename2,100);
			}
		}
		else{
			$twofile="../".$filename;
			if(file_exists($twofile)){
				$filename2=substr($twofile,0,-4)."_thumb_".$size.$fileveg;
				if (!file_exists($filename2)){
					$thumb = new Thumbnail($twofile);
					$thumb->resize($max_width,$max_height);
					if($crop=="1"){	$thumb->cropFromCenter($csize);}
					$thumb->save($filename2,100);
				}
				$filename2=substr($filename2,3);
			}
			else{$filename2="format/intra.jpg";}
		}
    return($filename2);
}

function format_bytes($a_bytes)
{
    if ($a_bytes < 1024) {
        return $a_bytes .' B';
    } elseif ($a_bytes < 1048576) {
        return round($a_bytes / 1024, 2) .' KiB';
    } elseif ($a_bytes < 1073741824) {
        return round($a_bytes / 1048576, 2) . ' MiB';
    } elseif ($a_bytes < 1099511627776) {
        return round($a_bytes / 1073741824, 2) . ' GiB';
    } elseif ($a_bytes < 1125899906842624) {
        return round($a_bytes / 1099511627776, 2) .' TiB';
    } elseif ($a_bytes < 1152921504606846976) {
        return round($a_bytes / 1125899906842624, 2) .' PiB';
    } elseif ($a_bytes < 1180591620717411303424) {
        return round($a_bytes / 1152921504606846976, 2) .' EiB';
    } elseif ($a_bytes < 1208925819614629174706176) {
        return round($a_bytes / 1180591620717411303424, 2) .' ZiB';
    } else {
        return round($a_bytes / 1208925819614629174706176, 2) .' YiB';
    }
}
function cutgraphname($filename,$size){
	if($size!="orig"){
		$gettype=substr($filename,-3);
		if($gettype=="jpg" || $gettype=="JPG"){
			$fileveg=".jpg";
		}
		if($gettype=="gif" || $gettype=="GIF"){
			$fileveg=".gif";
		}
		if($gettype=="png" || $gettype=="PNG"){
			$fileveg=".png";
		}
		$filename2=substr($filename,0,-4)."_thumb_".$size.$fileveg;
	}
	else{
		$filename2=$filename;
	}
	return $filename2;
}


function idchange($tablename,$mezo,$defval){
	$Fields=db_row("SELECT $mezo FROM $tablename WHERE id = '$defval'");
	$rex=$Fields[$mezo];
	return $rex;
}

function ezDate($d) {
        $ts = time() - strtotime(str_replace("-","/",$d));
        
        if($ts>31536000) $val = round($ts/31536000,0).'&nbsp;éve';
        else if($ts>2419200) $val = round($ts/2419200,0).'&nbsp;hónapja';
        else if($ts>604800) $val = round($ts/604800,0).'&nbsp;hete';
        else if($ts>86400) $val = round($ts/86400,0).'&nbsp;napja';
        else if($ts>3600) $val = round($ts/3600,0).'&nbsp;órája';
        else if($ts>60) $val = round($ts/60,0).'&nbsp;perce';
        else $val = $ts.'&nbsp;mperce';
        
       # if($val>1) $val .= 's';
        return $val;
}
function ezrDate($d) {
	if(strtotime(str_replace("-","/",$d))>time()){
        $ts =strtotime(str_replace("-","/",$d)) - time() ;
		if($ts>31536000) $val = round($ts/31536000,0).'&nbsp;év múlva';
        else if($ts>2419200) $val = round($ts/2419200,0).'&nbsp;hónap múlva';
        else if($ts>604800) $val = round($ts/604800,0).'&nbsp;hét múlva';
        else if($ts>86400) $val = round($ts/86400,0).'&nbsp;nap múlva';
        else if($ts>3600) $val = round($ts/3600,0).'&nbsp;óra múlva';
        else if($ts>60) $val = round($ts/60,0).'&nbsp;perc múlva';
        else $val = $ts.'&nbsp;mperc múlva';

	}
	else{
		 $ts = time() - strtotime(str_replace("-","/",$d));
        
        if($ts>31536000) $val =' lejárt '.round($ts/31536000,0).'&nbsp;éve';
        else if($ts>2419200) $val = ' lejárt '.round($ts/2419200,0).'&nbsp;hónapja';
        else if($ts>604800) $val = ' lejárt '.round($ts/604800,0).'&nbsp;hete';
        else if($ts>86400) $val = ' lejárt '.round($ts/86400,0).'&nbsp;napja';
        else if($ts>3600) $val = ' lejárt '.round($ts/3600,0).'&nbsp;órája';
        else if($ts>60) $val = ' lejárt '.round($ts/60,0).'&nbsp;perce';
        else $val = ' lejárt '.$ts.'&nbsp;mperce';
	}

        return $val;
}
function pastorora($ts){
	if($ts>31536000) $val =' tranzakciós idő: '.round($ts/31536000,0).'&nbsp;év';
        else if($ts>2419200) $val = ' tranzakciós idő: '.round($ts/2419200,0).'&nbsp;hónap';
        else if($ts>604800) $val = ' tranzakciós idő: '.round($ts/604800,0).'&nbsp;hét';
        else if($ts>86400) $val = ' tranzakciós idő: '.round($ts/86400,0).'&nbsp;nap';
        else if($ts>3600) $val = ' tranzakciós idő: '.round($ts/3600,0).'&nbsp;óra';
        else if($ts>60) $val = ' tranzakciós idő: '.round($ts/60,0).'&nbsp;perc';
        else $val = ' tranzakciós idő: '.$ts.'&nbsp;mperc';
        return $val;
}
function bgtoborder($mycolor){
      $red=hexdec(substr($mycolor,0,2));
      $grn=hexdec(substr($mycolor,2,2));
      $blu=hexdec(substr($mycolor,4,2));
      $red=$red-32;
      if($red<0)$red=0;
      $grn=$grn-32;
      if($grn<0)$grn=0;
      $blu=$blu-32;
      if($blu<0)$blu=0;
      $red=dechex($red);
      $grn=dechex($grn);
      $blu=dechex($blu);
      if(strlen($red)==1)$red="0".$red;
      if(strlen($grn)==1)$grn="0".$grn;
      if(strlen($blu)==1)$blu="0".$blu;
      return $red.$grn.$blu;
 }
  
function storemail($cimzett,$texttartalom,$targy){
	db_execute("insert into mail_quene (customer_id,fromemail,fromname,email,subject,txtcontent,sendtime,status) values ('".$_SESSION['planetsys']['customer']['id']."','".$_SESSION['planetsys']['customer']['customeremail']."','".$_SESSION['planetsys']['customer']['emailfromname']."','".$cimzett."','".$targy."','".$texttartalom."',NOW(),'1')");	
} 

function utf7_to_utf8($str){
  $Index_64 = array(
      -1,-1,-1,-1, -1,-1,-1,-1, -1,-1,-1,-1, -1,-1,-1,-1,
      -1,-1,-1,-1, -1,-1,-1,-1, -1,-1,-1,-1, -1,-1,-1,-1,
      -1,-1,-1,-1, -1,-1,-1,-1, -1,-1,-1,62, 63,-1,-1,-1,
      52,53,54,55, 56,57,58,59, 60,61,-1,-1, -1,-1,-1,-1,
      -1, 0, 1, 2,  3, 4, 5, 6,  7, 8, 9,10, 11,12,13,14,
      15,16,17,18, 19,20,21,22, 23,24,25,-1, -1,-1,-1,-1,
      -1,26,27,28, 29,30,31,32, 33,34,35,36, 37,38,39,40,
      41,42,43,44, 45,46,47,48, 49,50,51,-1, -1,-1,-1,-1
  );

  $u7len = strlen($str);
  $str = strval($str);
  $p = $err = '';

  for ($i=0; $u7len > 0; $i++, $u7len--){
    $u7 = $str[$i];
    if ($u7 == '&'){
      $i++;
      $u7len--;
      $u7 = $str[$i];
      if ($u7len && $u7 == '-'){$p .= '&';  continue;}
      $ch = 0;
      $k = 10;
      for (; $u7len > 0; $i++, $u7len--){
        $u7 = $str[$i];
        if ((ord($u7) & 0x80) || ($b = $Index_64[ord($u7)]) == -1){break;}  
        if ($k > 0){$ch |= $b << $k;$k -= 6;}
        else{
          $ch |= $b >> (-$k);
          if ($ch < 0x80){
            if(0x20 <= $ch && $ch < 0x7f){return $err; }
			$p .= chr($ch);
          }
          elseif($ch < 0x800){
            $p .= chr(0xc0 | ($ch >> 6));
            $p .= chr(0x80 | ($ch & 0x3f));
          }
          else{
            $p .= chr(0xe0 | ($ch >> 12));
            $p .= chr(0x80 | (($ch >> 6) & 0x3f));
            $p .= chr(0x80 | ($ch & 0x3f));
          }

          $ch = ($b << (16 + $k)) & 0xffff;
          $k += 10;
        }
      }
      if ($ch || $k < 6){return $err;} 
      if (!$u7len || $u7 != '-'){return $err;}
      if ($u7len > 2 && $str[$i+1] == '&' && $str[$i+2] != '-'){return $err;}
    }
    elseif(ord($u7) < 0x20 || ord($u7) >= 0x7f){return $err;}
    else{$p .= $u7;}
  }

  return $p;
}


function dot($inn){
	return str_replace(",",".",$inn);
}

function gomail($cimzett,$tartalom,$targy){
	
	date_default_timezone_set('Etc/UTC');

		require_once 'admin/phpmailer/PHPMailerAutoload.php';

		$mail = new PHPMailer();
		$mail->CharSet = 'UTF-8';
		$mail->IsSMTP(); // telling the class to use SMTP
		
		$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
												   // 1 = errors and messages
												   // 2 = messages only
		$mail->SMTPAuth   = true;                  // enable SMTP authentication
		$mail->SMTPSecure = "tls";                 // sets the prefix to the servier
		$mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
		$mail->Port       = 587;                   // set the SMTP port for the GMAIL server
		$mail->Username   = "info@hrportal.hu";  // GMAIL username
		$mail->Password   = "K35Vd2";            // GMAIL password

		$mail->setFrom('info@hrportal.hu', 'HRPortal.hu');

		//Set who the message is to be sent to
		$mail->addAddress($cimzett);
		//Set the subject line
		$mail->Subject = $targy;

		//Replace the plain text body with one created manually
		$mail->Body = $tartalom;
		
		

		//send the message, check for errors
		if (!$mail->send()) {
			//echo "Mailer Error: " . $mail->ErrorInfo;
		} else {
			//echo "Message sent!";
		}
	}
function gomail_info($cimzett,$tartalom,$htmltartalom,$targy){
	date_default_timezone_set('Etc/UTC');
	require_once 'admin/phpmailer/PHPMailerAutoload.php';
	$mail = new PHPMailer();
	$mail->CharSet = 'UTF-8';
//	$mail->IsSMTP(); // telling the class to use SMTP
	$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
											   // 1 = errors and messages
											   // 2 = messages only
//	$mail->SMTPAuth   = true;                  // enable SMTP authentication
//	$mail->SMTPSecure = "tls";                 // sets the prefix to the servier
//	$mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
//	$mail->Port       = 587;                   // set the SMTP port for the GMAIL server
//	$mail->Username   = "info@hrportal.hu";  // GMAIL username
//	$mail->Password   = "K35Vd2";            // GMAIL password

	$mail->setFrom('info@hrportal.hu', 'HRPortal.hu');

	//Set who the message is to be sent to
	$mail->addAddress($cimzett);
	//Set the subject line
	$mail->Subject = $targy;

	//Replace the plain text body with one created manually
	$mail->AltBody = $tartalom;
		$mail->MsgHTML($htmltartalom);
	
	

	//send the message, check for errors
	if (!$mail->send()) {
		echo "Mailer Error: " . $mail->ErrorInfo;
	} else {
		echo "Message sent!";
	}
}
	
	
function generateCode($length=15) {
	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";
	$code = "";
	$clen = strlen($chars) - 1;  
	while (strlen($code) < $length) {
		$code .= $chars[mt_rand(0,$clen)];  
	}
	return $code;
}
function huf($hufstring){
 $mylen  = strlen($hufstring);
 $myrest = ($mylen %3);
 if($myrest>0)
    {
	$outhufstring = substr($hufstring,0,$myrest)." ";
    }
 while($myrest<$mylen)
    {
     $outhufstring.=substr($hufstring,$myrest,3)." ";
     $myrest+=3;
    }
 return substr($outhufstring,0,strlen($outhufstring)-1);
}

function xl2timestamp($xl_date){
	return mktime(0,0,0,1,$xl_date,1900)-86400;
}

function borderedparse($articleb){
	
	$pos = strpos($articleb, "[b1]");
	if ($pos === false) {} 
	else {
		$first="<div id='inbbbox' class='bbox-outer-50'><div id='inlins' class='bbox-grey'>";
		$articleb=str_replace("[b1]",$first,$articleb);
	}
	
	$pos = strpos($articleb, "[b2]");
	if ($pos === false) {} 
	else {
		$first="<div id='inbbbox' style='box-outer-100'><div id='inlins' class='bbox-grey'>";
		$articleb=str_replace("[b2]",$first,$articleb);
	}
	
	$pos = strpos($articleb, "[b3]");
	if ($pos === false) {} 
	else {
		$first="<div id='inbbbox' class='bbox-outer-50'><div id='inlins' class='bbox-blue'>";
		$articleb=str_replace("[b3]",$first,$articleb);
	}
	$pos = strpos($articleb, "[b4]");
	if ($pos === false) {} 
	else {
		$first="<div id='inbbbox' class='box-outer-100'><div id='inlins' class='bbox-blue'>";
		$articleb=str_replace("[b4]",$first,$articleb);
	}
	
	$pos = strpos($articleb, "[b5]");
	if ($pos === false) {} 
	else {
		$first="<div id='inbbbox' class='bbox-outer-50'><div id='inlins' class='bbox-green'>";
		$articleb=str_replace("[b5]",$first,$articleb);
	}
	$pos = strpos($articleb, "[b6]");
	if ($pos === false) {} 
	else {
		$first="<div id='inbbbox' class='box-outer-100'><div id='inlins' class='bbox-green'>";
		$articleb=str_replace("[b6]",$first,$articleb);
	}
	$pos = strpos($articleb, "[b7]");
	if ($pos === false) {} 
	else {
		$first="<div id='inbbbox' class='bbox-outer-50'><div id='inlins' class='bbox-green'>";
		$articleb=str_replace("[b7]",$first,$articleb);
	}
	$last="</div></div>";
	$articleb=str_replace("[b9]",$last,$articleb);
	return $articleb;

}
function pictureparse($articleb){
   //$result = mysql_query("SET NAMES latin2");
		preg_match_all("/\[img(.*?)\]/e",$articleb,$out,PREG_PATTERN_ORDER);
		
		if(count($out[1])>0){	
			for($i=0;$i<count($out[1]);$i++){
				$myelem=$out[1][$i];
				if($myelem!=""){
					$picparams=explode(",",$myelem);
					
					if(count($picparams)<"3"){
						$mimg=db_row("select * from img where id='".$picparams[0]."'");
						if(isset($mimg['id'])){
							list($widtha, $heighta) = getimagesize("images/uploaded/img/".$mimg['id'].".jpg");
							if($widtha>"236" && $widtha<"528"){
								$thumb=resizeimage("images/uploaded/img/".$mimg['id'].".jpg","236","236","236");	
							}
							elseif($widtha>="528"){
								$thumb=resizeimage("images/uploaded/img/".$mimg['id'].".jpg","528","528","528");	
							}
							else{
								$thumb="images/uploaded/img/".$mimg['id'].".jpg";
		
							}
							$img_table="<center><div class='kep_ala' style='float: ".$mimg['align'].";margin-bottom:10px;width:235px;'><a class=\"fancybox-thumbs\" data-fancybox-group=\"thumb\" href=\"images/uploaded/img/".$mimg['id'].".jpg\" title=\"".$mimg['description']."\"><img  src='$thumb'  style='border: 1px solid black;margin:10px;'></a>";
							if($mimg['description']!=""){
								$img_table.="<div style=\"clear:both;height:1px;\"></div><i><b>".$mimg['description']."</b></i>";
							}
							$img_table.="</div></center>";
						}
						else{
							$img_table="";
						}
					}
					elseif(count($picparams)=="3"){
						$gennum=generatecode(5);
						$mimg=db_row("select * from img where id='".$picparams[0]."'");
						if($picparams[1]=="L"){
							$l="left";	
						}
						elseif($picparams[1]=="R"){
							$l="right";
						}

							$img_table="";
							$my_image = array_values(getimagesize("images/uploaded/img/".$mimg['id'].".jpg"));
							list($width, $height, $type, $attr) = $my_image;
							
							//$resp=" img-smaller ";if($width>"320"){$resp=" img-responsive ";}
						if($mimg['description']!=""){
							$res="";if($picparams[2]=="600" || $picparams[2]=="528" || $width>"320" ){$res=" class=\"img-responsive\" ";}
							$img_table.="<div style=\"clear:both;height:1px;\"></div><span class=\"img-".$l." img-title \">"; 
							   $img_table.="<img alt=\"".$mimg['altag']."\" src=\"images/uploaded/img/".$mimg['id'].".jpg\" ".$res."></img>";
							  $img_table.="<i>".$mimg['description']."</i>";
							 $img_table.="</span><div style=\"clear:both;height:1px;\"></div>";
						}
						else{
							$res="";if( $picparams[2]=="600" || $picparams[2]=="528" || $width>"320" ){$res=" class=\"img-responsive\" ";}
							$img_table.="<div style=\"clear:both;height:1px;\"></div><span class=\"img-".$l." \">"; 
							   $img_table.="<div style=\"clear:both;height:1px;\"></div><img alt=\"".$mimg['altag']."\" src=\"images/uploaded/img/".$mimg['id'].".jpg\" ".$res."></img>";
							 $img_table.="</span><div style=\"clear:both;height:1px;\"></div>";
						} 
						
					}
					else{
						$mimg=db_row("select * from img where id='".$picparams[0]."'");
						$gennum=generatecode(5);
						$igazitas=$picparams[count($picparams)-2];
						$szelesseg=$picparams[count($picparams)-1];
						if($igazitas=="L"){
							$l="left";	
						}
						elseif($igazitas=="R"){
							$l="right";
						}
						
						
							$md="";
							for($r=0;$r<count($picparams)-2;$r++){
								$mimga=db_row("select * from img where id='".$picparams[$r]."'");
								$md.=$mimga['id']."_";	
							}
							$md=substr($md,0,-1);
						
						$img_table="<div  class='kep_ala' style='float:".$l.";vertical-align:top;margin-bottom:10px;margin-right:5px;width:235px;'>
						
						<a class=\"fancybox-thumbs\" data-fancybox-group=\"thumb\" href=\"images/uploaded/img/".$mimg['id'].".jpg\" title=\"".$mimg['description']."\"><img src='images/uploaded/img/".$mimg['id']."_thumb_".$szelesseg.".jpg' border='0' alt='".$mimg['altag']."' style=\"margin-top:0px;padding-top:0px;\"></a>";
							if($mimg['description']!=""){
									$img_table.="<center><i>".$mimg['description']."</i></center>";
								}
							
							$img_table.="</div>";
						
					}
					$articleb=preg_replace("/\[img".$myelem."\]/e","\$img_table",$articleb);
				}			
			}
			return $articleb;
		}
		else{
		return $articleb;	
		}	
}
function albumparse($articleb){
   
		preg_match_all("/\[imagealbum=(.*?)\]/e",$articleb,$out,PREG_PATTERN_ORDER);
		
		if(count($out[1])>0){	
			//print_r($out);
			for($i=0;$i<count($out[1]);$i++){
				
				if($out[1][$i]!=""){
				$myelem=$out[1][$i];
				if(substr($myelem,0,1)=="R"){
					$fl=" float:right;width:260px;margin-left:5px;";	
				}
				else{
					$fl=" float:left;width:260px;margin-right:5px;";	
				}
				$pic=db_row("select * from albums_pics where id='".substr($myelem,1)."'");
				$mypath="admin/imgalbums/albums/".$pic['albums_id']."/";
				$thumb2=resizeimage($mypath.$pic['filename'],515,315,"b","","");
				$pics=db_all("select * from albums_pics where  albums_id='".$pic['albums_id']."' order by id DESC");
				$mutat="";
				$img_table="<div style=\"".$fl."\">";
				for($z=0;$z<count($pics);$z++){
					
					//$img_table.=$pics[$z]['id']."==".$pic['id']."<br />";
					if($pics[$z]['id']!=$pic['id']){
						$mutat.="<a class=\"fancybox\" rel=\"group\" href=\"".$mypath.$pics[$z]['filename']."\" title=\"".$pics[$z]['description']."\"></a>";	
					}
					//$img_table.=$mutat;
						
				}
				$img_table.="<a class=\"fancybox\" rel=\"group\" href=\"".$mypath.$pic['filename']."\" title=\"".$pic['description']."\"><img src='".$mypath.$pic['filename']."' style='border: 0px solid ;cursor:pointer;width:260px;' width=\"260\" alt=\"".$pic['title']."\"/></a><center><i><b>".$pic['description']."</b></i></center>";
				
				
				$img_table.=$mutat;
				$img_table.="</div>";
				$articleb=preg_replace("/\[imagealbum=".$myelem."\]/e","\$img_table",$articleb);
			}
		}
			return $articleb;
		}
		else{
			return $articleb;	
		}	
}
function readDatabase($filename){
	//echo"$filename";
    $data = implode("", file($filename));
	
    $parser = xml_parser_create();
    xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
    xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
    xml_parse_into_struct($parser, $data, $values, $tags);
    xml_parser_free($parser);

    foreach ($tags as $key=>$val){
   
        if ($key == "item") {
			$elemek = $val;
            for ($i=0; $i < count($elemek); $i+=2) {
				$offset = $elemek[$i] + 1;
                $len = $elemek[$i + 1] - $offset;
                $mvalues=array_slice($values, $offset, $len);
                $elem="";
				//print_r($mvalues);
                for ($h=0; $h < count($mvalues); $h++) {
                	if($mvalues[$h]["tag"]=="pubDate"){
                		$elem['unixtime'] = strtotime(substr($mvalues[$h]["value"],5,-6));
                	}
					//echo $mvalues[$h]['value']."\n";
                	if(isset($mvalues[$h]['value'])){
						$elem[$mvalues[$h]["tag"]] =$mvalues[$h]['value'];
					}
					else{
							$elem[$mvalues[$h]["tag"]] ="";
					}
                }
               	$tdb[] =$elem; 
            }
        } else {
            continue;
        }
    }
    return $tdb;
}
function fine_date_digit($date,$onlydate=false){
  if ($date=="0000-00-00 00:00:00"){
   return "- nincs adat -";
  } 
   
  $myrow=getdate(strtotime($date));
  
  if ($myrow['mon']<10) $myrow['mon']="0".$myrow['mon'];
  if ($myrow['mday']<10) $myrow['mday']="0".$myrow['mday'];
  
  $dat=$myrow['year'].". ".$myrow['mon'].". ".$myrow['mday'].".";
  if ($onlydate==false)
   {
    if ($myrow['hours']!=0 || $myrow['minutes']!=0 || $myrow['seconds']!=0)
     $dat.=" ".(strlen($myrow['hours'])==1?"0".$myrow['hours']:$myrow['hours']).":".(strlen($myrow['minutes'])==1?"0".$myrow['minutes']:$myrow['minutes']).":".(strlen($myrow['seconds'])==1?"0".$myrow['seconds']:$myrow['seconds']);
   }
  return($dat);
 }   

 function writestatic($myid){
	//echo $myid;
	$fulllink=db_one("select link from links where art_id = '".$myid."' and def=1");
	$links=explode("/",$fulllink);
	if(!is_file("../static/".$links[count($links)-1])){
		@touch("../static/".$links[count($links)-1]);
		@chmod("../static/".$links[count($links)-1],0777);
	}
	$d=@join("",file("http://new.hrportal.hu/genstat/".$myid));
	if ($d!=""){
		$f=fopen("../static/".$links[count($links)-1],"w+");
		fwrite($f,$d);
		fclose($f);
	} 
}
function showgbanner($banner,$align="center",$border="0",$marginleft="0",$marginright="0",$margintop="0",$marginbottom="0"){
	$adsinteractive=1;
	//echo $banner;
	
	if($banner=="HRPortal_hu_cimlap_fent"){
		//echo"<div style=\"border:1px solid #ccc\">";
		echo"<div >";
		?>
		<!-- /22652647/Hrportal_728//970x90//250_nyito_superleaderbord_top_1 -->
		<div id='div-gpt-ad-1454067750572-17'>
		<script type='text/javascript'>
		googletag.cmd.push(function() { googletag.display('div-gpt-ad-1454067750572-17'); });
		</script>
		</div>
		<?
		echo"</div>";
	}
	elseif($banner=="HRPortal_hu_aloldalak_728_top"){
		echo"<div >";
		?>
		<!-- /22652647/Hrportal_728//970x90//250_cikk_superleaderbord_top_1 -->
		<div id='div-gpt-ad-1454067750572-16'>
		<script type='text/javascript'>
		googletag.cmd.push(function() { googletag.display('div-gpt-ad-1454067750572-16'); });
		</script>
		</div>
		<?
		echo"</div>";
	}
	elseif($banner=="HRPortal_hu_aloldalak_468_kozep"){
		echo"<div >";
		?>
			<!-- /22652647/Hrportal_468x60//120_cikk_normal_content_1 -->
		<div id='div-gpt-ad-1454067750572-12'>
		<script type='text/javascript'>
		googletag.cmd.push(function() { googletag.display('div-gpt-ad-1454067750572-12'); });
		</script>
		</div>
		<?
		echo"</div>";
	}
	elseif($banner=="HRPortal_hu_cimlap_kozep"){
		echo"<div >";
		?>
		<!-- /22652647/Hrportal_468x60//120_nyito_normal_content_1 -->
		<div id='div-gpt-ad-1454067750572-14'>
		<script type='text/javascript'>
		googletag.cmd.push(function() { googletag.display('div-gpt-ad-1454067750572-14'); });
		</script>
		</div>
		<?
		echo"</div>";
	}
	elseif($banner=="HRPortal_hu_cimlap_jobb1"){
		echo"<div style=\"float:right;\">";
		?>
		
		<!-- /22652647/Hrportal_300x250//600_nyito_rectangle_right_1 -->
		<div id='div-gpt-ad-1454067750572-9'>
		<script type='text/javascript'>
		googletag.cmd.push(function() { googletag.display('div-gpt-ad-1454067750572-9'); });
		</script>
		</div>

		<?
		echo"</div>";
	}
	elseif($banner=="HRPortal_hu_cimlap_jobb2"){
		echo"<div style=\"float:right;\">";
		?>
		
		<!-- /22652647/Hrportal_300x250//600_nyito_rectangle_right_2 -->
		<div id='div-gpt-ad-1454067750572-10'>
		<script type='text/javascript'>
		googletag.cmd.push(function() { googletag.display('div-gpt-ad-1454067750572-10'); });
		</script>
		</div>

		<?
		echo"</div>";
	}
	elseif($banner=="HRPortal_hu_cimlap_jobb3"){
		echo"<div style=\"float:right;\">";
		?>
		
		<!-- /22652647/Hrportal_300x250//600_nyito_rectangle_right_3 -->
		<div id='div-gpt-ad-1454067750572-11'>
		<script type='text/javascript'>
		googletag.cmd.push(function() { googletag.display('div-gpt-ad-1454067750572-11'); });
		</script>
		</div>
		<?
		echo"</div>";
	}
	elseif($banner=="HRPortal_hu_aloldalak_jobb1"){
		echo"<div style=\"float:right;\">";
		?>
		
		<!-- /22652647/Hrportal_300x250//600_cikk_rectangle_right_1 -->
		<div id='div-gpt-ad-1454067750572-6'>
		<script type='text/javascript'>
		googletag.cmd.push(function() { googletag.display('div-gpt-ad-1454067750572-6'); });
		</script>
		</div>
		<?
		echo"</div>";
	}
	elseif($banner=="HRPortal_hu_aloldalak_jobb2"){
		echo"<div style=\"float:right;\">";
		?>
			<!-- /22652647/Hrportal_300x250//600_cikk_rectangle_right_2 -->
		<div id='div-gpt-ad-1454067750572-7'>
		<script type='text/javascript'>
		googletag.cmd.push(function() { googletag.display('div-gpt-ad-1454067750572-7'); });
		</script>
		</div>
		<?
		echo"</div>";
	}
	elseif($banner=="HRPortal_hu_aloldalak_jobb3"){
		echo"<div style=\"float:right;\">";
		?>
		
		<!-- /22652647/Hrportal_300x250//600_cikk_rectangle_right_3 -->
		<div id='div-gpt-ad-1454067750572-8'>
		<script type='text/javascript'>
		googletag.cmd.push(function() { googletag.display('div-gpt-ad-1454067750572-8'); });
		</script>
		</div>
		<?
		echo"</div>";
	}
	elseif($banner=="HRPortal_hu_aloldalak_jobb4"){
		echo"<div style=\"float:right;\">";
		?>
		
	<!-- /22652647/Hrportal_300x250//600_cikk_rectangle_right_4 -->
		<div id='div-gpt-ad-1454067750572-18'>
		<script type='text/javascript'>
		googletag.cmd.push(function() { googletag.display('div-gpt-ad-1454067750572-18'); });
		</script>
		</div>
		<?
		echo"</div>";
	}
	else{
		
		echo"<div style=\"clear:both;height:5px;\"></div>";	
		if($border==1){
				echo"<div style=\"border:1px solid #ccc\">";	
		}
		elseif($banner=="HRPortal_hu_cimlap_fentaa"){
					?>
				
				<!--  Begin Rubicon Project Tag -->
				<!--  Site: hrportal.hu   Zone: munkajog   Size: Leaderboard  -->
				<script language="JavaScript" type="text/javascript">
				rp_account   = '14228';
				rp_site      = '74370';
				rp_zonesize  = '352092-2';
				rp_adtype    = 'js';
				rp_smartfile = '[SMART FILE URL]';
				</script>
				<script type="text/javascript" src="http://ads.rubiconproject.com/ad/14228.js"></script>
				<!--  End Rubicon Project Tag -->
			<div style="clear:both;height:10px;"></div>
			<?
		}
		else{	
			if($align=="center"){
				
			?>
				<center><script type='text/javascript'> 
				GA_googleAddSlot("ca-pub-2281350152988337", "<?=$banner;?>");
				GA_googleFetchAds();
				GA_googleFillSlot("<?=$banner;?>");
				</script></center>
				
			<?
			}
			if($align=="right"){
				?>
				<span style="float:right;border:1px solid #ccc;"><script type='text/javascript'> 
				GA_googleAddSlot("ca-pub-2281350152988337", "<?=$banner;?>");
				GA_googleFetchAds();
				GA_googleFillSlot("<?=$banner;?>");
				</script></span>
				
				<?
			}	
			if($align=="left"){
				?>
				<span style="float:left;border:1px solid #ccc;margin-left:<?=$marginleft;?>px;margin-right:<?=$marginright;?>px;margin-top:<?=$margintop;?>px;"><script type='text/javascript'> 
				GA_googleAddSlot("ca-pub-2281350152988337", "<?=$banner;?>");
				GA_googleFetchAds();
				GA_googleFillSlot("<?=$banner;?>");
				</script></span>
				
				<?
			}	
		}
		if($border==1){
			echo"</div>";
		}
	}
}

function showgbanner2($banner,$align="center",$border=0){
	echo"<div style=\"clear:both;height:1px;\"></div>";
	echo"<div  "; 
	if($border==1){
		echo" style=\"border:1px solid #ccc\" ";
	}
	echo">";
	
	if($banner=="HRPortal_minden_oldal_jobb01000" ){
		?>
			<span style="float:right;"><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0" width="250" height="100">
					<param name="movie" value="images/rovatszponzor/cegos/250_100_link.swf" />
					<param name="quality" value="best" />
					<param name="menu" value="true" />
					<param name="allowScriptAccess" value="sameDomain" />
					<embed src="images/rovatszponzor/cegos/250_100_link.swf" quality="best" menu="true" width="250" height="100" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" allowScriptAccess="sameDomain" />
				</object></span>
				
		<?
	}
	else{
		if($align=="right"){echo"<span style=\"float:right;\">";}
		elseif($align=="left"){echo"<span style=\"float:left;\">";}
		else{echo"<center>";}
		
			echo"<script type='text/javascript'> 
				GA_googleAddSlot(\"ca-pub-2281350152988337\", \"".$banner."\");
				GA_googleFetchAds();
				GA_googleFillSlot(\"".$banner."\");
			</script>";
		if($align=="right" || $align=="left"){echo"</span>";}
		else{echo"</center>";}
	}			
	echo"<div style=\"clear:both;height:1px;\"></div>";
	echo"</div>";
	
	
}	


function gettagad($article_id,$mlimit){
	$fnum=$article_id;
	$belekeret=" ";
	$cuccok=db_all("select * from tag_ads where status=1 and finish > CURDATE() order by RAND() ");	
		$mc=array();
		$aa=array();
		$m=0;
		for($l=0;$l<count($cuccok);$l++){
			if(in_array(md5($cuccok[$l]['code']),$mc)){
				$vane=db_one("select at.id from articles_tags at left join tags t on (at.tag_id=t.id)  where t.tagname='".$cuccok[$l]['name']."' and at.art_id='".$fnum."'");
				if($vane>0){
					db_execute("insert into tag_ads_counter (ta_id,art_id,visit_ip) values('".$cuccok[$l]['id']."','".$fnum."','".$_SERVER['REMOTE_ADDR']."')");
				}
			}
			else{
				$vane=db_one("select at.id from articles_tags at left join tags t on (at.tag_id=t.id)  where t.tagname='".$cuccok[$l]['name']."' and at.art_id='".$fnum."'");
				if($vane>0){
					$mc[]=md5($cuccok[$l]['code']);
					$aa[$m]['body']=$cuccok[$l]['body'];
					$aa[$m]['bc']=$cuccok[$l]['boxcolor'];
					db_execute("insert into tag_ads_counter (ta_id,art_id,visit_ip) values('".$cuccok[$l]['id']."','".$fnum."','".$_SERVER['REMOTE_ADDR']."')");
					$m++;
				}
			}
		}
		if(isset($mc[0])){	
			$belekeret.="<div style=\"margin-right:4px;\">[b".$aa[0]['bc']."]".$aa[0]['body']."[b9]<br />";
		}
		if(isset($aa[1]['body']) && $aa[1]['body']!="" && !in_array($fnum,$t100) && $mlimit==2){
			$belekeret.="[b".$aa[1]['bc']."]".$aa[1]['body']."[b9]<br />";
		}
		if(isset($mc[0])){
			$belekeret.="</div>";
		}	
	return $belekeret;
}	

function getartpic($art_id,$size=1){
	$mypr=db_row("select small_pic,large_pic,main_pic from articles where id='".$art_id."'");
	if($mypr['main_pic']!=""){
		$mp=$mypr['main_pic'];	
	}
	elseif($mypr['large_pic']!=""){
		$mp=$mypr['large_pic'];	
	}
	
	elseif($mypr['small_pic']!=""){
		$mp=$mypr['small_pic'];	
	}
return $mp;
}
function generatesponsortitle($page){
	if($page=="elbocsatas_leepites"){
			echo"<img src=\"images/rovatszponzor/kozak_sponzor_logo.jpg\" style=\"float:right;margin-top:10px;margin-right:5px;\"/>";
	}
	elseif($page=="interim_management"){
			echo"<img src=\"images/rovatszponzor/hammel_logo-60x20_02.jpg\" style=\"float:right;margin-top:10px;margin-right:5px;\"/>";
	}
	elseif($page=="hr_informatika"){
			echo"<img src=\"images/rovatszponzor/nexon_kisrovat.jpg\" style=\"float:right;margin-top:10px;margin-right:5px;\"/>";
	}
	elseif($page=="diakmunka"){
			echo"<img src=\"/images/banner/prohuman/prohuman_logo_cikkek.jpg\" style=\"float:right;margin-top:5px;margin-right:5px;height:20px;	\"/>";
	}
	elseif($page=="trening"){
			echo"<img src=\"images/rovatszponzor/Residence_hotels_rovatkep_60x20.jpg\" style=\"float:right;margin-top:10px;margin-right:5px;\"/>";
	}
	elseif($page=="e-learning"){
			echo"<img src=\"images/rovatszponzor/rovatsz_60x20.jpg\" style=\"float:right;margin-top:10px;margin-right:5px;\"/>";
	}
	else{
		echo"";
	}
	
}
function matekize($hh){
	$rep=str_replace(",",".",$hh);
return $rep;	
}
?>
