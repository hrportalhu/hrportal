<?
if($PAGE=="mainpage"){
?>
<script type='text/javascript'>
    (function() {
    var useSSL = 'https:' == document.location.protocol;
    var src = (useSSL ? 'https:' : 'http:') +
    '//www.googletagservices.com/tag/js/gpt.js';
    document.write('<scr' + 'ipt src="' + src + '"></scr' + 'ipt>');
    })();
    </script>
    <script type='text/javascript'>
    var mapping = googletag.sizeMapping().
    addSize([1216, 200], [[970, 90], [970, 250], [728, 90]]).
    addSize([1010, 200], [728, 90]).
    addSize([534, 200], [468, 60]).
    addSize([0, 0], [320, 50]).
    build();
    slot1a = googletag.defineSlot('/5297488/HRP001', [[970, 250], [970, 90], [468, 60], [320, 50], [728, 90]], 'div-gpt-ad-1461936145403-0').defineSizeMapping(mapping).addService(googletag.pubads());
    slot1b = googletag.defineSlot('/5297488/HRP002', [[970, 250], [970, 90], [468, 60], [320, 50], [728, 90]], 'div-gpt-ad-1461936145403-1').defineSizeMapping(mapping).addService(googletag.pubads());
    var mapping2 = googletag.sizeMapping().
    addSize([1216, 200], [[640, 360], [728, 90], [468, 60], [468, 120]]).
    addSize([564, 200], [[468, 120], [468, 60]]).
    addSize([0, 0], [300, 250]).
    build();
    slot2a = googletag.defineSlot('/5297488/HRP003', [[468, 250], [468, 60], [640, 360]], 'div-gpt-ad-1461936145403-2').defineSizeMapping(mapping2).addService(googletag.pubads());
    slot2b = googletag.defineSlot('/5297488/HRP004', [[468, 250], [468, 60], [640, 360]], 'div-gpt-ad-1461936145403-3').defineSizeMapping(mapping2).addService(googletag.pubads());
    slot2c = googletag.defineSlot('/5297488/HRP005', [[468, 250], [468, 60], [640, 360]], 'div-gpt-ad-1461936145403-4').defineSizeMapping(mapping2).addService(googletag.pubads());
    slot2d = googletag.defineSlot('/5297488/HRP006', [[468, 250], [468, 60], [640, 360]], 'div-gpt-ad-1461936145403-5').defineSizeMapping(mapping2).addService(googletag.pubads());
	var mapping3 = googletag.sizeMapping().
    addSize([1216, 200], [[300, 200],[300, 250],[250, 250],[250, 200], [300, 300],[250, 201]]).
    addSize([1010, 200], [[300, 200],[300, 250],[250, 250],[250, 200], [300, 300],[250, 201]]).
    addSize([768, 200], [[300, 200],[300, 250],[250, 250],[250, 200], [300, 300],[250, 201]]).
    addSize([696, 200], [[300, 200],[300, 250],[250, 250],[250, 200], [300, 300],[250, 201]]).
    addSize([596, 200], [[300, 200],[300, 250],[250, 250],[250, 200],[300, 300],[250, 201]]).
    addSize([0, 0], [320, 50]).
    build();
    slot3a = googletag.defineSlot('/5297488/HRP007', [[250, 200], [300, 300], [250, 400], [250, 100], [300, 100], [300, 600], [250, 250], [300, 250]], 'div-gpt-ad-1461936145403-6').defineSizeMapping(mapping3).addService(googletag.pubads());
    slot3b = googletag.defineSlot('/5297488/HRP008', [[250, 200], [300, 300], [250, 400], [250, 100], [300, 100], [300, 600], [250, 250], [300, 250]], 'div-gpt-ad-1461936145403-7').defineSizeMapping(mapping3).addService(googletag.pubads());
    slot3c = googletag.defineSlot('/5297488/HRP009', [[250, 200], [300, 300], [250, 400], [250, 100], [300, 100], [300, 600], [250, 250], [300, 250]], 'div-gpt-ad-1461936145403-8').defineSizeMapping(mapping3).addService(googletag.pubads());
    slot3d = googletag.defineSlot('/5297488/HRP0010', [[250, 200], [300, 300], [250, 400], [250, 100], [300, 100], [300, 600], [250, 250], [300, 250]], 'div-gpt-ad-1461936145403-9').defineSizeMapping(mapping3).addService(googletag.pubads());
    slot3e = googletag.defineSlot('/5297488/HRP0011', [[250, 200], [300, 300], [250, 400], [250, 100], [300, 100], [300, 600], [250, 250], [300, 250]], 'div-gpt-ad-1461936145403-10').defineSizeMapping(mapping3).addService(googletag.pubads());
    slot3f = googletag.defineSlot('/5297488/HRP0012', [[250, 200], [300, 300], [250, 400], [250, 100], [300, 100], [300, 600], [250, 250], [300, 250]], 'div-gpt-ad-1461936145403-11').defineSizeMapping(mapping3).addService(googletag.pubads());
    slot3g = googletag.defineSlot('/5297488/HRP0013', [[250, 200], [300, 300], [250, 400], [250, 100], [300, 100], [300, 600], [250, 250], [300, 250]], 'div-gpt-ad-1461936145403-12').defineSizeMapping(mapping3).addService(googletag.pubads());
    slot3h = googletag.defineSlot('/5297488/HRP0014', [[250, 200], [300, 300], [250, 400], [250, 100], [300, 100], [300, 600], [250, 250], [300, 250]], 'div-gpt-ad-1461936145403-13').defineSizeMapping(mapping3).addService(googletag.pubads());
    
   googletag.pubads().collapseEmptyDivs();
    googletag.enableServices();
    </script>
    <script type='text/javascript'>
var resizeTimer;
function resizer() {
   googletag.pubads().refresh([slot1a,slot1b,slot2a,slot2b,slot2c,slot2d,slot3a,slot3b,slot3c,slot3d,slot3e,slot3f,slot3g,slot3h]);
}
function resizer_header() {
   googletag.pubads().refresh([slot1a,slot1b,slot2a,slot2b,slot2c,slot2d]);
}
var cachedWidth = window.innerWidth;
window.addEventListener("resize", function() {
   var newWidth = window.innerWidth;
   if (newWidth !== cachedWidth) {
      cachedWidth = newWidth;
      clearTimeout(resizeTimer);
      resizeTimer = setTimeout(resizer, 250);
   }
});
</script>
<?	
}
else{
?>
<script type='text/javascript'>
    (function() {
    var useSSL = 'https:' == document.location.protocol;
    var src = (useSSL ? 'https:' : 'http:') +
    '//www.googletagservices.com/tag/js/gpt.js';
    document.write('<scr' + 'ipt src="' + src + '"></scr' + 'ipt>');
    })();
    </script>
    <script type='text/javascript'>
    var mapping = googletag.sizeMapping().
    addSize([1216, 200], [[970, 90], [970, 250], [728, 90]]).
    addSize([1010, 200], [728, 90]).
    addSize([534, 200], [468, 60]).
    addSize([0, 0], [320, 50]).
    build();
    slot1a = googletag.defineSlot('/5297488/HRP001', [[970, 250], [970, 90], [468, 60], [320, 50], [728, 90]], 'div-gpt-ad-1461936145403-0').defineSizeMapping(mapping).addService(googletag.pubads());
    slot1b = googletag.defineSlot('/5297488/HRP002', [[970, 250], [970, 90], [468, 60], [320, 50], [728, 90]], 'div-gpt-ad-1461936145403-1').defineSizeMapping(mapping).addService(googletag.pubads());
    var mapping2 = googletag.sizeMapping().
    addSize([1216, 200], [[640, 360], [728, 90], [468, 60], [468, 120]]).
    addSize([564, 200], [[468, 120], [468, 60]]).
    addSize([0, 0], [300, 250]).
    build();
    slot2a = googletag.defineSlot('/5297488/HRP003', [[468, 250], [468, 60], [640, 360]], 'div-gpt-ad-1461936145403-2').defineSizeMapping(mapping2).addService(googletag.pubads());
    slot2b = googletag.defineSlot('/5297488/HRP004', [[468, 250], [468, 60], [640, 360]], 'div-gpt-ad-1461936145403-3').defineSizeMapping(mapping2).addService(googletag.pubads());
    slot2c = googletag.defineSlot('/5297488/HRP005', [[468, 250], [468, 60], [640, 360]], 'div-gpt-ad-1461936145403-4').defineSizeMapping(mapping2).addService(googletag.pubads());
    slot2d = googletag.defineSlot('/5297488/HRP006', [[468, 250], [468, 60], [640, 360]], 'div-gpt-ad-1461936145403-5').defineSizeMapping(mapping2).addService(googletag.pubads());
	var mapping3 = googletag.sizeMapping().
    addSize([1216, 200], [[300, 200],[300, 250],[250, 250],[250, 200], [300, 300],[250, 201]]).
    addSize([1010, 200], [[300, 200],[300, 250],[250, 250],[250, 200], [300, 300],[250, 201]]).
    addSize([768, 200], [[300, 200],[300, 250],[250, 250],[250, 200], [300, 300],[250, 201]]).
    addSize([696, 200], [[300, 200],[300, 250],[250, 250],[250, 200], [300, 300],[250, 201]]).
    addSize([596, 200], [[300, 200],[300, 250],[250, 250],[250, 200],[300, 300],[250, 201]]).
    addSize([0, 0], [320, 50]).
    build();
    slot3a = googletag.defineSlot('/5297488/HRP007', [[250, 200], [300, 300], [250, 400], [250, 100], [300, 100], [300, 600], [250, 250], [300, 250]], 'div-gpt-ad-1461936145403-6').defineSizeMapping(mapping3).addService(googletag.pubads());
    slot3b = googletag.defineSlot('/5297488/HRP008', [[250, 200], [300, 300], [250, 400], [250, 100], [300, 100], [300, 600], [250, 250], [300, 250]], 'div-gpt-ad-1461936145403-7').defineSizeMapping(mapping3).addService(googletag.pubads());
    slot3c = googletag.defineSlot('/5297488/HRP009', [[250, 200], [300, 300], [250, 400], [250, 100], [300, 100], [300, 600], [250, 250], [300, 250]], 'div-gpt-ad-1461936145403-8').defineSizeMapping(mapping3).addService(googletag.pubads());
    slot3d = googletag.defineSlot('/5297488/HRP0010', [[250, 200], [300, 300], [250, 400], [250, 100], [300, 100], [300, 600], [250, 250], [300, 250]], 'div-gpt-ad-1461936145403-9').defineSizeMapping(mapping3).addService(googletag.pubads());
    slot3e = googletag.defineSlot('/5297488/HRP0011', [[250, 200], [300, 300], [250, 400], [250, 100], [300, 100], [300, 600], [250, 250], [300, 250]], 'div-gpt-ad-1461936145403-10').defineSizeMapping(mapping3).addService(googletag.pubads());
    slot3f = googletag.defineSlot('/5297488/HRP0012', [[250, 200], [300, 300], [250, 400], [250, 100], [300, 100], [300, 600], [250, 250], [300, 250]], 'div-gpt-ad-1461936145403-11').defineSizeMapping(mapping3).addService(googletag.pubads());
    slot3g = googletag.defineSlot('/5297488/HRP0013', [[250, 200], [300, 300], [250, 400], [250, 100], [300, 100], [300, 600], [250, 250], [300, 250]], 'div-gpt-ad-1461936145403-12').defineSizeMapping(mapping3).addService(googletag.pubads());
    slot3h = googletag.defineSlot('/5297488/HRP0014', [[250, 200], [300, 300], [250, 400], [250, 100], [300, 100], [300, 600], [250, 250], [300, 250]], 'div-gpt-ad-1461936145403-13').defineSizeMapping(mapping3).addService(googletag.pubads());
      
    googletag.pubads().collapseEmptyDivs();
    googletag.enableServices();
    </script>
    <script type='text/javascript'>
var resizeTimer;
function resizer() {
   googletag.pubads().refresh([slot1a,slot1b,slot2a,slot2b,slot2c,slot2d,slot3a,slot3b,slot3c,slot3d,slot3e,slot3f,slot3g,slot3h]);
}
function resizer_header() {
   googletag.pubads().refresh([slot1a,slot1b,slot2a,slot2b,slot2c,slot2d,slot3a,slot3c,slot3d,slot3e,slot3f,slot3g,slot3h]);
}
var cachedWidth = window.innerWidth;
window.addEventListener("resize", function() {
   var newWidth = window.innerWidth;
   if (newWidth !== cachedWidth) {
      cachedWidth = newWidth;
      clearTimeout(resizeTimer);
      resizeTimer = setTimeout(resizer, 250);
   }
});
</script>
<?	
	
}
?>
