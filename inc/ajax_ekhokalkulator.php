<?php
session_start();
header("Cache-control: private");
header('Content-Type: text/plain;charset=utf-8');

	$br=$_POST['edt_brutto'];
	
	$ea=127650;
	if($br>$ea){
		$ekbr=$br-$ea;	
		$br=$ea;
	}
	
	// replace . and spaces
	$patterns[0] = ' ';
	$patterns[1] = '.';
	$patterns[2] = ',';
	$replacements[0] = '';
	$replacements[1] = '';
	$replacements[2] = '';
	$br=str_replace($patterns, $replacements, $br);	
			
	 if ($br>500000000) {
	 	$br=500000000;
	}   
	 if ($br>7000000) $stat=0;
	 else $stat=1;
	 
   $elt=$_POST['edt_elt']; //)Összes eltartottak
   $gy=$_POST['edt_gyerekek']; //Kedvezményezett eltartottak
   $egyedulnevel=$_POST['edt_egyedulnevel'];   
   $frisshazas=$_POST['edt_frisshazas'];   
   $kedvezmeny=$_POST['edt_kedvezmeny'];   
   $munkaido=$_POST['edt_munkaido'];      
   
   $gyes=0;
   
   // nyugdijplafon
   
   $nypl=7942200; //Korábban 2007-ben 600000, majd 2008-ban 7137000, 2010-ben 7446000, 2011-ben 7665000
   
	

   
   if ($egyedulnevel==1)
   {
	   switch ($elt) {
	    case 0: 
		 $gyk=0; //ennyivel csökken az adó, neve: Családi adókedvezmény.
		 $cspotlek=0;
		break;
	    case 1:
		 $gyk=$gy*10000; //ennyivel csökken az adó
		 $cspotlek=$gy*13700;		
		break;
	    case 2:
	     $gyk=$gy*15000;
		 $cspotlek=$gy*14800;
		break;
	    default:
	     $gyk=$gy*33000;  //ennyivel csökken az adó
		 $cspotlek=$gy*17000;
	   }			
   }
    else
   {
	   switch ($elt) {
	    case 0:
		 $gyk=0; //ennyivel csökken az adó, neve: Családi adókedvezmény.
		 $cspotlek=0;
		break;
	    case 1:
		 $gyk=$gy*10000; //ennyivel csökken az adó		
		 $cspotlek=$gy*12200;		
		break;
	    case 2:
	     $gyk=$gy*15000;		
		 $cspotlek=$gy*13300;
		break;
	    default:
	     $gyk=$gy*33000;  //ennyivel csökken az adó		
		 $cspotlek=$gy*16000;
	   }	
   }
   //tartósan beteg, súlyosan beteg gyerek esetén 21 , 23 ezer per hó
     
	if($frisshazas==1 and $gyk==0){
		$fhk=5000;
	}
    else{
		$fhk=0;
	}	 

   $sbr=$br;
   
   
   // szamitott ado

   $ado=($sbr*12)*0.15;          // 15% 

  
   $ado=round($ado/12);
    
   // gyerekkedvezmennyel (családi adókedvezménnyel) (és 2015-tol frissházas kedvezménnyel) csokkentett SZJA
   if (($ado-$gyk-$fhk)>0){
    $gykado=$ado-$gyk-$fhk;
	$gykmarad=0;
   }
   else {
	$gykado=0;
	$gykmarad=$gyk+$fhk-$ado;
   }
   //2014-tol, ha nagyobb a családi adókedvezmény, mint az SZJA, akkor csökkentheto a családi adokedvezmennyel az egbizt (mvej) és a nyugdijjarulek (mvnyj) is.
   if (($br*0.07-$gykmarad)>0){
    $mvej=$br*0.07-$gykmarad;
    $gykmarad=0;
   }
   else {
    $mvej=0;
    $gykmarad=$gykmarad-$br*0.07;
   }
   
   if (($br*0.10-$gykmarad)>0){
    $mvnyj=$br*0.10-$gykmarad;
    $gykmarad=0;
   }
   else {
    $mvnyj=0;
    $gykmarad=$gykmarad-$br*0.10;
   }

	
	
	$kedvezmenylimit=100000*$munkaido/40;
    $szochjkedv=0;
	
	// Egyéb kedvezmények
	   switch ($kedvezmeny) {
	    case 1:
		 if ($br<$kedvezmenylimit) {
		   $szochjkedv=$br*0.27; 				//0-ra csökkenthetjük max a szoc. hozzájárulást
		   $szakkepzesikedv=$br*0.015; 	 //a teljes szakképzési hozzájárulás eltűnik	 
		 }
		 else{
		   $szochjkedv=$kedvezmenylimit*0.27; 							   //nincs 100 e Ft felett további kedvezmény
		   $szakkepzesikedv=$kedvezmenylimit*0.015; 				//nincs 100 e Ft felett további kedvezmény	 	 
		 }
		break;
	    case 2:
		 if ($br<$kedvezmenylimit) {
		   $szochjkedv=$br*0.145; 				//0-ra csökkenthetjük max a szoc. hozzájárulást
		   $szakkepzesikedv=0; 	 				  //nincs szakképzési hozzájárulásra kedvezmény	 
		 }
		 else{
		   $szochjkedv=$kedvezmenylimit*0.145; 					//nincs 100 e Ft felett kedvezmény
		   $szakkepzesikedv=0; 	 											   //nincs szakképzési hozzájárulásra kedvezmény	 	 
		 }	 
		break;
	    case 3:
		 if ($br<$kedvezmenylimit) {
		   $szochjkedv=$br*0.145; 				//0-ra csökkenthetjük max a szoc. hozzájárulást
		   $szakkepzesikedv=0; 	 				  //nincs szakképzési hozzájárulásra kedvezmény	 
		 }
		 else{
		   $szochjkedv=$kedvezmenylimit*0.145; 					//nincs 100 e Ft felett kedvezmény
		   $szakkepzesikedv=0; 	 											   //nincs szakképzési hozzájárulásra kedvezmény	 	 
		 }	 
		break;
	    case 4:
		 if ($br<$kedvezmenylimit) {
		   $szochjkedv=$br*0.22; 				//0-ra csökkenthetjük max a szoc. hozzájárulást
		   $szakkepzesikedv=$br*0.015; 	 //a teljes szakképzési hozzájárulás eltűnik	 
		 }
		 else{
		   $szochjkedv=$kedvezmenylimit*0.22; 					//nincs 100 e Ft felett további kedvezmény
		   $szakkepzesikedv=$kedvezmenylimit*0.015; 				//nincs 100 e Ft felett további kedvezmény	 	 
		 }
		break;
	    case 5:
		 if ($br<$kedvezmenylimit) {
		   $szochjkedv=$br*0.145; 				//0-ra csökkenthetjük max a szoc. hozzájárulást
		   $szakkepzesikedv=0; 	 				  //nincs szakképzési hozzájárulásra kedvezmény	 
		 }
		 else{
		   $szochjkedv=$kedvezmenylimit*0.145; 					//nincs 100 e Ft felett kedvezmény
		   $szakkepzesikedv=0; 	 											   //nincs szakképzési hozzájárulásra kedvezmény	 	 
		 }	 
		break;	
	    case 6:
		 if ($br<$kedvezmenylimit) {
		   $szochjkedv=$br*0.22; 				//0-ra csökkenthetjük max a szoc. hozzájárulást
		   $szakkepzesikedv=$br*0.015; 	 //a teljes szakképzési hozzájárulás eltűnik	 
		 }
		 else{
		   $szochjkedv=$kedvezmenylimit*0.22; 							   //nincs 100 e Ft felett további kedvezmény
		   $szakkepzesikedv=$kedvezmenylimit*0.015; 				//nincs 100 e Ft felett további kedvezmény	 	 
		 }
		break;		
	    case 7:
		 if ($br<$kedvezmenylimit) {
		   $szochjkedv=$br*0.22; 				//0-ra csökkenthetjük max a szoc. hozzájárulást
		   $szakkepzesikedv=$br*0.015; 	 //a teljes szakképzési hozzájárulás eltűnik	 
		 }
		 else{
		   $szochjkedv=$kedvezmenylimit*0.22; 							   //nincs 100 e Ft felett további kedvezmény
		   $szakkepzesikedv=$kedvezmenylimit*0.015; 				//nincs 100 e Ft felett további kedvezmény	 	 
		 }
		break;			 
	    case 8:
		 if ($br<($kedvezmenylimit*5)) {
		   $szochjkedv=$br*0.22; 				//0-ra csökkenthetjük max a szoc. hozzájárulást
		   $szakkepzesikedv=$br*0.015; 	 //a teljes szakképzési hozzájárulás eltűnik	 
		 }
		 else{
		   $szochjkedv=$kedvezmenylimit*0.22*5; 					//nincs 500 e Ft felett további kedvezmény
		   $szakkepzesikedv=$kedvezmenylimit*0.015*5; 				//nincs 500 e Ft felett további kedvezmény	 	 
		 }		
		break;					 	
	    case 9:
		 if ($br<75001) {
		   $szochjkedv=$br*0.16; 				//12000-rel csökkenthetjük max a szoc. hozzájárulást
		   $szakkepzesikedv=0; 	  //nincs szakképzési hozzájárulásra kedvezmény	 
		 }
		 else{
		   $szochjkedv=12000-0.21818181818181818181818181818182*($br-75000); 					//nincs 130 e Ft felett egyáltalán kedvezmény
		   if ($szochjkedv<0) $szochjkedv=0;
		   $szakkepzesikedv=0; 				 //nincs szakképzési hozzájárulásra kedvezmény	 	  
		 }		
		break;						
	    default:
		 $szochjkedv=0; //nincs kedvezmény
		 $szakkepzesikedv=0; //nincs kedvezmény		 
   }			
	
	
	
    $adojovairas=0;
	$nyjadokedv=0;
	$szolado=0;   
   // osszes ado
   if (($ado + $szolado - ($nyjadokedv + $adojovairas + $gyk + $fhk + $egyebkedv) /* osszes kedvezmeny */ ) > 0) 
    $osszesado=$ado + $szolado - ($nyjadokedv + $adojovairas + $gyk + $fhk);
   else
    $osszesado=0;
   
   $brosszeslevonas=($mvej)+($br*0.015)+($mvnyj)+$osszesado; //állami nyugdíjjárulék 1,5% 2008-tól, nem 0,5, 2011. január 13-ától 10% az állami, nincs magán. 6-ról 7% lett a munkavállalói eü. járulék

   
   // osszes 2015-ös adó
   if ($elt==2)  $gyk2015 = $gyk - ($gy*2500); 
   else $gyk2015 = $gyk;
   
   $ado2015 = $ado+($br*0.01);
   
   if (($ado2015 + $szolado - ($nyjadokedv + $adojovairas + $gyk2015 + $fhk + $egyebkedv) /* osszes kedvezmeny */ ) > 0) 
    $osszesado2015 = $ado2015 + $szolado - ($nyjadokedv + $adojovairas + $gyk2015 + $fhk);
   else
    $osszesado2015=0;
   $diff2015 = $osszesado2015-$osszesado;
   
	$bottomrows="";
	
  
$allado=0;
$munkado=0;
$muvado=0;
$ekhomunk=$ekbr*0.2;
$ekhomunkv=$ekbr*0.15;

$allado=$osszesado;
$allmvjarulek=$munkavallaloijarulek+$munkavallaloieujarulek+$allaminyugdijjarulek+$ekhomunkv+($mvej+$mvnyj)-($br*0.015-$szakkepzesikedv);
//echo $munkavallaloijarulek."+".$munkavallaloieujarulek."+".$allaminyugdijjarulek."+".$ekhomunkv."+(".$mvej."+".$mvnyj.")";

$allmjarulek=$munkaadojarulek+$munkaadotbjarulek+$szakkepzesi+$ekhomunk+($br*0.27-$szochjkedv)+($br*0.015-$szakkepzesikedv);

?>
<table class="md-margin table-style1 table-striped" cellpadding="0" cellspacing="0" >

   <tr class="even">
	   <th>Éves bruttó jövedelem:</th>
	   <td><?=number_format( (($br+$gyes)*12) ,0,".",".");?> Ft</td>
   </tr>
   <tr class="odd">
	   <th>Eltartottak száma *:</th>
	   <td><?=$elt;?></td>
   </tr>
   <tr class="even">
	   <th>Kedvezményezett eltartottak (gyermekek) száma **:</th>
	   <td><?=$gy;?></td>
   </tr>
   <tr class="odd">
	   <th>Szociális hozzájárulási adó:</th>
	   <td><?=number_format( ($br*0.22-$szochjkedv) ,0,".",".");?> Ft</td>
   </tr>
   <tr class="even">
	   <th>Szakképzési hozzájárulás:</th>
	   <td><?=number_format( ($br*0.015-$szakkepzesikedv) ,0,".",".");?> Ft</td>
   </tr>
   <tr class="odd">
	   <th>Munkaerő-piaci járulék (1,5%):</th>
	   <td><?=number_format( ($br*0.015) ,0,".",".");?> Ft</td>
   </tr>
   <tr class="even">
	   <th>Egészségbiztosítási járulék (7%):</th>
	   <td><?=number_format( ($mvej) ,0,".",".");?> Ft</td>
   </tr>
   <tr class="odd">
	   <th>Nyugdíjjárulék (10%):</th>
	   <td><?=number_format( ($mvnyj) ,0,".",".");?> Ft</td>
   </tr>                                      
   <tr class="even">
	   <th>GYES</th>
	   <td><?=number_format( $gyes ,0,".",".");?> Ft</td>
   </tr>
   <tr class="odd">
	   <th>Családi adókedvezmény:</th>
	   <td><?=number_format( $gyk ,0,".",".");?> Ft</td>
   </tr>
   <tr class="even">
	   <th>Friss házasok adókedvezménye:</th>
	   <td><?=number_format( $fhk ,0,".",".");?> Ft</td>
   </tr> 
   <tr class="odd">
	   <th>Számított SZJA:</th>
	   <td><?=number_format( ($ado) ,0,".",".");?> Ft</td>
   </tr>
   <tr class="even">
	   <th>Családi adókedvezménnyel csökkentett SZJA:</th>
	   <td><?=number_format( $gykado ,0,".",".");?> Ft</td>
   </tr>
   <tr class="odd">
	   <th>Fel nem használt családi adókedvezmény:</th>
	   <td><?=number_format( $gykmarad ,0,".",".");?> Ft</td>
   </tr>                                        
   <tr class="even">
	   <th><i>------------------------ EKHO szerint adózó rész------------------------</i></th>
	   <td>&nbsp;</td>
   </tr>   
     <tr class="odd">
	   <th>EKHO Szerint adózó alapösszeg:</th>
	   <td><?=number_format( ($ekbr),0,".",".");?> Ft</td>
   </tr>                                                           
   <tr class="even">
	   <th>EKHO Szerint az alapösszeg után a munkáltató által fizetend&#x0151;</th>
	   <td><?=number_format( ($ekhomunk),0,".",".");?> Ft</td>
   </tr>          
      <tr class="odd">
	   <th>EKHO Szerint az alapösszeg után a munkavállaló által fizetend&#x0151;</th>
	   <td><?=number_format( ($ekhomunkv),0,".",".");?> Ft</td>
   </tr>      
   <tr class="even">
	   <th><i>------------------------ Összes adók------------------------</i></th>
	   <td>&nbsp;</td>
   </tr>   
  
   <tr class="even">
	   <th>Összes adó:</th>
	   <td><?=number_format( $allado ,0,".",".");?> Ft</td>
   </tr>                                        
   <tr class="odd">
	   <th>Nyugdjíjárulék utáni kedvezmény:</th>
	   <td><?=number_format( $nyjadokedv ,0,".",".");?> Ft</td>
   </tr>                                                           
                    
   <tr class="even">
	   <th>Összes kedvezmény:</th>
	   <td><?=number_format( ($nyjadokedv + $adojovairas + $gyk) ,0,".",".");?> Ft</td>
   </tr>                                        
   <tr class="odd">
	   <th>Havi összes levonás a bruttó bérből:</th>
	   <td><?=number_format( ($allado+$allmvjarulek) ,0,".",".");?> Ft</td>
   </tr>                                                           
   <tr class="even">
	   <th>Havi összes munkaadói járulék:</th>
	   <td><?=number_format( ($allmjarulek) ,0,".",".");?> Ft</td>
   </tr>                                        
   <tr class="odd">
	   <th>Összesen havonta az államnak fizetendő:</th>
	   <td><?=number_format( ($allado+$allmvjarulek+$allmjarulek) ,0,".",".");?> Ft</td>
   </tr>                                                           
   <tr class="even">
	   <th>Munkaadó összes havi költsége:</th>
	   <td><b><?=number_format( ($br+$ekbr+$allmjarulek) ,0,".",".");?> Ft</b></td>
   </tr>                                        
   <tr class="odd">
	   <th>Nettó havi munkabér:</th>
	   <td><?=number_format( ($br+$ekbr-($allado+$allmvjarulek)) ,0,".",".");?> Ft</td>
   </tr>                                                           
   <tr class="even">
	   <th>Családi pótlék:</th>
	   <td><?=number_format( ($cspotlek) ,0,".",".");?> Ft</td>
   </tr>                                        
   <tr class="odd">
	   <th>Havi összes nettó jövedelem:</th>
	   <td><b><?=number_format( ($br+$ekbr-($allado+$allmvjarulek)+$cspotlek) ,0,".",".");?> Ft</b></td>
   </tr>                                                           
                    
</table>
<?
echo $br."+".$ekbr."-"."(".$allado."+".$allmvjarulek.")";
?>



<blockquote>
  <p><i>* Eltartottak száma:</i> ebbe beleértve a 18 év feletti, rendszeres jövedelemmel nem rendelkező, akkreditált iskolai rendszerű felsőfokú szakképzésben vagy főiskolai/egyetemi képzésben első képesítését/oklevelét szerző diák is. </p>
</blockquote>

<blockquote>
  <p><i>** Kedvezményezett eltartott gyermekek:</i> a családi pótlékra-, rokkantsági járadékra jogosult / jogosító gyermekek; a magzat (a fogantatás 91. napjától). </p>
</blockquote>

<blockquote>
  <p><i>*** Friss házasok adókedvezménye (2015):</i> A kedvezmény a házasságkötést követő első hónaptól jár legfeljebb 24 hónapon át azoknak a pároknak, ahol legalább az egyik félnek ez az első házassága. Ha ezalatt jön a (vagy van) gyerek, a frissházas-kedvezmény elveszik, és helyette családi kedvezmény jár.	</p>
</blockquote>

<?
mysql_close($connid);
?>
