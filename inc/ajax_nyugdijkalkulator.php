<?
$szuletesi_ev=$_GET['szuletesi_ev'];
$munkakezdes_eve=$_GET['MUNKAKEZDES_EVE'];
$munka_jellege=$_GET['MUNKA_JELLEGE'];
$beosztas=$_GET['BEOSZTAS'];
$brutto_havi_fiz_munkakezdes=$_GET['BRUTTO_HAVI_FIZ_MUNKAKEZDES'];
$brutto_havi_fiz_jelenleg=$_GET['BRUTTO_HAVI_FIZ_JELENLEG'];
$bonusz=$_GET['BBONUSZ'];
$extra_evek_mult=$_GET['EXTRA_EVEK_MULT'];
$kiesett_evek_mult=$_GET['KIESETT_EVEK_MULT'];
$extra_evek_jovo=$_GET['EXTRA_EVEK_JOVO'];
$kieso_evek_jovo=$_GET['KIESO_EVEK_JOVO'];

//paraméterek
$jelenleg_ev=date("Y",time());
$technikai_kamatlab=0.0365;
$ferfiak_aranya=0.4475;
$nok_aranya=0.5525;
$inflacio=0;
$fizetes_emeles=0.02;
$szja_also_kulcs_2008=0.18;
$szja_felso_kulcs_2008=0.36;
$szja_kulcshatar_2008=1700000;
$nyugdijjarulek_2008=0.095;
$eb_jarulek_2008=0.06;
$munkav_jarulek_2008=0.015;
//tb
$nyugdij_korhatar=62;
$tb_plafon_kezdet=1992;
$tb_plafon_jelenleg=7137000;
$tb_plafon_novekedesi_utem=0;
$tb_szabaly_valtozas_kezdet=2200;
$szuks_szolg_ido_jelenleg=10;
$szuks_szolg_ido_uj=20;
$max_jov=100000000;


//Első kalkuláció
$brutto_eves_fiz_munkakezdes=$brutto_havi_fiz_munkakezdes*12;
$brutto_eves_fiz_jelenleg=$brutto_havi_fiz_jelenleg*12;
$kor=$jelenleg_ev-$szuletesi_ev;
$nyugdijba_vonulas_ev=$szuletesi_ev+$nyugdij_korhatar;
if($nyugdijba_vonulas_ev<$tb_szabaly_valtozas_kezdet){
	$Szuks_szolg_ido_nyugdij=$szuks_szolg_ido_jelenleg;
}
else{
	$Szuks_szolg_ido_nyugdij=$szuks_szolg_ido_uj;
}
$szolgalati_evek_mult=$jelenleg_ev-$munkakezdes_eve+$extra_evek_mult-$kiesett_evek_mult;
$szolg_evek_teljes=$szolgalati_evek_mult+($nyugdijba_vonulas_ev-$jelenleg_ev)+$extra_evek_jovo-$kieso_evek_jovo;
?>
<h3>Számított paraméterek</h3>
<table class="md-margin table-style1 table-striped" cellpadding="0" cellspacing="0" >
   
   <tr class="odd">
	   <th>Bruttó éves fizetés munkakezdés évében:</th>
	   <td><?=huf($brutto_eves_fiz_munkakezdes);?> Ft</td>
   </tr>
   <tr class="even">
	   <th>Jelenlegi éves bruttó fizetes:</th>
	   <td><?=huf($brutto_eves_fiz_jelenleg) ;?> Ft</td>
   </tr>
   <tr class="odd">
	   <th>Életkor:</th>
	   <td><?=$kor;?></td>
   </tr>
   <tr class="even">
	   <th>Nyugdíjkorhatár:</th>
	   <td><?=$nyugdij_korhatar;?> év</td>
   </tr>
   <tr class="odd">
	   <th>Nyugdíjba vonulás várható éve:</th>
	   <td><?=$kor;?></td>
   </tr>
   <tr class="even">
	   <th>Szükséges minimum szolgálati év:</th>
	   <td><?=$Szuks_szolg_ido_nyugdij;?> év</td>
   </tr>
   <tr class="odd">
	   <th>Meglévő szolgálati évek száma:</th>
	   <td><?=$szolgalati_evek_mult;?> év</td>
   </tr>
   <tr class="even">
	   <th>Szolgálati idő nyugdíjba vonulásig (az eddig megszerzett évekkel együtt):</th>
	   <td><?=$szolg_evek_teljes;?> év</td>
   </tr>
</table>   
<?


if($beosztas=="vezeto"){
	$promo_tabla_szama="vezeto";
}
else{
	if($munka_jellege=="szellemi"){
		$promo_tabla_szama="szellemi";
	}
	else{
		$promo_tabla_szama="fizikai";
	}
}		
$brutto_jov_munkakezdes=$brutto_havi_fiz_munkakezdes*12*(1+($bonusz/100));
$valszorzo=matekize(db_one("select val_szorzo_tb from nyugdij_valortable where ev='".$munkakezdes_eve."'"));
$brutto_jov_munkakezdes_valorizalt=$brutto_havi_fiz_munkakezdes*$valszorzo;
$brutto_jov_jelenleg=$brutto_havi_fiz_jelenleg*12*(1+($bonusz/100));

?>
<h3>Jövedelem számítás</h3>
<table class="md-margin table-style1 table-striped" cellpadding="0" cellspacing="0" >
   
   <tr class="odd">
	   <th>Éves bruttó fizetés a munkakezdés évében.</th>
	   <td><?=huf($brutto_jov_munkakezdes);?> Ft</td>
   </tr>
   <tr class="even">
	   <th>Munkakezdéskori jövedelem jelenlegi időpontra valorizált értéke (havi bruttó)</th>
	   <td><?=huf($brutto_jov_munkakezdes_valorizalt);?> Ft</td>
   </tr>
   <tr class="odd">
	   <th>Éves bruttó fizetés a munkakezdés évében.</th>
	   <td><?=huf($brutto_jov_jelenleg);?> Ft</td>
   </tr>
   
</table>   
<?
$myjov=array();
$k=0;
for($i=$munkakezdes_eve;$i<=$nyugdijba_vonulas_ev;$i++){
	if($i<=$jelenleg_ev){
		$brutto_jov_becsult_promo_nelkul=round($brutto_jov_munkakezdes+($i-$munkakezdes_eve)*($brutto_jov_jelenleg-$brutto_jov_munkakezdes)/($jelenleg_ev-$munkakezdes_eve));
		$promo_szorzo="1";
	}
	else{
		$brutto_jov_becsult_promo_nelkul=round($brutto_jov_jelenleg*pow((1+$fizetes_emeles),($i-$jelenleg_ev)));
		$prom_tag1=db_one("select $promo_tabla_szama from nyugdij_eletkor_fizu where kor='".$nyugdij_korhatar."'");
		$prom_tag2=db_one("select $promo_tabla_szama from nyugdij_eletkor_fizu where kor='".$kor."'");
		$promo_szorzo=round(matekize($prom_tag1)/matekize($prom_tag2),4);
	}
	$brutto_jov_becsult_promoval=round($brutto_jov_becsult_promo_nelkul*$promo_szorzo);
	$jov_tb_plafon=db_one("select jarulekplafon from nyugdij_valortable where ev='$i'");
	if($i<$tb_plafon_kezdet){
		$ny_alap_brutto_nem_valorizalt=$brutto_jov_becsult_promoval;
	}
	else{
		if($brutto_jov_becsult_promoval<$jov_tb_plafon){
			$ny_alap_brutto_nem_valorizalt=$brutto_jov_becsult_promoval;	
		}
		else{
			$ny_alap_brutto_nem_valorizalt=$jov_tb_plafon;	
		}
	}

	$szrow=db_row("select * from nyugdij_szja_levonas where year='$i'");
	$valrow=db_row("select * from nyugdij_valortable where ev='$i'");
	$ado_brutto_jov_plafonig=$ny_alap_brutto_nem_valorizalt;
	$ado_eb_jarulek=matekize($szrow['egeszsegbizt_jarulek']);
	$ado_munkav_jarulek=matekize($szrow['munkavall_ajarulek']);
	$ado_brutto_jovedelem=$brutto_jov_becsult_promoval;
	$ado_tb_plafon=$jov_tb_plafon;
	$ado_nyugdij_jarulek=matekize($szrow['nyug_munkvall_jarulek']);
	//nah
	if($ado_brutto_jovedelem<$ado_tb_plafon){
		$insado=$ado_brutto_jovedelem;
	}
	else{
		$insado=$ado_tb_plafon;
	}	
	$ado_osszes_jarulek_munkav=$ado_brutto_jov_plafonig*($ado_eb_jarulek+$ado_munkav_jarulek)+$insado*$ado_nyugdij_jarulek;
	$ado_netto_jovedelem=$ny_alap_brutto_nem_valorizalt-$ado_osszes_jarulek_munkav;
	$savszam=$szrow['savszam'];
	include("inc/nyugdijkalkulator_szjakalk.inc");
	$ado_netto_kereset=$ado_brutto_jovedelem-$ado_szja_levonas-$ado_osszes_jarulek_munkav;
	$savszam_degr=$szrow['svaszam_degr'];
	include("inc/nyugdijkalkulator_degrkalk.inc");
	$ado_degresszios_levonas=$ado_netto_kereset-$ado_netto_kereset_degr_utan;
	$levonas_savos_beszamitas=$ado_degresszios_levonas;
	$ny_alap_netto_nem_valorizalt=$ny_alap_brutto_nem_valorizalt-$ado_szja_levonas-$ado_osszes_jarulek_munkav-$levonas_savos_beszamitas;
	$becsult_jovedelem_netto_valorizalt=round($ny_alap_netto_nem_valorizalt*matekize($valrow['val_szorzo_tb']));
	$becsult_jovedelem_brutto_valorizalt=$ny_alap_brutto_nem_valorizalt*matekize($valrow['val_szorzo_tb']);
	
	$myjov[$k]['jov_ev']=$i;
	$myjov[$k]['brutto_jov_becsult_promo_nelkul']=$brutto_jov_becsult_promo_nelkul;
	$myjov[$k]['promo_szorzo']=$promo_szorzo;
	$myjov[$k]['brutto_jov_becsult_promoval']=$brutto_jov_becsult_promoval;
	$myjov[$k]['jov_tb_plafon']=$jov_tb_plafon;
	$myjov[$k]['ny_alap_brutto_nem_valorizalt']=$ny_alap_brutto_nem_valorizalt;
	$myjov[$k]['ado_osszes_jarulek_munkav']=$ado_osszes_jarulek_munkav;
	$myjov[$k]['ado_netto_jovedelem']=$ado_netto_jovedelem;
	$myjov[$k]['ado_szja_levonas']=$ado_szja_levonas;
	$myjov[$k]['ado_netto_kereset']=$ado_netto_kereset;
	$myjov[$k]['ado_netto_kereset_degr_utan']=$ado_netto_kereset_degr_utan;
	$myjov[$k]['ado_degresszios_levonas']=$ado_degresszios_levonas;
	$myjov[$k]['levonas_savos_beszamitas']=$levonas_savos_beszamitas;
	$myjov[$k]['ny_alap_netto_nem_valorizalt']=$ny_alap_netto_nem_valorizalt;
	$myjov[$k]['becsult_jovedelem_netto_valorizalt']=$becsult_jovedelem_netto_valorizalt;
	$myjov[$k]['becsult_jovedelem_brutto_valorizalt']=$becsult_jovedelem_brutto_valorizalt;
	
	
	$k++;
}

$a=0;
$magnyug=0;
$onyug=0;
//echo"<pre>";
//print_r($myjov);
//echo"</pre>";
for($i=0;$i<count($myjov);$i++){
	$a=$a+$myjov[$i]['becsult_jovedelem_netto_valorizalt'];
	//$magnyug=$magnyug+$myjov[$i]['mnyp_hozammal_novelt_osszeg'];
	//$onyug=$onyug+$myjov[$i]['onyp_hozammal_novelt_osszeg'];
}
$hjrow=db_row("select * from nyugdij_szolgev_fizetes where szolg_ido='".$szolg_evek_teljes."'");
if($tag_mnyp=="igen"){
	$c="0.75";
	$b=matekize($hjrow['kereset_percent']);
	$HELYETTESITESI_RATA_2013ELOTT=$b*$c;		
}
else{
	$HELYETTESITESI_RATA_2013ELOTT=matekize($hjrow['kereset_percent'])*1	;	
}
$ATLAGOS_VALORIZALT_JOV_NETTO=$a/count($myjov);
$VARHATO_EVES_NYUGD_2013ELOTT=round($ATLAGOS_VALORIZALT_JOV_NETTO*$HELYETTESITESI_RATA_2013ELOTT);
$VARHATO_HAVI_NETTO_NYUGD=$VARHATO_EVES_NYUGD_2013ELOTT/12;
$final=0;
$final=$final+$VARHATO_HAVI_NETTO_NYUGD;

?>
<h3>TB Nyugdíj számítás</h3>
<table class="md-margin table-style1 table-striped" cellpadding="0" cellspacing="0" >
   
   <tr class="odd">
	   <th>Helyettesítési ráta.</th>
	   <td><?=($HELYETTESITESI_RATA_2013ELOTT*100);?> %</td>
   </tr>
   <tr class="even">
	   <th>Átlagos valorizált jövedelem - nettó</th>
	   <td><?=huf(round($ATLAGOS_VALORIZALT_JOV_NETTO));?> Ft</td>
   </tr>
   <tr class="odd">
	   <th>Várható éves nyugdíj nettó</th>
	   <td><?=huf(round($VARHATO_EVES_NYUGD_2013ELOTT));?> Ft</td>
   </tr>
    <tr class="even">
	   <th>Várható havi nyugdíj nettó</th>
	   <td><?=huf(round($VARHATO_HAVI_NETTO_NYUGD));?> Ft</td>
   </tr>
</table> 
<blockquote>
  <h3 style="color:#FF0000;">* Az adatok tájékoztató jellegűek!!</h3>
</blockquote>
