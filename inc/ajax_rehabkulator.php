<?
session_start();
include_once ("../inc/sys.conf");
include_once ("../inc/_functions.php");
header("Cache-control: private");
header('Content-Type: text/plain;charset=utf-8');

if($_REQUEST['letszam']>25){$rehabletszam=$_REQUEST['letszam']*0.05;	}
else{$rehabletszam=0;}
$fizrehab=$rehabletszam-$_REQUEST['rehabletszam'];
if($fizrehab<0){$fizrehab=0;}
$rehabjar=$fizrehab*1148850;
$hatrahofo=(($rehabletszam*12)-($_REQUEST['rehabletszam']*12))/($_REQUEST['hatraho']);
if($hatrahofo<0){$hatrahofo=0;}
if($fizrehab<0){$berjarulek=$_REQUEST['rehabber']*1.225;}else{$berjarulek=0;}
$berjarulek=$_REQUEST['rehabber']*1.225;
$munkberkolt=$berjarulek*$hatrahofo;
$munkaberhatraho=$munkberkolt*$_REQUEST['hatraho'];
$megtak=$rehabjar-$munkaberhatraho;
if($rehabjar<$munkaberhatraho){
	$megtak=0;
}


?>
<table class="md-margin table-style1 table-striped" cellpadding="0" cellspacing="0" >
   
   <tr class="odd">
	   <th>Rehabilitációs hozzájárulás mértéke - előírt foglalkoztatási kvóta<br />(statisztikai állományi létszám 5%-a):</th>
	   <td><?=$rehabletszam;?> fő</td>
   </tr>
   <tr class="even">
	   <th>Rehabilitációs hozzájárulás megfizetése  (foglalkoztatási kvóta eléréshez szükséges létszám után):</th>
	   <td><?=$fizrehab;?> fő</td>
   </tr>
   <tr class="odd">
	   <th>Rehabilitációs hozzájárulás mértéke :</th>
	   <td><?=number_format( $rehabjar ,0,".",".");?></td>
   </tr>
   <tr class="even">
	   <th>A hátralévő hónapokban foglalkoztatni szükséges megváltozott munkaképességű munkavállalók létszáma:</th>
	   <td><?=$hatrahofo;?> fő</td>
   </tr>
   <tr class="odd">
	   <th>Munkáltató bérköltsége járulékokkal:</th>
	   <td><?=number_format( $berjarulek ,0,".",".");?></td>
   </tr>
   <tr class="even">
	   <th>Munkáltató összes bérköltsége járulékokkal:</th>
	   <td><?=number_format( $munkberkolt ,0,".",".");?></td>
   </tr>
   <tr class="odd">
	   <th>Munkáltató  összes bérköltsége járulékokkal a hátralévő hónapokra:</th>
	   <td><?=number_format( $munkaberhatraho ,0,".",".");?></td>
   </tr>
      <tr class="even">
	   <th>Megváltozott munkaképességű munkavállalók foglalkoztatatása esetén várható költségmegtarítás a rehabilitációs hozzájárulás megfizetésével szemben:</th>
	   <td><?=number_format( $megtak ,0,".",".");?></td>
   </tr>                                             
</table>
