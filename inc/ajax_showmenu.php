<?
header('Content-Type: text/plain;charset=utf-8');
if($_REQUEST['menu']=="menu-1"){
	?>
	<div class="col-md-12 column">

			<div class="row clearfix">
				<div class="col-5 col-5-xs first">
					<ul>
						<li><a href="hr_strategia.html"><i class="fa fa-angle-double-right"></i> HR-stratégia</a></li>
						<li><a href="munkakor.html"><i class="fa fa-angle-double-right"></i> Munkakör</a></li>
						<li><a href="toborzas-kivalasztas.html"><i class="fa fa-angle-double-right"></i> Toborzás, kiválasztás</a></li>
						<li><a href="atipikus.html"><i class="fa fa-angle-double-right"></i> Atipikus foglalkoztatás</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="kepzes-trening-coaching.html"><i class="fa fa-angle-double-right"></i> Képzés, tréning, coaching</a></li>
						<li><a href="berszamfejtes.html"><i class="fa fa-angle-double-right"></i> Bérszámfejtés</a></li>
						<li><a href="employer_branding.html"><i class="fa fa-angle-double-right"></i> Employer Branding</a></li>
						<li><a href="interim_management.html"><i class="fa fa-angle-double-right"></i> Interim menedzsment</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="berezes_kompenzacio.html"><i class="fa fa-angle-double-right"></i> Bérezés, adózás</a></li>
						<li><a href="cafeteria.html"><i class="fa fa-angle-double-right"></i> Cafeteria</a></li>
						<li><a href="munkajog.html"><i class="fa fa-angle-double-right"></i> Munkajog</a></li>
						<li><a href="kompetencia_ertekeles.html"><i class="fa fa-angle-double-right"></i> Kompetencia-értékelés</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="fluktuacio.html"><i class="fa fa-angle-double-right"></i> Fluktuáció</a></li>
						<li><a href="szervezetfejlesztes.html"><i class="fa fa-angle-double-right"></i> Szervezetfejlesztés</a></li>
						<li><a href="hr_informatika.html"><i class="fa fa-angle-double-right"></i> HR-informatika</a></li>
						<li><a href="outsourcing.html"><i class="fa fa-angle-double-right"></i> Outsourcing</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="munkavedelem.html"><i class="fa fa-angle-double-right"></i> Munkavédelem</a></li>
						<li><a href="irodai_ergonomia.html"><i class="fa fa-angle-double-right"></i> Egészséges munkahely</a></li>
						<li><a href="csr.html"><i class="fa fa-angle-double-right"></i> CSR</a></li>
						<li><a href="elbocsatats_leepites.html"><i class="fa fa-angle-double-right"></i> Elbocsátás, outplacement</a></li>
					</ul>
				</div>
			</div>
		</div>
	<?
}
elseif($_REQUEST['menu']=="menu-2"){
	?>
		<div class="col-md-12 column">

				<div class="row clearfix">
					<div class="col-5 col-5-xs first">
						<ul>
							<li><a href="legfrissebb.html"><i class="fa fa-angle-double-right"></i> Legfrissebb cikkek</a></li>
							<li><a href="munkaugyi_hirek.html"><i class="fa fa-angle-double-right"></i> Munkaügyi hírek</a></li>
							<li><a href="munkaeropiac.html"><i class="fa fa-angle-double-right"></i> Munkaerőpiac</a></li>
							<li><a href="video.html"><i class="fa fa-angle-double-right"></i> Videók</a></li>
						</ul>
					</div>
					<div class="col-5 col-5-xs">
						<ul>
							<li><a href="vallalati_hirek.html"><i class="fa fa-angle-double-right"></i> Vállalati hírek</a></li>
							<li><a href="nagyvilagbol.html"><i class="fa fa-angle-double-right"></i> A nagyvilágból</a></li>
							<li><a href="magazin.html"><i class="fa fa-angle-double-right"></i> Magazin</a></li>
							<li><a href="kulturalis_ajanlo.html"><i class="fa fa-angle-double-right"></i> Kulturális ajánló</a></li>
						</ul>
					</div>
					<div class="col-5 col-5-xs">
						<ul>
							<li><a href="oktatas.html"><i class="fa fa-angle-double-right"></i> Oktatáspolitika</a></li>
							<li><a href="nyugdij.html"><i class="fa fa-angle-double-right"></i> Nyugdíj</a></li>
							<li><a href="munkaugyi_palyazatok.html"><i class="fa fa-angle-double-right"></i> Pályázatok</a></li>
							<li><a href="infografikak.html"><i class="fa fa-angle-double-right"></i> Infografikák</a></li>
						</ul>
					</div>
					<div class="col-5 col-5-xs">
						<ul>
							<li><a href="portrek.html"><i class="fa fa-angle-double-right"></i> Portrék</a></li>
							<li><a href="vezetovaltas.html"><i class="fa fa-angle-double-right"></i> Vezetőváltások</a></li>
							<li><a href="nap_mondasa.html"><i class="fa fa-angle-double-right"></i> Aranyköpés</a></li>
							<li><a href="fenykepalbum.html"><i class="fa fa-angle-double-right"></i> Képalbumok</a></li>
						</ul>
					</div>
					<div class="col-5 col-5-xs">
						<ul>
							<li><a href="heti_top.html"><i class="fa fa-angle-double-right"></i> Eheti legolvasottabb</a></li>
							<li><a href="havi_top.html"><i class="fa fa-angle-double-right"></i> Ehavi legolvasottabb</a></li>
							<li><a href="idezetek.html"><i class="fa fa-angle-double-right"></i> Idézetek</a></li>
							<li><a href="hrblog.html"><i class="fa fa-angle-double-right"></i> HRBlog</a></li>
							
						</ul>
					</div>
				</div>
			</div>
	<?	
}
elseif($_REQUEST['menu']=="menu-3"){
	?>
	 <div class="col-md-12 column">

		<div class="row clearfix">
			<div class="col-5 col-5-xs first">
				<ul>
					<li><a href="javascript:void(0);" onclick="javascript:showmenu('panel-3','menu-7');"><i class="fa fa-angle-double-right"></i> Elméleti HR ismeretek</a></li>
					<li><a href="konyvajanlo.html"><i class="fa fa-angle-double-right"></i> Könyvajánló</a></li>
					<li><a href="unnepek.html"><i class="fa fa-angle-double-right"></i> Munkaszünet külföldön</a></li>
					<li><a href="oneletrajz-szerkeszto.html"><i class="fa fa-angle-double-right"></i> Önéletrajz szerkesztő</a></li>
				</ul>
			</div>
			<div class="col-5 col-5-xs">
				<ul>
					<li><a href="hrszotar.html"><i class="fa fa-angle-double-right"></i> Humánerőforrás szótár</a></li>
					<li><a href="akronima.html"><i class="fa fa-angle-double-right"></i> Akroníma-tár</a></li>
					<li><a href="kerdezz_felelek.html"><i class="fa fa-angle-double-right"></i> Kérdezz - Felelek</a></li>
					<li><a href="munkakorok_erthetoen.html"><i class="fa fa-angle-double-right"></i> Munkakörök érthetően</a></li>
					
				</ul>
			</div>
			<div class="col-5 col-5-xs">
				<ul>
					<li><a href="tanulmanyok.html"><i class="fa fa-angle-double-right"></i> Kutatások, tanulmányok</a></li>
					<li><a href="munka_torvenykonyve.html"><i class="fa fa-angle-double-right"></i> a Munka Törvénykönyve</a></li>
					<li><a href="torvenyek_rendeletek.html"><i class="fa fa-angle-double-right"></i> Törvények, rendeletek</a></li>
					<li><a href="hr_prezik.html"><i class="fa fa-angle-double-right"></i> HR Prezik</a></li>
				</ul>
			</div>
			<div class="col-5 col-5-xs">
				<ul>
					<li><a href="munkaido_2017.html"><i class="fa fa-angle-double-right"></i> Munkaidő naptár 2017</a></li>
					<li><a href="munkaido_2016.html"><i class="fa fa-angle-double-right"></i> Munkaidő naptár 2016</a></li>
					<li><a href="munkaido_2015.html"><i class="fa fa-angle-double-right"></i> Munkaidő naptár 2015</a></li>
					<li><a href="javascript:void(0);" onclick="javascript:showmenu('panel-3','menu-18');"><i class="fa fa-angle-double-right"></i> Egyéb HR ismeretek</a></li>
				</ul>
			</div>
			<div class="col-5 col-5-xs">
				<ul>
					<li><a href="mi_mennyi_2016.html"><i class="fa fa-angle-double-right"></i> Mi, mennyi? 2016</a></li>
					<li><a href="mi_mennyi_2015.html"><i class="fa fa-angle-double-right"></i> Mi, mennyi? 2015</a></li>
					<li><a href="mi_mennyi_2014.html"><i class="fa fa-angle-double-right"></i> Mi, mennyi? 2014</a></li>
					<li><a href="oneletrajz.html"><i class="fa fa-angle-double-right"></i> Önéletrajz minták</a></li>

				</ul>
			</div>
		</div>
	</div>
	<?
}
elseif($_REQUEST['menu']=="menu-4"){
	?>
	<div class="col-md-12 column">
		<div class="row clearfix">
			<div class="col-5 col-5-xs first">
				<ul>
					<li><a href="berkalkulator_2017.html"><i class="fa fa-angle-double-right"></i> Bérkalkulátor 2017</a></li>
					<li><a href="berkalkulator_2016.html"><i class="fa fa-angle-double-right"></i> Bérkalkulátor 2016</a></li>
					<li><a href="berkalkulator_2015.html"><i class="fa fa-angle-double-right"></i> Bérkalkulátor 2015</a></li>
					<li><a href="berkalkulator_2014.html"><i class="fa fa-angle-double-right"></i> Bérkalkulátor 2014</a></li>
					
				</ul>
			</div>
			<div class="col-5 col-5-xs">
				<ul>
					<li><a href="cegautoado_kalkulator.html"><i class="fa fa-angle-double-right"></i> Cégautó adó kalkulátor 2016</a></li>
					<li><a href="vegkielegites_kalkulator.html"><i class="fa fa-angle-double-right"></i> Végkielégítés kalkulátor</a></li>
					<li><a href="diakmunka_kalkulator.html"><i class="fa fa-angle-double-right"></i> Diákmunka kalkulátor</a></li>
					<li><a href="tappenz_kalkulator.html"><i class="fa fa-angle-double-right"></i> Táppénz kalkulátor</a></li>
					<!--- <li><a class="menu" href="index.phtml?page=eap">EAP</a></li> --->
				</ul>
			</div>
			<div class="col-5 col-5-xs">
				<ul>
					<li><a href="szabadsagkalkulator.html"><i class="fa fa-angle-double-right"></i> Szabadságkalkulátor</a></li>
					<li><a href="nyugdijkalkulator.html" target="_blank"><i class="fa fa-angle-double-right"></i> Nyugdíjkalkulátor</a></li>
					<li><a href="ekho_kalkulator.html"><i class="fa fa-angle-double-right"></i> Ekho kalkulátor</a></li>
					<li><a href="javascript:void(0);" onclick="javascript:showmenu('panel-4','menu-15');"><i class="fa fa-angle-double-right"></i> Egyéb kalkulátorok</a></li>
				</ul>
			</div>
			<div class="col-5 col-5-xs">
				<ul>
					<li><a href="berenkivuli_fo_2017.html"><i class="fa fa-angle-double-right"></i> Béren kívüli juttatások 2017</a></li>
					<li><a href="berenkivuli_fo_2016.html"><i class="fa fa-angle-double-right"></i> Béren kívüli juttatások 2016</a></li>
					<li><a href="berenkivuli_fo_2015.html"><i class="fa fa-angle-double-right"></i> Béren kívüli juttatások 2015</a></li>
					<li><a href="berenkivuli_fo_2014.html"><i class="fa fa-angle-double-right"></i> Béren kívüli juttatások 2014</a></li>
					
				</ul>
			</div>
			<div class="col-5 col-5-xs">
				<ul>
					<li><a href="gyes_kalkulator.html"><i class="fa fa-angle-double-right"></i> Gyed, Gyes kalkulátor</a></li>
					<li><a href="allaskeresesi_tamogatas_kalkulator.html"><i class="fa fa-angle-double-right"></i> Álláskeresési járadék</a></li>
					<li><a href="rehabilitacios_kalkulator.html"><i class="fa fa-angle-double-right"></i> Rehabilitációs hozzájárulás</a></li>
					<li><a href="feor.html"><i class="fa fa-angle-double-right"></i> FEOR kereső</a></li>
				</ul>
			</div>
		</div>
	</div>
	<?
	
}
elseif($_REQUEST['menu']=="menu-5"){
	?>
			<div class="col-md-12 column">
			<div class="row clearfix">
				<div class="col-5 col-5-xs first">
					<ul>
						<li><a href="konyvajanlo.html"><i class="fa fa-angle-double-right"></i> Könyvajánló</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="http://www.hrpremium.hu" target="_blank"><i class="fa fa-angle-double-right"></i> HR Prémium</a></li>
					</ul>
				</div>
				<!--<div class="col-5 col-5-xs">
					<ul>
						<li><a href="irodainfo.html"><i class="fa fa-angle-double-right"></i> Iroda bérletí díj kalkukátor</a></li>
					</ul>
				</div>-->
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="hrtesztek.html"><i class="fa fa-angle-double-right"></i> Tesztek</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
				</div>
			</div>
		</div>
	<?
	
}
elseif($_REQUEST['menu']=="menu-6"){
	?>
			<div class="col-md-12 column">
			<div class="row clearfix">
				<div class="col-5 col-5-xs first">
					<ul>
						<li><a href="/munkaero_kolcsonzes.html"><i class="fa fa-angle-double-right"></i> Munkaerő-kölcsönzés</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="/munkaero_kozvetites.html"><i class="fa fa-angle-double-right"></i> Munkaerő-közvetítés</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="/tavmunka.html"><i class="fa fa-angle-double-right"></i> Távmunka</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="/diakmunka.html"><i class="fa fa-angle-double-right"></i> Diákmunka</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="javascript:void(0);" onclick="javascript:showmenu('panel-1','menu-1');"><i class="fa fa-angle-double-right"></i> Vissza</a></li>
					</ul>
				</div>
			</div>
		</div>
	<?
	
}
elseif($_REQUEST['menu']=="menu-7"){
	?>
			<div class="col-md-12 column">
			<div class="row clearfix">
				<div class="col-5 col-5-xs first">
					<ul>
						<li><a href="/jelentese/rovat/hr_strategia_elmelet.html"><i class="fa fa-angle-double-right"></i> HR stratégia</a></li>
						<li><a href="/jelentese/rovat/munkakortervezes_elmelet.html"><i class="fa fa-angle-double-right"></i> Munkakörtervezés</a></li>
						<li><a href="/jelentese/rovat/toborzas_kivalasztas_elmelet.html"><i class="fa fa-angle-double-right"></i> Toborzás, kiválasztás</a></li>
						<li><a href="/jelentese/rovat/berezes_kompenzacio_elmelet.html"><i class="fa fa-angle-double-right"></i> Bérezés, kompenzáció</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="/jelentese/rovat/szemelyzetfejlesztes_elmelet.html"><i class="fa fa-angle-double-right"></i> Személyzetfejlesztés</a></li>
						<li><a href="/jelentese/rovat/teljesitmenymenedzsment_elmelet.html"><i class="fa fa-angle-double-right"></i> Teljesítmény-menedzsment</a></li>
						<li><a href="/jelentese/rovat/osztonzesmenedzsment_elmelet.html"><i class="fa fa-angle-double-right"></i> Ösztönzésmenedzsment</a></li>
						<li><a href="/jelentese/rovat/berenkivuli_elmelet.html"><i class="fa fa-angle-double-right"></i> Bérenkívüli juttatások</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="/jelentese/rovat/munkaugyi_kapcsolatok_elmelet.html"><i class="fa fa-angle-double-right"></i> Munkaügyi kapcsolatok</a></li>
						<li><a href="/jelentese/rovat/szervezetfejlesztes_elmelet.html"><i class="fa fa-angle-double-right"></i> Szervezetfejlesztés</a></li>
						<li><a href="/jelentese/rovat/hr_informatika_elmelet.html"><i class="fa fa-angle-double-right"></i> HR informatika</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="/jelentese/rovat/munkavedelem_elmelet.html"><i class="fa fa-angle-double-right"></i> Munkavédelem</a></li>
						<li><a href="/jelentese/rovat/erdekegyeztetes_elmelet.html"><i class="fa fa-angle-double-right"></i> Érdekegyeztetés</a></li>
						<li><a href="/jelentese/rovat/elbocsatas_leepites_elmelet.html"><i class="fa fa-angle-double-right"></i> Elbocsátás, leépítés</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="javascript:void(0);" onclick="javascript:showmenu('panel-3','menu-3');"><i class="fa fa-angle-double-right"></i> Vissza</a></li>
					</ul>
				</div>
			</div>
		</div>
	<?
	
}
elseif($_REQUEST['menu']=="menu-8"){
	?>
			<div class="col-md-12 column">
			<div class="row clearfix">
				<div class="col-5 col-5-xs first">
					<ul>
						<li><a href="/kepzes_fejlesztes.html"><i class="fa fa-angle-double-right"></i> Képzés-fejlesztés</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="/trening.html"><i class="fa fa-angle-double-right"></i> Tréning</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="/coaching.html"><i class="fa fa-angle-double-right"></i> Coaching</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="/e-learning.html"><i class="fa fa-angle-double-right"></i> E-learning</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="javascript:void(0);" onclick="javascript:showmenu('panel-1','menu-1');"><i class="fa fa-angle-double-right"></i> Vissza</a></li>
					</ul>
				</div>
			</div>
		</div>
	<?
}
elseif($_REQUEST['menu']=="menu-9"){
	?>
			<div class="col-md-12 column">
			<div class="row clearfix">
				<div class="col-5 col-5-xs first">
					<ul>
						<li><a href="/berenkivuli_juttatasok.html"><i class="fa fa-angle-double-right"></i> Bérenkívüli juttatások</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="/cafeteria.html"><i class="fa fa-angle-double-right"></i> Cafeteria</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="javascript:void(0);" onclick="javascript:showmenu('panel-1','menu-1');"><i class="fa fa-angle-double-right"></i> Vissza</a></li>
					</ul>
				</div>
			</div>
		</div>
	<?
}
elseif($_REQUEST['menu']=="menu-10"){
	?>
			<div class="col-md-12 column">
			<div class="row clearfix">
				<div class="col-5 col-5-xs first">
					<ul>
						<li><a href="/kompenzacio.html"><i class="fa fa-angle-double-right"></i> Bérezés, kompenzáció</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="/teljesitmeny_ertekeles.html"><i class="fa fa-angle-double-right"></i> Teljesítmény-menedzsment</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="/osztonzes_menedzsment.html"><i class="fa fa-angle-double-right"></i> Ösztönzés-menedzsment</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="/adozas.html"><i class="fa fa-angle-double-right"></i> Adózás</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="javascript:void(0);" onclick="javascript:showmenu('panel-1','menu-1');"><i class="fa fa-angle-double-right"></i> Vissza</a></li>
					</ul>
				</div>
			</div>
		</div>
	<?
}
elseif($_REQUEST['menu']=="menu-11"){
	?>
			<div class="col-md-12 column">
			<div class="row clearfix">
				<div class="col-5 col-5-xs first">
					<ul>
						<li><a href="/hrstrategia.html"><i class="fa fa-angle-double-right"></i> HR-stratégia</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="/hr-benchmark.html"><i class="fa fa-angle-double-right"></i> HR-benchmark</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="/hr_controlling.html"><i class="fa fa-angle-double-right"></i> HR-kontrolling</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="javascript:void(0);" onclick="javascript:showmenu('panel-1','menu-1');"><i class="fa fa-angle-double-right"></i> Vissza</a></li>
					</ul>
				</div>
			</div>
		</div>
	<?
}
elseif($_REQUEST['menu']=="menu-12"){
	?>
			<div class="col-md-12 column">
			<div class="row clearfix">
				<div class="col-5 col-5-xs first">
					<ul>
						<li><a href="/toborzas.html"><i class="fa fa-angle-double-right"></i> Toborzás</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="/fejvadaszat.html"><i class="fa fa-angle-double-right"></i> Fejvadászat</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="javascript:void(0);" onclick="javascript:showmenu('panel-1','menu-1');"><i class="fa fa-angle-double-right"></i> Vissza</a></li>
					</ul>
				</div>
			</div>
		</div>
	<?
}
elseif($_REQUEST['menu']=="menu-13"){
	?>
			<div class="col-md-12 column">
			<div class="row clearfix">
				<div class="col-5 col-5-xs first">
					<ul>
						<li><a href="/elbocsatas.html"><i class="fa fa-angle-double-right"></i> Elbocsátás</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="/outplacement.html"><i class="fa fa-angle-double-right"></i> Outplacement</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="javascript:void(0);" onclick="javascript:showmenu('panel-1','menu-1');"><i class="fa fa-angle-double-right"></i> Vissza</a></li>
					</ul>
				</div>
			</div>
		</div>
	<?
}
elseif($_REQUEST['menu']=="menu-14"){
	?>
			<div class="col-md-12 column">
			<div class="row clearfix">
				<div class="col-5 col-5-xs first">
					<ul>
						<li><a href="/berenkivuli.html"><i class="fa fa-angle-double-right"></i> Bérenkívüli juttatások</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="/cafeteria.html"><i class="fa fa-angle-double-right"></i> Cafeteria</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="javascript:void(0);" onclick="javascript:showmenu('panel-1','menu-1');"><i class="fa fa-angle-double-right"></i> Vissza</a></li>
					</ul>
				</div>
			</div>
		</div>
	<?
}
elseif($_REQUEST['menu']=="menu-15"){
	?>
			<div class="col-md-12 column">
			<div class="row clearfix">
				<div class="col-5 col-5-xs first">
					<ul>
						<li><a href="berenkivuli_fo_2013.html"><i class="fa fa-angle-double-right"></i> Béren kívüli juttatások 2013</a></li>
						<li><a href="http://old.hrportal.hu/index.phtml?page=berenkivuli_fo_2012" target="_blank"><i class="fa fa-angle-double-right"></i> Béren kívüli juttatások 2012</a></li>
						<li><a href="http://old.hrportal.hu/index.phtml?page=berenkivuli_fo_2010" target="_blank"><i class="fa fa-angle-double-right"></i> Béren kívüli juttatások 2010</a></li>
						<li><a href="http://old.hrportal.hu/index.phtml?page=co2_kalkulator" target="_blank"><i class="fa fa-angle-double-right"></i> CO2 kalkulátor</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="http://old.hrportal.hu/index.phtml?page=berkalkulator_2012" target="_blank"><i class="fa fa-angle-double-right"></i> Bérkalkulátor 2012</a></li>
						<li><a href="http://old.hrportal.hu/index.phtml?page=berkalkulator_2011" target="_blank"><i class="fa fa-angle-double-right"></i> Bérkalkulátor 2011</a></li>
						<li><a href="http://old.hrportal.hu/index.phtml?page=berkalkulator_2010" target="_blank"><i class="fa fa-angle-double-right"></i> Bérkalkulátor 2010</a></li>
						<li><a href="http://old.hrportal.hu/index.phtml?page=berkalkulator_2009" target="_blank"><i class="fa fa-angle-double-right"></i> Bérkalkulátor 2009</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="http://old.hrportal.hu/index.phtml?page=ekho_kalkulator_2015" target="_blank"><i class="fa fa-angle-double-right"></i> Ekho Kalkulátor 2015</a></li>
						<li><a href="http://old.hrportal.hu/index.phtml?page=ekho_kalkulator_2014" target="_blank"><i class="fa fa-angle-double-right"></i> Ekho Kalkulátor 2014</a></li>
						<li><a href="http://old.hrportal.hu/index.phtml?page=ekho_kalkulator_2012" target="_blank"><i class="fa fa-angle-double-right"></i> Ekho Kalkulátor 2012</a></li>
						<li><a href="http://old.hrportal.hu/index.phtml?page=ekho_kalkulator_2011" target="_blank"><i class="fa fa-angle-double-right"></i> Ekho Kalkulátor 2011</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="bno.html"><i class="fa fa-angle-double-right"></i> Diagnózis kódok</a></li>
						<li><a href="berkalkulator_2013.html"><i class="fa fa-angle-double-right"></i> Bérkalkulátor 2013</a></li>
	
						<!--<li><a href="cegautoado_kalkulator.html"><i class="fa fa-angle-double-right"></i> Cégautó adó kalkulátor 2016</a></li>-->
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="javascript:void(0);" onclick="javascript:showmenu('panel-4','menu-4');"><i class="fa fa-angle-double-right"></i> Vissza</a></li>
					</ul>
				</div>
			</div>
		</div>
	<?
}
elseif($_REQUEST['menu']=="menu-16"){
	?>
			<div class="col-md-12 column">
			<div class="row clearfix">
				<div class="col-5 col-5-xs first">
					<ul>
						<li><a href="/szervezet-fejlesztes.html"><i class="fa fa-angle-double-right"></i> Szervezetfejlesztés</a></li>
						<li><a href="/relokacio.html"><i class="fa fa-angle-double-right"></i> Relokáció</a></li>
						
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="/mediacio.html"><i class="fa fa-angle-double-right"></i> Mediáció</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="/kommunikacio.html"><i class="fa fa-angle-double-right"></i> Kommunikáció</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="/erdekegyeztetes.html"><i class="fa fa-angle-double-right"></i> Érdekegyeztetés</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="javascript:void(0);" onclick="javascript:showmenu('panel-1','menu-1');"><i class="fa fa-angle-double-right"></i> Vissza</a></li>
					</ul>
				</div>
			</div>
		</div>
	<?
}
elseif($_REQUEST['menu']=="menu-17"){
	?>
			<div class="col-md-12 column">
			<div class="row clearfix">
				<div class="col-5 col-5-xs first">
					<ul>
						<li><a href="/portre.html"><i class="fa fa-angle-double-right"></i> Portré</a></li>
						
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="/nezopont.html"><i class="fa fa-angle-double-right"></i> Nézőpont</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="/hr_szubjektiv.html"><i class="fa fa-angle-double-right"></i> HR Szubjektív</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="/hr_vezeto_1_napja.html"><i class="fa fa-angle-double-right"></i> HR-vezető 1 napja</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="javascript:void(0);" onclick="javascript:showmenu('panel-2','menu-2');"><i class="fa fa-angle-double-right"></i> Vissza</a></li>
					</ul>
				</div>
			</div>
		</div>
	<?
}
elseif($_REQUEST['menu']=="menu-18"){
	?>
			<div class="col-md-12 column">
			<div class="row clearfix">
				<div class="col-5 col-5-xs first">
					<ul>
						<li><a href="/portre.html"><i class="fa fa-angle-double-right"></i> Mi, mennyi? 2013</a></li>
						
					</ul>
				</div>
				<div class="col-5 col-5-xs first">
					<ul>
						<li><a href="munkahoroszkop.html"><i class="fa fa-angle-double-right"></i> Munkahoroszkóp</a></li>
						
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="munkaido_2013.html"><i class="fa fa-angle-double-right"></i> Munkaidő naptár 2013</a></li>
						<li><a href="munkaido_2014.html"><i class="fa fa-angle-double-right"></i> Munkaidő naptár 2014</a></li>
					</ul>
				</div>
				<div class="col-5 col-5-xs">
					<ul>
						<li><a href="javascript:void(0);" onclick="javascript:showmenu('panel-3','menu-3');"><i class="fa fa-angle-double-right"></i> Vissza</a></li>
					</ul>
				</div>
			</div>
		</div>
	<?
}
elseif($_REQUEST['menu']=="mobile"){
	?>
	      <nav id="menu" class="nav">
     <ul>

			<li>
				<span>HR SZAKMAI</span>
				<ul>
					<li>
						<span><i class="fa fa-angle-double-right"></i> HR-stratégia</span>
						<ul>
							<li><a href="hrstartegia.html"><i class="fa fa-angle-double-right"></i> HR-stratégia</a></li>
							<li><a href="hr-benchmark.html"><i class="fa fa-angle-double-right"></i> HR-benchmark</a></li>
							<li><a href="hr_controlling.html"><i class="fa fa-angle-double-right"></i> HR-kontrolling</a></li>
						</ul>	
						
					</li>
					<li><a href="munkakor.html"><i class="fa fa-angle-double-right"></i> Munkakör</a></li>
					<li>
						<span><i class="fa fa-angle-double-right"></i> Toborzás, kiválasztás</span>
						<ul>
							<li><a href="toborzas.html"><i class="fa fa-angle-double-right"></i> Toborzás</a></li>
							<li><a href="fejvadaszat.html"><i class="fa fa-angle-double-right"></i> Fejvadászat</a></li>
						</ul>						
					</li>
					<li>
						<span><i class="fa fa-angle-double-right"></i> Atipikus foglalkoztatás</span>
						<ul>
							<li><a href="munkaero_kolcsonzes.html"><i class="fa fa-angle-double-right"></i> Munkaerő-kölcsönzés</a></li>
							<li><a href="munkaero_kozvetites.html"><i class="fa fa-angle-double-right"></i> Munkaerő-közvetítés</a></li>
							<li><a href="tavmunka.html"><i class="fa fa-angle-double-right"></i> Távmunka</a></li>
							<li><a href="diakmunka.html"><i class="fa fa-angle-double-right"></i> Diákmunka</a></li>
						</ul>	
					</li>
					<li>
						<span><i class="fa fa-angle-double-right"></i> Képzés, tréning, coaching</span>
						<ul>
							<li><a href="kepzes_fejlesztes.html"><i class="fa fa-angle-double-right"></i> Képzés-fejlesztés</a></li>
							<li><a href="trening.html"><i class="fa fa-angle-double-right"></i> Tréning</a></li>
							<li><a href="coaching.html"><i class="fa fa-angle-double-right"></i> Coaching</a></li>
							<li><a href="e-learning.html"><i class="fa fa-angle-double-right"></i> E-learning</a></li>
						</ul>	
					</li>
					<li><a href="berszamfejtes.html"><i class="fa fa-angle-double-right"></i> Bérszámfejtés</a></li>
					<li><a href="employer_branding.html"><i class="fa fa-angle-double-right"></i> Employer Branding</a></li>
					<li><a href="interim_management.html"><i class="fa fa-angle-double-right"></i> Interim menedzsment</a></li>
					<li>
						<span><i class="fa fa-angle-double-right"></i> Bérezés, adózás</span>
						<ul>
							<li><a href="kompenzacio.html"><i class="fa fa-angle-double-right"></i> Bérezés, kompenzáció</a></li>
							<li><a href="teljesitmeny_ertekeles.html"><i class="fa fa-angle-double-right"></i> Teljesítmény-menedzsment</a></li>
							<li><a href="osztonzes_menedzsment.html"><i class="fa fa-angle-double-right"></i> Ösztönzés-menedzsment</a></li>
							<li><a href="adozas.html"><i class="fa fa-angle-double-right"></i> Adózás</a></li>
						</ul>
					</li>
					<li>
						<span><i class="fa fa-angle-double-right"></i> Bérenkívüli juttatások</span>
						<ul>
							<li><a href="berenkivuli.html"><i class="fa fa-angle-double-right"></i> Bérenkívüli juttatások</a></li>
							<li><a href="cafeteria.html"><i class="fa fa-angle-double-right"></i> Cafeteria</a></li>
						</ul>
					</li>
					<li><a href="munkajog.html"><i class="fa fa-angle-double-right"></i> Munkajog</a></li>
					<li><a href="kompetenciaertekeles.html"><i class="fa fa-angle-double-right"></i> Kompetencia-értékelés</a></li>
					<li><a href="munkaugyi_kapcsolatok.html"><i class="fa fa-angle-double-right"></i> Munkaügyi kapcsolatok</a></li>
					<li>
						<span><i class="fa fa-angle-double-right"></i> Szervezetfejlesztés</span>
						<ul>
							<li><a href="szervezet-fejlesztes.html"><i class="fa fa-angle-double-right"></i> Szervezetfejlesztés</a></li>
							<li><a href="mediacio.html"><i class="fa fa-angle-double-right"></i> Mediáció</a></li>
							<li><a href="kommunikacio.html"><i class="fa fa-angle-double-right"></i> Kommunikáció</a></li>
							<li><a href="erdekegyeztetes.html"><i class="fa fa-angle-double-right"></i> Érdekegyeztetés</a></li>
							<li><a href="relokacio.html"><i class="fa fa-angle-double-right"></i> Relokáció</a></li>
						</ul>
					</li>
					<li><a href="hr_informatika.html"><i class="fa fa-angle-double-right"></i> HR-informatika</a></li>
					<li><a href="outsourcing.html"><i class="fa fa-angle-double-right"></i> Outsourcing</a></li>
					<li><a href="munkavedelem.html"><i class="fa fa-angle-double-right"></i> Munkavédelem</a></li>
					<li><a href="irodai_ergonomia.html"><i class="fa fa-angle-double-right"></i> Egészséges munkahely</a></li>
					<li><a href="hr_controlling.html"><i class="fa fa-angle-double-right"></i> HR-kontrolling</a></li>
					<li><a href="csr.html"><i class="fa fa-angle-double-right"></i> CSR</a></li>
					<li><a href="fluktuacio.html"><i class="fa fa-angle-double-right"></i> Fluktuáció</a></li>
					<li>
						<span><i class="fa fa-angle-double-right"></i> Elbocsátás, outplacement</span>
						<ul>
							<li><a href="elbocsatas.html"><i class="fa fa-angle-double-right"></i> Elbocsátás</a></li>
							<li><a href="outplacement.html"><i class="fa fa-angle-double-right"></i> Outplacement</a></li>
						</ul>
					</li>
					
					
				</ul>
			</li>
			<li>
				<span>HÍREK</span>
				<ul>
					<li><a href="legfrissebb.html"><i class="fa fa-angle-double-right"></i> Legfrissebb cikkek</a></li>
					<li><a href="munkaugyi_hirek.html"><i class="fa fa-angle-double-right"></i> Munkaügyi hírek</a></li>
					<li><a href="munkaeropiac.html"><i class="fa fa-angle-double-right"></i> Munkaerőpiac</a></li>
					<li><a href="video.html"><i class="fa fa-angle-double-right"></i> Videók</a></li>
					<li><a href="vallalati_hirek.html"><i class="fa fa-angle-double-right"></i> Vállalati hírek</a></li>
					<li><a href="nagyvilagbol.html"><i class="fa fa-angle-double-right"></i> A nagyvilágból</a></li>
					<li><a href="magazin.html"><i class="fa fa-angle-double-right"></i> Magazin</a></li>
					<li><a href="kulturalis_ajanlo.html"><i class="fa fa-angle-double-right"></i> Kulturális ajánló</a></li>
					<li><a href="oktatas.html"><i class="fa fa-angle-double-right"></i> Oktatáspolitika</a></li>
					<li><a href="nyugdij.html"><i class="fa fa-angle-double-right"></i> Nyugdij</a></li>
					<li><a href="nezopont.html"><i class="fa fa-angle-double-right"></i> Nézőpont</a></li>
					<li><a href="hrblog.html"><i class="fa fa-angle-double-right"></i> HRBlog</a></li>
					<li><a href="munkaugyi_palyazatok.html"><i class="fa fa-angle-double-right"></i> Pályázatok</a></li>
					<li><a href="portre.html"><i class="fa fa-angle-double-right"></i> Portré</a></li>
					<li><a href="vezetovaltas.html"><i class="fa fa-angle-double-right"></i> Vezetőváltások</a></li>
					<li><a href="nap_mondasa.html"><i class="fa fa-angle-double-right"></i> Aranyköpés</a></li>
					<li><a href="fenykepalbum.html"><i class="fa fa-angle-double-right"></i> Képalbumok</a></li>
					<li><a href="heti_top.html"><i class="fa fa-angle-double-right"></i> Eheti legolvasottabb</a></li>
					<li><a href="havi_top.html"><i class="fa fa-angle-double-right"></i> Ehavi legolvasottabb</a></li>
					<li><a href="idezetek.html"><i class="fa fa-angle-double-right"></i> Idézetek</a></li>
					<li><a href="hr_vezeto_1_napja.html"><i class="fa fa-angle-double-right"></i> HR-vezető 1 napja</a></li>
				</ul>
			</li>
			<li>
				<span>ELMÉLETI HR ISMERETEK</span>
				<ul>
					<li><a href="/jelentese/rovat/hr_strategia_elmelet.html"><i class="fa fa-angle-double-right"></i> HR stratégia</a></li>
					<li><a href="/jelentese/rovat/munkakortervezes_elmelet.html"><i class="fa fa-angle-double-right"></i> Munkakörtervezés</a></li>
					<li><a href="/jelentese/rovat/toborzas_kivalasztas_elmelet.html"><i class="fa fa-angle-double-right"></i> Toborzás, kiválasztás</a></li>
					<li><a href="/jelentese/rovat/berezes_kompenzacio_elmelet.html"><i class="fa fa-angle-double-right"></i> Bérezés, kompenzáció</a></li>
					<li><a href="/jelentese/rovat/szemelyzetfejlesztes_elmelet.html"><i class="fa fa-angle-double-right"></i> Személyzetfejlesztés</a></li>
					<li><a href="/jelentese/rovat/teljesitmenymenedzsment_elmelet.html"><i class="fa fa-angle-double-right"></i> Teljesítmény-menedzsment</a></li>
					<li><a href="/jelentese/rovat/osztonzesmenedzsment_elmelet.html"><i class="fa fa-angle-double-right"></i> Ösztönzésmenedzsment</a></li>
					<li><a href="/jelentese/rovat/berenkivuli_elmelet.html"><i class="fa fa-angle-double-right"></i> Bérenkívüli juttatások</a></li>
					<li><a href="/jelentese/rovat/munkaugyi_kapcsolatok_elmelet.html"><i class="fa fa-angle-double-right"></i> Munkaügyi kapcsolatok</a></li>
					<li><a href="/jelentese/rovat/szervezetfejlesztes_elmelet.html"><i class="fa fa-angle-double-right"></i> Szervezetfejlesztés</a></li>
					<li><a href="/jelentese/rovat/hr_informatika_elmelet.html"><i class="fa fa-angle-double-right"></i> HR informatika</a></li>
					<li><a href="/jelentese/rovat/munkavedelem_elmelet.html"><i class="fa fa-angle-double-right"></i> Munkavédelem</a></li>
					<li><a href="/jelentese/rovat/erdekegyeztetes_elmelet.html"><i class="fa fa-angle-double-right"></i> Érdekegyeztetés</a></li>
					<li><a href="/jelentese/rovat/elbocsatas_leepites_elmelet.html"><i class="fa fa-angle-double-right"></i> Elbocsátás, leépítés</a></li>
				</ul>
			</li>
			<li>
				<span>HR ISMERETEK</span>
				<ul>
					<li><a href="konyvajanlo.html"><i class="fa fa-angle-double-right"></i> Könyvajánló</a></li>
					<li><a href="unnepek.html"><i class="fa fa-angle-double-right"></i> Munkaszünet külföldön</a></li>
					<li><a href="munkahoroszkop.html"><i class="fa fa-angle-double-right"></i> Munkahoroszkóp</a></li>
					<li><a href="oneletrajz-szerkeszto.html"><i class="fa fa-angle-double-right"></i> Önéletrajz szerkesztő</a></li>
					<li><a href="hrszotar.html"><i class="fa fa-angle-double-right"></i> Humánerőforrás szótár</a></li>
					<li><a href="akronima.html"><i class="fa fa-angle-double-right"></i> Akroníma-tár</a></li>
					<li><a href="kerdezz_felelek.html"><i class="fa fa-angle-double-right"></i> Kérdezz - Felelek</a></li>
					<li><a href="infografikak.html"><i class="fa fa-angle-double-right"></i> Infografikák</a></li>
					<li><a href="tanulmanyok.html"><i class="fa fa-angle-double-right"></i> Kutatások, tanulmányok</a></li>
					<li><a href="munka_torvenykonyve.html"><i class="fa fa-angle-double-right"></i> a Munka Törvénykönyve</a></li>
					<li><a href="torvenyek_rendeletek.html"><i class="fa fa-angle-double-right"></i> Törvények, rendeletek</a></li>
					<li><a href="hr_prezik.html"><i class="fa fa-angle-double-right"></i> HR Prezik</a></li>
					<li><a href="munkaido_2017.html"><i class="fa fa-angle-double-right"></i> Munkaidő naptár 2017</a></li>
					<li><a href="munkaido_2016.html"><i class="fa fa-angle-double-right"></i> Munkaidő naptár 2016</a></li>
					<li><a href="munkaido_2015.html"><i class="fa fa-angle-double-right"></i> Munkaidő naptár 2015</a></li>
					<li><a href="munkaido_2014.html"><i class="fa fa-angle-double-right"></i> Munkaidő naptár 2014</a></li>
					<li><a href="#"><i class="fa fa-angle-double-right"></i> Egyéb HR ismeretek</a></li>
					<li><a href="mi_mennyi_2016.html"><i class="fa fa-angle-double-right"></i> Mi, mennyi? 2016</a></li>
					<li><a href="mi_mennyi_2015.html"><i class="fa fa-angle-double-right"></i> Mi, mennyi? 2015</a></li>
					<li><a href="mi_mennyi_2014.html"><i class="fa fa-angle-double-right"></i> Mi, mennyi? 2014</a></li>
					<li><a href="oneletrajz.html"><i class="fa fa-angle-double-right"></i> Önéletrajz minták</a></li>
					<li><a href="cegautoado_kalkulator.html"><i class="fa fa-angle-double-right"></i> Cégautóadó kalkulátor</a></li>
					<li><a href="bno.html"><i class="fa fa-angle-double-right"></i> Orvosi diagnózis kódok</a></li>
				</ul>
			</li>
			<li>
				<span>KALKULÁTOROK</span>
				<ul>
					<li><a href="berkalkulator_2017.html"><i class="fa fa-angle-double-right"></i> Bérkalkulátor 2017</a></li>
					<li><a href="berkalkulator_2016.html"><i class="fa fa-angle-double-right"></i> Bérkalkulátor 2016</a></li>
					<li><a href="berkalkulator_2015.html"><i class="fa fa-angle-double-right"></i> Bérkalkulátor 2015</a></li>
					<li><a href="berkalkulator_2014.html"><i class="fa fa-angle-double-right"></i> Bérkalkulátor 2014</a></li>
					<li><a href="berkalkulator_2013.html"><i class="fa fa-angle-double-right"></i> Bérkalkulátor 2013</a></li>
					<li><a href="irodainfo.html"><i class="fa fa-angle-double-right"></i> Iroda bérletí díj kalkukátor</a></li>
					<li><a href="vegkielegites_kalkulator.html"><i class="fa fa-angle-double-right"></i> Végkielégítés kalkulátor</a></li>
					<li><a href="diakmunka_kalkulator.html"><i class="fa fa-angle-double-right"></i> Diákmunka kalkulátor</a></li>
					<li><a href="tappenz_kalkulator.html"><i class="fa fa-angle-double-right"></i> Táppénz kalkulátor</a></li>
					<li><a href="szabadsagkalkulator.html"><i class="fa fa-angle-double-right"></i> Szabadságkalkulátor</a></li>
					<li><a href="nyugdijkalkulator.html" target="_blank"><i class="fa fa-angle-double-right"></i> Nyugdíjkalkulátor</a></li>
					<li><a href="ekho_kalkulator.html"><i class="fa fa-angle-double-right"></i> Ekho kalkulátor</a></li>
					<li><a href="#"><i class="fa fa-angle-double-right"></i> Egyéb kalkulátorok</a></li>
					<li><a href="berenkivuli_fo_2016.html"><i class="fa fa-angle-double-right"></i> Béren kívüli juttatások 2016</a></li>
					<li><a href="berenkivuli_fo_2015.html"><i class="fa fa-angle-double-right"></i> Béren kívüli juttatások 2015</a></li>
					<li><a href="berenkivuli_fo_2014.html"><i class="fa fa-angle-double-right"></i> Béren kívüli juttatások 2014</a></li>
					<li><a href="berenkivuli_fo_2013.html"><i class="fa fa-angle-double-right"></i> Béren kívüli juttatások 2013</a></li>
					<li><a href="gyes_kalkulator.html"><i class="fa fa-angle-double-right"></i> Gyed, Gyes kalkulátor</a></li>
					<li><a href="allaskeresesi_tamogatas_kalkulator.html"><i class="fa fa-angle-double-right"></i> Álláskeresési járadék</a></li>
					<li><a href="rehabilitacios_kalkulator.html"><i class="fa fa-angle-double-right"></i> Rehabilitációs hozzájárulás</a></li>
					<li><a href="feor.html"><i class="fa fa-angle-double-right"></i> FEOR kereső</a></li>
				</ul>
			</li>
			<li class="menu-separator">
				<span>HR PIACTÉR</span>
				<ul>
					<li><a href="#"><i class="fa fa-angle-double-right"></i> Könyvajánló</a>
					</li>
					<li><a href="#"><i class="fa fa-angle-double-right"></i> HR Prémium</a>
					</li>
					<li><a href="#"><i class="fa fa-angle-double-right"></i> Iroda bérletí díj kalkukátor</a>
					</li>

				</ul>
			</li>
			<li class="menu-separator">
				<a href="http://iratmintak.hrportal.hu/" ><i class="fa fa-file-text"></i> IRATMINTÁK</a>
			</li>
			<!--<li class="menu-separator">
				<a href="/all-you-can-move-201609.html" style="background-color:#EE4A23;color:#fff;">All You Can Move SportPass</a>
			</li>-->
		   
				<li><a href="/">Címlap</a></li>
				<li><a href="/allasok.html"><span>HR</span> Állások</a></li>
				<li><a href="/berkalkulator_2016.html">Bérkalkulátor</a></li>
				<li><a href="http://www.hrkatalogus.hu"><span>HR</span> Katalógus</a></li>
				<li><a href="http://www.hrclub.hu"><span>HR</span> Események</a></li>
		</ul>
    </nav>
	<?
}
?>
