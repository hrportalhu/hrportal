<?php

function add_zero($number){
	if ($number < 10){
		$number = "0".$number;
	}
	$result = $number;
	$number = "";
	return $result;	 
}
function IntervalDays($CheckIn,$CheckOut){
	$CheckInX = explode("-", $CheckIn);
	$CheckOutX =  explode("-", $CheckOut);
	$date1 =  mktime(0, 0, 0, $CheckInX[1],$CheckInX[2],$CheckInX[0]);
	$date2 =  mktime(0, 0, 0, $CheckOutX[1],$CheckOutX[2],$CheckOutX[0]);
	$interval =($date2 - $date1)/(3600*24);
	return  $interval ;
} 

  
  
  $today = getdate(); 
  $year_of_today = $today['year']; 
  
	if (!isset($_GET['edt_szulev'])) {
		$edt_szulev=1975;
	}
	else{ 
		$edt_szulev=$_GET['edt_szulev'];
	}

	if (!isset($_GET['edt_tavalyrol'])) {
		$edt_tavalyrol=0;
	}
	else {
		$edt_tavalyrol=$_GET['edt_tavalyrol'];
	}

	if (!isset($_GET['edt_gyerekek_storno'])) {
		$edt_gyerekek_storno=0;
	}
	else{ 
		$edt_gyerekek_storno=$_GET['edt_gyerekek_storno'];
	}
	$today = getdate(); 
	$year_of_today = $today['year']; 
	$month_of_today = $today['month']; 
	$day_of_today = $today['mday']; 	  
   
	$szev=$_GET['edt_szulev'];
	$kezdodatum=$year_of_today."-".add_zero($_GET['edt_kezdoho'])."-".$_GET['edt_kezdonap'];
	$maidatum = date("Y-m-d"); 
	$kezdonap=$_GET['edt_kezdonap'];
	$kezdoho=$_GET['edt_kezdoho'];
	if (($kezdonap==0) OR ($kezdoho==0) OR (IntervalDays($kezdodatum, $maidatum, "d") < 0)){
		$nem_iden_kezdte = TRUE;
		$ossz_munkanap_maig = IntervalDays($year_of_today."-01-01", $maidatum, "d");
		$ossz_munkanap = 365;  
	}
	else{
		$nem_iden_kezdte = FALSE;
		$ossz_munkanap_maig = IntervalDays($kezdodatum, $maidatum);
		$ossz_munkanap = IntervalDays($kezdodatum, $year_of_today."-12-31");    
	} 
    
if (($nem_iden_kezdte) AND (IntervalDays($kezdodatum, $maidatum, "d")>=0)){
	$tav=$_GET['edt_tavalyrol'];	 
}
else{
	$tav=0;
}



$gy=$_GET['edt_gyerekek'];
$gys=$_GET['edt_gyerekek_storno'];
$vak=$_GET['edt_blind'];
$fogy=$_GET['edt_fogy'];
$ion=$_GET['edt_ion'];
$age = $year_of_today - $edt_szulev;


// alapszabadság   
if ($age < 25) {
$base_holiday = 20;
} elseif ($age < 28) {
$base_holiday = 21;
} elseif ($age < 31) {
$base_holiday = 22;
} elseif ($age < 33) {
$base_holiday = 23;
} elseif ($age < 35) {
$base_holiday = 24;
} elseif ($age < 37) {
$base_holiday = 25;
} elseif ($age < 39) {
$base_holiday = 26;
} elseif ($age < 41) {
$base_holiday = 27;
} elseif ($age < 43) {
$base_holiday = 28;
} elseif ($age < 45) {
$base_holiday = 29;
} elseif ($age < 100) {
$base_holiday = 30;
}



// pótszabadság    
$extra_holiday = 0;
//fiatalkorú   
if ($age < 19) {
$extra_holiday+=5;
}
//gyerekek
if ($gys == 1){
$gysboolean = "nem";
$extra_holiday+=0;
}
else {
$gysboolean = "igen";   
switch ($gy) {
case 0:
	$extra_holiday+=0;
	break;
case 1:
	$extra_holiday+=2;
	break;
case 2:
	$extra_holiday+=4;
	break;
case 3:
	$extra_holiday+=7;
	break;
}		
}
//vak
if ($vak == 1) {
	$extra_holiday+=5;
	$vakboolean = "van";  		
}
else{
$vakboolean = "nincs";  		   		
}
//vak
if ($fogy == 1) {
$extra_holiday+=2;
$fogyboolean = "van";  		
}
else{
$fogyboolean = "nincs";  		   		
}

//ionos, föld alatti munkahely
if ($ion == 1) {
$extra_holiday+=5;
$ionboolean = "igen"; 
}
else{
$ionboolean = "nem"; 
}

//Munkakezdés dátuma   
if ($nem_iden_kezdte){
	$kezdodatum="Nem idén kezdett, illetve még nem dolgozik";
}

// De ha idén kezdett   
if ($ossz_munkanap_maig > 0){
	$until_today_base_holiday = round($base_holiday*$ossz_munkanap_maig/365);
	$until_today_extra_holiday = round($extra_holiday*$ossz_munkanap_maig/365);
	$base_holiday = round($base_holiday*$ossz_munkanap/365);
	$extra_holiday = round($extra_holiday*$ossz_munkanap/365);	 
}

if ($nem_iden_kezdte){
	$kezdodatum2="";
}
else{
	$kezdodatum2=$kezdodatum." - ";
}

?>


<table class="md-margin table-style1 table-striped" cellpadding="0" cellspacing="0" >
   
   <tr class="odd">
	   <th>Születési év:</th>
	   <td><?=$edt_szulev;?></td>
   </tr>
   <tr class="even">
	   <th>A munkakezdés dátuma jelenlegi munkaadójánál:</th>
	   <td><?=$kezdodatum;?> </td>
   </tr>
   <tr class="odd">
	   <th>Tavalyról áthozott szabadság napokban:</th>
	   <td><?=$tav;?></td>
   </tr>
   <tr class="even">
	   <th>Gyermekek száma:</th>
	   <td><?=$gy;?> </td>
   </tr>
   <tr class="odd">
	   <th>Van a munkavállalónak súlyos látáskárosodása?</th>
	   <td><?=$vakboolean;?></td>
   </tr>
   <tr class="even">
	   <th>Van a munkavállalónak fogyatékkal rendelkező gyermeke?</th>
	   <td><?=$fogyboolean;?> </td>
   </tr>
   <tr class="odd">
	   <th>A föld alatt állandó jelleggel dolgozó, illetve <br />ionizáló sugárzásnak kitett munkahelyen <br />naponta legalább három órát eltölt?</th>
	   <td><?=$ionboolean;?></td>
   </tr>
   <tr class="even">
	   <th>Éves alapszabadság</th>
	   <td><?=$base_holiday;?> nap</td>
   </tr>
   <tr class="odd">
	   <th>Éves pótszabadság:</th>
	   <td><?=$extra_holiday;?> nap</td>
   </tr>
   <tr class="even">
	   <th>Összes éves szabadság:</th>
	   <td><?=number_format( ($extra_holiday+$base_holiday+$tav) ,0,".",".");?> nap</td>
   </tr>
   <tr class="odd">
	   <th>Ebből felhasználható szabadság<br /><?=$maidatum;?>-ig: </th>
	   <td><?=number_format( ($until_today_extra_holiday+$until_today_base_holiday+$tav) ,0,".",".");?> nap</td>
   </tr>                                    
</table>
