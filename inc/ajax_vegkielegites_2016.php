<?php
session_start();
header("Cache-control: private");
header('Content-Type: text/plain;charset=utf-8');


$patterns =array(' ','.',',');
$replacements=array('','','');
$br=str_replace($patterns, $replacements, ($_REQUEST['dats']*$_REQUEST['edt_brutto']));	

if($br>500000000){
	$br=500000000;
}   
if($br>7000000){ 
	$stat=0;
}
else{
	$stat=1;
}
	 
$elt=$_POST['edt_elt']; 
$gy=$_POST['edt_gyerekek'];
$egyedulnevel=$_POST['edt_egyedulnevel'];
$frisshazas=$_POST['edt_frisshazas'];
$kedvezmeny=$_POST['edt_kedvezmeny']; 
$munkaido=$_POST['edt_munkaido'];
$gyes=0;
$nypl=7942200; //Korábban 2007-ben 600000, majd 2008-ban 7137000, 2010-ben 7446000, 2011-ben 7665000
  
if ($egyedulnevel==1){
   switch ($elt) {
	case 0: 
	 $gyk=0; //ennyivel csökken az adó, neve: Családi adókedvezmény.
	 $cspotlek=0;
	break;
	case 1:
	 $gyk=$gy*10000; //ennyivel csökken az adó
	 $cspotlek=$gy*13700;		
	break;
	case 2:
	 $gyk=$gy*15000;
	 $cspotlek=$gy*14800;
	break;
	default:
	 $gyk=$gy*33000;  //ennyivel csökken az adó
	 $cspotlek=$gy*17000;
   }			
}
else{
   switch ($elt) {
	case 0:
	 $gyk=0; //ennyivel csökken az adó, neve: Családi adókedvezmény.
	 $cspotlek=0;
	break;
	case 1:
	 $gyk=$gy*10000; //ennyivel csökken az adó		
	 $cspotlek=$gy*12200;		
	break;
	case 2:
	 $gyk=$gy*15000;		
	 $cspotlek=$gy*13300;
	break;
	default:
	 $gyk=$gy*33000;  //ennyivel csökken az adó		
	 $cspotlek=$gy*16000;
   }	
}
	
$fhk=0;	 
$sbr=$br;
$ado=($sbr*12)*0.15;          // 15% 
$ado=round($ado/12);
if (($ado-$gyk-$fhk)>0){
	$gykado=$ado-$gyk-$fhk;
	$gykmarad=0;
}
else {
	$gykado=0;
	$gykmarad=$gyk+$fhk-$ado;
}
//2014-tol, ha nagyobb a családi adókedvezmény, mint az SZJA, akkor csökkentheto a családi adokedvezmennyel az egbizt (mvej) és a nyugdijjarulek (mvnyj) is.
if (($br*0.07-$gykmarad)>0){
	$mvej=$br*0.07-$gykmarad;
	$gykmarad=0;
}
else {
	$mvej=0;
	$gykmarad=$gykmarad-$br*0.07;
}
   
if (($br*0.10-$gykmarad)>0){
	$mvnyj=$br*0.10-$gykmarad;
	$gykmarad=0;
}
else {
	$mvnyj=0;
	$gykmarad=$gykmarad-$br*0.10;
}

$kedvezmenylimit=100000*$munkaido/40;
$szochjkedv=0;
	
// Egyéb kedvezmények
switch ($kedvezmeny) {
case 1:
 if ($br<$kedvezmenylimit) {
   $szochjkedv=$br*0.27; 				//0-ra csökkenthetjük max a szoc. hozzájárulást
   $szakkepzesikedv=$br*0.015; 	 //a teljes szakképzési hozzájárulás eltűnik	 
 }
 else{
   $szochjkedv=$kedvezmenylimit*0.27; 							   //nincs 100 e Ft felett további kedvezmény
   $szakkepzesikedv=$kedvezmenylimit*0.015; 				//nincs 100 e Ft felett további kedvezmény	 	 
 }
break;
case 2:
 if ($br<$kedvezmenylimit) {
   $szochjkedv=$br*0.145; 				//0-ra csökkenthetjük max a szoc. hozzájárulást
   $szakkepzesikedv=0; 	 				  //nincs szakképzési hozzájárulásra kedvezmény	 
 }
 else{
   $szochjkedv=$kedvezmenylimit*0.145; 					//nincs 100 e Ft felett kedvezmény
   $szakkepzesikedv=0; 	 											   //nincs szakképzési hozzájárulásra kedvezmény	 	 
 }	 
break;
case 3:
 if ($br<$kedvezmenylimit) {
   $szochjkedv=$br*0.145; 				//0-ra csökkenthetjük max a szoc. hozzájárulást
   $szakkepzesikedv=0; 	 				  //nincs szakképzési hozzájárulásra kedvezmény	 
 }
 else{
   $szochjkedv=$kedvezmenylimit*0.145; 					//nincs 100 e Ft felett kedvezmény
   $szakkepzesikedv=0; 	 											   //nincs szakképzési hozzájárulásra kedvezmény	 	 
 }	 
break;
case 4:
 if ($br<$kedvezmenylimit) {
   $szochjkedv=$br*0.27; 				//0-ra csökkenthetjük max a szoc. hozzájárulást
   $szakkepzesikedv=$br*0.015; 	 //a teljes szakképzési hozzájárulás eltűnik	 
 }
 else{
   $szochjkedv=$kedvezmenylimit*0.27; 					//nincs 100 e Ft felett további kedvezmény
   $szakkepzesikedv=$kedvezmenylimit*0.015; 				//nincs 100 e Ft felett további kedvezmény	 	 
 }
break;
case 5:
 if ($br<$kedvezmenylimit) {
   $szochjkedv=$br*0.145; 				//0-ra csökkenthetjük max a szoc. hozzájárulást
   $szakkepzesikedv=0; 	 				  //nincs szakképzési hozzájárulásra kedvezmény	 
 }
 else{
   $szochjkedv=$kedvezmenylimit*0.145; 					//nincs 100 e Ft felett kedvezmény
   $szakkepzesikedv=0; 	 											   //nincs szakképzési hozzájárulásra kedvezmény	 	 
 }	 
break;	
case 6:
 if ($br<$kedvezmenylimit) {
   $szochjkedv=$br*0.27; 				//0-ra csökkenthetjük max a szoc. hozzájárulást
   $szakkepzesikedv=$br*0.015; 	 //a teljes szakképzési hozzájárulás eltűnik	 
 }
 else{
   $szochjkedv=$kedvezmenylimit*0.27; 							   //nincs 100 e Ft felett további kedvezmény
   $szakkepzesikedv=$kedvezmenylimit*0.015; 				//nincs 100 e Ft felett további kedvezmény	 	 
 }
break;		
case 7:
 if ($br<$kedvezmenylimit) {
   $szochjkedv=$br*0.27; 				//0-ra csökkenthetjük max a szoc. hozzájárulást
   $szakkepzesikedv=$br*0.015; 	 //a teljes szakképzési hozzájárulás eltűnik	 
 }
 else{
   $szochjkedv=$kedvezmenylimit*0.27; 							   //nincs 100 e Ft felett további kedvezmény
   $szakkepzesikedv=$kedvezmenylimit*0.015; 				//nincs 100 e Ft felett további kedvezmény	 	 
 }
break;			 
case 8:
 if ($br<($kedvezmenylimit*5)) {
   $szochjkedv=$br*0.27; 				//0-ra csökkenthetjük max a szoc. hozzájárulást
   $szakkepzesikedv=$br*0.015; 	 //a teljes szakképzési hozzájárulás eltűnik	 
 }
 else{
   $szochjkedv=$kedvezmenylimit*0.27*5; 					//nincs 500 e Ft felett további kedvezmény
   $szakkepzesikedv=$kedvezmenylimit*0.015*5; 				//nincs 500 e Ft felett további kedvezmény	 	 
 }		
break;					 	
case 9:
 if ($br<75001) {
   $szochjkedv=$br*0.16; 				//12000-rel csökkenthetjük max a szoc. hozzájárulást
   $szakkepzesikedv=0; 	  //nincs szakképzési hozzájárulásra kedvezmény	 
 }
 else{
   $szochjkedv=12000-0.21818181818181818181818181818182*($br-75000); 					//nincs 130 e Ft felett egyáltalán kedvezmény
   if ($szochjkedv<0) $szochjkedv=0;
   $szakkepzesikedv=0; 				 //nincs szakképzési hozzájárulásra kedvezmény	 	  
 }		
break;						
default:
 $szochjkedv=0; //nincs kedvezmény
 $szakkepzesikedv=0; //nincs kedvezmény		 
}			
$adojovairas=0;
$nyjadokedv=0;
$szolado=0;   
if (($ado + $szolado - ($nyjadokedv + $adojovairas + $gyk + $fhk + $egyebkedv) /* osszes kedvezmeny */ ) > 0){ 
    $osszesado=$ado + $szolado - ($nyjadokedv + $adojovairas + $gyk + $fhk);
}
else{
    $osszesado=0;
}
$brosszeslevonas=($mvej)+($br*0.015)+($mvnyj)+$osszesado; //állami nyugdíjjárulék 1,5% 2008-tól, nem 0,5, 2011. január 13-ától 10% az állami, nincs magán. 6-ról 7% lett a munkavállalói eü. járulék
// osszes 2015-ös adó
if ($elt==2){ 
	$gyk2015 = $gyk - ($gy*2500); 
}
else {
	$gyk2015 = $gyk;
}
$ado2015 = $ado+($br*0.01);
   
if (($ado2015 + $szolado - ($nyjadokedv + $adojovairas + $gyk2015 + $fhk + $egyebkedv) /* osszes kedvezmeny */ ) > 0){ 
    $osszesado2015 = $ado2015 + $szolado - ($nyjadokedv + $adojovairas + $gyk2015 + $fhk);
}
else{
    $osszesado2015=0;
}
$diff2015 = $osszesado2015-$osszesado;
$bottomrows="";
$vk=number_format(($_REQUEST['dats']*$_REQUEST['edt_brutto']),0,".",".");

 echo"<div style=\"clear:both;height:10px;\"></div>";
  echo"<div style=\"width:300px;margin:0 auto;border:1px solid #ccc;padding:10px;text-align:center;font-weight:bold;'\">";
	echo "&Ouml;n ".$vk." Ft bruttó v&eacute;gkiel&eacute;g&iacute;t&eacute;sre jogosult!";	
	echo"</div>";
	 echo"<div style=\"clear:both;height:10px;\"></div>";

?>


<table class="md-margin table-style1 table-striped" cellpadding="0" cellspacing="0" >
   
   <tr class="odd">
	   <th>Bruttó végkielégítés:</th>
	   <td><?=number_format($br,0,".",".");?> Ft</td>
   </tr>

   <tr class="odd">
	   <th>Eltartottak száma *:</th>
	   <td><?=$elt;?></td>
   </tr>
   <tr class="even">
	   <th>Kedvezményezett eltartottak (gyermekek) száma **:</th>
	   <td><?=$gy;?></td>
   </tr>
   <tr class="odd">
	   <th>Szociális hozzájárulási adó:</th>
	   <td><?=number_format( ($br*0.27-$szochjkedv) ,0,".",".");?> Ft</td>
   </tr>
   <tr class="even">
	   <th>Szakképzési hozzájárulás:</th>
	   <td><?=number_format( ($br*0.015-$szakkepzesikedv) ,0,".",".");?> Ft</td>
   </tr>
   <tr class="odd">
	   <th>Munkaerő-piaci járulék (1,5%):</th>
	   <td><?=number_format( ($br*0.015) ,0,".",".");?> Ft</td>
   </tr>
   <tr class="even">
	   <th>Egészségbiztosítási járulék (7%):</th>
	   <td><?=number_format( ($mvej) ,0,".",".");?> Ft</td>
   </tr>
   <tr class="odd">
	   <th>Nyugdíjjárulék (10%):</th>
	   <td><?=number_format( ($mvnyj) ,0,".",".");?> Ft</td>
   </tr>                                      
   <tr class="even">
	   <th>GYES</th>
	   <td><?=number_format( $gyes ,0,".",".");?> Ft</td>
   </tr>
   <tr class="odd">
	   <th>Családi adókedvezmény:</th>
	   <td><?=number_format( $gyk ,0,".",".");?> Ft</td>
   <tr class="odd">
	   <th>Számított SZJA:</th>
	   <td><?=number_format( ($ado) ,0,".",".");?> Ft</td>
   </tr>
   <tr class="even">
	   <th>Családi adókedvezménnyel csökkentett SZJA:</th>
	   <td><?=number_format( $gykado ,0,".",".");?> Ft</td>
   </tr>
   <tr class="odd">
	   <th>Fel nem használt családi adókedvezmény:</th>
	   <td><?=number_format( $gykmarad ,0,".",".");?> Ft</td>
   </tr>                                        
   <tr class="even">
	   <th>Összes adó:</th>
	   <td><?=number_format( $osszesado ,0,".",".");?> Ft</td>
   </tr>                                        
   <tr class="odd">
	   <th>Adójóváírás:</th>
	   <td><?=number_format( $adojovairas ,0,".",".");?> Ft</td>
   </tr>                                                           
   <tr class="even">
	   <th>Felhasznált összes munkavállalói kedvezmény:</th>
	   <td><?=number_format( ($nyjadokedv + $adojovairas + $gyk - $gykmarad) ,0,".",".");?> Ft</td>
   </tr>                                        
   <tr class="odd">
	   <th>Összes levonás a bruttó bérből:</th>
	   <td><?=number_format( $brosszeslevonas ,0,".",".");?> Ft</td>
   </tr>                                          
   <tr class="even">
	   <th>Összes munkaadói kedvezmény:</th>
	   <td><?=number_format( ($szochjkedv+$szakkepzesikedv) ,0,".",".");?> Ft</td>
   </tr>                                        
   <tr class="odd">
	   <th>Összes munkaadói járulék:</th>
	   <td><?=number_format( (($br*0.22-$szochjkedv)+($br*0.015-$szakkepzesikedv)) ,0,".",".");?> Ft</td>
   </tr>                                                           
   <tr class="even">
	   <th>Összesen az államnak fizetendő:</th>
	   <td><?=number_format( (($br*0.22-$szochjkedv)+($br*0.015-$szakkepzesikedv)+$brosszeslevonas) ,0,".",".");?> Ft</td>
   </tr>                                        
   <tr class="odd">
	   <th>Munkaadó összes költsége:</th>
	   <td><?=number_format( (($br*0.22-$szochjkedv)+($br*0.015-$szakkepzesikedv)+$br) ,0,".",".");?> Ft</td>
   </tr>                                        
   <tr class="even">
	   <th class="secondary" >Nettó jövedelem:</th>
	   <td><?=number_format( ($br-$brosszeslevonas) ,0,".",".");?> Ft</td>
   </tr>                                          
   <tr class="odd">
	   <th>Családi pótlék:</th>
	   <td><?=number_format( ($cspotlek) ,0,".",".");?> Ft</td>
   </tr>                                        
   <tr class="even">
	   <th class="secondary" >Összes nettó jövedelem:</th>
	   <td><?=number_format( ($br-$brosszeslevonas+$cspotlek) ,0,".",".");?> Ft</td>
   </tr>                                                           
</table>

<div align="center" onclick="javascript:ad_counter();"><span class='purple' style="font-weight:bold;font-size:14px;cursor:pointer;" >Többet is kereshetnél, ha beszélnél nyelveket! >></span></div>   
<div align="center"><br /><a title='8 000 állás' href='http://legjob.hu' class='purple' target='_blank'>Keressen többet! &nbsp;<strong>8 000 állandóan friss állás</strong> - www.legjob.hu >></a></div>



<blockquote>
  <p><i>* Eltartottak száma:</i> ebbe beleértve a 18 év feletti, rendszeres jövedelemmel nem rendelkező, akkreditált iskolai rendszerű felsőfokú szakképzésben vagy főiskolai/egyetemi képzésben első képesítését/oklevelét szerző diák is. </p>
</blockquote>

<blockquote>
  <p><i>** Kedvezményezett eltartott gyermekek:</i> a családi pótlékra-, rokkantsági járadékra jogosult / jogosító gyermekek; a magzat (a fogantatás 91. napjától). </p>
</blockquote>

<blockquote>
  <p><i>**** Munkavállalót érintő kedvezmények:</i> A munkahelyvédelmi akcióterv keretében a 25 év alatti és 55 év feletti munkavállalókra; a 25 és 54 év közötti szakképzetlen munkavállalókra; a tartósan állást kereső munkavállalóra; a gyes, gyed, gyet folyósítását követően visszatérő munkavállalókra; szabad vállalkozási zónákban foglalkoztatott új munkavállalókra; valamint a kutatóként foglalkoztatottakra az első 2 vagy 3 évben a munkáltató kedvezményeket vehet igénybe. Így csökkenhető a teljes munkáltatói költség. </p>
</blockquote>
<?
mysql_close($connid);
?>
