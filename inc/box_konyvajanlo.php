<?
$sub=db_row("SELECT
                         a.id AS a_id,
                         a.teaser AS a_teaser,
                         a.s_lead AS a_s_lead,						 
                         a.large_pic AS a_large_pic,						 
                         a.date_added AS a_date_added,
                         a.priority AS a_priority,
						l.link as link,
                         c.name AS c_name
                        FROM
                         articles a 
                        LEFT JOIN 
                        	categories c ON (a.category_id=c.id)
                        LEFT JOIN
							links l ON (a.id=l.art_id) 	
                        WHERE
                         a.is_approved='yes'
                        AND
                         a.large_pic!=''
                        AND
                         a.is_editorial='no'
						AND 
							  a.date_publish<NOW()		 
						AND 
							c.id='79' 
						and
						l.def=1	
					    ORDER BY         
							  rand()  							     
                        LIMIT 1");
?>



<div class="hr-allasok md-margin">
	<h4><i class="fa fa-book pull-left"></i><a href="/konyvajanlo.html">Könyvajánló</a></h4>
	<a href="<?=$sub['link'];?>"><strong><?=$sub['a_teaser'];?></strong></a>
	<p><a href="<?=$sub['link'];?>"><img src="images/uploaded/<?=$sub['a_large_pic'];?>" class="img-left pull-left boximg-100"><?=$sub['a_s_lead'];?></a></p>
	
	
	<div style="clear:both;height:1px;"></div>
</div>
