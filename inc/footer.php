  <div class="row clearfix md-margin top30px border-top">

                        
                            <div class="col-md-3 md-margin">

                                <?
							  include("inc/box_video.php");                                           
							?>
                            </div>
                            
                            <div class="col-md-3 md-margin">
							<?
							  include("inc/box_subjektiv.php");                                           
							?>
                            </div>
                            
                            <div class="col-md-3 md-margin">

                                <h4><i class="fa fa-facebook-official"></i> Facebook</h4>
                                
                                <div class="fb-page" data-href="https://www.facebook.com/hrportal.hu/?fref=ts" data-tabs="timeline" data-width="250" data-height="150" data-small-header="true" data-adapt-container-width="false" data-hide-cover="true" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/hrportal.hu/?fref=ts"><a href="https://www.facebook.com/hrportal.hu/?fref=ts">HR Portal</a></blockquote></div></div>

                            </div>
                            
                            <div class="col-md-3 md-margin">

                              <?
                              include("inc/box_kulturalisajanlo.php");   
                              
                              ?>
                            </div>

                        
                    </div>

                    <div class="row clearfix md-margin top30px border-top">
                        <div class="col-md-12 column">
                            <div class="col-md-4">&copy; Copyright HR Portal - 2003 - <?=date("Y",time());?></div>
                            <div class="col-md-8">
                                <ul class="footer-links pull-right">
                                    <li><a href="/rss">RSS</a></li>
                                    <li>·</li>
                                    <li><a href="/download/aszf_hrportal.pdf">ÁSZF</a></li>
                                    <li>·</li>
                                    <li><a href="/impresszum.html">Impresszum</a></li>
                                    <li>·</li>
                                    <li><a href="http://hrportal.hu/download/hrportal_referencia.pdf">Referenciák</a></li>
                                    <li>·</li>
                                    <li><a href="/magunkrol.html">Magunkról</a></li>
                                    <li>·</li>
                                    <li><a href="/kapcsolat.html">Kapcsolat</a></li>
                                    <li>·</li>
                                    <li><a href="/adatvedelem.html">Adatvédelem</a></li>
                                    <li>·</li>
                                    <li><a href="/mediaajanlat.html">Médiaajánlat</a></li>
                                    <li>·</li>
                                    <li><a href="/download/HRportal_mediaajanlat_politikai_hirdetoknek_2016_oktober.pdf">Médiaajánlat politikai hirdetőknek</a></li>
                                </ul>

                            </div>

                        </div>

                    </div>
                    
                    <!-- ***** COPYRIGHT ***** -->
					<div class="row clearfix md-margin top30px border-top">
                        <div class="col-md-12 column">
							<div id="lableca" ></div>
							<script type="text/javascript">getlablec();</script>
						</div>
					</div>
					<div style="clear:both;height:10px;"></div>
                </div>
            </div>
        </div>
    </div>
    
	

	

    <script src="js/classie.js"></script>
	<script src="js/cbpAnimatedHeader.js"></script>
    
<?
if($PAGE=="mainpage"){
	?>		
		<script type="text/javascript">
		<!--//--><![CDATA[//><!--
		var pp_gemius_identifier = 'zD5KsPfNh34UjX013hiClsTWP1H1F3tGfUCkUR3MiPT.f7';
		// lines below shouldn't be edited
		function gemius_pending(i) { window[i] = window[i] || function() {var x = window[i+'_pdata'] = window[i+'_pdata'] || []; x[x.length]=arguments;};};
		gemius_pending('gemius_hit'); gemius_pending('gemius_event'); gemius_pending('pp_gemius_hit'); gemius_pending('pp_gemius_event');
		(function(d,t) {try {var gt=d.createElement(t),s=d.getElementsByTagName(t)[0],l='http'+((location.protocol=='https:')?'s':''); gt.setAttribute('async','async');
		gt.setAttribute('defer','defer'); gt.src=l+'://huadn.hit.gemius.pl/xgemius.js'; s.parentNode.insertBefore(gt,s);} catch (e) {}})(document,'script');
		//--><!]]>
		</script>
	<?
	
}
else{
	?>
		<script type="text/javascript">
		<!--//--><![CDATA[//><!--
		var pp_gemius_identifier = 'bDhFBhiFYYCqPthkhx47lXZJHSGNJKL9TT54fXM4747.D7';
		// lines below shouldn't be edited
		function gemius_pending(i) { window[i] = window[i] || function() {var x = window[i+'_pdata'] = window[i+'_pdata'] || []; x[x.length]=arguments;};};
		gemius_pending('gemius_hit'); gemius_pending('gemius_event'); gemius_pending('pp_gemius_hit'); gemius_pending('pp_gemius_event');
		(function(d,t) {try {var gt=d.createElement(t),s=d.getElementsByTagName(t)[0],l='http'+((location.protocol=='https:')?'s':''); gt.setAttribute('async','async');
		gt.setAttribute('defer','defer'); gt.src=l+'://huadn.hit.gemius.pl/xgemius.js'; s.parentNode.insertBefore(gt,s);} catch (e) {}})(document,'script');
		//--><!]]>
		</script>
	<?
}
?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-696329-1', 'auto');
  ga('send', 'pageview');

</script>  

<?
if(
	($pagedata['category_id']=="31" && $pagedata['type']=="category") || 
	($pagedata['category_id']=="112" && $pagedata['type']=="category") || 
	($pagedata['category_id']=="24" && $pagedata['type']=="category") || 
	$pagedata['art_id']=="111643" ||
	$pagedata['art_id']=="111461" ||
	$pagedata['art_id']=="111103" ||
	$pagedata['art_id']=="111236" ||
	$pagedata['art_id']=="111491" ||
	$pagedata['art_id']=="111768" ||
	$pagedata['art_id']=="110984" ||
	$pagedata['art_id']=="110352" ||
	$pagedata['art_id']=="110483" ||
	$pagedata['art_id']=="110555" ||
	$pagedata['art_id']=="111857" ||
	$pagedata['art_id']=="111322" ||
	$pagedata['art_id']=="114897" ||
	$pagedata['art_id']=="114493" ||
	$pagedata['art_id']=="114869" ||
	$pagedata['art_id']=="114412" ||
	$pagedata['art_id']=="115333" ||
	$pagedata['art_id']=="119410" ||
	$pagedata['art_id']=="115331" ||
	$pagedata['art_id']=="113044" ||
	$pagedata['art_id']=="111562"
	
	){
		
	$postcont.="
	<script type=\"text/javascript\">
	/* <![CDATA[ */
	/*var google_conversion_id = 1057478147;
	var google_conversion_label = \"AbhmCI3grlsQg6yf-AM\";
	var google_custom_params = window.google_tag_params;
	var google_remarketing_only = true;
	/* ]]> */
	/*</script>
	<script type=\"text/javascript\" src=\"//www.googleadservices.com/pagead/conversion.js\">
	</script>
	<noscript>
	<div style=\"display:inline;\">
	<img height=\"1\" width=\"1\" style=\"border-style:none;\" alt=\"\" src=\"//googleads.g.doubleclick.net/pagead/viewthroughconversion/1057478147/?value=1.00&amp;currency_code=HUF&amp;label=AbhmCI3grlsQg6yf-AM&amp;guid=ON&amp;script=0\"/>
	</div>
	</noscript>

	";
	echo $postcont;
}
?>




<!-- iBILLBOARD Ad Server one2many code -->
<!-- HowTo: All the ad codes must be above this code! -->
<!-- HowTo: It is good idea to place this code just below the last ad code. -->
<script type='text/javascript' charset='utf-8' src='http://go.cz.bbelements.com/bb/bb_one2n.js'></script>
<script type='text/javascript'>/* <![CDATA[ */ 
var bbkeywords=''; //fill in: 'key1;key2;..'
bmone2n.addPosition('23945.1.5.2','');
bmone2n.addPosition('23945.1.1.2','');
bmone2n.addPosition('23945.1.3.1','');
bmone2n.addPosition('23945.1.3.2','');
bmone2n.getAd('go.cz.bbelements.com',bbkeywords,''+(typeof(bburlparam)=='string'?'&'+bburlparam:''),'utf-8');
/* ]]> */</script>
<div id='bmone2t-23945.1.5.2' style='display:none'><script type='text/javascript'>/* <![CDATA[ */ bmone2n.makeAd('23945.1.5.2'); /* ]]> */</script></div>
<div id='bmone2t-23945.1.1.2' style='display:none'><script type='text/javascript'>/* <![CDATA[ */ bmone2n.makeAd('23945.1.1.2'); /* ]]> */</script></div>
<div id='bmone2t-23945.1.3.1' style='display:none'><script type='text/javascript'>/* <![CDATA[ */ bmone2n.makeAd('23945.1.3.1'); /* ]]> */</script></div>
<div id='bmone2t-23945.1.3.2' style='display:none'><script type='text/javascript'>/* <![CDATA[ */ bmone2n.makeAd('23945.1.3.2'); /* ]]> */</script></div>
<script type='text/javascript'>/* <![CDATA[ */ bmone2n.moveAd(); /* ]]> */</script>
</body>

</html>
