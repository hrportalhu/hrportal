<?php

$kw="";
$title="";
	if(isset($pagedata['art_id'])){
		$meta=db_one("SELECT teaser from articles where id='".$pagedata['art_id']."'");
		$mkeywords=db_one("SELECT mkeywords from articles where id='".$pagedata['art_id']."'");
		$descri=strip_tags(db_one("SELECT s_lead from articles where id='".$pagedata['art_id']."'"));
		$img=db_one("SELECT large_pic from articles where id='".$pagedata['art_id']."'");
		
		$title=str_replace("<br>", " ", $meta);
		$title=strip_tags($title); 
		if($mkeywords==""){  
			$kw=str_replace(" ", ", ", $title);  
		}
		else{
			$kw=str_replace(" ", ", ", $mkeywords);
		}
	}


?>   

<title><?php echo $title.""; ?></title>
    <META name="description" content="Humánerőforrás, HR Portal - minden a munka világából. <?=$descri;?>" >
    <META name="keywords" content="<?php echo $kw;?>">
    <META name="Author" content="hrportal.hu">
    <META HTTP-EQUIV="Content-Language" content="hu">
    <META name="robots" content="all">
    <META name="distribution" content="Global">
    <META name="revisit-after" content="2 days">
    <META name="rating" content="General">
    <META name="doc-type" content="Web Page">
	<META http-equiv="Content-Type" content="text/html; charset=utf-8">	

	<link rel="image_src" href="http://www.hrportal.hu/<?=$oldpage.$img;?>" />
	<!--<meta property="og:image" content="http://www.hrportal.hu/<?=$oldpage.$img;?>"/>-->
	<meta property="og:site_name" content="HRPortal.hu hírportál"/>
	<meta property="og:type" content="article"/>
	<meta property="fb:app_id" content="376913275663916"/>
	<meta property="og:url" content="http://www.hrportal.hu/<?=$pagedata['link'];?>"/>
	<meta property="og:title" content="<?=$pagedata['title'];?>"/>
	<meta property="og:locale" content="hu_HU"/>	
	<meta property="og:description" content="<?=strip_tags($pagedata['og_description']);?>"/>	
	  <script type="text/javascript" src="https://apis.google.com/js/plusone.js">
      {"parsetags": "explicit"}
    </script>
