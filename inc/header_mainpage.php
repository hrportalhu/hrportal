<title>Humánerőforrás / HR Portál | HR Portal - minden a munka világából</title>
<meta name="description" content="Emberi erőforrás, HR Portál | HR Portal - minden a munka világából. Munkaügyi, vállalati hírek, állások HR-területen, tanulmányok, munkajog-tár, bérkalkulátor, szabadság kalkulátor és sok minden más" />
<meta name="keywords" content="HR, emberi erőforrás, humánerőforrás, humán erőforrás, human resource, munka, munkaügy, karrier" />
<meta name="Author" content="hrportal.hu" />
<meta http-equiv="Content-Language" content="hu" /> 
<meta http-equiv="Cache-control" content="max-age">
<meta name="robots" content="all" />
<meta name="distribution" content="Global" />
<meta name="revisit-after" content="2 days" />
<meta name="rating" content="General" />
<meta name="doc-type" content="Web Page" />
