<?
$title="Béren kívüli juttások, Adómentes juttatások, Kafetéria, Cafetéria 2017-ben - HR Portal - minden a munka világából";
$descr="Béren kívüli juttatások, adómentes juttatások, kalkulátor, cafeteria, kafetéria 2017-ben";
$kw="Béren kívüli, juttatások, cafeteria, kafetéria";
if($uritags[1]=="sport"){
	$title="Sport belépő cafeteria, béren kívüli juttások, Adómentes juttatások, Kafetéria, Cafetéria 2017-ben - HR Portal - minden a munka világából";
	$descr="Sport belépő cafeteria, Béren kívüli juttatások, adómentes juttatások, kalkulátor, cafeteria, kafetéria 2017-ben";
	$kw="Sport, belépő, belépő, Béren kívüli, juttatások, cafeteria, kafetéria";
}
if($uritags[1]=="mhetkezes"){
	$title="Munkahelyi étkeztetés, béren kívüli juttások, Adómentes juttatások, Kafetéria, Cafetéria 2017-ben - HR Portal - minden a munka világából";
	$descr="Munkahelyi étkeztetés, Béren kívüli juttatások, adómentes juttatások, kalkulátor, cafeteria, kafetéria 2017-ben";
	$kw="Étkezés, belépő, Béren kívüli, juttatások, cafeteria, kafetéria";
}
if($uritags[1]=="penz"){
	$title="Készpénz cafeteria, béren kívüli juttások, Adómentes juttatások, Kafetéria, Cafetéria 2017-ben - HR Portal - minden a munka világából";
	$descr="Készpénz cafeteria, Béren kívüli juttatások, adómentes juttatások, kalkulátor, cafeteria, kafetéria 2017-ben";
	$kw="készpénz, belépő, Béren kívüli, juttatások, cafeteria, kafetéria";
}
if($uritags[1]=="onyp"){
	$title="Önkéntes nyugdíjpénztári hozzájárulás, béren kívüli juttások, Adómentes juttatások, Kafetéria, Cafetéria 2017-ben - HR Portal - minden a munka világából";
	$descr="Önkéntes nyugdíjpénztári hozzájárulás, Béren kívüli juttatások, adómentes juttatások, kalkulátor, cafeteria, kafetéria 2017-ben";
	$kw="Önkéntes, nyugdíjpénztári, hozzájárulás, belépő, Béren kívüli, juttatások, cafeteria, kafetéria";
}
if($uritags[1]=="epp"){
	$title="Egészségpénztári-, önsegélyező pénztári hozzájárulás, béren kívüli juttások, Adómentes juttatások, Kafetéria, Cafetéria 2017-ben - HR Portal - minden a munka világából";
	$descr="Egészségpénztári-, önsegélyező pénztári hozzájárulás, Béren kívüli juttatások, adómentes juttatások, kalkulátor, cafeteria, kafetéria 2017-ben";
	$kw="Egészségpénztári, önsegélyező, pénztári, hozzájárulás, belépő, Béren kívüli, juttatások, cafeteria, kafetéria";
}
if($uritags[1]=="szeszall"){
	$title="Széchenyi Pihenőkártya vendéglátás alszámla, béren kívüli juttások, Adómentes juttatások, Kafetéria, Cafetéria 2017-ben - HR Portal - minden a munka világából";
	$descr="Széchenyi Pihenőkártya vendéglátás alszámla, Béren kívüli juttatások, adómentes juttatások, kalkulátor, cafeteria, kafetéria 2017-ben";
	$kw="Széchenyi, Pihenőkártya, vendéglátás,, belépő, Béren kívüli, juttatások, cafeteria, kafetéria";
}
if($uritags[1]=="szeve"){
	$title="Széchenyi Pihenőkártya szállás alszámla, béren kívüli juttások, Adómentes juttatások, Kafetéria, Cafetéria 2017-ben - HR Portal - minden a munka világából";
	$descr="Széchenyi Pihenőkártya szállás alszámla, Béren kívüli juttatások, adómentes juttatások, kalkulátor, cafeteria, kafetéria 2017-ben";
	$kw="Széchenyi, Pihenőkártya, szállás, belépő, Béren kívüli, juttatások, cafeteria, kafetéria";
}
if($uritags[1]=="szeszab"){
	$title="Széchenyi Pihenőkártya szabadidő alszámla, béren kívüli juttások, Adómentes juttatások, Kafetéria, Cafetéria 2017-ben - HR Portal - minden a munka világából";
	$descr="Széchenyi Pihenőkártya szabadidő alszámla, Béren kívüli juttatások, adómentes juttatások, kalkulátor, cafeteria, kafetéria 2017-ben";
	$kw="Széchenyi, Pihenőkártya, szabadidő, belépő, Béren kívüli, juttatások, cafeteria, kafetéria";
}
if($uritags[1]=="lakas"){
	$title="Lakáshitel-cafeteria, béren kívüli juttások, Adómentes juttatások, Kafetéria, Cafetéria 2017-ben - HR Portal - minden a munka világából";
	$descr="Lakáshitel-cafeteria, Béren kívüli juttatások, adómentes juttatások, kalkulátor, cafeteria, kafetéria 2017-ben";
	$kw="Lakáshitel, belépő, Béren kívüli, juttatások, cafeteria, kafetéria";
}
if($uritags[1]=="biztositas"){
	$title="Kockázati biztosítás, béren kívüli juttások, Adómentes juttatások, Kafetéria, Cafetéria 2017-ben - HR Portal - minden a munka világából";
	$descr="Kockázati biztosítás, Béren kívüli juttatások, adómentes juttatások, kalkulátor, cafeteria, kafetéria 2017-ben";
	$kw="Kockázati, biztosítás, belépő, Béren kívüli, juttatások, cafeteria, kafetéria";
}
if($uritags[1]=="ajandek"){
	$title="Ajándékutalvány, béren kívüli juttások, Adómentes juttatások, Kafetéria, Cafetéria 2017-ben - HR Portal - minden a munka világából";
	$descr="Ajándékutalvány, Béren kívüli juttatások, adómentes juttatások, kalkulátor, cafeteria, kafetéria 2017-ben";
	$kw="Ajándékutalvány, belépő, Béren kívüli, juttatások, cafeteria, kafetéria";
}
if($uritags[1]=="kultura"){
	$title="Kúltúra utalvány, béren kívüli juttások, Adómentes juttatások, Kafetéria, Cafetéria 2017-ben - HR Portal - minden a munka világából";
	$descr="Kúltúra utalvány, Béren kívüli juttatások, adómentes juttatások, kalkulátor, cafeteria, kafetéria 2017-ben";
	$kw="Kúltúra, utalvány, belépő, Béren kívüli, juttatások, cafeteria, kafetéria";
}
?>	
	
	
	
	
	
	
	
	
	<title><?=$title;?></title>
    <META name="description" content="<?=$descr;?>">
    <META name="keywords" content="<?=$kw;?>">
    <META name="Author" content="">
    <META HTTP-EQUIV="Content-Language" content="hu">
    <META name="robots" content="all">
    <META name="distribution" content="Global">
    <META name="revisit-after" content="2 days">
    <META name="rating" content="General">
    <META name="doc-type" content="Web Page">
