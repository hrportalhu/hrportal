   <title>Bérkalkulátor 2013: Bruttó-nettó bér kiszámítása 2013. január 1-től</title>
    <META name="description" content="Mennyit keresünk 2013-ban? Kattintson! Bérkalkulátor, bruttó nettó bérek számítása 2013-ban  - Humánerőforrás Portál | HR Portal - a munkaerőpiac és bérszámfejtés szakértője">
    <META name="keywords" content="bérkalkulátor 2013, bérkalkulátor, nettó bruttó, bruttó-nettó bér számítás, bérkalkuláció, bér kalkulátor, berkalkulator, szja">
    <META name="Author" content="">
    <META HTTP-EQUIV="Content-Language" content="hu">
    <META name="robots" content="all">
    <META name="distribution" content="Global">
    <META name="revisit-after" content="2 days">
    <META name="rating" content="General">
    <META name="doc-type" content="Web Page">		
	
		<link rel="image_src" href="https://fbcdn-sphotos-a-a.akamaihd.net/hphotos-ak-ash3/538492_500639446626187_1429374450_n.jpg" />
	<meta property="og:image" content="https://fbcdn-sphotos-a-a.akamaihd.net/hphotos-ak-ash3/538492_500639446626187_1429374450_n.jpg"/>
	<meta property="og:site_name" content="HRPortal.hu hírportál"/>
	<meta property="og:type" content="article"/>
	<meta property="fb:app_id" content="376913275663916"/>
	<meta property="og:url" content="http://www.hrportal.hu/index.phtml?page=berkalkulator_2012"/>
	<meta property="og:title" content="Bérkalkulátor 2013"/>
	<meta property="og:locale" content="hu_HU"/>	
	  <script type="text/javascript" src="https://apis.google.com/js/plusone.js">
      {"parsetags": "explicit"}
    </script>
