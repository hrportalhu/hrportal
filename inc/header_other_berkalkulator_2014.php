   <title>Bérkalkulátor 2014: Bruttó-nettó bér kiszámítása</title>
    <META name="description" content="Mennyit keresünk 2014-ben? Kattintson! Bérkalkulátor, bruttó nettó bérek számítása 2014-ben - Humánerőforrás Portál | HR Portal - minden a munka világából - Bérkalkulátor, bruttó nettó bérek számítása">
    <META name="keywords" content="hr, nettó bruttó, bruttó-nettó bér számítás, humán erőforrás, humánerőforrás, munka, bérkalkulátor, bérkalkuláció, bér kalkulátor, bér kalkuláció, berkalkulator">
    <META name="Author" content="">
    <META HTTP-EQUIV="Content-Language" content="hu">
    <META name="robots" content="all">
    <META name="distribution" content="Global">
    <META name="revisit-after" content="2 days">
    <META name="rating" content="General">
    <META name="doc-type" content="Web Page">	
	
		<link rel="image_src" href="http://sphotos-a.ak.fbcdn.net/hphotos-ak-ash3/538492_500639446626187_1429374450_n.jpg" />
	<meta property="og:image" content="http://sphotos-a.ak.fbcdn.net/hphotos-ak-ash3/538492_500639446626187_1429374450_n.jpg"/>
	<meta property="og:site_name" content="HRPortal.hu hírportál"/>
	<meta property="og:type" content="article"/>
	<meta property="fb:app_id" content="376913275663916"/>
	<meta property="og:url" content="http://www.hrportal.hu/index.phtml?page=berkalkulator"/>
	<meta property="og:title" content="Bérkalkulátor: Bruttó-nettó bér kiszámítása	"/>
	<meta property="og:locale" content="hu_HU"/>	
	<meta property="og:description" content="Mennyit keresünk 2014-ben? Kattintson! Bérkalkulátor, bruttó nettó bérek számítása 2014-ben - Humánerőforrás Portál | HR Portal - minden a munka világából - Bérkalkulátor, bruttó nettó bérek számítása"/>	
	  <script type="text/javascript" src="https://apis.google.com/js/plusone.js">
      {"parsetags": "explicit"}
    </script>
