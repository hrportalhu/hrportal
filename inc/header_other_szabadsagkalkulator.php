   <title>Szabadság számoló, éves szabadság számítása, kiszámolása | Humánerőforrás Portál</title>
    <META name="description" content="Szabadság kalkulátor, éves szabadság számítása, kiszámolása - Szabadság kalkulátor, éves szabadság számítása, szabadság kiszámolása, életkor">
    <META name="keywords" content="humán erőforrás, szabadság kalkulátor, éves szabadság számítás, munkavállaló, munkahely, szabadság kiszámolása, szabad nap">
    <META name="Author" content="">
    <META HTTP-EQUIV="Content-Language" content="hu">
    <META name="robots" content="all">
    <META name="distribution" content="Global">
    <META name="revisit-after" content="2 days">
    <META name="rating" content="General">
    <META name="doc-type" content="Web Page">
    
    		<link rel="image_src" href="http://sphotos-a.ak.fbcdn.net/hphotos-ak-ash3/538492_500639446626187_1429374450_n.jpg" />
	<meta property="og:image" content="http://sphotos-a.ak.fbcdn.net/hphotos-ak-ash3/538492_500639446626187_1429374450_n.jpg"/>
	<meta property="og:site_name" content="HRPortal.hu hírportál"/>
	<meta property="og:type" content="article"/>
	<meta property="fb:app_id" content="376913275663916"/>
	<meta property="og:url" content="http://www.hrportal.hu/index.phtml?page=szabadsagkalkulator"/>
	<meta property="og:title" content="Szabadság számoló, éves szabadság számítása, kiszámolása | Humánerőforrás Portál"/>
	<meta property="og:locale" content="hu_HU"/>	
	<meta property="og:description" content="Szabadság kalkulátor, éves szabadság számítása, kiszámolása - Szabadság kalkulátor, éves szabadság számítása, szabadság kiszámolása, életkor"/>	
	  <script type="text/javascript" src="https://apis.google.com/js/plusone.js">
      {"parsetags": "explicit"}
    </script>
