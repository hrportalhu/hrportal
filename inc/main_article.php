<?php
$article=db_row("SELECT
                              a.id AS a_id,
                              a.teaser AS a_teaser,
                              a.lead AS a_lead,
                              a.body AS a_body,
                              a.small_pic AS a_small_pic,
                              a.small_pic_align AS a_small_pic_align,
                              a.large_pic AS a_large_pic,
                              a.large_pic_align AS a_large_pic_align,
                              a.date_last_modified AS a_date_last_modified,
                              a.date_added AS a_date_added,
                              a.date_publish AS a_date_publish,
                              a.expert_cat_id AS a_expert_cat_id,
                              a.sponsor_id AS a_sponsor_id,
                              a.keyword1 as kw1,
                              a.keyword2 as kw2,
                              a.keyword3 as kw3,
                              a.keyword4 as kw4,
                              a.keyword5 as kw5,
                              a.nl2br AS a_nl2br,
                              a.source AS a_source,
                              c.id AS c_id,
                              c.name AS c_name,
                              c.pname AS c_pname,
                              c.wname AS c_wname,							
                              c.pre1_pname AS c_pre1_pname,
                              c.pre1_wname AS c_pre1_wname,	
							  l.link	AS link,
							  j.id as j_id,
							  j.name as j_name,						  
                              cw.shown_name AS cw_shown_name
                             FROM
                              (articles a LEFT JOIN categories c ON a.category_id=c.id)
                             LEFT JOIN
                              contentwriters cw ON a.contentwriter_login=cw.login
							  LEFT JOIN
                              links l ON a.id=l.art_id
							  LEFT JOIN
                              jurnalist j ON a.journalist_id=j.id  
                             WHERE
                              a.id='".$pagedata['art_id']."'
								AND (a.is_approved='yes' or l.prevs=1)
                             AND
								l.status=1
							 AND
								l.def=1
                             ");
                             
             

$mykeywords=db_all("select ta.id as id,ta.tag_id as tagid,t.tagname as tagname from articles_tags ta left join tags t on (ta.tag_id=t.id) where ta.art_id='".$pagedata['art_id']."'");;


$catbanner="";
//if($pagedata['category_id']=="31" || $pagedata['category_id']=="112" ){
//	$catbanner="[b1]<center><p><a href=\"http://www.hrportal.hu/rdl/tagads/178/humanpriority.hu/blog/2016/06/ellenseg-vagy-barat-hr-es-it-it-es-hr/?utm_source=hrportal&utm_medium=cpc&utm_content=2016.07.15.&utm_campaign=ellens%C3%A9g%20vagy%20barat\" target=\"_blank\"><img style=\"margin-right: auto; margin-left: auto; display: block;\" src=\"/images/banner/human_priority/_HR_400x400px.jpg\" alt=\"\" width=\"236\" /></a></p><div style=\"clear: both; height: 5px;\"> </div><a href=\"http://www.hrportal.hu/rdl/tagads/178/humanpriority.hu/blog/2016/06/ellenseg-vagy-barat-hr-es-it-it-es-hr/?utm_source=hrportal&utm_medium=cpc&utm_content=2016.07.15.&utm_campaign=ellens%C3%A9g%20vagy%20barat\" target=\"_blank\">Oldjuk meg az egyenletet…ITT!</a></center>[b9]";
//}
$article['a_body']=$catbanner.$article['a_body'];
$belkeret="";
if($article['c_pname']!="heti_ajanlo"){
	$mlimit=2;
	if($pagedata['category_id']=="31" || $pagedata['category_id']=="112" ){
		$mlimit=1;	
	}
	$belkeret=gettagad($pagedata['art_id'],$mlimit);
}
$article['a_body']=$belkeret.$article['a_body'];

$tagsid=db_all("select at.*, t.tagname as tname from articles_tags at left join tags t on (at.tag_id=t.id) where at.art_id='".$pagedata['art_id']."'");

include("inc/main_category_sponsor.php");
?>
<div class="col-md-8 column">
	<ol class="breadcrumb">
		<li><a href="/">Főoldal</a></li>
		<li><a href="/<?=$article['c_pname'];?>.html"><?=$article['c_wname'];?></a></li>
	</ol>
	<div class="tags">
		<?
		if(count($mykeywords)>0){
				
				for($k=0;$k<count($mykeywords);$k++){
					$returnValue = safe_filename($mykeywords[$k]['tagname']);
					if($returnValue!=""){
						$linkem="/cimkek/".$returnValue.".html";
						$chlink=db_one("select * from links where type='tag' and link='".$linkem."'");
						if($chlink==""){
							db_execute("insert into links (art_id,type,publish_date,link,title,og_title,def) value 
							('".$mykeywords[$k]['tagid']."','tag',NOW(),'".$linkem."','".$mykeywords[$k]['tagname']."','".$mykeywords[$k]['tagname']."','1')");	
						}
					}
					//echo"<a href=\"http://www.hrportal.hu/cimkek/".rawurlencode($mykeywords[$k]['tagname'])."\"></a>, ";
					echo"<a href=\"/cimkek/".$returnValue.".html\"><span class=\"label label-default\">".$mykeywords[$k]['tagname']."</span></a>&nbsp; ";
				}	
			}
		?>

	</div>
	<div class="cikk-info-bar clearfix">
			<?
			$imgdata="";
				if($article['j_id']=="15"){
					$imgdata="<img src=\"/images/journalist/szilagyikatalin.jpg\" alt=\"Szilágyi Katalin\" class=\"pull-left jurnalistpic\" />";
				}
				elseif	($article['j_id']=="5"){
					$imgdata="<img src=\"/images/journalist/gimothyeva.jpg\" alt=\"Gyimóthy Éva\" class=\"pull-left jurnalistpic\"  />";
				}
				elseif	($article['j_id']=="28"){
					$imgdata="<img src=\"/images/journalist/kerteszdalma.jpg\" alt=\"Kertész Dalma\" class=\"pull-left jurnalistpic\" />";
				}
				elseif	($article['j_id']=="8"){
					$imgdata="<img src=\"/images/journalist/meszaros_etelka_50.jpg\" alt=\"Mészáros Etelka\" class=\"pull-left jurnalistpic\"  />";
				}
				elseif	($article['j_id']=="32"){
					$imgdata="<img src=\"/images/journalist/szegedijuli.jpg\" alt=\"Szegedi Juli\"  class=\"pull-left jurnalistpic\" />";
				}
				echo $imgdata;
				if($article['j_name']==""){
					$article['j_name']="HRPortal.hu hírszerkesztő";
					echo"<span><i class=\"fa fa-pencil\"></i> Szerző: </span><a href=\"#\" class=\"link\">".$article['j_name']."</a><span class=\"pull-right\"><i class=\"fa fa-calendar\"></i> Megjelent: ".ezDate($article['a_date_publish'])."</span>";	
				}
				else{
					echo"<span><i class=\"fa fa-pencil\"></i> Szerző: </span><a href=\"/ujsagiro/".$article['j_id']."/".safe_filename($article['j_name']).".html\" class=\"link\">".$article['j_name']."</a><span class=\"pull-right\"><i class=\"fa fa-calendar\"></i> Megjelent: ".ezDate($article['a_date_publish'])."</span>";	
				}
				
			?>	
		
	</div>
	<article>
		<div class="row clearfix md-margin">
			<div class="col-md-12 column">
				<div class="media">
					<h1>
						<?
							echo $article['a_teaser'];
							if($article['a_sponsor_id']!=0){
								$spfile=db_row("select * from files where modul='partners' and type='partners' and type_id='".$article['a_sponsor_id']."' and pspec='1' and status=1");	
								if(substr($spfile['megjegyzes'],0,7)=="http://"){
										echo"<span style=\"float:right;max-height:80px;max-width:200px;\"><a href=\"".$spfile['megjegyzes']."\" target=\"_blank\"><img src=\"hrportaltrack/".$spfile['filename']."\"></span>";
								}
								else{
										echo"<span style=\"float:right;max-height:80px;max-width:200px;\"><img src=\"hrportaltrack/".$spfile['filename']."\"></span>";
								}
								//echo "select * from files where modul='parners' and type='partners' and type_id='".$article['a_sponsor_id']."' and pspec='1' and status=1";
								
							}
						?>
					</h1>
					<div class="media-body">
						
  
						<?
						$mp=getartpic($article['a_id']);
						if($mp!=""){
							
							$my_image = array_values(getimagesize($oldpage.$mp));
							list($width, $height, $type, $attr) = $my_image;
							$resp=" img-smaller ";if($width>"320"){$resp=" img-responsive ";}
						
							if($width<"320"){
								echo"<div class=\"clearfix\">
								<a href=\"\"><img alt=\"\" src=\"".($oldpage.$mp)."\" class=\"xs-margin pull-left separator img-border\"></a> 
								<p class=\"lead\">".$article['a_lead']."</p>
								</div>"; 
					
								echo"<div class=\"row clearfix\">";
									echo"<div class=\"col-md-12 col-sm-12 column\">";
										echo"<div class=\"banner-box md-margin\">";
											if($pagedata['category_id']!="4"){
												include("inc/ad_mainpage_468_1.php");		
											}
										echo"</div>";
									echo"</div>";
								echo"</div>";
					
							}
							else{
								echo"<div class=\"clearfix\">
								
								<p class=\"lead\">".$article['a_lead']."</p>
								</div>"; 
								echo"<div class=\"row clearfix\">";
									echo"<div class=\"col-md-12 col-sm-12 column\">";
										echo"<div class=\"banner-box md-margin\">";
											if($pagedata['category_id']!="4"){
												include("inc/ad_mainpage_468_1.php");		
											}
										
										echo"</div>";
									echo"</div>";
								echo"</div>";
								if($mp!=""){
									echo"<a href=\"\"><img alt=\"\" src=\"".($oldpage.$mp)."\" class=\"xs-margin ".$resp."\"></a>";
								}
							}
						}
						else{
							echo"<div class=\"clearfix\">
								
								<p class=\"lead\">".$article['a_lead']."</p>
								</div>"; 
								echo"<div class=\"row clearfix\">";
									echo"<div class=\"col-md-12 col-sm-12 column\">";
										echo"<div class=\"banner-box md-margin\">";
											if($pagedata['category_id']!="4"){
												include("inc/ad_mainpage_468_1.php");		
											}										
										echo"</div>";
									echo"</div>";
								echo"</div>";
						}
						
						
						?>
						<?=nl2br(borderedparse(albumparse(pictureparse($article['a_body']))));?>   
						
					</div>
				</div>
			</div>
		</div>
		<?
		if($article['a_expert_cat_id']!=0){
			$spots=db_all("select * from links where category_id='".$article['a_expert_cat_id']."' and (type='article' or type='news') order by publish_date desc limit 5");
			echo"<h3>Legutóbbi kérdések - válaszok</h3>";
			echo"<ul class=\"sm-margin\">";
			for($a=0;$a<count($spots);$a++){
				echo"<li><i class=\"fa fa-angle-right\"></i> <a href=\"".$spots[$a]['link']."\">".$spots[$a]['title']."</a></li>";
			}	
			echo"</ul>";
		}
		
		$pns=db_one("SELECT c.name AS c_name FROM (articles a LEFT JOIN categories c ON a.category_id=c.id) LEFT JOIN contentwriters cw ON a.contentwriter_login=cw.login  WHERE a.id='".$article['a_id']."'");	
			if($pns=="K_Munkajog" || $pns=="K_Cafeteria" || $pns=="K_Karriertanácsadás" || $pns=="K_Coaching"  || $pns=="K_Megváltozott_munkaképesség"  || $pns=="K_HR-kérdések" || $pns=="K_Diákmunka"  || $pns=="k_Relokáció" || $pns=="K_Adózási_kérdések" || $pns=="k_HR_Szoftver_Szakerto" || $pns=="K_Nyugdij_kerdesek" || $pns=="K_Outplacement" ){
				include("inc/other_artcleproffesional.php");	
			}
		?>
			                        <div class="row clearfix">
		                        <div class="col-md-12 col-sm-12 column">
			                        <div class="banner-box md-margin">
		                                    <?
												include("inc/ad_mainpage_468_2.php");
											?>
		                            </div>
		                        </div>
		                    </div>
		
	</article>
	<div class="fb-comments" data-href="http://www.hrportal.hu/<?=$pagedata['link']?>" data-numposts="5" data-width="100%" data-colorscheme="light"></div>
	<div class="fb-like" data-href="http://www.hrportal.hu<?=$pagedata['link']?>" data-layout="standard" data-action="recommend" data-show-faces="true" data-share="true"></div>
	<div style="clear:both;height:1px;"></div> 
	<script src="//platform.linkedin.com/in.js" type="text/javascript">
	lang: en_US
	</script>
	<script type="IN/Share" data-url="http://www.hrportal.hu<?=$pagedata['link']?>" data-counter="right"></script>

	<div id="plusone-div" > <g:plusone size="normal"></g:plusone></div>
	<script type="text/javascript">
	gapi.plusone.go("plusone-div");
	</script>

	<div style="width:30px;float:left;"><a href="http://www.twitter.com/hrportal_hu"><img src="http://twitter-badges.s3.amazonaws.com/t_small-a.png" alt="Follow hrportal_hu on Twitter" style="float:left;" /></a></div>

	<div class="helpers">
		<p>
			<?
			if ($a_source!=''){
				echo "<span><i>Forrás:</i></span> <a href=\"#\" class=\"link\"><i>".$a_source."</i></a></div>";
			}
			?>
			
		<a href="javascript:history.go(-1);" class="link pull-right"><i class="fa fa-angle-double-left"></i> vissza</a></p>
	</div>

	<div class="helpers">
		<p>
			<a href="/article_print.php?id=<?=$pagedata['art_id'];?>" target="_blank"><i class="fa fa-print"></i> Nyomtatható verzió</a>
			<!--<a href="" onClick="window.open('http://www.hrportal.hu/article_send.php?id=<?=$pagedata['art_id'];?>','','width=400,height=400,scrollbars=no,resizable=yes');" class="pull-right"><i class="fa fa-paper-plane"></i> Küldje el ezt a cikket ismerősének!</a>-->
		</p>
		<p> 
			<a href="/hirlevreg.html"><i class="fa fa-envelope"></i> Iratkozzon fel Hírlevelünkre!</a>
			<a href="/publikaljon.html" class="pull-right"><i class="fa fa-pencil"></i> Publikáljon Ön is a HR Portálon!</a>
		</p> 
	</div>
	<?
	include("inc/main_article_sponsorbox.php");
	
			$q2="select 
				l.*, 
				a.spot1 AS a_spot1, 
				a.spot2 AS a_spot2,
				a.spot3 AS a_spot3,
				a.large_pic AS a_large_pic, 
				c.name as cname,
				c.wname as cwname, 
				c.pname as cpname 
				from links l 
					LEFT JOIN categories c ON l.category_id=c.id  
					LEFT JOIN articles a ON l.art_id=a.id  
				where
				a.id!='".$pagedata['art_id']."' and 
				 l.category_id='".$pagedata['category_id']."' and l.art_id!=0 and l.type='article' and l.def=1 and l.aproved=1 ORDER BY  l.priority DESC,  l.publish_date DESC 	LIMIT 1";
			
			$rovat=db_all($q2);
	

	for($i=0;$i<count($rovat);$i++){
		$spots=db_all("SELECT  s.id AS s_id,s.teaser AS s_teaser ,	l.link AS link 	FROM articles s LEFT JOIN links l ON (s.id=l.art_id)  WHERE 	s.is_approved='yes' AND	l.def=1 AND l.aproved=1 and (s.id IN ('".$rovat[$i]['a_spot1']."', '".$rovat[$i]['a_spot2']."', '".$rovat[$i]['a_spot3']."' )) ORDER BY s.priority DESC, s.date_added DESC LIMIT 3");
		
		echo"<article>";
			echo"<div class=\"block-title\"><a href=\"/".$rovat[$i]['cpname'].".html\">".$rovat[$i]['cwname']."</a></div>";
			echo"<div class=\"row clearfix md-margin\">";
				echo"<div class=\"col-md-12 column\">";
					echo"<div class=\"media\">";
						echo"<h4><a href=\"".$rovat[$i]['link']."\">".$rovat[$i]['title']."</a></h4>";
						echo"<div class=\"media-body\">";
						if($rovat[$i]['a_large_pic']!=""){
							echo"<a href=\"".$rovat[$i]['link']."\"><img alt=\"\" src=\"".$oldpage.$rovat[$i]['a_large_pic']."\" class=\"xs-margin img-responsive\"></a>";
						}
							echo"<p>".$rovat[$i]['og_description']."<a class=\"more-btn\" href=\"".$rovat[$i]['link']."\" >tovább..</a></p>";
						echo"</div>";
						echo"<ul class=\"sm-margin\">";
						for($a=0;$a<count($spots);$a++){
							echo"<li><i class=\"fa fa-angle-right\"></i><a href=\"".$spots[$a]['link']."\"> ".$spots[$a]['s_teaser']."</a></li>";
						}	
						echo"</ul>";
					echo"</div>";
				echo"</div>";
			echo"</div>";
		echo"</article>";
	}
	
	$ne=db_all("select * from links l where l.category_id='".$article['c_id']."' and l.art_id!=0 and l.type='article' or l.type='news' and l.def=1 and l.aproved=1 ORDER BY  l.priority DESC, l.publish_date DESC 	LIMIT 2,5");							 
	$con=db_all("select * from links l where l.art_id!=0 and l.type='news' and l.def=1 and l.aproved=1  ORDER BY  l.priority DESC, l.publish_date DESC 	LIMIT 11");							 

	echo"<div class=\"row clearfix \">";
		echo"<div class=\"col-md-6 column md-margin\">";
			echo"<div class=\"block-title\"><span>További cikkek</span></div>";
			echo"<div class=\"row clearfix\">";
				for($i=0;$i<count($ne);$i++){
					echo"<div class=\"col-lg-12 col-md-12 col-sm-6 column\">";
						echo"<a href=\"".$ne[$i]['link']."\"><strong>".$ne[$i]['title']."</strong></a>";
						echo"<p>".limit_text(strip_tags($ne[$i]['og_description']),160)." <a class=\"more-btn\" href=\"".$ne[$i]['link']."\">Teljes cikk</a></p>";
					echo"</div>";
				}
				
			echo"</div>";
		echo"</div>";
		
		
		
		                                 
		echo"<div class=\"col-md-6 column md-margin\">";
			echo"<div class=\"block-title\"><span>Kapcsolódó hírek</span></div>";
				echo"<ul class=\"news-list\">";
				for($i=0;$i<count($con);$i++){
					echo"<li><a href=\"".$con[$i]['link']."\">".strip_tags($con[$i]['title'])."&nbsp;<span class=\"time\">".ezDate($con[$i]['publish_date'])."</span></a></li>";
				}
				echo"</ul>";
			echo"</div>";
		echo"</div>";

	
	?>
</div>
<div id="scarab_flyer">
	<!-- http://www.hrportal.hu/rdw/artid/31/34/1/beuszo/www.planetstar.hu-->
	
	<div class="banner" style="margin:5px;auto;width:320px; ">
			<div id='div-gpt-ad-1461936145403-18'>
			<script type='text/javascript'>
			googletag.cmd.push(function() { googletag.display('div-gpt-ad-1461936145403-18'); });
			</script>
			</div>
			</div>
	
	<div style="margin:5px;auto;width:320px; "><a href="http://www.hrportal.hu/rdl/tagads/1000193/onestep.hu" target="_blank"><img src="/images/banner/onestep/Onestep_logo_320x100.jpg" alt="onestep" /></a></div>
	<?
		//and  (MATCH (lead) AGAINST ('".$_POST['kfsearch']."')
	$rnum=rand(0,1);	
/*	
	if($rnum==0){	
		$qri="select 
				at.art_id as a_id, 
				a.teaser AS a_teaser, 
				a.lead as a_lead, 
				a.small_pic as s_pic,
				a.main_pic as m_pic,  
				l.link as link 
				from articles_tags at left join articles a on at.art_id=a.id LEFT JOIN links l ON (a.id=l.art_id) 	  where at.art_id>0 and at.art_id!='".$pagedata['art_id']."' AND l.def=1 and a.is_approved='yes' and (";
		for($i=0;$i<count($tagsid);$i++){
			$qri.=" at.tag_id='".$tagsid[$i]['tag_id']."' or ";	
		}
		$szam=3;
		if($szam==0){$szam=1;}
		$qri=substr($qri,0,-3).") group by at.art_id  order by at.art_id DESC limit ".$szam."";
		$myrel=db_all($qri);
	}
	else{
	*/ 
		$tq="";
		for($i=0;$i<count($tagsid);$i++){
			$tq.=$tagsid[$i]['tname']." ";	
		}
		//$mtq="and  (MATCH (keywords) AGAINST ('".$tq."')";
			$qri="select a.id as a_id, 
					a.teaser as a_teaser, 
					a.lead as a_lead, 
					a.small_pic as s_pic, 
					a.main_pic as m_pic, 
					l.link as link, 
					a.mkeywords as kw from articles a LEFT JOIN links l ON (a.id=l.art_id) where a.id!='".$pagedata['art_id']."' and l.aproved=1 and (MATCH (mkeywords) AGAINST ('".$tq."')) limit 3";
			$myrel=db_all($qri);	
	//}	
	//echo $pagedata['art_id'];
		//echo $qri;
		//print_r($myrel);
	?>
	<h3>
       <span style="float:left;width:180px;">Kapcsolódó tartalmaink</span>
       <span class="scarabbutton scarabact" id="scbut1" onclick="presscont1();">1</span>
       <span class="scarabbutton" id="scbut2" onclick="presscont2();">2</span>
       <span class="scarabbutton" id="scbut3" onclick="presscont3();">3</span>
        <span id="scarab_flyer_close" onclick="javascript:closefl();" title="Bezár" style="cursor:pointer;float:right;font-size:10px;">Bezár</span>
    </h3>
    <div style="clear:both;height:10px;"></div>
<?
	for($i=0;$i<count($myrel);$i++){
		$mycont="cont".($i+1);
		if($i==0){$disp=" block";}else{$disp=" none";}
		$pic="images/hr.jpg";
		if($myrel[$i]['s_pic']!=""){
			$pic="images/uploaded/".$myrel[$i]['s_pic'];
		}
		if($myrel[$i]['m_pic']!=""){
			$pic="images/uploaded/".$myrel[$i]['m_pic'];
		}
		?>
		<div class="cont" id="<?=$mycont;?>" style="display:<?=$disp;?>;" >
        <div style="width:80px;float:left;">   
        <a class="pic" href="<?=$myrel[$i]['link'];?>">
           <img src="<?=$pic;?>" alt="hr" width="80" />
       </a>
       </div>
       <div style="width:210px;float:right;">
      <h4>
		  <?
		  if($rnum==0){	
		  ?>
           <a href="http://www.hrportal.hu/rdw/artid/<?=$pagedata['art_id'];?>/<?=$myrel[$i]['a_id'];?>/1/beuszo/www.hrportal.hu<?=$myrel[$i]['link'];?>" title="<?=$myrel[$i]['a_teaser'];?>"><?=$myrel[$i]['a_teaser'];?></a>
           <?
	   }
	   else{
		  ?>
           <a href="http://www.hrportal.hu/rdw/artid/<?=$pagedata['art_id'];?>/<?=$myrel[$i]['a_id'];?>/2/beuszo/www.hrportal.hu<?=$myrel[$i]['link'];?>" title="<?=$myrel[$i]['a_teaser'];?>"><?=$myrel[$i]['a_teaser'];?></a>
           <?
		   
		  }
           ?>
       </h4>
      <div>
       
       <div ><?=limit_text(strip_tags($myrel[$i]['a_lead']),150);?></div>
	</div>
      </div>
       <div style="clear:both;height:2px;"></div>
   </div>
   <div style="clear:both;height:1px;"></div>
	
		<?
	}
	
?>
</div>
<?
if($rnum==1){
?>
<script>
	if(screen.width<1023){
		var magas=0.15;
	}
	else{
		var magas=0.50;	
	}
//	setscarabtimer();
//	var timer = setInterval(setscarabtimer, 9000);	
</script>	
<?
}
else{
?>
<script>
	if(screen.width<1023){
		var magas=0.15;
	}
	else{
		var magas=0.66;
	}
//	setscarabtimer();
//	var timer = setInterval(setscarabtimer, 9000);	
</script>	
<?
	
}
?>
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "NewsArticle",
  "mainEntityOfPage": {
    "@type": "WebPage",
    "@id": "<?=$pagedata['link'];?>"
  },
  "headline": "<?=$article['a_teaser'];?>",
  "image": {
    "@type": "ImageObject",
    "url": "http://www.hrportal.hu/<?=$oldpage.$mp;?>",
    "height": <?=$height;?>,
    "width": <?=$width;?>
  },
  "datePublished": "<?=$article['a_date_publish'];?>",
  "dateModified": "<?=$article['a_date_last_modified'];?>",
  "author": {
    "@type": "Person",
    "name": "<?=$article['a_teaser'];?>"
  },
   "publisher": {
    "@type": "Organization",
    "name": "HRPortal.hu",
    "logo": {
      "@type": "ImageObject",
      "url": "http://www.hrportal.hu/img/hr-portal-logo2.jpg",
      "width": 120,
      "height": 30
    }
  },
  "description": "<?=$article['a_lead'];?>"
}
</script>       
<script>
	<?
		if(substr($article['c_name'],0,2)=="K_"){
		?>
			$('.nav-tabs a[href="#panel-3"]').tab('show');
		<?
		}
		if($pagedata['art_id']=="features"){
		?>
			$('.nav-tabs a[href="#panel-3"]').tab('show');
		<?
		}
	?>
	addclcounter('<?=$pagedata['art_id'];?>');
</script>
