<?php
$mainarticle=db_row("SELECT
                              a.id AS a_id,
                              a.s_teaser AS a_s_teaser,
                              a.m_lead AS a_m_lead,						  
                              a.picalttag AS a_picalttag,							  
                              a.small_pic AS a_small_pic,
                              a.main_pic AS a_main_pic,
                              a.date_last_modified AS a_date_last_modified,
                              a.nl2br AS a_nl2br,
                              c.name AS c_name,
                              l.link AS link,
                              cw.shown_name AS cw_shown_name
                             FROM
                              articles a 
                             LEFT JOIN categories c 
                             	ON (a.category_id=c.id)
							 LEFT JOIN
							  	links l ON (a.id=l.art_id)
                             LEFT JOIN
                              	contentwriters cw ON a.contentwriter_login=cw.login
                             WHERE
                              a.is_approved='yes'
                             AND
                              a.is_editorial='yes'
							 AND 
							  a.date_publish<NOW()
							 AND
								l.status=1
							 AND
								l.def=1
								
                             LIMIT 1");
                             
$mainnews=db_all("select * from links where (type='news' or type='article') and status=1 and publish_date< NOW() and def=1 and aproved=1 order by publish_date desc limit 8");                             
$mainfresharticles=db_all("select * from links where status=1 and type='article' and publish_date< NOW() and def=1 and aproved=1 order by publish_date desc limit 8");                             
$mainhrtop=db_all("select 
						l.*,
						a.large_pic as l_pic,
						a.small_pic as s_pic,
						a.main_pic as m_pic, 
						a.s_lead as slead 
					from 
						links l 
					left join 
						articles a on (l.art_id=a.id) 
					where  
						(l.type='article' or l.type='news')  
						and l.status=1 
						and (a.large_pic!='' or a.main_pic!='' or a.small_pic!='') 
						and l.publish_date< NOW() 
						and a.priority=0 
						and l.def=1 
						and a.is_approved='yes'  
						and a.is_editorial='no'
						and l.category_id!='96' 
					order by 
						l.publish_date
					desc limit 2");                             
$maintoparticles=db_all("select l.* from links l left join articles_counter ac on (l.art_id=ac.art_id) where l.type='article' and l.publish_date  >= NOW() - INTERVAL 14 DAY and l.status=1 and l.publish_date< NOW() and l.def=1 and aproved=1 and l.title!='' order by ac.click_counter_toplist  desc limit 8");                             

?>
                    <div class="row clearfix">
	                    <div class="col-md-9 col-sm-12 column  md-margin">
		                    <div class="row clearfix md-margin">
	                        	<div class="col-md-9 column">
									<a href="<?=$mainarticle['link'];?>"> <img alt="" src="<?=($oldpage.$mainarticle['a_main_pic']);?>" class="sm-margin img-responsive"></a>
									<h1><a href="<?=$mainarticle['link'];?>"><?=$mainarticle['a_s_teaser'];?></a></h1>
									<p><?=$mainarticle['a_m_lead'];?></p>
									<p>
										<a class="more-btn" href="<?=$mainarticle['link'];?>">Teljes cikk   »</a> <a class="more-btn" href="/hr-szakmai.html">Több HR-szakmai cikk  »</a>
									</p>
								</div>
								<div class="col-md-3 column">
		                        <div class="tabbable" id="hir-nav">
			                        
		                            <ul class="nav nav-tabs sm-margin responsive">
										<li class="active"><a href="#hir-1" data-toggle="tab">HÍREK</a></li>
									    <li><a href="#hir-2" data-toggle="tab">NÉPSZERŰ</a></li>
									    <!-- 04.19. MÓDOSÍTÁS -->
									    <li><a href="#hir-3" data-toggle="tab">SZAKMA</a></li>
									    <!-- 04.19. MÓDOSÍTÁS -->
									</ul>
									
									<div class="tab-content responsive">
	                                        <div class="tab-pane active" id="hir-1">
	                            
					                           <ul class="news-list">
													<?php
														for($i=0;$i<count($mainnews);$i++){
															echo"<li><i class=\"fa fa-angle-right\"></i> <a href=\"".$mainnews[$i]['link']."\">".strip_tags($mainnews[$i]['title'])." <span class=\"time\">".ezdate($mainnews[$i]['publish_date'])."</span></a></li>";
														}
													?>
												</ul>
					                            
					                        </div> 
					                        <div class="tab-pane" id="hir-2">
	                            
											<ul class="news-list">
												<?php
													for($i=0;$i<count($maintoparticles);$i++){
														echo"<li><i class=\"fa fa-angle-right\"></i> <a href=\"".$maintoparticles[$i]['link']."\">".strip_tags($maintoparticles[$i]['title'])." <span class=\"time\">".ezdate($maintoparticles[$i]['publish_date'])."</span></a></li>";
													}
												?>
											</ul>
					                            
					                        </div>
					                        
					                        <!-- 04.19. MÓDOSÍTÁS -->   
					                        <div class="tab-pane" id="hir-3">
	                            
					                          <ul class="news-list">
												<?php
													for($i=0;$i<count($mainfresharticles);$i++){
														echo"<li><i class=\"fa fa-angle-right\"></i> <a href=\"".$mainfresharticles[$i]['link']."\">".strip_tags($mainfresharticles[$i]['title'])." <span class=\"time\">".ezdate($mainfresharticles[$i]['publish_date'])."</span></a></li>";
													}
												?>
											</ul>
					                            
					                        </div>  
					                        <!-- 04.19. MÓDOSÍTÁS -->
					                </div>         
								</div>	
	                        </div>
	                        
	                        </div>
	                        
	                        
	                        <div class="row clearfix">
		                        <div class="col-md-12 col-sm-12 column">
			                        <div class="banner-box md-margin">
		                                    <?
												include("inc/ad_mainpage_468_2.php");
											?>
		                            </div>
		                        </div>
		                    </div>
                        
                        </div>

                        <div class="col-md-3 col-sm-12 column">
                           <div class="row clearfix">
								<?
								
									for($i=0;$i<count($mainhrtop);$i++){
										$mp=getartpic($mainhrtop[$i]['art_id']);

										
										//$ar=db_row("select * from articles where id='".$mainhrtop[$i]['id']."'");
										echo"<div class=\"col-lg-12 col-md-12 col-sm-6 md-margin\">
											<h4><a href=\"".$mainhrtop[$i]['link']."\">".$mainhrtop[$i]['title']."</a></h4>
											<a href=\"".$mainhrtop[$i]['link']."\"><img alt=\"\" src=\"".$oldpage.$mp."\" class=\"xs-margin img-responsive\"></a>
											<p>".$mainhrtop[$i]['slead']." <a class=\"more-btn\" href=\"".$mainhrtop[$i]['link']."\">Teljes cikk</a></p>
										</div>";
						
									}
								?>
							</div>
                        </div>
                        
                        

                    </div>


