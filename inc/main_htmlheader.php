<?php

?>
<!DOCTYPE html>
<html lang="hu">

<head>
	<base href="<?=$ServerName;?>"> 
    <meta charset="utf-8">
    <?
		include("inc/main_seoheader.php");
    ?>
    
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
    <!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
    <!--script src="js/less-1.3.3.min.js"></script-->
    <!--append ‘#!watch’ to the browser URL, then refresh the page. -->

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.3.css" rel="stylesheet">
    <link href="fonts/fa/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/jquery.mmenu.css" type="text/css" rel="stylesheet" />
    <link href="css/jquery.mmenu.all.css" type="text/css" rel="stylesheet" />
    <link href="css/jquery.mmenu.themes.css" type="text/css" rel="stylesheet" />
    <link href="css/jquery-ui.min.css" type="text/css" rel="stylesheet" />
    <link href="js/bxslider/jquery.bxslider.css" rel="stylesheet" />
    <link href="css/planetstar.css" type="text/css" rel="stylesheet" />


    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
  <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="img/favicon.ico">

	<script type="text/javascript">	var ServerName="<?=$ServerName;?>";	</script>
    <script type="text/javascript" src="<?=$ServerName;?>/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?=$ServerName;?>/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/responsive-tabs.js"></script>
    <!--<script type="text/javascript" src="<?=$ServerName;?>js/scripts.js"></script>-->
    <script type="text/javascript" src="js/jquery.mmenu.min.all.js"></script>
    <script type="text/javascript" src="js/jquery.mmenu.min.js"></script>
    <script src="/js/bxslider/jquery.bxslider.min.js"></script>
    
    <!--Fancybox element -->
    <!-- Add mousewheel plugin (this is optional) -->
	<script type="text/javascript" src="js/jquery.mousewheel-3.0.6.pack.js"></script>
    <link rel="stylesheet" href="js/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
	<script type="text/javascript" src="js/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>

	<!-- Optionally add helpers - button, thumbnail and/or media -->
	<link rel="stylesheet" href="js/fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
	<script type="text/javascript" src="/js/fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
	<script type="text/javascript" src="js/fancybox/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

	<link rel="stylesheet" href="js/fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
	<script type="text/javascript" src="js/fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
    
    <script type="text/javascript" src="js/hrportal.js"></script>


    <script type="text/javascript">
        (function ($) {
            fakewaffle.responsiveTabs(['xs', 'sm']);
        })(jQuery);

        $('.collapse').collapse()
        $(document).ready(function () {
          
        });
        
               
    </script>
 


<script type='text/javascript'>
jQuery(document).ready(function($) {
  $('#bookmark-this').click(function(e) {
    var bookmarkURL = window.location.href;
    var bookmarkTitle = document.title;

    if ('addToHomescreen' in window && window.addToHomescreen.isCompatible) {
      // Mobile browsers
      addToHomescreen({ autostart: false, startDelay: 0 }).show(true);
    } else if (window.sidebar && window.sidebar.addPanel) {
      // Firefox version < 23
      window.sidebar.addPanel(bookmarkTitle, bookmarkURL, '');
    } else if ((window.sidebar && /Firefox/i.test(navigator.userAgent)) || (window.opera && window.print)) {
      // Firefox version >= 23 and Opera Hotlist
      $(this).attr({
        href: bookmarkURL,
        title: bookmarkTitle,
        rel: 'sidebar'
      }).off(e);
      return true;
    } else if (window.external && ('AddFavorite' in window.external)) {
      // IE Favorite
      window.external.AddFavorite(bookmarkURL, bookmarkTitle);
    } else {
      // Other browsers (mainly WebKit - Chrome/Safari)
      alert('Press ' + (/Mac/i.test(navigator.userAgent) ? 'Cmd' : 'Ctrl') + '+D to bookmark this page.');
    }

    return false;
  });
});
</script>
	<!-- Place this tag in the <head> of your document -->
<script type="text/javascript">
window.___gcfg = {lang: 'hu'};
(function() 
{var po = document.createElement("script");
po.type = "text/javascript"; po.async = true;po.src = "https://apis.google.com/js/plusone.js";
var s = document.getElementsByTagName("script")[0];
s.parentNode.insertBefore(po, s);
})();</script>


<script type='text/javascript'>
    (function() {
    var useSSL = 'https:' == document.location.protocol;
    var src = (useSSL ? 'https:' : 'http:') +
    '//www.googletagservices.com/tag/js/gpt.js';
    document.write('<scr' + 'ipt src="' + src + '"></scr' + 'ipt>');
    })();
    </script>
    <script type='text/javascript'>
    var mapping = googletag.sizeMapping().
    addSize([1140, 200], [[970, 90],[970, 90]]).
    addSize([1010, 200], [728, 90]).
    addSize([534, 200], [468, 60]).
    addSize([0, 0], [320, 50]).
    build();
    slot1a = googletag.defineSlot('/5297488/HRP001', [[970, 250], [970, 90], [468, 60], [320, 50], [728, 90]], 'div-gpt-ad-1461936145403-0').defineSizeMapping(mapping).addService(googletag.pubads());
    slot1b = googletag.defineSlot('/5297488/HRP002', [[970, 250], [970, 90], [468, 60], [320, 50], [728, 90]], 'div-gpt-ad-1461936145403-1').defineSizeMapping(mapping).addService(googletag.pubads());
    var mapping2 = googletag.sizeMapping().
    addSize([728, 200], [[640, 360], [728, 90]]).
    addSize([564, 200], [[468, 120], [468, 60]]).
    addSize([0, 0], [320, 50]).
    build();
    slot2a = googletag.defineSlot('/5297488/HRP003', [[320, 50],[468, 250], [468, 60], [640, 360],[600, 300]], 'div-gpt-ad-1461936145403-2').defineSizeMapping(mapping2).addService(googletag.pubads());
    slot2b = googletag.defineSlot('/5297488/HRP004', [[320, 50],[468, 250], [468, 60], [640, 360],[600, 300]], 'div-gpt-ad-1461936145403-3').defineSizeMapping(mapping2).addService(googletag.pubads());
    slot2c = googletag.defineSlot('/5297488/HRP005', [[320, 50],[468, 250], [468, 60], [640, 360],[600, 300]], 'div-gpt-ad-1461936145403-4').defineSizeMapping(mapping2).addService(googletag.pubads());
    slot2d = googletag.defineSlot('/5297488/HRP006', [[320, 50],[468, 250], [468, 60], [640, 360],[600, 300]], 'div-gpt-ad-1461936145403-5').defineSizeMapping(mapping2).addService(googletag.pubads());
	var mapping3 = googletag.sizeMapping().
    addSize([1216, 400], [[300, 200],[300, 250],[250, 250],[250, 200], [300, 300],[250, 201],[250, 100]]).
    addSize([1010, 400], [[300, 200],[300, 250],[250, 250],[250, 200], [300, 300],[250, 201],[250, 100]]).
    addSize([768, 400], [[300, 200],[300, 250],[250, 250],[250, 200], [300, 300],[250, 201],[250, 100]]).
    addSize([696, 400], [[300, 200],[300, 250],[250, 250],[250, 200], [300, 300],[250, 201],[250, 100]]).
    addSize([596, 400], [[300, 200],[300, 250],[250, 250],[250, 200],[300, 300],[250, 201],[250, 100]]).
    addSize([0, 0], [320, 50]).
    build();
    slot3a = googletag.defineSlot('/5297488/HRP007', [[250, 200], [300, 300], [250, 400], [250, 100], [300, 100], [300, 600], [250, 250], [300, 250]], 'div-gpt-ad-1461936145403-6').defineSizeMapping(mapping3).addService(googletag.pubads());
    slot3b = googletag.defineSlot('/5297488/HRP008', [[250, 200], [300, 300], [250, 400], [250, 100], [300, 100], [300, 600], [250, 250], [300, 250]], 'div-gpt-ad-1461936145403-7').defineSizeMapping(mapping3).addService(googletag.pubads());
    slot3c = googletag.defineSlot('/5297488/HRP009', [[250, 200], [300, 300], [250, 400], [250, 100], [300, 100], [300, 600], [250, 250], [300, 250]], 'div-gpt-ad-1461936145403-8').defineSizeMapping(mapping3).addService(googletag.pubads());
    slot3d = googletag.defineSlot('/5297488/HRP0010', [[250, 200], [300, 300], [250, 400], [250, 100], [300, 100], [300, 600], [250, 250], [300, 250]], 'div-gpt-ad-1461936145403-9').defineSizeMapping(mapping3).addService(googletag.pubads());
    slot3e = googletag.defineSlot('/5297488/HRP0011', [[250, 200], [300, 300], [250, 400], [250, 100], [300, 100], [300, 600], [250, 250], [300, 250]], 'div-gpt-ad-1461936145403-10').defineSizeMapping(mapping3).addService(googletag.pubads());
    slot3f = googletag.defineSlot('/5297488/HRP0012', [[250, 200], [300, 300], [250, 400], [250, 100], [300, 100], [300, 600], [250, 250], [300, 250]], 'div-gpt-ad-1461936145403-11').defineSizeMapping(mapping3).addService(googletag.pubads());
    slot3g = googletag.defineSlot('/5297488/HRP0013', [[250, 200], [300, 300], [250, 400], [250, 100], [300, 100], [300, 600], [250, 250], [300, 250]], 'div-gpt-ad-1461936145403-12-1').defineSizeMapping(mapping3).addService(googletag.pubads());
    slot3h = googletag.defineSlot('/5297488/HRP0014', [[250, 200], [300, 300], [250, 400], [250, 100], [300, 100], [300, 600], [250, 250], [300, 250]], 'div-gpt-ad-1461936145403-13').defineSizeMapping(mapping3).addService(googletag.pubads());
    slot3i = googletag.defineSlot('/5297488/HRPortal_hu_kafeteria_jobb2', [[250, 200], [250, 100], [250, 250], [300, 250], [300, 100], [250, 300], [300, 300], [300, 600]], 'div-gpt-ad-1477913218720-0').defineSizeMapping(mapping3).addService(googletag.pubads());
    slot3j = googletag.defineSlot('/5297488/HRPortal_hu_kafeteria_jobb1', [[250, 200], [250, 100], [250, 250], [300, 250], [300, 100], [250, 300], [300, 300], [300, 600]], 'div-gpt-ad-1477913218720-1').defineSizeMapping(mapping3).addService(googletag.pubads());
    slot3k = googletag.defineSlot('/5297488/HRPortal_hu_berkalkulator_jobb0', [ [250, 100]], 'div-gpt-ad-1477913218720-200').defineSizeMapping(mapping3).addService(googletag.pubads());
    slot3l = googletag.defineSlot('/5297488/HRPortal_hu_employer_branding_jobb1', [[300, 600], [300, 100], [250, 300], [300, 300], [250, 100], [250, 250], [300, 250], [250, 200]], 'div-gpt-ad-1477913218720-2').defineSizeMapping(mapping3).addService(googletag.pubads());
    slot3m = googletag.defineSlot('/5297488/HRPortal_hu_employer_branding_jobb2', [[300, 600], [300, 100], [250, 300], [300, 300], [250, 100], [250, 250], [300, 250], [250, 200]], 'div-gpt-ad-1477913218720-3').defineSizeMapping(mapping3).addService(googletag.pubads());

    var mapping4 = googletag.sizeMapping().
    addSize([1216, 200], [[1140, 60]]).
    addSize([1010, 200], [[728, 90],[728,60]]).
    addSize([564, 200], [[468, 60]]).
    addSize([0, 0], [[300, 250],[320,50]]).
    build();
    slot4a = googletag.defineSlot('/5297488/HRP015', [[1140,60],[468, 60] ], 'div-gpt-ad-1461936145403-14').defineSizeMapping(mapping4).addService(googletag.pubads());
    slot4b = googletag.defineSlot('/5297488/HRPortal_hu_sponsor_kafeteria', [[320, 50],[468, 60], [728, 90],[1140, 60] ], 'div-gpt-ad-1461936145403-15').defineSizeMapping(mapping4).addService(googletag.pubads());
    slot4c = googletag.defineSlot('/5297488/HRPortal_hu_sponsor_hr_informatika', [[250, 60], [250, 50], [980, 60], [468, 60], [728, 90], [320, 50], [970, 90], [970, 250], [728, 60], [1140, 60], [320, 60]], 'div-gpt-ad-1461936145403-16').defineSizeMapping(mapping4).addService(googletag.pubads());
    slot4d = googletag.defineSlot('/5297488/HRPortal_hu_sponsor_employer_branding', [[728, 90], [250, 50], [320, 50], [980, 60], [320, 60], [468, 60], [728, 60], [970, 90], [970, 250], [1140, 60]], 'div-gpt-ad-1461936145403-17').defineSizeMapping(mapping4).addService(googletag.pubads());
    
  
    var mapping5 = googletag.sizeMapping().
    addSize([1216, 200],[320,100]).
    addSize([1010, 200], [320,100]).
    addSize([564, 200], [320,100]).
    addSize([0, 0], [320,100]).
    build();
    slot5a = googletag.defineSlot('/5297488/HRPortal_hu_beuszo_01', [320, 100], 'div-gpt-ad-1461936145403-18').defineSizeMapping(mapping5).addService(googletag.pubads());    
 
  
   googletag.pubads().collapseEmptyDivs();
    googletag.enableServices();
    </script>
    <script type='text/javascript'>
var resizeTimer;
function resizer() {
   googletag.pubads().refresh([slot1a,slot1b,slot2a,slot2b,slot2c,slot2d,slot3a,slot3b,slot3c,slot3d,slot3e,slot3f,slot3g,slot3h,slot3i,slot3j,slot3k,slot3l,slot3m,slot4a,slot4b,slot4c,slot4d,slot5a]);
}

var cachedWidth = window.innerWidth;
window.addEventListener("resize", function() {
   var newWidth = window.innerWidth;
   if (newWidth !== cachedWidth) {
      cachedWidth = newWidth;
      clearTimeout(resizeTimer);
      resizeTimer = setTimeout(resizer, 250);
   }
});
</script>

<script type="text/javascript">
    (function(p,u,s,h){
        p._pcq=p._pcq||[];
        p._pcq.push(['_currentTime',Date.now()]);
        s=u.createElement('script');
        s.type='text/javascript';
        s.async=true;
        s.src='https://cdn.pushcrew.com/js/12f8d227a1814006638d1bc02d1342f1.js';
        h=u.getElementsByTagName('script')[0];
        h.parentNode.insertBefore(s,h);
    })(window,document);
</script>
</head>
<body >
    <div id="fb-root"></div>
<!--<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/hu_HU/sdk.js#xfbml=1&version=v2.5&appId=376913275663916";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>-->

<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '376913275663916',
      xfbml      : true,
      version    : 'v2.8'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
