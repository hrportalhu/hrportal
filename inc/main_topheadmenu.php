<?php

?>

<div class="navbar navbar-default navbar-fixed-top">
	<div class="container"> 
		<a href="" class="logo"><img alt="" src="img/hr-portal-logo2.jpg"></a> 
		<nav class="site-nav" >
			<ul class="nav">
				<li><a href="http://www.hrblog.hu" target="_blank"><span>HR</span> Blog</a></li>
				<li><a href="/allasok.html"><span>HR</span> Állások</a></li>
				<li><a href="/berkalkulator_2016.html">Bérkalkulátor</a></li>
				<li><a href="http://www.hrkatalogus.hu" target="_blank"><span>HR</span> Katalógus</a></li>
				<li><a href="http://www.hrclub.hu" target="_blank"><span>HR</span> Események</a></li>
			</ul>
		</nav>
	</div>   
</div>   
