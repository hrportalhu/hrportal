<?php
	switch(strftime("%m")){
		case 1:
		$napok = array("Fruzsina","Ábel","Genovéva, Benjámin","Titusz, Leona","Simon","Boldizsár","Attila, Ramóna","Gyöngyvér","Marcell","Melánia","Ágota","ErnŐ","Veronika","Bódog","Lóránt, Loránd","Gusztáv","Antal, Antónia","Piroska","Sára, Márió","Fábián, Sebestyén","Ágnes","Vince, Artúr","Zelma, Rajmund","Timót","Pál","Vanda, Paula","Angelika","Károly, Karola","Adél","Martina, Gerda","Marcella");
		break;

		case 2:
		$napok = array("Ignác","Karolina, Aida","Balázs","Ráhel, Csenge","Ágota, Ingrid","Dorottya, Dóra","Tódor, Rómeó","Aranka","Abigél, Alex","Elvira","Bertold, Marietta","Lívia, Lídia","Ella, Linda","Bálint, Valentin","Kolos, Georgina","Julianna, Lilla","Donát","Bernadett","Zsuzsanna","Aladár, Álmos","Eleonóra","Gerzson","Alfréd","Mátyás","Géza","Edina","Ákos, Bátor","Elemér");
		break;

		case 3:
		$napok = array("Albin","Lujza","Kornélia","Kázmér","Adorján, Adrián","Leonóra, Inez","Tamás","Zoltán","Franciska, Fanni","Ildikó","Szilárd","Gergely","Krisztián, Ajtony","Matild","Kristóf","Henrietta","Gertrúd, Patrik","Sándor, Ede","József, Bánk","Klaudia","Benedek","Beáta, Izolda","Emőke","Gábor, Karina","Irén, Irisz","Emánuel","Hajnalka","Gedeon, Johanna","Auguszta","Zalán","Árpád");
		break;

		case 4:
		$napok = array("Hugó","Áron","Buda, Richárd","Izidor","Vince","Vilmos, Bíborka","Herman","Dénes","Erhard","Zsolt","Leó, Szaniszló","Gyula","Ida","Tibor","Anasztázia, Tas","Csongor","Rudolf","Andrea, Ilma","Emma","Tivadar","Konrád","Csilla, Noémi","Béla","György","Márk","Ervin","Zita","Valéria","Péter","Katalin, Kitti");
		break;

		case 5:
		$napok = array("MUNKA ÜNN.,Fülöp, Jakab","Zsigmond","Tímea, Irma","Mónika, Flórián","Györgyi","Ivett, Frida","Gizella","Mihály","Gergely","Ármin, Pálma","Ferenc","Pongrác","Szervác, Imola","Bonifác","Zsófia, Szonja","Mózes, Botond","Paszkál","Erik, Alexandra","Ivó, Milán","Bernát, Felícia","Konstantin","Júlia, Rita","Dezső","Eszter, Eliza","Orbán","Fülöp, Evelin","Hella","Emil, Csanád","Magdolna","Janka, Zsanett","Angéla, Petronella");
		break;

		case 6:
		$napok = array("Tünde","Kármen, Anita","Klotild","Bulcsú","Fatime","Norbert, Cintia","Róbert","Medárd","Félix","Margit, Gréta","Barnabás","Villő","Antal, Anett","Vazul","Jolán, Vid","Jusztin","Laura, Alida","Arnold, Levente","Gyárfás","Rafael","Alajos, Leila","Paulina","Zoltán","Iván","Vilmos","János, Pál","László","Levente, Irén","Péter, Pál","Pál");
		break;

		case 7:
		$napok = array("Tihamér, Annamária","Ottó","Kornél, Soma","Ulrik","Emese, Sarolta","Csaba","Appolónia","Ellák","Lukrécia","Amália","Nóra, Lili","Izabella, Dalma","Jenő","Őrs, Stella","Henrik, Roland","Valter","Endre, Elek","Frigyes","Emília","Illés","Dániel, Daniella","Magdolna","Lenke","Kinga, Kincső","Kristóf, Jakab","Anna, Anikó","Olga, Liliána","Szabolcs","Márta, Flóra","Judit, Xénia","Oszkár");
		break;

		case 8:
		$napok = array("Boglárka","Lehel","Hermina","Domonkos, Dominika","Krisztina","Berta, Bettina","Ibolya","László","Emőd","Lörinc","Zsuzsanna, Tiborc","Klára","Ipoly","Marcell","Mária","Ábrahám","Jácint","Ilona","Huba","ALKOTMÁNY ÜNN., István","Sámuel, Hajna","Menyhért, Mirjam","Bence","Bertalan","Lajos, Patrícia","Izsó","Gáspár","Ágoston","Beatrix, Erna","Rózsa","Erika, Bella");
		break;

		case 9:
		$napok = array("Egyed, Egon","Rebeka, Dorina","Hilda","Rozália","Viktor, Lőrinc","Zakariás","Regina","Mária, Adrienn","Ádám","Nikolett, Hunor","Teodóra","Mária","Kornél","Szeréna, Roxána","Enikő, Melitta","Edit","Zsófia","Diána","Vilhelmina","Friderika","Máté, Mirella","Móric","Tekla","Gellért, Mercédesz","Eufrozina, Kende","Jusztina","Adalbert","Vencel","Mihály","Jeromos");
		break;

		case 10:
		$napok = array("Malvin","Petra","Helga","Ferenc","Aurél","Brúnó, Renáta","Amália","Koppány","Dénes","Gedeon","Brigitta","Miksa","Kálmán, Ede","Helén","Teréz","Gál","Hedvig","Lukács","Nándor","Vendel","Orsolya","Előd","Gyöngyi","Salamon","Blanka, Bianka","Dömötör","Szabina","Simon, Szimonetta","Nárcisz","Alfonz","Farkas");
		break;

		case 11:
		$napok = array("Marianna","Achilles","Győző","Károly","Imre","Lénárd","Rezső","Zsombor","Tivadar","Réka","Márton","Jónás, Renátó","Szilvia","Aliz","Albert, Lipót","Ödön","Hortenzia, Gergő","Jenő","Erzsébet","Jolán","Olivér","Cecília","Kelemen, Klementina","Emma","Katalin","Virág","Virgil","Stefánia","Taksony","András, Andor");
		break;

		case 12:
		$napok = array("Elza","Melinda, Vivien","Ferenc, Olívia","Borbála, Barbara","Vilma","Miklós","Ambrus","Mária","Natália","Judit","Árpád","Gabriella","Luca, Otília","Szilárda","Valér","Etelka, Aletta","Lázár, Olimpia","Auguszta","Viola","Teofil","Tamás","Zéno","Viktória","Ádám, Éva","Eugénia","István","János","Kamilla","Tamás, Tamara","Dávid","Szilveszter");
	}

echo date("Y. ");
    
switch (date("m"))
 {
  case "01": echo "január"; break;
  case "02": echo "február"; break;
  case "03": echo "március"; break;
  case "04": echo "április"; break;
  case "05": echo "május"; break;
  case "06": echo "június"; break;
  case "07": echo "július"; break;
  case "08": echo "augusztus"; break;
  case "09": echo "szeptember"; break;
  case "10": echo "október"; break;
  case "11": echo "november"; break;
  case "12": echo "december"; break;
 }
echo strftime(" %d.").", ".$napok[(strftime("%d")-1)]."";
