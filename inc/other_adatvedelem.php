
                    
<div class="col-md-8 column">
<ol class="breadcrumb">
	<li><a href="/">Főoldal</a></li>
	<li><a href="/adatvedelem.html">Adatvédelem</a></li>
</ol>
<article>
	<div class="row clearfix md-margin">
		<div class="col-md-12 column">
			<h1 class="md-margin">Adatvédelem</h1>
			
				<div class="media">
			 <p>Köszöntjük a Brand & Standard Capital Tanácsadó és Szolgáltató Kft. által fenntartott HR Portál weboldal felhasználói között. Az alábbiakban cégünk adatvédelmi politikájának irányelveiről szeretnénk tájékoztatni. Mielőbb továbblép a regisztrációra, kérjük, szenteljen néhány percet áttanulmányozásukra. 
					<br /><br /><br />Weboldalunk látogatása során Ön akár tudtával és beleegyezésével, akár tudtán kívül, a számítógépek egymás közötti kommunikációja során olyan adatokat hoz tudomásunkra, melyek az adatvédelmi törvény (a személyes adatok védelméről és a közérdekű adatok nyilvánosságáról szóló 1992.évi LXIII. törvény) szerint személyes adatoknak minősülhetnek. Az adatközlés önkéntes: kizárólag Öntől függ, hogy kérdéseinkre válaszol-e. 
					<br /><br />A HR Portál weboldalán bizonyos szolgáltatások csak regisztrációt követően vehetők igénybe. A regisztráció során Önt arra kérjük, hogy részünkre személyes adatokat adjon meg. A sikeres regisztráció érdekében kérjük majd neve, munkáltatója, beosztása, telefonszáma és e-mail elérhetősége megadását. A beosztásra és telefonszámra vonatkozó rovatok kitöltése a regisztrációhoz nem elengedhetetlen, de ügyfélkapcsolataink építése, szolgáltatásaink továbbfejlesztése szempontjából köszönettel vennénk, ha ezekre is válaszolna. 
					<br /><br />Cégünk hírlevél szolgáltatást is nyújt azzal a céllal, hogy látogatóink e-mail formájában is értesülhessenek az aktuális hírekről, tanulmányokról, üzleti lehetőségekről. Cégünk tartózkodik a kéretlen, ún. spam e-mail-ek küldésétől, ezért kérjük Önt, amennyiben hírlevelünket szívesen fogadná, regisztrációja során jelezze ezt a hírlevél szolgáltatás igénybevételének megjelölésével.
					<br /><br />Biztosítani kívánjuk arról, hogy a regisztráció során tudomásunkra jutott személyes adatait (az Ön kifejezett hozzájáruló nyilatkozata hiányában) semmilyen körülmények között nem tesszük más számára hozzáférhetővé, azokat szolgáltatásunk igénybevételi lehetőségeinek támogatása céljából kezeljük és dolgozzuk fel. 
					<br /><br />Az Ön által megadott adatokat cégünk csak addig kezeli, amíg Ön erre igényt tart. Az adatok törlésére az Ön kérésére bármikor, azonnal sor kerülhet.
					<br /><br />Webszerverünk felismeri a site-ra érkező látogatók domain nevét. Szerverünkön tároljuk az oldalaink látogatottságát jelző információkat (pl. a látogatás időpontja és időtartama) és a látogatók által önként megadott adatokat, pl. a regisztráció során megadott információkat. Az így összegyűjtött adatok - miután az adatszolgáltatóval elektronikus levélcíme vagy az Ön által szolgáltatott más adat közlése közvetlenül összefüggésbe hozható - személyes adatnak minősülnek. E körben Önt az Ön által közölt adatokkal egyező jogvédelem illeti meg. 
					<br />Az oldal bizonyos részein cookie-kat (sütiket) használunk. 
					<br /><br />Cégünk az adatvédelmi jog hatályos előírásai szerint törekszik arra, hogy szolgáltatása technikai szempontból is biztonságosnak minősüljön. Fizikai létesítményeinkben olyan biztonsági intézkedések vannak érvényben, melyek megfelelően védik a felhasználók adatait a megrongálódástól, a rossz szándékú felhasználástól vagy a megváltoztatástól. Mindent megteszünk annak érdekében, hogy az Ön által megadott személyes adatokhoz illetéktelen ne férhessen hozzá, ne tudja megváltoztatni, törölni, az adatokban sérülést okozni. 
					<br /><br />Ugyanez internetes oldalainkat elérhetővé tevő szolgáltatókra is vonatkozik, akik a tudomásukra jutott információkat üzleti titokként kötelesek kezelni. 
					<br /><br />Cégünk munkatársai bármikor rendelkezésére állnak, hogy az általunk kezelt személyes adataival kapcsolatosan Önnek tájékoztatást adjanak. Amennyiben úgy érzi, hogy cégünk személyes adatainak kezelésével kapcsolatosan nem elvárásainak megfelelően jár el, kérjük haladéktalanul jelezze ezt nekünk, hogy az esetet kivizsgálhassuk, s gyakorlatunkat az Ön igényeihez igazíthassuk. Önnek ahhoz is joga van, hogy jogsérelem bekövetkezte vagy annak közvetlen fennállásának veszélye esetén panaszával az adatvédelmi biztoshoz forduljon. E körben Önt a közérdekű bejelentővel azonos jogvédelem illeti meg. 
					<br /><br />Amennyiben a fentiek az Ön számára elfogadhatóak, úgy kérjük, kattintson a regisztráció ikonjára. 
					<br /><br />Ha az adatvédelmi politikánkban foglaltakkal nem ért egyet, kérjük, tudassa velünk ennek okát, hogy szolgáltatásainkat az Ön elvárásaihoz tudjuk igazítani. Cégünk az általa nyújtott szolgáltatást ügyfelei megelégedésére, a hatályos jogszabályok legmesszebbmenőkig való betartásával kívánja nyújtani, ezért számunkra minden vélemény fontos. Amennyiben a fentiek alapján úgy döntött, hogy nem kívánja regisztráltatni magát, s szolgáltatásainkat nem óhajtja igénybe venni, köszönjük megtisztelő érdeklődését és figyelmét.
				</p>
 
			</div> 	
	
		</div>
	</div>
</article>
</div>
