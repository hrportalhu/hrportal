 <!-- ***** CENTER COLUMN ***** -->
 <div class="col-md-8 column">
   <ol class="breadcrumb">
		<li><a href="/">Főoldal</a>
		</li>
		<li><a href="/allaskeresesi_tamogatas_kalkulator.html">Álláskeresési járadék kalkulátor 2017</a>
		</li>
	</ol>
	<article>
		<div class="row clearfix md-margin">
			<div class="col-md-12 column">
			   <h1  class="md-margin" >Álláskeresési járadék kalkulátor 2017</h1>                                       
			   <form class="form-horizontal md-margin" onsubmit="return false;">
				  <div id="errordiv" style="color:#EA0000;text-align:center;font-size:13px;font-weight:bold;"></div>
				  <div class="form-group">
					<label for="startdate" class="col-sm-7 control-label">Munkaviszony kezedte :</label>
					<div class="col-sm-2">
					  <input type="text" class="form-control" id="startdate" name="startdate" value="<?=date("Y-m-d",time());?>"/>
					</div>
				  </div>
                                           
				  <div class="form-group">
					<label for="enddate" class="col-sm-7 control-label">Munkaviszony vége:</label>
					<div class="col-sm-2">
					  <input type="text" class="form-control" id="enddate" name="enddate" value="<?=date("Y-m-d",time());?>"/>
					</div>
				  </div>
                                           
				  <div class="form-group">
					<label for="megszunes" class="col-sm-7 control-label">Munkaviszony megsz&#x0171;nésének módja:</label>
					<div class="col-sm-4">
					  <select name='megszunes' id="megszunes" class="form-control">
						<option value='0' selected> Felmondás-Megsz&#x0171;nés</option>
						<option value='1'> Munkavállaló rendes felmondása</option>
						<option value='2'> Munkaadó rendkívüli felmondása </option>
					</select>
					</div>
				  </div>
                                           
				  <div class="form-group">
					<label for="havi_br" class="col-sm-7 control-label">Havi járulékalap (Bruttó - Ft):</label>
					<div class="col-sm-2">
					  <input type="text" class="form-control" id="havi_br" name="havi_br" placeholder="111000"/>
					</div>
				  </div>
                                           

				  <div class="form-group">
					<div class="col-sm-offset-7 col-sm-10">
					  <button class="btn btn btn-main" onclick="javascript:tamogataskulator();">Számol</button>
					</div>
				  </div>
				</form>
				
				<div id="tamogataseredmeny"></div>
									<div class="fb-like" data-href="http://www.hrportal.hu<?=$URI;?>" data-layout="standard" data-action="recommend" data-show-faces="true" data-share="true"></div>
					<div style="clear:both;height:1px;"></div> 
					<script src="//platform.linkedin.com/in.js" type="text/javascript">
					lang: en_US
					</script>
					<script type="IN/Share" data-url="http://www.hrportal.hu<?=$URI;?>" data-counter="right"></script>

					<div id="plusone-div" > <g:plusone size="normal"></g:plusone></div>
					<script type="text/javascript">
					gapi.plusone.go("plusone-div");
					</script>
			</div>
		</div>
	</article>
	
</div>
<!-- ***** CENTER COLUMN ***** -->
	
<script>
	
	$('.nav-tabs a[href="#panel-4"]').tab('show');
	
</script>

