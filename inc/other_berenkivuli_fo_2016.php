 <!-- ***** CENTER COLUMN ***** -->
 <div class="col-md-8 column">
   <ol class="breadcrumb">
		<li><a href="/">Főoldal</a>
		</li>
		<li><a href="/berenkivuli_fo_2016.html">Béren kívüli juttatások 2016</a>
		</li>
	</ol>
	<article>
		<div class="row clearfix md-margin">
			<div class="col-md-12 column">
			   <h1  class="md-margin" >Béren kívüli juttatások 2016</h1> 
				<p>Állítsa össze maga vagy dolgozói cafeteria csomagját és számolja ki mennyit takaríthat meg a hagyományos bérfizetéshez képest. </p> 
				<p>Kérjük figyelmesen nézze meg, hogy az adott érték hónapra, évre vagy alkalomra vonatkozik-e, mert elemenként eltérő mértékegységeket használ a kalkulátor. Ha esetleg valamit helytelenül rögzített, lehetősége van könnyen eltávolítani és felvinni újból a helyes értéket.</p>   
                                      
			   <form class="form-horizontal md-margin" onsubmit="return false;">
				   
				  
				  <div class="form-group">
					<label for="edt_elt" class="col-sm-6 control-label">Juttatás formája *</label>
					<div class="col-sm-6">
					  <select name="bertype" class="form-control" id="bertype" onchange="javascript:getbertypepref();" >
						<option value='0' selected>Kérem válasszon</option>
						<option value='mhetkezes'>Munkahelyi étkeztetés</option>
						<option value='erzsbetutalvany'>Erzsébet-utalvány</option>
						<option value='onyp'>Önkéntes nyugdíjpénztári hozzájárulás</option>
						<option value='epp'>Egészségpénztári-, önsegélyező pénztári hozzájárulás</option>
						<option value='szeve'>Széchenyi Pihenőkártya vendéglátás alszámla</option>
						<option value='szeszall'>Széchenyi Pihenőkártya szállás alszámla</option>
						<option value='szeszab'>Széchenyi Pihenőkártya szabadidő alszámla</option>
						<option value='lakas'>Lakáshitel-cafeteria</option>
						<option value='biztositas'>Kockázati biztosítás</option>
						<option value='ajandek'>Ajándékutalvány</option>
					</select>
					</div>
				  </div>
				<div class="form-group">
					<label for="love" class="col-sm-6 control-label">Juttatás mértéke (Ft)</label>
					<div class="col-sm-6">
					  <input type="text" class="form-control" id="love" name="love" placeholder="10000" >
					  <span style="width:50px;float:left;" id="penznem">&nbsp;</span>
					</div>
				  </div>
				        
			  
				  <div class="form-group">
					<div class="col-sm-offset-7 col-sm-10">
					  <button class="btn btn btn-main" onclick="javascript:berenkivuli2016();">Hozzáad</button>
					</div>
				  </div>
				</form>
				
				<div id="berenkivuli2016_cont"></div>
									<div class="fb-like" data-href="http://www.hrportal.hu<?=$URI;?>" data-layout="standard" data-action="recommend" data-show-faces="true" data-share="true"></div>
					<div style="clear:both;height:1px;"></div> 
					<script src="//platform.linkedin.com/in.js" type="text/javascript">
					lang: en_US
					</script>
					<script type="IN/Share" data-url="http://www.hrportal.hu<?=$URI;?>" data-counter="right"></script>

					<div id="plusone-div" > <g:plusone size="normal"></g:plusone></div>
					<script type="text/javascript">
					gapi.plusone.go("plusone-div");
					</script>
			</div>
		</div>
	</article>

</div>
<!-- ***** CENTER COLUMN ***** -->
	
<script>
	
	$('.nav-tabs a[href="#panel-4"]').tab('show');
	
</script>

