 <!-- ***** CENTER COLUMN ***** -->
<?
$val=" placeholder=\"10000\" ";
if($uritags[1]=="sport"){
	$val=" value=\"10000\" ";
}
if($uritags[1]=="mhetkezes"){
	$val=" value=\"8000\" ";
}
if($uritags[1]=="penz"){
	$val=" value=\"100000\" ";
}
if($uritags[1]=="onyp"){
	$val=" value=\"10000\" ";
}
if($uritags[1]=="epp"){
	$val=" value=\"10000\" ";
}
if($uritags[1]=="szeszall"){
	$val=" value=\"100000\" ";
}
if($uritags[1]=="szeve"){
	$val=" value=\"50000\" ";
}
if($uritags[1]=="szeszab"){
	$val=" value=\"50000\" ";
}
if($uritags[1]=="lakas"){
	$val=" value=\"10000\" ";
}
if($uritags[1]=="biztositas"){
	$val=" value=\"10000\" ";
}
if($uritags[1]=="ajandek"){
	$val=" value=\"10000\" ";
}
if($uritags[1]=="kultura"){
	$val=" value=\"10000\" ";
}
 ?>
 <div class="col-md-8 column">
   <ol class="breadcrumb">
		<li><a href="/">Főoldal</a>
		</li>
		<li><a href="/berenkivuli_fo_2017.html">Béren kívüli juttatások 2017</a>
		</li>
	</ol>
	<article>
		<div class="row clearfix md-margin">
			<div class="col-md-12 column">
			   <h1  class="md-margin" >Béren kívüli juttatások 2017</h1> 
				<p>Állítsa össze maga vagy dolgozói cafeteria csomagját és számolja ki mennyit takaríthat meg a hagyományos bérfizetéshez képest. </p> 
				<p>Kérjük figyelmesen nézze meg, hogy az adott érték hónapra, évre vagy alkalomra vonatkozik-e, mert elemenként eltérő mértékegységeket használ a kalkulátor. Ha esetleg valamit helytelenül rögzített, lehetősége van könnyen eltávolítani és felvinni újból a helyes értéket.</p>   
                                      
			   <form class="form-horizontal md-margin" onsubmit="return false;">
				   
				  
				  <div class="form-group">
					<label for="edt_elt" class="col-sm-6 control-label">Juttatás formája *</label>
					<div class="col-sm-6">
					  <select name="bertype" class="form-control" id="bertype" onchange="javascript:getbertypepref();" >
						<option value='0' selected>Kérem válasszon</option>
						<option value='mhetkezes' <? if($uritags[1]=="mhetkezes"){echo"selected";} ?>>Munkahelyi étkeztetés</option>
						<!--<option value='erzsbetutalvany'>Erzsébet-utalvány</option>-->
						<option value='penz' <? if($uritags[1]=="penz"){echo"selected";} ?>>Készpénz cafeteria</option>
						<option value='onyp' <? if($uritags[1]=="onyp"){echo"selected";} ?>>Önkéntes nyugdíjpénztári hozzájárulás</option>
						<option value='epp' <? if($uritags[1]=="epp"){echo"selected";} ?>>Egészségpénztári-, önsegélyező pénztári hozzájárulás</option>
						<option value='szeve' <? if($uritags[1]=="szeve"){echo"selected";} ?>>Széchenyi Pihenőkártya vendéglátás alszámla</option>
						<option value='szeszall' <? if($uritags[1]=="szeszall"){echo"selected";} ?>>Széchenyi Pihenőkártya szállás alszámla</option>
						<option value='szeszab' <? if($uritags[1]=="szeszab"){echo"selected";} ?>>Széchenyi Pihenőkártya szabadidő alszámla</option>
						<option value='lakas' <? if($uritags[1]=="lakas"){echo"selected";} ?>>Lakáshitel-cafeteria</option>
						<option value='biztositas' <? if($uritags[1]=="biztositas"){echo"selected";} ?>>Kockázati biztosítás</option>
						<option value='ajandek' <? if($uritags[1]=="ajandek"){echo"selected";} ?>>Ajándékutalvány</option>
						<option value='sport' <? if($uritags[1]=="sport"){echo"selected";} ?>>Sport belépő cafeteria</option>
						<option value='kultura' <? if($uritags[1]=="kultura"){echo"selected";} ?>>Kúltúra utalvány</option>
					</select>
					</div>
				  </div>
				<div class="form-group">
					<label for="love" class="col-sm-6 control-label">Juttatás mértéke (Ft)</label>
					<div class="col-sm-6">
					  <input type="text" class="form-control" id="love" name="love" <?=$val;?> >
					  <span style="width:50px;float:left;" id="penznem">&nbsp;</span>
					</div>
				  </div>
				        
			  
				  <div class="form-group">
					<div class="col-sm-offset-7 col-sm-10">
					  <button class="btn btn btn-main" onclick="javascript:berenkivuli2017();">Hozzáad</button>
					</div>
				  </div>
				</form>
				<?
				if(isset($uritags[1])){
					echo"<h3>Példakalkuláció</h3>";	
				}
				?>
				<div id="berenkivuli2017_cont"></div>
				<div style="clear:both;height:10px;"></div>
				<h3>Kafetéria elemek gyors elérése</h3>
				<div style="clear:both;height:10px;"></div>
				<div>
					<a href="/berenkivuli_fo_2017.html/sport">Sport belépő cafeteria</a>&nbsp;|&nbsp;
					<a href="/berenkivuli_fo_2017.html/mhetkezes">Munkahelyi étkeztetés</a>&nbsp;|&nbsp;
					<a href="/berenkivuli_fo_2017.html/penz">Készpénz cafeteria</a>&nbsp;|&nbsp;
					<a href="/berenkivuli_fo_2017.html/onyp">Önkéntes nyugdíjpénztári hozzájárulás</a>&nbsp;|&nbsp;
					<a href="/berenkivuli_fo_2017.html/epp">Egészségpénztári-, önsegélyező pénztári hozzájárulás</a>&nbsp;|&nbsp;
					<a href="/berenkivuli_fo_2017.html/szeve">Széchenyi Pihenőkártya vendéglátás alszámla</a>&nbsp;|&nbsp;
					<a href="/berenkivuli_fo_2017.html/szeszall">Széchenyi Pihenőkártya szállás alszámla</a>&nbsp;|&nbsp;
					<a href="/berenkivuli_fo_2017.html/szeszab">Széchenyi Pihenőkártya szabadidő alszámla</a>&nbsp;|&nbsp;
					<a href="/berenkivuli_fo_2017.html/lakas">Lakáshitel-cafeteria</a>&nbsp;|&nbsp;
					<a href="/berenkivuli_fo_2017.html/biztositas">Kockázati biztosítás</a>&nbsp;|&nbsp;
					<a href="/berenkivuli_fo_2017.html/ajandek">Ajándékutalvány</a>&nbsp;|&nbsp;
					<a href="/berenkivuli_fo_2017.html/kultura">Kúltúra utalvány</a>&nbsp;|&nbsp;
				</div>
				<div style="clear:both;height:10px;"></div>
				<div class="fb-like" data-href="http://www.hrportal.hu<?=$URI;?>" data-layout="standard" data-action="recommend" data-show-faces="true" data-share="true"></div>
					<div style="clear:both;height:1px;"></div> 
					<script src="//platform.linkedin.com/in.js" type="text/javascript">
					lang: en_US
					</script>
					<script type="IN/Share" data-url="http://www.hrportal.hu<?=$URI;?>" data-counter="right"></script>

					<div id="plusone-div" > <g:plusone size="normal"></g:plusone></div>
					<script type="text/javascript">
					gapi.plusone.go("plusone-div");
					</script>
			</div>
		</div>
	</article>

</div>
<!-- ***** CENTER COLUMN ***** -->
	
<script>
	
	$('.nav-tabs a[href="#panel-4"]').tab('show');
	<?
	if(isset($uritags[1])){
		?>
		berenkivuli2017();
		<?
	}
?>
</script>

