<?
include("inc/main_category_sponsor.php");
?>
<SCRIPT LANGUAGE="JavaScript">


function ValidateForm(form){

   if(IsEmpty(form.edt_brutto)){ 
      alert('Kérjük, adja meg a bruttó havi munkabért!') 
      form.edt_brutto.focus(); 
      return false; 
   } 
 
 
   if (!IsNumeric(form.edt_brutto.value)) { 
      alert('Kérjük, csak számjegyeket adjon meg!') 
      form.edt_brutto.focus(); 
      return false; 
      } 
 
   if (form.edt_elt.value < form.edt_gyerekek.value ) { 
      alert('Az eltartottak száma nem lehet alacsonyabb a kedvezményezett eltartott gyermekek számánál!') 
      form.edt_brutto.focus(); 
      return false; 
      }  
 
return true;
 
} 
</SCRIPT>
 <div class="col-md-8 column">
   <ol class="breadcrumb">
		<li><a href="/">Főoldal</a>
		</li>
		<li><a href="/berkalkulator_2014.html">Bérkalkulátor 2014</a>
		</li>
	</ol>
	<article>
		<div class="row clearfix md-margin fullnewslistpannon1">
			<div class="col-md-12 column">
			   <h1  class="md-margin" >Bérkalkulátor 2014</h1>
			   			   <?
			   if(isset($_GET['edt_brutto'])){
					$patterns =array(' ','.',',');$replacements=array('','','');
					$br=str_replace($patterns, $replacements, $_GET['edt_brutto']);	
					$elt=$_GET['edt_elt']; 
					$gy=$_GET['edt_gyerekek'];
					$egyedulnevel=$_GET['edt_egyedulnevel'];
					$frisshazas=$_GET['edt_frisshazas'];
					$kedvezmeny=$_GET['edt_kedvezmeny']; 
					$munkaido=$_GET['edt_munkaido'];
					$mybr=" value=\"".$br."\" ";
					if($_GET['edt_brutto']!=""){
						include("inc/ajax_berkalkulator_2014.php");
					}	
				}
				else{
					$br="";
					$elt=0; 
					$gy=0;
					$egyedulnevel=0;
					$frisshazas=0;
					$kedvezmeny=0; 
					$munkaido=0;
					$mybr=" palceholder=\"200000\" ";
				}		
			   ?>                                        
			   <form class="form-horizontal md-margin" action="/berkalkulator_2014.html" method="get" onsubmit="javascript:return ValidateForm(this)" >
				   <div style="clear:both;height:1px;"></div><div style="text-align:center;font-weight:bold;font-size:14px;margin-bottom:10px;">A bérkalkulátor motorját a <a href="http://www.nexon.hu/ber-es-munkaugyi-modul" target="_blank"><img src="images/banner/nexon_small.jpg" alt="nexon" /></a> biztosítja.</div><div style="clear:both;height:1px;"></div>
				  <div class="form-group">
					<label for="edt_brutto" class="col-sm-7 control-label">Bruttó havi munkabér (Ft)</label>
					<div class="col-sm-2">
					  <input type="text" class="form-control" id="edt_brutto" name="edt_brutto" placeholder="200000">
					</div>
				  </div>
				  <div class="form-group">
					<label for="edt_elt" class="col-sm-7 control-label">Eltartottak (gyermekek) száma *</label>
					<div class="col-sm-2">
					  <select class="form-control"  name="edt_elt" id="edt_elt">
						  <option value="0" selected>nincsenek</option>
						  <option value="1">1</option>
						  <option value="2">2</option>
						  <option value="3">3</option>
						  <option value="4">4</option>
						  <option value="5">5</option>
						</select>
					</div>
				  </div>

				  <div class="form-group">
					<label for="edt_gyerekek" class="col-sm-7 control-label">Ebből kedvezményezett eltartottak száma **</label>
					<div class="col-sm-2">
					  <select class="form-control" name="edt_gyerekek" id="edt_gyerekek">
						  <option value="0" selected>nincsenek</option>
						  <option value="1">1</option>
						  <option value="2">2</option>
						  <option value="3">3</option>
						  <option value="4">4</option>
						  <option value="5">5</option>
						</select>
					</div>
				  </div>

				  <div class="form-group">
					<label for="text1" class="col-sm-7 control-label">Gyermekét/gyermekeit egyedül neveli</label>
					<div class="col-sm-2">
					  <select class="form-control" name="edt_egyedulnevel" id="edt_egyedulnevel">
						  <option value="1">igen</option>
						  <option value="0" selected>nem</option>
					   </select>
					</div>
				  </div>

				             
			   <fieldset>
				   
				   <span class="secondary">Alábbiak nem befolyásolják a nettó bért:</span>
				   <div class="form-group">
					<label for="text1" class="col-sm-7 control-label">Munkavállaló ****</span></label>
					<div class="col-sm-5">
					  <select class="form-control" name="edt_kedvezmeny" id="edt_kedvezmeny">
							<option value='0' selected>kedvezményre nem jogosult.</option>
							<option value='1' >25 év alatti pályakezdő.</option>
							<option value='2' >25 év alatti nem pályakezdő.</option>
							<option value='3' >25-54 éves szakképzetlen.</option>
							<option value='4' >tartósan álláskereső.</option>	
							<option value='5' >55 év feletti.</option>
							<option value='6' >gyed, gyes, gyetről tér vissza.</option>		
							<option value='7' >új mv. szab. vállalk. zónában</option>			
							<option value='8' >kutatóként van foglalkoztatva</option>				
							<option value='9' >bérkompenzációra jogosult</option>			
						</select>
					</div>
				  </div>
				  
				  <div class="form-group">
					<label for="text1" class="col-sm-7 control-label">Teljes / részmunkaidős foglalkoztatás</span></label>
					<div class="col-sm-5">
					  <select class="form-control" name="edt_munkaido" id="edt_munkaido">
							<option value='40' selected>teljes munkaidős</option>
							<option value='8' >heti 8 órás</option>
							<option value='10' >heti 10 órás</option>
							<option value='16' >heti 16 órás</option>
							<option value='20' >heti 20 órás</option>	
							<option value='24' >heti 24 órás</option>
							<option value='32' >heti 32 órás</option>				
						</select>
					</div>
				  </div>
				 </fieldset>
				  <div class="form-group">
					<div class="col-sm-offset-7 col-sm-10">
					  <button type="submit" class="btn btn btn-main" >Számol</button>
					</div>
				  </div>
				</form>
				
				<div id="berkalkulator_2014_cont"></div>
									<div class="fb-like" data-href="http://www.hrportal.hu<?=$URI;?>" data-layout="standard" data-action="recommend" data-show-faces="true" data-share="true"></div>
					<div style="clear:both;height:1px;"></div> 
					<script src="//platform.linkedin.com/in.js" type="text/javascript">
					lang: en_US
					</script>
					<script type="IN/Share" data-url="http://www.hrportal.hu<?=$URI;?>" data-counter="right"></script>

					<div id="plusone-div" > <g:plusone size="normal"></g:plusone></div>
					<script type="text/javascript">
					gapi.plusone.go("plusone-div");
					</script>
			</div>
		</div>
	</article>
	
</div>
<!-- ***** CENTER COLUMN ***** -->
	
<script>
	
	$('.nav-tabs a[href="#panel-4"]').tab('show');
	
</script>

