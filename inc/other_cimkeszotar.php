<?
$szocikks=db_all("SELECT
                         a.id AS a_id,
                         a.teaser AS a_teaser,
                         a.date_added AS a_date_added,
							a.keyword1 AS a_keyword1,
                         c.name AS c_name,
                         l.link as link
                        FROM
                         (features a LEFT JOIN f_categories c ON a.category_id=c.id)
                          LEFT JOIN links l ON a.id=l.art_id
                        WHERE
                         a.is_approved='yes'
                       
						AND 
						 (c.name='Acronymtár')
						 AND
                         l.def='1'
                         group by l.art_id
                        ORDER BY a.teaser asc,a.date_added DESC ");
?>

<div class="col-md-8 column">


<ol class="breadcrumb">
	<li><a href="/">Főoldal</a>
	</li>
	<li><a href="/akronima.html"> Cimkeszótár</a>
	</li>
   
</ol>



<article>
   <div class="row clearfix md-margin">
		<div class="col-md-12 column">
			<h1  class="md-margin" >Cimkeszótár</h1>
			<div>

				  <!-- Nav tabs -->
				  <ul class="nav nav-tabs dictionary sm-margin" role="tablist">
					<li class="active"><a href="#a" role="tab" data-toggle="tab" onclick="gettag('a');">A</a></li>
					<li><a href="#b" role="tab" data-toggle="tab"  onclick="gettag('b');">B</a></li>
					<li><a href="#c" role="tab" data-toggle="tab"  onclick="gettag('c');">C</a></li>
					<li><a href="#d" role="tab" data-toggle="tab"  onclick="gettag('d');">D</a></li>
					<li><a href="#e" role="tab" data-toggle="tab"  onclick="gettag('e');">E</a></li>
					<li><a href="#f" role="tab" data-toggle="tab"  onclick="gettag('f');">F</a></li>
					<li><a href="#g" role="tab" data-toggle="tab"  onclick="gettag('g');">G</a></li>
					<li><a href="#h" role="tab" data-toggle="tab"  onclick="gettag('h');">H</a></li>
					<li><a href="#i" role="tab" data-toggle="tab"  onclick="gettag('i');">I</a></li>
					<li><a href="#j" role="tab" data-toggle="tab"  onclick="gettag('j');">J</a></li>
					<li><a href="#k" role="tab" data-toggle="tab"  onclick="gettag('k');">K</a></li>
					<li><a href="#l" role="tab" data-toggle="tab"  onclick="gettag('l');">L</a></li>
					<li><a href="#m" role="tab" data-toggle="tab"  onclick="gettag('m');">M</a></li>
					<li><a href="#n" role="tab" data-toggle="tab"  onclick="gettag('n');">N</a></li>
					<li><a href="#o" role="tab" data-toggle="tab"  onclick="gettag('o');">O</a></li>
					<li><a href="#p" role="tab" data-toggle="tab"  onclick="gettag('p');">P</a></li>
					<li><a href="#r" role="tab" data-toggle="tab"  onclick="gettag('r');">R</a></li>
					<li><a href="#s" role="tab" data-toggle="tab"  onclick="gettag('s');">S</a></li>
					<li><a href="#t" role="tab" data-toggle="tab"  onclick="gettag('t');">T</a></li>
					<li><a href="#u" role="tab" data-toggle="tab"  onclick="gettag('u');">U</a></li>
					<li><a href="#v" role="tab" data-toggle="tab"  onclick="gettag('v');">V</a></li>
					<li><a href="#z" role="tab" data-toggle="tab"  onclick="gettag('z');">Z</a></li>
					<li><a href="#q" role="tab" data-toggle="tab"  onclick="gettag('q');">Q</a></li>
					<li><a href="#w" role="tab" data-toggle="tab"  onclick="gettag('w');">W</a></li>
					<li><a href="#y" role="tab" data-toggle="tab"  onclick="gettag('y');">Y</a></li>
					<li><a href="#0" role="tab" data-toggle="tab"  onclick="gettag('0');">0-9</a></li>
				  </ul>
				
				  <!-- Tab panes -->
				  <div class="tab-content md-margin">
					<div role="tabpanel" class="tab-pane active" id="a"></div>
					<div role="tabpanel" class="tab-pane" id="b"></div>
					<div role="tabpanel" class="tab-pane" id="c"></div>
					<div role="tabpanel" class="tab-pane" id="d"></div>
					<div role="tabpanel" class="tab-pane" id="e"></div>
					<div role="tabpanel" class="tab-pane" id="f"></div>
					<div role="tabpanel" class="tab-pane" id="g"></div>
					<div role="tabpanel" class="tab-pane" id="h"></div>
					<div role="tabpanel" class="tab-pane" id="i"></div>
					<div role="tabpanel" class="tab-pane" id="j"></div>
					<div role="tabpanel" class="tab-pane" id="k"></div>
					<div role="tabpanel" class="tab-pane" id="l"></div>
					<div role="tabpanel" class="tab-pane" id="m"></div>
					<div role="tabpanel" class="tab-pane" id="n"></div>
					<div role="tabpanel" class="tab-pane" id="o"></div>
					<div role="tabpanel" class="tab-pane" id="p"></div>
					<div role="tabpanel" class="tab-pane" id="r"></div>
					<div role="tabpanel" class="tab-pane" id="s"></div>
					<div role="tabpanel" class="tab-pane" id="t"></div>
					<div role="tabpanel" class="tab-pane" id="u"></div>
					<div role="tabpanel" class="tab-pane" id="v"></div>
					<div role="tabpanel" class="tab-pane" id="z"></div>
					<div role="tabpanel" class="tab-pane" id="q"></div>
					<div role="tabpanel" class="tab-pane" id="w"></div>
					<div role="tabpanel" class="tab-pane" id="y"></div>
					<div role="tabpanel" class="tab-pane" id="0"></div>
															
				</div>                                        
		
		
		
		</div>
	</div>
</article>
                </div>
						



<script>
	
	$('.nav-tabs a[href="#panel-3"]').tab('show');
	showmenu('panel-3','menu-3');
	gettag("a");
</script>
