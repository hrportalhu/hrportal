                 
<div class="col-md-8 column">
<ol class="breadcrumb">
	<li><a href="/">Főoldal</a></li>
	<li><a href="/diakmunka_szakerto.html">Diákmunka szakért&#x0151;je</a></li>
</ol>
<article>
	<div class="row clearfix md-margin">
		<div class="col-md-12 column">
			<h1 class="md-margin">Diákmunka szakért&#x0151;je</h1>
			
				<div class="media">
			 <p><img class="img-smaller" src="images/hegedus_cs_120.jpg"></a><strong>Hegedűs Csaba </strong> vagyok, vagyok, a Prohumán Diákmunka operatív igazgatója. 13 év szakmai tapasztalattal rendelkezem a humánerőforrás szolgáltatások területén, mely időszak alatt az ország egész területén dolgoztam diákmunkaerőt foglalkoztató kis, közepes és multinacionális vállalatokkal. A Prohumán Diákmunka legfőbb célkitűzése az, hogy minél több diákkal megismertesse a munka világát, valamint olyan ismeretekkel, gyakorlati tapasztalattal gazdagítsa a diákokat, hogy ha kikerülnek a „nagybetűs életbe”, akkor megállják a helyüket a folyamatosan változó munkaerőpiacon.<br />Sokan, akik diákmunkát vállalnak alapvető jogokkal és kötelezettségekkel nincsenek tisztában (miért is lennének, hiszen eddig nem kellett ilyen ismeretekkel rendelkezniük). Elsősorban ezzel kapcsolatos témákban várom a kérdéseket.
	<br />Közérdeklődésre számot tartó kérdéseiket és a válaszokat - igény szerint név nélkül - megjelentetjük oldalunkon is. 
				 Kérdéseire, lehetőség szerint, egy héten belül válaszolunk.</p>
 
			</div> 	
			<div id="messageform">
				<form class="form-horizontal md-margin" onsubmit="return false;">
					  <div class="form-group">
						<label for="edt_name" class="col-sm-3 control-label normal-label">Az Ön neve*:</label>
						<div class="col-sm-6">
						  <input type="text" class="form-control" id="edt_name" name="edt_name">
						</div>
					  </div>
					  <div class="form-group">
						<label for="edt_email" class="col-sm-3 control-label normal-label">E-mail címe*:</label>
						<div class="col-sm-6">
						  <input type="text" class="form-control" id="edt_email" name="edt_email">
						</div>
					  </div>
					  <div class="form-group">
						<label for="edt_tel" class="col-sm-3 control-label normal-label">Telefonszáma:</label>
						<div class="col-sm-6">
						  <input type="text" class="form-control" id="edt_tel" name="edt_tel">
						</div>
					  </div>
					  
					  <div class="form-group">
						<label for="edt_cname" class="col-sm-3 control-label normal-label">Cégének neve:</label>
						<div class="col-sm-6">
						  <input type="text" class="form-control" id="edt_cname"  name="edt_cname">
						</div>
					  </div>
					  
					  <div class="form-group">
						<label for="edt_employ" class="col-sm-3 control-label normal-label">Cégének alkalmazotti létszáma:</label>
						<div class="col-sm-6">
						  <input type="text" class="form-control" id="edt_employ"  name="edt_employ">
						</div>
					  </div>

					  

					  <div class="form-group">
						<label for="edt_message" class="col-sm-3 control-label normal-label">Kérdése*:</label>
						<div class="col-sm-6">
						  <textarea class="form-control" id="edt_message" name="edt_message"></textarea>
						</div>
					  </div>
					  <div class="form-group">
						<label for="text5" class="col-sm-3 control-label normal-label"></label>
						<div class="col-sm-6">
						  <i>*: kötelezően kitöltendő mező </i>												    
						 </div>
					  </div>
					  
					  <div class="form-group">
							<div class="col-sm-offset-3 col-sm-3">
							   <button class="btn btn btn-main" onclick="javascript:gomail_specialist('diakmunka');">Mehet</button>
							</div>
						</div>
				</form>	
									<div class="fb-like" data-href="http://www.hrportal.hu<?=$URI;?>" data-layout="standard" data-action="recommend" data-show-faces="true" data-share="true"></div>
					<div style="clear:both;height:1px;"></div> 
					<script src="//platform.linkedin.com/in.js" type="text/javascript">
					lang: en_US
					</script>
					<script type="IN/Share" data-url="http://www.hrportal.hu<?=$URI;?>" data-counter="right"></script>

					<div id="plusone-div" > <g:plusone size="normal"></g:plusone></div>
					<script type="text/javascript">
					gapi.plusone.go("plusone-div");
					</script>	
			</div>
		</div>
	</div>
</article>
</div>
