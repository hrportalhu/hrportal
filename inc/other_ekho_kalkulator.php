 <!-- ***** CENTER COLUMN ***** -->
 <div class="col-md-8 column">
   <ol class="breadcrumb">
		<li><a href="/">Főoldal</a>
		</li>
		<li><a href="/ekho_kalkulator.html">EKHO kalkulátor 2017</a>
		</li>
	</ol>
	<article>
		<div class="row clearfix md-margin">
			<div class="col-md-12 column">
			   <h1  class="md-margin" >EKHO kalkulátor 2017</h1>                                       
			   <form class="form-horizontal md-margin" onsubmit="return false;">
				   <div style="clear:both;height:1px;"></div><div style="text-align:center;font-weight:bold;font-size:14px;margin-bottom:10px;">Az EKHO kalkulátor motorját a <a href="http://www.nexon.hu/ber-es-munkaugyi-modul" target="_blank"><img src="images/banner/nexon_small.jpg" alt="nexon" /></a> biztosítja.</div><div style="clear:both;height:1px;"></div>
				  <div class="form-group">
					<label for="edt_brutto" class="col-sm-7 control-label">Bruttó havi munkabér (Ft)</label>
					<div class="col-sm-2">
					  <input type="text" class="form-control" id="edt_brutto" name="edt_brutto" placeholder="200000">
					</div>
				  </div>
				  <div class="form-group">
					<label for="edt_elt" class="col-sm-7 control-label">Eltartottak (gyermekek) száma *</label>
					<div class="col-sm-2">
					  <select class="form-control"  name="edt_elt" id="edt_elt">
						  <option value="0" selected>nincsenek</option>
						  <option value="1">1</option>
						  <option value="2">2</option>
						  <option value="3">3</option>
						  <option value="4">4</option>
						  <option value="5">5</option>
						</select>
					</div>
				  </div>

				  <div class="form-group">
					<label for="edt_gyerekek" class="col-sm-7 control-label">Ebből kedvezményezett eltartottak száma **</label>
					<div class="col-sm-2">
					  <select class="form-control" name="edt_gyerekek" id="edt_gyerekek">
						  <option value="0" selected>nincsenek</option>
						  <option value="1">1</option>
						  <option value="2">2</option>
						  <option value="3">3</option>
						  <option value="4">4</option>
						  <option value="5">5</option>
						</select>
					</div>
				  </div>

				  <div class="form-group">
					<label for="text1" class="col-sm-7 control-label">Gyermekét/gyermekeit egyedül neveli</label>
					<div class="col-sm-2">
					  <select class="form-control" name="edt_egyedulnevel" id="edt_egyedulnevel">
						  <option value="1">igen</option>
						  <option value="0" selected>nem</option>
					   </select>
					</div>
				  </div>

				  <div class="form-group" name="edt_frisshazas"  id="edt_frisshazas">
					<label for="text1" class="col-sm-7 control-label">Friss házasok <span>(2014. dec. 31. után kötött házasságokra)</span></label>
					<div class="col-sm-2">
						<select class="form-control">
						  <option value="1">igen</option>
						  <option value="0" selected>nem</option>
					   </select>
					</div>
				  </div>                                            
			  
				  <div class="form-group">
					<div class="col-sm-offset-7 col-sm-10">
					  <button class="btn btn btn-main" onclick="javascript:ekhokalkulator();">Számol</button>
					</div>
				  </div>
				</form>
				
				<div id="ekhokalkulator_cont"></div>
			</div>
		</div>
	</article>
						<div class="fb-like" data-href="http://www.hrportal.hu<?=$URI;?>" data-layout="standard" data-action="recommend" data-show-faces="true" data-share="true"></div>
					<div style="clear:both;height:1px;"></div> 
					<script src="//platform.linkedin.com/in.js" type="text/javascript">
					lang: en_US
					</script>
					<script type="IN/Share" data-url="http://www.hrportal.hu<?=$URI;?>" data-counter="right"></script>

					<div id="plusone-div" > <g:plusone size="normal"></g:plusone></div>
					<script type="text/javascript">
					gapi.plusone.go("plusone-div");
					</script>
	 
</div>
<!-- ***** CENTER COLUMN ***** -->
	
<script>
	
	$('.nav-tabs a[href="#panel-4"]').tab('show');
	
</script>

