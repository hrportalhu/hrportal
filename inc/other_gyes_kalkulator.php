 <!-- ***** CENTER COLUMN ***** -->
 <div class="col-md-8 column">
   <ol class="breadcrumb">
		<li><a href="/">Főoldal</a>
		</li>
		<li><a href="/gyes_kalkulator.html"> GYES kalkulátor 2017</a>
		</li>
	</ol>
	<article>
		<div class="row clearfix md-margin">
			<div class="col-md-12 column">
			   <h1  class="md-margin" > GYES kalkulátor 2017</h1>                                       
			   <form class="form-horizontal md-margin" onsubmit="return false;">
				  <div id="errordiv" style="color:#EA0000;text-align:center;font-size:13px;font-weight:bold;"></div>
				  <div class="form-group">
					<label for="almak" class="col-sm-7 control-label">Gyermek születésének (várható) id&#x0151;pontja:</label>
					<div class="col-sm-2">
					  <input type="text" class="form-control" id="almak" name="almak" />
					</div>
				  </div>

                                           
				  <div class="form-group">
					<label for="megszunes" class="col-sm-7 control-label">Gyes jogosultsági idejét, összegét befolyásoló körülmény:</label>
					<div class="col-sm-4">
					 <select name='gyes_jog' id="gyes_jog" class="form-control">
						<option value='0'> Nincs </option>
						<option value='1'> Ikergyermekek </option>
						<option value='2'> Súlyosan fogyatékos </option>
						<option value='3'> Tartósan beteg </option>
					</select>
					</div>
				  </div>
                                           
				  <div class="form-group">
					<label for="havi_br" class="col-sm-7 control-label">Havi bruttó jövedelem (Ft):</label>
					<div class="col-sm-2">
					  <input type="text" class="form-control" id="havi_br" name="havi_br" />
					</div>
				  </div>
                                           
				  <div class="form-group">
					<label for="havi_br" class="col-sm-7 control-label">El&#x0151;z&#x0151; évi átlagjövedelem / nap (táppénz alap):</label>
					<div class="col-sm-2">
					 <div id="napiatlag">0 ft/nap</div>
					</div>
				  </div>
                                           
				<blockquote>
				  <p><i>*Kalkulátorunk 365 napos biztosítási idővel számol! Az eredmények tájékozató jelegűek!</i></p>
				</blockquote>
				  <div class="form-group">
					<div class="col-sm-offset-7 col-sm-10">
					  <button class="btn btn btn-main" onclick="javascript:gyeskalkulator();">Számol</button>
					</div>
				  </div>
				</form>

				<div id="gyeseredmeny"></div>
									<div class="fb-like" data-href="http://www.hrportal.hu<?=$URI;?>" data-layout="standard" data-action="recommend" data-show-faces="true" data-share="true"></div>
					<div style="clear:both;height:1px;"></div> 
					<script src="//platform.linkedin.com/in.js" type="text/javascript">
					lang: en_US
					</script>
					<script type="IN/Share" data-url="http://www.hrportal.hu<?=$URI;?>" data-counter="right"></script>

					<div id="plusone-div" > <g:plusone size="normal"></g:plusone></div>
					<script type="text/javascript">
					gapi.plusone.go("plusone-div");
					</script>
			</div>
		</div>
	</article>
	 
</div>
<!-- ***** CENTER COLUMN ***** -->
	
<script>
	
	$('.nav-tabs a[href="#panel-4"]').tab('show');
	$('#almak').datepicker({
		closeText: 'bezárás',
		prevText: '&laquo;&nbsp;vissza',
		nextText: 'előre&nbsp;&raquo;',
		currentText: 'ma',
		monthNames: ['Január', 'Február', 'Március', 'Április', 'Május', 'Június',
		'Július', 'Augusztus', 'Szeptember', 'Október', 'November', 'December'],
		monthNamesShort: ['Jan', 'Feb', 'Már', 'Ápr', 'Máj', 'Jún',
		'Júl', 'Aug', 'Szep', 'Okt', 'Nov', 'Dec'],
		dayNames: ['Vasárnap', 'Hétfö', 'Kedd', 'Szerda', 'Csütörtök', 'Péntek', 'Szombat'],
		dayNamesShort: ['Vas', 'Hét', 'Ked', 'Sze', 'Csü', 'Pén', 'Szo'],
		dayNamesMin: ['V', 'H', 'K', 'Sze', 'Cs', 'P', 'Szo'],
		weekHeader: 'Hé',
		dateFormat: 'yy-mm-dd',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''});

</script>

