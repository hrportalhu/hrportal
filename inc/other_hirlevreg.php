                           <div class="col-md-8 column">
                            <ol class="breadcrumb">
                                <li><a href="#">Főoldal</a>
                                </li>
                                <li><a href="/hirlevreg.html">Regisztráció</a>
                                </li>
                               
                            </ol>
                            <article>
                                <div class="row clearfix md-margin">
                                    <div class="col-md-12 column">


											<h1>Regisztráció</h1>
											
											<p class="md-margin">Szeretnénk minél többet megtudni olvasóinkról. Kérjük, segítse munkánkat az alábbi információkkal.<br> <i>Mielőtt regisztrálna, kérjük olvassal el <a href="/adatvedelem.html">adatvédelmi politikánkat.</a></i></p>
											<div id="messageform">
											<form  class="form-horizontal md-margin" action="http://www.hrportal.hu/hirlevel/subscribe" method="POST" accept-charset="utf-8">
												  <div class="form-group">
												    <label for="text1" class="col-sm-3 control-label normal-label">Az Ön neve*:</label>
												    <div class="col-sm-6">
												      <input type="text" class="form-control" name="name" id="name">
												    </div>
												  </div>
												  
												  <div class="form-group">
												    <label for="text2" class="col-sm-3 control-label normal-label">Cége*:</label>
												    <div class="col-sm-6">
												      <input type="text" class="form-control" name="cegnev" id="cegnev">
												    </div>
												  </div>
												  
												  <div class="form-group">
												    <label for="text3" class="col-sm-3 control-label normal-label">Beosztása:</label>
												    <div class="col-sm-6">
												      <input type="text" class="form-control" name="beoszt" id="beoszt">
												    </div>
												  </div>
												  
												  <div class="form-group">
												    <label for="text4" class="col-sm-3 control-label normal-label">Telefonszáma:</label>
												    <div class="col-sm-6">
												      <input type="text" class="form-control" name="telefon" id="telefon">
												    </div>
												  </div>
												  
												  <div class="form-group">
												    <label for="text5" class="col-sm-3 control-label normal-label">E-mail címe*:</label>
												    <div class="col-sm-6">
												      <input type="text" class="form-control" id="email" name="email">
												    </div>
												  </div>
												  <input type="hidden" name="list" value="1XxuFW3YXr6Lo1WNR892892rSA" />
												  <div class="form-group">
												    <label for="text5" class="col-sm-3 control-label normal-label"></label>
												    <div class="col-sm-6">
												      <i>*: kötelezően kitöltendő mező </i>												    
												     </div>
												  </div>
												  
												  <div class="form-group">
													    <div class="col-sm-offset-3 col-sm-3">
															 <input class="btn btn btn-main"  type="submit" name="submit" id="submit" value="Regisztrál"/>
													     
													    </div>
												    </div>
												  
												
											</form>	
																							
									</div>
									</div>
								</div>
                            </article>
                        </div>
                        <!-- ***** CENTER COLUMN ***** -->

