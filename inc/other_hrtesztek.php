 <!-- ***** CENTER COLUMN ***** -->
 <div class="col-md-8 column">
   <ol class="breadcrumb">
		<li><a href="/">Főoldal</a>
		</li>
		<li><a href="/hrtesztek.html">HR Tesztek</a>
		</li>
	</ol>
	<article>
		<div class="row clearfix md-margin">
			<div class="col-md-12 column">
			   <h1  class="md-margin" >HR Tesztek</h1>                                       
			  <p>
					
<?php

	echo "<div >

			<h4>
				<a href=\"http://www.peopletest.net/hu/demo\" title=\"Numerikus teszt\">Numerikus teszt</a>
			</h4>
			<div>
				<div style=\"margin-top:5px;\">Ez a teszt azt m&eacute;ri, hogy a kit&ouml;lt&#x0151; mennyire k&eacute;pes t&aacute;bl&aacute;zatokb&oacute;l &eacute;s grafikonokb&oacute;l gyorsan, helyes k&ouml;vetkeztet&eacute;seket levonni. Ez a fajta k&eacute;pess&eacute;g nagyon fontos minden olyan helyzetben, ahol gyakran kell sz&aacute;madatokra &eacute;p&uuml;l&#x0151; d&ouml;nt&eacute;seket hozni.</div>
			</div>
		</div>";
	echo"<div style=\"clear:both;height:20px;\"></div>";
	echo "<div >

			<h4>
				<a href=\"http://www.peopletest.net/hu/demo\" title=\"Verb&aacute;lis teszt\">Verb&aacute;lis teszt</a>
			</h4>
			<div>
				<div style=\"margin-top:5px;\">A verb&aacute;lis teszt az &uuml;zleti dokumentumok, jelent&eacute;sek &eacute;s egy&eacute;b neh&eacute;z &iacute;rott sz&ouml;vegek &eacute;rtelmez&eacute;s&eacute;re val&oacute; k&eacute;pess&eacute;get, valamit a k&eacute;rd&eacute;sek megv&aacute;laszol&aacute;s&aacute;hoz sz&uuml;ks&eacute;ges &aacute;ltal&aacute;nos verb&aacute;lis analiz&aacute;l&oacute; &eacute;s logikai k&eacute;pess&eacute;get m&eacute;ri.</div>
			</div>
		</div>";
	echo"<div style=\"clear:both;height:20px;\"></div>";
	echo "<div >

			<h4>
				<a href=\"http://www.peopletest.net/hu/demo\" title=\"Kompetencia k&eacute;rd&#x0151;&iacute;v\">Kompetencia k&eacute;rd&#x0151;&iacute;v</a>
			</h4>
			<div>
				<div style=\"margin-top:5px;\">Ez az interneten felvehet&#x0151; r&ouml;vid k&eacute;rd&#x0151;&iacute;v a munka vil&aacute;g&aacute;ban fontos 8 &aacute;tfog&oacute; kompetenci&aacute;t vizsg&aacute;lja. A k&eacute;rd&#x0151;&iacute;vb&#x0151;l k&eacute;sz&iacute;tett jelent&eacute;s sz&aacute;mszer&#x0171;en, illetve pontokban rendezett, k&ouml;z&eacute;rthet&#x0151; &aacute;ll&iacute;t&aacute;sok seg&iacute;ts&eacute;g&eacute;vel &iacute;rja le a jel&ouml;lt jellemz&#x0151; munkahelyi viselked&eacute;s&eacute;t.</div>
			</div>
		</div>";
		echo"<div style=\"clear:both;height:20px;\"></div>";
	echo "<div >

			<h4>
				<a href=\"http://www.peopletest.net/hu/demo\" title=\"Sz&oacute;kincs teszt\">Sz&oacute;kincs teszt</a>
			</h4>
			<div>
				<div style=\"margin-top:5px;\">A sz&oacute;kincs m&eacute;r&eacute;s&eacute;n kereszt&uuml;l a teszt azt vizsg&aacute;lja, hogy a tesztkit&ouml;lt&#x0151; mennyire k&eacute;pes felismerni &eacute;s reproduk&aacute;lni a t&aacute;rsadalom &aacute;ltal m&aacute;r hozz&aacute;f&eacute;rhet&#x0151;v&eacute; tett verb&aacute;lis inform&aacute;ci&oacute;kat, ismereteket.</div>
			</div>
		</div>";
echo"<div style=\"clear:both;height:20px;\"></div>";
  
?>        
				</p>
<div ><a title='Tov&aacute;bbi aj&aacute;nlatok' href='https://www.peopletest.net'><b>Tov&aacute;bbi tesztek </b></a></div>					

				
				
			</div>
		</div>
	</article>

</div>
<!-- ***** CENTER COLUMN ***** -->
	
<script>
	
	$('.nav-tabs a[href="#panel-5"]').tab('show');
	
</script>

