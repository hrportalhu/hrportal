
                    
<div class="col-md-8 column">
<ol class="breadcrumb">
	<li><a href="/">Főoldal</a></li>
	<li><a href="/impresszum.html">Impresszum</a></li>
</ol>
<article>
	<div class="row clearfix md-margin">
		<div class="col-md-12 column">
			<h1 class="md-margin">Impresszum</h1>
			
				<div class="media">
			 <p>  <div class='ileft'><strong>Üzemeltető:</strong></div>
  <div class='iright'>Brand & Standard Capital Tanácsadó és Szolgáltató Kft.</div>
  <div id='newrow' name='newrow'></div>
  <div class='ileft'><strong>Szerkesztőség:</strong></div>
  <div class='iright'>1135 Bp., Petneházy u. 55. I./7.</div>
  <div id='newrow' name='newrow'></div>
  <div class='ileft'><strong>Tel:</strong></div>
  <div class='iright'>06.1.783.5778, 06.1.781.7596</div>
  <div id='newrow' name='newrow'></div>
  <div class='ileft'><strong>E-mail:</strong></div>
  <div class='iright'><a href='mailto:info KUKAC hrportal.hu' >info KUKAC hrportal.hu</a></div>
  <div id='newrow' name='newrow'></div>
  <div class='ileft'><strong>Web: </strong></div>
  <div class='iright'><a href='http://www.hrportal.hu' >www.hrportal.hu</a></div>
  <div id='newrow' name='newrow'></div>
  <div class='ileft'><strong>Főszerkesztő: </strong></div>
  <div class='iright'>Szegedi Júlia <br /><a href='mailto:julia.szegedi KUKAC hrportal.hu' >julia.szegedi KUKAC hrportal.hu</a></div>
  </div>
  
  <div id='newrow' name='newrow'></div>
  <div class='ileft'><strong>Szerkesztő-újságírók: </strong></div>
  <div class='iright'>Barna Eszter<br /><a href='mailto:eszter.barna KUKAC hrportal.hu' >eszter.barna KUKAC hrportal.hu</a></div> <div id='newrow' name='newrow'></div>
   <div id='newrow' name='newrow'></div>
  <div class='iright'>László Berta<br /><a href='mailto:laszlo.berta KUKAC hrportal.hu' >laszlo.berta KUKAC hrportal.hu</a></div> <div id='newrow' name='newrow'></div>
 
   
  <div id='newrow' name='newrow'></div>
  <div class='ileft'><strong>Értékesítési menedzser: </strong></div>
  <div class='iright'>Halmi Edit<br /><a href='mailto:edit.halmi KUKAC hrportal.hu' >edit.halmi KUKAC hrportal.hu</a> </div>    <div id='newrow' name='newrow'></div>
  
  <div class='ileft'><strong>Értékesítési menedzser: </strong></div>
  <div class='iright'>Pacz Dóra<br /><a href='mailto:dora.pacz KUKAC hrportal.hu' >dora.pacz KUKAC hrportal.hu</a> </div>  
	
<div class='iright'>
 <br /><br />
 A <strong>HR Portal saját cikkei szerzői jogvédelem alatt állnak</strong>, így azok 
mindennemű másodközlése kizárólag a tulajdonos írásos 
beleegyezésével lehetséges. 
<br /><br />
A HR Portalon olvasható más oldalaktól átvett tartalomért a forrásban 
megjelölt szerzőt illeti a felelősség. Az átvett cikkek másodközlésére 
kizárólag a cikk szerzőjének engedélyével kerülhet 
sor.				</p>
 
			</div> 	
	
		</div>
	</div>
</article>
</div>
