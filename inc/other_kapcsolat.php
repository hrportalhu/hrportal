                           <div class="col-md-8 column">
                            <ol class="breadcrumb">
                                <li><a href="/">Főoldal</a>
                                </li>
                                <li><a href="/kapcsolat.html">Kapcsolat</a>
                                </li>
                            </ol>
                            <article>
                                <div class="row clearfix md-margin">
                                    <div class="col-md-12 column">
										<h1>Kapcsolat</h1>
											<p class="md-margin">Kapcsolatba léphet szerkesztőségünkkel a 06.1.783.5778-as telefonszámon vagy az alábbi űrlap segítségével:</a></i></p>
											<div id="messageform">
											<form class="form-horizontal md-margin" onsubmit="return false;">
												  <div class="form-group">
												    <label for="text1" class="col-sm-3 control-label normal-label">Az Ön neve*:</label>
												    <div class="col-sm-6">
												      <input type="text" class="form-control" id="text1">
												    </div>
												  </div>
												  
												  <div class="form-group">
												    <label for="text2" class="col-sm-3 control-label normal-label">Üzenet tárgya*:</label>
												    <div class="col-sm-6">
												      <input type="text" class="form-control" id="text2">
												    </div>
												  </div>
												  
												  <div class="form-group">
												    <label for="text3" class="col-sm-3 control-label normal-label">Beosztása:</label>
												    <div class="col-sm-6">
												      <input type="text" class="form-control" id="text3">
												    </div>
												  </div>

												  
												  <div class="form-group">
												    <label for="text5" class="col-sm-3 control-label normal-label">E-mail címe*:</label>
												    <div class="col-sm-6">
												      <input type="text" class="form-control" id="text5">
												    </div>
												  </div>
												  <div class="form-group">
												    <label for="text6" class="col-sm-3 control-label normal-label">Üzenet*:</label>
												    <div class="col-sm-6">
												      <textarea class="form-control" id="text6"></textarea>
												    </div>
												  </div>
												  <div class="form-group">
												    <label for="text5" class="col-sm-3 control-label normal-label"></label>
												    <div class="col-sm-6">
												      <i>*: kötelezően kitöltendő mező </i>												    
												     </div>
												  </div>
												  
												  <div class="form-group">
													    <div class="col-sm-offset-3 col-sm-3">
													      <button class="btn btn btn-main" onclick="javascript:gomail_system();">Mehet</button>
													    </div>
												    </div>
												  
												
											</form>	
																							
									</div>
									</div>
								</div>
                            </article>

                        </div>

