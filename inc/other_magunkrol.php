
                    
<div class="col-md-8 column">
<ol class="breadcrumb">
	<li><a href="/">Főoldal</a></li>
	<li><a href="/magunkrol.html">Magunkról</a></li>
</ol>
<article>
	<div class="row clearfix md-margin">
		<div class="col-md-12 column">
			<h1 class="md-margin">Magunkról</h1>
			
				<div class="media">
			 <b>Tisztelt Érdeklődő!</b>
<br /><br />
Köszönjük, hogy <strong>HR Portal</strong> (<em>Human Resource = Emberi Erőforrás</em>) oldalait látogatja és reméljük, hogy sok érdekes és értékes információval, hasznos eszközzel tudjuk az Ön munkáját segíteni.
<br /><br />
A HR Portal 2003. márciusi indulása óta változatlan koncepcióval, egy humán szakmai információs oldal kíván lenni. Egy portál, ahol a munkaügy, személyügy, humánpolitika iránt érdeklődők a friss munkaügyi hírek, tanulmányok és munkaerőpiaci információkon túl a humán erőforrás szakma gyakorlati üzleti világába is betekintést nyerhetnek.
<br /><br />
A HR Portal folyamatosan bővül, fejlődik, ennek Ön is aktív részese lehet. Kérjük, ossza meg velünk elképzeléseit, örömmel fogadjuk minden javaslatát, szakmai anyagait, publikációit. 
<br /><br />
Kellemes időtöltést kívánunk!
<br /><br />
Üdvözlettel:
<br /><br />
a HR Portal csapata
<br /><br /><br />
 
			</div> 	
	
		</div>
	</div>
</article>
</div>
