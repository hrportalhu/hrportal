
                  
                  
                           <div class="col-md-8 column">
                            <ol class="breadcrumb">
                                <li><a href="/">Főoldal</a>
                                </li>
                                <li><a href="/munkaido_2014.html">Munkaidő naptár - 2014</a>
                                </li>
                            </ol>
                            <article>
                                <div class="row clearfix md-margin">
                                    <div class="col-md-12 column">
											<h1>Munkaidő naptár - 2014</h1>
											<div class="calbox">
			<div class="caltrm">Január</div>
			<div class="caltrd">
				<div class="caltd">Hé</div><div class="caltd">Ke</div><div class="caltd">Sz</div><div class="caltd">Cs</div>	<div class="caltd">Pé</div><div class="caltd">Sz</div><div class="caltd">Va</div>
			</div>
			<div class="caltr">
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltdred">1</div>
				<div class="caltd">2</div>
				<div class="caltd">3</div>
				<div class="caltdb">4</div>
				<div class="caltdb">5</div>
			</div>
			<div class="caltr">
				<div class="caltd">6</div>
				<div class="caltd">7</div>
				<div class="caltd">8</div>
				<div class="caltd">9</div>
				<div class="caltd">10</div>
				<div class="caltdb">11</div>
				<div class="caltdb">12</div>
			</div>
			<div class="caltr">
				<div class="caltd">13</div>
				<div class="caltd">14</div>
				<div class="caltd">15</div>
				<div class="caltd">16</div>
				<div class="caltd">17</div>
				<div class="caltdb">18</div>
				<div class="caltdb">19</div>
			</div>
			<div class="caltr">
				<div class="caltd">20</div>
				<div class="caltd">21</div>
				<div class="caltd">23</div>
				<div class="caltd">23</div>
				<div class="caltd">24</div>
				<div class="caltdb">25</div>
				<div class="caltdb">26</div>
			</div>
			<div class="caltr">
				<div class="caltd">27</div>
				<div class="caltd">28</div>
				<div class="caltd">29</div>
				<div class="caltd">30</div>
				<div class="caltd">31</div>
				<div class="caltdb"></div>
				<div class="caltdb"></div>
			</div>
			<div class="caltr">
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltdb"></div>
				<div class="caltdb"></div>
			</div>
			<div class="caltrm">22 munkanap 176 óra </div>
		</div>
		
		<div class="calbox">
			<div class="caltrm">Február</div>
			<div class="caltrd">
				<div class="caltd">Hé</div><div class="caltd">Ke</div><div class="caltd">Sz</div><div class="caltd">Cs</div>	<div class="caltd">Pé</div><div class="caltd">Sz</div><div class="caltd">Va</div>
			</div>
			<div class="caltr">
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltdb">1</div>
				<div class="caltdb">2</div>
			</div>
			<div class="caltr">
				<div class="caltd">3</div>
				<div class="caltd">4</div>
				<div class="caltd">5</div>
				<div class="caltd">6</div>
				<div class="caltd">7</div>
				<div class="caltdb">8</div>
				<div class="caltdb">9</div>
			</div>
			<div class="caltr">
				<div class="caltd">10</div>
				<div class="caltd">11</div>
				<div class="caltd">12</div>
				<div class="caltd">13</div>
				<div class="caltd">14</div>
				<div class="caltdb">15</div>
				<div class="caltdb">16</div>
			</div>
			<div class="caltr">
				<div class="caltd">17</div>
				<div class="caltd">18</div>
				<div class="caltd">19</div>
				<div class="caltd">20</div>
				<div class="caltd">21</div>
				<div class="caltdb">22</div>
				<div class="caltdb">23</div>
			</div>
			<div class="caltr">
				<div class="caltd">24</div>
				<div class="caltd">25</div>
				<div class="caltd">26</div>
				<div class="caltd">27</div>
				<div class="caltd">28</div>
				<div class="caltdb"></div>
				<div class="caltdb"></div>
			</div>
			<div class="caltr">
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltdb"></div>
				<div class="caltdb"></div>
			</div>
			<div class="caltrm">20 munkanap 160 óra </div>
		</div>
		
		<div class="calbox">
			<div class="caltrm">Március</div>
			<div class="caltrd">
				<div class="caltd">Hé</div><div class="caltd">Ke</div><div class="caltd">Sz</div><div class="caltd">Cs</div>	<div class="caltd">Pé</div><div class="caltd">Sz</div><div class="caltd">Va</div>
			</div>
			<div class="caltr">
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltdb">1</div>
				<div class="caltdb">2</div>
			</div>
			<div class="caltr">
				<div class="caltd">3</div>
				<div class="caltd">4</div>
				<div class="caltd">5</div>
				<div class="caltd">6</div>
				<div class="caltd">7</div>
				<div class="caltdb">8</div>
				<div class="caltdb">9</div>
			</div>
			<div class="caltr">
				<div class="caltd">10</div>
				<div class="caltd">11</div>
				<div class="caltd">12</div>
				<div class="caltd">13</div>
				<div class="caltd">14</div>
				<div class="caltdred">15</div>
				<div class="caltdb">16</div>
			</div>
			<div class="caltr">
				<div class="caltd">17</div>
				<div class="caltd">18</div>
				<div class="caltd">19</div>
				<div class="caltd">20</div>
				<div class="caltd">21</div>
				<div class="caltdb">22</div>
				<div class="caltdb">23</div>
			</div>
			<div class="caltr">
				<div class="caltd">24</div>
				<div class="caltd">25</div>
				<div class="caltd">26</div>
				<div class="caltd">27</div>
				<div class="caltd">28</div>
				<div class="caltdb">29</div>
				<div class="caltdb">30</div>
			</div>
			<div class="caltr">
				<div class="caltd">31</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltdb"></div>
				<div class="caltdb"></div>
			</div>
			<div class="caltrm">21 munkanap 168 óra </div>
		</div>
		
		<div class="calbox">
			<div class="caltrm">Április</div>
			<div class="caltrd">
				<div class="caltd">Hé</div><div class="caltd">Ke</div><div class="caltd">Sz</div><div class="caltd">Cs</div>	<div class="caltd">Pé</div><div class="caltd">Sz</div><div class="caltd">Va</div>
			</div>
			<div class="caltr">
				<div class="caltd">&nbsp;</div>
				<div class="caltd">1</div>
				<div class="caltd">2</div>
				<div class="caltd">3</div>
				<div class="caltd">4</div>
				<div class="caltdb">5</div>
				<div class="caltdb">6</div>
			</div>
			<div class="caltr">
				<div class="caltd">7</div>
				<div class="caltd">8</div>
				<div class="caltd">9</div>
				<div class="caltd">10</div>
				<div class="caltd">11</div>
				<div class="caltdb">12</div>
				<div class="caltdb">13</div>
			</div>
			<div class="caltr">
				<div class="caltd">14</div>
				<div class="caltd">15</div>
				<div class="caltd">16</div>
				<div class="caltd">17</div>
				<div class="caltd">18</div>
				<div class="caltdb">19</div>
				<div class="caltdb">20</div>
			</div>
			<div class="caltr">
				<div class="caltdred">21</div>
				<div class="caltd">22</div>
				<div class="caltd">23</div>
				<div class="caltd">24</div>
				<div class="caltd">25</div>
				<div class="caltdb">26</div>
				<div class="caltdb">27</div>
			</div>
			<div class="caltr">
				<div class="caltd">28</div>
				<div class="caltd">29</div>
				<div class="caltd">30</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltdb"></div>
				<div class="caltdb"></div>
			</div>
			<div class="caltr">
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltdb"></div>
				<div class="caltdb"></div>
			</div>
			<div class="caltrm">21 munkanap 168 óra </div>
		</div>
		
		<div class="calbox">
			<div class="caltrm">Május</div>
			<div class="caltrd">
				<div class="caltd">Hé</div><div class="caltd">Ke</div><div class="caltd">Sz</div><div class="caltd">Cs</div>	<div class="caltd">Pé</div><div class="caltd">Sz</div><div class="caltd">Va</div>
			</div>
			<div class="caltr">
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltdred">1</div>
				<div class="caltdsarga">2</div>
				<div class="caltdb">3</div>
				<div class="caltdb">4</div>
			</div>
			<div class="caltr">
				<div class="caltd">5</div>
				<div class="caltd">6</div>
				<div class="caltd">7</div>
				<div class="caltd">8</div>
				<div class="caltd">9</div>
				<div class="caltdszurke">10</div>
				<div class="caltdb">11</div>
			</div>
			<div class="caltr">
				<div class="caltd">12</div>
				<div class="caltd">13</div>
				<div class="caltd">14</div>
				<div class="caltd">15</div>
				<div class="caltd">16</div>
				<div class="caltdb">17</div>
				<div class="caltdb">18</div>
			</div>
			<div class="caltr">
				<div class="caltd">19</div>
				<div class="caltd">20</div>
				<div class="caltd">21</div>
				<div class="caltd">22</div>
				<div class="caltd">23</div>
				<div class="caltdb">24</div>
				<div class="caltdb">25</div>
			</div>
			<div class="caltr">
				<div class="caltd">26</div>
				<div class="caltd">27</div>
				<div class="caltd">28</div>
				<div class="caltd">29</div>
				<div class="caltd">30</div>
				<div class="caltdb">31</div>
				<div class="caltdb"></div>
			</div>
			<div class="caltr">
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltdb"></div>
				<div class="caltdb"></div>
			</div>
			<div class="caltrm">21 munkanap 168 óra </div>
		</div>
		
		<div class="calbox">
			<div class="caltrm">Június</div>
			<div class="caltrd">
				<div class="caltd">Hé</div><div class="caltd">Ke</div><div class="caltd">Sz</div><div class="caltd">Cs</div>	<div class="caltd">Pé</div><div class="caltd">Sz</div><div class="caltd">Va</div>
			</div>
			<div class="caltr">
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltdb">&nbsp;</div>
				<div class="caltdb">1</div>
			</div>
			<div class="caltr">
				<div class="caltd">2</div>
				<div class="caltd">3</div>
				<div class="caltd">4</div>
				<div class="caltd">5</div>
				<div class="caltd">6</div>
				<div class="caltdb">7</div>
				<div class="caltdb">8</div>
			</div>
			<div class="caltr">
				<div class="caltdred">9</div>
				<div class="caltd">10</div>
				<div class="caltd">11</div>
				<div class="caltd">12</div>
				<div class="caltd">13</div>
				<div class="caltdb">14</div>
				<div class="caltdb">15</div>
			</div>
			<div class="caltr">
				<div class="caltd">16</div>
				<div class="caltd">17</div>
				<div class="caltd">18</div>
				<div class="caltd">19</div>
				<div class="caltd">20</div>
				<div class="caltdb">21</div>
				<div class="caltdb">22</div>
			</div>
			<div class="caltr">
				<div class="caltd">23</div>
				<div class="caltd">24</div>
				<div class="caltd">25</div>
				<div class="caltd">26</div>
				<div class="caltd">27</div>
				<div class="caltdb">28</div>
				<div class="caltdb">29</div>
			</div>
			<div class="caltr">
				<div class="caltd">30</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltdb"></div>
				<div class="caltdb"></div>
			</div>
			<div class="caltrm">20 munkanap 160 óra </div>
		</div>
		
		<div class="calbox">
			<div class="caltrm">Július</div>
			<div class="caltrd">
				<div class="caltd">Hé</div><div class="caltd">Ke</div><div class="caltd">Sz</div><div class="caltd">Cs</div>	<div class="caltd">Pé</div><div class="caltd">Sz</div><div class="caltd">Va</div>
			</div>
			<div class="caltr">
				<div class="caltd">&nbsp;</div>
				<div class="caltd">1</div>
				<div class="caltd">2</div>
				<div class="caltd">3</div>
				<div class="caltd">4</div>
				<div class="caltdb">5</div>
				<div class="caltdb">6</div>
			</div>
			<div class="caltr">
				<div class="caltd">7</div>
				<div class="caltd">8</div>
				<div class="caltd">9</div>
				<div class="caltd">10</div>
				<div class="caltd">11</div>
				<div class="caltdb">12</div>
				<div class="caltdb">13</div>
			</div>
			<div class="caltr">
				<div class="caltd">14</div>
				<div class="caltd">15</div>
				<div class="caltd">16</div>
				<div class="caltd">17</div>
				<div class="caltd">18</div>
				<div class="caltdb">19</div>
				<div class="caltdb">20</div>
			</div>
			<div class="caltr">
				<div class="caltd">21</div>
				<div class="caltd">22</div>
				<div class="caltd">23</div>
				<div class="caltd">24</div>
				<div class="caltd">25</div>
				<div class="caltdb">26</div>
				<div class="caltdb">27</div>
			</div>
			<div class="caltr">
				<div class="caltd">28</div>
				<div class="caltd">29</div>
				<div class="caltd">30</div>
				<div class="caltd">31</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltdb"></div>
				<div class="caltdb"></div>
			</div>
			<div class="caltr">
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltdb"></div>
				<div class="caltdb"></div>
			</div>
			<div class="caltrm">23 munkanap 184 óra </div>
		</div>
		
		<div class="calbox">
			<div class="caltrm">Augusztus</div>
			<div class="caltrd">
				<div class="caltd">Hé</div><div class="caltd">Ke</div><div class="caltd">Sz</div><div class="caltd">Cs</div>	<div class="caltd">Pé</div><div class="caltd">Sz</div><div class="caltd">Va</div>
			</div>
			<div class="caltr">
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">1</div>
				<div class="caltdb">2</div>
				<div class="caltdb">3</div>
			</div>
			<div class="caltr">
				<div class="caltd">4</div>
				<div class="caltd">5</div>
				<div class="caltd">6</div>
				<div class="caltd">7</div>
				<div class="caltd">8</div>
				<div class="caltdb">9</div>
				<div class="caltdb">10</div>
			</div>
			<div class="caltr">
				<div class="caltd">11</div>
				<div class="caltd">12</div>
				<div class="caltd">13</div>
				<div class="caltd">14</div>
				<div class="caltd">15</div>
				<div class="caltdb">16</div>
				<div class="caltdb">17</div>
			</div>
			<div class="caltr">
				<div class="caltd">18</div>
				<div class="caltd">19</div>
				<div class="caltdred">20</div>
				<div class="caltd">21</div>
				<div class="caltd">22</div>
				<div class="caltdb">23</div>
				<div class="caltdb">24</div>
			</div>
			<div class="caltr">
				<div class="caltd">25</div>
				<div class="caltd">26</div>
				<div class="caltd">27</div>
				<div class="caltd">28</div>
				<div class="caltd">29</div>
				<div class="caltdb">30</div>
				<div class="caltdb">31</div>
			</div>
			<div class="caltr">
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltdb"></div>
				<div class="caltdb"></div>
			</div>
			<div class="caltrm">20 munkanap 160 óra </div>
		</div>
		
		<div class="calbox">
			<div class="caltrm">Szeptember</div>
			<div class="caltrd">
				<div class="caltd">Hé</div><div class="caltd">Ke</div><div class="caltd">Sz</div><div class="caltd">Cs</div>	<div class="caltd">Pé</div><div class="caltd">Sz</div><div class="caltd">Va</div>
			</div>
			<div class="caltr">
				<div class="caltd">1</div>
				<div class="caltd">2</div>
				<div class="caltd">3</div>
				<div class="caltd">4</div>
				<div class="caltd">5</div>
				<div class="caltdb">6</div>
				<div class="caltdb">7</div>
			</div>
			<div class="caltr">
				<div class="caltd">8</div>
				<div class="caltd">9</div>
				<div class="caltd">10</div>
				<div class="caltd">11</div>
				<div class="caltd">12</div>
				<div class="caltdb">13</div>
				<div class="caltdb">14</div>
			</div>
			<div class="caltr">
				<div class="caltd">15</div>
				<div class="caltd">16</div>
				<div class="caltd">17</div>
				<div class="caltd">18</div>
				<div class="caltd">19</div>
				<div class="caltdb">20</div>
				<div class="caltdb">21</div>
			</div>
			<div class="caltr">
				<div class="caltd">22</div>
				<div class="caltd">23</div>
				<div class="caltd">24</div>
				<div class="caltd">25</div>
				<div class="caltd">26</div>
				<div class="caltdb">27</div>
				<div class="caltdb">28</div>
			</div>
			<div class="caltr">
				<div class="caltd">29</div>
				<div class="caltd">30</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltdb"></div>
				<div class="caltdb"></div>
			</div>
			<div class="caltr">
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltdb"></div>
				<div class="caltdb"></div>
			</div>
			<div class="caltrm">22 munkanap 176 óra </div>
		</div>
		
		<div class="calbox">
			<div class="caltrm">Október</div>
			<div class="caltrd">
				<div class="caltd">Hé</div><div class="caltd">Ke</div><div class="caltd">Sz</div><div class="caltd">Cs</div>	<div class="caltd">Pé</div><div class="caltd">Sz</div><div class="caltd">Va</div>
			</div>
			<div class="caltr">
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">1</div>
				<div class="caltd">2</div>
				<div class="caltd">3</div>
				<div class="caltdb">4</div>
				<div class="caltdb">5</div>
			</div>
			<div class="caltr">
				<div class="caltd">6</div>
				<div class="caltd">7</div>
				<div class="caltd">8</div>
				<div class="caltd">9</div>
				<div class="caltd">10</div>
				<div class="caltdb">11</div>
				<div class="caltdb">12</div>
			</div>
			<div class="caltr">
				<div class="caltd">13</div>
				<div class="caltd">14</div>
				<div class="caltd">15</div>
				<div class="caltd">16</div>
				<div class="caltd">17</div>
				<div class="caltdszurke">18</div>
				<div class="caltdb">19</div>
			</div>
			<div class="caltr">
				<div class="caltd">20</div>
				<div class="caltd">21</div>
				<div class="caltd">22</div>
				<div class="caltdred">23</div>
				<div class="caltdsarga">24</div>
				<div class="caltdb">25</div>
				<div class="caltdb">26</div>
			</div>
			<div class="caltr">
				<div class="caltd">27</div>
				<div class="caltd">28</div>
				<div class="caltd">29</div>
				<div class="caltd">30</div>
				<div class="caltd">31</div>
				<div class="caltdb"></div>
				<div class="caltdb"></div>
			</div>
			<div class="caltr">
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltdb"></div>
				<div class="caltdb"></div>
			</div>
			<div class="caltrm">22 munkanap 176 óra </div>
		</div>
		
		<div class="calbox">
			<div class="caltrm">November</div>
			<div class="caltrd">
				<div class="caltd">Hé</div><div class="caltd">Ke</div><div class="caltd">Sz</div><div class="caltd">Cs</div>	<div class="caltd">Pé</div><div class="caltd">Sz</div><div class="caltd">Va</div>
			</div>
			<div class="caltr">
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltdred">1</div>
				<div class="caltdb">2</div>
			</div>
			<div class="caltr">
				<div class="caltd">3</div>
				<div class="caltd">4</div>
				<div class="caltd">5</div>
				<div class="caltd">6</div>
				<div class="caltd">7</div>
				<div class="caltdb">8</div>
				<div class="caltdb">9</div>
			</div>
			<div class="caltr">
				<div class="caltd">10</div>
				<div class="caltd">11</div>
				<div class="caltd">12</div>
				<div class="caltd">13</div>
				<div class="caltd">14</div>
				<div class="caltdb">15</div>
				<div class="caltdb">16</div>
			</div>
			<div class="caltr">
				<div class="caltd">17</div>
				<div class="caltd">18</div>
				<div class="caltd">19</div>
				<div class="caltd">20</div>
				<div class="caltd">21</div>
				<div class="caltdb">22</div>
				<div class="caltdb">23</div>
			</div>
			<div class="caltr">
				<div class="caltd">24</div>
				<div class="caltd">25</div>
				<div class="caltd">26</div>
				<div class="caltd">27</div>
				<div class="caltd">28</div>
				<div class="caltdb">29</div>
				<div class="caltdb">30</div>
			</div>
			<div class="caltr">
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltdb"></div>
				<div class="caltdb"></div>
			</div>
			<div class="caltrm">20 munkanap 160 óra </div>
		</div>
		
		<div class="calbox">
			<div class="caltrm">December</div>
			<div class="caltrd">
				<div class="caltd">Hé</div><div class="caltd">Ke</div><div class="caltd">Sz</div><div class="caltd">Cs</div>	<div class="caltd">Pé</div><div class="caltd">Sz</div><div class="caltd">Va</div>
			</div>
			<div class="caltr">
				<div class="caltd">1</div>
				<div class="caltd">2</div>
				<div class="caltd">3</div>
				<div class="caltd">4</div>
				<div class="caltd">5</div>
				<div class="caltdb">6</div>
				<div class="caltdb">7</div>
			</div>
			<div class="caltr">
				<div class="caltd">8</div>
				<div class="caltd">9</div>
				<div class="caltd">10</div>
				<div class="caltd">11</div>
				<div class="caltd">12</div>
				<div class="caltdszurke">13</div>
				<div class="caltdb">14</div>
			</div>
			<div class="caltr">
				<div class="caltd">15</div>
				<div class="caltd">16</div>
				<div class="caltd">17</div>
				<div class="caltd">18</div>
				<div class="caltd">19</div>
				<div class="caltdb">20</div>
				<div class="caltdb">21</div>
			</div>
			<div class="caltr">
				<div class="caltd">22</div>
				<div class="caltd">23</div>
				<div class="caltdsarga">24</div>
				<div class="caltdred">25</div>
				<div class="caltdred">26</div>
				<div class="caltdb">27</div>
				<div class="caltdb">28</div>
			</div>
			<div class="caltr">
				<div class="caltd">29</div>
				<div class="caltd">30</div>
				<div class="caltd">31</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltdb"></div>
				<div class="caltdb"></div>
			</div>
			<div class="caltr">
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltd">&nbsp;</div>
				<div class="caltdb"></div>
				<div class="caltdb"></div>
			</div>
			<div class="caltrm">21 munkanap 168 óra </div>
		</div>
		
		
					
		<div style="font-size:15px;">Jelmagyarázat:</div>
		<div class="caltdred">&nbsp;</div>&nbsp;Munkaszüneti nap
		<div style="clear:both;height:5px;"></div>
		<div class="caltdsarga">&nbsp;</div>&nbsp;Áthelyezett pihenőnap
		<div style="clear:both;height:5px;"></div>
		<div class="caltdszurke">&nbsp;</div>&nbsp;Áthelyezett munkanap
		<div style="clear:both;height:5px;"></div>
		<div class="caltd">&nbsp;</div>&nbsp;Munkanap
		<div style="clear:both;height:5px;"></div>
		<div class="caltdb">&nbsp;</div>&nbsp;Heti pihenőnap
		<div style="clear:both;height:25px;"></div>
		<div style="width:323px;margin:0 auto;"><a href="http://www.bdo.hu/" target="_blank"><img src="images/banner/bdologo.png" alt="bdo magyarorszag" /></a></div>
		
										   
										   	

											
											
									</div>
								</div>
                            </article>
                            					<div class="fb-like" data-href="http://www.hrportal.hu<?=$URI;?>" data-layout="standard" data-action="recommend" data-show-faces="true" data-share="true"></div>
					<div style="clear:both;height:1px;"></div> 
					<script src="//platform.linkedin.com/in.js" type="text/javascript">
					lang: en_US
					</script>
					<script type="IN/Share" data-url="http://www.hrportal.hu<?=$URI;?>" data-counter="right"></script>

					<div id="plusone-div" > <g:plusone size="normal"></g:plusone></div>
					<script type="text/javascript">
					gapi.plusone.go("plusone-div");
					</script>
                        </div>
                        <!-- ***** CENTER COLUMN ***** -->

