<a href="http://www.hrportal.hu/rdl/tagads/1000106/www.viennalife.hu/hello-biztos-nyugdij" target="_blank"><img src="http://www.hrportal.hu/images/banner/vienna/HRportal_980x60.gif" class="img-responsive"></a><img src="http://www.hrportal.hu/rdl/tagads/10000107/www.hrportal.hu/redot.gif"></img>
			
			<div style="clear:both;height:10px;"></div>
   
<SCRIPT LANGUAGE="JavaScript">


function ValidateForm(form){

   if(IsEmpty(form.edt_brutto)){ 
      alert('Kérjük, adja meg a bruttó havi munkabért!') 
      form.edt_brutto.focus(); 
      return false; 
   } 
 
 
   if (!IsNumeric(form.edt_brutto.value)) { 
      alert('Kérjük, csak számjegyeket adjon meg!') 
      form.edt_brutto.focus(); 
      return false; 
      } 
 
   if (form.edt_elt.value < form.edt_gyerekek.value ) { 
      alert('Az eltartottak száma nem lehet alacsonyabb a kedvezményezett eltartott gyermekek számánál!') 
      form.edt_brutto.focus(); 
      return false; 
      }  
 
return true;
 
} 
</SCRIPT>      
 <div class="col-md-8 column">
   <ol class="breadcrumb">
		<li><a href="/">Főoldal</a>
		</li>
		<li><a href="/nyugdijkalkulator.html">Nyugdíjkalkulátor</a>
		</li>
	</ol>
	<article>
		<div class="row clearfix md-margin ">
			<div class="col-md-12 column">
			   <h1  class="md-margin" >Nyugdíjkalkulátor</h1>
			   <?
			   if(isset($_GET['szuletesi_ev'])){

					$szuletesi_ev="1975";
					$MUNKAKEZDES_EVE="1995";
					$MUNKA_JELLEGE=$_GET['MUNKA_JELLEGE'];
					$BEOSZTAS=$_GET['BEOSZTAS'];
					$BRUTTO_HAVI_FIZ_MUNKAKEZDES=$_GET['BRUTTO_HAVI_FIZ_MUNKAKEZDES'];
					$BRUTTO_HAVI_FIZ_JELENLEG=$_GET['BRUTTO_HAVI_FIZ_JELENLEG'];
					$BBONUSZ=$_GET['BBONUSZ'];
					$EXTRA_EVEK_MULT=$_GET['EXTRA_EVEK_MULT'];
					$EXTRA_EVEK_JOVO=$_GET['EXTRA_EVEK_JOVO'];
					$KIESO_EVEK_MULT=$_GET['KIESO_EVEK_MULT'];
					$KIESO_EVEK_JOVO=$_GET['KIESO_EVEK_JOVO'];
					if($_GET['szuletesi_ev']!=""){
						include("inc/ajax_nyugdijkalkulator.php");
					}
				}
				else{
					$szuletesi_ev="1975";
					$MUNKAKEZDES_EVE="1995";
					$MUNKA_JELLEGE="";
					$BEOSZTAS="";
					$BRUTTO_HAVI_FIZ_MUNKAKEZDES="110000";
					$BRUTTO_HAVI_FIZ_JELENLEG="300000";
					$BBONUSZ="";
					$EXTRA_EVEK_MULT="";
					$EXTRA_EVEK_JOVO="";
					$KIESO_EVEK_MULT="";
					$KIESO_EVEK_JOVO="";
				}		
			   ?>                           
			   <form class="form-horizontal md-margin" action="/nyugdijkalkulator.html" method="get" onsubmit="javascript:return ValidateForm(this)" >
				   <!--<div style="clear:both;height:1px;"></div><div style="text-align:center;font-weight:bold;font-size:14px;margin-bottom:10px;">A bérkalkulátor motorját a <a href="http://www.nexon.hu/ber-es-munkaugyi-modul" target="_blank"><img src="images/banner/nexon_small.jpg" alt="nexon" /></a> biztosítja.</div><div style="clear:both;height:1px;"></div>-->
				  <h3>Általános adatok</h3>
				  <div class="form-group">
					<label for="szuletesi_ev" class="col-sm-7 control-label">Születés éve</label>
					<div class="col-sm-2">
					  <input type="text" class="form-control" id="szuletesi_ev" name="szuletesi_ev" value="<?=$szuletesi_ev;?>">
					</div>
				  </div>
				  
				  <div class="form-group">
					<label for="MUNKAKEZDES_EVE" class="col-sm-7 control-label">Munkakezdés éve</label>
					<div class="col-sm-2">
					  <input type="text" class="form-control" id="MUNKAKEZDES_EVE" name="MUNKAKEZDES_EVE" value="<?=$MUNKAKEZDES_EVE;?>">
					</div>
				  </div>
				 
				 
				  <div class="form-group">
					<label for="MUNKA_JELLEGE" class="col-sm-7 control-label">Szellemi vagy fizikai munkát végez?</label>
					<div class="col-sm-2">
					  <select class="form-control"  name="MUNKA_JELLEGE" id="MUNKA_JELLEGE">
						  <option value="fizikai" <?=($MUNKA_JELLEGE=="fizikai")?"selected":"";?> >Fizikai</option>
						  <option value="szellemi" <?=($MUNKA_JELLEGE=="szellemi")?"selected":"";?>>Szellemi</option>
						</select>
					</div>
				  </div>
				 
				  <div class="form-group">
					<label for="BEOSZTAS" class="col-sm-7 control-label">Ön alkalmazott vagy vezető?</label>
					<div class="col-sm-2">
					  <select class="form-control"  name="BEOSZTAS" id="BEOSZTAS">
						  <option value="vezeto" <?=($BEOSZTAS=="vezeto")?"selected":"";?> >Vezető</option>
						  <option value="alkalmazott" <?=($BEOSZTAS=="alkalmazott")?"selected":"";?>>Alkalmazott</option>
						</select>
					</div>
				  </div>
				  
				 <h3>Jövedelmi adatok</h3>
				  <div class="form-group">
					<label for="BRUTTO_HAVI_FIZ_MUNKAKEZDES" class="col-sm-7 control-label">Mekkora volt az áltagos havi bruttó bére a munkakezdés évében?</label>
					<div class="col-sm-2">
					  <input type="text" class="form-control" id="BRUTTO_HAVI_FIZ_MUNKAKEZDES" name="BRUTTO_HAVI_FIZ_MUNKAKEZDES" value="<?=$BRUTTO_HAVI_FIZ_MUNKAKEZDES;?>">
					</div>
				  </div>
				 
				  <div class="form-group">
					<label for="BRUTTO_HAVI_FIZ_JELENLEG" class="col-sm-7 control-label">Jelenlegi havi bruttó fizetes</label>
					<div class="col-sm-2">
					  <input type="text" class="form-control" id="BRUTTO_HAVI_FIZ_JELENLEG" name="BRUTTO_HAVI_FIZ_JELENLEG" value="<?=$BRUTTO_HAVI_FIZ_JELENLEG;?>">
					</div>
				  </div>
				 
				  <div class="form-group">
					<label for="BBONUSZ" class="col-sm-7 control-label">"A bruttó fizetésén felül évente átlagosan hány százalék bónuszban (prémiumban, jutalomban) részesül (az éves bruttó fizetés százalékában adja meg)."</label>
					<div class="col-sm-2">
					  <input type="text" class="form-control" id="BBONUSZ" name="BBONUSZ" value="<?=$BBONUSZ;?>">
					</div>
				  </div>
				 <h3>Tb nyugdíj</h3>
					<div class="form-group">
						<label for="EXTRA_EVEK_MULT" class="col-sm-7 control-label">Extra évek:</label>
						<div class="col-sm-2">
						  <input type="text" class="form-control" id="EXTRA_EVEK_MULT" name="EXTRA_EVEK_MULT" value="<?=$EXTRA_EVEK_MULT;?>">
						</div>
					  </div>
					<div class="form-group">
						<label for="KIESETT_EVEK_MULT" class="col-sm-7 control-label">Kiesett évek:</label>
						<div class="col-sm-2">
						  <input type="text" class="form-control" id="KIESETT_EVEK_MULT" name="KIESETT_EVEK_MULT" value="<?=$KIESETT_EVEK_MULT;?>">
						</div>
					  </div>
					<div class="form-group">
						<label for="EXTRA_EVEK_JOVO" class="col-sm-7 control-label">Várhatóan még hány extra évet szerez a nyugdíjba vonulásig?</label>
						<div class="col-sm-2">
						  <input type="text" class="form-control" id="EXTRA_EVEK_JOVO" name="EXTRA_EVEK_JOVO" <?=$EXTRA_EVEK_JOVO;?>>
						</div>
					  </div>
					<div class="form-group">
						<label for="KIESO_EVEK_JOVO" class="col-sm-7 control-label">Várhatóan hány éve esik még ki a nyugdíjba vonulásig?</label>
						<div class="col-sm-2">
						  <input type="text" class="form-control" id="KIESO_EVEK_JOVO" name="KIESO_EVEK_JOVO" <?=$KIESO_EVEK_JOVO;?>>
						</div>
					  </div>
					  			
				 </fieldset>
				  <div class="form-group">
					<div class="col-sm-offset-7 col-sm-10">
					  <button type="submit" class="btn btn btn-main" >Számol</button>
					  
					</div>
				  </div>
				</form>
				
				<div id="berkalkulator_2016_cont"></div>
									<div class="fb-like" data-href="http://www.hrportal.hu<?=$URI;?>" data-layout="standard" data-action="recommend" data-show-faces="true" data-share="true"></div>
					<div style="clear:both;height:1px;"></div> 
					<script src="//platform.linkedin.com/in.js" type="text/javascript">
					lang: en_US
					</script>
					<script type="IN/Share" data-url="http://www.hrportal.hu<?=$URI;?>" data-counter="right"></script>

					<div id="plusone-div" > <g:plusone size="normal"></g:plusone></div>
					<script type="text/javascript">
					gapi.plusone.go("plusone-div");
					</script>
			</div>
		</div>
	</article>
	 
</div>
<!-- ***** CENTER COLUMN ***** -->
	
<script>
	
	$('.nav-tabs a[href="#panel-4"]').tab('show');
	
</script>

