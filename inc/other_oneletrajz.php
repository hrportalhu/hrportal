 <article>



                                <div class="row clearfix md-margin">
                                    <div class="col-md-12 column">

										<h1  class="md-margin" >Önéletrajz sablonok</h1>
                                       
                                        <p><strong>Kérjük, válasszon melyik szakmának az önéletrajzát szeretné letölteni:</strong></p>
                                        
                                        <p class="md-margin" >Ha az Ön által keresett kifejezést nem találná, kérjük, írja meg nekünk az <a href="mailto:info@hrportal.hu">info@hrportal.hu</a> címre és rövidesen bővítjük a listát. Köszönjük.</p>
                                        
                                        
                                        
										<div>

											  <!-- Nav tabs -->
											  <ul class="nav nav-tabs dictionary sm-margin" role="tablist">
											    <li class="active"><a href="#a" role="tab" data-toggle="tab">A</a></li>
											    <li><a href="#b" role="tab" data-toggle="tab">B</a></li>
											   
											    <li><a href="#e" role="tab" data-toggle="tab">E</a></li>
											    <li><a href="#f" role="tab" data-toggle="tab">F</a></li>
											    <li><a href="#g" role="tab" data-toggle="tab">G</a></li>
											    <li><a href="#h" role="tab" data-toggle="tab">H</a></li>
											    <li><a href="#i" role="tab" data-toggle="tab">I</a></li>

											    <li><a href="#k" role="tab" data-toggle="tab">K</a></li>
											    <li><a href="#l" role="tab" data-toggle="tab">L</a></li>
											    <li><a href="#m" role="tab" data-toggle="tab">M</a></li>
											    <li><a href="#n" role="tab" data-toggle="tab">N</a></li>

											    <li><a href="#p" role="tab" data-toggle="tab">P</a></li>
											    <li><a href="#r" role="tab" data-toggle="tab">R</a></li>
											    <li><a href="#s" role="tab" data-toggle="tab">S</a></li>
											    <li><a href="#t" role="tab" data-toggle="tab">T</a></li>
											    <li><a href="#u" role="tab" data-toggle="tab">U</a></li>
											    <li><a href="#v" role="tab" data-toggle="tab">V</a></li>

											  </ul>
											
											  <!-- Tab panes -->
											  <div class="tab-content md-margin">
											    <div role="tabpanel" class="tab-pane active" id="a">
												    <ul >
													  <li><a href="/c/asszisztens-anna-jobangel-oneletrajz-mintaja-adminisztratoroknak-20140407.html"><i class="fa fa-angle-right"></i> Adminisztrátor</a></li>
													  <li><a href="/c/a-tjobs-oneletrajz-mintaja-asztalosoknak-20140522.html"><i class="fa fa-angle-right"></i> Asztalos</a></li>
													  <li><a href="/c/a-tjobs-nemet-nyelvu-oneletrajz-mintaja-asztalosoknak-20150302.html"><i class="fa fa-angle-right"></i> Asztalos - német nyelvű</a></li>
													  <li><a href="/c/a-tjobs-angol-nyelvu-oneletrajz-mintaja-asztalosoknak-20150310.html"><i class="fa fa-angle-right"></i> Asztalos - angol nyelvű</a></li>
													  <li><a href="/c/cv-nevjegy-allaskeresoknek-20150105.html"><i class="fa fa-angle-right"></i> Álláskereső</a></li>
													  <li><a href="/c/oneletrajz-minta-aruhaz-igazgatoknak-20150116.html"><i class="fa fa-angle-right"></i> Áruház igazgatóknak</a></li>
													  <li><a href="/c/oneletrajz-minta-arufeltoltoi-munkakorre-20150116.html"><i class="fa fa-angle-right"></i> Arufeltöltő</a></li>
													  <li><a href="/c/oneletrajz-minta-junior-account-managereknek-20150327.html"><i class="fa fa-angle-right"></i> Account manager - Junior</a></li>
													  <li><a href="/c/oneletrajz-minta-account-managereknek-20150330.html"><i class="fa fa-angle-right"></i> Account manager</a></li>
													  <li><a href="/c/angol-nyelvu-oneletrajz-minta-autobusz-szereloknek-20151022.html"><i class="fa fa-angle-right"></i> Autóbusz szerelő</a></li>
													  <li><a href="/c/angol-nyelvu-oneletrajz-minta-autofenyezok-szamara-20151022.html"><i class="fa fa-angle-right"></i> Autófényező - angol</a></li>
													</ul>
												 </div>
											    <div role="tabpanel" class="tab-pane" id="b">												    
												    <ul>
													  <li><a href="/c/oneletrajz-minta-boltvezetoknek-20150116.html"><i class="fa fa-angle-right"></i> Boltvezető</a></li>
													  <li><a href="/c/angol-nyelvu-oneletrajz-minta-badogosoknak-a-tjobstol-20151026.html"><i class="fa fa-angle-right"></i> Bádogos - angol nyelvű</a></li>
												    </ul>
												 </div>
											   											
											  
											    <div role="tabpanel" class="tab-pane" id="e">												    
												    <ul>
													  <li><a href="/c/egyszeru-sablon-foleg-magyar-allasokra-a-verzio-20130528.html"><i class="fa fa-angle-right"></i> Egyszerű sablon főleg magyar állásokra A verzió</a></li>
													  <li><a href="/c/egyszeru-sablon-foleg-magyar-allasokra-b-verzio-20130528.html"><i class="fa fa-angle-right"></i> Egyszerű sablon főleg magyar állásokra B verzió</a></li>
													  <li><a href="/c/egyszeru-sablon-foleg-magyar-allasokra-c-verzio-20130528.html"><i class="fa fa-angle-right"></i> Egyszerű sablon főleg magyar állásokra C verzió</a></li>
													  <li><a href="/c/oneletrajz-minta-eladoknak-20150116.html"><i class="fa fa-angle-right"></i> Eladó</a></li>
													  <li><a href="/c/angol-nyelvu-oneletrajz-minta-ertekesitoknek-20151022.html"><i class="fa fa-angle-right"></i> Eladó - angol nyelvű</a></li>
												    </ul>
												 </div>
											   											
											    <div role="tabpanel" class="tab-pane" id="f">												    
												    <ul>
													  <li><a href="/c/frizura-nelli-europass-oneletrajz-sablon-20130528.html"><i class="fa fa-angle-right"></i> Fodrász</a></li>
													  <li><a href="/c/angol-nyelvu-oneletrajz-minta-favagoknak-20151026.html"><i class="fa fa-angle-right"></i> Favágó - angol nyelvű</a></li>
												    </ul>
												 </div>
											    <div role="tabpanel" class="tab-pane" id="g">												    
												    <ul>
													  <li><a href="/c/jobangel-oneletrajzmintaja-gyedrol-visszateroknek-20141029.html"><i class="fa fa-angle-right"></i> GYED-ről visszatérőknek</a></li>
												    </ul>
												 </div>
											    <div role="tabpanel" class="tab-pane" id="h">												    
												    <ul>
													  <li><a href="/c/a-tjobs-oneletrajz-mintaja-hegesztoknek-20140522.html"><i class="fa fa-angle-right"></i> Hegesztő</a></li>
													  <li><a href="/c/a-tjobs-nemet-nyelvu-oneletrajz-mintaja-hegesztoknek-20150302.html"><i class="fa fa-angle-right"></i> Hegesztő - német nyelvű</a></li>
													  <li><a href="/c/a-tjobs-oneletrajz-mintaja-hentesek-szamara-20140522.html"><i class="fa fa-angle-right"></i> Hentes</a></li>
													  <li><a href="/c/a-tjobs-nemet-nyelvu-oneletrajzmintaja-hazigondozoi-allashoz-20150310.html"><i class="fa fa-angle-right"></i> Házi gondozó - német nyelvű</a></li>
													  <li><a href="/c/a-tjobs-angol-nyelvu-oneletrajz-mintaja-apolok-szamara-20150310.html"><i class="fa fa-angle-right"></i> Házi gondozó - angol nyelvű</a></li>
													  <li><a href="/c/egy-ujabb-angol-oneletrajz-minta-apoloknak-20151022.html"><i class="fa fa-angle-right"></i> Házi gondozó </a></li>
												    </ul>
												 </div>
											    <div role="tabpanel" class="tab-pane" id="i">												    
												    <ul>
													  <li><a href="/c/allaskereso-peter-monster-oneletrajz-minta-informatikusoknak-20130620.html"><i class="fa fa-angle-right"></i> Informatikus 1</a></li>
													  <li><a href="/c/sablon-sandor-a-beck-and-partners-oneletrajz-mintaja-informatikusoknak-20140425.html"><i class="fa fa-angle-right"></i> Informatikus 2</a></li>
													  <li><a href="/c/jobangel-oneletrajzmintaja-idosebb-munkavallaloknak-20141029.html"><i class="fa fa-angle-right"></i> Idősebb munkavállalók</a></li>
												    </ul>
												 </div>

											    <div role="tabpanel" class="tab-pane" id="k">												    
												    <ul>
													  <li><a href="/c/kiss-gabor-europass-oneletrajz-sablon-20130528.html"><i class="fa fa-angle-right"></i> Könyvelő</a></li>
													  <li><a href="/c/kiss-anita-monster-oneletrajz-sablon-kereskedelmi-terulet-20130528.html"><i class="fa fa-angle-right"></i> Kereskedelem</a></li>
													  <li><a href="/c/jobangel-oneletrajzmintaja-karriervaltas-esetere-20141029.html"><i class="fa fa-angle-right"></i> Karrierváltás</a></li>
													  <li><a href="/c/szakmai-oneletrajz-sablon-komoly-szakmai-multra-20150330.html"><i class="fa fa-angle-right"></i> Komoly szakmai múlt</a></li>
													  <li><a href="/hr/angol-nyelvu-kreativ-cv-minta-20160930.html"><i class="fa fa-angle-right"></i> Angol nyelvű kreatív CV-minta</a></li>
													  <li><a href="/hr/magyar-nyelvu-kreativ-cv-minta-20160930.html"><i class="fa fa-angle-right"></i> Magyar nyelvű kreatív CV-minta</a></li>
												    </ul>
												 </div>
											    <div role="tabpanel" class="tab-pane" id="l">												    
												    <ul>
													  <li><a href="/c/oneletrajz-minta-logisztikai-menedzsereknek-20150327.html"><i class="fa fa-angle-right"></i> Logisztikai menedzser</a></li>
												    </ul>
												 </div>
											    <div role="tabpanel" class="tab-pane" id="m">												    
												    <ul>
													  <li><a href="/c/marketinges-mariann-jobangel-oneletrajz-mintaja-marketingeseknek-20140407.html"><i class="fa fa-angle-right"></i> Marketing</a></li>
													  <li><a href="/c/mernok-miklos-a-beck-and-partners-oneletrajz-mintaja-mernokoknek-20140425.html"><i class="fa fa-angle-right"></i> Mérnök</a></li>
													  <li><a href="/c/jobangel-oneletrajza-jelenleg-munkanelkuliek-szamara-20141029.html"><i class="fa fa-angle-right"></i> Munkanélküli</a></li>
												    </ul>
												 </div>
											    <div role="tabpanel" class="tab-pane" id="n">												    
												    <ul>
													  <li><a href="/c/noverkent-dolgozna-angliaban-ime-egy-oneletrajz-minta-20151026.html"><i class="fa fa-angle-right"></i> Nővér - angol nyelvű</a></li>
												    </ul>
												 </div>

											    <div role="tabpanel" class="tab-pane" id="p">												    
												    <ul>
													  <li><a href="/c/allaskereso-jozsef-monster-oneletrajz-minta-penzugy-20130528.html"><i class="fa fa-angle-right"></i> Pénzügy 1</a></li>
													  <li><a href="/c/penzugyes-piroska-jobangel-oneletrajz-mintaja-penzugyeseknek-20140407.html"><i class="fa fa-angle-right"></i> Pénzügy 2</a></li>
													  <li><a href="/c/projekt-pongrac-jobangel-oneletrajz-mintaja-projektvezetoknek-20140407.html"><i class="fa fa-angle-right"></i> Projektvezetés</a></li>
													  <li><a href="/c/oneletrajz-minta-penztarosoknak-20150116.html"><i class="fa fa-angle-right"></i> Pénztáros</a></li>
													  <li><a href="/c/oneletrajz-minta-palyakezdoknek-20150625.html"><i class="fa fa-angle-right"></i> Pályakezdőnek</a></li>
												    </ul>
												 </div>
											    <div role="tabpanel" class="tab-pane" id="r">												    
												    <ul>
													  <li><a href="/c/recepcios-rita-jobangel-oneletrajz-mintaja-20140429.html"><i class="fa fa-angle-right"></i> Recepciós</a></li>
													  <li><a href="/c/oneletrajz-minta-raktaros-munkakorhoz-20150116.html"><i class="fa fa-angle-right"></i> Raktáros</a></li>
												    </ul>
												 </div>
											    <div role="tabpanel" class="tab-pane" id="s">												    
												    <ul>
													  <li><a href="/c/a-tjobs-oneletrajz-mintaja-soforoknek-20140522.html"><i class="fa fa-angle-right"></i> Sofőr</a></li>
													  <li><a href="/c/a-tjobs-nemet-nyelvu-oneletrajz-mintaja-soforoknek-20150302.html"><i class="fa fa-angle-right"></i> Sofőr - német nyelvű</a></li>
													  <li><a href="/c/angol-nyelvu-oneletrajz-minta-tehergepkocsi-soforoknek-a-tjobstol-20151022.html"><i class="fa fa-angle-right"></i> Sofőr - angol nyelvű</a></li>
													  <li><a href="/c/a-tjobs-oneletrajz-mintaja-szakacsoknak-20140522.html"><i class="fa fa-angle-right"></i> Szakács</a></li>
													  <li><a href="/c/nemet-nyelvu-cv-minta-szakacsoknak-a-tjobstol-20150302.html"><i class="fa fa-angle-right"></i> Szakács - német nyelvű</a></li>
													  <li><a href="/c/angol-nyelvu-cv-minta-szakacsoknak-a-tjobstol-20150310.html"><i class="fa fa-angle-right"></i> Szakács - angol nyelvű</a></li>
													  <li><a href="/c/oneletrajz-mint-szallitmanyozasi-ugyintezoknek-20150327.html"><i class="fa fa-angle-right"></i> Szállítmányozási ügyintéző</a></li>
												    </ul>
												 </div>
											    <div role="tabpanel" class="tab-pane" id="t">												    
												    <ul>
													  <li><a href="/c/angol-nyelvu-oneletrajz-minta-targoncas-munkakorre-20151026.html"><i class="fa fa-angle-right"></i> Targoncás</a></li>
												    </ul>
												 </div>
											    <div role="tabpanel" class="tab-pane" id="u">												    
												    <ul>
													  <li><a href="/c/ugyfelszolgalati-vezeto-ulrika-jobangel-oneletrajz-mintaja-20140429.html"><i class="fa fa-angle-right"></i> Ügyfélszolgálati vezető</a></li>
													  <li><a href="/c/ugyfelszolgalati-munkatars-jobangel-oneletrajz-mintaja-20140429.html"><i class="fa fa-angle-right"></i> Ügyfélszolgálati munkatárs</a></li>
													  <li><a href="/c/oneletrajz-minta-uzleti-tanacsadoknak-20150625.html"><i class="fa fa-angle-right"></i> Üzleti tanácsadó</a></li>
												    </ul>
												 </div>
											    <div role="tabpanel" class="tab-pane" id="v">												    
												    <ul>
													  <li><a href="/c/minta-maria-villamosmernok-sablon-komoly-szakmai-multtal-rendelkezoknek-20130528.html"><i class="fa fa-angle-right"></i> Villamosmérnök</a></li>
													  <li><a href="/c/a-tjobs-oneletrajz-mintaja-villanyszereloknek-20140522.html"><i class="fa fa-angle-right"></i> Villanyszerelő</a></li>
													  <li><a href="/c/nemet-nyelvu-cv-minta-villanyszereloknek-a-tjobstol-20150310.html"><i class="fa fa-angle-right"></i> Villanyszerelő - német nyelvű</a></li>
													  <li><a href="/c/angol-nyelvu-oneletrajz-minta-villanyszereloknek-a-tjobstol-20150310.html"><i class="fa fa-angle-right"></i> Villanyszerelő - angol nyelvű</a></li>
												    </ul>
												 </div>
											    
											   											
											</div>                                        
                                    
                                    
                                    
                                    </div>
								</div>
                            </article>
                            					<div class="fb-like" data-href="http://www.hrportal.hu<?=$URI;?>" data-layout="standard" data-action="recommend" data-show-faces="true" data-share="true"></div>
					<div style="clear:both;height:1px;"></div> 
					<script src="//platform.linkedin.com/in.js" type="text/javascript">
					lang: en_US
					</script>
					<script type="IN/Share" data-url="http://www.hrportal.hu<?=$URI;?>" data-counter="right"></script>

					<div id="plusone-div" > <g:plusone size="normal"></g:plusone></div>
					<script type="text/javascript">
					gapi.plusone.go("plusone-div");
					</script>
<?

?>
