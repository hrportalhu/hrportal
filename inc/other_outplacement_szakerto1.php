
                    
<div class="col-md-8 column">
<ol class="breadcrumb">
	<li><a href="/">Főoldal</a></li>
	<li><a href="/outplacement_szakerto1.html">fluktuációkezelő és outplacement szakértőnk</a></li>
</ol>
<article>
	<div class="row clearfix md-margin">
		<div class="col-md-12 column">
			<h1 class="md-margin">Fluktuáció és Outplacement szakértőnk</h1>
			
				<div class="media">
			 <p><img class="img-smaller" src="images/csikos_k_120.jpg"></a><strong>Csikós-Nagy Katalin </strong> Az Evolution Group cégcsoport alapítója, 18 éve tevékenykedik a HR területen. Az első 10 évben alkalmazottként a munkáltatókat támogatta a munkaerő toborzás, kiválasztás területén, különböző HR fejlesztésekben. Nagy sikereket ért el piacvezető külföldi tulajdonú HR szolgáltató vállalatnál. 7 éve hozta létre saját vállalkozását, majd 5 éve kezdte el a munkavállalói oldal támogatását outplacement programokon keresztül, így mindennaposan látja, tapasztalja mindkét oldal kihívásait. 15 fős szakmai csapatával éves szinten közel 200 csoportos, 3000 egyéni tanácsadást lát el, többek között a Magyar Telekom, a UPC, Danone, DE, Delphi Packard elégedett ügyfelei. Az elmúlt egy évben kialakult súlyos munkaerőhiány egyik következménye a soha nem tapasztalt mértékű fluktuáció. Mi az oka? Hogy állíthatjuk meg ezt a folyamatot? Alig találunk új munkaerőt és már a régi munkavállalók is felmondanak! Mit tehetünk ellene?		<br />Közérdeklődésre számot tartó kérdéseiket és a válaszokat - igény szerint név nélkül - megjelentetjük oldalunkon is. 
				 Kérdéseire, lehetőség szerint, egy héten belül válaszolunk.</p>
 
			</div> 	
			<div id="messageform">
				<form class="form-horizontal md-margin" onsubmit="return false;">
					  <div class="form-group">
						<label for="edt_name" class="col-sm-3 control-label normal-label">Az Ön neve*:</label>
						<div class="col-sm-6">
						  <input type="text" class="form-control" id="edt_name" name="edt_name">
						</div>
					  </div>
					  <div class="form-group">
						<label for="edt_email" class="col-sm-3 control-label normal-label">E-mail címe*:</label>
						<div class="col-sm-6">
						  <input type="text" class="form-control" id="edt_email" name="edt_email">
						</div>
					  </div>
					  <div class="form-group">
						<label for="edt_tel" class="col-sm-3 control-label normal-label">Telefonszáma:</label>
						<div class="col-sm-6">
						  <input type="text" class="form-control" id="edt_tel" name="edt_tel">
						</div>
					  </div>
					  
					  <div class="form-group">
						<label for="edt_cname" class="col-sm-3 control-label normal-label">Cégének neve:</label>
						<div class="col-sm-6">
						  <input type="text" class="form-control" id="edt_cname"  name="edt_cname">
						</div>
					  </div>
					  
					  <div class="form-group">
						<label for="edt_employ" class="col-sm-3 control-label normal-label">Cégének alkalmazotti létszáma:</label>
						<div class="col-sm-6">
						  <input type="text" class="form-control" id="edt_employ"  name="edt_employ">
						</div>
					  </div>

					  

					  <div class="form-group">
						<label for="edt_message" class="col-sm-3 control-label normal-label">Kérdése*:</label>
						<div class="col-sm-6">
						  <textarea class="form-control" id="edt_message" name="edt_message"></textarea>
						</div>
					  </div>
					  <div class="form-group">
						<label for="text5" class="col-sm-3 control-label normal-label"></label>
						<div class="col-sm-6">
						  <i>*: kötelezően kitöltendő mező </i>												    
						 </div>
					  </div>
					  
					  <div class="form-group">
							<div class="col-sm-offset-3 col-sm-3">
							   <button class="btn btn btn-main" onclick="javascript:gomail_specialist('outplacement1');">Mehet</button>
							</div>
						</div>
				</form>		
									<div class="fb-like" data-href="http://www.hrportal.hu<?=$URI;?>" data-layout="standard" data-action="recommend" data-show-faces="true" data-share="true"></div>
					<div style="clear:both;height:1px;"></div> 
					<script src="//platform.linkedin.com/in.js" type="text/javascript">
					lang: en_US
					</script>
					<script type="IN/Share" data-url="http://www.hrportal.hu<?=$URI;?>" data-counter="right"></script>

					<div id="plusone-div" > <g:plusone size="normal"></g:plusone></div>
					<script type="text/javascript">
					gapi.plusone.go("plusone-div");
					</script>
			</div>
		</div>
	</div>
</article>
</div>
