
                    
<div class="col-md-8 column">
<ol class="breadcrumb">
	<li><a href="/">Főoldal</a></li>
	<li><a href="/outplacement_szakerto2.html">Outplacement szakértőnk</a></li>
</ol>
<article>
	<div class="row clearfix md-margin">
		<div class="col-md-12 column">
			<h1 class="md-margin">Outplacement szakértőnk</h1>
			
				<div class="media">
			 <p><img class="img-smaller" src="images/hadas_h_120.jpg"></a><strong>Hadas Hajdu Helga</strong> A BME Gazdaságtudományi karán, HR menedzserként végeztem. Piacvezető személyzeti szolgáltatónál személyzeti tanácsadóként, fejvadászként dolgoztam ahol 2007-ben megkaptam az év tanácsadója díjat. Munkám során tapasztaltam meg, hogy a munkavállalók és a munkáltatók között kommunikációs űr tátong. Személyes célom, hogy a munkaerő-piaci szereplők jobban megértsék egymást. 2012 óta üzleti és karrier coachként dolgozom.  "Mindenki egyedi, tökéletesen egyedi…tehát semmi szükség arra, hogy különleges akarj lenni. Már eleve az vagy." (Osho)<br />
<div style="clear:both;height:10px;"></div>
Cégünk eredményei:  
<ul>
<li>3000 egyéni tanácsadás évente fizikai, szellemi, vezetői munkakörökben, sokrétű iparágban  </li>
<li>6 hónap alatt 80%-os elhelyezkedési arány</li>
<li>98% résztvevői elégedettség </li>
</ul>
Cégünk számára kiemelt fontosságú a társadalmi szerepvállalásunk, ingyenes JOB Klub -ot 3 éve rendszeresen működtetünk.<br />
A következő témákban várjuk kérdéseiket: elbocsátás, gondoskodó leépítő programok, karrier tanácsadás, álláskeresés és munkaerő-piaci információk<br />

				<br />Közérdeklődésre számot tartó kérdéseiket és a válaszokat - igény szerint név nélkül - megjelentetjük oldalunkon is. 
				 Kérdéseire, lehetőség szerint, egy héten belül válaszolunk.</p>
 
			</div> 	
			<div id="messageform">
				<form class="form-horizontal md-margin" onsubmit="return false;">
					  <div class="form-group">
						<label for="edt_name" class="col-sm-3 control-label normal-label">Az Ön neve*:</label>
						<div class="col-sm-6">
						  <input type="text" class="form-control" id="edt_name" name="edt_name">
						</div>
					  </div>
					  <div class="form-group">
						<label for="edt_email" class="col-sm-3 control-label normal-label">E-mail címe*:</label>
						<div class="col-sm-6">
						  <input type="text" class="form-control" id="edt_email" name="edt_email">
						</div>
					  </div>
					  <div class="form-group">
						<label for="edt_tel" class="col-sm-3 control-label normal-label">Telefonszáma:</label>
						<div class="col-sm-6">
						  <input type="text" class="form-control" id="edt_tel" name="edt_tel">
						</div>
					  </div>
					  
					  <div class="form-group">
						<label for="edt_cname" class="col-sm-3 control-label normal-label">Cégének neve:</label>
						<div class="col-sm-6">
						  <input type="text" class="form-control" id="edt_cname"  name="edt_cname">
						</div>
					  </div>
					  
					  <div class="form-group">
						<label for="edt_employ" class="col-sm-3 control-label normal-label">Cégének alkalmazotti létszáma:</label>
						<div class="col-sm-6">
						  <input type="text" class="form-control" id="edt_employ"  name="edt_employ">
						</div>
					  </div>

					  

					  <div class="form-group">
						<label for="edt_message" class="col-sm-3 control-label normal-label">Kérdése*:</label>
						<div class="col-sm-6">
						  <textarea class="form-control" id="edt_message" name="edt_message"></textarea>
						</div>
					  </div>
					  <div class="form-group">
						<label for="text5" class="col-sm-3 control-label normal-label"></label>
						<div class="col-sm-6">
						  <i>*: kötelezően kitöltendő mező </i>												    
						 </div>
					  </div>
					  
					  <div class="form-group">
							<div class="col-sm-offset-3 col-sm-3">
							   <button class="btn btn btn-main" onclick="javascript:gomail_specialist('outplacement2');">Mehet</button>
							</div>
						</div>
				</form>
									<div class="fb-like" data-href="http://www.hrportal.hu<?=$URI;?>" data-layout="standard" data-action="recommend" data-show-faces="true" data-share="true"></div>
					<div style="clear:both;height:1px;"></div> 
					<script src="//platform.linkedin.com/in.js" type="text/javascript">
					lang: en_US
					</script>
					<script type="IN/Share" data-url="http://www.hrportal.hu<?=$URI;?>" data-counter="right"></script>

					<div id="plusone-div" > <g:plusone size="normal"></g:plusone></div>
					<script type="text/javascript">
					gapi.plusone.go("plusone-div");
					</script>		
			</div>
		</div>
	</div>
</article>
</div>
