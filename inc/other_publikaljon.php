                           <div class="col-md-8 column">
                            <ol class="breadcrumb">
                                <li><a href="/">Főoldal</a>
                                </li>
                                <li><a href="/publikaljon.html">Regisztráció</a>
                                </li>
                               
                            </ol>
                           <article>
	<div class="row clearfix md-margin">
		<div class="col-md-12 column">
			<h2 class="md-margin">Publikáljon Ön is a HRPortal.hu-n</h2>
			
				<div class="media">
				<p>Szeretne Ön is publikálni a HR Portalon? Ha Ön HR-rel, képzéssel, munkajoggal, cafeteriával, munkavédelemmel, interim- vagy épp változásmendzsmenttel, vagy más, a munka világához kapcsolódó területtel foglalkozó szakemberként úgy érzi, szívesen viszontlátná szakmai írásait portálunkon, vegye fel velünk a kapcsolatot. A témákat tekintve szívesen közreadunk esettanulmányokat, véleménycikkeket, HR-vezetői portrékat, szakmai napokról szóló beszámolókat.</p>
				<p class="md-margin">Kapcsolatba léphet szerkesztőségünkkel a 06.1.783.5778-as telefonszámon vagy az info@hrportal.hu e-mail címen.</a></i></p>
 
			</div> 	
	
		</div>
	</div>
</article>
                        </div>
                        <!-- ***** CENTER COLUMN ***** -->

