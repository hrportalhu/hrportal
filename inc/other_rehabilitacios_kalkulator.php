 <!-- ***** CENTER COLUMN ***** -->
 <div class="col-md-8 column">
   <ol class="breadcrumb">
		<li><a href="/">Főoldal</a>
		</li>
		<li><a href="/rehabilitacios_kalkulator.html">Rehabilitációs kalkulátor</a>
		</li>
	</ol>
	<article>
		<div class="row clearfix md-margin">
			<div class="col-md-12 column">
			   <h1  class="md-margin" >Rehabilitációs kalkulátor <img src="http://www.hrportal.hu/images/rovatszponzor/workway/wwc_rehab_kalkulator_3.jpg"></h1>                                       
			   <form class="form-horizontal md-margin" onsubmit="return false;">
				  <div id="errordiv" style="color:#EA0000;text-align:center;font-size:13px;font-weight:bold;"></div>
				  <div class="form-group">
					<label for="letszam" class="col-sm-7 control-label">CÉG átlagos statisztikai állományi létszáma:</label>
					<div class="col-sm-2">
					  <input type="text" class="form-control" id="letszam" name="letszam" value="60"/>
					</div>
				  </div>
                                           
				  <div class="form-group">
					<label for="rehabletszam" class="col-sm-7 control-label">Megváltozott munkaképességű munkavállalók foglalkoztatotti létszáma:</label>
					<div class="col-sm-2">
					  <input type="text" class="form-control" id="rehabletszam" name="rehabletszam" value="0"/>
					</div>
				  </div>
                                           
				  <div class="form-group">
					<label for="hatraho" class="col-sm-7 control-label"><?=date('Y',time());?>-es évből hátralévő hónapok száma:</label>
					<div class="col-sm-2">
					  <input type="text" class="form-control" id="hatraho" name="hatraho" value="12" />
					</div>
				  </div>
                                           
				  <div class="form-group">
					<label for="rehabber" class="col-sm-7 control-label">Megváltozott munkaképességű munkavállaló bruttó havi személyi alapbére: /Ft/</label>
					<div class="col-sm-2">
					  <input type="text" class="form-control" id="rehabber" name="rehabber" value="50000"/>
					</div>
				  </div>
                                           

				  <div class="form-group">
					<div class="col-sm-offset-7 col-sm-10">
					  <button class="btn btn btn-main" onclick="javascript:rehabkulator();">Számol</button>
					</div>
				  </div>
				</form>
				
				<div id="rehaberedmeny"></div>
									<div class="fb-like" data-href="http://www.hrportal.hu<?=$URI;?>" data-layout="standard" data-action="recommend" data-show-faces="true" data-share="true"></div>
					<div style="clear:both;height:1px;"></div> 
					<script src="//platform.linkedin.com/in.js" type="text/javascript">
					lang: en_US
					</script>
					<script type="IN/Share" data-url="http://www.hrportal.hu<?=$URI;?>" data-counter="right"></script>

					<div id="plusone-div" > <g:plusone size="normal"></g:plusone></div>
					<script type="text/javascript">
					gapi.plusone.go("plusone-div");
					</script>
			</div>
		</div>
	</article>

</div>
<!-- ***** CENTER COLUMN ***** -->
	
<script>
	
	$('.nav-tabs a[href="#panel-4"]').tab('show');
	
</script>

