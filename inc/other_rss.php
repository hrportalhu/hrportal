<div class="col-md-8 column">
<ol class="breadcrumb">
	<li><a href="/">Főoldal</a></li>
	<li><a href="/rss.html">RSS hírforrások</a></li>
</ol>
<article>
	<div class="row clearfix md-margin">
		<div class="col-md-12 column">
			<h1 class="md-margin">HR PORTAL - RSS csatornák</h1>
			<p><strong>Mi az RSS? </strong>
				<br /><br />
				Az RSS a Really Simple Syndication rövidítése. Segítségével azonnal értesülhet a HR Portalon vagy más portálokon megjelenő Önt érdeklő hírekről, szakmai cikkekről. Ehhez telepítenie kell gépére egy ún. RSS-olvasót (pl. Feedreader) és beállítani kedvenc oldalainak RSS-csatornáját.
				Ha az oldalon új hír vagy cikk jelenik meg, arról értesítést kapunk és elolvashatjuk címét, rövid összefoglalóját (lead-jét) is, valamint átkattinthatunk a teljes cikkre. 
				<br /><br />
				Néhány program önmagában támogatja az RSS-t. Ilyen például a Mozilla Firefox ("live bookmarks"-on, élő könyvjelzőkön keresztül), a Mozilla Thunderbird ("RSS accounts"-on, RSS postaládán keresztül), az Opera ("Newsfeeds"-en, hírcsatornákon keresztül), Avant Browser ("XML feeds"-en keresztül), valamint a Maxthon Browser ("RSS"-en keresztül). Jelenleg már az Internet Explorer 7.0-s és a Safari újabb verziói is támogatják a formátumot.
				<br /><br />
				Az alábbi narancssárga RSS ikonokra kattintva iratkozhat fel a hírcsatornáinkra.
				<br /><br /></p>
			<div class="media" style="margin-left:30px;">
			
<a href="http://hrportal.hu/rss/mind.xml" target="_blank"><img src="/images/img/HR_rss.gif" /></a>&nbsp;<a href="http://hrportal.hu/rss/mind.xml" target="_blank">HR Portal - Minden hírünk és cikkünk</a><br />
<a href="http://hrportal.hu/rss/hr-szakmai.xml" target="_blank"><img src="/images/img/HR_rss.gif" /></a>&nbsp;<a href="http://hrportal.hu/rss/hr-szakmai.xml" target="_blank">HR Portal - Minden HR szakmai cikkünk</a><br />
<a href="http://hrportal.hu/rss/hirek.xml" target="_blank"><img src="/images/img/HR_rss.gif" /></a>&nbsp;<a href="http://hrportal.hu/rss/hirek.xml" target="_blank">Minden hírünk (Hírek, Céghírek, Magazin)</a><br />
<a href="http://hrportal.hu/rss/hr-informatika.xml" target="_blank"><img src="/images/img/HR_rss.gif" /></a>&nbsp;<a href="http://hrportal.hu/rss/hr-informatika.xml" target="_blank">HR Informatika</a><br />
<a href="http://hrportal.hu/rss/munkajog.xml" target="_blank"><img src="/images/img/HR_rss.gif" /></a>&nbsp;<a href="http://hrportal.hu/rss/munkajog.xml" target="_blank">Munkajog</a><br />
<a href="http://hrportal.hu/rss/berenkivuli-kafeteria.xml" target="_blank"><img src="/images/img/HR_rss.gif" /></a>&nbsp;<a href="http://hrportal.hu/rss/berenkivuli-kafeteria.xml" target="_blank">Béren kvüli juttatások, kafetéria</a><br />
<a href="http://hrportal.hu/rss/trening-kepzes.xml" target="_blank"><img src="/images/img/HR_rss.gif" /></a>&nbsp;<a href="http://hrportal.hu/rss/trening-kepzes.xml" target="_blank">Tréning, képzés</a><br />
<a href="http://hrportal.hu/rss/toborzas.xml" target="_blank"><img src="/images/img/HR_rss.gif" /></a>&nbsp;<a href="http://hrportal.hu/rss/toborzas.xml" target="_blank">Toborzás</a><br />
<a href="http://hrportal.hu/rss/szervezet-fejlesztes.xml" target="_blank"><img src="/images/img/HR_rss.gif" /></a>&nbsp;<a href="http://hrportal.hu/rss/szervezet-fejlesztes.xml" target="_blank">Szervezet-fejlesztés</a><br />
<a href="http://hrportal.hu/rss/kompenzacio.xml" target="_blank"><img src="/images/img/HR_rss.gif" /></a>&nbsp;<a href="http://hrportal.hu/rss/kompenzacio.xml" target="_blank">Kompenzáció</a><br />
<a href="http://hrportal.hu/rss/munkakor.xml" target="_blank"><img src="/images/img/HR_rss.gif" /></a>&nbsp;<a href="http://hrportal.hu/rss/munkakor.xml" target="_blank">Munkakör</a><br />
<a href="http://hrportal.hu/rss/kommunikacio.xml" target="_blank"><img src="/images/img/HR_rss.gif" /></a>&nbsp;<a href="http://hrportal.hu/rss/kommunikacio.xml" target="_blank">Kommunikáció</a><br />
<a href="http://hrportal.hu/rss/teljesitmeny-ertekeles.xml" target="_blank"><img src="/images/img/HR_rss.gif" /></a>&nbsp;<a href="http://hrportal.hu/rss/teljesitmeny-ertekeles.xml" target="_blank">Teljesítmény-értékelés</a><br />
<a href="http://hrportal.hu/rss/elbocsatas.xml" target="_blank"><img src="/images/img/HR_rss.gif" /></a>&nbsp;<a href="http://hrportal.hu/rss/elbocsatas.xml" target="_blank">Elbocsátás</a><br />
<a href="http://hrportal.hu/rss/munkaeropiac.xml" target="_blank"><img src="/images/img/HR_rss.gif" /></a>&nbsp;<a href="http://hrportal.hu/rss/munkaeropiac.xml" target="_blank">Munkaerőpiac</a><br />
<a href="http://hrportal.hu/rss/munkavedelem.xml" target="_blank"><img src="/images/img/HR_rss.gif" /></a>&nbsp;<a href="http://hrportal.hu/rss/munkavedelem.xml" target="_blank">Munkavédelem</a><br />
<a href="http://hrportal.hu/rss/interim-menedzsment.xml" target="_blank"><img src="/images/img/HR_rss.gif" /></a>&nbsp;<a href="http://hrportal.hu/rss/interim-menedzsment.xml" target="_blank">Interim-menedzsment</a><br />
<a href="http://hrportal.hu/rss/hr-controlling.xml" target="_blank"><img src="/images/img/HR_rss.gif" /></a>&nbsp;<a href="http://hrportal.hu/rss/hr-controlling.xml" target="_blank">HR kontrolling</a><br />
<a href="http://hrportal.hu/rss/hr-controlling.xml" target="_blank"><img src="/images/img/HR_rss.gif" /></a>&nbsp;<a href="http://hrportal.hu/rss/atipikus_foglalkoztatas.xml" target="_blank">Atipikus foglalkoztatás</a><br />
<a href="http://hrportal.hu/rss/hr-controlling.xml" target="_blank"><img src="/images/img/HR_rss.gif" /></a>&nbsp;<a href="http://hrportal.hu/rss/osztonzes_menedzsment.xml" target="_blank">Ösztönzés-menedzsment</a><br />
<a href="http://hrportal.hu/rss/hr-controlling.xml" target="_blank"><img src="/images/img/HR_rss.gif" /></a>&nbsp;<a href="http://hrportal.hu/rss/kompetencia_ertekeles.xml" target="_blank">Kompetencia-értékelés</a><br />
<a href="http://hrportal.hu/rss/hr-controlling.xml" target="_blank"><img src="/images/img/HR_rss.gif" /></a>&nbsp;<a href="http://hrportal.hu/rss/outsourcing.xml" target="_blank">Outsourcing</a><br />
<a href="http://hrportal.hu/rss/hr-controlling.xml" target="_blank"><img src="/images/img/HR_rss.gif" /></a>&nbsp;<a href="http://hrportal.hu/rss/hr_strategia.xml" target="_blank">HR stratégia</a><br />
<a href="http://hrportal.hu/rss/hr-munkaero_kozvetites.xml" target="_blank"><img src="/images/img/HR_rss.gif" /></a>&nbsp;<a href="http://hrportal.hu/rss/hr_munkaero_kozvetites.xml" target="_blank">Munkaerő közvetítés</a><br />



			</div> 	
			
		</div>
	</div>
</article>
</div>
