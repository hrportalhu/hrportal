<?
	$today = getdate(); 
   $year_of_today = $today['year']; 
   $month_of_today = $today['month']; 
   $day_of_today = $today['mday']; 	  
?>
 <!-- ***** CENTER COLUMN ***** -->
 <div class="col-md-8 column">
   <ol class="breadcrumb">
		<li><a href="/">Főoldal</a>
		</li>
		<li><a href="/szabadsagkalkulator.html"> Szabadság kalkulátor 2016</a>
		</li>
	</ol>
	<article>
		<div class="row clearfix md-margin">
			<div class="col-md-12 column">
			   <h1  class="md-margin" > Szabadság kalkulátor 2016</h1>    
			   <?
			    if(isset($_GET['edt_szulev'])){
					
					include("inc/ajax_szabadsagkalkulator.php");
				}
				?>	                                   
			   <form class="form-horizontal md-margin" action="/szabadsagkalkulator.html" method="get">
				  <div id="errordiv" style="color:#EA0000;text-align:center;font-size:13px;font-weight:bold;"></div>
                                           
				  <div class="form-group">
					<label for="megszunes" class="col-sm-7 control-label">Munkavállaló születési éve:</label>
					<div class="col-sm-4">
					<?
					 echo "<select name=\"edt_szulev\"  id=\"edt_szulev\"  class=\"form-control\">";
						for ($i=1950; $i<=2000; $i=$i+1){
							echo "<option value=\"".$i."\"";
							if ($i==1970){echo " selected";}
							echo ">".$i."</option>";
						}
					 
					echo "</select>\n";
					?>
					</div>
				  </div>
                                           
				  <div class="form-group">
					<label for="megszunes" class="col-sm-7 control-label">Ha a munkavállaló idén kezdett dolgozni jelenlegi munkahelyén</strong>, melyik volt az első munkanapja?</label>
					<div class="col-sm-2">

						 <select name='edt_kezdoho'  id='edt_kezdoho'  class="form-control">
							<option value='0' > - </option>
							<option value='1' ><? echo $year_of_today.". "; ?>Január</option>
							<option value='2' ><? echo $year_of_today.". "; ?>Február</option>
							<option value='3' ><? echo $year_of_today.". "; ?>Március</option>
							<option value='4' ><? echo $year_of_today.". "; ?>Április</option>
							<option value='5' ><? echo $year_of_today.". "; ?>Május</option>
							<option value='6' ><? echo $year_of_today.". "; ?>Június</option>
							<option value='7' ><? echo $year_of_today.". "; ?>Július</option>
							<option value='8' ><? echo $year_of_today.". "; ?>Augusztus</option>
							<option value='9' ><? echo $year_of_today.". "; ?>Szeptember</option>
							<option value='10' ><? echo $year_of_today.". "; ?>Október</option>
							<option value='11' ><? echo $year_of_today.". "; ?>November</option>
							<option value='12' ><? echo $year_of_today.". "; ?>December</option>
						   </select>
						   </div>
						   <div class="col-sm-2">
						   <select name='edt_kezdonap' id='edt_kezdonap' class="form-control">
							<option value='0' > - </option>
							<option value='01'>1</option>
							<option value='02' >2</option>
							<option value='03' >3</option>
							<option value='04' >4</option>
							<option value='05' >5</option>
							<option value='06' >6</option>
							<option value='07' >7</option>
							<option value='08' >8</option>
							<option value='09' >9</option>
							<option value='10' >10</option>
							<option value='11' >11</option>
							<option value='12' >12</option>
							<option value='13' >13</option>
							<option value='14' >14</option>
							<option value='15' >15</option>
							<option value='16' >16</option>
							<option value='17' >17</option>
							<option value='18' >18</option>
							<option value='19' >19</option>
							<option value='20' >20</option>
							<option value='21' >21</option>
							<option value='22' >22</option>
							<option value='23' >23</option>
							<option value='24' >24</option>
							<option value='25' >25</option>
							<option value='26' >26</option>
							<option value='27' >27</option>
							<option value='28' >28</option>
							<option value='29' >29</option>
							<option value='30' >30</option>
							<option value='30' >31</option>	
						   </select>

					</div>
				
				  </div>

                                           
				  <div class="form-group">
					<label for="megszunes" class="col-sm-7 control-label">Munkavállaló tavalyról még meglévő szabadsága napokban</label>
					<div class="col-sm-4">
							<select name='edt_tavalyrol' id='edt_tavalyrol' class="form-control">
							<option value='0' > - </option>
							<option value='01'>1</option>
							<option value='02' >2</option>
							<option value='03' >3</option>
							<option value='04' >4</option>
							<option value='05' >5</option>
							<option value='06' >6</option>
							<option value='07' >7</option>
							<option value='08' >8</option>
							<option value='09' >9</option>
							<option value='10' >10</option>
							<option value='11' >11</option>
							<option value='12' >12</option>
							<option value='13' >13</option>
							<option value='14' >14</option>
							<option value='15' >15</option>
							<option value='16' >16</option>
							<option value='17' >17</option>
							<option value='18' >18</option>
							<option value='19' >19</option>
							<option value='20' >20</option>
							<option value='21' >21</option>
							<option value='22' >22</option>
							<option value='23' >23</option>
							<option value='24' >24</option>
							<option value='25' >25</option>
							<option value='26' >26</option>
							<option value='27' >27</option>
							<option value='28' >28</option>
							<option value='29' >29</option>
							<option value='30' >30</option>
							<option value='30' >31</option>	
						   </select>

					</div>
				  </div>
                                           
				  <div class="form-group">
					<label for="megszunes" class="col-sm-7 control-label">Munkavállaló 16 évesnél fiatalabb gyermekeinek száma</label>
					<div class="col-sm-4">
							<select name='edt_gyerekek' id='edt_gyerekek' class="form-control">
							<option value='0' > - nincsenek gyerekei - </option>
							<option value='1' >1</option>
							<option value='2' >2</option>
							<option value='3' >több mint 2</option>
						   </select>

					</div>
				  </div>
                                           
				  <div class="form-group">
					<label for="megszunes" class="col-sm-7 control-label">Van-e a munkavállalónak súlyos látáskárosodása?</label>
					<div class="col-sm-4">
							<select name='edt_blind'  id='edt_blind' class="form-control">
								<option value='0' > - nincs - </option>
								<option value='1' > - van - </option>
						   </select>

					</div>
				  </div>
                                           
				  <div class="form-group">
					<label for="megszunes" class="col-sm-7 control-label">Van e fogyatékkal élő gyermeke?</label>
					<div class="col-sm-4">
							<select name='edt_fogy' id='edt_fogy' class="form-control">
								<option value='0' > - nincs - </option>
								<option value='1' > - van - </option>
						   </select>

					</div>
				  </div>
                                           
				  <div class="form-group">
					<label for="megszunes" class="col-sm-7 control-label">A föld alatt állandó jelleggel dolgozó, illetve <br />ionizáló sugárzásnak kitett munkahelyen <br />naponta legalább három órát eltölt  munkavállaló?</label>
					<div class="col-sm-4">
							<select name='edt_ion' id='edt_ion' class="form-control">
								<option value='0' > - nem - </option>
								<option value='1' > - igen - </option>
						   </select>

					</div>
				  </div>

                                           
				 
				
				  <div class="form-group">
					<div class="col-sm-offset-7 col-sm-10">
					  <button class="btn btn btn-main" onclick="javascript:szabadsagkalkulator();">Számol</button>
					</div>
				  </div>
				</form>

				<div id="szabadsageredmeny"></div>
									<div class="fb-like" data-href="http://www.hrportal.hu<?=$URI;?>" data-layout="standard" data-action="recommend" data-show-faces="true" data-share="true"></div>
					<div style="clear:both;height:1px;"></div> 
					<script src="//platform.linkedin.com/in.js" type="text/javascript">
					lang: en_US
					</script>
					<script type="IN/Share" data-url="http://www.hrportal.hu<?=$URI;?>" data-counter="right"></script>

					<div id="plusone-div" > <g:plusone size="normal"></g:plusone></div>
					<script type="text/javascript">
					gapi.plusone.go("plusone-div");
					</script>
			</div>
		</div>
	</article>

</div>
<!-- ***** CENTER COLUMN ***** -->
	
<script>
	
	$('.nav-tabs a[href="#panel-4"]').tab('show');
	
</script>

