 <!-- ***** CENTER COLUMN ***** -->
 <div class="col-md-8 column">
   <ol class="breadcrumb">
		<li><a href="/">Főoldal</a>
		</li>
		<li><a href="/tappenz_kalkulator.html">Táppénz kalkulátor 2017</a>
		</li>
	</ol>
	<article>
		<div class="row clearfix md-margin">
			<div class="col-md-12 column">
			   <h1  class="md-margin" >Táppénz kalkulátor 2017</h1>                                       
			   <form class="form-horizontal md-margin" onsubmit="return false;">
				  
				  <h3>Munkaviszony:</h3>
				  <div class="form-group">
					  <div class="col-sm-12">
					<input type="radio" name="mv" value="0" checked/>&nbsp;&nbsp;Folyamatos munkaviszony<br/>
					<input type="radio" name="mv" value="1" />&nbsp;&nbsp;Legalább 180 nap munkaviszony<br/>
					<input type="radio" name="mv" value="2" />&nbsp;&nbsp;Nem rendelkezik 180 napi jövedelemmel, de a táppénzre való jogosultság első napját megelőzően van minimum 180 napi folyamatos biztosítási jogviszonya<br/>
					<input type="radio" name="mv" value="3" />&nbsp;&nbsp;Nem rendelkezik 180 napi jövedelemmel, és a táppénzre való jogosultság első napját megelőzően nincs minimum 180 naptári napi folyamatos biztosítási jogviszonya<br/><br/>
					
				  </div>
				  </div>
				  
				  <div class="form-group">
					<label for="kereset" class="col-sm-7 control-label">Szerződés szerinti havi bruttó kereset (Ft)</label>
					<div class="col-sm-2">
					  <input type="text" class="form-control" id="kereset" name="kereset" />
					</div>
				  </div>
                                           
				  <div class="form-group">
					<label for="naps" class="col-sm-7 control-label">Kieső napok(naptári) száma</label>
					<div class="col-sm-2">
					  <input type="text" class="form-control" id="naps" name="naps" />
					</div>
				  </div>
                                           

				  <div class="form-group">
					<div class="col-sm-offset-7 col-sm-10">
					  <button class="btn btn btn-main" onclick="javascript:tappenzkulator();">Számol</button>
					</div>
				  </div>
				</form>
				
				<div id="tappenzkulator_cont"></div>
									<div class="fb-like" data-href="http://www.hrportal.hu<?=$URI;?>" data-layout="standard" data-action="recommend" data-show-faces="true" data-share="true"></div>
					<div style="clear:both;height:1px;"></div> 
					<script src="//platform.linkedin.com/in.js" type="text/javascript">
					lang: en_US
					</script>
					<script type="IN/Share" data-url="http://www.hrportal.hu<?=$URI;?>" data-counter="right"></script>

					<div id="plusone-div" > <g:plusone size="normal"></g:plusone></div>
					<script type="text/javascript">
					gapi.plusone.go("plusone-div");
					</script>
			</div>
		</div>
	</article>

</div>
<!-- ***** CENTER COLUMN ***** -->
	
<script>
	
	$('.nav-tabs a[href="#panel-4"]').tab('show');
	
</script>

