
<link rel="stylesheet" href="css/bootstrap.vertical-tabs.css">
<link rel="stylesheet" href="css/flag-icon.css">
                    
<div class="col-md-8 column">
<ol class="breadcrumb">
	<li><a href="/">Főoldal</a></li>
	<li><a href="/unnepek.html">Munkaszüneti naptár </a></li>
</ol>
<article>
	<div class="row clearfix md-margin">
		<div class="col-md-12 column">
			<h1 class="md-margin">Munkaszüneti naptár</h1>
			<div class="row">
				<div class="col-md-4 col-xs-3">
					<ul class="nav nav-tabs tabs-left">
						<li class="active"><a href="#hu" data-toggle="tab"><span class="flag-icon flag-icon-hu"></span> Magyarország</a></li>
						<li><a href="#al" data-toggle="tab"><span class="flag-icon flag-icon-al"></span> Albánia</a></li>
						<li><a href="#ar" data-toggle="tab"><span class="flag-icon flag-icon-ar"></span> Argentína</a></li>
						<li><a href="#at" data-toggle="tab"><span class="flag-icon flag-icon-at"></span> Ausztria</a></li>
						<li><a href="#au" data-toggle="tab"><span class="flag-icon flag-icon-au"></span> Ausztrália</a></li>
						<li><a href="#be" data-toggle="tab"><span class="flag-icon flag-icon-be"></span> Belgium</a></li>
						<li><a href="#br" data-toggle="tab"><span class="flag-icon flag-icon-br"></span> Brazília</a></li>
						<li><a href="#bg" data-toggle="tab"><span class="flag-icon flag-icon-bg"></span> Bulgária</a></li>
						<li><a href="#cy" data-toggle="tab"><span class="flag-icon flag-icon-cy"></span> Ciprus</a></li>
						<li><a href="#cz" data-toggle="tab"><span class="flag-icon flag-icon-cz"></span> Csehország</a></li>
						<li><a href="#dk" data-toggle="tab"><span class="flag-icon flag-icon-dk"></span> Dánia</a></li>
						<li><a href="#za" data-toggle="tab"><span class="flag-icon flag-icon-za"></span> Dél-afrikai Köztársaság</a></li>
						<li><a href="#ee" data-toggle="tab"><span class="flag-icon flag-icon-ee"></span> Észtország</a></li>
						<li><a href="#fi" data-toggle="tab"><span class="flag-icon flag-icon-fi"></span> Finnország</a></li>
						<li><a href="#fr" data-toggle="tab"><span class="flag-icon flag-icon-fr"></span> Franciaország</a></li>
						<li><a href="#gr" data-toggle="tab"><span class="flag-icon flag-icon-gr"></span> Görögország</a></li>
						<li><a href="#nl" data-toggle="tab"><span class="flag-icon flag-icon-nl"></span> Hollandia</a></li>
						<li><a href="#cr" data-toggle="tab"><span class="flag-icon flag-icon-cr"></span> Horvátország</a></li>
						<li><a href="#ir" data-toggle="tab"><span class="flag-icon flag-icon-ir"></span> Írország</a></li>
						<li><a href="#is" data-toggle="tab"><span class="flag-icon flag-icon-is"></span> Izland</a></li>
						<li><a href="#il" data-toggle="tab"><span class="flag-icon flag-icon-il"></span> Izrael</a></li>
						<li><a href="#jp" data-toggle="tab"><span class="flag-icon flag-icon-jp"></span> Japán</a></li>
						<li><a href="#ca" data-toggle="tab"><span class="flag-icon flag-icon-ca"></span> Kanada</a></li>
						<li><a href="#cn" data-toggle="tab"><span class="flag-icon flag-icon-cn"></span> Kína</a></li>
						<li><a href="#cu" data-toggle="tab"><span class="flag-icon flag-icon-cu"></span> Kuba</a></li>
						<li><a href="#pl" data-toggle="tab"><span class="flag-icon flag-icon-pl"></span> Lengyelország</a></li>
						<li><a href="#lv" data-toggle="tab"><span class="flag-icon flag-icon-lv"></span> Lettország</a></li>
						<li><a href="#li" data-toggle="tab"><span class="flag-icon flag-icon-li"></span> Liechtenstein</a></li>
						<li><a href="#lt" data-toggle="tab"><span class="flag-icon flag-icon-lt"></span> Litvánia</a></li>
						<li><a href="#lu" data-toggle="tab"><span class="flag-icon flag-icon-lu"></span> Luxemburg</a></li>
						<li><a href="#mt" data-toggle="tab"><span class="flag-icon flag-icon-mt"></span> Málta</a></li>
						<li><a href="#mx" data-toggle="tab"><span class="flag-icon flag-icon-mx"></span> Mexikó</a></li>
						<li><a href="#mn" data-toggle="tab"><span class="flag-icon flag-icon-mn"></span> Mongólia</a></li>
						<li><a href="#gb" data-toggle="tab"><span class="flag-icon flag-icon-gb"></span> Nagy-Britannia</a></li>
						<li><a href="#de" data-toggle="tab"><span class="flag-icon flag-icon-de"></span> Németország</a></li>
						<li><a href="#no" data-toggle="tab"><span class="flag-icon flag-icon-no"></span> Norvégia</a></li>
						<li><a href="#it" data-toggle="tab"><span class="flag-icon flag-icon-it"></span> Olaszország</a></li>
						<li><a href="#ru" data-toggle="tab"><span class="flag-icon flag-icon-ru"></span> Oroszország</a></li>
						<li><a href="#pt" data-toggle="tab"><span class="flag-icon flag-icon-pt"></span> Portugália</a></li>
						<li><a href="#ro" data-toggle="tab"><span class="flag-icon flag-icon-ro"></span> Románia</a></li>
						<li><a href="#es" data-toggle="tab"><span class="flag-icon flag-icon-es"></span> Spanyolország</a></li>
						<li><a href="#ch" data-toggle="tab"><span class="flag-icon flag-icon-ch"></span> Svájc</a></li>
						<li><a href="#se" data-toggle="tab"><span class="flag-icon flag-icon-se"></span> Svédország</a></li>
						<li><a href="#rs" data-toggle="tab"><span class="flag-icon flag-icon-rs"></span> Szerbia</a></li>
						<li><a href="#sk" data-toggle="tab"><span class="flag-icon flag-icon-sk"></span> Szlovákia</a></li>
						<li><a href="#si" data-toggle="tab"><span class="flag-icon flag-icon-si"></span> Szlovénia</a></li>
						<li><a href="#tr" data-toggle="tab"><span class="flag-icon flag-icon-tr"></span> Törökország</a></li>
						<li><a href="#nz" data-toggle="tab"><span class="flag-icon flag-icon-nz"></span> Új-Zéland</a></li>
						<li><a href="#ua" data-toggle="tab"><span class="flag-icon flag-icon-ua"></span> Ukrajna</a></li>
						<li><a href="#us" data-toggle="tab"><span class="flag-icon flag-icon-us"></span> USA</a></li>
						<li><a href="#eu" data-toggle="tab"><span class="flag-icon flag-icon-eu"></span> Munkával kapcsolatos nevezetes világnapok</a></li>
					</ul>
				</div>
				<div class="col-md-8 col-xs-9">
					<div class="tab-content">
						<div class="tab-pane active" id="hu">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4 col-xs-12">január 1.</span><span class="col-md-8 col-xs-12">újév</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4 col-xs-12">március 15.</span><span class="col-md-8 col-xs-12">az 1848/49. évi forradalom és szabadságharc kezdete</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4 col-xs-12">május 1.</span><span class="col-md-8 col-xs-12">a munka ünnepe</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4 col-xs-12">augusztus 20.</span><span class="col-md-8 col-xs-12">Szent István ünnepe</span></div>
							<hr class="narrow">
							<div class="row"> <span class="col-md-4 col-xs-12">október 23.</span><span class="col-md-8 col-xs-12">az 1956-os forradalom kezdete</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4 col-xs-12">november 1.</span><span class="col-md-8 col-xs-12">mindenszentek</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4 col-xs-12">december 25-26</span><span class="col-md-8 col-xs-12">karácsony</span></div>
							<hr class="narrow md-margin">

							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">húsvét</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4 col-md-offset-4">pünkösd</span></div>
							<hr class="narrow">
						</div>

						<div class="tab-pane" id="al">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1-2.</span><span class="col-md-8">újév</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4">május 1.</span><span class="col-md-8">munkások ünnepe</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4">október 19.</span><span class="col-md-8">Teréz anya napja</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4">november 28.</span><span class="col-md-8">függetlenség napja</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4">november 1.</span><span class="col-md-8">mindenszentek</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4">december 25.</span><span class="col-md-8">karácsony</span></div>
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">katolikus húsvét</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4 col-md-offset-4">ortodox húsvét</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4 col-md-offset-4">kis bajram, nagy bajram /iszlám közösségekben/</span></div>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="ar">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4">Tavasz</span><span class="col-md-8">Nagypéntek</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4">május 1.</span><span class="col-md-8">munkások ünnepe</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4">május 25.</span><span class="col-md-8">az Első Nemzeti Kormány megalakulása</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4">július 9.</span><span class="col-md-8">függetlenség napja</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4">december 8.</span><span class="col-md-8">Mária szeplőtelen fogantatása</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4">25-26.</span><span class="col-md-8">karácsony</span></div>
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">húsvét</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4">március 24.</span><span class="col-md-8">1976-os katonai puccs emléknapja</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4">április 2.</span><span class="col-md-8">Malvinasi veteránok és áldozatok napja</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4">június 20.</span><span class="col-md-8">a zászló napja</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4">augusztus 17.</span><span class="col-md-8">José de San Martín emléknapja</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4">szeptember 29.</span><span class="col-md-8">feltalálók napja Bíró László József születésnapja</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4">október 12.</span><span class="col-md-8">Amerika felfedezésének ünnepe</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4">december 31.</span><span class="col-md-8">szilveszter</span></div>
							<hr class="narrow">
							<p><b>Megjegyzés:</b><br />Az ünnepek egy része Argentínában állandó időpontú, más részük áthelyezhető. Az előbbiek közé tartozik az újév (Ano Nuevo), a nagypéntek (Viernes Santo), május 1. a munka ünnepe (Dia del Trabajo), május 25., az első nemzeti kormány évfordulója (Primer Gobierno Patrio), július 9., a függetlenség napja (Dia de la Independencia), december 8, Mária szeplőtelen fogantatásának napja (Día de la Inmaculada Concepción) és a karácsony (Navidad). Az áthelyezhető (trasladables) ünnepekhez tartozó munkaszüneti napot mindig hétfőn tartják. Ha az ünnep keddre vagy szerdára esik, akkor a megelőző hétfőn, ha csütörtökre vagy péntekre, akkor a következő hétfőn.</p>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="at">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4">január 6.</span><span class="col-md-8">vízkereszt</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4">május 1.</span><span class="col-md-8">munkások ünnepe</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4">augusztus 15.</span><span class="col-md-8">nagyboldogasszony ünnepe</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4">október 26.</span><span class="col-md-8">nemzeti ünnep</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4">november 1.</span><span class="col-md-8">mindenszentek</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4">december 8.</span><span class="col-md-8">szeplőtelen fogantatás</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4">december 25-26.</span><span class="col-md-8">karácsony</span></div>
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">nagypéntek</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4 col-md-offset-4">húsvét</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4 col-md-offset-4">húsvét után 40 nappal Krisztus mennybemenetele</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4 col-md-offset-4">pünkösd</span></div>
							<hr class="narrow">
							<div class="row"><span class="col-md-4 col-md-offset-4">Úr napja </span></div>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="au">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">január 26.</span><span class="col-md-8">Ausztrália napja</span></div>
							<div class="row"><span class="col-md-4">március 14. (hétfő)</span><span class="col-md-8">munkaszüneti nap</span></div>
							<div class="row"><span class="col-md-4">április 25.</span><span class="col-md-8">Anzac nap</span></div>
							<div class="row"><span class="col-md-4">május 2. (hétfő)</span><span class="col-md-8">munkaszüneti nap</span></div>
							<div class="row"><span class="col-md-4">június 13. (hétfő)</span><span class="col-md-8">a királynő születésnapja</span></div>
							<div class="row"><span class="col-md-4">augusztus 22. (hétfő)</span><span class="col-md-8">munkaszüneti nap</span></div>
							<div class="row"><span class="col-md-4">október 3. (hétfő)</span><span class="col-md-8">munkaszüneti nap</span></div>
							<div class="row"><span class="col-md-4">november 1. (kedd)</span><span class="col-md-8">mindszentek</span></div>
							<div class="row"><span class="col-md-4">december 23-28.</span><span class="col-md-8">karácsonyi ünnepek </span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">Húsvét</span></div>
							<hr class="narrow">
							<p><b>Megjegyzés:</b><br />Ausztrál érdekesség, hogy karácsonyt és húsvétot leszámítva minden ünnepet, ünnepnapot a legközelebbi hétfőn ünnepelnek, hogy így mindig hosszú hétvége legyen. </p>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="be">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">május 1.</span><span class="col-md-8">a munka ünnepe</span></div>
							<div class="row"><span class="col-md-4">július 21.</span><span class="col-md-8">I. Lipót trónra lépésének évfordulója</span></div>
							<div class="row"><span class="col-md-4">augusztus 15.</span><span class="col-md-8">nagyboldogasszony ünnepe</span></div>
							<div class="row"><span class="col-md-4">november 1.</span><span class="col-md-8">mindenszentek napja</span></div>
							<div class="row"><span class="col-md-4">november 11.</span><span class="col-md-8">az első világháborút lezáró fegyverszüneti egyezmény napja</span></div>
							<div class="row"><span class="col-md-4">december 25.</span><span class="col-md-8">karácsony </span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">húsvét</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">pünkösd</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">áldozócsütörtök (a húsvét utáni hatodik csütörtök) </span></div>
							<hr class="narrow">
							<h4>Nem hivatalos munkaszüneti nap (csak az érintett közösségek ünneplik)</h4>
							<div class="row"><span class="col-md-4">július 11.</span><span class="col-md-8">a flamand közösség ünnepnapja  </span></div>
							<div class="row"><span class="col-md-4">szeptember 27.</span><span class="col-md-8">a francia közösség ünnepnapja   </span></div>
							<div class="row"><span class="col-md-4">november 15.</span><span class="col-md-8">a német nyelvi közösség ünnepnapja</span></div>
							<hr class="narrow">
						</div>
						
						<div class="tab-pane" id="br">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">Április 21.</span><span class="col-md-8">Tiradentes, brazil nemzeti hős napja</span></div>
							<div class="row"><span class="col-md-4">Május 1.</span><span class="col-md-8">munka ünnepe</span></div>
							<div class="row"><span class="col-md-4">Szeptember 7</span><span class="col-md-8">a függetlenség napja</span></div>
							<div class="row"><span class="col-md-4">Október 12.</span><span class="col-md-8">Aparecida-i miasszonyunk napja</span></div>
							<div class="row"><span class="col-md-4">November 15.</span><span class="col-md-8">köztársaság kikiáltása</span></div>
							<div class="row"><span class="col-md-4">December 25.</span><span class="col-md-8">karácsony</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							
						</div>
						<div class="tab-pane" id="bg">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">január 6.</span><span class="col-md-8">vízkereszt</span></div>
							<div class="row"><span class="col-md-4">március 3.</span><span class="col-md-8">nemzeti ünnep</span></div>
							<div class="row"><span class="col-md-4">május 1.</span><span class="col-md-8">a munka ünnepe</span></div>
							<div class="row"><span class="col-md-4">május 6.</span><span class="col-md-8">nemzeti ünnep</span></div>
							<div class="row"><span class="col-md-4">május 24.</span><span class="col-md-8">nemzeti ünnep</span></div>
							<div class="row"><span class="col-md-4">szeptember 6.</span><span class="col-md-8">az egyesítés napja</span></div>
							<div class="row"><span class="col-md-4">december 24-26.</span><span class="col-md-8">karácsony</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">húsvét</span></div>
							<hr class="narrow">
						</div>

						<div class="tab-pane" id="cy">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">január 6.</span><span class="col-md-8">vízkereszt</span></div>
							<div class="row"><span class="col-md-4">március 25.</span><span class="col-md-8">a görög függetlenség napja</span></div>
							<div class="row"><span class="col-md-4">április 1.</span><span class="col-md-8">a ciprusi függetlenségi harc kezdetének napja</span></div>
							<div class="row"><span class="col-md-4">augusztus 3.</span><span class="col-md-8">III. Makáriosz érsek halálának napja</span></div>
							<div class="row"><span class="col-md-4">augusztus 15.</span><span class="col-md-8">nagyboldogasszony</span></div>
							<div class="row"><span classs="col-md-4">október 28.</span><span class="col-md-8">a "nem" napja, az 1940-es Mussolini-ultimátum elutasítása</span></div>
							<div class="row"><span classs="col-md-4">december 25-26.</span><span class="col-md-8">karácsony</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">húsvét</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">nagypéntek</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">zöld hétfő/tiszta hétfő (a nagyböjt első napja) </span></div>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="cz">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">az önálló cseh állam megújulásának napja, újév</span></div>
							<div class="row"><span class="col-md-4">május 1.</span><span class="col-md-8">a munka ünnepe</span></div>
							<div class="row"><span class="col-md-4">május 8.</span><span class="col-md-8">a győzelem napja</span></div>
							<div class="row"><span class="col-md-4">július 5.</span><span class="col-md-8">Cirill és Metód szláv hittérítők ünnepe</span></div>
							<div class="row"><span class="col-md-4">július 6.</span><span class="col-md-8">Husz János mester máglyahalálának napja</span></div>
							<div class="row"><span class="col-md-4">október 28.</span><span class="col-md-8">az önálló csehszlovák állam megalapításának napja</span></div>
							<div class="row"><span class="col-md-4">november 17.</span><span class="col-md-8">a szabadságért és demokráciáért folytatott harc</span></div>
							<div class="row"><span class="col-md-4">december 24-26</span><span class="col-md-8">szenteste, karácsony</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">húsvét</span></div>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="dk">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">április 16.</span><span class="col-md-8">Margarita királynő születésnapja</span></div>
							<div class="row"><span class="col-md-4">április 18.</span><span class="col-md-8">a zászlók napja</span></div>
							<div class="row"><span class="col-md-4">június 5.</span><span class="col-md-8">az alkotmány napja</span></div>
							<div class="row"><span class="col-md-4">december 25-26</span><span class="col-md-8">szenteste, karácsony</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">húsvét</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">pünkösd</span></div>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="za">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">március 21.</span><span class="col-md-8">emberi jogok napja</span></div>
							<div class="row"><span class="col-md-4">április 27.</span><span class="col-md-8">szabadság napja</span></div>
							<div class="row"><span class="col-md-4">május 1.</span><span class="col-md-8">munka ünnepe</span></div>
							<div class="row"><span class="col-md-4">június 16.</span><span class="col-md-8">ifjúság napja</span></div>
							<div class="row"><span class="col-md-4">augusztus 9.</span><span class="col-md-8">nemzeti nőnap</span></div>
							<div class="row"><span class="col-md-4">szeptember 24.</span><span class="col-md-8">örökség napja</span></div>
							<div class="row"><span class="col-md-4">december 16.</span><span class="col-md-8">megbékélés napja</span></div>
							<div class="row"><span class="col-md-4">december 25-26.</span><span class="col-md-8">karácsony</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">nagypéntek</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">húsvét</span></div>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="ee">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">február 24.</span><span class="col-md-8">a függetlenség 1918-as kikiáltásának napja</span></div>
							<div class="row"><span class="col-md-4">június 23.</span><span class="col-md-8">a győzelem napja</span></div>
							<div class="row"><span class="col-md-4">augusztus 20.</span><span class="col-md-8">az észt függetlenség 1991-es helyreállításának napja</span></div>
							<div class="row"><span class="col-md-4">december 25-26.</span><span class="col-md-8">karácsony </span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">Húsvét</span></div>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="fi">
							<h4>Állandó:</h4>

							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">január 6.</span><span class="col-md-8">vízkereszt</span></div>
							<div class="row"><span class="col-md-4">május 1.</span><span class="col-md-8">majális</span></div>
							<div class="row"><span class="col-md-4">december 6.</span><span class="col-md-8">a függetlenség napja</span></div>
							<div class="row"><span class="col-md-4">december 24-26.</span><span class="col-md-8">karácsony</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">nagypéntek</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">húsvét</span></div>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="fr">

							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">május 1.</span><span class="col-md-8">munka ünnepe</span></div>
							<div class="row"><span class="col-md-4">május 8.</span><span class="col-md-8">győzelem napja (második világháború befejezése)</span></div>
							<div class="row"><span class="col-md-4">július 14.</span><span class="col-md-8">forradalom napja (Bastille nap)</span></div>
							<div class="row"><span class="col-md-4">augusztus 15.</span><span class="col-md-8">nagyboldogasszony ünnepe</span></div>
							<div class="row"><span class="col-md-4">november 1.</span><span class="col-md-8">mindenszentek</span></div>
							<div class="row"><span class="col-md-4">november 11.</span><span class="col-md-8">fegyverszünet napja (első világháború befejezése)</span></div>
							<div class="row"><span class="col-md-4">december 25-26.</span><span class="col-md-8">karácsony napja</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">húsvét</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">áldozócsütörtök</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">pünkösd</span></div>
							<hr class="narrow">
							<p><b>Érdekesség:</b><br />Amennyiben egy ünnep vasárnapra esik, akkor hétfő munkaszüneti nap, kivéve húsvét és pünkösd esetében. Amikor ünnep keddre vagy csütörtökre esik, gyakran átvariálják a munkanapokat Franciaországban. </p>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="gr">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">március 25.</span><span class="col-md-8">a nemzeti forradalom kezdete (1821)</span></div>
							<div class="row"><span class="col-md-4">május 1.</span><span class="col-md-8">a munka ünnepe</span></div>
							<div class="row"><span class="col-md-4">augusztus 15.</span><span class="col-md-8">nagyboldogasszony</span></div>
							<div class="row"><span class="col-md-4">október 28.</span><span class="col-md-8">a "NEM" napja (Nem Mussolini ultimátumára)</span></div>
							<div class="row"><span class="col-md-4">december 25-26.</span><span class="col-md-8">karácsony</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">húsvét</span></div>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="nl">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">április 30.</span><span class="col-md-8">a királynő születésnapja</span></div>
							<div class="row"><span class="col-md-4">május 5.</span><span class="col-md-8">a szabadság ünnepe</span></div>
							<div class="row"><span class="col-md-4">december 25-26.</span><span class="col-md-8">karácsony</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">húsvét</span></div>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="cr">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">január 6.</span><span class="col-md-8">vízkereszt</span></div>
							<div class="row"><span class="col-md-4">május 1.</span><span class="col-md-8">munka ünnepe</span></div>
							<div class="row"><span class="col-md-4">június 22.</span><span class="col-md-8">antifasiszta ellenállás emléknapja</span></div>
							<div class="row"><span class="col-md-4">június 25.</span><span class="col-md-8">nemzeti ünnep</span></div>
							<div class="row"><span class="col-md-4">augusztus 5.</span><span class="col-md-8">győzelem napja (Balkáni háború 1995.)</span></div>
							<div class="row"><span class="col-md-4">augusztus 15.</span><span class="col-md-8">nagyboldogasszony</span></div>
							<div class="row"><span class="col-md-4">október 8.</span><span class="col-md-8">függetlenségi nap (elszakadás Jugoszláviától)</span></div>
							<div class="row"><span class="col-md-4">november 1.</span><span class="col-md-8">mindenszentek</span></div>
							<div class="row"><span class="col-md-4">december 25-26.</span><span class="col-md-8">karácsony</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">nagypéntek</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">úrnapja</span></div>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="ir">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">március 17.</span><span class="col-md-8">Szent Patrik napja</span></div>
							<div class="row"><span class="col-md-4">május első hétfő</span><span class="col-md-8">munka ünnepe</span></div>
							<div class="row"><span class="col-md-4">június első hétfő</span><span class="col-md-8">munkaszüneti nap</span></div>
							<div class="row"><span class="col-md-4">augusztusi első hétfő</span><span class="col-md-8">munkaszüneti nap</span></div>
							<div class="row"><span class="col-md-4">októberi utolsó hétfő</span><span class="col-md-8">munkaszüneti nap</span></div>
							<div class="row"><span class="col-md-4">december 25.</span><span class="col-md-8">karácsony</span></div>
							<div class="row"><span class="col-md-4">december 26.</span><span class="col-md-8">Szent István napja </span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">nagypéntek</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">húsvét</span></div>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="is">
							<h4>Állandó:</h4>

							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">május 1.</span><span class="col-md-8">a munka ünnepe</span></div>
							<div class="row"><span class="col-md-4">június 17.</span><span class="col-md-8">nemzeti ünnep</span></div>
							<div class="row"><span class="col-md-4">december 25-26.</span><span class="col-md-8">karácsony </span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">húsvét</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">pünkösd</span></div>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="il">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">március 30 - április 5.</span><span class="col-md-8">Pészach (zsidó húsvét, pászkaünnep)</span></div>
							<div class="row"><span class="col-md-4">április 19.</span><span class="col-md-8">elesett hősök emlékének napja</span></div>
							<div class="row"><span class="col-md-4">április 20.</span><span class="col-md-8">Yom Ha'Atzmaut (Izrael függetlenségének napja)</span></div>
							<div class="row"><span class="col-md-4">május 19.</span><span class="col-md-8">Shavuot (A hetek ünnepe, a Pészach után hét héttel tartják, mint a keresztények a Pünkösdöt).</span></div>
							<div class="row"><span class="col-md-4">Szeptember 9 - 10.</span><span class="col-md-8">Rosh Hashana (újév)</span></div>
							<div class="row"><span class="col-md-4">Szeptember 18.</span><span class="col-md-8">Yom Kippur (Engesztelés napja)</span></div>
							<div class="row"><span class="col-md-4">Szeptember 23.</span><span class="col-md-8">Szukkot (a sátoros ünnep)</span></div>
							<div class="row"><span class="col-md-4">Szeptember 30.</span><span class="col-md-8">Simchat Torah (a Tóra örömünnepe)</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<hr class="narrow">
							<p><b>Érdekesség:</b><br />A Pészach és a Szukkot ünnepek az első nap előestéjén kezdődnek, az üzletek azokon a napokon pénteki nyitva tartás szerint vannak nyitva, az első és utolsó nap nemzeti ünnepnapnak számít. A többi napon, úgynevezett félünnep van, amikor az üzletek a megszokott nyitva tartás szerint működnek. Az ünnepek általában a megadott dátumok előestéjén kezdődnek és napnyugta utánig tartanak.A heti pihenőnap a szombat, ami péntek estétől szombat napnyugtáig tart, az üzletek és hivatalok pénteken kora délután bezárnak. A muszlim és keresztény ünnepeket is megtartják egyes régiókban, így a heti ünnepnap helyenként péntekre vagy vasárnapra is eshet.</p>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="jp">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév(Az ünneplés 3 napig tart. Dolgoznia senkinek sem szabad és, bár hivatalosan csak január 1. számít ünnepnek, a hivatalok és a vállalatok december 29. és január 3. között zárva tartanak.)</span></div>
							<div class="row"><span class="col-md-4">január második hétfője</span><span class="col-md-8">felnőtté válás ünnepe</span></div>
							<div class="row"><span class="col-md-4">február 11.e</span><span class="col-md-8">államalapítás ünnepe</span></div>
							<div class="row"><span class="col-md-4">március 21.</span><span class="col-md-8">átavaszi napéjegyenlőség, a természet ünnepe</span></div>
							<div class="row"><span class="col-md-4">április 29.</span><span class="col-md-8">növények ünnepe (a néhai Hirohito császár születésnapja, új néven megőrizték munkaszüneti napnak)</span></div>
							<div class="row"><span class="col-md-4">május 3.</span><span class="col-md-8">az Alkotmány ünnepe</span></div>
							<div class="row"><span class="col-md-4">május 4.</span><span class="col-md-8">munkaszüneti nap</span></div>
							<div class="row"><span class="col-md-4">május 5.</span><span class="col-md-8">gyermekek napja</span></div>
							<div class="row"><span class="col-md-4">szeptember 15.</span><span class="col-md-8">idősek napja, az öregek tiszteletének ünnepe</span></div>
							<div class="row"><span class="col-md-4">szeptember 23.</span><span class="col-md-8">az őszi napéjegyenlőség ünnepe</span></div>
							<div class="row"><span class="col-md-4">október 10.</span><span class="col-md-8">az egészség és sport ünnepe</span></div>
							<div class="row"><span class="col-md-4">november 23.</span><span class="col-md-8">hálaadás a munkáért</span></div>
							<div class="row"><span class="col-md-4">december 23.</span><span class="col-md-8">Akihito császár születésnapja - Japán nemzeti ünnepe</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<p><b>Érdekesség:</b><br />A három májusi, egymáshoz közel eső, gyakran szombattal és vasárnappal kiegészülő állami ünnep alkotja az ún. 'Aranyhetet'. Amennyiben egyetlen nap van két nemzeti ünnep között, akkor az is szünnap lesz. Ilyen május 4., ami minden évben szünnap. Amikor egy nemzeti ünnep vasárnapra esik, akkor a következő nem ünnepnapot, általában hétfőt, veszik ki ünnepelni.</p>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="ca">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">március 15.</span><span class="col-md-8">nemzeti ünnep</span></div>
							<div class="row"><span class="col-md-4">május 23.</span><span class="col-md-8">Viktória királynő ünnepe</span></div>
							<div class="row"><span class="col-md-4">július 1.</span><span class="col-md-8">Kanada-nap</span></div>
							<div class="row"><span class="col-md-4">szeptember 5.</span><span class="col-md-8">a munka ünnepe</span></div>
							<div class="row"><span class="col-md-4">december 25-26.</span><span class="col-md-8">karácsony</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">nagypéntek</span></div>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="cn">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1-2.</span><span class="col-md-8">újév (kétnapos ünnep)</span></div>
							<div class="row"><span class="col-md-4">május 1-3.</span><span class="col-md-8">munka ünnepe (háromnapos ünnep)</span></div>
							<div class="row"><span class="col-md-4">október 1-3.</span><span class="col-md-8">nemzeti ünnep (háromnapos ünnep)</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">kínai újév (háromnapos ünnep)</span></div>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="cu">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">a forradalom napja (1959)</span></div>
							<div class="row"><span class="col-md-4">május 1.</span><span class="col-md-8">a munka ünnepe</span></div>
							<div class="row"><span class="col-md-4">július 26.</span><span class="col-md-8">a Moncada-laktanya megtámadása évfordulójának ünnepe (1953)</span></div>
							<div class="row"><span class="col-md-4">október 10.</span><span class="col-md-8">a függetlenség napja (1868)</span></div>
							<div class="row"><span class="col-md-4">december 25.</span><span class="col-md-8">karácsony</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">

						</div>
						<div class="tab-pane" id="pl">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">május 1.</span><span class="col-md-8">a munka ünnepe</span></div>
							<div class="row"><span class="col-md-4">május 3.</span><span class="col-md-8">az alkotmány ünnepe</span></div>
							<div class="row"><span class="col-md-4">augusztus 15.</span><span class="col-md-8">nagyboldogasszony</span></div>
							<div class="row"><span class="col-md-4">november 1.</span><span class="col-md-8">mindenszentek</span></div>
							<div class="row"><span class="col-md-4">december 25-26.</span><span class="col-md-8">karácsony</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
						</div>
						<div class="tab-pane" id="lv">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">május 1.</span><span class="col-md-8">munka ünnepe</span></div>
							<div class="row"><span class="col-md-4">június 23.</span><span class="col-md-8">Szent Iván éj</span></div>
							<div class="row"><span class="col-md-4">november 18.</span><span class="col-md-8">a függetlenség napja</span></div>
							<div class="row"><span class="col-md-4">december 25-26.</span><span class="col-md-8">karácsony</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">nagypéntek</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">húsvét</span></div>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="li">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">Mária istenanyasága</span></div>
							<div class="row"><span class="col-md-4">január 6.</span><span class="col-md-8">vízkereszt</span></div>
							<div class="row"><span class="col-md-4">február 2.</span><span class="col-md-8">Urunk bemutatása a templomban (gyertyaszentelő boldogasszony)</span></div>
							<div class="row"><span class="col-md-4">március 19.</span><span class="col-md-8">Szent József</span></div>
							<div class="row"><span class="col-md-4">május 1.</span><span class="col-md-8">munkás Szent József</span></div>
							<div class="row"><span class="col-md-4">augusztus 15.</span><span class="col-md-8">nagyboldogasszony, a család védőszentjének ünnepe</span></div>
							<div class="row"><span class="col-md-4">szeptember 8.</span><span class="col-md-8">Mária születésnapja</span></div>
							<div class="row"><span class="col-md-4">november 1.</span><span class="col-md-8">mindenszentek</span></div>
							<div class="row"><span class="col-md-4">december 8.</span><span class="col-md-8">szeplőtelen fogantatás</span></div>
							<div class="row"><span class="col-md-4">december 25.</span><span class="col-md-8">karácsony</span></div>
							<div class="row"><span class="col-md-4">december 26.</span><span class="col-md-8">Szent István első vértanú </span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">húsvét</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">pünkösd</span></div>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="lt">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">január 13.</span><span class="col-md-8">a szabadság védelmezőinek napja</span></div>
							<div class="row"><span class="col-md-4">február 16.</span><span class="col-md-8">a függetlenség napja</span></div>
							<div class="row"><span class="col-md-4">március 11.</span><span class="col-md-8">Litvánia államiságának visszaállítása</span></div>
							<div class="row"><span class="col-md-4">június 14.</span><span class="col-md-8">a Gyász és Remény napja</span></div>
							<div class="row"><span class="col-md-4">június 23.</span><span class="col-md-8">a Szent Iván éji ünnepségek kezdete</span></div>
							<div class="row"><span class="col-md-4">július 6.</span><span class="col-md-8">Mindaugas király megkoronázása és az államiság napja</span></div>
							<div class="row"><span class="col-md-4">augusztus 23.</span><span class="col-md-8">a Fekete Szalag napja</span></div>
							<div class="row"><span class="col-md-4">szeptember 8.</span><span class="col-md-8">a Nemzet napja</span></div>
							<div class="row"><span class="col-md-4">november 1.</span><span class="col-md-8">mindenszentek</span></div>
							<div class="row"><span class="col-md-4">december 24-25.</span><span class="col-md-8">szenteste és karácsony</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">húsvét</span></div>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="lu">
							<h4>Állandó:</h4>

							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">május 20.</span><span class="col-md-8">Ascension</span></div>
							<div class="row"><span class="col-md-4">június 23.</span><span class="col-md-8">a Nagyherceg születésnapja</span></div>
							<div class="row"><span class="col-md-4">augusztus 15</span><span class="col-md-8">Assumption</span></div>
							<div class="row"><span class="col-md-4">november 1.</span><span class="col-md-8">mindenszentek</span></div>
							<div class="row"><span class="col-md-4">december 25</span><span class="col-md-8">karácsony</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">húsvét</span></div>
							<div class="row"><span class="col-md-4">február vége </span><span class="col-md-8">karnevál hétfő</span></div>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="mt">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">február 10.</span><span class="col-md-8">Szent Pál hajótörése</span></div>
							<div class="row"><span class="col-md-4">március 19.</span><span class="col-md-8">Szent József ünnepe</span></div>
							<div class="row"><span class="col-md-4">március 31.</span><span class="col-md-8">a szabadság napja</span></div>
							<div class="row"><span class="col-md-4">május 1.</span><span class="col-md-8">a munka ünnepe</span></div>
							<div class="row"><span class="col-md-4">június 7.</span><span class="col-md-8">Sette Giugno (1917. június 7-re emlékezés)</span></div>
							<div class="row"><span class="col-md-4">június 29.</span><span class="col-md-8">Péter Pál ünnep</span></div>
							<div class="row"><span class="col-md-4">augusztus 15.</span><span class="col-md-8">nagyboldogasszony ünnepe</span></div>
							<div class="row"><span class="col-md-4">szeptember 8.</span><span class="col-md-8">a győzelmek ünnepe (1565, 1800, 1943)</span></div>
							<div class="row"><span class="col-md-4">szeptember 21.</span><span class="col-md-8">a függetlenség napja (az 1964-ben kivívott függetlenség emlékére)</span></div>
							<div class="row"><span class="col-md-4">december 8.</span><span class="col-md-8">szeplőtelen fogantatás</span></div>
							<div class="row"><span class="col-md-4">december 13.</span><span class="col-md-8">a köztársaság napja</span></div>
							<div class="row"><span class="col-md-4">december 25.</span><span class="col-md-8">karácsony</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">nagypéntek</span></div>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="mx">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">február első hétfője:</span><span class="col-md-8">az alkotmány napja</span></div>
							<div class="row"><span class="col-md-4">március 21.</span><span class="col-md-8">Benito Juárez születésének évfordulója</span></div>
							<div class="row"><span class="col-md-4">május 1.</span><span class="col-md-8">a munka ünnepe</span></div>
							<div class="row"><span class="col-md-4">május 5.</span><span class="col-md-8">a pueblai csata évfordulója</span></div>
							<div class="row"><span class="col-md-4">szeptember 16.</span><span class="col-md-8">a függetlenség napja</span></div>
							<div class="row"><span class="col-md-4">november 2.</span><span class="col-md-8">halottak napja</span></div>
							<div class="row"><span class="col-md-4">november 20.</span><span class="col-md-8">az 1910-es forradalom kitörésének évfordulója</span></div>
							<div class="row"><span class="col-md-4">december 25.</span><span class="col-md-8">karácsony</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">nagyhét</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">pünkösd</span></div>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="mn">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">március 8.</span><span class="col-md-8">nőnap</span></div>
							<div class="row"><span class="col-md-4">június 1.</span><span class="col-md-8">anyák napja és gyermeknap</span></div>
							<div class="row"><span class="col-md-4">július 11-13.</span><span class="col-md-8">Naadam (Nemzeti Játékok)</span></div>
							<div class="row"><span class="col-md-4">november 26.</span><span class="col-md-8">függetlenség napja</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4">február elején, holdfázistól függően</span><span class="col-md-8">holdújév</span></div>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="gb">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">január 19.</span><span class="col-md-8">Martin Luther King születésnapja</span></div>
							<div class="row"><span class="col-md-4">február 16.</span><span class="col-md-8">brit nemzeti ünnep</span></div>
							<div class="row"><span class="col-md-4">május 31.</span><span class="col-md-8">emlékezés napja</span></div>
							<div class="row"><span class="col-md-4">július 4.</span><span class="col-md-8">a függetlenség napja</span></div>
							<div class="row"><span class="col-md-4">szeptember 6.</span><span class="col-md-8">a munka ünnepe</span></div>
							<div class="row"><span class="col-md-4">október 11.</span><span class="col-md-8">Kolumbusz-nap</span></div>
							<div class="row"><span class="col-md-4">december 25-26.</span><span class="col-md-8">karácsony</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">nagypéntek</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">húsvét</span></div>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="de">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">május 1.</span><span class="col-md-8">a munka ünnepe</span></div>
							<div class="row"><span class="col-md-4">május 20.</span><span class="col-md-8">a feltámadás napja</span></div>
							<div class="row"><span class="col-md-4">október 3.</span><span class="col-md-8">a német egység napja</span></div>
							<div class="row"><span class="col-md-4">december 25-26.</span><span class="col-md-8">karácsony</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">nagypéntek</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">húsvét</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">áldozócsütörtök</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">pünkösd</span></div>
							<hr class="narrow md-margin">
							<h4>Baden-Württemberg, Bajorország, Szász-Anhalt:</h4>
							<div class="row"><span class="col-md-4">január 6.</span><span class="col-md-8">vízkereszt</span></div>
							<hr class="narrow md-margin">
							<h4>Szászország, valamint Bajorország néhány területén:</h4>
							<div class="row"><span class="col-md-4">augusztus 15.</span><span class="col-md-8">nagyboldogasszony</span></div>
							<hr class="narrow md-margin">
							<h4>Brandenburg, Mecklenburg-Előpomeránia, Szász-Anhalt, Szászország, valamint Türingia Szabadállam néhány területén:</h4>
							<div class="row"><span class="col-md-4">október 31. </span><span class="col-md-8">reformáció napja</span></div>
							<hr class="narrow md-margin">
							<h4>Szászország:</h4>
							<div class="row"><span class="col-md-4">november 22. </span><span class="col-md-8">imádság és bűnbánat napja </span></div>
							<hr class="narrow md-margin">
							<h4>Bajorország, Baden-Württemberg, Észak-Rajna-Vesztfália, Rajna-vidék-Pfalz, Szászország:</h4>
							<div class="row"><span class="col-md-4">november 1.</span><span class="col-md-8">mindenszentek</span></div>
							<h4>Megjegyzés:</h4>
							<p>karácsony előestéje (december 24.) és szilveszter (december 31.) ugyan nem munkaszüneti nap, a bankok és az üzletek mégis zárva tartanak. Kölnben és környékén a karnevál (változó dátumú fesztivál, húsvét előtti hetedik hétre esik) ugyancsak munkaszüneti napszerű. </p>

						</div>
						<div class="tab-pane" id="no">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">január 5. csak délután</span><span class="col-md-8">vízkereszt estéje</span></div>
							<div class="row"><span class="col-md-4">január 6.</span><span class="col-md-8">vízkereszt</span></div>
							<div class="row"><span class="col-md-4">április 30. csak délután</span><span class="col-md-8">Valborg estéje</span></div>
							<div class="row"><span class="col-md-4">május 1.</span><span class="col-md-8">május elseje</span></div>
							<div class="row"><span class="col-md-4">június 6.</span><span class="col-md-8">nemzeti ünnep</span></div>
							<div class="row"><span class="col-md-4">június 23. csak délután</span><span class="col-md-8">Szent Iván éj</span></div>
							<div class="row"><span class="col-md-4">június 24.</span><span class="col-md-8">Szent Iván napja</span></div>
							<div class="row"><span class="col-md-4">november 3-4.</span><span class="col-md-8">mindenszentek</span></div>
							<div class="row"><span class="col-md-4">december 24-26.</span><span class="col-md-8">karácsony</span></div>
							<div class="row"><span class="col-md-4">december 31.csak délután</span><span class="col-md-8">újév estéje</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">nagycsütörtök </span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">nagypéntek</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">húsvét</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">áldozócsütörtök</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">pünkösd</span></div>
							<hr class="narrow">
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="it">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">január 6.</span><span class="col-md-8">vízkereszt</span></div>
							<div class="row"><span class="col-md-4">április 2.</span><span class="col-md-8">Szent Márk ünnepe</span></div>
							<div class="row"><span class="col-md-4">április 25.</span><span class="col-md-8">a felszabadítás ünnepe</span></div>
							<div class="row"><span class="col-md-4">május 1.</span><span class="col-md-8">a munka ünnepe</span></div>
							<div class="row"><span class="col-md-4">június 2.</span><span class="col-md-8">olasz nemzeti ünnep</span></div>
							<div class="row"><span class="col-md-4">Június 24.</span><span class="col-md-8">Keresztelő Szent János ünnepe</span></div>
							<div class="row"><span class="col-md-4">Június 29.</span><span class="col-md-8">Szent Péter és Pál apostolok ünnepe</span></div>
							<div class="row"><span class="col-md-4">Augusztus 15.</span><span class="col-md-8">Szűz Mária mennybemenetele</span></div>
							<div class="row"><span class="col-md-4">november 1.</span><span class="col-md-8">mindenszentek</span></div>
							<div class="row"><span class="col-md-4">december 8.</span><span class="col-md-8">a szeplőtelen fogantatás ünnepe</span></div>
							<div class="row"><span class="col-md-4">december 25-26.</span><span class="col-md-8">karácsony</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">húsvét</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">pünkösd</span></div>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="ru">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1-5.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">január 7.</span><span class="col-md-8">karácsony</span></div>
							<div class="row"><span class="col-md-4">február 23.</span><span class="col-md-8">az anyaország védői</span></div>
							<div class="row"><span class="col-md-4">március 8.</span><span class="col-md-8">nőnap</span></div>
							<div class="row"><span class="col-md-4">május 1.</span><span class="col-md-8">a tavasz és a munka ünnepe</span></div>
							<div class="row"><span class="col-md-4">május 9.</span><span class="col-md-8">a győzelem napja</span></div>
							<div class="row"><span class="col-md-4">november 4.</span><span class="col-md-8">a nemzeti egység napja </span></div>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="pt">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">március 15.</span><span class="col-md-8">az 1848 évi forradalom és szabadságharc ünnepe</span></div>
							<div class="row"><span class="col-md-4">április 25.</span><span class="col-md-8">a felszabadulás napja</span></div>
							<div class="row"><span class="col-md-4">május 1.</span><span class="col-md-8">a munka ünnepe</span></div>
							<div class="row"><span class="col-md-4">június 10.</span><span class="col-md-8">nemzeti ünnep</span></div>
							<div class="row"><span class="col-md-4">június 24.</span><span class="col-md-8">Szent János ünnepe</span></div>
							<div class="row"><span class="col-md-4">augusztus 15.</span><span class="col-md-8">Szűz Mária mennybemenetele</span></div>
							<div class="row"><span class="col-md-4">október 5.</span><span class="col-md-8">a köztársaság ünnepe</span></div>
							<div class="row"><span class="col-md-4">november 1.</span><span class="col-md-8">Mindszentek</span></div>
							<div class="row"><span class="col-md-4">december 1.</span><span class="col-md-8">az ifjúság ünnepe</span></div>
							<div class="row"><span class="col-md-4">december 8.</span><span class="col-md-8">Szeplőtelen Fogantatás ünnepe</span></div>
							<div class="row"><span class="col-md-4">december 25.</span><span class="col-md-8">karácsony</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">nagypéntek</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">húsvét</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">pünkösd</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">mennybemenetel</span></div>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="ro">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">január 2.</span><span class="col-md-8">újév másnapja</span></div>
							<div class="row"><span class="col-md-4">április 6.</span><span class="col-md-8">Ortodox Nagypéntek</span></div>
							<div class="row"><span class="col-md-4">április 8.</span><span class="col-md-8">Ortodox Húsvét</span></div>
							<div class="row"><span class="col-md-4">április 9.</span><span class="col-md-8">Ortodox Húsvét</span></div>
							<div class="row"><span class="col-md-4">május 1.</span><span class="col-md-8">a munka ünnepe</span></div>
							<div class="row"><span class="col-md-4">december 24-25-26</span><span class="col-md-8">karácsony</span></div>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="es">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">január 6.</span><span class="col-md-8">vízkereszt</span></div>
							<div class="row"><span class="col-md-4">március 19.</span><span class="col-md-8">Szent József napja</span></div>
							<div class="row"><span class="col-md-4">május 1.</span><span class="col-md-8">a munka ünnepe</span></div>
							<div class="row"><span class="col-md-4">június 24.</span><span class="col-md-8">a király névnapja, védőszentjének ünnepe</span></div>
							<div class="row"><span class="col-md-4">június 29.</span><span class="col-md-8">Szent Péter és Szent Pál apostol ünnepe</span></div>
							<div class="row"><span class="col-md-4">július 25.</span><span class="col-md-8">Szent Jakab apostol ünnepe</span></div>
							<div class="row"><span class="col-md-4">augusztus 15.</span><span class="col-md-8">Szűz Mária mennybevétele</span></div>
							<div class="row"><span class="col-md-4">október 12.</span><span class="col-md-8">Amerika felfedezésének ünnepe</span></div>
							<div class="row"><span class="col-md-4">november 1.</span><span class="col-md-8">mindenszentek</span></div>
							<div class="row"><span class="col-md-4">december 6.</span><span class="col-md-8">az Alkotmány napja</span></div>
							<div class="row"><span class="col-md-4">december 25.</span><span class="col-md-8">karácsony</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">nagypéntek</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">pünkösdhétfő</span></div>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="ch">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1-2.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">május 31.</span><span class="col-md-8">az emlékezés napja</span></div>
							<div class="row"><span class="col-md-4">július 4.</span><span class="col-md-8">a függetlenség napja</span></div>
							<div class="row"><span class="col-md-4">augusztus 1.</span><span class="col-md-8">Svájci nemzeti nap</span></div>
							<div class="row"><span class="col-md-4">szeptember 6.</span><span class="col-md-8">a munka ünnepe</span></div>
							<div class="row"><span class="col-md-4">November 1.</span><span class="col-md-8">a munka ünnepe</span></div>
							<div class="row"><span class="col-md-4">december 25-26.</span><span class="col-md-8">karácsony</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">nagypéntek</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">Húsvét</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">Pünkösd</span></div>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="se">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">január 6.</span><span class="col-md-8">vízkereszt</span></div>
							<div class="row"><span class="col-md-4">május 1.</span><span class="col-md-8">a munka ünnepe</span></div>
							<div class="row"><span class="col-md-4">december 25.</span><span class="col-md-8">karácsony</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">nagypéntek</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">húsvét</span></div>
							<div class="row"><span class="col-md-4">május második felében </span><span class="col-md-8">urunk mennybemenetele</span></div>
							<div class="row"><span class="col-md-4">június vége </span><span class="col-md-8">Szent Iván éjjele</span></div>
							<div class="row"><span class="col-md-4">november eleje </span><span class="col-md-8">mindenszentek</span></div>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="rs">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1-2.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">január 7.</span><span class="col-md-8">ortodox karácsony</span></div>
							<div class="row"><span class="col-md-4">február 15.</span><span class="col-md-8">alkotmány ünnepe</span></div>
							<div class="row"><span class="col-md-4">május 1-2.</span><span class="col-md-8">a munka ünnepe</span></div>
							<div class="row"><span class="col-md-4">június 28.</span><span class="col-md-8">Vidovdan - Szent Vid napja</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Megjegyzés:</h4>
							<p>Szerbiában a Julianus-naptárt használják, ezért az ünnepek két héttel elcsúsznak a Gergely-naptárhoz képest, így a Szenteste január 6-ára, a Szilveszter pedig január 13-ára esik. A Karácsonyt (szerbül: Božić) eredetileg kizárólag január 6-án tartották, de manapság már december 25-én, azaz a Gergely-naptár szerint is meg szokták ünnepelni.</p>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="sk">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">szlovák nemzeti ünnep</span></div>
							<div class="row"><span class="col-md-4">január 6.</span><span class="col-md-8">vízkereszt</span></div>
							<div class="row"><span class="col-md-4">május 1.</span><span class="col-md-8">a munka ünnepe</span></div>
							<div class="row"><span class="col-md-4">május 8.</span><span class="col-md-8">a II. világháború vége</span></div>
							<div class="row"><span class="col-md-4">július 5.</span><span class="col-md-8">Szt. Cyril és Szt. Methodius ünnepe</span></div>
							<div class="row"><span class="col-md-4">augusztus 29.</span><span class="col-md-8">a szlovák nemzet felemelkedésének napja</span></div>
							<div class="row"><span class="col-md-4">szeptember 1.</span><span class="col-md-8">a szlovák alkotmány ünnepe</span></div>
							<div class="row"><span class="col-md-4">szeptember 15.</span><span class="col-md-8">hétfájdalmú Szűz Mária ünnepe</span></div>
							<div class="row"><span class="col-md-4">november 1.</span><span class="col-md-8">mindenszentek</span></div>
							<div class="row"><span class="col-md-4">november 17.</span><span class="col-md-8">a demokráciáért folytatott küzdelem napja</span></div>
							<div class="row"><span class="col-md-4">december 24.-25.-26.</span><span class="col-md-8">karácsony</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">nagypéntek</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">húsvét</span></div>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="si">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">január 2.</span><span class="col-md-8">újév másnapja</span></div>
							<div class="row"><span class="col-md-4">február 8.</span><span class="col-md-8">a szlovén kultúra napja</span></div>
							<div class="row"><span class="col-md-4">április 27.</span><span class="col-md-8">az ellenállás napja</span></div>
							<div class="row"><span class="col-md-4">május 1.</span><span class="col-md-8">munka ünnepe</span></div>
							<div class="row"><span class="col-md-4">június 25.</span><span class="col-md-8">nemzeti ünnep</span></div>
							<div class="row"><span class="col-md-4">augusztus 15.</span><span class="col-md-8">nagyboldogasszony ünnepe</span></div>
							<div class="row"><span class="col-md-4">október 31.</span><span class="col-md-8">a reformáció napja</span></div>
							<div class="row"><span class="col-md-4">november 1.</span><span class="col-md-8">halottak napja</span></div>
							<div class="row"><span class="col-md-4">december 25.</span><span class="col-md-8">karácsony</span></div>
							<div class="row"><span class="col-md-4">december 26.</span><span class="col-md-8">a függetlenség napja</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">húsvét</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">pünkösd</span></div>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="tr">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">április 23.</span><span class="col-md-8">nemzeti függetlenség napja</span></div>
							<div class="row"><span class="col-md-4">május 19.</span><span class="col-md-8">ifjúság napja</span></div>
							<div class="row"><span class="col-md-4">augusztus 30.</span><span class="col-md-8">felszabadulás napja</span></div>
							<div class="row"><span class="col-md-4">október 29.</span><span class="col-md-8">köztársaság napja</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Megjegyzés:</h4>
							<p>A vallási ünnepek az iszlám naptárt követik.</p>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="nz">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">január 2.</span><span class="col-md-8">újév másnapja</span></div>
							<div class="row"><span class="col-md-4">január 29.</span><span class="col-md-8">Auckland évforduló</span></div>
							<div class="row"><span class="col-md-4">február 6.</span><span class="col-md-8">Waitangi Szerződés Napja</span></div>
							<div class="row"><span class="col-md-4">április 25.</span><span class="col-md-8">ANZAC Nap - A háborúban meghalt új-zélandiakra</span></div>
							<div class="row"><span class="col-md-4">december 25-26.</span><span class="col-md-8">karácsony</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">nagypéntek</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">húsvét</span></div>
							<div class="row"><span class="col-md-4">június első hétfője</span><span class="col-md-8">a királynő születésnapja</span></div>
							<div class="row"><span class="col-md-4">október negyedik hétfője</span><span class="col-md-8">munka ünnepe</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Megjegyzés:</h4>
							<p>Az új-zélandi kolónia 1841-es alapításának az ünnepe, és már 1842 óta munkaszüneti nap. 1963-ig az elsődleges nemzeti ünnep volt. 1840 február 6-án kötött szerződést a brit korona a maori törzsfőkkel. 1963-ban lett Új-Zéland elsődleges nemzeti ünnepe, míg a január 29. ünnepet átnevezték Auckland Évfordulónak. ANZAC Nap - A háborúban meghalt új-zélandiakra emlékezés napja. ANZAC - Australian and New Zealand Army Corps azaz Ausztrál és Új Zélandi Fegyveres Erők.</p>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="ua">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">január 7.</span><span class="col-md-8">ortodox karácsony</span></div>
							<div class="row"><span class="col-md-4">május 1.</span><span class="col-md-8">a munka ünnepe</span></div>
							<div class="row"><span class="col-md-4">május 1.</span><span class="col-md-8">a munka ünnepe</span></div>
							<div class="row"><span class="col-md-4">május 9.</span><span class="col-md-8">a győzelem napja</span></div>
							<div class="row"><span class="col-md-4">június 28.</span><span class="col-md-8">az alkotmány ünnepe</span></div>
							<div class="row"><span class="col-md-4">augusztus 24.</span><span class="col-md-8">a függetlenség napja</span></div>
							<div class="row"><span class="col-md-4">december 25.</span><span class="col-md-8">katolikus karácsony</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4 col-md-offset-4">Ortodox húsvét</span></div>
							<hr class="narrow">
						</div>
						<div class="tab-pane" id="us">
							<h4>Állandó:</h4>
							<div class="row"><span class="col-md-4">január 1.</span><span class="col-md-8">újév</span></div>
							<div class="row"><span class="col-md-4">júlis 4.</span><span class="col-md-8">úfüggetlenség napja</span></div>
							<div class="row"><span class="col-md-4">október 12.</span><span class="col-md-8">Kolumbusz napja</span></div>
							<div class="row"><span class="col-md-4">november 11.</span><span class="col-md-8">veteránok napja</span></div>
							<div class="row"><span class="col-md-4">december 25-26.</span><span class="col-md-8">karácsony</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							<h4>Változó dátumok:</h4>
							<div class="row"><span class="col-md-4">január harmadik hétfője</span><span class="col-md-8">Martin Luther King születésnapja</span></div>
							<div class="row"><span class="col-md-4">február harmadik hétfője</span><span class="col-md-8">George Washington születésnapja</span></div>
							<div class="row"><span class="col-md-4">május utolsó hétfője</span><span class="col-md-8">háborús hősök emléknapja</span></div>
							<div class="row"><span class="col-md-4 col-md-offset-4">húsvét</span></div>
							<div class="row"><span class="col-md-4">szeptember első hétfője</span><span class="col-md-8">munka ünnepe</span></div>
							<div class="row"><span class="col-md-4">november negyedik csütörtökje</span><span class="col-md-8">hálaadás napja</span></div>
							<h4>Megjegyzés:</h4>
							<p>Ha egy szövetségi ünnep hétvégére esik, a munkaszüneti napot átteszik hétfőre, vagy péntekre.</p>
							<hr class="narrow">
							<hr class="narrow md-margin">

						</div>
						<div class="tab-pane" id="eu">
							<h4>Állandó:</h4>
								<div class="row"><span class="col-md-4">február 1.</span><span class="col-md-8">A Civilek Napja Magyarszágon</span></div>
								<div class="row"><span class="col-md-4">február 24.</span><span class="col-md-8">Idegenvezet—i Világnap</span></div>
								<div class="row"><span class="col-md-4">április 2.</span><span class="col-md-8">Szakszervezeti Akciónap</span></div>
								<div class="row"><span class="col-md-4">április 14.</span><span class="col-md-8">Könyvtárosok Világnapja</span></div>
								<div class="row"><span class="col-md-4">április 22.</span><span class="col-md-8">Ápolók Napja Magyarországon</span></div>
								<div class="row"><span class="col-md-4">április 24.</span><span class="col-md-8">A Magyar Rendőrség Napja</span></div>
								<div class="row"><span class="col-md-4">április 28.</span><span class="col-md-8">A Munkahelyi Balesetek Gyásznapja</span></div>
								<div class="row"><span class="col-md-4">május 1.</span><span class="col-md-8">Munkavállalók Szolidaritás Napja</span></div>
								<div class="row"><span class="col-md-4">május 4.</span><span class="col-md-8">A Magyar Tűzoltók Napja</span></div>
								<div class="row"><span class="col-md-4">május 5.</span><span class="col-md-8">Az Esélyegyenlőség Napja</span></div>
								<div class="row"><span class="col-md-4">május 10.</span><span class="col-md-8">A Mentők Napja Magyarországon</span></div>
								<div class="row"><span class="col-md-4">május 12.</span><span class="col-md-8">Ápolónők Nemzetközi Napja</span></div>
								<div class="row"><span class="col-md-4">május 13.</span><span class="col-md-8">Az Egészség és Biztonság Nemzetközi Napja a Bányaiparban</span></div>
								<div class="row"><span class="col-md-4">húsvét negyedik vasárnapja</span><span class="col-md-8">Hivatások Világnapja</span></div>
								<div class="row"><span class="col-md-4">június első vasárnapja</span><span class="col-md-8">Pedagógus nap</span></div>
								<div class="row"><span class="col-md-4">június 08.</span><span class="col-md-8">Az Építők Napja Magyarországon</span></div>
								<div class="row"><span class="col-md-4">június 12.</span><span class="col-md-8">A Gyermekmunka elleni Világnap</span></div>
								<div class="row"><span class="col-md-4">június 26.</span><span class="col-md-8">Közalkalmazottak Napja</span></div>
								<div class="row"><span class="col-md-4">június 27.</span><span class="col-md-8">A Magyar Határőrség Napja</span></div>
								<div class="row"><span class="col-md-4">július második vasárnapja</span><span class="col-md-8">Vasutas Nap</span></div>
								<div class="row"><span class="col-md-4">július 1.</span><span class="col-md-8">A Köztisztviselők Napja Magyarországon</span></div>
								<div class="row"><span class="col-md-4">július 27.</span><span class="col-md-8">Rendszergazdák Világnapja</span></div>
								<div class="row"><span class="col-md-4">szeptember 18.</span><span class="col-md-8">Rokkantak Napja</span></div>
								<div class="row"><span class="col-md-4">október 5.</span><span class="col-md-8">A Pedagógus jogok Világnapja</span></div>
								<div class="row"><span class="col-md-4">október 6.</span><span class="col-md-8">Építészek Világnapja</span></div>
								<div class="row"><span class="col-md-4">október 9.</span><span class="col-md-8">Postai Világnap</span></div>
								<div class="row"><span class="col-md-4">december 5.</span><span class="col-md-8">Az Önkéntesség Nemzetközi Napja</span></div>
							<hr class="narrow">
							<hr class="narrow md-margin">
							
						</div>
											<div class="fb-like" data-href="http://www.hrportal.hu<?=$URI;?>" data-layout="standard" data-action="recommend" data-show-faces="true" data-share="true"></div>
					<div style="clear:both;height:1px;"></div> 
					<script src="//platform.linkedin.com/in.js" type="text/javascript">
					lang: en_US
					</script>
					<script type="IN/Share" data-url="http://www.hrportal.hu<?=$URI;?>" data-counter="right"></script>

					<div id="plusone-div" > <g:plusone size="normal"></g:plusone></div>
					<script type="text/javascript">
					gapi.plusone.go("plusone-div");
					</script>
					</div>
				</div>  
			</div> 	
		</div>
	</div>
</article>
</div>
