 <!-- ***** CENTER COLUMN ***** -->
 <div class="col-md-8 column">
   <ol class="breadcrumb">
		<li><a href="/">Főoldal</a>
		</li>
		<li><a href="/vegkielegites_kalkulator.html"> Végkielégítés kalkulátor 2017</a>
		</li>
	</ol>
	<article>
		<div class="row clearfix md-margin">
			<div class="col-md-12 column">
			   <h1  class="md-margin" > Végkielégítés kalkulátor 2017</h1>                                       
			   <form class="form-horizontal md-margin" onsubmit="return false;">
				  <div class="form-group">
					<label for="edt_brutto" class="col-sm-7 control-label">Távolléti bruttó díj (Ft/hó)*</label>
					<div class="col-sm-2">
					  <input type="text" class="form-control" id="edt_brutto" name="edt_brutto" placeholder="200000">
					</div>
				  </div>
				  <div class="form-group">
					<label for="dats" class="col-sm-7 control-label">Munkaviszony időtartama :*</label>
					<div class="col-sm-2">
					  <select class="form-control"  name="dats" id="dats">
						<option value="1" selected>3-5 év</option>
						<option value="2">5-10 év</option>
						<option value="3">10-15 év</option>
						<option value="4">15-20 év</option>
						<option value="5">20-25 év</option>
						<option value="6">25 év felett</option>
						</select>
					</div>
				  </div>
				  <div class="form-group">
					<label for="edt_elt" class="col-sm-7 control-label">Eltartottak (gyermekek) száma *</label>
					<div class="col-sm-2">
					  <select class="form-control"  name="edt_elt" id="edt_elt">
						  <option value="0" selected>nincsenek</option>
						  <option value="1">1</option>
						  <option value="2">2</option>
						  <option value="3">3</option>
						  <option value="4">4</option>
						  <option value="5">5</option>
						</select>
					</div>
				  </div>

				  <div class="form-group">
					<label for="edt_gyerekek" class="col-sm-7 control-label">Ebből kedvezményezett eltartottak száma **</label>
					<div class="col-sm-2">
					  <select class="form-control" name="edt_gyerekek" id="edt_gyerekek">
						  <option value="0" selected>nincsenek</option>
						  <option value="1">1</option>
						  <option value="2">2</option>
						  <option value="3">3</option>
						  <option value="4">4</option>
						  <option value="5">5</option>
						</select>
					</div>
				  </div>

				  <div class="form-group">
					<label for="text1" class="col-sm-7 control-label">Gyermekét/gyermekeit egyedül neveli</label>
					<div class="col-sm-2">
					  <select class="form-control" name="edt_egyedulnevel" id="edt_egyedulnevel">
						  <option value="1">igen</option>
						  <option value="0" selected>nem</option>
					   </select>
					</div>
				  </div>

				                                           
			   <fieldset>
				   
				   <span class="secondary">Alábbiak nem befolyásolják a nettó bért:</span>
				   <div class="form-group">
					<label for="text1" class="col-sm-7 control-label">Munkavállaló ****</span></label>
					<div class="col-sm-5">
					  <select class="form-control" name="edt_kedvezmeny" id="edt_kedvezmeny">
							<option value='0' selected>kedvezményre nem jogosult.</option>
							<option value='1' >25 év alatti pályakezdő.</option>
							<option value='2' >25 év alatti nem pályakezdő.</option>
							<option value='3' >25-54 éves szakképzetlen.</option>
							<option value='4' >tartósan álláskereső.</option>	
							<option value='5' >55 év feletti.</option>
							<option value='6' >gyed, gyes, gyetről tér vissza.</option>		
							<option value='7' >új mv. szab. vállalk. zónában</option>			
							<option value='8' >kutatóként van foglalkoztatva</option>				
							<option value='9' >bérkompenzációra jogosult</option>			
						</select>
					</div>
				  </div>
				  
				  <div class="form-group">
					<label for="text1" class="col-sm-7 control-label">Teljes / részmunkaidős foglalkoztatás</span></label>
					<div class="col-sm-5">
					  <select class="form-control" name="edt_munkaido" id="edt_munkaido">
							<option value='40' selected>teljes munkaidős</option>
							<option value='8' >heti 8 órás</option>
							<option value='10' >heti 10 órás</option>
							<option value='16' >heti 16 órás</option>
							<option value='20' >heti 20 órás</option>	
							<option value='24' >heti 24 órás</option>
							<option value='32' >heti 32 órás</option>				
						</select>
					</div>
				  </div>
				 </fieldset>
				  <div class="form-group">
					<div class="col-sm-offset-7 col-sm-10">
					  <button class="btn btn btn-main" onclick="javascript:vegkielegites_2016();">Számol</button>
					</div>
				  </div>
				</form>
				
				<div id="vegkielegites_cont"></div>
					<div class="fb-like" data-href="http://www.hrportal.hu<?=$URI;?>" data-layout="standard" data-action="recommend" data-show-faces="true" data-share="true"></div>
					<div style="clear:both;height:1px;"></div> 
					<script src="//platform.linkedin.com/in.js" type="text/javascript">
					lang: en_US
					</script>
					<script type="IN/Share" data-url="http://www.hrportal.hu<?=$URI;?>" data-counter="right"></script>

					<div id="plusone-div" > <g:plusone size="normal"></g:plusone></div>
					<script type="text/javascript">
					gapi.plusone.go("plusone-div");
					</script>
			</div>
		</div>
	</article>

</div>
<!-- ***** CENTER COLUMN ***** -->
	
<script>
	
	$('.nav-tabs a[href="#panel-4"]').tab('show');
	
</script>

