function checkEmail(emailstr){emailRegExp = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.([a-z]){2,4})$/;if(emailRegExp.test(emailstr)){return true;}else{return false;}}
var aTextField;

var panel, menu;
function showmenu(panel,menu){
	$.ajax(
		{
			type:'POST',
			url: '/inc/ajax_showmenu.php',
			data:'panel='+panel+'&menu='+menu,
			success: function(data){
				$("#"+panel).html(data);
				if(menu=="mobile"){
					  $("#menu").mmenu();
					  $("#menu").trigger( "open" );
				}
				
				
			}
		});
}

function CreateBookmarkLink() {
	title = "Humánerőforrás / HR Portál | HR Portal - minden a munka világából";
	url = "http://www.hrportal.hu/";
	if (window.sidebar) {
		window.sidebar.addPanel(title, url,"");
	} else if( window.external ) {
		window.external.AddFavorite( url, title);
	}
	else if(window.opera && window.print) {
	return true; }
}

function IsNumeric(sText){
	var ValidChars = "0123456789.";
	var IsNumber=true;
	var Char;
	for (i = 0; i < sText.length && IsNumber == true; i++){ 
		Char = sText.charAt(i); 
		if (ValidChars.indexOf(Char) == -1){IsNumber = false;}
	}
	return IsNumber;
}
function IsEmpty(aTextField) {
   if ((aTextField.value.length==0) ||
   (aTextField.value==null)) {
      return true;
   }
   else { return false; }
}	


function berkalkulator_2016(){
	if($("#edt_brutto").val()==""){ alert('Kérjük, adja meg a bruttó havi munkabért!') }
	else{if (!IsNumeric($("#edt_brutto").val())){ alert('Kérjük, csak számjegyeket adjon meg!') }
		else{
			if ($("#edt_elt").val() < $("#edt_gyerekek").val() ){ alert('Az eltartottak száma nem lehet alacsonyabb a kedvezményezett eltartott gyermekek számánál!') } 
			else{
				$.ajax({type:'POST',url: '/inc/ajax_berkalkulator_2016.php',data:'edt_brutto='+$("#edt_brutto").val()+'&edt_elt='+$("#edt_elt").val()+'&edt_gyerekek='+$("#edt_gyerekek").val()+'&edt_egyedulnevel='+$("#edt_egyedulnevel").val()+'&edt_frisshazas='+$("#edt_frisshazas").val()+'&edt_kedvezmeny='+$("#edt_kedvezmeny").val()+'&edt_munkaido='+$("#edt_munkaido").val(),success: function(data){$("#berkalkulator_2016_cont").html(data);}});
			} 
		} 
	} 
}
function berkalkulator_2015(){
	if($("#edt_brutto").val()==""){ alert('Kérjük, adja meg a bruttó havi munkabért!') }
	else{if (!IsNumeric($("#edt_brutto").val())){ alert('Kérjük, csak számjegyeket adjon meg!') }
		else{
			if ($("#edt_elt").val() < $("#edt_gyerekek").val() ){ alert('Az eltartottak száma nem lehet alacsonyabb a kedvezményezett eltartott gyermekek számánál!') } 
			else{
				$.ajax({type:'POST',url: '/inc/ajax_berkalkulator_2015.php',data:'edt_brutto='+$("#edt_brutto").val()+'&edt_elt='+$("#edt_elt").val()+'&edt_gyerekek='+$("#edt_gyerekek").val()+'&edt_egyedulnevel='+$("#edt_egyedulnevel").val()+'&edt_frisshazas='+$("#edt_frisshazas").val()+'&edt_kedvezmeny='+$("#edt_kedvezmeny").val()+'&edt_munkaido='+$("#edt_munkaido").val(),success: function(data){$("#berkalkulator_2015_cont").html(data);}});
			} 
		} 
	} 
}
function berkalkulator_2014(){
	if($("#edt_brutto").val()==""){ alert('Kérjük, adja meg a bruttó havi munkabért!') }
	else{if (!IsNumeric($("#edt_brutto").val())){ alert('Kérjük, csak számjegyeket adjon meg!') }
		else{
			if ($("#edt_elt").val() < $("#edt_gyerekek").val() ){ alert('Az eltartottak száma nem lehet alacsonyabb a kedvezményezett eltartott gyermekek számánál!') } 
			else{
				$.ajax({type:'POST',url: '/inc/ajax_berkalkulator_2014.php',data:'edt_brutto='+$("#edt_brutto").val()+'&edt_elt='+$("#edt_elt").val()+'&edt_gyerekek='+$("#edt_gyerekek").val()+'&edt_egyedulnevel='+$("#edt_egyedulnevel").val()+'&edt_kedvezmeny='+$("#edt_kedvezmeny").val()+'&edt_munkaido='+$("#edt_munkaido").val(),success: function(data){$("#berkalkulator_2014_cont").html(data);}});
			} 
		} 
	} 
}
function berkalkulator_2013(){
	if($("#edt_brutto").val()==""){ alert('Kérjük, adja meg a bruttó havi munkabért!') }
	else{if (!IsNumeric($("#edt_brutto").val())){ alert('Kérjük, csak számjegyeket adjon meg!') }
		else{
			if ($("#edt_elt").val() < $("#edt_gyerekek").val() ){ alert('Az eltartottak száma nem lehet alacsonyabb a kedvezményezett eltartott gyermekek számánál!') } 
			else{
				$.ajax({type:'POST',url: '/inc/ajax_berkalkulator_2013.php',data:'edt_brutto='+$("#edt_brutto").val()+'&edt_elt='+$("#edt_elt").val()+'&edt_gyerekek='+$("#edt_gyerekek").val()+'&edt_egyedulnevel='+$("#edt_egyedulnevel").val()+'&edt_kedvezmeny='+$("#edt_kedvezmeny").val()+'&edt_munkaido='+$("#edt_munkaido").val(),success: function(data){$("#berkalkulator_2013_cont").html(data);}});
			} 
		} 
	} 
}
function diakkalkulator(){
	if($("#ora").val()==""){ alert('Kérjük, adja meg a ledolgozott időt!') }
	else{
		$.ajax({type:'POST',url: '/inc/ajax_diakkalkulator.php',data:'ora='+$("#ora").val()+'&oraber='+$("#oraber").val(),success: function(data){$("#diakmunka_cont").html(data);}});
	}  
}
function tappenzkulator(){
	
		$.ajax({type:'POST',url: '/inc/ajax_tappenzkulator.php',data:'kereset='+$("#kereset").val()+'&naps='+$("#naps").val()+'&mv='+$("input[name='mv']:checked").val(),success: function(data){$("#tappenzkulator_cont").html(data);}});
 
}
function isszam(szam){var ervenyes="0123456789";var text=szam;for (var i=0;i<text.length;i++){if (ervenyes.indexOf(text.charAt(i)) == -1){return false;}}return true;}  		
function rehabkulator(){
if($("#letszam").val()!='' && isszam($("#letszam").val())  && $("#rehabletszam").val()!='' && isszam($("#rehabletszam").val()) && $("#hatraho").val()!='' && isszam($("#hatraho").val()) && $("#rehabber").val()!='' && isszam($("#rehabber").val()) ){
	
	if($("#letszam").val()>19){
		if($("#hatraho").val()>0 && $("#hatraho").val()<13){
			if($("#rehabber").val()>38999){
		$.ajax({
			type:'POST',
			url: '/inc/ajax_rehabkulator.php',
			data:'letszam='+$("#letszam").val()+'&rehabletszam='+$("#rehabletszam").val()+'&hatraho='+$("#hatraho").val()+'&rehabber='+$("#rehabber").val(),
			success: function(data){
				
				$("#rehaberedmeny").html(data);
				$("#errordiv").html("");
				}
			});
		}
		else{
			$("#errordiv").html("A szem&eacute;lyi alapb&eacute;r nem lehet kevesebb mint a minim&aacute;lb&eacute;r fele!");
			$("#rehaberedmeny").html("");
			
		}
		}
		else{
			$("#errordiv").html("A h&oacute;napok sz&aacute;ma 1-12 k&ouml;z&ouml;tt lehet!");
			$("#rehaberedmeny").html("");
		}
		}
		else{
			$("#errordiv").html("20 f&#x0151; alkalmazotti l&eacute;sz&aacute;m alatt nem kell rehabilit&aacute;ci&oacute;s j&aacute;rul&eacute;kot fizetni!");
			$("#rehaberedmeny").html("");
		}
		}
	else{
		$("#errordiv").html("K&eacute;rj&uuml; t&ouml;lts&ouml;n ki minden mez&#x0151;t!");
		$("#rehaberedmeny").html("");
	}	
}
function tamogataskulator(){

if($("#havi_br").val()!=""){
	$.ajax({
		type:'POST',
		url: '/inc/ajax_tamogataskulator.php',
		data:'startdate='+$("#startdate").val()+'&enddate='+$("#enddate").val()+'&havi_br='+$("#havi_br").val()+'&megszunes='+$("#megszunes").val(),
		success: function(data){
			
			$("#tamogataseredmeny").html(data);
			}
		});
	}
	else{
		alert("A munkab\u00E9r k\u00F6telez\u0151 mez\u0151!");	
	}	
}
function gyeskalkulator(){
//round(($_REQUEST['havi_br']*12)/365);//
var napi=Math.round($("#havi_br").val()*12/365);
	$.ajax({
		type:'POST',
		url: '/inc/ajax_gyeskalkulator.php',
		data:'szuletes='+$("#almak").val()+'&gyes_jog='+$("#gyes_jog").val()+'&havi_br='+$("#havi_br").val(),
		success: function(data){
			
			$("#napiatlag").html(napi+" ft/nap");
			$("#gyeseredmeny").html(data);
			}
		});
}		
function vegkielegites_2016(){
	if($("#edt_brutto").val()==""){ alert('Kérjük, adja meg a bruttó havi munkabért!') }
	else{if (!IsNumeric($("#edt_brutto").val())){ alert('Kérjük, csak számjegyeket adjon meg!') }
		else{
			if ($("#edt_elt").val() < $("#edt_gyerekek").val() ){ alert('Az eltartottak száma nem lehet alacsonyabb a kedvezményezett eltartott gyermekek számánál!') } 
			else{
				$.ajax({type:'POST',url: '/inc/ajax_vegkielegites_2016.php',data:'edt_brutto='+$("#edt_brutto").val()+'&edt_elt='+$("#edt_elt").val()+'&edt_gyerekek='+$("#edt_gyerekek").val()+'&edt_egyedulnevel='+$("#edt_egyedulnevel").val()+'&edt_kedvezmeny='+$("#edt_kedvezmeny").val()+'&edt_munkaido='+$("#edt_munkaido").val()+'&dats='+$("#dats").val(),success: function(data){$("#vegkielegites_cont").html(data);}});
			} 
		} 
	} 
}
function szabadsagkalkulator(){
if(($("#edt_kezdoho").val()!=0) && ($("#edt_kezdonap").val()==0)){
	alert("Kérem ebben az esetben adja meg a munkakezdés napját!");
}
else{
	$.ajax({
		type:'POST',
		url: '/inc/ajax_szabadsagkalkulator.php',
		data:	'edt_szulev='+$("#edt_szulev").val()+
				'&edt_kezdoho='+$("#edt_kezdoho").val()+
				'&edt_kezdonap='+$("#edt_kezdonap").val()+
				'&edt_tavalyrol='+$("#edt_tavalyrol").val()+
				'&edt_gyerekek='+$("#edt_gyerekek").val()+
				'&edt_blind='+$("#edt_blind").val()+
				'&edt_ion='+$("#edt_ion").val()+
				'&edt_fogy='+$("#edt_fogy").val(),
		success: function(data){$("#szabadsageredmeny").html(data);}});
	}	
}
function ekhokalkulator(){
	if($("#edt_brutto").val()==""){ alert('Kérjük, adja meg a bruttó havi munkabért!') }
	else{if (!IsNumeric($("#edt_brutto").val())){ alert('Kérjük, csak számjegyeket adjon meg!') }
		else{
			if ($("#edt_elt").val() < $("#edt_gyerekek").val() ){ alert('Az eltartottak száma nem lehet alacsonyabb a kedvezményezett eltartott gyermekek számánál!') } 
			else{
				$.ajax({type:'POST',url: '/inc/ajax_ekhokalkulator.php',data:'edt_brutto='+$("#edt_brutto").val()+'&edt_elt='+$("#edt_elt").val()+'&edt_gyerekek='+$("#edt_gyerekek").val()+'&edt_egyedulnevel='+$("#edt_egyedulnevel").val()+'&edt_frisshazas='+$("#edt_frisshazas").val(),success: function(data){$("#ekhokalkulator_cont").html(data);}});
			} 
		} 
	} 
}
function getbertypepref(){
	if($("#bertype").val()=="mhetkezes"){
		$("#penznem").html('Ft/h\u00F3');	
	}
	else if($("#bertype").val()=="erzsbetutalvany"){
		$("#penznem").html('Ft/h\u00F3');	
	}
	else if($("#bertype").val()=="onyp"){
		$("#penznem").html('Ft/h\u00F3');	
	}
	else if($("#bertype").val()=="epp"){
		$("#penznem").html('Ft/h\u00F3');	
	}
	else if($("#bertype").val()=="szeve"){
		$("#penznem").html('Ft/\u00E9v');	
	}
	else if($("#bertype").val()=="szeszall"){
		$("#penznem").html('Ft/\u00E9v');	
	}
	else if($("#bertype").val()=="szeszab"){
		$("#penznem").html('Ft/\u00E9v');	
	}
	else if($("#bertype").val()=="penz"){
		$("#penznem").html('Ft/\u00E9v');	
	}
	else if($("#bertype").val()=="lakas"){
		$("#penznem").html('Ft/\u00E9v');	
	}
	else if($("#bertype").val()=="biztositas"){
		$("#penznem").html('Ft/h\u00F3');	
	}
	else if($("#bertype").val()=="ajandek"){
		$("#penznem").html('Ft/h\u00F3');	
	}
	else if($("#bertype").val()=="sport"){
		$("#penznem").html('Ft/\u00E9v');	
	}
}
var melyik;
function getberenkivuli2017nulla(melyik){
	$.ajax({
		type:'POST',
		url: '/inc/ajax_getberenkivuli2017nulla.php',
		data:'melyik='+melyik,
		success: function(data){
			berenkivuli2017();
			$("#love").val('');
		}
	});
}
function getberenkivuli2016nulla(melyik){
	$.ajax({
		type:'POST',
		url: '/inc/ajax_getberenkivuli2016nulla.php',
		data:'melyik='+melyik,
		success: function(data){
			berenkivuli2016();
			$("#love").val('');
		}
	});
}
function getberenkivuli2015nulla(melyik){
	$.ajax({
		type:'POST',
		url: '/inc/ajax_getberenkivuli2016nulla.php',
		data:'melyik='+melyik,
		success: function(data){
			berenkivuli2015();
			$("#love").val('');
		}
	});
}
function getberenkivuli2014nulla(melyik){
	$.ajax({
		type:'POST',
		url: '/inc/ajax_getberenkivuli2016nulla.php',
		data:'melyik='+melyik,
		success: function(data){
			berenkivuli2014();
			$("#love").val('');
		}
	});
}
function getberenkivuli2013nulla(melyik){
	$.ajax({
		type:'POST',
		url: '/inc/ajax_getberenkivuli2016nulla.php',
		data:'melyik='+melyik,
		success: function(data){
			berenkivuli2013();
			$("#love").val('');
		}
	});
}
function berenkivuli2016(){
	if($("#bertype").val()=="lakas" && $("#love").val()>1000000){
		alert("Csak \u00E9vi 1 milli\u00F3 forint sz\u00E1molhat\u00F3 el.");	
	}
	else{
		$.ajax({
			type:'POST',
			url: '/inc/ajax_berenkivuli2016.php',
			data:'love='+$("#love").val()+'&bertype='+$("#bertype").val(),
			success: function(data){
				$("#berenkivuli2016_cont").html(data);
				$("#love").val('');
			}
		});
	}
}
function berenkivuli2017(){
	if($("#bertype").val()=="lakas" && $("#love").val()>1000000){
		alert("Csak \u00E9vi 1 milli\u00F3 forint sz\u00E1molhat\u00F3 el.");	
	}
	else{
		if($("#bertype").val()=="kultura" && $("#love").val()>50000){
			alert("Csak \u00E9vi 50 ezer forint sz\u00E1molhat\u00F3 el.");	
		}
		else{
			$.ajax({
				type:'POST',
				url: '/inc/ajax_berenkivuli2017.php',
				data:'love='+$("#love").val()+'&bertype='+$("#bertype").val(),
				success: function(data){
					$("#berenkivuli2017_cont").html(data);
					$("#love").val('');
				}
			});
		}
	}
}
function berenkivuli2015(){
	if($("#bertype").val()=="lakas" && $("#love").val()>1000000){
		alert("Csak \u00E9vi 1 milli\u00F3 forint sz\u00E1molhat\u00F3 el.");	
	}
	else{
		$.ajax({
			type:'POST',
			url: '/inc/ajax_berenkivuli2015.php',
			data:'love='+$("#love").val()+'&bertype='+$("#bertype").val(),
			success: function(data){
				$("#berenkivuli2015_cont").html(data);
				$("#love").val('');
			}
		});
	}
}
function berenkivuli2014(){
	if($("#bertype").val()=="lakas" && $("#love").val()>1000000){
		alert("Csak \u00E9vi 1 milli\u00F3 forint sz\u00E1molhat\u00F3 el.");	
	}
	else{
		$.ajax({
			type:'POST',
			url: '/inc/ajax_berenkivuli2014.php',
			data:'love='+$("#love").val()+'&bertype='+$("#bertype").val(),
			success: function(data){
				$("#berenkivuli2014_cont").html(data);
				$("#love").val('');
			}
		});
	}
}
function berenkivuli2013(){
	if($("#bertype").val()=="lakas" && $("#love").val()>1000000){
		alert("Csak \u00E9vi 1 milli\u00F3 forint sz\u00E1molhat\u00F3 el.");	
	}
	else{
		$.ajax({
			type:'POST',
			url: '/inc/ajax_berenkivuli2013.php',
			data:'love='+$("#love").val()+'&bertype='+$("#bertype").val(),
			success: function(data){
				$("#berenkivuli2013_cont").html(data);
				$("#love").val('');
			}
		});
	}
}

function showfeor(){
	$.ajax({
			type:'POST',
			url: '/inc/ajax_showfeor.php',
			data:'text1='+$("#text1").val()+'&text2='+$("#text2").val(),
			success: function(data){
				$("#accordion").html(data);
			}
		});
}

function gettag(tag){
	$.ajax({
			type:'POST',
			url: '/inc/ajax_gettag.php',
			data:'text1='+tag,
			success: function(data){
				$("#"+tag).html(data);
			}
		});
}

function ad_counter(){
	$.ajax({type:'POST',url: '/inc/ajax_ad_counter.php',data:'stli=katedra',success: function(data){ document.location="http://katedra.hu/halozat-landing/nyelvtanulas-legjobb-befektetes"; }});
}

function ad_counter2(){
	$.ajax({type:'POST',url: '/inc/ajax_ad_counter.php',data:'stli=fizetesek_2',success: function(data){ document.location="http://www.fizetesek.hu/?utm_medium=cpc&utm_source=hrportal.hu&utm_campaign=hrportal"; }});
}
function ad_counter3(){
	$.ajax({type:'POST',url: '/inc/ajax_ad_counter.php',data:'stli=napikonf',success: function(data){ document.location="http://www.hrportal.hu/index.phtml?page=munkavedelem_maskent_2014"; }});
}

function sethrszotar(){
	document.location=$("#szocikk").val();
}
var specialist;
function gomail_specialist(specialist){
	$.ajax({
			type:'POST',
			url: '/inc/gomail_specialist.php',
			data:'specialist='+specialist+
			'&edt_tel='+$("#edt_tel").val()+
			'&edt_name='+$("#edt_name").val()+
			'&edt_employ='+$("#edt_employ").val()+
			'&edt_email='+$("#edt_email").val()+
			'&edt_email='+$("#edt_email").val()+
			'&edt_message='+$("#edt_message").val(),
			success: function(data){
				$("#messageform").html('<h2><center><b>Köszönjük.</b><br><br>Üzenetét megkaptuk.<br /><a href="/" >Tovább >></a></h2>');
			}
		});
}
function gomail_system(){
	$.ajax({
			type:'POST',
			url: '/inc/gomail_system.php',
			data:'specialist=1'+
			'&text1='+$("#text1").val()+
			'&text2='+$("#text2").val()+
			'&text3='+$("#text3").val()+
			'&text5='+$("#text5").val()+
			'&text6='+$("#text6").val(),
			success: function(data){
				$("#messageform").html('<h2><center><b>Köszönjük.</b><br><br>Üzenetét megkaptuk.<br /><a href="/" >Tovább >></a></h2>');
			}
		});
}
function hirlevreg(){
	if(checkEmail($("#text5").val())){
	$.ajax({
			type:'POST',
			url: '/inc/ajax_hirlevreg.php',
			data:'specialist=1'+
			'&text1='+$("#text1").val()+
			'&text2='+$("#text2").val()+
			'&text3='+$("#text3").val()+
			'&text5='+$("#text5").val()+
			'&text4='+$("#text4").val(),
			success: function(data){
				$("#messageform").html(data);
			}
		});
	}
	else{
		alert("Kérem adjon meg helyes e-mail címet!");	
	}	
}


var cookieName = 'ScarabHRPFlyer';
var scarabflyer_closed = false;

function closefl(){
	$('#scarab_flyer').stop().animate({ right: '-400px' }, 'slow');
				scarabflyer_closed = true;
				scarabflyer_hidden = true;
				createCookie(cookieName, '1', 1);
}

var cookieName2 = 'PremiumHRPFlyer';
var hrpremiumflyer_closed = false;

function closefl2(){
	$('#hrpremium_flyer').stop().animate({ left: '-400px' }, 'slow');
				hrpremiumflyer_closed = true;
				hrpremiumflyer_hidden = true;
				createCookie(cookieName2, '1', 1);
}

function presscont2(){
	$('#scbut2').addClass("scarabact");
	$('#scbut1').removeClass("scarabact");
	$('#scbut3').removeClass("scarabact");
	$('#cont1').hide();
	$('#cont2').show();
	$('#cont3').hide();
}
function presscont1(){
	$('#scbut1').addClass("scarabact");
	$('#scbut2').removeClass("scarabact");
	$('#scbut3').removeClass("scarabact");
	$('#cont1').show();
	$('#cont2').hide();
	$('#cont3').hide();
}
function presscont3(){
	$('#scbut3').addClass("scarabact");
	$('#scbut1').removeClass("scarabact");
	$('#scbut2').removeClass("scarabact");
	$('#cont1').hide();
	$('#cont2').hide();
	$('#cont3').show();
}
function presscont22(){
	$('#scbut22').addClass("scarabact");
	$('#scbut12').removeClass("scarabact");
	$('#scbut32').removeClass("scarabact");
	$('#cont12').hide();
	$('#cont22').show();
	$('#cont32').hide();
}
function presscont12(){
	$('#scbut12').addClass("scarabact");
	$('#scbut22').removeClass("scarabact");
	$('#scbut32').removeClass("scarabact");
	$('#cont12').show();
	$('#cont22').hide();
	$('#cont32').hide();
}
function presscont32(){
	$('#scbut32').addClass("scarabact");
	$('#scbut12').removeClass("scarabact");
	$('#scbut22').removeClass("scarabact");
	$('#cont12').hide();
	$('#cont22').hide();
	$('#cont32').show();
}

function setscarabtimer(){
	setTimeout(function(){ 
		presscont2();
		setTimeout(function(){
			presscont3();  
			setTimeout(function(){presscont1();  }, 3000);	
		}, 3000);	
	 }, 3000);	
}


var param;
function getscreenh(param){
	if((get_ScrollY() + $(window).height()) < ($(document).height() * param)){
		return false;
	}
	else{
		return true;
	}
	//var lastScreen =  ? false : true;
}

function get_ScrollY(){
	scrOfY = 0;
	if(typeof( window.pageYOffset)=='number'){
		scrOfY = window.pageYOffset;
	}else if(document.body && (document.body.scrollLeft || document.body.scrollTop)){
		scrOfY = document.body.scrollTop;
	}else if(document.documentElement && (document.documentElement.scrollLeft || document.documentElement.scrollTop)){
		scrOfY = document.documentElement.scrollTop;
	}
	return scrOfY;
}
function createCookie(name,value,days){
	if (days){
		var date = new Date();
		date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
		var expires = "; expires=" + date.toGMTString();
	} else {
		var expires = "";
	}
	document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name){
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++)	{
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name){
	createCookie(name,"",-1);
}

function parseCookie(cookievalues){
	var cookieValuesArray = new Array();
	var CookiesArray = cookievalues.split("&");
	for (var i = 0; i < CookiesArray.length; i++){
		var v = CookiesArray[i].split(":");
		cookieValuesArray[i] = v[1];
	}
	return cookieValuesArray;
}



var art_id;
function addclcounter(art_id){
		$.ajax({
		type:'POST',
		url: '/inc/ajax_addclcounter.php',
		data:'art_id='+art_id,
		success: function(data){}
		});
}
  function tl( whichLayer )
	{
	  var elem, vis;
	  if( document.getElementById ) // this is the way the standards work
	    elem = document.getElementById( whichLayer );
	  else if( document.all ) // this is the way old msie versions work
		elem = document.all[whichLayer];
	  else if( document.layers ) // this is the way nn4 works
	    elem = document.layers[whichLayer];
	  vis = elem.style;
	  // if the style.display value is blank we try to figure it out here
	  if(vis.display==''&&elem.offsetWidth!=undefined&&elem.offsetHeight!=undefined)
	    vis.display = (elem.offsetWidth!=0&&elem.offsetHeight!=0)?'block':'none';
	  vis.display = (vis.display==''||vis.display=='block')?'none':'block';
	}

var rovat;
function getnexoninlinebanner(rovat){
		$.ajax({
		type:'GET',
		url: '/inc/ajax_getnexoninlinebanner.php',
		data:'fileid=1&rovat='+rovat,
		success: function(data){
			
			if(data=="1"){
				document.getElementById('layerbanner').style.display="block";
				$("#cucc").addClass("ui-widget-overlay");
				te=setTimeout(function(){hidenexin()}, 30000);
			}
			else if(data=="2"){
				document.getElementById('layerbanner2').style.display="block";
				$("#cucc").addClass("ui-widget-overlay");
				te=setTimeout(function(){hidenexin()}, 60000);
			}
			else if(data=="3"){
				document.getElementById('layerbanner4').style.display="block";
				//$("#cucc").addClass("ui-widget-overlay");
				te=setTimeout(function(){hidenexin()}, 60000);
			}
			else if(data=="4"){
				document.getElementById('layerbanner5').style.display="block";
				//$("#cucc").addClass("ui-widget-overlay");
				te=setTimeout(function(){hidenexin()}, 60000);
			}
			else if(data=="6"){
				document.getElementById('layerbanner6').style.display="block";
				//$("#cucc").addClass("ui-widget-overlay");
				te=setTimeout(function(){hidenexin()}, 30000);
			}
			else{
				document.getElementById('layerbanner').style.display="none";
				//document.getElementById('layerbanner2').style.display="none";
	
				
				$('#cucc').removeClass('ui-widget-overlay');
			}
		}
	});
}

var vote_id;
function showvote(vote_id){
	$.ajax({
		type:'POST',
		url: '/inc/ajax_showvote.php',
		data:'vote_id='+vote_id,
		success: function(data){
			if(data==1){
			
				showform(vote_id);	
			}
			else{
				showeredmeny(vote_id);
			}
			
			
		}});
}
function dovote(vote_id){
	//$("#voteform input[type='radio']:checked").val();
	$.ajax({
		type:'POST',
		url: '/inc/ajax_dovote.php',
		data:'vote_id='+vote_id+'&opt='+$("#voteform input[type='radio']:checked").val(),
		success: function(data){
			showvote(vote_id);
		}});
}
function showform(vote_id){
	
	$.ajax({
		type:'POST',
		url:'/inc/ajax_showform.php',
		data:'vote_id='+vote_id,
		success: function(data){
			$('#hrvote').html(data);
			
			
		}});
}

function getlablec(){
	$.ajax({
		type:'POST',
		url: '/inc/ajax_getlablec.php',
		data:'fileid=1',
		success: function(data){
			$("#lableca").html(data);
			      googletag.cmd.push(function() { googletag.display('div-gpt-ad-1461936145403-14'); });
			//$("#lablec").show();
		}
	});
}
function closefixedbox(){
	$.ajax({
		type:'POST',
		url: '/inc/ajax_closefixedbox.php',
		data:'fileid=1',
		success: function(data){
			//$("#lableca").html(data);
			$("#fixdiv").hide("slow");
		}
	});
}
function closegooglefixedbox(){
	$.ajax({
		type:'POST',
		url: '/inc/ajax_closegooglefixedbox.php',
		data:'fileid=1',
		success: function(data){
			//$("#lableca").html(data);
			$("#fixdivgoogle").hide("slow");
		}
	});
}

function showeredmeny(vote_id){
	$.ajax({
		type:'POST',
		url: '/inc/ajax_showeredmeny.php',
		data:'vote_id='+vote_id,
		success: function(data){
			$('#hrvote').html(data);
		}});
}



$(document).ready(function(){
	//resizer();
//	if($("#panel-1").height()<20){
//			document.location='http://www.hrportal.hu';
//	}
		//
		var scarabflyer_hidden = true;
				$(window).scroll(function(){
				
				if(!readCookie(cookieName)){
					$('#scarab_flyer_close').click(function(){});
					if(getscreenh(magas) && !scarabflyer_closed){
						$('#scarab_flyer').stop().animate({ right: '0px' }, 'slow');
						scarabflyer_hidden = false;
					}else if(!scarabflyer_hidden){
						scarabflyer_hidden = true;
						$('#scarab_flyer').stop().animate({ right: '-400px' }, 'slow');
					}
				}
			});
	
		$('.bxslider').bxSlider({  auto: true,  speed:500,  pause:5000});
		$("#bookmarkme").click(function() {
		  if (window.sidebar) { // Mozilla Firefox Bookmark
			window.sidebar.addPanel(location.href,document.title,"");
		  } else if(window.external) { // IE Favorite
			window.external.AddFavorite(location.href,document.title); }
		  else if(window.opera && window.print) { // Opera Hotlist
			this.title=document.title;
			return true;
		  }
		});
	//alert($( "#div-gpt-ad-1461936145403-6" ).height());

		$(".fancybox").fancybox({
    helpers:  {
		title : {
            type : 'inside'
        },
        
        thumbs : {
            width: 50,
            height: 50,
            position: 'top'
        }
    }
});

		
});
