<?php
 function make_menuitem($text, $url, $new=false, $kereslet=false)
  {
   global $page;
   
   //if ($page==$url) $active=true; else $active=false;
   if (strpos($_SERVER['QUERY_STRING'],$url)===false) $active=false; else $active=true;
   if ($page=="fooldal" and $page==$url) $active=true;
   
   if (substr($url,-1)=="!")
    $url=substr($url,0,-1)."?".SID;
   else
    {
     if ($kereslet==false){
	  if (!ereg("http://",$url) AND ($url!="/linktar.phtml")) 	 
      //if (($url!="/linktar.phtml") AND ($url!="/") AND ($url!="http://hrclub.hu")
	  $url="index.phtml?page=".$url."&".SID;
	 }
     else{
      $url="kereslet_data.phtml?page=".$url."&id=".$kereslet."&".SID;
	 }
    }
   
   switch ($active)
    {
     case false:
               switch ($new)
                {
                 case "feature":
                  $bgcolor="#939BB2";
                  $bgcolor_lighter="#CACDD9";
                  $highlight="Highlight_feature";                   
                 break;
                  
                 case "new":
                  $bgcolor="#DB396F";
                  $bgcolor_lighter="#E55A88";
                  $highlight="Highlight_new";                   
                 break;
                 
                 case "szak":
                  $bgcolor="#DC3A54";
                  $bgcolor_lighter="#DC8694";
                  $highlight="Highlight_szak";                   
                 break;                 
                 
				 case "club":
                  $bgcolor="#959583";
                  $bgcolor_lighter="#B0B79C";
                  $highlight="Highlight_club";                 
                  break;                 
				 
                 default:
                  $bgcolor="#424F78";
                  $bgcolor_lighter="#7081AF";
                  $highlight="Highlight";                 
                  break;                 
                }

               $ret="
                 <tr>\n
                  <td colspan='2' height='1' background='/images/pixel.gif' bgcolor='$bgcolor_lighter'><img src='/images/pixel.gif'></td>\n
                 </tr>\n
                 <tr>\n
                  <td width='14' height='14' background='/images/pixel.gif' bgcolor='$bgcolor_lighter'><img src='/images/pixel.gif'></td>\n
                  <td align='left' height='14' bgcolor='$bgcolor' class='menuitem' onClick=\"window.location.href='$url';\" onMouseOver=\"$highlight(this,1)\" onMouseOut=\"$highlight(this,0)\">&nbsp;&nbsp;&nbsp;<a href='$url' title='$text' class='menuitem'>$text</a></td>\n
                 </tr>\n
                 <tr>\n
                  <td colspan='2' height='1' background='/images/pixel.gif' bgcolor='#000000'><img src='/images/pixel.gif'></td>\n
                 </tr>\n"; 
               break;
     default  :
               $ret="
                 <tr>\n
                  <td colspan='2' height='1' background='/images/pixel.gif' bgcolor='#5A7B9D'><img src='/images/pixel.gif'></td>\n
                 </tr>\n
                 <tr>\n
                  <td colspan='2' align='left' height='14' bgcolor='#003366' class='menuitem' onClick=\"window.location.href='$url';\" onMouseOver=\"Highlight_active(this,1)\" onMouseOut=\"Highlight_active(this,0)\">&nbsp;&nbsp;&nbsp;<a href='$url' title='Emberi er�forr�s Port�l: $text' class='menuitem'>$text</a></td>\n
                 </tr>\n                 
                 <tr>\n
                  <td colspan='2' height='1' background='/images/pixel.gif' bgcolor='#000000'><img src='/images/pixel.gif'></td>\n
                 </tr>   ";              
               break;
    }
     
    return $ret;
  }

 # This function returns a HTTP POST, GET variable
 function http_gpvar($var)
  {
   $return=false;
   @$return=urldecode($HTTP_GET_VARS["$var"]);
   if (!$return)
    @$return=urldecode($HTTP_POST_VARS["$var"]);
   return $return;
  }   
     
 function http_gpcvar($var_name)
  {
   return @$_REQUEST[$var_name];
  }   
       
 function http_get_var($var_name)
  {
   global $_GET;
   return @$_GET[$var_name];
  }

 function http_post_var($var_name)
  {
   global $_POST;  
   return @$_POST[$var_name];
  }
  
 function http_file_var($var_name)
  {
   return @$_FILES[$var_name];
  }  
  
function fine_date($date,$onlydate=false)
 {
  if ($date=="0000-00-00 00:00:00")
   return "- nincs adat -";
   
  $myrow=getdate(strtotime($date));
  $dat=$myrow['year'].".";
  switch($myrow['mon'])
   {
    case 1: $dat.=' janu�r '; break;
    case 2: $dat.=' febru�r '; break;
    case 3: $dat.=' m�rcius '; break;
    case 4: $dat.=' �prilis '; break;
    case 5: $dat.=' m�jus '; break;
    case 6: $dat.=' j�nius '; break;
    case 7: $dat.=' j�lius '; break;
    case 8: $dat.=' augusztus '; break;
    case 9: $dat.=' szeptember '; break;
    case 10: $dat.=' okt�ber '; break;
    case 11: $dat.=' november '; break;
    case 12: $dat.=' december '; break;
   }     
  $dat.=$myrow['mday'].".";
  if ($onlydate==false)
   {
    if ($myrow['hours']!=0 || $myrow['minutes']!=0 || $myrow['seconds']!=0)
     $dat.=" ".(strlen($myrow['hours'])==1?"0".$myrow['hours']:$myrow['hours']).":".(strlen($myrow['minutes'])==1?"0".$myrow['minutes']:$myrow['minutes']).":".(strlen($myrow['seconds'])==1?"0".$myrow['seconds']:$myrow['seconds']);
   }
  return($dat);
 }  
 
function fine_date_digit($date,$onlydate=false)
 {
  if ($date=="0000-00-00 00:00:00")
   return "- nincs adat -";
   
  $myrow=getdate(strtotime($date));
  
  if ($myrow['mon']<10) $myrow['mon']="0".$myrow['mon'];
  if ($myrow['mday']<10) $myrow['mday']="0".$myrow['mday'];
  
  $dat=$myrow['year'].". ".$myrow['mon'].". ".$myrow['mday'].".";
  if ($onlydate==false)
   {
    if ($myrow['hours']!=0 || $myrow['minutes']!=0 || $myrow['seconds']!=0)
     $dat.=" ".(strlen($myrow['hours'])==1?"0".$myrow['hours']:$myrow['hours']).":".(strlen($myrow['minutes'])==1?"0".$myrow['minutes']:$myrow['minutes']).":".(strlen($myrow['seconds'])==1?"0".$myrow['seconds']:$myrow['seconds']);
   }
  return($dat);
 }   
 

// this function loads/saves data from/into a cookie.
function manage_cookie($cookiename)
 {
  if (http_gpvar($cookiename))
   {
    if (isset($_COOKIE[$cookiename]))
    { 
     if (http_gpvar($cookiename)!=$_COOKIE[$cookiename])
      {
       cookieHeader($cookiename,http_gpvar($cookiename),90);
       ${$cookiename}=http_gpvar($cookiename);
      } else
         ${$cookiename}=$_COOKIE[$cookiename];
    }
    else
     {
      ${$cookiename}=http_gpvar($cookiename);
      cookieHeader($cookiename,http_gpvar($cookiename),90); 
     }
    return ${$cookiename};
   } else
   {
    if (isset($_COOKIE[$cookiename]))
     return $_COOKIE[$cookiename];
    else
     return false;
   }
 }

function cookieHeader($name, $value, $expDays=0, $expHours=0, $expMins=0, $expSecs=0, $manualExp=0)
 {
 $exp = ($expDays * 86400) + ($expHours * 3600) + ($expMins * 60) + $expSecs;

 if (ereg("MSIE", getenv("HTTP_USER_AGENT"))) {
    $time = mktime()+$exp;
     $date = gmdate("D, d-M-Y H:i:s \G\M\T", ($time));
     header("Set-Cookie: $name=$value; expires=$date; path=/;");
 } else {
    setcookie($name,$value,time()+$exp,"/");
 }
 
 return true;
}

  # E-mail checker script
  function check_email($mail)
   {
    if(!ereg("^([0-9,a-z,A-Z]+)([.,_,-]([0-9,a-z,A-Z]+))*[@]([0-9,a-z,A-Z]+)([.,_,-]([0-9,a-z,A-Z]+))*[.]([0-9,a-z,A-Z]){2}([0-9,a-z,A-Z])?$",$mail))
     return false; // not valid mail addr
    else
     return true;  // valid addr
   }

# Gives back written category name and subpage name of an article from categ name in DB
function categ_name($cat_db_name)
  {
   global $pname;
   global $cname;
   global $pre_pname;
   global $pre_cname;   
   
   switch ($cat_db_name) {
case "Z_HR-informatika":
   $pname="hr_informatika";
   $cname="HR Informatika";
   break;
case "Z_B�renK�v�li":
   $pname="berenkivuli";
   $cname="B�ren k�v�li juttat�sok";
   break;
case "Z_Kafet�ria":
   $pname="kafeteria";
   $cname="Cafeteria";
   break;   
case "Z_Tr�ning":
   $pname="trening";
   $cname="Tr�ning";
   break;
case "Z_Toborz�s":
   $pname="toborzas";
   $cname="Toborz�s";
   break;
case "Z_Szervezetfejleszt�s":
   $pname="szervezetfejlesztes";
   $cname="Szervezetfejleszt�s";
   break;
case "Z_Kompenz�ci�":
   $pname="kompenzacio";
   $cname="Kompenz�ci�";
   break;
case "Z_Munkak�r":
   $pname="munkakor";
   $cname="Munkak�r";
   break;
case "Z_Kommunik�ci�":
   $pname="kommunikacio";
   $cname="Kommunik�ci�";
   break;
case "Z_K�pz�sFejleszt�s":
   $pname="kepzes_fejlesztes";
   $cname="K�pz�s-fejleszt�s";
   break;      
case "Z_Teljes�tm�ny-�rt�kel�s":
   $pname="teljesitmeny_ertekeles";
   $cname="Teljes�tm�ny-�rt�kel�s";
   break;   
case "Z_Elbocs�t�s":
   $pname="elbocsatas";
   $cname="Elbocs�t�s";
   break;   
case "Z_Munkaeropiac":
   $pname="munkaero_piac";
   $cname="Munkaer�piac";
   break;   
case "Z_Munkav�delem":
   $pname="munkavedelem";
   $cname="Munkav�delem";
   break;   
case "H�rek":
   $pname="hr_hirek";
   $cname="Munka�gyi h�rek";
   break;      
case "P�ly�zatok":
   $pname="palyazatok";
   $cname="Munka�gyi p�ly�zatok";
   break;   
case "EU":
   $pname="eu_hirek";
   $cname="A nagyvil�g h�rei";
   break;   
case "Z_Munkajog":
   $pname="munkajog";
   $cname="Munkajog";
   break;   
case "B�rsz�mfejt�s":
   $pname="kompenzacio";
   $cname="Kompenz�ci�";
   break;      
case "Magazin":
   $pname="magazin";
   $cname="Magazin";
   break;      
case "C�gh�rek":
   $pname="vallalati_hirek";
   $cname="V�llalati h�rek";
   break;   
case "Oktat�s":
   $pname="oktatas";
   $cname="Oktat�s";
   break;   
case "Tanulm�nyok":
   $pname="tanulmanyok";
   $cname="Tanulm�nyok";
   break;   
case "Portr�":
   $pname="portre";
   $cname="Portr�";
   break;      
case "Heti aj�nl�":
   $pname="fooldal";
   $cname="Heti aj�nl�";
   break;      
case "Z_Interim":
   $pname="interim_management";
   $cname="Interim management";
   break;         
case "Z_Europass":
   $pname="europass";
   $cname="Europass";
   $pre_pname="toborzas";
   $pre_cname="Toborz�s";
   break;            
case "Z_K�lcs�nz�s":
   $pname="munkaero_kolcsonzes";
   $cname="Munkaer�-k�lcs�nz�s";
   $pre_pname="toborzas";
   $pre_cname="Toborz�s";
   break;               
}
return true;
} 

function dateDiff($dformat, $endDate, $beginDate)
{
$date_parts1=explode($dformat, $beginDate);
$date_parts2=explode($dformat, $endDate);
#echo "<h1>".$date_parts1[0].$date_parts1[1].$date_parts1[2]."</h1>";
$start_date=GregorianToJD($date_parts1[0], $date_parts1[1], $date_parts1[2]);
$end_date=GregorianToJD($date_parts2[0], $date_parts2[1], $date_parts2[2]);
$ready=$end_date-$start_date;
return $ready;
}

function ezDate($d) {
        $ts = time() - strtotime(str_replace("-","/",$d));
        
        if($ts>31536000) $val = round($ts/31536000,0).'&nbsp;�ve';
        else if($ts>2419200) $val = round($ts/2419200,0).'&nbsp;h�napja';
        else if($ts>604800) $val = round($ts/604800,0).'&nbsp;hete';
        else if($ts>86400) $val = round($ts/86400,0).'&nbsp;napja';
        else if($ts>3600) $val = round($ts/3600,0).'&nbsp;�r�ja';
        else if($ts>60) $val = round($ts/60,0).'&nbsp;perce';
        else $val = $ts.'&nbsp;mperce';
        
       # if($val>1) $val .= 's';
        return $val;
}


function elvalaszt($text,$nr=10)
{
    $mytext=explode(" ",trim($text));
    $newtext=array();
    foreach($mytext as $k=>$txt)
    {
        if (strlen($txt)>$nr)
        {
            $txt=wordwrap($txt, $nr, "-", 1);
        }
        $newtext[]=$txt;
    }
    return implode(" ",$newtext);
} 	

function openad($zoneid)
{
	echo"
<script type='text/javascript'><!--//<![CDATA[
   var m3_u = (location.protocol=='https:'?'https://hrportal.hu/hik/www/delivery/ajs.php':'http://hrportal.hu/hik/www/delivery/ajs.php');
   var m3_r = Math.floor(Math.random()*99999999999);
   if (!document.MAX_used) document.MAX_used = ',';
   document.write (\"<scr\"+\"ipt type='text/javascript' src='\"+m3_u);
   document.write (\"?zoneid=".$zoneid."\");
   document.write ('&amp;cb=' + m3_r);
   if (document.MAX_used != ',') document.write (\"&amp;exclude=\" + document.MAX_used);
   document.write (\"&amp;loc=\" + escape(window.location));
   if (document.referrer) document.write (\"&amp;referer=\" + escape(document.referrer));
   if (document.context) document.write (\"&context=\" + escape(document.context));
   if (document.mmm_fo) document.write (\"&amp;mmm_fo=1\");
   document.write (\"'><\/scr\"+\"ipt>\");
//]]>--></script>	
	";
    return true;
} 	


?>
