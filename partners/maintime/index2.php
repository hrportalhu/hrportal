<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>

  <head>
    <title>MAN IN TIME Consulting</title>
    
    <!-- import .css file -->
    <link rel="stylesheet" type="text/css" href="manintime.css" />
    
    <!-- import java script functions -->
    <script src="assets/manintime.js"></script>

  </head>

  <body>
    <div>
      <table width="100%" class="header" cellspacing="0" cellpadding="0">
  <tr height="28px">
    <td class="header" ><a class="header" href="index.php?site=kikvagyunk">KIK VAGYUNK?</a></td>
    <td class="header" ><a class="header" href="index.php?site=filozofiank">FILOZ�FI�NK</a></td>

    <td class="header" ><a class="header" href="index.php?site=szolgaltatasaink">SZOLG�LTAT�SAINK</a></td>
    <td class="header" ><a class="header" href="index.php?site=karriertanacsadas">KARRIERTAN�CSAD�S</a></td>
    <td class="header" ><a class="header" href="index.php?site=allasajanlatok">�LL�SAJ�NLATOK</a></td>
    <td class="header" ><a class="header" href="index.php?site=partnereink">PARTNEREINK</a></td>
    <td class="header" ><a class="header" href="index.php?site=media">M�DIA</a></td>
    <td class="header" ><a class="header" href="index.php?site=kapcsolat">KAPCSOLAT</a></td>

  </tr>
  <tr>
    <td colspan="8" height="6px" style="background: #646464;"></td>
  </tr>
  <tr height="4px" style="background: #ffffff;">
    <td ></td>
    <td ></td>
    <td ></td>
    <td ></td>

    <td ></td>
    <td ></td>
    <td ></td>
    <td ></td>
  </tr>
</table>

    </div>
    <div>
      

<table>
  <tr>
    <td><img src="./images/logo.bmp"></td>
    <td>
      <table>
        <tr height="30">
          <td width="160" align="left"><img src="./images/leftline.bmp"></td>
          <td width="485" align="left">
            <a href="index.php?site=szolgaltatasaink&subsite=hrtanacsadas" id="smenu">HR TAN�CSAD�S, </a>

            <a href="index.php?site=szolgaltatasaink&subsite=hroutsourcing" id="smenu">HR OUTSOURCING, </a>
            <a href="index.php?site=szolgaltatasaink&subsite=csrtanacsadas" id="smenu">CSR TAN�CSAD�S </a>
          </td>
          <td></td>
        </tr>
        <tr height="30">
          <td></td>
          <td width="485" align="right">

            <a href="index.php?site=szolgaltatasaink&subsite=munkaerokereses" id="smenu">MUNKAER� KERES�S, </a>
            <a href="index.php?site=szolgaltatasaink&subsite=kepzes" id="smenu">K�PZ�S, FEJLESZT�S, COACHING </a>
          </td>
          <td width="160" align="right" ><img src="./images/rightline.bmp"></td>
        </tr>
      </table>
    </td>
  </tr>

</table>

    </div>
    <div class="menu">
      <table width="100%" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <table class="menupont" cellspacing="0" cellpadding="0">
        <tr id="line"><td></td></tr>
        <tr class="menupont">

          <td id="tdspace"></td>
          <td id="tdmarkeroff"></td><td id="tdcontentoff"><a href="index.php?site=ajanlatkeres" id="amenu">AJ�NLATK�R�S</a></td>
        </tr>
        <tr id="line"><td></td></tr>
        <tr class="menupont">
          <td id="tdspace"></td>
          <td id="tdmarkeroff"></td><td id="tdcontentoff"><a href="index.php?site=hrklub" id="amenu">HR KLUB</a></td>
        </tr>

        <tr id="line"><td></td></tr>
        <tr class="menupont">
          <td id="tdspace"></td>
          <td id="tdmarkeroff"></td><td id="tdcontentoff"><a href="index.php?site=rolunkmondtak" id="amenu">R�LUNK MONDT�K</a></td>
        </tr>
        <tr id="line"><td></td></tr>
        <tr class="menupont">
          <td id="tdspace"></td>

          <td id="tdmarkeroff"></td><td id="tdcontentoff"><a href="index.php?site=olvasnivalo" id="amenu">OLVASNIVAL�</a></td>
        </tr>
        <tr id="line"><td></td></tr>
        <tr class="menupont">
          <td id="tdspace"></td>
          <td id="tdmarkeroff"></td><td id="tdcontentoff"><a href="index.php?site=hasznosoldalak" id="amenu">HASZNOS OLDALAK</a></td>
        </tr>
        <tr id="line"><td></td></tr>

      </table>
    </td>
    <td><img src="./images/kep1.bmp"></td>
    <td><img src="./images/kep2.bmp"></td>
    <td><img src="./images/kep3.bmp"></td>
    <td><img src="./images/kep4.bmp"></td>
  </tr>
</table>
    </div>

    <div class="content">
      <table><tr>
<td valign="top">
<p class="head"><b>BEMUTATKOZ�S</b></p>
<div id="szovegblock">
  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. In viverra leo. Morbi pellentesque velit at neque. Maecenas vulputate tincidunt ipsum. Suspendisse potenti. Suspendisse tellus. Sed eget nisl in risus elementum porta. Aliquam urna. Aenean tincidunt odio nec quam. Morbi non est. Cras porttitor tincidunt elit.</p>
  <p>Etiam quis metus a metus porttitor dapibus. Maecenas a libero eget sapien molestie consequat. Sed neque sapien, vulputate vel, lacinia ac, semper eget, elit. In magna quam, malesuada sit amet, elementum quis, aliquam ut, erat. Morbi ligula nisi, viverra vitae, convallis non, fringilla vel, odio. Sed faucibus mattis lectus. Sed orci. Curabitur consectetuer. In sollicitudin est eu elit. Cras mauris ante, semper ultricies, malesuada eget, accumsan at, neque. Vestibulum in risus. Vestibulum sodales, mauris in vulputate venenatis, justo orci tincidunt pede, nec lacinia elit nulla quis orci. Sed sit amet nunc sit amet elit sagittis commodo. Mauris pharetra tincidunt arcu. Nulla ac massa eget est facilisis ultrices. Vestibulum nulla leo, luctus vel, pulvinar non, rutrum ac, purus. Aenean augue massa, gravida sed, tincidunt eget, feugiat vel, justo. Curabitur adipiscing lobortis risus.</p>
  <p>Sed tellus massa, dapibus eu, pulvinar eu, varius nec, lorem. Integer rhoncus, pede ac porttitor varius, justo pede ultricies metus, nec adipiscing eros nisi non risus. Nulla nunc. Donec dolor. In tincidunt massa ac erat. Nullam dictum, lacus ut scelerisque facilisis, dui urna fringilla odio, sit amet tempus lectus mauris sed pede. Praesent vehicula pellentesque libero. Nullam pellentesque. Suspendisse vehicula metus vitae odio. Donec porta neque et augue. Vivamus fermentum, enim a aliquet tincidunt, nunc diam adipiscing augue, sed egestas justo quam quis ipsum. Nunc vel dolor. Aenean accumsan vestibulum purus.</p>

  <p>Etiam quis metus a metus porttitor dapibus. Maecenas a libero eget sapien molestie consequat. Sed neque sapien, vulputate vel, lacinia ac, semper eget, elit.</p>
</div>
</td>
<td valign="top" width="340px">
	<iframe src='maintime_nyito.php' frameborder='NO' scrolling='NO' width="100%" height="380px"></iframe>
   
</td>
</tr></table>    </div>
    <div class="footer">
      <table width="100%" class="footer">
        <tr>
          <td class="footer">Man In Time Consulting Bt.</td>

          <td class="footer">2146 Mogyor�d, Szent Jakab park 62.</td>
          <td class="footer">Tel: +36 28 123-89-65</td>
          <td class="footer">E-mail: info@manintime.hu</td>
        </tr>
      </table>
    </div>
  </body>

</html>
