function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function table_go(height) {
	allh = (document.documentElement.clientHeight-height);
	if (allh>$('main').getHeight())
 		$('main').style.height=allh+'px';
}
function placeIt(i,e) {
		Position.absolutize(e);
		topt = e.style.top;
		leftt = e.style.left;
		Position.relativize(e);		
		Position.absolutize($(i));	
		$(i).style.top = (parseInt(topt)+25)+'px';
		$(i).style.left = (parseInt(leftt)+15)+'px';
//		Position.relativize($(i));	
}
function placePDMenu(i,e) {
		Position.absolutize(e);
		topt = e.style.top;
		leftt = e.style.left;
		Position.relativize(e);		
		Position.absolutize($(i));	
		$(i).style.top = (parseInt(topt)+32)+'px';
		$(i).style.left = (parseInt(leftt)-3)+'px';
//		Position.relativize($(i));	
}

function hideIt(i) {
	if ($(i).style.display == '') {	
		new Effect.BlindUp(i/*,Object.extend({afterFinishInternal: function(effect) { 
																																			        effect.element.hide().undoClipping();
																																							table_go()}})*/);
	}
}
function waithideIt(i) {
		if ($(i).timeout) {
			clearTimeout($(i).timeout);			
		}
}
function dohideIt(i) {
		if ($(i).timeout)
			clearTimeout($(i).timeout);
		$(i).timeout = setTimeout('hideIt("'+i+'")',3000);
}
function showIt(i,e) {
	var fu = i;
	if ($(i).style.display == 'none') {
		$(i).style.visibility = 'hidden';	
		$(i).style.display = '';	
		placeIt(i,e);		
		$(i).style.display = 'none';		
		$(i).style.visibility = 'visible';			
		new Effect.BlindDown(i/*,Object.extend({afterFinishInternal: function(effect) { 												 
												       effect.element.undoClipping();
															 table_go()}})*/);
		dohideIt(i)
	} else {
		if ($(i).timeout)
			clearTimeout($(i).timeout);			
		hideIt(i);
//		placeIt(i,e);		
	}
}
function showPDMenu(i,e) {
	var fu = i;
	if ($(i).style.display == 'none') {
		$(i).style.visibility = 'hidden';	
		$(i).style.display = '';	
		placePDMenu(i,e);		
		$(i).style.display = 'none';		
		$(i).style.visibility = 'visible';			
		new Effect.BlindDown(i);
		dohideIt(i)
	} else {
		if ($(i).timeout)
			clearTimeout($(i).timeout);			
		hideIt(i);
//		placeIt(i,e);		
	}
}