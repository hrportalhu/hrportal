<html>
<head>
<title>TTI Hungary Kft.</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-2">
<!-- Fireworks MX Dreamweaver MX target.  Created Wed Nov 01 22:23:16 GMT+0100 (K�z�p-eur�pai t�li ido) 2006-->
<base href="http://www.tti-hungary.hu">
<link href="tti.css" rel="stylesheet" type="text/css">
<script language=JavaScript src="menu.js"></script><script language=JavaScript src="horizmenu_hier.js"></script>
<script language=JavaScript src="ma_hier.js"></script>
<link href="mv_menu.css" rel=stylesheet>
<style>
.leadtext {
	font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	line-height: 16px;
	color:#696969;
}

.body {
	font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	line-height: 16px;
	color:#696969;
}

a {
	color: Blue;
	text-decoration: none;
}

a:hover {
	
	background-color: #e5c6b7;
	text-decoration: none;
}
</style>
</head>
<body bgcolor="#CCCCCC" link="#FFFFFF" vlink="#FFFFFF" alink="#FFFFFF" leftmargin="0" topmargin="0">
<script language="JavaScript">
new menu (horizmenuHierarchy, [
{
	'height':  25,
	'width' : 70,
	'firstX' : 460,
	'firstY' : 44,
	'nextX' : 1,
	'hideAfter' : 200,
	'css'   : 'gurtl0',
	'trace' : true
},
{
	'height':  23,
	'width' : 155,
	'firstY' : 30,
	'firstX' : -10,
	'nextY' : -1,
	'css' : 'gurtl1'
},
{
	'firstX' : 130,
	'firstY' : 0
}
])
</script>

<script language="JavaScript">
new menu (ma_Hierarchy, [
{
	'height':  25,
	'width' : 150,
	'firstX' : 20,
	'firstY' : 200,
	'nextY' : 3,
	'hideAfter' : 200,
	'css'   : 'gurtl0',
	'trace' : true
},
{
	'width' : 160,
	'firstY' :  0,
	'firstX' : 215,
	'nextY' : -1,
	'css' : 'gurtl1'
},
{
	'firstX' : 160,
	'firstY' : 0
}
])
</script>
<table border="0" cellpadding="0" cellspacing="0" width="1000">
<!-- fwtable fwsrc="allasker.png" fwbase="allasker.jpg" fwstyle="Dreamweaver" fwdocid = "742308039" fwnested="0" -->
  <tr>
   <td><img src="images/spacer.gif" width="214" height="1" border="0" alt=""></td>
   <td><img src="images/spacer.gif" width="112" height="1" border="0" alt=""></td>
   <td><img src="images/spacer.gif" width="64" height="1" border="0" alt=""></td>
   <td><img src="images/spacer.gif" width="57" height="1" border="0" alt=""></td>
   <td><img src="images/spacer.gif" width="160" height="1" border="0" alt=""></td>
   <td><img src="images/spacer.gif" width="121" height="1" border="0" alt=""></td>
   <td><img src="images/spacer.gif" width="103" height="1" border="0" alt=""></td>
   <td><img src="images/spacer.gif" width="40" height="1" border="0" alt=""></td>
   <td><img src="images/spacer.gif" width="129" height="1" border="0" alt=""></td>
   <td><img src="images/spacer.gif" width="1" height="1" border="0" alt=""></td>
  </tr>

  <tr>
   <td><img name="allasker_r1_c1" src="images/allasker_r1_c1.gif" width="214" height="87" border="0" alt=""></td>
   <td><a href="index.htm"><img name="index_r1_c2" src="images/index_r1_c2.gif" width="112" height="87" border="0" alt=""></a></td>
   <td colspan="2"><a href="magunkrol.htm"><img name="index_r1_c3" src="images/index_r1_c3.gif" width="121" height="87" border="0" alt=""></a></td>
   <td><img name="index_r1_c5" src="images/index_r1_c5.gif" width="160" height="87" border="0" alt=""></td>
   <td><a href="oldalterkep.htm"><img name="index_r1_c6" src="images/index_r1_c6.gif" width="121" height="87" border="0" alt=""></a></td>
   <td><a href="kapcsolat.htm"><img name="index_r1_c7" src="images/index_r1_c8.gif" width="103" height="87" border="0" alt=""></a></td>
   <td colspan="2"><img name="index_r1_c8" src="images/index_r1_c9.gif" usemap="#index_r1_c9Map" width="169" height="87" border="0" alt=""></td>
   <td><img src="images/spacer.gif" width="1" height="87" border="0" alt=""></td>
  </tr>
  <tr>
   <td valign="top" bgcolor="#D1A089"><img name="allasker_r2_c1" src="images/munkaado_r2_c1.gif" width="214" height="81" border="0" alt=""></td>
   <td colspan="2" rowspan="2" valign="top" bgcolor="#AD6442"><img name="allasker_r2_c2" src="images/mv_r2_c2.jpg" width="176" height="204" border="0" alt=""></td>
   <td rowspan="4" colspan="5" valign="top" bgcolor="#ffffff"><table width="100%" border="0" cellspacing="0" cellpadding="0">
     <tr>
       <td width="10">&nbsp;</td>
       <td valign="top">
	   